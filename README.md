
# 个人博客系统搭建

## 写在前面
***
+ 之前一直在`CSDN`上写博文，一次改粉丝可见性，有一篇整理了三年的`博文`被`CSDN`吃了>_<|||，就给我留了几个字。
![首页](/imgs/csdn.png)
那天晚上伤心了好久，觉也没睡，费了好多时间，还是上学时就开始写的，也有一些小伙伴收藏，所以吸取教训，想自己搞一个备份。自己搭一个博客系统，在`Github`上托管，后期会放到`Gitee`上，现在`Gitee`上的`Gitee Page `功能不支持。`Github`上大多时候是访问不到的。
+ 笔记主要是关于自己搭建的过程，遇到的坑的一些总结，给小伙伴提供一些避坑经验。同时上传了自己的代码，可以直接拉去我的快速搭建。
+ 笔记由两部分内容构成: `博客展示`和`具体的搭建教程`。

***
💪💪 博客搭建思路：基于`hexo`+`Amazing` 来实现，本地写好 markdown 之后，在本地将 markdown 渲染成 HTML，然后将渲染好的 HTML 上传到服务端(Github)。所以不需要`数据库`，`生产部署`不需要环境，很方便。在本地搭建的时候需要`Node.js` 、`Git`环境。

💪💪  关于 `hexo`:`Hexo `是一个快速、简洁且高效的`博客框架`。Hexo 使用 `Markdown`(或其他渲染引擎)解析文章，在几秒内，即可利用靓丽的主题生成静态网页。建议小伙伴移步官网了解下：[https://hexo.io/zh-cn/docs/](https://hexo.io/zh-cn/docs/)

💪💪  关于`Amazing`主题：是一个基于主题`icarus`基础之上的一个主题，个人比较喜欢。了解更多请移步 [https://github.com/removeif/hexo-theme-amazing](https://github.com/removeif/hexo-theme-amazing)

💪💪 如果小伙伴想自己学着搭建，建议小伙伴直接按照官网教程来搞，很easy，这里分享一些`避坑经验`
- [X] <font color="#E6A23C">**本地的搭好的博客系统代码，不要和渲染的`HTML`放到一个`Github`库里，不然本地的搭建代码会被屏蔽掉。**</font>本地的代码如果没有其他用，可以不上传`Github`仓库。
- [X] 关于`Amazing`主题中,`评论模块`和`碎碎念`等`issue`需要申请`Gitup客户端`授权，这里可以参考[使用说明系列(部署中有问题此issue讨论):https://github.com/removeif/hexo-theme-amazing/issues/16](https://github.com/removeif/hexo-theme-amazing/issues/16)
- [X] 推荐一篇关于博客搭建的博客[不用花一分线，松哥手把手教你上线个人博客](http://www.javaboy.org/2020/0106/hexo-blog.html)
***

💪💪 如果小伙伴想方便一点，也可以直接拉我的代码，按照下面的部署方式就可以了。

💪💪 关于搭建好博客系统如何`写文章`，其实特别方便，需要指定文件夹新建一个`markdown`文件，内容如下：
```markdown
title: 标题
top: 1
toc: 是否显示目录
recommend: 文章推荐权值 
keywords: 关键字
date: 2019-09-19 22:10:43  日期
thumbnail:
tags: 工具教程 标签
categories: [工具教程,主题工具] 分类

博文摘要
<!-- more -->
博文正文

```
之前就啥都不用管了，一且OK了.

Ok,先展示一下我的成果，然后我们谈谈如何搭建
***
## 博客展示

#### 我的博客地址:[https://liruilongs.github.io/](https://liruilongs.github.io/)

### 🎈🎈🎈🎈 PC端效果🎈🎈🎈🎈 

#### 首页
![首页](/imgs/main.png)
#### 归档
![归档](/imgs/archives.png)
#### 博客阅读
![博客阅读](/imgs/content.png)

***
### 🎈🎈🎈🎈 平板电脑🎈🎈🎈🎈 

#### 首页
![](/imgs/mainpb.png)
#### 博客阅读(主题跟换)
![](/imgs/mess.png)
***
![](/imgs/messb.png)
#### 留言墙
![](/imgs/message.png)

### 🎈🎈🎈🎈 手机端🎈🎈🎈🎈 

|主页|音乐|标签|关于|分类|
|--|--|--|--|--|
|![](/imgs/mains.jpg)|![](/imgs/messs.jpg)|![](/imgs/tag.jpg)|![](/imgs/gaunyu.jpg)|![](/imgs/fenlei.jpg)|


## 具体的搭建教程

#### 默认小伙伴已经安装了`Node.js` 、`Git`环境。并且`Node版本`满足要求，`Git配置了SSH免密`。

#### 具体的环境要求参考官网：[https://hexo.io/docs/](https://hexo.io/docs/)

🎈🎈下面我们愉快的开始吧🎈🎈
*** 
💪 Git Bash 执行：
```bash
$ git clone git@gitee.com:liruilonger/blog.git
```
💪 在`blog`目录下进入`cmd`执行
```bash
npm install
```
💪 在`themes`目录同样执行
```bash
cd ./themes
npm install
```
💪 如果以上出现问题，可以按照错误提示或百度，自行解决。

🎈🎈🎈🎈 做到这一部我们的博客框架就已经搭建完成啦 🎈🎈🎈🎈

💪 下面就是生成静态文件
```bash
hexo g
```
💪 启动服务器。默认情况下，访问网址为： http://localhost:4000/
```bash
hexo s
```
控制台打印：
```bash
D:\code\blogger\blogger\themes>hexo s
INFO  Validating config
Inferno is in development mode.
Inferno is in development mode.
INFO  =======================================
 ██╗ ██████╗ █████╗ ██████╗ ██╗   ██╗███████╗
 ██║██╔════╝██╔══██╗██╔══██╗██║   ██║██╔════╝
 ██║██║     ███████║██████╔╝██║   ██║███████╗
 ██║██║     ██╔══██║██╔══██╗██║   ██║╚════██║
 ██║╚██████╗██║  ██║██║  ██║╚██████╔╝███████║
 ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝
=============================================
INFO  === Checking package dependencies ===
INFO  === Checking theme configurations ===
INFO  === Registering Hexo extensions ===
INFO  Start processing
INFO  Hexo is running at http://localhost:4000 . Press Ctrl+C to stop.
INFO  Good bye
```
🎈 🎈 之后我们就可以在本地访问啦。🎈 🎈 

## 部署：结合Github Page 发布静态html

### 小伙伴可以参考官方文档[https://hexo.io/zh-cn/docs/one-command-deployment](https://hexo.io/zh-cn/docs/one-command-deployment),我们这里也是采用这种方式部署。
#### 默认小伙伴已经有个Github 账户，并且已经申请Github客户端的`ID`和`密匙`,关于[Github客户端的授权申请(可以在这里找一下)](https://github.com/removeif/hexo-theme-amazing/issues/16) 


这里需要修改发布的Github库为自己的库:[https://gitee.com/liruilonger/blog/blob/master/_config.yml#L104](https://gitee.com/liruilonger/blog/blob/master/_config.yml#L104)，并且这个库已开通`Github Page`。关于`Github Page`请自行百度。

```yaml
deploy:
  type: git
  repo: git@github.com:LIRUILONGS/LIRUILONGS.github.io.git # 自己的库URL
  branch: master #部署的分支

```
🎈 🎈 之后我们就可以使用命令部署了,部署完成之后就可以根据`Github Page`访问啦🎈 🎈 
```bash
hexo d
```

OK这样我们就完成了简单部署，

`日常写作`中，我们可以在写完博客之后，`编译`，然后`发布`，就会直接推送到我们的`Github Page`的库里面。 
```bash
hexo g
hexo d
```

```bash
D:\code\blogger\blogger\themes>hexo g
INFO  Validating config
Inferno is in development mode.
Inferno is in development mode.
INFO  =======================================
 ██╗ ██████╗ █████╗ ██████╗ ██╗   ██╗███████╗
 ██║██╔════╝██╔══██╗██╔══██╗██║   ██║██╔════╝
 ██║██║     ███████║██████╔╝██║   ██║███████╗
 ██║██║     ██╔══██║██╔══██╗██║   ██║╚════██║
 ██║╚██████╗██║  ██║██║  ██║╚██████╔╝███████║
 ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝
=============================================
INFO  === Checking package dependencies ===
INFO  === Checking theme configurations ===
INFO  === Registering Hexo extensions ===
INFO  Start processing
INFO  Files loaded in 2.45 s
INFO  Generated: album/index.html
INFO  Generated: media/index.html
INFO  Generated: about/index.html
INFO  Generated: message/index.html
INFO  Generated: content.json
INFO  5 files generated in 3.07 s

D:\code\blogger\blogger\themes>hexo d
INFO  Validating config
Inferno is in development mode.
Inferno is in development mode.
INFO  =======================================
 ██╗ ██████╗ █████╗ ██████╗ ██╗   ██╗███████╗
 ██║██╔════╝██╔══██╗██╔══██╗██║   ██║██╔════╝
 ██║██║     ███████║██████╔╝██║   ██║███████╗
 ██║██║     ██╔══██║██╔══██╗██║   ██║╚════██║
 ██║╚██████╗██║  ██║██║  ██║╚██████╔╝███████║
 ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝
=============================================
INFO  === Checking package dependencies ===
INFO  === Checking theme configurations ===
INFO  === Registering Hexo extensions ===
INFO  Deploying: git
INFO  Clearing .deploy_git folder...
INFO  Copying files from public folder...
INFO  Copying files from extend dirs...
warning: LF will be replaced by CRLF in 20
```
关于`Amazing`主题中`评论留言`功能的`Github`相关修改，请小伙伴参考`Amazing`主题作者的[README.md](https://gitee.com/liruilonger/blog/blob/master/themes/amazing/README.md)
有问题的小伙伴欢迎交流。
***

我的博客地址:[https://liruilongs.github.io/](https://liruilongs.github.io/)

另，推荐一个聚合图床:https://www.superbed.cn/timeline  ，嗯，我也不知道它啥时候死，先用着。



