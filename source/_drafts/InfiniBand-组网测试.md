---
title: InfiniBand 组网测试
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-04-28 02:17:47/InfiniBand 组网测试.html'
mathJax: false
date: 2023-04-28 10:17:47
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

### 简单介绍

InfiniBand是一种高性能计算和数据中心网络技术。它提供了一种低延迟、高带宽和可靠性的连接方式，用于连接服务器、存储设备和其他计算资源。InfiniBand的设计目标是为数据中心和企业级网络提供更好的性能和可扩展性。

InfiniBand技术支持多种拓扑结构，包括点对点、全局共享存储、矩形网格等。采用InfiniBand连接的设备可以通过RDMA（Remote Direct Memory Access）技术实现零拷贝数据传输，在减少CPU负载的同时提高了应用程序的性能。此外，InfiniBand还提供了诸如缓冲区管理、QoS（Quality of Service）和虚拟化等特性，以支持高效的数据传输和资源管理。

由于其高性能、低延迟和可靠性，InfiniBand已成为许多高性能计算和数据中心环境的首选互连技术。 InfiniBand技术也被广泛应用于科学计算、金融交易、电信和网络服务等领域，以满足高性能、低延迟和大规模数据传输需求。


### 名词的解释


在InfiniBand组网中，涉及了一些专有名词和术语，以下是这些名词的解释：

+ InfiniBand适配器（`HCA`）(网卡)： InfiniBand适配器是连接InfiniBand网络的设备，负责将计算机系统中的数据和控制信息转换为InfiniBand协议格式。
+ InfiniBand子网管理器（`SM`）(InfiniBand 交换机)： InfiniBand子网管理器是一个特殊的InfiniBand适配器，用于管理整个InfiniBand网络。它负责配置和维护InfiniBand网络的路由表、端口状态和链路速率等信息。
+ 连接建立（connect）： 连接建立是指在两个节点之间建立通信连接的过程。在这个过程中，两个InfiniBand适配器交换相应的信息并确定通信能力和资源。
+ InfiniBand数据报（IB包）： InfiniBand数据报是在InfiniBand网络上传输数据的基本单位。它包括源和目标地址、传输的数据、以及其他元数据。
+ Remote Direct Memory Access（RDMA）： RDMA是一种在InfiniBand网络上实现零拷贝数据传输的技术。它允许数据直接从内存复制到另一个计算机系统的内存，而无需通过CPU进行中间处理。
+ 链路速率（link speed）： 链路速率是指InfiniBand网络中物理链路上的最大传输速率。它通常以Gbps（千兆比特每秒）为单位表示。
+ 断开连接（disconnect）： 断开连接是指在两个节点之间结束通信连接的过程，使得每个适配器和端口可以重新分配给其他节点。





```bash
⬢[root@toolbox ~]# ibstat
CA 'mlx4_0'
        CA type: MT4099
        Number of ports: 2
        Firmware version: 2.42.5000
        Hardware version: 1
        Node GUID: 0x0002c903003fc5e0
        System image GUID: 0x0002c903003fc5e3
        Port 1:
                State: Active
                Physical state: LinkUp
                Rate: 40
                Base lid: 45
                LMC: 0
                SM lid: 1
                Capability mask: 0x02514868
                Port GUID: 0x0002c903003fc5e1
                Link layer: InfiniBand
        Port 2:
                State: Active
                Physical state: LinkUp
                Rate: 40
                Base lid: 46
                LMC: 0
                SM lid: 1
                Capability mask: 0x02514868
                Port GUID: 0x0002c903003fc5e2
                Link layer: InfiniBand
⬢[root@toolbox ~]#
```

+ CA类型： HCA型号为 MT4099。
+ 端口数量： HCA有2个端口。
+ 固件版本： HCA固件版本为2.42.5000。
+ 硬件版本： HCA硬件版本为1。
+ 节点GUID： 此HCA的节点全局唯一标识符（GUID）为0x0002c903003fc5e0。
+ 系统镜像GUID： 此HCA的系统镜像GUID为0x0002c903003fc5e3。
+ 端口1： 这提供了有关HCA端口1的信息：
+ 状态： 端口1的状态为“活动”。
+ 物理状态： 端口1的物理状态为“LinkUp”。
+ 速率： 端口1的数据传输速率为40 Gbps。
+ 基本LID： 端口1的基本LID（本地标识符）为45。
+ LMC： 端口1的LMC（LID掩码计数）为0，表示仅使用基本LID。
+ SM LID： 端口1的SM LID（子网管理器LID）为1。
+ 能力掩码： 端口1的能力掩码为0x02514868。
+ 端口GUID： 端口1的端口GUID为0x0002c903003fc5e1。
+ 链接层： 端口1使用的链接层协议是InfiniBand。


----


+ `ibdiagnet`	检测当前集群下物理链路的健康情况	


+ `ibnetdiscover -p`:	扫描网络并显示每个端口的 LID、GUID、机器名和端口号等信息。			

+ 

```bash
⬢[root@toolbox ~]# ibnetdiscover
#
# Topology file: generated on Fri Apr 28 11:09:33 2023
#
# Initiated from node 0002c903003fc5e0 port 0002c903003fc5e1

vendid=0x2c9
devid=0xcb20
sysimgguid=0x248a070300e43740
switchguid=0x248a070300e43740(248a070300e43740)
Switch  36 "S-248a070300e43740"         # "MF0;sw02:MSB7700/U1" enhanced port 0 lid 1 lmc 0
[1]     "H-0002c9030039d940"[2](2c9030039d942)          # "MT25408 ConnectX Mellanox Technologies" lid 50 4xQDR
[2]     "H-0002c903003638a0"[2](2c903003638a2)          # "MT25408 ConnectX Mellanox Technologies" lid 48 4xQDR
[3]     "H-0002c903004214f0"[2](2c903004214f2)          # "MT25408 ConnectX Mellanox Technologies" lid 44 4xQDR
[4]     "H-0002c903003fc5e0"[2](2c903003fc5e2)          # "MT25408 ConnectX Mellanox Technologies" lid 46 4xQDR
[5]     "H-0002c90300427f50"[1](2c90300427f51)          # "MT25408 ConnectX Mellanox Technologies" lid 42 4xQDR
[6]     "H-0002c903003aa5f0"[2](2c903003aa5f2)          # "MT25408 ConnectX Mellanox Technologies" lid 39 4xQDR
[9]     "H-0002c90300feeed0"[2](2c90300feeed2)          # "MT25408 ConnectX Mellanox Technologies" lid 38 4xQDR
[10]    "H-0002c90300363bf0"[2](2c90300363bf2)          # "MT25408 ConnectX Mellanox Technologies" lid 36 4xQDR
[11]    "H-0002c9030030b690"[2](2c9030030b692)          # "MT25408 ConnectX Mellanox Technologies" lid 34 4xQDR
[12]    "H-0002c903003997e0"[2](2c903003997e2)          # "MT25408 ConnectX Mellanox Technologies" lid 52 4xQDR
[13]    "H-24be05ffffce1730"[2](24be05ffffce1732)               # "MT25408 ConnectX Mellanox Technologies" lid 30 4xQDR
[14]    "H-0002c903003a0560"[1](2c903003a0561)          # "MT25408 ConnectX Mellanox Technologies" lid 27 4xQDR
[17]    "H-0002c903003ea9e0"[2](2c903003ea9e2)          # "MT25408 ConnectX Mellanox Technologies" lid 26 4xQDR
[18]    "H-0002c9030039fb00"[2](2c9030039fb02)          # "MT25408 ConnectX Mellanox Technologies" lid 24 4xQDR
[19]    "H-24be05ffffce5680"[2](24be05ffffce5682)               # "MT25408 ConnectX Mellanox Technologies" lid 22 4xQDR
[20]    "H-0002c9030039b950"[2](2c9030039b952)          # "MT25408 ConnectX Mellanox Technologies" lid 20 4xQDR
[21]    "H-0002c9030040ecb0"[2](2c9030040ecb2)          # "MT25408 ConnectX Mellanox Technologies" lid 3 4xQDR
[22]    "H-24be05ffffae0c70"[2](24be05ffffae0c72)               # "MT25408 ConnectX Mellanox Technologies" lid 6 4xQDR
[25]    "H-0002c90300346e40"[2](2c90300346e42)          # "MT25408 ConnectX Mellanox Technologies" lid 7 4xQDR
[26]    "H-0002c903003993f0"[2](2c903003993f2)          # "MT25408 ConnectX Mellanox Technologies" lid 10 4xQDR
[27]    "H-0002c90300363800"[2](2c90300363802)          # "MT25408 ConnectX Mellanox Technologies" lid 12 4xQDR
[28]    "H-0002c9030040e440"[2](2c9030040e442)          # "MT25408 ConnectX Mellanox Technologies" lid 14 4xQDR
[29]    "H-0002c903003fb180"[2](2c903003fb182)          # "MT25408 ConnectX Mellanox Technologies" lid 18 4xQDR
[30]    "H-0002c903003f0f70"[2](2c903003f0f72)          # "MT25408 ConnectX Mellanox Technologies" lid 16 4xQDR
[35]    "S-506b4b03005ec3a0"[35]                # "MF0;sw01:MSB7700/U1" lid 2 4xEDR
[36]    "S-506b4b03005ec3a0"[36]                # "MF0;sw01:MSB7700/U1" lid 2 4xEDR

vendid=0x2c9
devid=0xcb20
sysimgguid=0x506b4b03005ec3a0
switchguid=0x506b4b03005ec3a0(506b4b03005ec3a0)
Switch  36 "S-506b4b03005ec3a0"         # "MF0;sw01:MSB7700/U1" enhanced port 0 lid 2 lmc 0
[1]     "H-0002c9030039d940"[1](2c9030039d941)          # "MT25408 ConnectX Mellanox Technologies" lid 49 4xQDR
[2]     "H-0002c903003638a0"[1](2c903003638a1)          # "MT25408 ConnectX Mellanox Technologies" lid 47 4xQDR
[3]     "H-0002c903004214f0"[1](2c903004214f1)          # "MT25408 ConnectX Mellanox Technologies" lid 43 4xQDR
[4]     "H-0002c903003fc5e0"[1](2c903003fc5e1)          # "MT25408 ConnectX Mellanox Technologies" lid 45 4xQDR
[5]     "H-0002c90300427f50"[2](2c90300427f52)          # "MT25408 ConnectX Mellanox Technologies" lid 41 4xQDR
[6]     "H-0002c903003aa5f0"[1](2c903003aa5f1)          # "MT25408 ConnectX Mellanox Technologies" lid 40 4xQDR
[9]     "H-0002c90300feeed0"[1](2c90300feeed1)          # "MT25408 ConnectX Mellanox Technologies" lid 37 4xQDR
[10]    "H-0002c90300363bf0"[1](2c90300363bf1)          # "MT25408 ConnectX Mellanox Technologies" lid 35 4xQDR
[11]    "H-0002c9030030b690"[1](2c9030030b691)          # "MT25408 ConnectX Mellanox Technologies" lid 33 4xQDR
[12]    "H-0002c903003997e0"[1](2c903003997e1)          # "MT25408 ConnectX Mellanox Technologies" lid 51 4xQDR
[13]    "H-24be05ffffce1730"[1](24be05ffffce1731)               # "MT25408 ConnectX Mellanox Technologies" lid 29 4xQDR
[14]    "H-0002c903003a0560"[2](2c903003a0562)          # "MT25408 ConnectX Mellanox Technologies" lid 28 4xQDR
[17]    "H-0002c903003ea9e0"[1](2c903003ea9e1)          # "MT25408 ConnectX Mellanox Technologies" lid 25 4xQDR
[18]    "H-0002c9030039fb00"[1](2c9030039fb01)          # "MT25408 ConnectX Mellanox Technologies" lid 23 4xQDR
[19]    "H-24be05ffffce5680"[1](24be05ffffce5681)               # "MT25408 ConnectX Mellanox Technologies" lid 21 4xQDR
[20]    "H-0002c9030039b950"[1](2c9030039b951)          # "MT25408 ConnectX Mellanox Technologies" lid 19 4xQDR
[21]    "H-0002c9030040ecb0"[1](2c9030040ecb1)          # "MT25408 ConnectX Mellanox Technologies" lid 4 4xQDR
[22]    "H-24be05ffffae0c70"[1](24be05ffffae0c71)               # "MT25408 ConnectX Mellanox Technologies" lid 5 4xQDR
[25]    "H-0002c90300346e40"[1](2c90300346e41)          # "MT25408 ConnectX Mellanox Technologies" lid 8 4xQDR
[26]    "H-0002c903003993f0"[1](2c903003993f1)          # "MT25408 ConnectX Mellanox Technologies" lid 9 4xQDR
[27]    "H-0002c90300363800"[1](2c90300363801)          # "MT25408 ConnectX Mellanox Technologies" lid 11 4xQDR
[28]    "H-0002c9030040e440"[1](2c9030040e441)          # "MT25408 ConnectX Mellanox Technologies" lid 13 4xQDR
[29]    "H-0002c903003fb180"[1](2c903003fb181)          # "MT25408 ConnectX Mellanox Technologies" lid 17 4xQDR
[30]    "H-0002c903003f0f70"[1](2c903003f0f71)          # "MT25408 ConnectX Mellanox Technologies" lid 15 4xQDR
[35]    "S-248a070300e43740"[35]                # "MF0;sw02:MSB7700/U1" lid 1 4xEDR
[36]    "S-248a070300e43740"[36]                # "MF0;sw02:MSB7700/U1" lid 1 4xEDR
```

交换机信息



```bash
vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003f0f73
caguid=0x2c903003f0f70
Ca      2 "H-0002c903003f0f70"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003f0f71)      "S-506b4b03005ec3a0"[30]                # lid 15 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903003f0f72)      "S-248a070300e43740"[30]                # lid 16 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003fb183
caguid=0x2c903003fb180
Ca      2 "H-0002c903003fb180"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003fb181)      "S-506b4b03005ec3a0"[29]                # lid 17 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903003fb182)      "S-248a070300e43740"[29]                # lid 18 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c9030040e443
caguid=0x2c9030040e440
Ca      2 "H-0002c9030040e440"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c9030040e441)      "S-506b4b03005ec3a0"[28]                # lid 13 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c9030040e442)      "S-248a070300e43740"[28]                # lid 14 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c90300363803
caguid=0x2c90300363800
Ca      2 "H-0002c90300363800"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c90300363801)      "S-506b4b03005ec3a0"[27]                # lid 11 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c90300363802)      "S-248a070300e43740"[27]                # lid 12 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003993f3
caguid=0x2c903003993f0
Ca      2 "H-0002c903003993f0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003993f1)      "S-506b4b03005ec3a0"[26]                # lid 9 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903003993f2)      "S-248a070300e43740"[26]                # lid 10 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c90300346e43
caguid=0x2c90300346e40
Ca      2 "H-0002c90300346e40"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c90300346e41)      "S-506b4b03005ec3a0"[25]                # lid 8 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c90300346e42)      "S-248a070300e43740"[25]                # lid 7 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x24be05ffffae0c73
caguid=0x24be05ffffae0c70
Ca      2 "H-24be05ffffae0c70"          # "MT25408 ConnectX Mellanox Technologies"
[1](24be05ffffae0c71)   "S-506b4b03005ec3a0"[22]                # lid 5 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](24be05ffffae0c72)   "S-248a070300e43740"[22]                # lid 6 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c9030040ecb3
caguid=0x2c9030040ecb0
Ca      2 "H-0002c9030040ecb0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c9030040ecb1)      "S-506b4b03005ec3a0"[21]                # lid 4 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c9030040ecb2)      "S-248a070300e43740"[21]                # lid 3 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c9030039b953
caguid=0x2c9030039b950
Ca      2 "H-0002c9030039b950"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c9030039b951)      "S-506b4b03005ec3a0"[20]                # lid 19 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c9030039b952)      "S-248a070300e43740"[20]                # lid 20 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c9030039fb03
caguid=0x2c9030039fb00
Ca      2 "H-0002c9030039fb00"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c9030039fb01)      "S-506b4b03005ec3a0"[18]                # lid 23 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c9030039fb02)      "S-248a070300e43740"[18]                # lid 24 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x24be05ffffce5683
caguid=0x24be05ffffce5680
Ca      2 "H-24be05ffffce5680"          # "MT25408 ConnectX Mellanox Technologies"
[1](24be05ffffce5681)   "S-506b4b03005ec3a0"[19]                # lid 21 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](24be05ffffce5682)   "S-248a070300e43740"[19]                # lid 22 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003ea9e3
caguid=0x2c903003ea9e0
Ca      2 "H-0002c903003ea9e0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003ea9e1)      "S-506b4b03005ec3a0"[17]                # lid 25 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903003ea9e2)      "S-248a070300e43740"[17]                # lid 26 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003a0563
caguid=0x2c903003a0560
Ca      2 "H-0002c903003a0560"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003a0561)      "S-248a070300e43740"[14]                # lid 27 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR
[2](2c903003a0562)      "S-506b4b03005ec3a0"[14]                # lid 28 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x24be05ffffce1733
caguid=0x24be05ffffce1730
Ca      2 "H-24be05ffffce1730"          # "MT25408 ConnectX Mellanox Technologies"
[1](24be05ffffce1731)   "S-506b4b03005ec3a0"[13]                # lid 29 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](24be05ffffce1732)   "S-248a070300e43740"[13]                # lid 30 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003997e3
caguid=0x2c903003997e0
Ca      2 "H-0002c903003997e0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003997e1)      "S-506b4b03005ec3a0"[12]                # lid 51 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903003997e2)      "S-248a070300e43740"[12]                # lid 52 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c9030030b693
caguid=0x2c9030030b690
Ca      2 "H-0002c9030030b690"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c9030030b691)      "S-506b4b03005ec3a0"[11]                # lid 33 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c9030030b692)      "S-248a070300e43740"[11]                # lid 34 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c90300363bf3
caguid=0x2c90300363bf0
Ca      2 "H-0002c90300363bf0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c90300363bf1)      "S-506b4b03005ec3a0"[10]                # lid 35 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c90300363bf2)      "S-248a070300e43740"[10]                # lid 36 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c90300feeed3
caguid=0x2c90300feeed0
Ca      2 "H-0002c90300feeed0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c90300feeed1)      "S-506b4b03005ec3a0"[9]         # lid 37 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c90300feeed2)      "S-248a070300e43740"[9]         # lid 38 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c90300427f53
caguid=0x2c90300427f50
Ca      2 "H-0002c90300427f50"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c90300427f51)      "S-248a070300e43740"[5]         # lid 42 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR
[2](2c90300427f52)      "S-506b4b03005ec3a0"[5]         # lid 41 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003aa5f3
caguid=0x2c903003aa5f0
Ca      2 "H-0002c903003aa5f0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003aa5f1)      "S-506b4b03005ec3a0"[6]         # lid 40 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903003aa5f2)      "S-248a070300e43740"[6]         # lid 39 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903004214f3
caguid=0x2c903004214f0
Ca      2 "H-0002c903004214f0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903004214f1)      "S-506b4b03005ec3a0"[3]         # lid 43 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903004214f2)      "S-248a070300e43740"[3]         # lid 44 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003638a3
caguid=0x2c903003638a0
Ca      2 "H-0002c903003638a0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003638a1)      "S-506b4b03005ec3a0"[2]         # lid 47 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903003638a2)      "S-248a070300e43740"[2]         # lid 48 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c9030039d943
caguid=0x2c9030039d940
Ca      2 "H-0002c9030039d940"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c9030039d941)      "S-506b4b03005ec3a0"[1]         # lid 49 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c9030039d942)      "S-248a070300e43740"[1]         # lid 50 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR

vendid=0x2c9
devid=0x1003
sysimgguid=0x2c903003fc5e3
caguid=0x2c903003fc5e0
Ca      2 "H-0002c903003fc5e0"          # "MT25408 ConnectX Mellanox Technologies"
[1](2c903003fc5e1)      "S-506b4b03005ec3a0"[4]         # lid 45 lmc 0 "MF0;sw01:MSB7700/U1" lid 2 4xQDR
[2](2c903003fc5e2)      "S-248a070300e43740"[4]         # lid 46 lmc 0 "MF0;sw02:MSB7700/U1" lid 1 4xQDR
⬢[root@toolbox ~]#
```

### iperf3

客户端

```bash
⬢[root@toolbox ~]# iperf3 -s
-----------------------------------------------------------
Server listening on 5201 (test #1)
-----------------------------------------------------------
Accepted connection from 192.168.26.2, port 43292
[  5] local 192.168.26.1 port 5201 connected to 192.168.26.2 port 43306
[ ID] Interval           Transfer     Bitrate
[  5]   0.00-1.00   sec  2.48 GBytes  21.3 Gbits/sec
[  5]   1.00-2.00   sec  2.58 GBytes  22.2 Gbits/sec
[  5]   2.00-3.00   sec  2.49 GBytes  21.4 Gbits/sec
[  5]   3.00-4.00   sec  2.59 GBytes  22.3 Gbits/sec
[  5]   4.00-5.00   sec  2.51 GBytes  21.5 Gbits/sec
[  5]   5.00-6.00   sec  2.44 GBytes  20.9 Gbits/sec
[  5]   6.00-7.00   sec  2.42 GBytes  20.8 Gbits/sec
[  5]   7.00-8.00   sec  2.56 GBytes  22.0 Gbits/sec
[  5]   8.00-9.00   sec  2.48 GBytes  21.3 Gbits/sec
[  5]   9.00-10.00  sec  2.52 GBytes  21.6 Gbits/sec
[  5]  10.00-10.00  sec   622 KBytes  20.8 Gbits/sec
- - - - - - - - - - - - - - - - - - - - - - - - -
```

服务端

```bash
⬢[root@toolbox ~]#  iperf3  -c 192.168.26.1
Connecting to host 192.168.26.1, port 5201
[  5] local 192.168.26.6 port 41866 connected to 192.168.26.1 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  5]   0.00-1.00   sec  2.67 GBytes  23.0 Gbits/sec  3946    401 KBytes
[  5]   1.00-2.00   sec  2.64 GBytes  22.6 Gbits/sec  4408    263 KBytes
[  5]   2.00-3.00   sec  2.68 GBytes  23.0 Gbits/sec  4027    428 KBytes
[  5]   3.00-4.00   sec  2.83 GBytes  24.3 Gbits/sec  3662    381 KBytes
[  5]   4.00-5.00   sec  2.86 GBytes  24.6 Gbits/sec  3212    414 KBytes
[  5]   5.00-6.00   sec  2.79 GBytes  24.0 Gbits/sec  4233    372 KBytes
[  5]   6.00-7.00   sec  2.80 GBytes  24.1 Gbits/sec  4195    428 KBytes
[  5]   7.00-8.00   sec  2.61 GBytes  22.4 Gbits/sec  4121    465 KBytes
[  5]   8.00-9.00   sec  2.76 GBytes  23.7 Gbits/sec  4708    519 KBytes
[  5]   9.00-10.00  sec  2.55 GBytes  21.9 Gbits/sec  4095    445 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-10.00  sec  27.2 GBytes  23.4 Gbits/sec  40607             sender
[  5]   0.00-10.00  sec  27.2 GBytes  23.3 Gbits/sec                  receiver

iperf Done.
⬢[root@toolbox ~]#
```
性能测试的输出结果，其中包含了有关测试过程和结果的详细信息。下面是每个字段的含义：

+ ID： 测试流的唯一标识符。
+ Interval： 测试的时间间隔，以秒为单位。
+ Transfer： 在测试过程中传输的总字节数。
+ Bitrate： 传输速率，以比特每秒（bps）为单位。
+ Retr： 在测试期间发生的重传次数。
+ Sender： 表示此行所列出的结果来自iperf3客户端。
+ Receiver： 表示此行所列出的结果来自iperf3服务器。

从该输出结果可以看出，这次iperf3测试使用TCP协议，在10秒的测试时间内，数据传输量为27.2 GB，传输速率为 23.4Gbps。重传次数为40607次。





### iperf3 如何测试  InfiniBand  组网


iperf3可以用于测试InfiniBand组网的性能和带宽。为此，您需要在InfiniBand网络中执行以下步骤：

确认InfiniBand适配器已启用IPoIB功能。IPoIB是一种在InfiniBand网络上传输IP数据的方法，它允许使用标准的TCP/IP协议栈和网络应用程序。在启用IPoIB之前，请确保已正确配置InfiniBand适配器和子网管理器（SM）。

```bash
[root@worker108 ~]# lsmod | grep ib_ipoib
ib_ipoib              172032  0
ib_cm                 139264  2 rdma_cm,ib_ipoib
ib_core               442368  10 rdma_cm,ib_ipoib,rpcrdma,mlx4_ib,iw_cm,ib_mthca,ib_umad,rdma_ucm,ib_uverbs,ib_cm
[root@worker108 ~]#
```

在至少两个节点上运行iperf3。其中一个节点充当iperf3服务器，而其他节点则充当客户端。

在服务器节点上启动iperf3服务器。在命令行上执行以下命令：
```bash
iperf3 -s
```

此命令将在默认的TCP端口5201上启动iperf3服务器，并等待客户端连接请求。

在客户端节点上启动iperf3客户端。在命令行上执行以下命令：
```bash
iperf3 -c <server_ip_address> -b <bandwidth>
```

其中，"<server_ip_address>"是iperf3服务器的IP地址，"<bandwidth>"是您想要测试的带宽速率。例如，如果您想测试25Gbps的带宽，则可以指定"-b 25g"。

进行iperf3测试。一旦客户端与服务器建立连接，iperf3测试即可开始。在测试过程中，可以使用各种选项和参数来调整测试过程和输出结果。例如，您可以指定测试时间、并发连接数、数据块大小等。

查看测试结果。测试完成后，iperf3客户端将显示详细的测试结果，包括带宽、吞吐量、延迟和数据包丢失率等信息。可以使用这些信息来评估网络性能和诊断问题。

需要注意的是，在InfiniBand网络中使用iperf3时，建议使用RDMA选项以最大限度地利用InfiniBand网络的性能和特性。此外，还可以使用其他工具和方法来测试InfiniBand网络，例如perfquery和ib_read_bw等命令行工具。
```





## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
