---
title: 生产级运维工具 ansible 安装
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-04-23 01:52:20/生产级运维工具 ansible 安装.html'
mathJax: false
date: 2023-04-23 09:52:20
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

机器信息查看

```bash
okd-operator@okd-operator:~/ansible_okd-operator$ hostnamectl
   Static hostname: okd-operator
         Icon name: computer-vm
           Chassis: vm
        Machine ID: fdeb7a53e370497abd8acb617cb0baca
           Boot ID: 79f1c0b03bf24754a10640c4c9be7a28
    Virtualization: vmware
  Operating System: Debian GNU/Linux 11 (bullseye)
            Kernel: Linux 5.10.0-18-amd64
      Architecture: x86-64
okd-operator@okd-operator:~/ansible_okd-operator$
```


控制节点 ansible 安装
```bash
sudo apt install  ansible
```







## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
