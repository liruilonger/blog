---
title: 《Flask Web  开发入门》读书笔记
tags:
  - Python
  - Flask
categories:
  - Flask
toc: true
recommend: 1
keywords: python
uniqueId: '2021-09-03 01:52:58/《Flask Web  开发入门》读书笔记'
mathJax: false
date: 2021-09-03 09:52:58
thumbnail:
---
**<font color="009688">一辈子很长，就找个有趣的在在一起——王小波《三十而立》</font>**
<!-- more -->
## 写在前面
***
+ 学`docker `编排，有一个用`Flask`框架的`Demo`，感觉挺方便，所以学习下
+ 基于`《Flask Web开发实战：入门、进阶与原理解析》`做的读书笔记
+ 个人还是比较喜欢看书，看书的话节奏可以自己把握，内容可以全部呈现，选择适合自己的学习，更重要的原因，书里的知识面更广，有利用知识体系的构建。
+ 当然视频也不错，内容比较精炼，但是好坏参差，一般要选大一点培训机构的视频比较好。所以一般先看书，然后看视频加深记忆。
+ 还在更新中 

**<font color="009688">一辈子很长，就找个有趣的在在一起——王小波《三十而立》</font>**

***

# <font color=royalblue>第一部分</font>
## <font color=royalblue>第一章 初识 FLask</font>
```py
from flask import Flask

app=Flask(__name__)


#为视图绑定多个URL
@app.route("/")
@app.route("/index")
def hello():
    return "<h1>liurilong</h1>"

## 动态URL
@app.route("/user/<name>")
def printName(name):
    return name



if __name__ == "__main__":
    app.run(host='127.0.0.1', port=8085,debug=True)

```

环境准备
```bash
#虚拟环境
mkvirtualenv #创建虚拟环境
rmvirtualenv #删除虚拟环境
workon #进入虚拟环境、查看所有虚拟环境
deactivate #退出虚拟环境

#pip
pip install #安装依赖包
pip uninstall #卸载依赖包
pip list #查看已安装的依赖包
pifreeze #冻结当前环境的依赖包 
```

## <font color=orange>第二章 Flask 与 HTTP</font>

### 2.1 <font color=amber>请求响应循环</font>
web服务器接收请求,`通过WSGI将HTTP格式的请求数据转换成我们的Flask程序能够使用的Python数据。`

在程序中, Flask根据请求的URL执行对应的视图函数,`获取返回值生成响应。响应依次经过wsGI转换生成HTTP响应`,再经由Web服务器传递,

最终被发出请求的客户端接收。浏览器渲染响应中包含的HTML和css代码,并执行JavaScript代码,最终把解析后的页面呈现在用户浏览器的窗口中。
### 2.2 <font color=red>HTTP请求</font>
#### 2.2.1 <font color=royalblue>请求报文</font>
|请求报文|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/6d490cb882d14ee5bca57144928fea85.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|
#### 2.2.2 <font color=royalblue>Request对象</font>
请求解析和响应封装实际上大部分是由`Werkzeug`完成的, Flask子类化`Werkzeug`的请求(Request)和响应( Response)对象并添加了和程序相关的特定功能。

|使用request的属性获取请求URL&&request象常用的属性和方法|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/0bbb4b52ecf64fafbfd2d6296331b62d.png)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/27636405e66c4e05a95af144f0cb16e4.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|

```py
@app.route("/param")
def param():
    name = request.args.get('name','Flask')
    return name

```
>`Werkzeug的MutliDict类是字典的子类`,它主要实现了`同一个键对应多个值的情况`。比如一个文件上传字段可能会接收多个文件。这时就可以通过`getist()`方法来获取文件对象列表。而`ImmutableMultiDict`类继承了`MutliDict`类,但其值不可更改。


#### 2.2.3 <font color=brown>在Flask中处理请求</font>
路由匹配为了便于将请求分发到对应的视图函数,`程序实例中存储了一个路由表(app.url_map)`,其中定义了`URL规则和视图函数的映射关系`。

当请求发来后, Flask会根据请求报文中的URL(path部分)来尝试与这个表中的所有URL规则进行匹配,调用匹配成功的视图函数。如果没有找到匹配的URL规则,说明程序中没有处理这个URL的视图函数, Flask会自动返回404错误响应(Not Found,表示资源未找到)。


```py
print(app.url_map)
####
Map([<Rule '/index' (HEAD, GET, OPTIONS) -> hello>,
 <Rule '/param' (HEAD, GET, OPTIONS) -> param>,
 <Rule '/' (HEAD, GET, OPTIONS) -> hello>,
 <Rule '/static/<filename>' (HEAD, GET, OPTIONS) -> static>,
 <Rule '/user/<name>' (HEAD, GET, OPTIONS) -> printName>])
```
##### <font color=amber>路由匹配：</font>
每一个路由除了包含URL规则外,还设置了监听的HTTP方法。`GET`是最常用的HTTP方法,所以视图函数默认监听的方法类型就是`GET, HEAD`,`OPTION`方法的请求由Flask处理`,而像`DELETE,PUT`等方法一般不会在`程序中实现`,在后面我们构建`WebAPI`时才会用到这些方法。我们可以在`app.route()装饰器中使用methods参数传入一个包含监听的HTTP方法的可迭代对象`,下面的视图函数同时监听GET请求和POST请求:
```py
@app.route("/param",methods=['GET','POST'])

Map([<Rule '/index' (GET, HEAD, OPTIONS) -> hello>,
 <Rule '/param' (GET, HEAD, POST, OPTIONS) -> param>,
 <Rule '/' (GET, HEAD, OPTIONS) -> hello>,
 <Rule '/static/<filename>' (GET, HEAD, OPTIONS) -> static>,
 <Rule '/user/<name>' (GET, HEAD, OPTIONS) -> printName>])
```
##### <font color=green>设置监听的HTTp方法</font>
```py

## 设置监听的 http 方法
@app.route("/param",methods=['GET','POST'])
def param():
    name = request.args.get('name','Flask')
    print(app.url_map)
    return name
```
##### <font color=yellowgreen>URL处理</font>
|-|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/312209d76b4d43958e61bdd1d6ff123b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

```py
## URL解析
@app.route('/goback/<int:year>')
def goback(year):
    return '<p>Weloc... to %d</p>' % (2018- year)
```

#### 2.2.4 <font color=chocolate>请求钩子</font>
|-|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/c25ced3596b64e95beefb57f4bae3b0e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

有时我们需要对请求进行`预处理( preprocessing)`和`后处理(postprocessing)`,这时可以使用`Flask`提供的一些请求钩子( Hook),它们可以用来注册在`请求处理`的`不同阶段执行`的处理函数(或称为回调函数,即`Callback`)。这些请求钩子使用装饰器实现.
```py
@app.before_request
def do_something():
    pass #这里的代码会在每个请求前运行
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/10daf6d1cc594e048f32c7faad92f766.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

after request钩子和after-this request钩子必须接收一个响应类对象作为参数,并且返回同一个或更新后的响应对象。 

### 2.3 <font color=camel>HTTP响应</font>
|-|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/e121721c371240629ecb655f6a754360.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

#### 2.3.1<font color=blue>在Flask中生成响应报文</font>
```py
# 指定状态码
@app.route('/hello')
def hell():
    return 'hello',201
# 指定状态码，响应首部字段
@app.route('/hello')
def hello():
    return '',302,{'Location','http://www.liruilong.com'}
```
+ 重定向
```py
from flask import Flask,redirect
# 重定向
@app.route('/hello')
def hello():
    return redirect('http://www.liruilong.com');
# 重定向到视图    
def hello_():
    return redirect(url_for('hello'))    
```
|-|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/81467893011f411d8592a6a8ab322f53.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_19,color_FFFFFF,t_70,g_se,x_16)|



#### 2.3.1<font color=camel>在Flask中生成响应</font>

响应在Flask中使用Response对象表示,响应报文中的大部分内容由服务器处理,大多数情况下,只负责返回主体内容。

Flask会先判断是否可以找到与请求URL相匹配的路由

如果没有则返回404响应。如果找到,`则调用对应的视图函数,视图函数的返回值构成了响应报文的主体内容,正确返回时状态码默认为200`, Flask会调用`make_response()`方法将`视图函数返回值转换为响应对象`。

视图函数可以返回最多由`三个元素`组成的`元组`:
+ 响应主体
+ 状态码
+ 首部字段(首部字段可以为字典,或是两元素元组组成的列表)。

#### 2.3.2<font color=purple>响应格式</font>
#### 2.3.3<font color=orange>来-块cookie</font>
#### 2.3.4 <font color=camel>session:安全的Cookie</font>

### 3.1 <font color=orange>响应报文</font>
#### 2.3.1  <font color=royalblue>在Flask中生成响应.</font>
#### 2.3.2 <font color=chocolate>响应格式</font>
#### 2.3.3 <font color=red>来一块Cookie</font>
#### 2.3.4  <font color=royalblue>session:安全的Cookie </font>
### 2.4 <font color=chocolate>Flask上下文</font>
#### 2.4.1 <font color=yellowgreen>上下文全局变量</font>
#### 2.4.2 <font color=blue>激活上下文.</font>
#### 2.4.3 <font color=purple>上下文钩子</font>
### 2.5 <font color=blue>HTTP进阶实践</font>
#### 2.5.1 <font color=yellowgreen>重定向回上一个页面</font>
#### 2.5.2 <font color=camel>使用AJAX技术发送异步请求</font>
#### 2.5.3 <font color=purple>HTTP服务器端推送</font>
#### 2.5.4 <font color=red>Web安全防范</font>

## <font color=tomato>第3章模板</font>
### 3.1 <font color=red>模板基本用法</font>
#### 3.1.1 <font color=red>创建模板</font>
#### 3.1.2 <font color=orange>模板语法</font>
#### 3.1.3 <font color=amber>渲染模板</font>
### 3.2 <font color=amber>模板辅助工具</font>
#### 3.2.1 <font color=seagreen>上下文</font>.
#### 3.2.2 <font color=red>全局对象</font>
#### 3.2.3 <font color=royalblue>过滤器</font>
#### 3.2.4 <font color=chocolate>测试器</font>
### 3.3 <font color=camel>模板结构组织</font>.
#### 3.3.1 <font color=plum>局部模板</font>
#### 3.3.2 <font color=yellowgreen>宏</font>
#### 3.3.3 <font color=blue>模板继承</font>
### 3.4  <font color=royalblue>模板进阶实践</font>.
#### 3.4.1 <font color=amber>空白控制</font>
#### 3.4.2 <font color=tomato>加载静态文件</font>
#### 3.4.3 <font color=chocolate>消息闪现</font>
#### 3.4.4 <font color=amber>自定义错误页面</font>
#### 3.4.5 <font color=camel>JavaScript和CSS中的Jinja2</font>
 

