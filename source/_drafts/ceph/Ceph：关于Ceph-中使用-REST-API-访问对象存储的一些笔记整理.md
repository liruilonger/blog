---
title: Ceph：关于Ceph 中使用 REST API 访问对象存储的一些笔记整理
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-04-16 09:20:36/Ceph：关于Ceph 中使用 REST API 访问对象存储的一些笔记整理.html'
mathJax: false
date: 2023-04-16 17:20:36
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 使用 REST API 访问对象存储
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


# 使用 REST API 访问对象存储

## 使用Amazon S3 API提供对象存储

### RADOS网关中的 Amazon S3 API

通过`Amazon S3`接口，开发人员可以通过兼容 Amazon S3 接口管理对象存储资源。通过S3接口实现的应用可以与除RADOS网关之外的其他兼容S3的对象存储服务互操作，并将存储从其他位置迁移到Ceph存储集群。

在混合云环境中，可以配置应用程序以使用不同的身份验证密钥、区域和供应商服务，使用相同的API无缝地混合私有企业和公共云资源和存储位置。

`Amazon S3`接口定义了将`对象存储为桶`的`名称空间`。应用通过S3接口访问和管理对象和桶时，需要使用`RADOS Gateway用户进行认证`。每个用户都有一个识别用户的`访问密钥`(认证)和一个验证用户身份的`密钥`(鉴权)

在使用`Amazon S3 API`时，需要考虑`对象和元数据`的大小限制:

1. 对象大小的最小值为`0B`，最大值为`5tb`
2. 单个上传操作的最大容量为`5GB`
3. 使用`multipart`上传功能上传大于`100MB`的对象。
4. 单个`HTTP请求`的元数据最大长度为`16000字节`

### 创建Amazon S3 API的用户

首先创建所需的 `RADOS网关用户`，然后使用它们用网关对` Amazon S3客户端` 进行身份验证。使用 `radosgw-adrnin user create`命令创建RADOS网关用户,在创建 `RADOS Gateway` 用户时

+ 需要同时使用`--uid`和`--display-name`选项，并指定唯一的帐户名和用户友好的显示名。
+ 使用`--access-key`和`--secret`选项为RADOS用户指定自定义AWS帐户和密钥

```bash
[ceph: root@node /]# radosgw-admin user create \
	--uid=testuser \
	--display-name="Test User" \
	--email=test@example.com \ 
	--access-key=12345 \
	--secret=67898 
. . . output omitted ... 
"keys": 
{ 
"user": "testuser", 
"access_key": "12345",
"secret_key": "67890" 
}
```

如果不指定`access key`和`secret key`，则`radosgw-admin`命令会自动生成并显示在输出信息中

```bash
[ceph: root@node /]# radosgw-admin user create \
	--uid=s3user \ 
	--display-name="Amazon S3 API user" 
. . . output omitted ... 
"keys": [ 
{ 
"user" : "s3user", 
"access_key": "8PI209ARWNGJI99K8TOS", 
"secret_key": "brKaQdhyR022znWVVdDLuAEafRjbrAorr0GoXNl" 
}
```

当`radosgw-admin`命令自动生成访问密钥和密钥时，任一密钥都可能包含一个`JSON转义字符(\)`。客户端可能无法正确处理此字符。可以重新生成或手动指定键来避免此问题

### 管理Ceph对象网关用户

#### 用户的密钥管理

要`重新生成`一个现有用户的密钥，使用带有-gen-secret选项的radosgw-admin key create命令

```bash
[ceph: root@node /]# radosgw-admin key create \
	--uid=s3user \ 
	--access-key="8PI2D9ARWNGJI99K8TOS" \
	--gen-secret 
... output omitted ... 
"keys": 
{ 
"user" : "s3user", 
"access_key": "8PI209ARWNGJI99K8TOS", 
"secret_key": "MFVxrGNMBjK007JscLFbEyrEmJFnLl43PHSswpLC" 
} 
```

若要为`现有用户添加`访问键，请使用 `--gen-access-key` 选项。创建`额外的键`可以方便地向需要`不同或唯一键`的`多个应用程序授予相同的用户访问权`

```bash
[ceph: root@node /]# radosgw-admin key create \
	--uid=s3user \
	--gen-access-key 
```

要从用户那里`移除`访问密钥和相关的密钥，可以使用带有`--access-key`选项的`radosgw-admin key rm`命令。这对于删除单个应用程序访问而不影响对其他键的访问非常有用

```bash
[ceph: root@node /]# radosgw-admin key rm \
	--uid=s3user \
	--access-key=8PI209ARWNGJI99K8TOS 
```

#### 用户管理

使用`radosgw-admin user suspend`和`radosgw-admin user enable`命令暂时`关闭和启用`RADOS Gateway用户。当挂起时，用户的子用户也会挂起，并且无法与RADOS网关服务交互。

可以修改用户的`邮箱、显示名、按键和访问控制级别`等信息。访问控制级别包括:`读、写、读写和完全(读写级别和访问控制管理能力)`。

```bash
[ceph: root@node /)# radosgw-admin user modify \
	--uid=johndoe \
	--access=full
```

要删除用户并删除他们的`对象和存储桶`，请使用`--purge-data`选项

```bash
[ceph: root@node /]# radosgw-admin user rm \
	--uid=s3user \
	--purge-data
```

#### 配额管理 

通过设置配额来限制用户或桶使用的存储空间。请先设置配额参数，再启用配额。如果需要禁用配额，请将`quota`参数设置为负值

`桶配额`适用于指定`UUID所拥有的所有桶`，与访问或向这些桶进行上传的用户无关

本例中为“app1”用户设置的最大配额为1024个对象。启用用户配额

```bash
[ceph: root@node /]# radosgw-admin quota set \
	--quota-scope=user \
	--uid=app1 \
	--max-objects=1024 
[ceph: root@node /]# radosgw-admin quota enable \
	--quota-scope=user \
	--uid=app1 
```

同样，通过将 `--quota-scope` 选项设置为桶，可以对桶使用配额。本例中设置的`日志历史桶大小不超过1024字节`

```bash
[ceph: root@node /]# radosgw-admin quota set \
	--quota-scope=bucket \
	--uid=loghistory \
	--max-objects=1024B 
[ceph: root@node /)# radosgw-admin quota enable \
	--quota-scope=bucket \
	--uid=loghistory
```

`全局配额`影响集群中的`所有桶`

```bash
[ceph: root@node /)# radosgw-admin global quota set \
	--quota-scope bucket \
	--max-objects 2048 
[ceph: root@node /)# radosgw-admin global quota enable \
	--quota-scope bucket
```

如果需要在 `zone和period` 配置中实现，可以使用`radosgw-admin period update - -commit`命令提交修改。或者，重新`启动RGW`实例来实现`配额`


#### 检索对象存储信息

可以使用以下命令检索用户信息和统计信息

|       动作       | 命令                                     |
| :--------------: | ---------------------------------------- |
|   获取用户信息   | [radosgw-admin user info - -uid=uid]()   |
| 检索用户统计信息 | [radosgw-admin user stats - - uid=uid]() |

存储管理员通过监控带宽使用情况来确定存储资源的使用情况或用户带宽使用情况。监视还可以帮助查找不活动的应用程序或不适当的用户配额。

使用`radosgw-admin user stats`和`radosgw-admin user info`命令查看用户信息和统计信息

```bash
[ceph: root@node /]# radosgw-admin user info --uid=uid 
[ceph: root@node /]# radosgw-admin user stats --uid=uid 
```

使用`radosgw-admin usage show`命令显示某用户在指定日期的使用统计信息

```bash
[ceph: root@node /)# radosgw-admin usage show \
	--uid=uid \
	--start-date=start \
	--end-date=end
```

使用`radosgw-admin usage show`命令查看所有用户的统计信息。使用这些总体统计信息可以帮助理解对象存储模式，并为扩展RADOS网关服务规划新实例的部署

```bash
[ceph: root@node /]# radosgw-admin usage show \
	--show-log-entries=false
```

### 使用RADOS网关访问S3对象

`Amazon S3 API`支持`多种桶URL格式`，包括
+ `http://server.example.com/bucket`
+ `http://bucket.server.example.com/` 

客户端，有些客户端，例如 `s3cmd` 命令，只支持`第二种URL格式`。默认情况下，`RADOS网关`不启用该格式。要启用第二种URL格式，需要设置`rgw_dns`名称

参数设置DNS后缀

```bash
[ceph: root@node /]# ceph config set client.rgw rgw_dns_name dns_suffix
```

其中`dns_suffix`是用于创建`bucket`名称的完全限定域名

除了配置`rgw_dns_name`之外，还必须为该域配置一个指向`RADOS`网关IP地址的通配符DNS记录。不同的DNS服务器，实现通配符DNS表项的语法也不同

#### 使用Amazon S3 API客户端

来自`awscli`安装包的命令通过使用S3 API支持桶和对象管理。可以使用`aws mb`命令创建桶。这个示例命令创建名为`demobucket`的桶

```bash
[ceph: root@node /]# aws s3 mb s3://demobucket
```

使用`aws cp`命令上传对象到桶中。这个示例命令使用本地文件`/tmp/demoobject`上传一个名为`demoobject`的对象到demobucket桶

```bash
[ceph: root@node /]# aws \
	--acl=public-read-write \
	s3 cp /tmp/demoobject s3://demobucket/demoobject 
```

`radosgw-admin` 命令支持对桶的操作，如`radosgw-admin`桶列表和`radosgw-admin`桶 rm 命令

S3有多种公共客户端，如`awscli、cloudberry、cyberduck和curl`，这些客户端提供了对对象存储的访问，支持S3 API 

#### S3桶版本、生命周期和策略

`S3桶版本`支持在`一个桶中存储一个对象的多个版本`。RADOS Gateway支持版本桶，为上传到桶中的对象添加`版本标识符`。桶的所有者将桶配置为`版本桶`

通过使用为一组桶对象定义的规则，RADOS Gateway还支持`S3 API对象过期`。每个规则都有一个`前缀`，用于选择对象，以及对象不可用的天数

`RADOS网关`只支持应用于桶的`Amazon S3 API策略语言的子集`。用户、组或角色不支持策略。`桶策略`通过标准的S3操作进行管理，而不是使用`radosgw-admin`命令

`S3策略`使用JSON格式定义以下元素:

1. `Resource键`定义了策略修改的权限。该策略使用与资源关联的Amazon Resource Name (ARN)来识别它
2. `Actions键`定义资源允许或拒绝的操作。每个资源都有一组可用的操作
3. `Effect键`指示策略是否允许或拒绝先前为资源定义的操作。缺省情况下，策略拒绝对某个资源的访问
4. `Principal键`定义策略允许或拒绝访问资源的用户

```json
{ 
  "Version": "2021-03-10", 
  "Statement ": [ 
    { 
      "Effect": "Allow", 
      "Principal": { 
        "AWS" : ["arn:aws:iarm::testaccount:user/testuser"] 
      }, 
      "Action" : "s3:ListBucket", 
      "Resource": [ 
      "arn:aws:s3:::testbucket"
      ]
     }
    ]
}
```

#### 支持S3 MFA删除

`S3 MFA (Multi-Factor Authentication)` 是 `Amazon S3` 中的一种安全措施，可以为您的 S3 存储桶启用 MFA 删除保护。启用 MFA 删除保护后，您必须提供` MFA `令牌才能删除存储桶中的对象或者删除整个存储桶。

`RADOS网关`服务支持使用`Time-based、One-time Password (TOTP)`密码作为鉴权因素删除S3 MFA。该特性增加了安全性，防止不适当的和未经授权的数据删除。

除了标准的S3鉴权外，还可以配置桶要求`一次性TOTP令牌`来删除数据。删除对象时，桶的所有者必须在HTTPS协议中包含包含认证设备序列号和认证码的请求头，才能永久删除对象版本或改变桶的版本状态。没有报头，请求将失败

#### 使用REST API创建新的IAM策略和角色

`IAM(身份和访问管理)`角色的REST APls和用户策略现在可以在与`S3 API`相同的命名空间中使用，并且可以在`Ceph对象网关中使用与S3 AP相同的端点访问`。该特性允许最终用户通过使用REST apl创建新的1AM策略和角色

#### Ceph对象网关对Amazon S3资源的支持

`AWS提供安全令牌服务(STS)`，以允许与现有的OpenlD连接进行安全联合;符合`OAuth 2.0`的身份服务，如`Keycloak`。`STS`是一个独立的REST服务，它为应用程序或用户在对身份提供者进行身份验证后访问S3端点提供临时令牌。

以前，没有永久Amazon Web服务(AWS)凭证的用户无法通过Ceph对象网关访问S3资源

`Ceph Object Gateway`实现了`STS api`的一个子集，它为身份和访问管理提供临时凭证。这些临时凭证可用于进行后续的S3调用，这些调用将由RGW中的`STS`引擎进行身份验证。可以通过作为参数传递给`STS api`的`IAM策略`进一步限制临时凭证的权限，`Ceph Object Gateway`支持`STS sumeRoleWithWebIdentity
`
## 通过Swift接口提供对象存储

### 在RADOS网关中的OpenStack Swift支持

通过`OpenStack Swift`接口，开发人员可以通过Swift兼容接口管理对象存储资源。使用S3 API实现的应用程序可以与除RADOS网关之外的其他兼容swift的对象存储服务互操作，并将存储从其他位置迁移到Ceph存储集群。


在混合云环境下，可以配置自己的应用，通过相同的API，将私有企业OpenStack或独立的Swift资源与公有云OpenStack资源和存储位置无缝混合。OpenStack Swift API是Amazon S3 API的替代方案通过RADOS网关访问存储在 Ceph 存储集群中的对象。`OpenStack Swift和Amazon S3 API`之间有重要的区别

`OpenStack Swift`指的是将对象存储为容器的命名空间

`OpenStack Swift API`的用户模型与`Amazon S3 API`不同。使用`OpenStack Swift`接口使用`RADOS Gateway`进行认证时，需要为自己的`RADOS Gateway`用户配置子用户

### 为OpenStack Swift创建Subuser

 Amazon S3 API授权和身份验证模型采用单层设计。一个用户帐户可能有多个访问密钥和秘密，用户可以使用这些密钥和秘密来提供不同类型的访问OpenStack Swift API但是，它采用多层设计，用于容纳租户和分配的用户。Swift租户拥有服务使用的存储空间及其容器。快速用户分配给服务，并且对租户拥有的存储具有不同级别的访问权限

适应OpenStack Swift API在认证和授权模型中，RADOS网关引入了子用户的概念。这个模型允许Swift API将租户作为RADOS Gateway用户和Swift API用户作为RADOS Gateway的子用户处理，Swift API tenant:user 映射到RADOS网关认证系统为user:subuser，为每个Swift用户创建一个子用户，它与一个RADOS网关用户和一个接入密钥相关联

使用radosgw-admin subuser create命令创建子用户，如下所示:

```bash
[ceph: root@node /]# radosgw-admin subuser create \
	--uid=username \
	--subuser=username:swift \
	--access=full
```

--access选项设置用户权限(read、write、read/write、full)， --uid指定现有关联的RADOS Gateway用户。使用命令radosgw- admin key create

使用--key-type=swift选项创建与子子用户关联的swift身份验证密钥

```bash
[ceph: root@node /]# radosgw-admin key create \
	--subuser=username:swift \
	--key-type=swift
```

当Swift客户端与RADOS网关通信时，后者既是数据服务器，也是Swift认证守护进程(使用/auth URL路径)。RADOS Gateway同时支持内部Swift(1.0版本)和OpenStack Keystone(2.0版本)的认证

使用- K指定的密钥是用Swift密钥创建的密钥

确保命令行参数不被任何操作系统环境变量覆盖或影响。如果使用的是Auth 1.0版本，那么使用ST_ Auth、ST_ USER和ST_ KEY环境变量。如果使用Auth 2.0版本，那么使用OS_ Auth_URL, OS_ USERNAME, OS_ PASSWORD，OS_ TENANT_NAME、OS_ TENANT_ID环境变量

在Swift API中，容器是对象的集合。Swift API中的对象是存储在Swift中的二进制大对象(blob)。

使用Swift API验证RADOS Gateway可访问性，使用Swift post命令创建容器

```bash
[root@node -]$ swift \
	-A http://host/auth \
	-u username:swift \
	-K secret \
	post container-name
```

上传文件到容器，使用swift upload命令

```bash
[root@node -]$ swift \
	-A http://host/auth \
	-U username:swift \
	-K secret \
	upload container-name file-name
```

如果使用绝对路径来定义文件位置，则对象的名称包含文件的路径，包括斜杠/。例如，下面的命令将/etc/hosts文件上传至services桶

```bash
[root@node -]$ swift \
	-A http://host/auth/ \
	-U user:swift \
	-K secret \
	upload services /etc/hosts
```

在本例中，上传的对象名称为etc/hosts。可以使用- -object-name选项定义对象名称

使用download命令下载文件

```bash
[root@node -]$ swift \
	-A http://host/auth \
	-U username:swift \
	-K secret \
	download container-name object-name 
```

### 管理Ceph对象网关子用户

使用radosgw-admin subuser Modify命令修改用户的访问级别。访问级别将用户权限设置为读、写、读/写和完全

```bash
[root@node -]$ radosgw-admin subuser modify \
	--subuser=uid:_subuserid_ \
	--access=access-level
```

使用radosgw-admin subuser rm命令移除用户。--purge-data选项清除与子用户相关的所有数据，而- -purge- keys选项清除所有子用户key

```bash
[root@node -]$ radosgw-admin subuser rm \
	--subuser=uid:_subuserid_ \
	[--purge-data] \
	[--purge-keys] 
```

可以通过radosgw-admin key命令对子密钥进行管理。本例创建子用户key

```bash
[root@node -]$ radosgw-admin key create \
	--subuser=uid:_subuserid_ \
	--key-type=swift \
	[--access-key=access-key] \
	[--secret-key=secret-key] 
```

key-type选项只允许swift或s3的值。如果需要手动指定S3的访问密钥，请使用--access-key;如果需要手动指定S3或Swift的密钥，请使用--secret-key。如果没有指定访问密钥和秘密密钥，radosgw-admin命令会自动生成它们并显示在输出中，或者，使用--gen-access-key选项只生成一个随机访问密钥，或者使用--gen-secret选项只生成一个随机密钥

可以使用radosgw-admin key rm命令删除子密钥

```bash
[root@node -]$ radosgw-admin key rm \
	--subuser=uid:subuserid
```

### Swift容器对象版本控制和过期

Swift API支持容器的对象版本控制，提供了在容器中保持一个对象的多个版本的能力。对象版本控制可以避免意外的对象覆盖和删除，并存档以前的对象版本。Swift API只有在对象内容发生变化时才会在版本化容器中创建一个新的对象版本。

要在容器上启用版本控制，请将容器标志的值设置为存储版本的容器的名称。在创建新容器或更新现有容器的元数据时设置该标志

对于每个要进行版本控制的容器，应该使用不同的存档容器。不建议在存档容器上启用版本控制

Swift API支持两个版本标记的头键，X-History-Location或X- versions - Location，它们决定了Swift API处理对象DELETE操作的方式

设置了X-History- Location标志后，在删除容器内的对象后会收到404 Not Found错误。Swift将对象复制到存档容器中，并在版本化容器中删除原始副本。可以从归档容器中恢复对象

通过设置X-Versions-Location标志，Swift可以在版本化容器中移除当前对象的版本。然后，Swift将归档容器中最近的对象版本复制到版本化容器中，并从归档容器中删除最近的对象版本

要从设置了X-Versions- Location标志的版本化容器中完全删除一个对象，必须在存档容器中有多少个可用的对象版本就删除多少次

在一个OpenStack Swift容器中，只能同时设置其中一个标志。如果容器的元数据包含这两个标志，则发出400 Bad Request错误

RADOS Gateway支持Swift API对象版本特性。要在RADOS网关中激活该特性，在/etc/ceph/ceph.conf配置文件中[client .radosgw.radosgw-name]中设置rgw_swift versioning_enabled为true

在使用Swift API添加对象时，RADOS网关还支持使用X-Delete-AT和X-Delete-After header。在报头指定的时间，RADOS网关停止服务该对象，并在不久后删除它

### 在Swift中支持多租户

OpenStack Swift API支持租户隔离桶和用户。Swift API将用户创建的每个新桶与租户关联。该特性允许在不同的租户上使用相同的桶名，因为租户可以隔离资源。为了向后兼容，对于没有关联租户的容器，Swift API使用一个通用的、无名称的租户。

在“RADOS Gateway”中使用radosgw-admin命令配置Swift API租户。该命令需要一个租户创建使用- -tenant选项提供的用户。

```bash
[root@node -]$ radosgw-admin user create \
	--tenant testtenant \
	--uid testuser \
	--display-name "Swift User" \
	--subuser testswift:testuser \
	--key-type swift \
	--access full
```

任何对子用户的进一步引用都必须包括租户

```bash
[root@node -)$ radosgw-admin \
--subuser 'testtenant$testswift:testuser' \
--key-type swift \
--secret redhat 
```






## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
