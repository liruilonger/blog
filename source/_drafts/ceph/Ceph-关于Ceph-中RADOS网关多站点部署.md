---
title: 'Ceph:关于Ceph 中RADOS网关多站点部署'
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-06-14 06:43:51/"Ceph:关于Ceph 中RADOS网关多站点部署".html'
mathJax: false
date: 2023-06-14 14:43:51
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***



## 配置多站点对象存储部署

### RADOS网关多站点部署

`Ceph RADOS`网关支持在一个`全局命名空间内的多站点部署`，这允许`RADOS网关在多个 Ceph 存储集群之间自动复制对象数据`。一个常见的支持用例是活动/活动复制用于灾难恢复的地理上独立的集群

最新的多站点配置简化了`故障转移和故障恢复过程`，支持集群之间的主动/主动复制配置，并合并了一些新特性，比如更简单的配置和对名称空间的支持

![在这里插入图片描述](https://img-blog.csdnimg.cn/ebd88d1037584e768442b9877685bd20.png)


#### 多站点组件

下面列出了多站点组件和定义

**zone**

`zone` 由自己的 Ceph 存储集群支持。每个区域都有一个或多个与之相关联的RADOS网关

**zone group**

`zone group`是一个或多个zone的集合。存储在zone组中的一个zone中的数据会复制到该zone组中的所有其他zone。每个区域组中的一个区域被指定为该组的主区域。zone组中的其他zone均为二级zone

**realm**

`realm` 表示`多站点复制空间中所有对象和存储桶的全局命名空间`。

一个 `realm` 包含一个或多个专区组，每个专区组包含一个或多个专区。域中的一个 zone 组被指定为`主 zone 组`，其他的`zone`组被指定为`从zone组`。

环境中的所有`RADOS网关`都从主区域组和主区域的RADOS网关中提取配置

因为主区域组中的主区域处理所有元数据更新，所以创建用户等操作必须在主区域中进行

可以在辅助区域执行元数据操作，但不建议这样做，因为元数据不会在该realm上同步。这种行为可能导致元数据碎片和区域之间的配置不一致

这个架构可以用几种方式构建:

1. 单个 zone 配置在域中有一个zone组和一个zone。一个或多个(可能是负载均衡的)RADOS网关由一个红帽Ceph存储集群支持
2. multizone配置包含一个zone组和多个zone。每个zone由一个或多个RADOS网关和一个独立的 Ceph 集群支持。存储在一个zone内的数据会复制到zone组内的所有zone。如果一个区域发生灾难性故障，这可以用于灾难恢复
3. multizone组配置包含多个zone组，每个zone组包含一个或多个zone。通过多区域组可以管理一个区域内或多个区域内的RADOS网关的地理位置
4. 多区域配置允许使用相同的硬件来支持多个对象名称空间，这些名称空间在区域组和区域之间是通用的


`一个最小的RADOS Gateway多站点部署需要两个红帽Ceph存储集群`，每个集群需要一个`RADOS Gateway`。

它们存在于同一个realm中，并被分配到相同的主区域组。一个RADOS网关与该区域组中的主区域相关联。另一个与该区域组中的一个独立辅助区域相关联。这是一个基本的多区域配置

##### 更改Periods和Epochs

`每个realm都有一个相关联的Period，每个Period都有一个相关联的epoch。`

Period用于跟踪realm、区域组和区域在特定时间的配置状态。epoch是用于跟踪特定realm期间配置更改的版本号。每个时期都有一个唯一的ID，包含realm配置，并且知道以前的Period ID

更新主域配置时，`RADOS Gateway`服务会更新Period。这个新Period就成了realm的当前Period，而这个Period的epoch又依次增加。对于其他配置更改，只增加epoch，Period不变

##### 多站点同步流程

`RADOS网关同步所有主、从zone group集之间的元数据和数据操作`。

`元数据操作与桶相关`:桶的创建、桶的删除、桶的启停、桶的用户管理。元数据主分区位于主分区组的主分区中，负责管理元数据的更新。数据操作是那些与对象相关的操作

当多站点配置激活时，RADOS网关会在主备区域之间进行初始的全同步。后续更新是增量式的

当RADOS Gateway将数据写入一个区域组内的任何区域时，它会在其他区域组中的所有区域之间同步该数据。当RADOS网关同步数据时，所有活动网关都会更新数据日志，并通知其他网关。当RADOS网关因为桶或用户操作而同步元数据时，master会更新元数据日志，并通知其他RADOS网关

### 配置多站点RGW部署

通过使用`Ceph orchestrator`命令行接口或使用服务规范文件，可以部署、配置和删除`多站点Ceph RADOS网关`实例

#### 多站点配置示例

下面的示例配置一个`realm`，其中`一个区域组`包含`两个区域`，一个作为主区域，另一个作为辅助区域。每个区域有一个与之相关联的RADOS Gateway实例

##### 配置主区域

这些示例步骤在主区域中配置`RADOS Gateway`实例

1. 创建realm
   ```bash
   [ceph: root@node01 /]# radosgw-admin realm create --default --rgw-realm=gold
   ```
2. 创建主zone组
   ```bash
   [ceph: root@node01 /]# radosgw-admin zonegroup create --rgw-zonegroup=us --master --default --endpoints=http://node01:80
   ```
3. 创建主zone
   ```bash
   [ceph: root@node01 /]# radosgw-admin zone create --rgw-zone=datacenter01 --master --rgw-zonegroup=us --endpoints=http://node01:80 --access-key=12345 --secret=67890 --default
   ```
4. 创建系统用户
   
   ```bash
   [ceph: root@node01 /]# radosgw-admin user create --uid=sysadm --display-name="SysAdmin" --access-key=12345 --secret=67890 --system 
   ```
5. 提交更改
   ```bash
   [ceph: root@node01 /]# radosgw-admin period update --commit 
   ```
6. 创建主zone的RADOS Gateway服务
   ```bash
   (ceph: root@node /)# ceph orch apply rgw gold-service --realm=gold --zone=datacenter81 --placement="1 node81"
   ```
7. 更新配置数据库中的区域名称
   
   ```bash
   (ceph: root@node01 /)# ceph config set client.rgw rgw_zone datacenter01
   ```

##### 配置辅助zone

这些示例步骤在辅助区域上配置RADOS Gateway实例
1. 拉取realm配置
   
   ```bash
   [ceph: root@node02 /]# radosgw-admin realm pull --rgw-realm=gold --url=http://node01:80 --access-key=12345 --secret=67898 --default 
   ```
2. 拉取 period
   ```bash
   (ceph: root@node02 /]# radosgw-admin period pull --url=http://node81:8888 --access-key=12345 --secret=67898
   ```
3. 创建辅助zone
   ```bash
   [ceph: root@node /]# radosgw-admin zone create --rgw-zone=datacenter02 --rgw-zonegroup=us --endpoints=http://node02:80 --access-key=12345 --secret=67898 --default
   ```
   将zone加入到zone组时，可以使用--read-only选项将其设置为只读
4. 提交更改
   ```bash
   [ceph: root@node02 ]# radosgw-admin period update --commit 
   ```
5. 创建辅助区域的RADOS Gateway服务
   ```bash
   [ceph: root@node02 /]# ceph orch apply rgw gold-service --realm=gold --zone=datacenter02 --placement="1 node02" 
   ```
6. 更新配置数据库中的区域名称
   ```bash
   [ceph: root@node02 /]# ceph config set client.rgw rgw_zone datacenter02
   ```
   使用radosgw-admin sync status命令查看同步状态

### 管理区域故障转移

在`多站点部署`中，当`主区域不可用`时，`备用区域`可以继续为`读写请求提供服务`。但由于主分区不可用，无法创建新的桶和用户。如果主区域不能立即恢复，则`提升一个辅助区域作为主区域的替代品`。

如果要`提升辅助zone`，需要修改zone和zone组，并提交period更新

1. 将主区域指向辅助区域(datacenter02)
   ```bash
   [ceph: root@node02 /]# radosgw-admin zone modify --master --rgw-zone=datacenter02 
   ```
2. 修改主分区角色后，需要更新主分区组
   ```bash
   [ceph: root@node02 /]# radosgw-admin zonegroup modify --rgw-zonegroup=us --endpoints=http://node02:80
   ```
3. 提交更改
   ```bash
   [ceph: root@node02 /]# radosgw-admin period update --commit
   ```
### 元数据搜索功能

大型对象变得越来越普遍。用户仍然需要对对象进行智能访问，以便从对象元数据中提取额外的信息。与对象关联的元数据提供关于对象的深刻信息。`RADOS Gateway支持通过Elasticsearch查询对象存储中的元数据`，并根据用户自定义的元数据对对象进行索引

`RADOS网关`支持`Elasticsearch`作为`multimode`架构的一个组件

`Elasticsearch`可以管理`zone`组中所有对象的元数据。一个区域组可以包含存储对象的区域，而其他区域则存储这些对象的元数据

以下命令将`metadata-zone` zone定义为由`Elasticsearch`管理的元数据区域:

```bash
[ceph: root@node /]# radosgw-admin zone modify --rgw-zone=metadata-zone --tier-type=elasticsearch --tier-config=endpoint=http://node03:9200, num_shards=10,num_replicas=1 
```

+ --tier-type 选项将区域类型设置为lastsearch
+ --tier-config选项定义Elasticsearch区域的配置
+ endpoint参数定义了访问Elasticsearch服务器的端点
+ num_shards参数定义了E asticsearch使用的分片数量
+ num_replicas参数指定Elasticsearch使用的副本数量

### RADOS网关多站点监控

可以在`Ceph存储仪表板`中监控`RADOS网关的性能和使用统计数据`。为了将RADOS网关与Ceph存储仪表板集成，必须在JSON格式文件中添加带有系统标志的`RGW用户登录凭证`

当向仪表板添加`rgw系统用户`时，使用`ceph dashboard set-rgw-api-access-key`和`ceph dashboard set-rgw-api-secret- key`命令提供访问密钥和密钥

```bash
[ceph: root@node /]# cat access_key 
{'myzone. node.tdncax ': ' ACCESS_KEY '} 
[ceph: root@node /]# cat secret_key 
{'myzone. node .tdncax ': ' SECRET_KEY '} 
[ceph: root@node /]# ceph dashboard set-rgw-api-access-key -i access_key 
Option RGW__API_ACCESS_KEY updated 
[ceph: root@node /]# ceph dashboard set-rgw-api-secret-key -i secret_key 
Option RGW__API_SECRET_KEY updated 
```

`Ceph RADOS Gateway`服务详情可通过登录“Dashboard”，单击“Object Gateway”查看。可以选择守护进程、用户或桶。在Daemons子菜单中，仪表板显示了RGW守护进程的列表

![](https://k8s.ruitong.cn:8080/Redhat/CL260-RHCS5.0-en-1-20211117/images/object/ObjectGateway_DaemonList.png)

单击守护进程名称，可以查看详细信息和性能统计信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/b8e10faa85bb4407b564e974a9cf5907.png)


如果需要查看业务的整体性能，单击“整体性能”

![在这里插入图片描述](https://img-blog.csdnimg.cn/2ac064e5ebd24aa2a9d435717fa6292a.png)


















## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
