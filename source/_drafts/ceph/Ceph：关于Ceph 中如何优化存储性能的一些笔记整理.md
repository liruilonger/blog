---
title: 'Ceph：关于Ceph 中如何优化存储性能的一些笔记整理'
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-06-14 06:48:03/"Ceph：关于Ceph 中如何优化存储性能的一些笔记整理".html'
mathJax: false
date: 2023-06-14 14:48:03
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 分享一些 优化 Ceph 存储性能 的笔记
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***



## 优化 Ceph 存储性能

### 定义性能调优

`性能调优` 是裁减系统配置的过程，以便特定的关键应用程序具有最佳的 `响应时间或吞吐量`。

Ceph 集群的性能调优有三个指标: `延迟`、`IOPS(每秒输入输出操作)`和`吞吐量`。

#### 延迟

`磁盘延迟是设备的一个函数，响应时间是整个服务器的一个函数`，所以说 磁盘延迟和响应时间是同一件事，这是一种常见的误解。 

对于使用旋转盘片的`硬盘驱动器`，磁盘延迟有两个组成部分:
+ **寻道时间:**  在盘片上将磁头定位到正确轨道所花费的时间，通常为0.2到0.8毫秒
+ **旋转延迟:**  轨道上正确的起始扇区从磁头下经过所需要的额外时间，通常为几毫秒。在磁头定位好之后，驱动器就可以开始从盘片传输数据了。在这一点上，顺序数据传输速率很重要。

对于`固态硬盘(ssd)`，等效的度量是存储设备的随机访问延迟，通常`小于一毫秒`。

对于非易失性存储器表示驱动器(`NVMes`)，存储驱动器的随机访问延迟通常以`微秒为单位`
  
#### 每秒操作 (IOPS)

**每秒操作 (IOPS)** 系统每秒能处理的`读写请求数`与`存储设备的能力和应用有关`。

当应用程序发出`I/O`请求时，操作系统将请求传输给设备，并等待直到请求完成。

+ 使用旋转盘片的`硬盘`的IOPS在`50到200之间`，
+ `ssd`的IOPS在`数千到数十万之间`，
+ `NVMes`的IOPS在数`十万左右`
  
#### 吞吐量

`吞吐量`指的是系统每秒可以读取或写入的实际字节数。

`块的大小`和数据`传输速率`会影响`吞吐量`。磁盘块大小越大，延迟因素衰减得越多。数据传输速率越`高`，磁盘将数据从其表面传输到缓冲区的速度就越`快`

+ 使用旋转盘片的`硬盘`的吞吐量约为 `150mb/s`, 
+ `ssd`约为 `500mbp/s`, 
+ `NVMes`约为 `2000mb/s`。

可以测量网络和整个系统的吞吐量，从远程客户端到服务器

### 调优目标

使用的`硬件`决定了`系统和Ceph集群`的性能限制

性能调优的`目标`是尽可能`高效地使用硬件`

一个常见的现象是，调优一个特定子系统可能会对另一个子系统的性能产生`负面影响`。

例如，可以以`高吞吐量`为代价来`优化系统`以获得`低延迟`，因此，在开始调优之前，建立与 Ceph 集群的预期工作负载一致的目标:

根据工作负载的不同，调优目标应该包括:

1. `减少时延`
2. `增加设备侧IOPS`
3. `增加块大小`


**IOPS优化**

`块设备`上的`工作负载`通常是`IOPS密集型`的，例如 OpenStack 虚拟机上运行的数据库。典型的部署需要`高性能SAS驱动器`来存储`ssd或NVMe设备`上的日志

**吞吐量的优化**

`RADOS网关`上的工作负载通常是`吞吐量密集型`的。对象可以存储大量的数据，比如音频和视频内容

**容量优化**

需要以尽可能低的成本存储大量数据的工作负载通常`以性能换取价格`。选择更便宜和更慢的`SATA驱动器`是这种工作负载的解决方案




## 优化Ceph性能

下面的部分描述了调优 Ceph 的推荐实践

### Ceph部署优化建议

正确规划 Ceph 集群部署是很重要的。

`MONs` 的性能对于整个集群的性能至关重要。对于`大型部署`，`MONs`应该位于`专用节点`上。为了保证`仲裁`的正确性，需要`奇数`个MONs

Ceph 设计用于处理大量数据，如果使用正确的硬件并正确地调优集群，则可以提高性能

在集群安装之后，开始持续监视集群，以排除故障并安排维护活动。

尽管 Ceph 具有显著的`自愈能力`，但许多类型的故障事件都需要 `快速通知和人工干预`。

如果出现性能问题，请在`磁盘、网络和硬件级别`进行故障排除。然后，继续诊断 `RADOS块设备` 和 `Ceph RADOS网关`

### OSD的优化建议

在写 `BlueStore` `块数据库`和`WAL (write-ahead log)`时，为了提高效率，建议使用`ssd`盘或`NVMes`盘。

OSD的数据、块数据库和WAL可以配置在相同的存储设备上，也可以通过对这些组件使用`单独的设备来进行配置`

在典型的部署中，`osd 使用具有高延迟的传统旋转磁盘`，因为它们提供了令人满意的指标，以更低的`每兆字节成本`满足定义的目标。

默认情况下，`BlueStore osd` 将数据、块数据库和WAL放在`同一个块设备`上。但是，可以通过`为块数据库和WAL使用单独的低延迟ssd或NVMe设备来最大化效率`。

指定 OSD 服务对应的 BlueStore 设备

```yaml
service_type: osd
service_id: osd_example
placement:
  host_pattern: '*'
data_devices: # 对象数据
  paths:
    - /dev/vda
db_devices:  # 块数据库
  paths: 
    - /dev/nvme0
wal_devices: # 预写日志
  paths: 
    - /dev/nvme1
```

多个块数据库和WALs可以`共享同一个SSD或NVMe设备`，降低存储基础设施成本

考虑以下 `SSD` 规格对预期工作负载的影响:

1. 支持仪式数量的平均故障间隔时间(MTBF)
2. `IOPS`功能
3. 数据传送速率
4. BUS/ SSD几个功能

当一个承载日志的 `SSD` 或 `NVMe` 设备失效时，每个使用它来承载日志的 OSD 也将不可用。在决定在同一存储设备上放置多少块数据库或WALs时，请考虑这一点

### Ceph RADOS网关的优化建议

`RADOS 网关` 上的工作负载通常是`吞吐量密集型`的。

作为对象存储的音频和视频材料可能很大。但是，`桶索引池`通常显示更 `I/O密集型` 的工作负载模式。**建议将索引池存储在SSD设备上。**

RADOS网关为每个桶维护一个`索引`。

默认情况下，Ceph 将这个索引存储在一个`RADOS对象`中。当一个桶存储超过 `100,000个对象` 时，`单个索引对象`成为瓶颈，索引性能下降。

>Ceph 可以在多个 `RADOS对象` 或分片中保存`大索引`。通过设置 `rgw_override_bucket_index_max` 可启用该特性。建议为每桶中预期的对象数除以 `100,000`

随着索引的增长，Ceph必须定期重新共享桶。`Ceph` 提供了`桶索引自动重分片`功能。`rgw_dynamic_resharding` 参数(默认为true)控制该特性

### CephFS的建议

保存目录结构和其他索引的`元数据池`可能成为`CephFS`瓶颈。为了最大限度地减少这种限制，`元数据池`使用`SSD`设备(一个cephfs文件系统至少需要两个池，一个存储cephfs数据，另一个存储cephfs元数据)

每个`MDS`在`内存中为不同类型的项(如inode)维护一个缓存`。Ceph通过`mds_cache_memory_limit`参数限制这个缓存的大小。其默认值(以绝对字节表示)为`4gb`

## PG 放置组调优

由于某些`OSD节点`上不必要的CPU和RAM活动，集群中pg的总数可能会影响整体性能。Red Hat建议在将集群投入生产之前验证每个池的PG分配。还要考虑回填和回收的具体测试，对客户端I/O请求的影响，有两个重要的值:

1. 集群中`pg`的总数
2. 特定池中可使用的`pg`数量

使用这个公式来估算一个`特定池`中可使用的`pg`数量:

`放置组总数= (OSDs * 100) /副本数量`

应用每个池的公式可以获得集群的pg总数。Red Hat建议每个`OSD 100至200 pg`

**Splitting PGs**

Ceph支持增加或减少池中pg的数量。
如果在创建池时不指定该值，则创建池时使用默认值 `8pg`，这个值非常低

pg_autoscale_mode属性允许Ceph做出建议，并自动调整pg_num和pgp_num参数。在创建新池时，默认启用此选项。pg_num参数定义特定对象的pg数量。pgp_num参数定义CRUSH算法考虑放置的pg的数量。

红帽建议你增加放置组的数量，直到你达到理想的pg数量。大量增加pg的数量会导致集群性能下降，因为会产生预期的数据，重新定位和再平衡是密集的。

使用ceph osd pool set命令通过设置参数pg_num手动增加或减少pg数量。在禁用pg_autoscale模式选项的情况下，手动增加池中的pg数量时，应该只增加少量的增量

将放置组的总数设置为2的幂可以更好地在osd中分布pg。增加pgp_num参数会自动增加pgp_num参数，但会逐步增加，以减少对集群性能的影响

**PG合并**

Red Hat Ceph Storage可以将两个PG合并成一个更大的PG，减少PG的总数。
当池中的pg数量过大且性能下降时，合并可能很有用。因为合并是一个复杂的过程，所以每次只合并一个PG，以尽量减少对集群性能的影响

**PG Auto-scaling**

如前所述，PG自动缩放功能允许Ceph做出建议，并自动调整PG的数量。该特性在创建池时默认启用。对于现有的池，使用这个命令配置自动伸缩:

```bash
[admin@node -)$ ceph osd pool set pool-name pg_autoscale_mode mode
```

将模式参数设置为off以禁用它，设置为on以启用它，并允许Ceph自动调整pg的数量，或在必须调整pg数量时发出警报。

查看自动缩放模块提供的信息:

```bash
[admin@node -)$ ceph osd pool autoscale-status
```

#### 12.1.4 设计集群架构

在设计Ceph集群时，考虑扩展选择，以匹配未来的数据需求，并使用正确的网络大小和体系结构促进足够的吞吐量

##### 可扩展性

可以通过两种方式扩展集群存储:

1. 通过向集群中添加更多节点向外扩展

2. 通过向现有节点添加更多资源进行扩展

扩展要求节点可以接受更多的CPU和RAM资源，以处理磁盘数量和磁盘大小的增加。
向外扩展需要添加具有类似资源和容量的节点，以匹配集群的现有节点，从而实现平衡操作

##### 网络的最佳实践

连接Ceph集群中节点的网络对于良好的性能至关重要，因为所有客户端和I/O集群操作都使用它。红帽公司推荐以下做法:

1. 为了提高性能和提供更好的故障排除隔离，OSD流量和客户端流量使用单独的网络

2. 存储集群的网络容量不小于10gb。1gb组网不适合生产环境

3. 根据集群和客户端流量以及存储的数据量评估网络大小

4. 强烈建议进行网络监控

5. 在可能的情况下，使用单独的网卡连接到网络，或者使用单独的端口

![](https://k8s.ruitong.cn:8080/Redhat/CL260-RHCS5.0-en-1-20211117/images/tuning/tuning-tuning-cephperf-network.svg)

Ceph守护进程自动绑定到正确的接口，例如将MONs绑定到公共网络，将osd绑定到公共网络和集群网络

#### 12.1.5 手动控制PG的主OSD

使用主亲和性设置来影响Ceph选择一个特定的OSD作为放置组的主OSD。
设置越高，一个OSD被选择为主OSD的可能性越大。
可以通过配置集群来避免主OSD使用慢盘或控制器来缓解问题或瓶颈。ceph osd primary-affinity命令用于修改osd的主亲和性。亲和力是0和1之间的一个实数

```bash
[admin@node -)$ ceph osd primary-affinity osd-number affinity 
```

##### OSD恢复与回填

当Ceph在集群中添加或移除一个OSD时，Ceph会对pg进行重新平衡，使用新的OSD或重新创建存储在被移除的OSD中的副本。
这些回填和恢复操作会产生较高的集群网络流量负载，从而影响性能。

为避免集群性能下降，请调整回填和恢复操作，使重新平衡与集群正常操作之间的关系。
Ceph提供参数来限制回填和回收操作的1/0和网络活动。

以下列表包括其中一些参数:

|            参数             | 定义                                 |
| :-------------------------: | ------------------------------------ |
|  osd_recovery_op_ priority  | 恢复操作优先级                       |
|   osd_recovery_max_active   | 每个OSD的最大并发恢复请求数          |
|    osd_recovery_threads     | 用于数据恢复的线程数                 |
|      osd_max_backfills      | 单个OSD的最大回填数                  |
|    osd_backfill_scan_min    | 每次回填扫描的最小对象数             |
|    osd_backfill_scan_max    | 每次回填扫描的最大对象数             |
|   osd_backfill_full_ratio   | 向OSD回填请求的阈值                  |
| osd_backfill_retry_interval | 在重新尝试回填请求之前，需要等待几秒 |

#### 12.1.6 配置硬件

对集群的预期工作负载使用现实的指标，构建集群的硬件配置，以提供足够的性能，但保持成本尽可能低。
Red Hat建议将这些硬件配置用于以下三个性能优先级:

**IOPS优化**

1. 每个NVMe设备使用两个osd

2. NVMe驱动器将数据、块数据库和WAL配置在同一个存储设备上

3. 假设有一个2ghz的CPU，每个NVMe使用10个核，或者每个SSD使用2个核

4. 分配16gb内存作为基准，每个OSD加5gb内存

5. 每2个osd使用10gbe网卡

**吞吐量的优化**

1. 每个硬盘使用一个OSD

2. 将块数据库和WAL放置在ssd或nvme上

3. 使用至少7200 RPM的硬盘驱动器

4. 假设有一个2ghz的CPU，每个HDD使用半个核

5. 分配16gb内存作为基准，每个OSD加5gb内存

6. 每12个osd使用10个GbE网卡

**容量优化**

1. 每个硬盘使用一个OSD

2. hdd将数据、块数据库和WAL配置在同一个存储设备上

3. 使用至少7200 RPM的硬盘驱动器

4. 假设有一个2ghz的CPU，每个HDD使用半个核

5. 分配16gb内存作为基准，每个OSD加5gb内存

6. 每12个osd使用10个GbE网卡

#### 12.1.7 使用Ceph性能工具进行调优

性能工具提供基准度量来检查集群的性能问题

##### 性能计数器和指标

每个Ceph守护进程都维护一组内部计数器和仪表。有几个工具可以访问这些计数器:

- **仪表板插件**
  Dashboard插件公开了一个可以在端口8443上访问的web界面。
  “集群OSD”菜单提供OSD的基本实时统计信息，如读字节数、写字节数、读操作次数、写操作次数等。
  使用ceph mgr模块Enable Dashboard命令启用Dashboard插件。如果您使用cephadm bootstrap命令引导集群，那么默认情况下仪表板是启用的
- **Manager (MGR) Prometheus 插件**
  该插件在端口9283上公开性能指标，供外部Prometheus服务器收集。Prometheus是一个开源的系统监视和警报工具

- **ceph命令行工具**
  ceph命令具有查看指标和更改守护进程参数的选项

#### 12.1.8 性能压力工具

Red Hat Ceph Storage提供了对Ceph集群进行压力测试和基准测试的工具。

##### RADOS bench命令

RADOS bench是一个简单的测试RADOS对象存储的工具。
它在集群上执行写和读测试，并提供统计数据。
该命令的通用语法如下:

```bash
[admin@node -]$ rados bench \
	 -p POOL-NAME SECONDS write|seq|rand -b objsize -t concurrency
```

以下是该工具的常用参数:

1. seq和rand测试是顺序和随机读取基准测试。这些测试要求首先运行写入基准测试，并使用--no-cleanup选项。
   默认情况下，RADOS平台会删除为编写测试创建的对象。
   --no-cleanup选项将保留这些对象，这对于在相同的对象上执行多个测试非常有用

2. 默认的对象大小是4mb

3. 默认并发数为16

使用--no-cleanup选项，在运行rados bench命令后，必须手动删除池中保留的数据

例如，rados bench命令提供的吞吐量、IOPS、时延信息如下:

```bash
[ceph: root@server /]# rados bench \
	-p testbench 10 write --no-cleanup hints = 1
```

##### RBD bench命令

RBD测试在为测试创建的现有映像上测量I/O的吞吐量和延迟

这些是默认值:

1. 如果不为 size 参数提供后缀，则该命令假定以字节为单位

2. 默认池名为 rbd

3. -io-size 的默认值是 4096 字节

4. --io-threads 的默认值为 16

5. --io-total 的默认值是 1gb

6. --io-mode 的默认值是 seq

例如，rbd bench命令提供的信息包括吞吐量和延迟:

```bash
[ceph: root@server /]# rbd bench \
	--io-type write testimage --pool=testbench 
```



## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
