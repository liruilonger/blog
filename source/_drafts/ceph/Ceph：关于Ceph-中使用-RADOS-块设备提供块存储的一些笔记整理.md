---
title: Ceph：关于Ceph 中使用 RADOS 块设备提供块存储的一些笔记整理
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-05-16 09:14:59/Ceph：关于Ceph 中使用 RADOS 块设备提供块存储的一些笔记整理.html'
mathJax: false
date: 2023-05-16 17:14:59
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

# 使用 RADOS 块设备提供块存储

## 管理RADOS块设备

### 基于RBD的块存储

`块设备`是服务器、笔记本电脑和其他计算系统最常见的长期存储设备。它们将数据存储在固定大小的块中。块设备包括基于旋转磁碟的硬盘驱动器和基于非易失性存储器的固态驱动器。

要使用块存储，需要使用`文件系统格式化块设备`，并将其`挂载`到`Linux文件系统层次结构`上

`RBD (RADOS Block Device)` 特性提供来自 `Ceph` 存储集群的块存储。RADOS 在 Ceph 存储集群的池中提供了存储为 RBD 镜像的虚拟块设备

### 管理和配置RBD镜像

作为存储管理员，可以使用 `rbd` 命令创建、列表、检索块设备镜像信息、调整大小和移除块设备镜像。创建RBD镜像的示例如下:

1. 确保 `rbd` 镜像的 `rbd` 池(或自定义池)存在。使用 `ceph osd pool create` 命令创建 `RBD镜像池`。创建完成后，需要使用`rbd pool init`命令对其进行初始化(初始化会自动rbd的应用程序)
2. 虽然 Ceph 管理员可以访问池，但建议您使用`Ceph auth`命令为客户端创建一个更受限制的`Cephx`用户。授予用户`只对需要的RBD池进行读写访问`.
3. 使用 `rbd create --size size pool-name/image-name` 命令创建RBD镜像。如果不指定存储池名称，则使用默认的存储池名称

```bash
# 查看指定池的 镜像和镜像具体信息
[root@clienta ~]# rbd ls  pooldemo
test
[root@clienta ~]# rbd info  pooldemo/test
rbd image 'test':
        size 128 MiB in 32 objects
        order 22 (4 MiB objects)
        snapshot_count: 0
        id: 170bb9d2a23c
        block_name_prefix: rbd_data.170bb9d2a23c
        format: 2
        features: layering, exclusive-lock, object-map, fast-diff, deep-flatten
        op_features:
        flags:
        create_timestamp: Thu May 11 01:59:19 2023
        access_timestamp: Thu May 11 01:59:19 2023
        modify_timestamp: Thu May 11 01:59:19 2023
[root@clienta ~]#
```
 
`rbd_defaultlt_pool` 参数指定用于存储 RBD 镜像的默认池的名称。使用 `ceph config set osd rbd_default pool value` 设置该参数

### 访问RADOS块设备存储

客户机允许裸机服务器或虚拟机使用 RBD镜像 作为普通的 基于块的存储:
+ 内核 `RBD客户端(krbd)` 将 `RBD`镜像映射到 Linux 块设备。
+ `librbd` 库为 `KVM` 虚拟机和 `OpenStack` 云实例提供 `RBD` 存储。



#### 使用 RBD 内核客户端访问 Ceph存储

Ceph客户端可以使用本地`Linux内核模块krbd`挂载`RBD镜像`。这个模块将RBD镜像`映射`到名称为`/dev/rbd0`的`Linux块设备
`
![在这里插入图片描述](https://img-blog.csdnimg.cn/9019aca19103444997d8fd80a3a851cb.png)


`rbd device map`命令使用`krbd`内核模块来映射一个`image`。`rbd map` 命令是 `rbd device map` 命令的缩写。`rbd device unmap (rbd unmap)` 命令使用`krbd内核模块`解除映射的镜像。将`RBD池`中的test RBD镜像映射到客户端主机上的`/dev/rbd0`设备
```bash
[root@node ~]# rbd map rbd/test 
/dev/rbd0 
```

Ceph 客户端系统可以像其他块设备一样使用映射的块设备(在示例中称为`/dev/rbd0`)。可以使用文件系统对其进行格式化、挂载和卸载

`两个客户端可以同时将同一个RBD镜像映射为一个块设备`。这对于备用服务器的高可用性集群非常有用，但是`Red Hat`建议当`块设备包含一个普通的单挂载文件系统`时，一次将一个块设备附加到一个客户机上。同时在两个或多个客户机上挂载包含普通文件系统(如XFS)的RADOS块设备可能会`导致文件系统损坏和数据丢失`

`rbd device list` 命令，缩写为` rbd showmapped`，用来列出机器上映射的rbd镜像

```bash
[root@clienta ~]# rbd showmapped
id  pool      namespace  image  snap  device
0   pooldemo             test   -     /dev/rbd0
[root@clienta ~]#
```

```bash
[root@clienta ~]# mkfs.xfs  /dev/rbd0
meta-data=/dev/rbd0              isize=512    agcount=8, agsize=4096 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1
data     =                       bsize=4096   blocks=32768, imaxpct=25
         =                       sunit=16     swidth=16 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=1872, version=2
         =                       sectsz=512   sunit=16 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
Discarding blocks...Done.
[root@clienta ~]# mkdir /mnt/rdb
[root@clienta ~]# mount /dev/rbd0 /mnt/rdb
[root@clienta ~]# df -h /mnt/rdb/
Filesystem      Size  Used Avail Use% Mounted on
/dev/rbd0       121M  7.8M  113M   7% /mnt/rdb
[root@clienta ~]# dd if=/dev/zero of=/mnt/rdb/test1 bs=10M count=1
1+0 records in
1+0 records out
10485760 bytes (10 MB, 10 MiB) copied, 0.00433976 s, 2.4 GB/s
[root@clienta ~]# ls -lh /mnt/rdb
total 10M
-rw-r--r--. 1 root root 10M May 11 02:13 test1
```

`rbd device unmap` 命令，缩写为`rbd unmap`，用于从客户端机器上解除rbd镜像的映射

```bash
[root@node ~]# rbd unmap /dev/rbd0
```
`rbd map`和`rbd unmap`命令需要root权限




##### 持久化映射RBD图像

`rbdmap` 服务可以在启动和关闭系统时自动将RBD镜像映射和解除映射到设备。`/etc/ceph/rbdmap` 中查找具有其凭证的映射图像，服务使用它们在 `/etc/fstab` 文件中显示的挂载点来挂载和卸载RBD镜像
```bash
[root@clienta ~]# systemctl  status rbdmap.service
● rbdmap.service - Map RBD devices
   Loaded: loaded (/usr/lib/systemd/system/rbdmap.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[root@clienta ~]#
```

以下步骤将 `rbdmap` 配置为持久化映射和解除映射一个已经包含文件系统的RBD镜像:

1. 为文件系统创建挂载点

2. 在`/etc/ceph/rbdmap` RBD 映射文件中创建一个单行条目。这个条目必须指定 RBD 池和镜像的名称。它还必须引用具有读写权限的 Cephx 用户来访问镜像和相应的密钥环文件。确保客户端系统上存在用于Cephx用户的key-ring文件

```bash
# cat >> /etc/ceph/rbdmap <<EOF
test_pool/test id=test_pool.clientb,keyring=/etc/ceph/ceph.client.test_pool.clientb.keyring
EOM
```
3. 在客户端系统的`/etc/fstab`文件中为RBD创建一个条目。块设备的名称形式如下:

```bash
/dev/rbd/pool_name/image_name
```
```bash
# cat >> /etc/fstab <<EOF
/dev/rbd/test_pool/test /mnt/rbd xfs noauto 0 0
EOF
```

指定 `noauto` 挂载选项，因为处理文件系统挂载的是 `rbdmap` 服务，而不是`Linux fstab`例程

4. 确认块设备映射成功。使用rbdmap map命令挂载设备。使用rbdmap unmap命令卸载
```bash
# rbdmap map
# rbd showmapped
# rbdmap unmap
# rbd showmapped
```
5. 启用rbdmap systemd服务，有关更多信息，请参阅rbdmap(8)
```bash
# systemctl enable rbdmap
# reboot

# df /mnt/rbd
```

#### 使用基于librbd 的客户端访问 Ceph 存储

librbd 库为用户空间应用程序提供了对 RBD 镜像的直接访问。它继承了 librados 将数据块映射到 Ceph对象存储中的对象的能力，并实现了访问RBD镜像以及创建快照和克隆的能力

简单的Python脚本示例，用于连接到CEPH集群并使用librbd API打开和读取块设备镜像：

```py
import rados, rbd

# 创建RADOS集群对象并连接到Ceph集群
cluster = rados.Rados(conffile='/etc/ceph/ceph.conf')
cluster.connect()

# 打开RBD映像
with cluster.open_ioctx('rbd') as ioctx:
    with rbd.Image(ioctx, 'test_image') as image:
        # 读取图像数据并将其输出到控制台
        data = image.read(0, 4096)
        print(data)

# 断开与Ceph集群的连接
cluster.shutdown()

```
使用rados和rbd Python模块连接到Ceph集群，并使用 open_ioctx() 和 Image() 函数打开RBD映像对象。然后，我们使用 read() 函数从映像中读取数据。

### RBD缓存

因为 `Ceph块设备` 的用户空间实现(例如，librbd)不能利用Linux页面缓存，所以它执行自己的内存缓存，称为`RBD缓存`。

`RBD缓存` 的行为与Linux 页面缓存的方式类似。当OS实现一个 barrier 机制或一个flush请求时，Ceph将所有脏数据写入osd。这意味着使用回写缓存与在虚拟机中使用物理硬盘缓存(例如，Linux内核>= 2.6.32)一样安全。缓存使用最近最少使用(`LRU`)算法，在回写模式下，它可以合并连续的请求以获得更好的吞吐量


`RBD 缓存`对于客户机来说是本地的，因为它使用发起I/O请求的机器上的RAM。例如，如果你的redhat OpenStack平台安装的Nova计算节点使用librbd作为虚拟机，OpenStack客户端启动I/O请求将使用本地RAM作为RBD缓存

#### RBD缓存配置

**缓存未启用**

读取和写入到Ceph对象存储。Ceph集群在所有相关 OSD 日志上写入和刷新数据时承认写入操作。

**缓存启用(回写式)**

考虑两个值，未刷新的缓存字节数U和最大脏缓存字节数M，当 `U < M` 时，或者在将数据写回磁盘直到 `U < M` 时，才承认写操作

**直写式高速缓存**

将最大脏字节设置为`O`以强制透写模式。Ceph集群在所有相关`OSD`日志上写入和刷新数据时承认写入操作

如果使用回写模式，那么当librbd库将数据写入服务器的本地缓存时，它会缓存并承认I/O请求。考虑对战略生产服务器进行透写，以减少服务器故障时数据丢失或文件系统损坏的风险。Red Hat Ceph Storage提供了以下一组RBD缓存参数:

| 参数                                 | 描述                           | 默认    |
| ---------------------------------- | ---------------------------- | ----- |
| rbd_cache                          | 启用RBD缓存，Value=true I false   | true  |
| rbd_cache_size                     | 每个RBD的缓存大小，单位为字节             | 32 MB |
| rbd_cache_ max_dirty               | 每个RBD镜像允许的最大脏字节              | 24 MB |
| rbd_cache_target_dirty             | 每个RBD镜像启动抢占式刷写的脏字节           | 16 MB |
| rbd_cache_max_dirty_age            | 刷写前的最大页寿命(以秒为单位)             | 1     |
| rbd_cache_writethrough_until_flush | 从write-through模式开始，直到执行第一次刷新 | true  |

分别执行`ceph config set client parameter value`命令或`ceph config set global parameter value`命令


### 调整RBD镜像格式

`RBD` 镜像在对象上条带化，并存储在RADOS对象存储中。`Ceph` 提供了定义这些镜像如何条纹化的参数

#### RADOS块设备镜像布局

`RBD` 镜像中的所有对象都有一个名称，以每个RBD镜像的`RBD Block name Prefix`字段的值开头，通过`RBD info`命令显示。在这个前缀之后，有一个句点(.)，后面跟着对象编号。对象编号字段的值是一个12个字符的十六进制数

```bash
[root@node ~]# rbd info rbdimage 
rbd image ' rbdimage ': 
size 10240{nbsp}MB in 2560 objects 
order 22 (4 MiB objects) 
snapshot_ count: 0 
id: 867cba5c2d68 
block_name_prefix: rbd_data.867cba5c2d68 
format: 2 
features: layering, exclusive-lock, object-map, fast-diff, deep-flatten 
```
+ rbd image ' rbdimage '：表示RBD镜像的名称。
+ size 10240 MB in 2560 objects：表示RBD镜像的大小和对象数量。
+ order 22 (4 MiB objects)：表示RBD镜像的对象大小（以2的幂次方字节数表示）。
+ snapshot_count: 0：表示RBD镜像中快照的数量。
+ id: 867cba5c2d68：表示RBD镜像的ID。
+ block_name_prefix: rbd_data.867cba5c2d68：表示RBD镜像数据块的前缀名称。
+ format: 2：表示RBD镜像的格式版本。
+ features: layering, exclusive-lock, object-map, fast-diff, deep-flatten：表示RBD镜像所使用的特性，如层级、排它锁、对象映射、快速差异和深度展开等。

```bash
[root@node -]# rados -p rbd ls 
rbd_object_map . d3d0d7d0b79e . 0000000000000008 
rbd_id. rbdimage 
rbd_object_map . d42cle0al883 
rbd_directory 
rbd_children 
rbd_info 
rbd_header. d3d0d7d0b79e 
rbd_header. d42cle0al883 
rbd_object_map . d3d0d7d0b79e 
rbd_trash 
```
+ rbd_object_map.<image_id>.<snap_id>：表示RBD镜像的对象映射。
+ rbd_id.<image_name>：表示RBD镜像的ID。
+ rbd_directory：表示RBD镜像的目录。
+ rbd_children：表示RBD镜像的子镜像列表。
+ rbd_info：表示RBD镜像的信息。
+ rbd_header.<image_id>：表示RBD镜像的头文件。
+ rbd_trash：表示RBD镜像被删除时存储在其中的内容。

Ceph块设备支持在一个红帽Ceph存储集群内的多个OSD上条带化存储数据

![](https://k8s.ruitong.cn:8080/Redhat/CL260-RHCS5.0-en-1-20211117/images/block/devices-rbd-layout.svg)

##### RBD镜像顺序

`镜像顺序`是RBD镜像中使用的对象的大小。镜像顺序的值必须在12到25之间，其中12 = 4 KiB, 13 = 8 KiB。为例。默认镜像顺序为22，产生4个MiB节点。可以使用rbd create命令的`--order`选项来覆盖缺省值

你可以用`--object-size`选项指定对象的大小。该参数指定的对象大小必须在4096 (4kib) ~ 33,554,432 (32mib)之间，单位为字节/ K或M(如4096、8k或4m)

##### RBD镜像格式

每个RBD镜像都有三个相关参数:

**image_format** RBD镜像格式版本。默认值为2，即最新版本。版本1已被弃用，不支持克隆和镜像等特性

**stripe_unit** 一个对象中存储的连续字节数，默认为`object_size`。

**stripe_count** 条带跨越的RBD镜像对象数目，默认为1

对于RBD格式2镜像，可以更改每个参数的值。设置必须与下列等式对齐:

```bash
stripe_unit * stripe_count = object_size
```

For example:

```bash
stripe_unit = 1048576, stripe_count = 4 for default 4 MiB objects
```

记住object_size必须不小于4096字节，且不大于33,554,432字节。当你创建RBD镜像时，使用`--object-size`选项指定这个值。缺省情况下，节点大小为4192304字节(4mib)


## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
