---
title: Ceph：关于Ceph 中使用 RADOS 网关提供对象存储的一些笔记整理
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-04-16 09:18:15/Ceph：关于Ceph 中使用 RADOS 网关提供对象存储的一些笔记整理.html'
mathJax: false
date: 2023-04-16 17:18:15
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


#  使用 RADOS 网关提供对象存储

## 部署对象存储网关

### 对象存储是什么？

对象存储将数据存储为离散项，每个单独称为对象。与文件系统中的文件不同，对象不是按目录和子目录树组织的。相反，对象存储在`平面名称空间`中。通过使用`对象的唯一对象ID(也称为对象键)`检索每个对象

应用程序不使用普通的文件系统操作来访问对象数据。相反，应用程序访问一个`REST API`来发送和接收对象。 

Ceph 支持两种最常用的对象资源:`Amazon S3(简单存储服务)`和`OpenStack Swift (OpenStack对象存储)`

+ `Amazon S3`将对象存储的平面命名空间称为`桶`，
+ `OpenStack Swift`将其称为`容器`。

因为名称空间是平面的，所以`桶和容器`都不能`嵌套`。Ceph通常使用`桶`这个术语， 一个`用户帐户`可以访问同一个存储集群中的多个`桶`。每个桶可以有不同的访问权限，用于存储不同用例的对象

对象存储的优点
+ 是易于使用、扩展和扩展。因为每个对象都有一个惟一的ID，所以可以在用户不知道对象位置的情况下存储或检索它
+ 没有目录层次结构，简化了对象之间的关系
+ 与文件类似，对象包含二进制数据流，并且可以增长到任意大的大小
+ 对象还包含关于对象数据的元数据，并支持扩展元数据信息，通常以键-值对的形式存在。您还可以创建自己的元数据键，并将自定义信息作为键值存储在对象中

### RADOS网关

`RADOS网关`，也被称为`RGW (Object Gateway)`，是一种服务，为使用标准对象存储 api 的客户端提供对 Ceph 集群的访问。`RADOS网关` 同时支持 `Amazon S3和OpenStack Swift的api`


核心守护进程 `radosgw` 构建在`librados` 库之上。这个守护进程提供一个基于 `Beast HTTP、WebSocket`和 网络协议库的web服务接口，作为处理API请求的前端

`radosgw` 是 Ceph 的客户端，提供对其他客户端应用程序的对象访问。客户端应用程序使用标准的`api与RADOS网关通信`，而`RADOS网关使用librados模块`调用与Ceph集群通信

![在这里插入图片描述](https://img-blog.csdnimg.cn/d14f8474c5ba455e94e05691da60f0d9.png)


RADOS网关提供了 `radosgw-admin`实用程序，用于创建使用该`网关的用户`。这些用户只能访问网关，而`不是`直接访问存储集群的`cephx用户`。

在提交`Amazon S3或OpenStack Swift API`请求时，RADOS网关客户端使用这些网关用户帐户进行`鉴权`。

通过RADOS网关对网关用户进行身份验证后，网关使用cephx凭据向存储集群进行身份验证，以处理对象请求。网关用户也可以通过集成基于`ldap`的外部认证服务进行管理

RADOS网关服务`自动`在每个区域的基础上`创建池`。这些池使用配置数据库中的放置组(PG)值，并使用默认的CRUSH层次结构

`默认池`设置对于生产环境可能不是最优的

RADOS网关为`默认区域`创建多个池:

1. `rgw.root` 存储记录信息
2. `default.rgw.control` 用作控制池
3. `default.rgw.meta` 存储user_key和其他关键元数据
4. `default.rgw.log` 包含所有bucket或container以及对象操作的日志，如create，read，delete
5. `default.rgw.buckets.index` 存储桶的索引
6. `default.rgw.buckets.data` 存储桶数据
7. `default.rgw.buckets.non-ec` 用于multipart对象元数据上传

可以使用`自定义设置手动创建池`。Red Hat 建议将区域名称作为手动创建的池的前缀，`.<zone-name>.rgw.control`,例如：`.us-east1-1.rgw.buckets.data` 作为`us-east-1`区域的池名称

#### RADOS网关支持静态Web内容托管

`RADOS网关`支持静态网站托管在`S3桶`中，这可以比使用虚拟机更有效的网站托管。这适用于只使用静态元素的网站，如XHTML或HTML文件，或CSS

为静态web主机部署RADOS网关实例有限制
+ 实例不能用于S3或Swift API访问
+ 实例的域名应该与标准S3和Swift AP的域名不同，而且不能重叠。网关实例
+ 实例应该使用不同于标准S3和Swift API网关实例的面向公共的IP地址

### RADOS网关部署

`cephadm`工具将`RADOS网关服务部署`为一组守护进程，用于管理单个集群或多站点部署。使用`client.rgw.*`来定义新的RADOS网关守护进程的参数和特征

使用`Ceph Orchestrator`部署或删除`RADOS网关服务`。通过命令行接口或服务规范文件使用`Ceph Orchestrator`

```bash
[ceph: root@node /)# ceph orch apply rgw <service-name> [--realm=<realm>J \ 
[--zone=<zone>J [--port=< port>] --placement="<num-daemons> (<hostl> ... ]" \ 
[--unmanaged]
```

在本例中，`Ceph Orchestrator`在单个集群中使用两个守护进程部署`my_rgw_service RADOS`网关服务，并在80端口上提供服务

```bash
[ceph: root@node /]# ceph orch apply rgw my_rgw_service
```

如果`client.rgw.*`未定义或在构建时传递给Ceph协调器，然后部署使用这些`默认设置`。

下面的示例YAML文件包含为RADOS Gateway部署定义的公共参数

```yaml
service_type: rgw 
service_name : rgw_service_name 
placement: 
  count: 2 
  hosts: 
    - node01 
    - node02 
spec: 
  rgw_frontend_port: 8080 
  rgw_realm: realm_name 
  rgw_zone: zone_name 
  ssl: true 
  rgw_frontend_ssl_certificate: |
    -----BEGIN PRIVATE KEY-----
    ... output omitted ... 
    -----END PRIVATE KEY-----
    -----BEGIN CERTIFICATE-----
    ... output omitted ... 
    -----END CERTIFICATE-----
networks: 
  - 172.25.200.0/24 
```

在本例中，创建RGW服务的参数与前一个服务类似，但现在使用的是CLI

```bash
[ceph: root@node /]# ceph orch apply rgw rgw_service_name --realm=realm_name \ 
--zone=zone_name --port 8080 --placement="2 node01 node02" --ssl 
```

注意，在服务规范文件中，`realm、zone和port`的参数名称与`CLI`使用的参数名称不同。`RGW`实例使用的网络、ssl证书内容等参数只能通过服务规范文件定义。

`count`参数设置在hosts参数中定义的每台服务器上创建RGW实例的数量。如果创建多个实例，那么Ceph协调器将第一个实例的端口设置为指定的`rgw_frontend_port`或port值。对于后续的每个实例，端口值增加1。使用前面的YAML文件示例，服务部署创建:

1. node0l服务器中的两个RGW实例，一个端口为8080，另一个端口为8081
2. node02服务器中的两个RGW实例，一个端口为8080，另一个端口为8081

每个实例都有自己唯一的端口供访问，并对请求创建相同的响应。通过部署提供单一业务IP地址和端口的负载均衡器服务来配置RADOS网关的高可用性

Ceph编排器服务通过使用该格式来命名守护进程 `rgw.<realm>.<zone>.<host>.<random-string>`

##### 自定义服务配置

使用集群配置文件`client.rgw section`中`rgw_frontend`参数中的`port`选项为RADOS网关配置Beast前端web端口。使用`ceph config`命令查看当前配置

```bash
[ceph: root@node /]# ceph config get client.rgw rgw_frontends 
beast port=7480
```

使用`TLS/SSL (Transport Layer Security/Secure Socket Layer)`协议时，在端口号的末尾使用s字符定义端口，例如port=443。端口选项支持使用加号字符(+)的双端口配置，以便用户可以访问在两个不同的端口上的RADOS网关。

例如，`rgw_frontend`配置可以使`RADOS网关在80/TCP端口上监听，并在443/TCP端口上支持TLS/SSL`

```bash
[ceph: root@node /]# ceph config get client.rgw rgw_frontends 
beast port=80+443s
```

### 使用Beast前端

`RADOS网关` 提供`Beast`嵌入式HTTP服务器作为前端。Beast前端使用`Boost.Beast` 库来做HTTP解析以及使用`Boost.Asio`库来做异步网络I/O,

Beast 是一个 C++ 库，它提供了一个基于 Boost.Asio 的 HTTP 和 WebSocket 协议的实现。Boost.Asio 是一个 C++ 网络库，它提供了异步 I/O 操作的支持，可以用于实现高性能的网络应用程序。

##### Beast的配置选项

通过配置来自证书颁发机构(CA)的证书，以及RGW实例的主机名和匹配的密钥，配置Beast web服务器使用TLS

Beast配置选项通过Ceph配置文件或配置数据库传递给嵌入式web服务器。如果未指定值，则默认值为空

**port and ssl_port**

设置IPv4和IPv6协议的侦听端口号，可以多次指定，如 `port=80 port=8000`

**endpoint and ssl_endpoint**

以`address [:port]`的形式设置侦听地址，可以多次指定，如 `endpoint= [:: 1] endpoint=192.168.0.100:8000`

**ssl_certificate**

指定用于启用SSL的端点的SSL证书文件的路径

**ssl_private_key**

指定 SSL 私钥，但是如果没有提供值，则使用`ssl_certificate`指定的文件作为私钥

**tcp_nodelay**

在某些环境下设置性能优化参数

Red Hat建议使用 `HAProxy和keepalive` 服务在生产环境中配置`TLS/ SSL`访问

### 高可用Proxy和加密

当`RADOS Gateway`业务负载增加时，可以部署更多的RGW实例，以支持业务负载。可以在`单个zone组部署中添加实例`，但是要考虑到每个RGW实例都有自己的IP地址，并且很难在`单个zone中平衡对不同实例的请求`

相反，配置`HAProxy和keepalive`来平衡`RADOS`网关服务器之间的负载。`HAProxy`只提供一个IP地址，它平衡所有RGW实例的请求

`Keepalived`确保代理节点保持相同的呈现IP地址，与节点可用性无关

为`HAProxy和keepa live`服务配置至少两个独立的主机，以保持高可用性

`HAProxy`服务可以配置为使用`HTTPS`协议。要启用HTTPS，需要为配置生成SSL密钥和证书。如果没有来自证书颁发机构的证书，那么请使用自签名证书

#### 服务器端加密

可以启用服务器端加密，以便在无法通过SSL发送加密请求时，允许使用不安全的HTTP向RADOS网关服务发送请求。目前仅在使用Amazon S3 API时支持服务器端加密场景。

有两个选项配置服务器端加密的RADOS网关，`客户提供的密钥或密钥管理服务`

+ `客户提供的密钥`

该选项根据`Amazon SSE-C`规范实现。对RADOS网关服务的每个读或写请求都包含一个用户通过S3客户端提供的加密密钥

一个对象只能用一个密钥加密，用户使用不同的密钥加密不同的对象。跟踪用于加密每个对象的密钥是用户的责任

##### 密钥管理服务

通过配置`密钥管理服务`，可以安全地保存`RADOS Gateway`服务的密钥。当配置密钥管理服务时，RADOS网关服务按需检索密钥，以对对象进行加密或解密。

下图演示了RADOS网关和示例HashiCorp Vault密钥管理服务之间的加密流程

![在这里插入图片描述](https://img-blog.csdnimg.cn/1849f4d6c1ba46b69f7eb4eece326593.png)


目前，已测试的RADOS网关密钥管理服务实现包括`HashiCorp Vault`和`OpenStack Barbican`

## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
