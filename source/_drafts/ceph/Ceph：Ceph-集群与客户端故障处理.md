---
title: Ceph：Ceph 集群与客户端故障处理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-06-24 15:48:39/Ceph：Ceph 集群与客户端故障处理 .html'
mathJax: false
date: 2023-06-24 23:48:39
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


# 集群与客户端故障处理

#### 开始故障排除

支持 Ceph 集群的硬件随着时间的推移会出现故障。集群中的数据变得碎片化，需要维护。应该在集群中执行一致的监视和故障排除，以保持集群处于健康状态

##### 识别问题

在对Ceph问题进行故障排除时，`第一步是确定是哪个Ceph组件导致了问题`。

有时，可以在 ceph 运行状况 detailorceph运行状况status命令提供的信息中找到此组件。其他时候，必须进一步调查以发现问题。验证集群的状态，以帮助确定是单个故障还是整个节点故障

以下故障排除清单建议接下来的步骤:

1. 确定导致问题的Ceph组件
2. 为标识的组件设置调试日志并查看日志
3. 验证拥有一个受支持的配置
4. 确定是否有缓慢或卡住的操作


##### 故障排除集群健康

Red Hat Ceph Storage持续运行各种健康状况检查，以监控集群的健康状况。当运行状况检查失败时，集群运行状况状态将更改为HEALTH_WARN或HEALTH_ERR，具体取决于运行状况检查失败的严重程度和影响。

Red Hat Ceph Storage还将健康检查警告和错误记录到集群日志中。ceph status和ceph health命令显示集群运行状况状态。当集群健康状态为HEAL TH_WARN或HEAL TH_ERR时，使用ceph health detail命令查看健康检查消息，以便可以开始对问题进行故障排除

```bash
[ceph: root@node /]# ceph health detail
```

一些运行状况状态消息指示某个特定问题;其他人则提供了更一般的指示。例如，如果群集运行状况状态更改为HEALTH_ WARN，并且看到运行状况消息HEALTH_WARN 1 osds down Degraded data redundancy，那么这就是问题的明确指示

其他运行状况状态消息可能需要进一步的故障排除，因为它们可能指示几个可能的根本原因。例如，以下消息表示一个问题有多种可能的解决方案

```bash
[ceph: root@node /]# ceph health detail
```

可以通过更改指定池的pg num设置来解决这个问题，或者通过重新配置pg autosca ler模式设置从warn变为on，以便Ceph自动调整pg的数量

当集群性能健康状况检查失败时，Ceph会发送有关性能的健康状况消息。例如，OSD之间通过发送心跳ping消息来监控OSD守护进程的可用性。Ceph还使用OSD的ping响应时间来监控网络性能。一个失败的OSD ping消息可能意味着来自某个特定OSD的延迟，表明该OSD存在潜在的问题。多个OSD的ping消息失败可能是网络组件故障，如OSD主机间网络切换

##### 屏蔽Ceph健康警报

可能希望暂时关闭一些集群警告，因为已经知道它们，而且还不需要修复它们。

例如，如果你关闭一个OSD进行维护，那么集群会报告一个`HEALTH_WARN`状态。可以静音此警告消息，以便健康检查不会影响报告的整体状态。

Ceph通过健康检查码指定健康检查警报。例如，前面的HEALTH_ WARN消息显示了POOL_TOO_FEW_PGS运行状况代码。要使运行状况警报消息静音，请使用ceph health命令

```bash
[ceph: root@node /)# ceph health mute health-code [duration]
```

健康代码是ceph health detail命令提供的代码。可选参数duration是静音运行状况消息的时间，以秒、分钟或小时为单位指定。可以使用ceph health unmute heal th-code取消健康信息的静音，当您健康消息设置静音时，如果健康状态进一步降级，Ceph将自动取消警报的静音。例如，如果集群报告一个OSD故障，将该警报设置为静音，如果另一个OSD故障，Ceph将自动移除静音。任何可测量的运行状况警报都将取消静音

#### 12.3.2 配置日志记录

如果集群的某个特定区域出现问题，那么可以为该区域启用日志记录。
例如，如果osd运行正常，但的元数据服务器没有正常运行，请为特定的元数据服务器实例启用调试日志记录。根据需要为每个子系统启用日志记录。向Ceph配置添加调试通常是在运行时临时完成的。如果在启动集群时遇到问题，可以将Ceph调试日志记录添加到Ceph配置数据库中。查看默认路径“/var/log/Ceph”下的Ceph日志文件。Ceph将日志存储在基于内存的缓存中。

##### 理解Ceph日志

在运行时使用Ceph命令配置Ceph日志记录。如果在启动集群时遇到错误，那么可以更新Ceph配置数据库，以便它在启动期间进行日志记录。

您可以为集群中的每个子系统设置不同的日志记录级别。调试级别在1到20之间，其中1表示简洁，20表示详细。

Ceph不发送基于内存的日志到输出日志，除了以下情况:

1. 一个致命的信号出现了

2. 代码中的断言被触发

3. 你的请求

要对输出日志级别和内存级别使用不同的调试级别，请使用斜杠(/)字符。例如，debug_mon = 1/5设置ceph-mon守护进程的输出日志级别为1，内存日志级别为5

##### 在运行时配置日志

4003151606

要在运行时激活调试输出，请使用ceph tell命令

```bash
[ceph: root@node /]# ceph tell type.id config set debug_subsystem debug-level
```

type和id参数是Ceph守护进程的类型及其id。该子系统为需要修改调试级别的具体子系统

这个例子修改了Ceph组件间消息系统的OSD O调试级别:

```bash
[ceph: root@node /]# ceph tell osd.0 config set debug_ms 5 
```

在运行时查看配置设置如下:

```bash
[ceph: root@node /)# ceph tell osd.0 config show
```

##### 在配置数据库中配置日志

配置子系统调试级别，以便它们在引导时记录到默认日志文件中。使用Ceph config set命令将调试设置添加到Ceph配置数据库中

例如，通过在Ceph配置数据库中设置以下参数，为特定的Ceph守护进程添加调试级别:

```bash
[ceph: root@node /]# ceph config set global debug_ms 1/5 
[ceph: root@node /)# ceph config set osd debug_ms 1 
[ceph: root@node /]# ceph config set osd debug_osd 1/5 
[ceph: root@node /]# ceph config set mon debug_mon 20 
```

##### 设置日志文件轮转

Ceph组件的调试日志是资源密集型的，可以生成大量的数据。如果磁盘几乎满了，那么可以通过修改/etc/logrotate.d/ceph上的日志旋转配置来加速日志轮转，Cron作业调度器使用此文件调度日志轮换

可以在轮转频率之后添加一个大小设置，这样当日志文件达到指定的大小时就会进行轮转:

```bash
rotate 7 
weekly 
size size 
compress 
sharedscripts 
```

使用crontab命令添加一个检查/etc/logrotate.d/ceph文件

```bash
[ceph: root@node /]# crontab -e 
```

例如，可以指示Cron检查/etc/logrotate./ceph每30分钟

```bash
30 * * * * /usr/sbin/logrotate /etc/logrotate.d/ceph > /dev/null 2>&1
```

#### 12.3.3 故障诊断网络问题

Ceph节点使用网络相互通信。当osd被报告为down时，网络问题可能是原因。带有时钟偏差的监视器是网络问题的常见原因。时钟歪斜，或计时歪斜，是同步数字电路系统中的一种现象，在这种现象中，相同的源时钟信号在不同的时间到达不同的元件。如果读数之间的差异与集群中配置的数据相差太远，那么就会出现时钟倾斜错误。该错误可能会导致丢包、延迟或带宽受限，影响集群的性能和稳定性

下面的网络故障排除清单建议下一步步骤:

1. 确保集群中的cluster_network和public_network参数包含正确的值。可以通过使用[ceph config get mon cluster_network]()或[ceph config get mon public_network]()命令检索它们的值，或通过检查ceph.conf文件

2. 检查所有网络接口是否正常

3. 验证Ceph节点，并验证它们能够使用它们的主机名相互连接，如果使用防火墙，确保Ceph节点能够在适当的端口上相互连接。打开适当的端口，如有必要，重新安装端口

4. 验证主机之间的网络连接是否有预期的延迟，并且没有丢包，例如，使用ping命令

5. 连接较慢的节点可能会减慢较快节点的速度。检查交换机间链路是否能够承受已连接节点的累计带宽

6. 验证NTP在集群节点中运行正常。例如，可以查看chronyc tracking命令提供的信息

#### 12.3.4 Ceph客户端故障处理

下面列出了客户端在访问Red Hat Ceph存储集群时遇到的最常见问题:

1. 客户机无法使用监视器(MONs)

2. 使用CLI导致的不正确或缺少命令行参数

3. /etc/ceph/ceph.conf不正确、丢失或不可访问

4. 密钥环文件不正确、丢失或不可访问

ceph-common包为rados、ceph、rbd和radosgw-admin命令提供bash选项卡补全。在shell提示符下输入命令时，可以通过按Tab键来访问选项和属性补全

##### 启用和修改日志文件

在对客户端进行故障诊断时，请提高日志级别

在客户端系统中，可以通过c[eph config set client debug_ms 1]()命令将debug_ms = 1参数添加到配置数据库中。Ceph客户端将调试信息保存在“/var/log/Ceph/Ceph-client.id.log的日志文件

大多数Ceph客户机命令，例如rados、Ceph或rbd，也接受- -debug -ms=1选项，以只执行日志级别增加的命令

##### 启用客户端管理套接字

默认情况下，Ceph客户端在启动时创建一个UNIX域套接字。可以使用此套接字与客户机通信，以检索实时性能数据或动态获取或设置配置参数

在/var/run/ceph/fsid目录中，有该主机的admin套接字列表。允许每个OSD一个管理套接字，每个MON一个套接字，每个MGR一个套接字。管理员可以使用附带- -admin-daemon socket-patch选项的ceph命令查询通过的客户端套接字

```bash
[ceph: root@node /]# sudo ls -al /var/run/ceph/fsid 
```

下面的示例使用FUSE客户端挂载一个CephFS文件系统，获取性能计数器，并将debug_ms配置参数设置为1:

```bash
[root@host ~]# ceph-fuse -n client.admin /mnt/mountpoint

[root@host ~]# ls \
	/var/run/ceph/2ae6d05a-229a-11ec-925e-52540000fa0c
ceph-client.admin.54240.94381967377904.asok
[root@host ~]# ceph --admin-daemon \
	/var/run/ceph/ceph-client.admin.54240.94381967377904.asok \
	perf dump
[root@host ~]# ceph --admin-daemon \
	/var/run/ceph/ceph-client.admin.54240.94381967377904.asok \
	config show
[root@host -]# ceph --admin-daemon \
	/var/run/ceph/ceph-client.admin.54240.94381967377904.asok \
	config set debug_ms 5
[root@host ~]# ceph --admin-daemon \
	/var/run/ceph/ceph-client.admin.54240.94381967377904.asok \
	config show
```

##### 比较Ceph版本和功能

早期版本的Ceph客户端可能无法从已安装的Ceph集群提供的特性中获益。例如，较早的客户机可能无法从erasure-coded池检索数据。因此，在升级Ceph集群时，还应该更新客户机。RADOS网关、用于CephFS的FUSE客户端、librbd或命令行，例如RADOS或RBD，都是Ceph客户端的例子。

在客户端，你可以通过Ceph versions命令找到正在运行的Ceph集群的版本:

```bash
[ceph: root@node /]# ceph versions
```

还可以使用ceph features命令列出支持的特性级别。如果无法升级客户端，[ceph osd set-require-min -compat-client version-name]()指定ceph集群支持的最小客户端版本，使用这个最小的客户端设置，Ceph拒绝使用与当前客户端版本不兼容的特性

使用ceph osd命令验证集群所需的最小版本:

```bash
[ceph: root@node /]# ceph osd get-require-min-compat-client 
luminous 
```

##### 使用Cephx

Red Hat Ceph Storage提供了用于加密认证的cepphx协议。如果启用了Cephx，那么Ceph将在默认的/etc/ceph/路径中查找密钥环。要么为所有组件启用Cephx，要么完全禁用它。Ceph不支持混合设置，比如为客户端启用cepphx，但为Ceph服务之间的通信禁用它。默认情况下，启用了Cephx，当客户端试图访问Ceph集群时，如果没有Cephx，就会收到错误消息

所有Ceph命令都作为客户端进行身份验证。默认为admin用户，但可以使用--name和--ID选项指定用户名或用户ID

Cephx的问题通常与以下方面有关:

1. 对钥匙圈或/etc/ceph/ceph.conf的权限不正确

2. 丢失钥匙圈和/etc/ceph/ceph.conf文件

3. 给定用户的cephx权限不正确或无效。使用ceph认证列表命令来识别问题

4. 不正确或拼写错误的用户名，也可以使用ceph auth list命令来验证

#### 12.3.5 Ceph监视器故障排除

可以通过ceph运行状况详细信息命令或查看ceph日志提供的信息来识别错误消息

以下是最常见的Ceph MON错误消息列表:

**mon.X is down (out of quorum)**

如果Ceph MON守护进程没有运行，则会出现一个错误，阻止该守护进程启动。例如，可能是守护进程有一个损坏的存储，或者/var分区已满。

如果Ceph MON守护进程正在运行，但被报告为关闭，那么原因取决于MON的状态。如果Ceph MON处于探测状态的时间长于预期，那么它就无法找到其他Ceph监视器。这个问题可能是由网络问题引起的，或者Ceph Monitor可能有一个过时的Ceph Monitor map (monmap)试图在不正确的IP地址上到达其他Ceph Monitor。

如果Ceph MON处于e lee ting状态的时间超过预期，那么它的时钟可能不会同步。如果状态从同步变为eleeting，那么这意味着Ceph MON生成映射的速度比同步进程能够处理的速度要快。

如果状态是leader or peon,，那么Ceph Mon已经达到法定人数，但集群的其他成员不承认法定人数。这个问题主要是由时钟故障引起的同步异常、网络故障或NTP同步异常

**clock skew**

这个错误消息表明MON的时钟可能没有被同步。mon_clock_drift_allowed参数控制集群在显示警告消息之前允许的时钟之间的最大差值。主要是由于时钟同步失败、网络故障或NTP同步异常等原因造成的

**mon.X store is getting too big**

当存储太大并延迟对客户端查询的响应时，Ceph MON会显示此警告消息

#### 12.3.6 Ceph OSDs故障处理

使用ceph status命令查看监视器的仲裁。如果群集显示健康状态，则群集可以组成仲裁。如果没有监视器仲裁，或者监视器状态出现错误，请首先解决监视器问题，然后继续验证网络

以下是最常见的Ceph OSD错误消息列表:

- **full osds**

当集群达到mon_osd_full_ratio参数设置的容量时，Ceph返回HEALTH_ERR full osds消息。默认设置为0.95，即集群容量的95%。

使用ceph df命令确定已使用的原始存储的百分比，由% raw used列给出。如果裸存储占比超过70%，则可以删除不必要的数据，或者通过增加OSD来扩展集群来减少裸存储

- **nearfull osds**

当集群达到由mon_osd_nearfull_ratio默认参数设置的容量时，Ceph返回nearfull osds消息。默认值为0.85，即集群容量的85%。

产生此警告消息的主要原因是:

1. 集群OSD间OSD数不均衡

2. 基于OSD数量、用例、每个OSD的目标pg数和OSD利用率，放置组计数不正确

3. 集群使用不成比例的CRUSH可调项

4. osd的后端存储几乎满了

要解决此问题:

1. 验证PG计数是否足够

2. 确认您使用了集群版本最优的CRUSH可调项，如果不是，请调整它们

3. 根据利用率修改osd的权重

4. 确定osd使用的磁盘上剩余的空间

**osds are down**

当osds down或flapping时，Ceph返回osds是down消息。该消息的主要原因是ceph-osd的某个进程故障，或者与其他osd的网络连接出现问题

#### 12.3.7 RADOS网关故障处理

可以排除Ceph RESTful接口和一些常见的RADOS网关问题

##### 调试Ceph RESTful接口

radosgw守护进程是一个Ceph客户端，它位于Ceph集群和HTTP客户端之间。它包括自己的网络服务器Beast，它支持HTTP和HTTPS。

如果发生错误，应该查看/var /log/ceph/文件夹中的日志文件

要将日志记录到文件中，请将log_to_file参数设置为true。可以通过log_file和debug参数更新日志文件的位置和日志级别。还可以在Ceph配置数据库中启用rgw_enable_ops_ log和rgw_ enable_usage_ log参数，分别记录每次成功的RADOS网关操作和使用情况

```bash
[ceph: root@node /]# ceph config set \
	client.rgw log_file /var/log/ceph/ceph-rgw-node.log 
[ceph: root@node /]# ceph config set client.rgw log_to_file true 
[ceph: root@node /]# ceph config set client.rgw debug_rgw 20 
[ceph: root@node /]# ceph config set \
	client.rgw rgw_enable_ops_log true 
[ceph: root@node /)# ceph config set \
	global rgw_enable_usage_log true
```

使用[radosgw-admin log list]()命令查看调试日志。该命令提供可用的日志对象列表。使用[radosgw-admin log show]()命令查看日志文件信息。若要直接从日志对象检索信息，请在对象ID中添加- -object参数。如果要检索具有时间戳的桶的信息，可以添加--bucket、--date和--bucket- ID参数，这些参数分别表示桶名、时间戳和桶ID

##### 常见的RADOS网关问题

在RADOS网关中最常见的错误是客户端和RADOS网关之间的时间倾斜，因为S3协议使用日期和时间来签署每个请求。为了避免这个问题，在Ceph和客户节点上都使用NTP

可以通过在RADOS网关日志文件中查找HTTP状态行来验证RADOS网关请求完成的问题

RADOS网关是一个Ceph客户端，它将所有配置存储在RADOS对象中。保存配置数据的RADOS pg组必须处于active +clean状态。如果状态不是active+clean，那么如果主OSD无法提供数据服务，Ceph I/O请求就会挂起，HTTP客户端最终会超时。使用ceph健康详细信息命令识别未激活的pg

#### 12.3.8 CephFS故障排除

CephFS元数据服务器(MDS)维护一个与其客户端、FUSE或内核共享的缓存，以便MDS可以将其缓存的一部分委托给客户端。
例如，访问inode的客户机可以在本地管理和缓存对该对象的更改。
如果其他客户端也请求访问同一个节点，MDS可以请求第一个客户端用新的元数据更新服务器

为了保持缓存的一致性，MDS需要与客户端建立可靠的网络连接

Ceph可以自动断开或驱逐没有响应的客户端。发生这种情况时，未刷新的客户端数据将丢失

当客户端试图获得对CephFS的访问时，MDS请求具有当前能力的客户端释放它们。如果客户机没有响应，那么CephFS会在超时后显示一条错误消息。可以使用ceph fs set命令使用session_timeout属性配置超时时间。缺省值是60秒

session_autoc丢失属性控制退出。如果客户端与MDS的通信失败时间超过默认的300秒，则MDS将客户端逐出

Ceph暂时禁止被驱逐的客户端，以便他们不能重新连接。如果出现这种禁止，您必须重新引导客户端系统或卸载并重新挂载文件系统才能重新连接













## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
