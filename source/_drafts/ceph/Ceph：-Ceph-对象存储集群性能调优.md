---
title: Ceph： Ceph 对象存储集群性能调优
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-06-24 15:42:09/Ceph： Ceph 对象存储集群性能调优.html'
mathJax: false
date: 2023-06-24 23:42:09
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

### 12.2 对象存储集群性能调优

#### 12.2.1 维护OSD性能

良好的客户端性能要求在其物理限度内使用OSDs。为了保持OSD性能，评估以下调优机会:

1. 调优OSD使用的BlueStore后端，以便将对象存储在物理设备上

2. 调整自动数据擦洗和深度擦洗的时间表

3. 调整异步快照修整(删除已删除的快照)时间表

4. 控制当OSDs失败或添加或替换时，回填和恢复操作发生的速度

#### 12.2.2 在Ceph BlueStore上存储数据

OSD守护进程的默认后端对象存储是BlueStore。以下列表描述了使用BlueStore的一些主要特性:

**直接管理存储设备**

BlueStore消耗原始块设备或分区。这简化了存储设备的管理，因为不需要其他抽象层，比如本地文件系统

**高效的写时复制**

Ceph块设备和Ceph文件系统快照依赖于在BlueStore中高效实现的写时复制克隆机制。这将为常规快照和依赖克隆实现高效两阶段提交的erasure-coded 池带来高效的I/O

**没有大型双重写入**

BlueStore首先将任何新数据写入块设备上未分配的空间，然后提交一个Roeks DB事务，该事务更新对象元数据以引用磁盘的新区域

**多设备支持**

BlueStore可以使用多个块设备来存储数据、元数据和预写日志

在BlueStore中，原始分区以bluestore_min_alloc_size变量指定的大小块管理。对于hdd和ssd，bluestore_min_alloc_size默认设置为4096，相当于4 KB。如果要写入原始分区的数据小于块大小，那么它将被填充为0。如果块大小不适合工作负载(例如编写许多小对象)，则可能会导致浪费未使用的空间

Red Hat建议设置bluestore_min_alloc_size变量来匹配最小的通用写操作，以避免浪费未使用的空间。例如，如果客户端经常写入4 KB的对象，那么在OSD节点上配置设置，例如bluestore_min_alloc_size = 4096。如果之前用bluestore_min_alloc_size_ssd或bluestore_min_a lloc_size_hdd变量设置了bluetore_min_alloc_size_hdd变量，那么设置bluetore_min_alloc_size变量将覆盖HOD或SSD的特定设置

使用ceph config命令设置bluestore_min_alloc_size变量的值:

```bash
[root@node -]# ceph \
	config set osd.ID bluestore_min_alloc_size_device-type value 
```

#### 12.2.3 BlueStore碎片化工具

随着时间的推移，OSD的空闲空间会变得碎片化。分片正常，但分片过多会降低OSD性能。使用BlueStore时，使用BlueStore碎片化工具检查碎片级别。BlueStore碎片化工具生成一个BlueStore OSD的碎片级别分数。碎片化评分在0 ~ 1之间，0为无碎片化，1为重度碎片化。

作为参考，O和0.7之间的值被认为是小的和可接受的碎片，0.7和0.9之间的分数是可观的，但仍然是安全的碎片，高于0.9的分数表明严重的碎片导致性能问题。

使用BlueStore碎片化工具查看碎片评分:

```bash
[root@node -]# ceph daemon osd.ID bluestore allocator score block
```

#### 12.2.4 维护数据一致性和擦洗

osd负责验证数据一致性，使用轻洗净和深洗净。

- 轻擦洗验证对象的存在、校验和和大小。
- 深度擦洗读取数据并重新计算和验证对象的校验和。

默认情况下，Red Hat Ceph Storage每天执行轻度擦洗，每周执行深度擦洗。
但是，Ceph可以在任何时候开始擦洗操作，这可能会影响集群的性能。
可以使用[ceph osd set noscrub]()和[ceph osd unset noscrub]()命令启用或禁用集群级光擦除。
尽管清除操作会影响性能，但Red Hat建议启用该特性，因为它可以维护数据完整性。
Red Hat建议设置刷洗参数，以将刷洗限制在工作负载最低的已知时间段内

默认配置允许在白天的任何时间轻擦洗

##### 轻擦洗

通过在ceph的[osd]部分添加参数来调整轻刷洗过程。
例如，使用osd_scrub_begin_hour参数来设置时间

扫描开始，从而避免在工作负载高峰期间进行轻度扫描。
轻扫描特性有以下调优参数: osd_scrub_begin_hour = begin_ hour:: begin_ hour参数指定开始扫描的时间。有效值从0到23。如果该值设置为0，且osd_scrub_end_hour也为0，则全天都允许擦洗

- **osd_scrub_end_hour = end_hour**
  end hour参数指定停止擦洗的时间。取值范围为0 ~ 23。如果该值设置为0，并且osd_scrub_begin_hour也为0，那么全天都允许扫描。

- **osd_scrub_load_threshold**
  如果系统负载低于阈值(由get loadavg () / number online CPUs参数定义)，则执行擦洗。默认值为0.5

- **osd scrub_min_interval**
  如果负载低于osd scrub_ load_threshold参数中设置的阈值，执行刷洗的频率不超过该参数中定义的秒数。缺省值为1天

- **osd_scrub_interval_randomize_ratio**
  在参数“osd_scrub min interval ”中定义的值上添加一个随机延迟。默认值为0.5

- **osd_scrub_max_interval**
  无论负载如何，在执行擦洗之前，不要等待超过这个时间。缺省值为7天

- **osd_scrub_priority**
  通过该参数设置擦洗操作的优先级。缺省值为5。该值相对于osd_client_op_priority的值，后者的默认优先级更高，值为63

##### 深层擦洗

[ceph osd set nodeep-scrub]()和[ceph osd unset nodeep-scrub]()命令用于在集群级别启用和禁用深度扫描。可以通过将深度扫描参数添加到ceph配置文件的[osd]部分来配置深度扫描参数。与轻擦洗参数一样，对深擦洗配置的任何更改都会影响集群性能。以下参数是调优深度擦洗最关键的参数:

- **osd_deep_scrub_interval**
  深度擦洗的间隔时间。缺省值为7天

- **osd_scrub_sleep**
  在深度刷洗硬盘读取之间引入暂停。增加该值可以降低擦洗操作的速度，并降低对客户端操作的影响。缺省值为0

可以使用一个外部调度程序来实现轻扫描和深扫描，使用以下命令:

[ceph pg dump]()命令在last_SCRUB和last_DEEP_SCRUB列中显示最后一次轻擦洗和深擦洗

[ceph pg scrub pg-id]()命令在特定的pg上安排深度磨砂

[ceph pg deep-scrub pg-id]()命令在一个特定的pg上安排深磨砂

使用[ceph osd pool set pool-name parameter value]()命令设置指定池的参数

##### 池擦写参数

还可以使用这些池参数在池级控制轻擦洗和深擦洗:

- **noscrub**
  如果设置为true, Ceph不会轻擦洗池。默认值为false

- **nodeep-scrub**
  如果设置为true, Ceph不会深度擦洗池。默认值为false

- **scrub_min_interval**
  擦洗的次数不要超过该参数中定义的秒数。
  如果设置为默认0，则Ceph使用osd_scrub_min_interva l全局配置参数
- **scrub_ max_interval**
  在擦洗池之前，等待的时间不要超过该参数中定义的周期。
  如果设置为默认0, Ceph使用osd_scrub_max_interval全局配置参数

- **deep_ scrub_interval**
  深度擦洗的间隔时间。
  如果设置为默认0, Ceph将使用Osd_deep_scrub_interval全局配置参数

#### 12.2.5 裁剪快照和osd

快照在pool和RBD级别都是可用的。
当删除快照时，Ceph将快照数据的删除安排为异步操作，称为快照修整

为了减少快照修整过程对集群的影响，可以在删除每个快照对象后设置暂停。
通过使用osd snap_trim_sleep参数配置此暂停，该参数是允许下一次快照微调操作之前等待的秒数。该参数的默认值为0。请根据实际环境设置，联系红帽技术支持进行设置

使用osd_snap_trim_priority参数控制快照修剪进程，该参数的默认值为5

#### 12.2.6 控制回填和恢复

为了限制回填和恢复操作对集群的影响，保持集群的性能，有必要对回填和恢复操作进行控制

当有新的OSD加入集群，或者当一个OSD死亡，Ceph将其pg重新分配给其他OSD时，就会进行回填。当发生这样的事件时，Ceph会在可用的osd上创建对象副本

当Ceph OSD变得不可访问并恢复在线时，就会发生恢复，例如由于短时间的中断。OSD进入恢复模式，获取最新的数据副本

可以通过以下参数管理回填和回收操作:

- **osd_rnax_backfills**
  控制每个OSD的最大回填次数。缺省值为1

- **osd_recovery_max_active**
  控制每个OSD的最大并发恢复次数。缺省值为3
- **osd_recovery_op_priority**
  设置恢复优先级。取值范围为1 ~ 63。数字越高，优先级越高。缺省值为3







## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
