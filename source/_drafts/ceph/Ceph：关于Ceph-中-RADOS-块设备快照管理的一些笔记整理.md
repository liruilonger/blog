---
title: Ceph：关于Ceph 中 RADOS 块设备快照克隆管理的一些笔记整理
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-05-14 07:53:10/Ceph：关于Ceph 中 RADOS 块设备快照克隆管理的一些笔记整理.html'
mathJax: false
date: 2023-05-14 15:53:10
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

# 管理RADOS块设备快照

## 启用RBD快照和克隆功能

使用RBD格式2的镜像支持几个可选特性。 使用 `rbd feature enable` 或 `rbd feature disable` 命令开启或关闭 `rbd` 镜像 `特性`。

在 `rbd` 池中的镜像上启用了独占锁定的支持特性

```bash
[root@clienta ~]# rbd feature enable  test_pool/test2 exclusive-lock
```

禁用独占锁定的支持特性，使用 `rbd feature disable` 命令:
```bash
[root@clienta ~]# rbd feature disable  test_pool/test2 exclusive-lock

```
需要注意的是，一些特性是不可变的，例如 layering 特性，它只能在创建RBD镜像时启用，无法在后续更改

```bash
[root@clienta ~]# rbd feature enable  test_pool/test1 layering
rbd: failed to update image features: (22) Invalid argument
2023-05-14T04:17:21.304-0400 7fd8fa9ed2c0 -1 librbd::Operations: cannot update immutable features
```
```bash
[root@clienta ~]# rbd create  --size 100M test_pool/test2 --image-feature layering
[root@clienta ~]# rbd feature disable  test_pool/test2 layering
rbd: failed to update image features: (22) Invalid argument
2023-05-14T04:22:24.452-0400 7fc687f7a2c0 -1 librbd::Operations: cannot update immutable features
```



这些是RBD镜像的一些可用特性;

| 名称             | 描述                                          |
| -------------- | ------------------------------------------- |
| layering       | 分层支持以启用克隆                                   |
| striping       | Striping v2支持增强性能，用librbd支持                 |
| exclusive-lock | 独占锁定的支持                                     |
| object-map     | 对象映射支持(需要独占锁)                               |
| fast-diff      | 快速diff命令支持(需要object-map AND exclusive-lock) |
| deep-flatten   | 扁平化RBD镜像的所有快照                               |
| journaling     | 日志记录                                        |
| data- pool     | EC数据池支持                                     |

## RBD 快照

RBD快照是在`特定时间`创建的`RBD镜像`的`只读副本`。

RBD 快照使用 `COW` 技术来减少维护快照所需的存储空间。当集群对 `RBD` 快照镜像发起写 `I/O` 请求时，需要先将原始数据复制到该 `RBD快照`镜像所在位置组的其他区域。

快照在创建时不消耗任何`存储空间`，而是随着它们所包含的对象的大小而增长改变。RBD镜像支持 `增量快照`

```bash
[root@clienta ~]# ceph osd pool ls detail  | grep rbd
pool 6 'rbd' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 32 pgp_num 32 autoscale_mode on last_change 206 flags hashpspool,selfmanaged_snaps stripe_width 0 application rbd
[root@clienta ~]# rbd ls rbd
image1
[root@clienta ~]# rbd info rbd/image1
rbd image 'image1':
        size 128 MiB in 32 objects
        order 22 (4 MiB objects)
        snapshot_count: 0
        id: d39d87b69e3b
        block_name_prefix: rbd_data.d39d87b69e3b
        format: 2
        features: layering, exclusive-lock, object-map, fast-diff, deep-flatten
        op_features:
        flags:
        create_timestamp: Mon May 15 11:24:35 2023
        access_timestamp: Mon May 15 11:24:35 2023
        modify_timestamp: Mon May 15 11:24:35 2023
[root@clienta ~]#
[root@clienta ~]# rbd map --pool rbd image1
/dev/rbd0
[root@clienta ~]# rbd showmapped
id  pool  namespace  image   snap  device
0   rbd              image1  -     /dev/rbd0
[root@clienta ~]#
```
`blockdev --getro /dev/rbd0` 命令用于获取块设备 `/dev/rbd0` 的只读状态。

`blockdev` 命令通常用于设置或获取块设备属性，而 `--getro` 则是用于检索指定设备的只读状态。

运行该命令后，它将返回 0 或 1，其中 0 表示设备未设置为只读，而 1 表示设备已设置为只读。

```bash
[root@clienta ~]# mkfs.xfs /dev/rbd0
meta-data=/dev/rbd0              isize=512    agcount=8, agsize=4096 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1
data     =                       bsize=4096   blocks=32768, imaxpct=25
         =                       sunit=16     swidth=16 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=1872, version=2
         =                       sectsz=512   sunit=16 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
Discarding blocks...Done.
[root@clienta ~]# bl
blkdeactivate  blkid          blkmapd        blkrawverify   blkzone
blkdiscard     blkiomon       blkparse       blktrace       blockdev
[root@clienta ~]# blockdev  --getro /dev/rbd0
0
[root@clienta ~]#
```

在创建快照之前，使用 `fsfreeze` 命令暂停对文件系统的访问。

`fsfreeze --freeze` 命令停止对文件系统的访问，并在磁盘上创建一个稳定的镜像。当文件系统`未冻结`时，不要进行文件系统快照，因为这会破坏快照的文件系统。

```bash
[root@clienta ~]# df -h /dev/rbd0
Filesystem      Size  Used Avail Use% Mounted on
/dev/rbd0       121M  7.8M  113M   7% /mnt/rbd0
[root@clienta ~]# sudo fsfreeze --freeze /mnt/rbd0
```
快照完成后，使用`fsfreeze - -unfreeze`命令恢复对文件系统的操作和访问


`快照COW` 过程在`对象级别`操作，与 RBD镜像的`写I/O请求的大小无关`。如果您向具有快照的RBD镜像写入单个字节，那么 Ceph 将整个受影响的对象从RBD镜像复制到快照区域



如果 RBD 镜像存在快照，则删除RBD镜像失败。使用 `rbd snap purge` 命令删除快照

使用 `rbd snap create` 命令创建 Ceph 设备的快照

```bash
[root@clienta ~]# rbd snap create  rbd/image1@firstsnap
Creating snap: 100% complete...done.
```

使用 `rbd snap ls` 命令列出块设备快照

```bash
[root@clienta ~]# rbd snap ls rbd/image1
SNAPID  NAME       SIZE     PROTECTED  TIMESTAMP
     4  firstsnap  128 MiB             Mon May 15 11:47:54 2023
```

`rbd snap rollback` 命令用于回滚块设备快照，快照中的数据将覆盖当前镜像版本

```bash
[root@clienta ~]# rbd snap  rollback  rbd/image1@firstsnap
Rolling back to snapshot: 100% complete...done.
[root@clienta ~]#
```
`rbd disk-usage` 命令用于获取指定 RBD 镜像的磁盘使用情况信息。

```bash
[root@clienta ~]# rbd disk-usage --pool rbd image1
NAME              PROVISIONED  USED
image1@firstsnap      128 MiB  36 MiB
image1                128 MiB  36 MiB
<TOTAL>               128 MiB  72 MiB
[root@clienta ~]#
```

使用 `rbd snap rm` 命令删除 Ceph 块设备的快照

```bash
[root@node ~]# rbd snap rm pool/image@secondsnap 
Removing snap: 100% complete ... done
```

## RBD 克隆

RBD 克隆是以 RBD `受保护快照`为基础的 RBD 镜像的读写副本。RBD克隆也可以被扁平化，这可以将其转换为独立于源的 RBD 镜像。克隆过程有三个步骤:

1. 创建一个快照
```bash
[root@clienta ~]# rbd snap create  rbd/image1@snapshot
Creating snap: 100% complete...done.
```
2. 保护快照不被删除
```bash
[root@node ~]# rbd snap protect pool/image@snapshot 
```
3. 使用受保护快照创建克隆
```bash
[root@node ~]# rbd clone pool/imagename@snapshotname poollclonename 
[root@clienta ~]# rbd clone  rbd/image1@snapshot rbd/clon1
```
4. 查看克隆信息
```bash
[root@clienta ~]# rbd children rbd/image1@snapshot
rbd/clon1
```


新创建的克隆行为类似于常规的 RBD 镜像，并支持 `COW (Copy-on-Write) 和 COR (Copy-on-Read)`，其中默认为 COW。

COW 的工作原理是，在将写入 I/O 请求应用于克隆之前，先将父快照数据复制到克隆中。因此，当克隆进行修改时，它只会修改与父快照不同的块，而不会修改与父快照相同的块。

这种机制可以减少存储空间的消耗，因为`克隆和父快照之间共享相同的数据块`。但是，由于需要在写入 I/O 之前复制数据，因此可能会影响性能。COR 与之相反，它是在读取 I/O 请求时执行数据复制操作，以`保证克隆和父快照之间的数据隔离`。


可以因此`启用COR支持RBD克隆`。与父RBD快照和克隆快照相同的数据直接从父RBD快照读取。如果父节点的osd相对于客户端有较高的延迟，这可能会使读取更昂贵。COR将对象第一次读取时复制到克隆

如果启用了 COR, Ceph会在处理读I/O请求之前将数据从父快照复制到克隆中，如果数据还没有出现在克隆中。通过运行`ceph config set client rbd_c lone_copy_on_read true`命令或`ceph config set global rbd_clone_copy_on_read true`命令来激活COR特性，不覆盖原始数据

如果在 RBD 克隆上禁用了 COR，克隆不能满足的每一个读操作都会向克隆的父节点发出I/O请求

![](https://k8s.ruitong.cn:8080/Redhat/CL260-RHCS5.0-en-1-20211117/images/block/device-clone02.svg)

克隆`COW和COR`过程在`对象级`操作，不管请求的I/O大小。要读取或写入RBD克隆的单个字节，Ceph将整个对象从父镜像或快照复制到克隆中

使用rbd命令管理rbd克隆

| 命令                                                                        | 描述    |
| ------------------------------------------------------------------------- | ----- |
| rbd children [pool-name/]image-name@snapshot-name                         | `列出克隆`  |
| rbd clone [pool-name/]parent-image@snap-name [pool-name/]child-image-name | `创建克隆`  |
| rbd flatten [pool-namel]child-image-name                                  | `扁平化克隆` |

当`扁平化`一个克隆时，`Ceph` 将所有缺失的数据从父副本复制到克隆中，然后删除对父副本的引用。克隆将成为一个独立的RBD镜像，并且不再是受保护快照的子快照

`不能直接从池中删除RBD镜像`。相反，使用`rbd trash mv`命令将镜像从池中移动到垃圾中。使用`rbd trash rm`命令从垃圾中删除对象。可以将克隆正在使用的活动镜像移动到垃圾中，以便稍后删除

## 导入和导出RBD镜像

```bash
[root@clienta ~]# ceph osd pool create  rbd 32 32
pool 'rbd' created
[root@clienta ~]# ceph osd pool application enable  rbd rbd
enabled application 'rbd' on pool 'rbd'
[root@clienta ~]# rbd pool init -p rbd
[root@clienta ~]# rbd ls
[root@clienta ~]#
[root@clienta ~]# rbd create  test --size 128 --pool rbd
[root@clienta ~]# rbd ls
test
[root@clienta ~]# rbd map  --pool rbd test
/dev/rbd1
```
```bash
[root@clienta ~]# mkfs.xfs /dev/rbd1
meta-data=/dev/rbd1              isize=512    agcount=8, agsize=4096 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=1, sparse=1, rmapbt=0
         =                       reflink=1
data     =                       bsize=4096   blocks=32768, imaxpct=25
         =                       sunit=16     swidth=16 blks
naming   =version 2              bsize=4096   ascii-ci=0, ftype=1
log      =internal log           bsize=4096   blocks=1872, version=2
         =                       sectsz=512   sunit=16 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
Discarding blocks...Done.
[root@clienta ~]# mkdir /mnt/rbd
mkdir: cannot create directory ‘/mnt/rbd’: File exists
[root@clienta ~]# mount /dev/rbd1 /mnt/rbd
[root@clienta ~]# mount | grep rbd
/dev/rbd0 on /mnt/rbd0 type xfs (rw,relatime,seclabel,attr2,inode64,logbufs=8,logbsize=64k,sunit=128,swidth=128,noquota)
/dev/rbd1 on /mnt/rbd type xfs (rw,relatime,seclabel,attr2,inode64,logbufs=8,logbsize=64k,sunit=128,swidth=128,noquota)
[root@clienta ~]#
```

```bash

```

### 导入和导出RBD镜像

`RBD 导出和导入机制`允许维护 RBD 镜像的可操作副本，在同一个集群中或通过使用单独的集群实现功能完整且可访问的。可以将这些副本用于各种用例，包括:

1. 使用真实的数据量测试新版本
2. 使用真实的数据量运行质量保证流程
3. 使用真实的数据量实现业务连续性场景
4. 将备份流程与生产块设备解耦

RADOS块设备特性提供了导出和导入`整个RBD镜像`或仅RBD镜像在`两个时间点之间`变化的能力

#### 导出RBD镜像

Ceph 提供了`rbd export`命令来导出rbd镜像到文件中。该命令将RBD镜像或RBD镜像快照导出到指定的目标文件中。rbd export命令的语法如下:

```bash
rbd export [--export-format {1|2}) (image-spec | snap-spec) [dest-path] 
```

`--export-format` 选项指定导出数据的格式，允许将较早的RBD格式1镜像转换为较新的格式2镜像。下面的示例将一个名为test的RBD镜像导出到/tmp/test.dat文件

```bash
[ceph: root@node /]# rbd export rbd/test /tmp/test.dat
```

#### 导入RBD镜像

`Ceph` 提供了 `rbd import` 命令，用于从文件中导入`rbd`镜像。该命令创建一个新镜像，并从指定的源路径导入数据。`rbd import` 命令的语法如下:

```bash
rbd import [--export-format {1|2}] [--image-format format-id] [--object-size size-in-B/K/M] [--stripe-unit size-in-B/K/M --stripe-count num] [--image-feature feature-name] ... [--image-shared] src-path [image-spec]
```

`--export-format`选项指定导入数据的数据格式。当导入format 2导出的数据时，使用 `--stripe-unit， --stripe-count， --object-size`和`--image-feature`选项创建新的`RBD format 2 image`

`--export-format`参数值必须与相关rbd匹配RBD导入命令


```bash
[root@clienta ~]# rbd export rbd/test  /mnt/export.dat
Exporting image: 100% complete...done.
[root@clienta ~]# rbd import  /mnt/export.dat rbd/tests
Importing image: 100% complete...done.
[root@clienta ~]# rbd --pool rbd ls
test
tests
[root@clienta ~]#
```

### 导出和导入RBD镜像的变化

Ceph 提供`rbd export-diff`和`rbd import-diff`命令来导出和导入`rbd`镜像上两个时间点之间的更改。语法与rbd export、rbd import命令相同

时间端点可以是:

1. RBD镜像的当前内容，如`poolname/imagename`
2. RBD镜像的快照，例如`poolname/imagename@snapname`

开始时间包括:

1. RBD镜像的创建日期和时间。例如，不使用 `--from-snap`选项
2. RBD镜像的快照，例如使用`--from-snap snapname`选项获取

如果指定了起始点快照，该命令将在创建该快照后导出所做的更改，如果不指定快照，该命令将导出自创建以来的所有更改RBD镜像，与常规的RBD镜像导出操作相同

`import-diff` 操作执行以下有效性检查:

1. 如果`export-diff`是相对于一个开始快照，那么这个快照也必须存在于目标RBD镜像中
2. 如果使用export-diff命令时指定了一个结束快照，则导入数据后在目标RBD镜像中创建相同的快照名称

#### 导出和导入过程的管道

将破折号(-)字符指定为导出操作的目标文件将导致输出转到标准输出(stdout)。

还可以使用`破折号字符(-)`指定标准输出或标准输入(stdin)作为导出目标或导入源。可以将两个命令管道到一个命令中

```bash
[ceph: root@node /]# rbd export rbd/img1 - | rbd import - bup/img1
```

`rbd merge-diff`命令将两个连续增量的`rbd export-diff`镜像操作的输出合并到一个目标路径上。`该命令一次只能处理两条增量路径`

```bash
[ceph: root@node /]# rbd merge-diff first second merged 
```

要在一个命令中合并两个以上的`连续增量路径`，可以将一个`rbd export-diff`输出管道到另一个rbd export-diff命令。

使用破折号字符(-)作为管道前的命令中的目标，并作为管道后的命令中的源。

例如，可以将`三个增量差异合并到一个命令行上的单个合并目标中`。前一个`export-diff`命令快照的结束时间必须等于后一个export -diff命令快照的开始时间

```bash
[ceph: root@node /]# rbd merge-diff first second - | rbd merge-diff - third merged
```

`rbd merge-diff`命令只支持 `stripe-count`为1的rbd镜像















## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***
<https://docs.ceph.com/>

<https://access.redhat.com/documentation/zh-cn/red_hat_ceph_storage/5>

CL260 授课老师课堂笔记


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
