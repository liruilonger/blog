---
title: SO-VITS-SVC部署环境搭建
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-06-06 07:01:49/SO-VITS-SVC部署环境搭建.html'
mathJax: false
date: 2023-06-06 15:01:49
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


docker run -v $(pwd)/output:/output deezer/spleeter:3.8-5stems separate test.mp3 -o /output





## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
