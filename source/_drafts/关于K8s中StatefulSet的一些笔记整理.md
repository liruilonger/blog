---
title: 关于K8s中StatefulSet的一些笔记整理
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: java
uniqueId: '2022-11-01 12:12:10/关于K8s中StatefulSet的一些笔记整理.html'
mathJax: false
date: 2022-11-01 20:12:10
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

StatefulSet 是用来管理有状态应用的工作负载 API 对象。

StatefulSet 用来管理某 Pod 集合的部署和扩缩， 并为这些 Pod 提供持久存储和持久标识符。

和 Deployment 类似， StatefulSet 管理基于相同容器规约的一组 Pod。但和 Deployment 不同的是，StatefulSet 为它们的每个 Pod 维护了一个有粘性的 ID。这些 Pod 是基于相同的规约来创建的， 但是不能相互替换：无论怎么调度，每个 Pod 都有一个永久不变的 ID。

如果希望使用存储卷为工作负载提供持久存储，可以使用 StatefulSet 作为解决方案的一部分。 尽管 StatefulSet 中的单个 Pod 仍可能出现故障， 但持久的 Pod 标识符使得将现有卷与替换已失败 Pod 的新 Pod 相匹配变得更加容易。


### 使用 StatefulSet 


StatefulSet 对于需要满足以下一个或多个需求的应用程序很有价值：

+ 稳定的、唯一的网络标识符。
+ 稳定的、持久的存储。
+ 有序的、优雅的部署和扩缩。
+ 有序的、自动的滚动更新。

在上面描述中，“稳定的”意味着 Pod 调度或重调度的整个过程是有持久性的。 如果应用程序不需要任何稳定的标识符或有序的部署、删除或扩缩， 则应该使用由一组无状态的副本控制器提供的工作负载来部署应用程序，比如 Deployment 或者 ReplicaSet 可能更适用于你的无状态应用部署需要。


### 限制
+ 给定 Pod 的存储必须由 `PersistentVolume Provisioner` 基于所请求的 `storage class` 来制备，或者由管理员预先制备。
+ 删除或者扩缩 StatefulSet 并不会删除它关联的存储卷。 这样做是为了保证数据安全，它通常比自动清除 StatefulSet 所有相关的资源更有价值。
+ StatefulSet 当前需要无头服务来负责 Pod 的网络标识。你需要负责创建此服务。
+ 当删除一个 StatefulSet 时，该 StatefulSet 不提供任何终止 Pod 的保证。 为了实现 StatefulSet 中的 Pod 可以`有序且体面地终止，可以在删除之前将 StatefulSet 缩容到 0`。
+ 在默认 Pod 管理策略(OrderedReady) 时使用滚动更新， 可能进入需要人工干预才能修复的损坏状态



### StatefulSet 创建

对应 SVC 创建
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cat headless.yaml
apiVersion: v1
kind: Service
metadata:
  name: web-headless
  labels:
    app: nginx_headless
spec:
  ports:
  - port: 30088
    targetPort: 80
    name: nginx-web-headless
  clusterIP: None
  selector:
    app: web-headless
```
对应 statefulSet 创建
```yaml

```



## 博文参考


