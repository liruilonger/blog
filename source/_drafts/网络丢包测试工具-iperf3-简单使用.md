---
title: 网络丢包测试工具 iperf3 简单认知
tags:
  - iperf3
categories:
  - iperf3
toc: true
recommend: 1
keywords: iperf3
uniqueId: '2023-04-28 08:54:24/网络丢包测试工具 iperf3 简单使用.html'
mathJax: false
date: 2023-04-28 16:54:24
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ `iperf3` 是一种网络性能测试工具
+ 工作中用到，这里简单介绍
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

### iperf3

`iperf3` 是一种网络性能测试工具，可以用于测试`带宽、吞吐量` 等指标。使用iperf3进行性能测试的基本步骤：


启动iperf3服务器。在要测试的计算机上启动iperf3服务器，以便接受客户端请求。在命令行中执行以下命令：

```bash
iperf3 -s
```

该命令将把iperf3服务器启动在默认TCP端口（5201）上，并开始监听客户端连接请求。

在另一个终端窗口中启动iperf3客户端，并连接到服务器。在要测试的另一台计算机上，在命令行中执行以下命令：

```bash
iperf3 -c <server_ip_address>
```

其中，"<server_ip_address>"是iperf3服务器的IP地址。这将启动iperf3客户端，并尝试连接到指定的服务器。

进行性能测试。一旦客户端与服务器建立连接，性能测试即可开始。iperf3默认使用TCP协议进行测试，测试时间为10秒。可以使用各种选项和参数来调整测试过程和输出结果。

```bash
⬢[root@toolbox ~]#  iperf3  -c 192.168.26.1
Connecting to host 192.168.26.1, port 5201
[  5] local 192.168.26.6 port 41866 connected to 192.168.26.1 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  5]   0.00-1.00   sec  2.67 GBytes  23.0 Gbits/sec  3946    401 KBytes
[  5]   1.00-2.00   sec  2.64 GBytes  22.6 Gbits/sec  4408    263 KBytes
[  5]   2.00-3.00   sec  2.68 GBytes  23.0 Gbits/sec  4027    428 KBytes
[  5]   3.00-4.00   sec  2.83 GBytes  24.3 Gbits/sec  3662    381 KBytes
[  5]   4.00-5.00   sec  2.86 GBytes  24.6 Gbits/sec  3212    414 KBytes
[  5]   5.00-6.00   sec  2.79 GBytes  24.0 Gbits/sec  4233    372 KBytes
[  5]   6.00-7.00   sec  2.80 GBytes  24.1 Gbits/sec  4195    428 KBytes
[  5]   7.00-8.00   sec  2.61 GBytes  22.4 Gbits/sec  4121    465 KBytes
[  5]   8.00-9.00   sec  2.76 GBytes  23.7 Gbits/sec  4708    519 KBytes
[  5]   9.00-10.00  sec  2.55 GBytes  21.9 Gbits/sec  4095    445 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-10.00  sec  27.2 GBytes  23.4 Gbits/sec  40607             sender
[  5]   0.00-10.00  sec  27.2 GBytes  23.3 Gbits/sec                  receiver

iperf Done.
```

这是iperf3的测试结果，其中包括：

```bash
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  5]   0.00-1.00   sec  2.67 GBytes  23.0 Gbits/sec  3946    401 KBytes
[  5]   1.00-2.00   sec  2.64 GBytes  22.6 Gbits/sec  4408    263 KBytes
```

+ ID：iperf3测试任务的标识符。
+ Interval：测试时间间隔。
+ Transfer：传输数据量。
+ Bitrate：传输速率。
+ Retr：重传操作次数。
+ Cwnd：TCP拥塞窗口大小。

这两行结果显示了2个1秒钟的测试时间间隔内的性能指标。第一个时间间隔中，传输了2.67GBytes的数据，速率为23.0Gbits/sec，并执行了3946次重传操作。TCP拥塞窗口大小为401KBytes。在第二个时间间隔中，传输了2.64 GBytes的数据，速率为22.6 Gbits/sec，并执行了4408次重传操作。TCP拥塞窗口大小为263 KBytes。通过这些指标，可以了解网络带宽和连接质量等信息。


```bash
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-10.00  sec  27.2 GBytes  23.4 Gbits/sec  40607             sender
[  5]   0.00-10.00  sec  27.2 GBytes  23.3 Gbits/sec                  receiver

```
+ ID： 测试流的唯一标识符。
+ Interval： 测试的时间间隔，以秒为单位。
+ Transfer： 在测试过程中传输的总字节数。
+ Bitrate： 传输速率，以比特每秒（bps）为单位。
+ Retr： 在测试期间发生的重传次数。
+ Sender： 表示此行所列出的结果来自iperf3客户端。
+ Receiver： 表示此行所列出的结果来自iperf3服务器。

根据上述iperf3测试的结果，从192.168.26.6到192.168.26.1的连接中，传输速率大约是23.4 Gbits/sec。整个测试持续了10秒钟，期间发送了27.2GB的数据，共进行了40607次重传操作。这个速度非常快，表明网络质量较好，可用于高速数据传输。




需要注意的是，iperf3也支持其他协议和选项，例如UDP、多线程并发测试和带宽限制等。您可以参考官方文档或手册，以了解更多关于iperf3的用法和示例。


-s： 启动iperf3服务器，并等待客户端连接请求。

iperf3 -s
-c <server_ip_address>： 建立与指定服务器的连接，并进行性能测试。可以使用"-u"选项来执行UDP测试。

iperf3 -c 10.0.0.1
-t <duration>： 指定测试的时间长度，以秒为单位。默认值为10秒。

iperf3 -c 10.0.0.1 -t 30
-P <num_threads>： 指定并发连接数。每个连接将在单独


指定总包大小

```bash
⬢[root@toolbox ~]#  iperf3  -c 192.168.26.1 -i 40Gbits
Connecting to host 192.168.26.1, port 5201
[  5] local 192.168.26.3 port 36474 connected to 192.168.26.1 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  5]   0.00-10.00  sec  26.0 GBytes  22.4 Gbits/sec  38517    424 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-10.00  sec  26.0 GBytes  22.4 Gbits/sec  38517             sender
[  5]   0.00-10.00  sec  26.0 GBytes  22.4 Gbits/sec                  receiver

iperf Done.
```
指定每秒的包大小
```bash
⬢[root@toolbox ~]#  iperf3  -c 192.168.26.1 -b 40Gbits
Connecting to host 192.168.26.1, port 5201
[  5] local 192.168.26.3 port 45604 connected to 192.168.26.1 port 5201
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd
[  5]   0.00-1.00   sec  2.65 GBytes  22.7 Gbits/sec  4347    514 KBytes
[  5]   1.00-2.00   sec  2.77 GBytes  23.8 Gbits/sec  3378    564 KBytes
[  5]   2.00-3.00   sec  2.78 GBytes  23.8 Gbits/sec  2974    516 KBytes
[  5]   3.00-4.00   sec  2.68 GBytes  23.0 Gbits/sec  2967    558 KBytes
[  5]   4.00-5.00   sec  2.64 GBytes  22.7 Gbits/sec  4959    379 KBytes
[  5]   5.00-6.00   sec  2.61 GBytes  22.4 Gbits/sec  3439    459 KBytes
[  5]   6.00-7.00   sec  2.80 GBytes  24.1 Gbits/sec  4277    562 KBytes
[  5]   7.00-8.00   sec  2.66 GBytes  22.9 Gbits/sec  3901    523 KBytes
[  5]   8.00-9.00   sec  2.76 GBytes  23.7 Gbits/sec  3455    519 KBytes
[  5]   9.00-10.00  sec  2.86 GBytes  24.6 Gbits/sec  3459    467 KBytes
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-10.00  sec  27.2 GBytes  23.4 Gbits/sec  37156             sender
[  5]   0.00-10.00  sec  27.2 GBytes  23.4 Gbits/sec                  receiver

iperf Done.
⬢[root@toolbox ~]#
```



## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
