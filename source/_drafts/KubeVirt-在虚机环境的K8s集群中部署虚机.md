---
title: 'KubeVirt:在虚拟机环境下的 K8s集群中部署虚拟机'
tags:
  - KubeVirt
categories:
  - KubeVirt
toc: true
recommend: 1
keywords: KubeVirt
uniqueId: '2023-01-18 22:54:39/"KubeVirt:在虚机环境的K8s集群中部署虚机".html'
mathJax: false
date: 2023-01-19 06:54:39
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 需要嵌套虚拟化
+ 理解不足小伙伴帮忙指正

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

在虚机中我们可以搭建虚机，参加过红帽考试的小伙伴应该不陌生，红帽的学习环境都是虚拟里面部署的虚机。

KubeVirt 是 Kubernetes 的虚拟化插件

为什么使用 VM 而不是容器？

容器非常适合受信任的工作负载，因为它们非常快并且可以共享内存等宝贵资源。但是在同一主机上运行的两个容器仍然共享相同的 Linux 内核，并且总是存在各种内核 CVE。

VM 提供了更好的安全性和封装性，但速度较慢且占用更多资源。如果您允许不受信任的用户在您的基础架构内运行应用程序，那么您(在许多其他措施中)需要一个硬封装层，因此需要 VM。

虚拟机如何与 KubeVirt 一起工作
KubeVirt 提供 K8s 资源，例如VirtualMachineInstance. 创建此资源将导致自动创建 Pod 和连接的 VM。这太棒了，因为这意味着我们可以(大部分)像对待普通 Pod 一样对待 VM。

但这到底是怎么回事呢？因为假设我们有虚拟机，所以我们在这些虚拟机上安装 Kubernetes。在 Kubernetes 内部，我们安装了 KubeVirt。KubeVirt 将在哪里创建像 Pod 一样对待的新虚拟机？让我们看看下面的图片：


一个集群

kubectl客户端实用程序

Kubernetes apiserver 必须有--allow-privileged=true才能运行 KubeVirt 的特权 DaemonSet。

```bash
┌──[root@vms100.liruilongs.github.io]-[~/back]
└─$cat /etc/kubernetes/manifests/kube-apiserver.yaml | grep allow-p
    - --allow-privileged=true
```

KubeVirt 需要硬件虚拟化支持，这样就可以在 VM 上创建嵌套的 VM。大多数专用服务器以及 GCP 等云提供商都可以进行嵌套虚拟化。

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$ egrep '(vmx|svm)' /proc/cpuinfo
flags           : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon rep_good nopl xtopology tsc_reliable nonstop_tsc eagerfpu pni pclmulqdq vmx ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch tpr_shadow vnmi ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid avx512f avx512dq rdseed adx smap avx512ifma clflushopt clwb avx512cd sha_ni avx512bw avx512vl xsaveopt xsavec xgetbv1 arat avx512vbmi avx512_vpopcntdq
flags           : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp lm constant_tsc arch_perfmon rep_good nopl xtopology tsc_reliable nonstop_tsc eagerfpu pni pclmulqdq vmx ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch tpr_shadow vnmi ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid avx512f avx512dq rdseed adx smap avx512ifma clflushopt clwb avx512cd sha_ni avx512bw avx512vl xsaveopt xsavec xgetbv1 arat avx512vbmi avx512_vpopcntdq
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$ egrep '(vmx|svm)' /proc/cpuinfo
```
### 部署 KubeVirt

KubeVirt 可以使用 KubeVirt operator 安装，它管理所有 KubeVirt 核心组件的生命周期。

```bash
# Point at latest release
$ export RELEASE=$(curl https://storage.googleapis.com/kubevirt-prow/release/kubevirt/kubevirt/stable.txt)
# Deploy the KubeVirt operator
$ kubectl apply -f https://github.com/kubevirt/kubevirt/releases/download/${RELEASE}/kubevirt-operator.yaml
# Create the KubeVirt CR (instance deployment request) which triggers the actual installation
$ kubectl apply -f https://github.com/kubevirt/kubevirt/releases/download/${RELEASE}/kubevirt-cr.yaml
# wait until all KubeVirt components are up
$ kubectl -n kubevirt wait kv kubevirt --for condition=Available
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$export RELEASE=$(curl https://storage.googleapis.com/kubevirt-prow/release/kubevirt/kubevirt/stable.txt)
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$echo $RELEASE
v0.58.0
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$curl -o kubevirt-operator.yaml https://github.com/kubevirt/kubevirt/releases/download/${RELEASE}/kubevirt-operator.yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl apply  -f kubevirt-operator.yaml
namespace/kubevirt unchanged
....
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$curl -o kubevirt-cr.yaml  https://github.com/kubevirt/kubevirt/releases/download/${RELEASE}/kubevirt-cr.yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl apply  -f kubevirt-cr.yaml
kubevirt.kubevirt.io/kubevirt configured
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl -n kubevirt wait kv kubevirt --for condition=Available
kubevirt.kubevirt.io/kubevirt condition met
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$
```


```bash

```

默认情况下，KubeVirt 将部署 7 个 pod、3 个服务、1 个守护进程、3 个部署应用程序、3 个副本集。
### Virtctl 安装


ubeVirt 提供了一个名为virtctl的附加二进制文件，用于快速访问 VM 的串行和图形端口，并处理启动/停止操作。

安装
virtctl可以从 KubeVirt github 页面的发布页面获取。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$VERSION=$(kubectl get kubevirt.kubevirt.io/kubevirt -n kubevirt -o=jsonpath="{.status.observedKubeVirtVersion}")
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$ARCH=$(uname -s | tr A-Z a-z)-$(uname -m | sed 's/x86_64/amd64/') || windows-amd64.exe
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$echo ${ARCH}
linux-amd64
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$echo ${VERSION}
v0.59.0-alpha.2
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$curl -L -o virtctl https://github.com/kubevirt/kubevirt/releases/download/${VERSION}/virtctl-${VERSION}-${ARCH}
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$chmod  +x virtctl
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$ls
kubevirt-cr.yaml  kubevirt-operator.yaml  virtctl
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$mv virtctl kubectl-virt
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$mv kubectl-virt /usr/local/bin/
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl virt version
Client Version: version.Info{GitVersion:"v0.59.0-alpha.2", GitCommit:"6f97adc2fd3144cb9813363e3bb147c39c2260af", GitTreeState:"clean", BuildDate:"2023-01-03T16:07:04Z", GoVersion:"go1.19.2", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{GitVersion:"v0.59.0-alpha.2", GitCommit:"6f97adc2fd3144cb9813363e3bb147c39c2260af", GitTreeState:"clean", BuildDate:"2023-01-03T16:07:04Z", GoVersion:"go1.19.2", Compiler:"gc", Platform:"linux/amd64"}
```

### KubeVirt 使用

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$wget --no-check-certificate https://kubevirt.io/labs/manifests/vm.yaml

```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl delete  -f vm.yaml -n kubevirt
virtualmachine.kubevirt.io "testvm" deleted
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl apply  -f vm.yaml -n kubevirt
virtualmachine.kubevirt.io/testvm created
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl get vms -n kubevirt
NAME     AGE   STATUS    READY
testvm   11s   Stopped   False
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl-virt start testvm -n kubevirt
VM testvm was scheduled to start
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl get vms -n kubevirt
NAME     AGE   STATUS     READY
testvm   40s   Starting   False
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$
```


```bash
# Start the virtual machine:
kubectl patch virtualmachine testvm --type merge -p  '{"spec":{"running":true}}'

# Stop the virtual machine:
kubectl patch virtualmachine testvm --type merge -p   '{"spec":{"running":false}}'
```







## 博文参考

***
https://kubevirt.io/user-guide/operations/basic_use/

https://itnext.io/kubevirt-on-killercoda-on-kubevirt-3bc02f7eac7f

https://kubevirt.io/labs/kubernetes/lab1