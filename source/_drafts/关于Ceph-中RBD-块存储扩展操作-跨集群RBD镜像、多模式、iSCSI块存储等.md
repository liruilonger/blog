---
title: 关于Ceph 中RBD 块存储扩展操作(跨集群RBD镜像、多模式、iSCSI块存储等)
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-06-13 02:58:18/关于Ceph 中RBD 块存储扩展操作(跨集群RBD镜像、多模式、iSCSI块存储等).html'
mathJax: false
date: 2023-06-13 10:58:18
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***



# 扩展块存储操作

## 配置RBD镜像

### RBD镜像

Ceph 支持`两个存储集群之间`的`RBD镜像`，这里听着有些拗口，Ceph 中，使用块设备之前，需要创建一个镜像。这里镜像也做动词，单纯指镜像操作，即两个集群的RBD镜像互为镜像，即可以理解为`A集群镜像是B集群镜像实时更新的副本`

Ceph 允许自动将 `RBD 镜像`从一个 `Ceph 集群` 复制到另一个远程集群。使用异步机制在网络上实现 `镜像源(主)RBD镜像`和`目标(次)RBD镜像`之间的数据一致。

如果包含主RBD镜像的集群不可用，那么可以从远程集群故障转移到辅助RBD镜像，并重新启动使用它的应用程序。

`当从源RBD镜像故障转移到镜像RBD镜像时，必须降级源RBD镜像，提升目标RBD镜像`。一个被降级且被锁定不可用，并且提升后的镜像在读/写模式下变得可用和可访问。

RBD镜像特性需要`RBD-mirror`守护进程，`rbd-mirror`守护进程从远程对等集群提取镜像更新，并将它们应用到本地集群镜像

#### 支持的镜像配置

RBD 镜像支持两种配置:

`单向镜像(active-passive)`

在单向模式下，一个集群的RBD镜像以读/写模式可用，远程集群包含镜像。`镜像代理`在`远程`集群上运行。该模式允许配置`多个备用集群`

![在这里插入图片描述](https://img-blog.csdnimg.cn/5eb67cb0fe9e4de6927678f42ccde24b.png)

`双向镜像(active-active)`

在双向模式下，Ceph同步源和目标对((primary and secondary)，此模式`只允许在两个集群之间进行复制`，并且必须在`每个`集群上`配置镜像代理`。

![](https://k8s.ruitong.cn:8080/Redhat/CL260-RHCS5.0-en-1-20211117/images/block/RBD_mirror_two-way_diagram.svg)

### 支持的镜像(Mirroring)模式

`RBD镜像`支持两种模式:`pool`模式和`image`模式

+ `pool模式`:在池模式下，Ceph自动为镜像池中创建的每个RBD镜像启用镜像。当在源集群的池中创建镜像时，Ceph会在远程集群上创建辅助镜像。

+ `image模式`：在镜像模式中，可以有选择地为镜像池中的各个RBD镜像启用镜像。在这种模式下，必须显式地选择要在两个集群之间复制的RBD镜像。

### RBD异步镜像模式

两个 Ceph 集群间`异步镜像`的RBD镜像有以下两种模式:

+ `基于日志的镜像`

这种模式使用RBD日志镜像特性来确保两个 Ceph 集群之间的时间点和崩溃一致性复制。在修改实际的镜像之前，对RBD镜像的每次写入都首先记录到关联的日志。远程集群读取此日志，并将更新重播到镜像的本地副本

+ `基于快照的镜像`

基于快照的镜像通过定时调度或手动创建的RBD镜像镜像快照，在两个 Ceph 集群之间复制崩溃一致性RBD镜像。远程集群决定两个镜像之间的任何数据或元数据更新快照并将增量复制到镜像的本地副本。

`RBD fast-diff image`特性可以快速确定更新的数据块，而不需要扫描整个RBD镜像。在故障转移场景中使用之前，必须同步两个快照之间的完整增量。任何部分应用的增量集都将在故障转移时回滚。

### 管理复制

+ `镜像重新同步`

如果两个对等集群之间的状态不一致，`rbd-mirror`守护进程不会尝试`mirror`不一致的镜像，使用`rbd mirror image resync`重新同步镜像。

```bash
[ceph: root@node /]# rbd mirror image resync mypool/myimage 
```

+ `启用和禁用镜像mirror`

使用`rbd mirror image enable`或`rbd mirror image disable`在两个对端存储集群上的整个池`image模式`中启用或禁用mirroring模式。

```bash
[ceph: root@node /]# rbd mirror image enable mypool/myimage
```

+ `使用基于快照的镜像`

使用基于快照的镜像，通过禁用镜像和启用快照，将基于日志的镜像转换为基于快照的镜像

```bash
[ceph: root@node /]# rbd mirror image disable mypool/myimage 
Mirroring disabled
```

```bash
[ceph: root@node /]# rbd mirror image enable mypool/myimage snapshot 
Mirroring enabled
```

### 配置RBD mirror

作为存储管理员，可以通过在红帽Ceph存储集群之间镜像数据镜像来提高冗余。Ceph块设备镜像提供了防止数据丢失的保护，比如站点故障。

为了实现RBD镜像，并使`RBD -mirror`守护进程发现它的对等体集群，必须有一个注册的对等体和创建的用户帐户。 `Ceph  5`使用`rbd mirror pool peer bootstrap create`命令自动完成这个过程。

`rbd-mirror` 守护进程的每个实例必须同时连接到本地和远程 Ceph 集群。另外，网络在两个数据中心之间必须有足够的带宽来处理镜像工作负载

+ `逐步配置RBD镜像`

`rbd-mirror`守护进程不需要源集群和目标集群具有惟一的内部名称;两者都可以并且应该称自己为ceph。`rbd mirror pool peer boot st rap`命令利用`--site-name`选项来描述`rbd-mirror`守护进程使用的集群。

下面列出了在两个集群之间配置镜像所需的步骤，分别称为`prod`和`backup`:

1. 在两个集群prod和backup中创建名称相同的池
2. 创建或修改RBD镜像以启用`exclusive- lock`和`journaling`
3. 在池上启用`池模式mirroring`或`镜像模式mirroring`
4. 在prod集群中，`bootstrap`存储集群`peer`并保存`bootstrap token`
5. 部署`rbd-mirror`守护进程。
   1. 对于`单向`复制，rbd-mirror守护进程只在备份集群上运行
   2. 对于`双向`复制，rbd-mirror守护进程在两个集群上运行
6. 在备份集群中，导入`bootstrap token`
   1. 对于单向复制，使用`--direction rx-only`参数

+ `逐步配置单向池模式`

在本例中，将看到使用prod和backup集群配置单向镜像所需的详细说明

```bash
[admin@node -]$ ssh admin@prod-node 
[admin@prod-node -]# sudo cephadm shell --mount /home/admin/token/ 
[ceph: root@prod-node /]# ceph osd pool create rbd 32 32 
pool ' rbd' created 
[ceph: root@prod-node /]# ceph osd pool application enable rbd rbd 
enabled application ' rbd' on pool ' rbd' 
[ceph: root@prod-node /]# rbd pool init -p rbd 
[ceph: root@prod-node /]# rbd create my-image --size 1024 --pool rbd --image-feature=exclusive-lock,journaling 
[ceph: root@prod-node /]# rbd mirror pool enable rbd pool 
[ceph: root@prod-node /]# rbd - -image my-image info 
rbd image 'my-image ': 
  size 1 GiB in 256 objects 
  order 22 (4 MiB objects) 
  snapshot_ count: 0 
  id: acf674690a0c 
  block_name_prefix: rbd_ data.acf674690a0c 
  format : 2 
  features: exclusive- lock, journaling 
  op_ features: 
  flags: 
  create_timestamp: Wed Oct 6 22:07:41 2021 
  access_timestamp: Wed Oct 6 22:07:41 2021 
  modify_ timestamp: Wed Oct 6 22:07:41 2021 
  journal: acf674690a0c 
  mirroring state: enabled 
  mirroring mode: journal 
  mirroring global id: d1140b2e-4809-4965-852a-2c21d181819b
  mirroring primary: true 
```

```bash
[ceph: root@prod-node /]# rbd mirror pool peer bootstrap create --site-name prod rbd > /mnt/bootstrap_token_prod 
[ceph: root@prod-node /]# exit 
exit 
[root@prod-node -]# rsync -avP /home/admin/token/bootstrap_token_prod backup-node:/home/admin/token/bootstrap_token_prod 
. . . output omitted ... 
[root@prod-node -]# exit 
logout 
```
backup集群配置
```bash
[admin@node -]$ ssh admin@backup-node 
[root@backup-node -]# cephadm shell --mount /home/admin/token/ 
[ceph: root@backup-node /]# ceph osd pool create rbd 32 32
pool ' rbd ' created 
[ceph: root@backup-node /]# ceph osd pool application enable rbd rbd 
enabled application ' rbd' on pool 'rbd' 
[ceph: root@backup-node /]# rbd pool init -p rbd 
[ceph: root@backup-node /]# ceph orch apply rbd-mirror --placement=backup-node.example.com 
Scheduled rbd-mirror update ... 
[ceph: root@backup-node /]# rbd mirror pool peer bootstrap import --site-name backup - -direction rx-only rbd /mnt/bootstrap_token prod
[ceph: root@backup-node /]# rbd -p rbd ls 
my-image
```

backup集群显示以下池信息和状态

```bash
[ceph: root@backup-node /]# rbd mirror pool info rbd 
Mode: pool 
Site Name: backup 
Peer Sites: 
UUID: 5e2f6c8c-a7d 9-4c59-8128-d5c8678f9980 
Name: prod 
Direction: rx-only 
Client: client.rbd-mirror-peer 
[ceph: root@backup-node /]# rbd mirror pool status 
health: OK 
daemon health: OK 
image health: OK 
images: 1 total 
  1 replaying
```

prod cluster显示以下池信息和状态

```bash
[ceph: root@prod-node /]# rbd mirror pool info rbd 
Mode: pool 
Site Name: prod 
Peer Sites: 
UUID: 6c5f860c-b683-44b4-9592-54c8f26ac749 
Name: backup 
Mirror UUID: 7224dlc5-4bd5-4bc3-aa19-e3b34efd8369 
Direction: tx-only 
[ceph: root@prod-node /]# rbd mirror pool status 
health: UNKNOWN 
daemon health: UNKNOWN 
image health: OK 
images: 1 total 
  1 replaying
```

在单向模式下，源集群不知道复制的状态。目标集群中的RBD镜像代理更新状态信息

+ `故障转移过程`

如果 `主RBD镜像` 不可用，可以通过以下步骤`启用对备RBD镜像`的访问:

1. 停止对主RBD镜像的访问。这意味着停止使用镜像的所有应用程序和虚拟机
2. 使用`rbd mirror image demote pool-name/image-name`命令降级 `rbd` 主镜像
3. 使用`rbd mirror image promote pooI-name/image-name`命令提升 `rbd` 副镜像
4. 恢复对RBD镜像的访问。重新启动应用程序和虚拟机

当发生非有序关闭后的故障转移时，必须从`备份存储集群`中的`Ceph Monitor节点`提升`非主镜像`。使用`--force`选项，因为`降级无法传播到主存储集群`

## 提供iSCSI块存储

### 描述Ceph iSCSI网关

`Ceph  5` 可以对存储在集群中的`RADOS块设备`镜像提供高可用的`iSCSI访问`。

iSCSI协议允许客户端(启动器)通过TCP/IP网络向存储设备(目标器)发送SCSI命令。每个启动器和目标器都由一个iSCSI限定名称(iSCSI qualified name, IQN)唯一标识。使用标准iSCSI启动器的客户端可以访问集群存储，而不需要本地Ceph RBD客户端支持。

`LinuxI/O` 目标内核子系统运行在每个iSCSI网关上，以支持iSCSI协议。SCSI目标子系统以前被称为LIO，现在被称为TCM，或目标核心模块。TCM子系统利用一个用户空间传递(TCMU)与Ceph librbd库交互，将RBD镜像公开给iSCSI客户端

+ `特定于iSCSI的OSD调优`

osd (Object Storage Devices)和MONs (monitor)不需要任何与iscsi相关的服务器设置。主要是为了`限制客户端SCSI超时时间`，减少集群用于检测故障OSD的延迟超时设置

在cephadm shell中，运行`ceph tell <daemon_type>.<id> config set`命令设置超时参数

```bash
[root@node -]# ceph config set osd osd_heartbeat_interval 5 
[root@node -]# ceph config set osd osd_heartbeat_grace 20 
[root@node -]# ceph config set osd osd_client_watch_timeout 15 
```

+ `部署iSCSI网关`

可以将iSCSI网关部署在专用节点上，也可以与osd节点同时部署。部署 `Ceph  iSCSI`网关前，需要满足以下前提条件:

1. 请安装Red Hat Enterprise Linux 8.3及以上版本的iSCSI网关节点
2. 有一个运行的集群，运行 Ceph  5或更高版本
3. 为iSCSI网关节点上作为目标公开的每个RBD镜像提供90个RAM MiB
4. 在每个Ceph iSCSI节点的防火墙上开放TCP端口3260和5000
5. 创建一个新的RADOS块设备或使用一个现有的可用设备

**创建配置文件**

部署iSCSI网关节点时，使用`cephadm shell`创建一个`/etc/ceph/`目录下名为`iscsi-gateway.yaml`的配置文件。文件应该显示如下:

```yaml
service_type: iscsi 
service_id: iscsi 
placement: 
  hosts: 
    - serverc.lab.example.com 
    - servere.lab.example.com 
spec: 
  pool: iscsipooll 
  trusted_ip_list: "172.25.250.12,172.25.250.14" 
  api_port: 5000 
  api_secure: false 
  api_user: admin 
  api_password: redhat
```

**应用规格文件和部署iSCSI网关**

使用ceph orch apply命令通过使用-i选项来使用规范文件来实现该配置

```bash
[ceph: root@node /]# ceph orch apply -i /etc/ceph/iscsi-gateway.yaml 
Scheduled iscsi.iscsi update ... 
```

**列出网关并验证它们是否存在**

```bash
[ceph: root@node /]# ceph dashboard iscsi-gateway-list 
{"gateways": {"serverc.lab.example.com": {"service url": "http:// 
admin:redhat@172.25.250.12:5000"}, "servere.lab.example.com": {"service_ url": 
"http://admin: redhat@172.25.250.14:5000"}}} 
```

打开一个 web 浏览器，并以具有管理权限的用户身份登录到`Ceph Dashboard`。在“Ceph Dashboard”界面，单击“Block-->iSCSI”，进入“iSCSI Overview”界面。

![](https://k8s.ruitong.cn:8080/Redhat/CL260-RHCS5.0-en-1-20211117/images/block/gui-dashboard-iscsi-overview.png)

配置Ceph Dashboard访问iSCSI网关api后，使用Ceph Dashboard管理iSCSI目标。使用Ceph仪表板来创建、查看、编辑和删除iSCSI目标

### 配置iSCSI Target

使用`Ceph Dashboard`或`ceph-iscsi gwcli`实用程序配置`iSCSI`目标

这些是从`Ceph`仪表板配置`iSCSI`目标的示例步骤。

1. 登录Dashboard。
2. 在导航菜单中单击“Block➔iSCSI”
3. 单击“Targets”页签
4. 在“创建”列表中选择“创建”
5. 在“创建目标”窗口中，设置以下参数:
   1. 修改Target IQN(可选)
   2. 单击“+添加portal”，选择至少两个网关中的第一个
   3. 单击“+添加image”，选择需要导出的image
   4. 单击“创建目标”

### 配置iSCSI启动器

配置iSCSI启动器与Ceph iSCSI网关通信与任何行业标准iSCSI网关相同。对于RHEL 8，需要安装iscsi-initiator-utils和device-mapper-multipath软件包。iscsi-initiator-uti ls包中包含配置iSCSI启动器所需的实用程序。当使用多个iSCSI网关时，可以通过集群的iSCSI目标配置支持多路径的客户端在网关之间进行故障转移。

系统可以通过多个不同的通信路径访问相同的存储设备，无论这些路径使用的是Fibre Channel、SAS、iSCSI还是其他技术。多路径允许配置一个虚拟设备，使其可以使用任何这些通信路径来访问存储。如果其中一条路径出现故障，系统会自动切换到其他路径。如果部署单个iSCSI网关进行测试，则无需配置多路径。

在本示例中，配置iSCSI启动器使用多路径支持和登录iSCSI目标器。配置客户端的CHAP用户名和密码登录iSCSI目标器。

1. 安装iSCSI启动器工具

```bash
[root@node -]# yum install iscsi-initiator-utils
```

2. 配置多路径I/O
   
   1. 安装多路径工具
      
      ```bash
      [ root@node -]# yum install device-mapper-multipath
      ```
   
   2. 启用并创建默认多路径配置
      
      ```bash
      [root@node -]# mpathconf --enable --with_multipathd y 
      ```
   
   3. 在/etc/multipath.conf添加如下内容
      
      ```bash
      devices { 
          device { 
              vendor                 "LIO-ORG" 
              hardware handler       "1 alua" 
              path grouping policy   "failover" 
              path_selector          "queue-length 8" 
              failback                60
              path_checker            tur 
              prio                    alua
              prio_ args              exclusive_pref_bit 
              fast_ io _fail_tmo      25
              no_path_retry           queue
          } 
      } 
      ```
   
   4. 重新启动multipathd服务
      
      ```bash
      [root@node -]# systemctl reload multipathd
      ```

3. 如果需要进行配置，设置CHAP认证

在/etc/iscsi/iscsid.conf中更新CHAP用户名和密码

```bash
node.session.auth.authmethod = CHAP 
node.session.auth.username = user 
node.session.auth.password = password 
```

4. 发现目标并登录iSCSI portal，查看目标及其多路径配置
   1. 发现iSCSI portal
      ```bash
      [root@node -)# iscsiadm -m discovery -t st -p 10.3.0.210 
      10.30.0.210:3260, 1 iqn.2001-07.com.ceph: 1634089632951 
      10.30.0.133:3260, 2 iqn.2001-07.com.ceph: 1634089632951 
      ```
   
   2. 登录iSCSI portal
      ```bash
      [root@node -)# iscsiadm -m node -T iqn.2001-07.com.ceph:1634089632951 -l 
      Logging in to [iface: default, target :iqn.2001-07.com.ceph:1634089632951, portal: 10.30.0.210,3260] 
      Logging in to [iface: default, target : iqn.2001-07.com.ceph :1634089632951, portal: 10.30.0.133,3260] 
      Login to [iface: default, target : iqn.2001-07.com.ceph:1634089632951, portal: 10.30.0.210,3260] successful. 
      Login to [iface: default, target: iqn.2001-07.com.ceph:1634089632951, portal: 10.30.0.133,3260] successful.
      ```
   
   3. 验证任何附加的SCSI目标
      ```bash
      [root@node -]# lsblk 
      NAME MAJ :MIN RM SIZE RO TYPE MOUNTPOINT 
      sda 8:0 0 lG 0 disk 
      Lmpatha 253:0 0 lG 0 mpath 
      sdb 8:16 0 lG 0 disk 
      Lmpatha 253:0 0 lG 0 mpath 
      vda 252 :0 0 10G 0 disk
      ```
   4. 使用multipath命令显示在故障转移配置中设置的设备，每个路径都有一个优先级组
      ```bash
      
      ```










## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
