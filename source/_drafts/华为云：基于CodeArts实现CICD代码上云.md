---
title: 华为云:基于华为云 CodeArts 实现CICD代码上云(原华为云软开云平台)
tags:
  - 华为云
categories:
  - 华为云
toc: true
recommend: 1
keywords: 华为云
uniqueId: '2023-02-24 23:18:23/华为云：基于CodeArts实现CICD代码上云.html'
mathJax: false
date: 2023-02-25 07:18:23
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 博文内容为使用 华为云平台 `CodeArts` 实现 CICD 项目上云的Demo
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***



使用华为云CodeArts实现20分钟一行代码上云该实验旨在指导用户在短时间内熟悉并利用软件开发生产线CodeArts快速搭建项目，并部署在弹性云服务器ECS上，供用户进行访问。


购买弹性云服务器该步骤将创建并配置一台ECS，用于后续应用的部署。

![在这里插入图片描述](https://img-blog.csdnimg.cn/22f650082add4616b7b2ebb5146b499b.png)


点击右上角“购买弹性云服务器”，配置参数如下：① 计费方式：按需计费，② 区域：华北-北京四，③ 可用区：可用区1，如下图所示


⑥ 镜像：公共镜像，镜像类型：CentOS，镜像版本：CentOS 7.6 64bit(40GB)， ⑦ 安全防护：勾选免费开启主机安全基础防护 ⑧ 系统盘：高IO，40GB， 如下图所示：


点击“下一步：网络配置”，配置参数如下：① 网络：vpc-hce，② 扩展网卡：默认，③ 安全组：sg-hce，如下图所示：


④ 弹性公网IP：现在购买，⑤ 线路：全动态BGP，⑥ 公网带宽：按带宽计费，⑦ 带宽大小：2M，如下图所示：


点击“下一步：高级配置”，参数如下：① 云服务器名称：ecs-Linux，② 登录凭证：密码，③ 用户名：root，④ 密码：自定义，如hC8iUA0UAs7V%hcj⑤ 云备份：暂不购买，如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/4907a7399a6d44a3ae70a276855cc8e6.png)


3. 使用软件开发生产线CodeArts搭建软件开发项目该步骤将在软件开发生产线CodeArts上搭建一个软件开发项目，并完成代码仓库，代码检查，编译构建，发布，部署等软件开发操作。



3.1. 新建项目进入华为云“控制台”，鼠标移动到页面左侧菜单栏，点击服务列表->“开发与运维”->“软件开发生产线CodeArts”进入，如下图：


è½¯ä»¶å¼åå¹³å°DevCloud


![在这里插入图片描述](https://img-blog.csdnimg.cn/22f650082add4616b7b2ebb5146b499b.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/1e869d93d23b48c097406c06e4c4f47d.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/982a6cdc26f04493b62d53f01e141ed8.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/30d252f8e93b4beebc3658d826e6201d.png)


![在这里插入图片描述](https://img-blog.csdnimg.cn/638920adcda04894b116ee64a6eb3249.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/563786dde2044a4f97b5a06a1ddb78ff.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e4f2aa5a23264b1f8378732647a66f4b.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/100164d756ea4d7ea3626e0238453ec2.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e76e9d7f369f41878bc8d486dea6c04f.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/66604635508d4600bbd48731b425ed9c.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/982978ba720f49bebe3a8c3e37caa3ca.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/4d4d977d426743b8ab9c58801a177853.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e3631ad6989a4968b99372eea6269edf.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/6b2bf34749ec4d219cd561d811b6e89f.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/1e0d7e1bb6694d5b954d84a6bbdf07e8.png)


3.1.1.开通CodeArts服务(若已开通请跳过此步骤)在CodeArts控制台，左侧菜单栏切换到“总览”，选择点击CodeArts “基础版”的“免费开通”按钮。

![在这里插入图片描述](https://img-blog.csdnimg.cn/1b76e39c91d349928c6617e817c5ec11.png)

3.3. 创建配置代码仓库在当前主机组管理页面，点击顶部菜单的“服务”-> “代码托管”，进入代码托管页面，如下图所示：


![在这里插入图片描述](https://img-blog.csdnimg.cn/a2a4ab2d520f4f868da2a1e5e7fb74a6.png)

进入“按模版新建”页面后，选择左侧菜单栏的“自动创建流水线”->“是”，其他默认，如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/4f5ed46800114d9c8b4fe4fa5dafb543.png)
进入“按模版新建”页面后，选择左侧菜单栏的“自动创建流水线”->“是”，其他默认，如下图所示：

在“按模版新建”页面右侧的模版列表中选择名为“Java Web Demo”的官方代码仓库模板，如下图所示：

![在这里插入图片描述](https://img-blog.csdnimg.cn/1f98ef7f8422449b99add3071830793a.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/b8dfda470240478d98c90ef2a181fe94.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/d2d2822eb5674b5da7b8e0802b0568ba.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/e3e4a9bbb3f54c5387381381221dc94c.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/aeb7c1a9263642adb6b3268d72f25ee8.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/6142766528af496ea7ddb03e412c3b2c.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/d78cd3f1ab294e9e9b9d6ee5d5c826ec.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/59388b1113ea49ecbf4920abd0fadbc0.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/95766046aded48cd8803cee44bcdd448.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/26527e586f6d41179d5c2ab35f1fb1cf.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/b2765a5b228c46e68fb1e38169e2ad8f.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/dfeaaa7f33414018bee5e41d24834288.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/cc2c63856d624f2cba1986a2111326b7.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/7df444d67ac2469e8b9a4e3d22858c9a.png)


![在这里插入图片描述](https://img-blog.csdnimg.cn/d0d61af97bff4d319b9752ceba150d6e.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/6e84ef778fb24d04a11cb94a8b9feafb.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/6561cbe4fc334cf2810f9d2fe644aa19.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/ded02b778a3046f9bcad0587875027ca.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/167ae5eb541f40ff841296c5eaabb4f0.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/83e4a021104a4c3ca4a8281841b9bbc0.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/7b7cd9ba69fb44a881ab4e16608d5310.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/b60fc3b332d244fc9442e03597561cd4.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/a9bb7903efe54c94afc3125b95b36ffc.png)
4. DevOps持续构建，迭代演进该步骤将会通过代码提交触发流水线自动执行的方式，自动化完成代码检查，编译构建，发布，部署任务的执行。4.1. 配置流水线回到“部署”服务页面，点击顶部菜单的“服务”->“流水线”，进入流水线服务页面，如下图所示
5：

![在这里插入图片描述](https://img-blog.csdnimg.cn/aaa9629778a04d059828d549fedd8754.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/105a6c801dbb4c1a82a918ba9903118b.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/2b653f1238014e2f92cb2d151596c72e.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/f93151441c4240ffad81c9a579e97c44.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/04ec4b47aec545f5bf1bd1bebf463430.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/229e29cc016740f9a133f1d77d2a6b15.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/5e4cf602fa3345299002722ed305e5e3.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/f0a199c365c04c9d9287e2c5ac42485f.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/8ffe26758f6e4c709f916077299b3328.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/c3b5bff5731f431bb36437fd8d184cef.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/776cefce509148c48ed300730a39b9f5.png)


## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
