---
title: K8s面试系列：常用API资源总结速记
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-02-14 09:10:18/K8s：面试系列：常用API资源总结.html'
mathJax: false
date: 2023-02-14 17:10:18
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***
### Kubernetes API概述


https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.26/


在 k8s 体系中，把涉及部署环境，自动化运维相关所有行为、实体抽象为 API 对象，用对应的 API 对象来表示相关的容器化应用部署行为，或者叫动作实体。

比如抽象出 `Pod` API 来表示一组在共享网络(IP)、存储和计算资源的环境下运行的容器，这里实际上是抽象了多个容器化应用部署在一个逻辑主机的环境中。通过 抽象出 `Service` API 来表示一个高可用的服务，不通的 Service 类型对应的不通的服务发布方式，。通过抽象 `deploy` API 来描述一个 高可用集群环境的 部署行为，通过  `ReplicaSet`  来表示一个任何时候都能保持副本数不变的集群的行为。


在 K8s中 一个常见的  API 对象由下面四部分构成：

+ 资源种类和版本 `ResourceKind、Version`: 这是关于 API 资源的类型和版本说明

```yaml
apiVersion: v1
kind: Pod
```
+ 资源对象元数据 `ResourceMeta`: 这是关于资源的元数据，名称、所属命名空间、注释和标签，从属关系等。

```yaml
metadata:
  creationTimestamp: null
  labels:
    run: pod-static
  name: pod-static
  namespeace: default
```
+ 资源对象规格参数 `ResourceSpec`: 这是由用户定义的，描述系统的期望状态。在创建或更新一个对象时填写这个。
```yaml
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-demo
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
......
```
+ 资源对象状态 `ResourceStatus`: 这是由服务动态生成的，用于描述API对象当前状态。在大多数情况下，用户不需要改变这个。
```yaml
status:
  phase: Running
  conditions:
    - type: Initialized
      status: 'True'
      lastProbeTime: null
      lastTransitionTime: '2023-02-18T14:46:54Z'
    - type: Ready
      status: 'True'
      lastProbeTime: null
      lastTransitionTime: '2023-02-18T14:54:04Z'
    - type: ContainersReady
      status: 'True'
      lastProbeTime: null
      lastTransitionTime: '2023-02-18T14:54:04Z'
    - type: PodScheduled
      status: 'True'
      lastProbeTime: null
      lastTransitionTime: '2023-02-11T04:11:46Z'
  hostIP: 192.168.26.105
  podIP: 10.244.169.101
  podIPs:
    - ip: 10.244.169.101
  startTime: '2023-02-11T04:11:46Z'
  ..................
```


## Kubernetes API资源类别



k8s API  资源种类在整体上可以分为四个部分

+ 工作负载 API 资源： 工作负载资源负责管理和运行集群上的容器。一般通过控制器来实现。
+ 服务 API资源: 用来将工作负载 组合为 成一个外部可访问的、负载平衡的服务的对象。
+ 配置和存储 API 资源： 用来将初始化数据注入到你的应用程序和持久化容器外部数据的对象。
+ 元数据 API 资源：是你用来配置集群内其他资源的行为的对象。
+ 集群 API 资源： 是定义如何配置集群本身的对象；这些通常只由集群操作员使用。




### 工作负载资源

工作负载资源负责 `管理和运行集群中的容器`。`容器` 是由 `控制器` 通过 `Pod` 创建的。`Pods` 运行容器并提供环境依赖，如共享网络环境持久性存储卷和配置或注入容器的秘密数据。

常见的控制器资源：

+ `Deployments`：用于无状态的持久性应用(如HTTP服务器)。它可以实现 `Pod` 和 `ReplicaSets` 的声明式更新。
+ `StatefulSets`: 用于有状态的持久化应用(如数据库)。 为 Pod 提供持久存储和持久标识符
+ `Jobs/CronJob`: 用于(周期)运行完成的应用程序的Job(如批处理Job)。
+ `DaemonSet`：用于节点守护应用,确保全部(或者某些)节点上运行一个 `Pod` 的副本。
+ `ReplicaSet`：ReplicaSet 的目的是维护一组在任何时候都处于运行状态的 Pod 副本的稳定集合。 
+ `ReplicationController`: 在k8s之前的版本用于无状态应用部署，现在推荐使用 `deployment`


其他资源类型:

+ `Container`: 单个容器,容器只能在Pod 中创建，通常通过 控制器来完成，单个 Pod 中的容器会在共同调度下，于同一位置运行在相同的节点上。
+ `Pod`: 简单讲，Pod为一组容器，这组容器共享存储，网络，命名空间以及运行容器的一些声明，是在 K8s 中创建管理的最小的可部署的计算单元， Pod 所建模的是特定于应用的 “逻辑主机”。


### 服务资源

服务资源负责将你的工作负载拼接成一个可访问的负载均衡服务。默认情况下，工作负载只能在集群内访问，而且必须使用 `LoadBalancer`或 `NodePort`服务在外部公开。

常见的资源类型：

+ `Service`：用于在多个工作负载副本中提供`单一ip端点`负载平衡。并且提供相同的访问方式。
+ `Ingress`: 简单讲，类似一个反向代理，可以理解为 Haproxy 或者 Nginx ，一个规则集合，对集群中服务的外部访问进行管理的 API 对象，允许入站连接到后端定义某个端点。提供一个https(s)端点路由到一个或多个服务。

其他资源类型：
+ `Endpoints`: 是实现实际服务的后端Pod端点的集合。
+ `EndpointSlice`:代表实现服务的后端Pod端点的一个子集。当 `Endpoints` 超过100个，会形成新的 EndpointSlice 
IngressClass 表示 Ingress 的类别，由 Ingress Spec 引用。
+ `Ingress controller`:Ingress 资源工作，集群必须有一个正在运行的 Ingress 控制器。
+ `IngressClass`: 指定实现的Ingress Controller 名称，建立Ingress 和  Ingress controller 之间的关系。

### 配置和存储资源

配置和存储资源负责将数据注入你的应用程序并将数据从外部持久化到你的容器。

常见的资源类型。

ConfigMap持有供pod使用的配置数据。它提供文本键值对，通过环境变量、命令行参数或文件注入到应用程序中。
Secrets保存某种类型的秘密数据。它提供二进制数据，通过文件注入到应用程序中。
Volumes用于提供容器外部的文件系统。可能在同一Pod内的不同容器之间共享，并具有超越容器或Pod的持久性。
其他资源类型。

PersistentVolumeClaim(PVC)是用户对持久化卷的请求和要求。
PersistentVolume(PV)是一个由管理员提供的存储资源。
StorageClass描述了一个可以动态配置PersistentVolumes的存储类别的参数。
CSIDriver 捕获有关部署在集群上的容器存储接口(CSI)卷驱动器的信息。


### 策略资源
政策资源是指





## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***



***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
