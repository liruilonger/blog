---
title: OKD 多网络配置笔记
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-04-24 10:17:10/OKD 多网络配置笔记.html'
mathJax: false
date: 2023-04-24 18:17:10
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


在 Kubernetes 中，容器网络被委派给实现 Container Network Interface (CNI) 的网络插件


OpenShift Container Platform 使用 Multus CNI 插件来串联 CNI 插件

集群安装过程中:
+ 配置 `default pod` 网络,默认网络处理集群中的所有一般网络流量。
+ 基于可用的 CNI 插件定义额外网络，并将一个或多个此类网络附加到 pod。为集群定义多个额外网络


要将额外网络接口附加到 pod，您必须创建配置来定义接口的附加方式。

您可以使用 `NetworkAttachmentDefinition` 自定义资源（CR）来指定各个接口。各个 CR 中的 CNI 配置定义如何创建该接口。


OpenShift Container Platform 中的额外网络
+ bridge
+ host-device
+ ipvlan
+ macvlan
+ SR-IOV


配置额外网络



## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
