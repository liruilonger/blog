title: Ansible最佳实践之Ansible Tower
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-08-22 17:16:27/Ansible最佳实践之Ansible Tower.html'
mathJax: false
date: 2022-08-23 01:16:27
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

可视化仪表板
远程命令执行
基于角色的访问控制(RBAC)
凭证管理
图形化管理inventory
集中记录和审核
作业调度
集成通知
实时和历史作业状态报告
多剧本工作流程
用户触发的自动化
RESTful API

为何选择红帽 Ansible Tower正确编写的Ansible Playbook 可以供不同的团队使用，但Ansible 没有提供用于管理其共享访问的功能。此外，尽管playbook可能支持复杂任务的委派，但这些任务的执行可能需要高级别特权和高度保护的管理员凭据。
IT团队首选的工具集通常会有所不同。有些团队可能选择直接执行playbook，而有些团队可能希望从现有的持续集成和交付工具套件触发playbook执行。此外，通常使用基于GUl的工具来工作的人可能会发现Ansible的纯命令行界面令人生畏且难以使用。

Ansible Tower 简化了与共享Ansible基础架构相关的管理，同时通过引入各种功能来维持组织的安全性，如用于管理 playbook的集中式Web界面、基于角色的访问控制(RBAC)以及集中式日志记录和审计。RESTAPI确保Ansible Tower 与企业的现有工作流和工具集轻松集成。Ansible Tower API和通知功能让Ansible Playbook与Jenkins、红帽CloudForms 或红帽卫星等其他工具的关联变得特别简单，从而实现持续集成和部署。它提供了相应的机制，可以实现集中使用和控制计算机凭据及其他机密，而不必将它们公开给Ansible Tower 最终用户。




## 红帽 Ansible Tower 架构
红帽 Ansible Tower 是一款 Django Web 应用，可在 Linux 服务器上作为企业内自托管解决方案运行，架设于企业的现有 Ansible 基础架构基础上。


Ansible Tower 将其数据存储在 PostgreSQL 后端数据库中，同时使用RabbitMQ 消息传递系统。

根据企业的需求，可以使用以下架构部署Ansible Tower：

+ 单机版(allin-one)，这是标准架构。所有Ansible Tower组件，Web前端，RESTful API后端和PostgreSQL数据库都位于一台计算机上。
+ 单机版+远程数据库，Ansible Tower Web UI和RESTfulAPl后端安装在单台计算机上，而PostgreSQL数据库安装在另一台服务器上。
+ 高可用性集群，早期的Ansible Tower版本提供了一种冗余的主备体系结构，该体系结构由一个主节点和一个或多个备节点组成.从Red Hat Ansible Tower 3.1开始，此体系结构现在被双活高可用性集群所取代。
+ 远程数据库`OpenShift Pod`，集群运行在`OpenShit容器`中，通过添加和删除pod缩放Ansible Tower。


内存要求：
>Red Hat Ansible Tower主机最小4G内存。
>实际的内存需求取决于Red Hat Ansible Tower并行配置的最大主机数。红帽建议为每个并行主机提供100MB的内存，为Red Hat Ansible Tower服务提供2GB的内存。
>Postgres数据库最小4G内存。

存储要求：
+ Red Hat Ansible Tower至少需要20GB的硬盘空间，并且/var目录必须有10GB的可用空间，IOPS不低于750。
+ Postgres数据库至少需要20GB的硬盘空间，IOPS不低于10001，所有tower数据都存储在数据库中。数据清单储随着托管主机数量，运行的作业数量，事实缓存数量以及任何单个作业中的任务数量而增加。

>数据库，PostgreSQL versionn9.6.X运行Ansible Tower3.2及之后版本。
Ansible Engine，Ansible version 2.2(at minimum)运行Ansible Tower versions3.2及之后版本。


SELinux，Red Hat Ansible Tower仅支持SELinux target策略，可以将其设置为强制模式、允许模式或禁用模式。不支持其他SELinux策略。
>托管主机，Ansible管理的系统应满足使用Red Hat Ansible Tower服务器上安装的Ansible版本进行管理的计算机的要求。


红帽 Ansible Tower 许可和支持
有兴趣评估红帽 Ansible Tower 的管理员可以免费获得试用版许可证。如需如何开始的说明，请参
见 https://www.ansible.com/tower-trial
自 2019 年 6 月 1 日起，红帽 Ansible Tower 包含在简化的红帽 Ansible 自动化订阅内。此订阅包括对红帽
Ansible Tower 和红帽 Ansible Engine 的⽀持，以及对与网络设备和服务器相关的用例的支持。
Ansible Tower 组件许可
红帽 Ansible Tower 利用各种各样的软件组件，其中一些可能根据不同的开源许可来提供。
/usr/share/doc/ansible-tower ，目录下提供了这些特定组件各自的许可证。
红帽 Ansible Tower 安装程序
Ansible Tower 提供两种不同的安装软件包：
存档较小，但是需要互联网连接，以便从不同的软件包存储库下载 Ansible Tower 软件包的标准 Setup 程
序：http://releases.ansible.com/ansible-tower/setup/ 。最新程序：
http://release.ansible.com/ansible-tower-setup-latest.tar.gz 。
可以在离线环境下安装的 Bundle setup 程序：http://release.ansible.com/setup-bundle/ansible-tower-
setup-bundle-latest.el8.tar 。


安装 Ansible Tower
以下过程适用于 RHEL 8.0 或更高版本的单机安装流程：
1. 以 root 用户身份，下载 Ansible Tower Bundle 包
2. 提取 Ansible Tower Bundle 包，然后切换到含有提取内容的目录
3. 编辑 inventory 文件，设置以下账户的密码：Ansible Tower admin 账户密码、PostgreSQL 数据库用户账
户密码和 RabbitMQ 消息队列账户密码。
4. 运行 setup.sh 脚本来启动 Ansible Tower 安装程序
5. 安装程序成功后，使用浏览器连接 Ansible Tower 系统，接受安全证书
6. 使用 admin 用户登录，密码为 invetory 中设置的值
7. 第一次登录时，需要输入许可证并接受最终用户许可协议



## 博文参考


