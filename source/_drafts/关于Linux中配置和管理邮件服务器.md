---
title: 关于Linux中配置和管理邮件服务器
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-10-23 15:22:11/关于Linux中配置和管理邮件服务器.html'
mathJax: false
date: 2022-10-23 23:22:11
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

![在这里插入图片描述](https://img-blog.csdnimg.cn/47adeddb589143e19f1b872440da7d2b.png)

### 电子邮件基本概念
+ MUA(Mail User Agent)，接收和发送邮件的客户端，例如outlook、Mac Mail、mutt。
+ MTA(Mail Transfer Agent)，通过SMTP协议发送、转发邮件，例如sendmail、postfix。
+ MDA(Mail Deliver Agent)，将MTA接收到的邮件保存到磁盘或指定地方，通常会进行垃圾邮件及病毒扫描，例如procmail、dropmail。
+ MRA(Mail Receive Agent)负责实现IMAP与POP3协议，与MUA进行交互，例如dovecot。


邮件服务器基本都有MTA，MDA，MRA组成。

#### 传输协议

+ SMTP(Simple Mail Transfer Protocol)，发送邮件所使用的标准协议。
+ IMAP(Internet Message Access Protocol)，接收邮件使用的标准协议之一。
+ POP3(Post Office Protocol3)，接收邮件使用的标准协议之。


### 空客户端邮件服务器
RHEL 8中Postfix 软件提供MTA功能。在大多数linux系统服务器上，管理员将Postfix配置为空客户端邮件服务器。空客户端邮件服务器不接收任何外部邮件，而是将电子邮件转发到中继邮件服务器。中继邮件服务器根据收件人域的MX记录将消息传输到收件人邮件服务器。

安装软件

```bash
┌──[root@vms81.liruilongs.github.io]-[/var/spool/mail]
└─$yum -y install postfix
```
```bash
┌──[root@vms81.liruilongs.github.io]-[/var/spool/mail]
└─$rpm -qc postfix
/etc/pam.d/smtp.postfix
/etc/postfix/access
/etc/postfix/canonical
/etc/postfix/generic
/etc/postfix/header_checks
/etc/postfix/main.cf
/etc/postfix/master.cf
/etc/postfix/relocated
/etc/postfix/transport
/etc/postfix/virtual
/etc/sasl2/smtpd.conf
```

### 修改Postfix主要配置

Postfix服务由master守护程序提供。

/etc/postfix目录还包含其他Postfix配置文件。例如，/etc/postfix/master.cf控制着主进程启动哪些子服务。

```bash
┌──[root@vms81.liruilongs.github.io]-[/var/spool/mail]
└─$cat /etc/postfix/master.cf  | grep -v  ^#
smtp      inet  n       -       n       -       -       smtpd
pickup    unix  n       -       n       60      1       pickup
cleanup   unix  n       -       n       -       0       cleanup
qmgr      unix  n       -       n       300     1       qmgr
tlsmgr    unix  -       -       n       1000?   1       tlsmgr
rewrite   unix  -       -       n       -       -       trivial-rewrite
bounce    unix  -       -       n       -       0       bounce
defer     unix  -       -       n       -       0       bounce
trace     unix  -       -       n       -       0       bounce
verify    unix  -       -       n       -       1       verify
flush     unix  n       -       n       1000?   0       flush
proxymap  unix  -       -       n       -       -       proxymap
proxywrite unix -       -       n       -       1       proxymap
smtp      unix  -       -       n       -       -       smtp
relay     unix  -       -       n       -       -       smtp
showq     unix  n       -       n       -       -       showq
error     unix  -       -       n       -       -       error
retry     unix  -       -       n       -       -       error
discard   unix  -       -       n       -       -       discard
local     unix  -       n       n       -       -       local
virtual   unix  -       n       n       -       -       virtual
lmtp      unix  -       -       n       -       -       lmtp
anvil     unix  -       -       n       -       1       anvil
scache    unix  -       -       n       -       1       scache
┌──[root@vms81.liruilongs.github.io]-[/var/spool/mail]
└─$
```
Postfix主要配置文件是/etc/postfix/main.cf。

```bash
┌──[root@vms81.liruilongs.github.io]-[/var/spool/mail]
└─$cat /etc/postfix/main.cf  | grep -v ^# | grep -v ^$
queue_directory = /var/spool/postfix
command_directory = /usr/sbin
daemon_directory = /usr/libexec/postfix
data_directory = /var/lib/postfix
mail_owner = postfix
inet_interfaces = localhost
inet_protocols = all
mydestination = $myhostname, localhost.$mydomain, localhost
unknown_local_recipient_reject_code = 550
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases


debug_peer_level = 2
debugger_command =
         PATH=/bin:/usr/bin:/usr/local/bin:/usr/X11R6/bin
         ddd $daemon_directory/$process_name $process_id & sleep 5
sendmail_path = /usr/sbin/sendmail.postfix
newaliases_path = /usr/bin/newaliases.postfix
mailq_path = /usr/bin/mailq.postfix
setgid_group = postdrop
html_directory = no
manpage_directory = /usr/share/man
sample_directory = /usr/share/doc/postfix-2.10.1/samples
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
┌──[root@vms81.liruilongs.github.io]-[/var/spool/mail]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$postconf inet_interfaces
inet_interfaces = localhost
┌──[root@vms81.liruilongs.github.io]-[~]
└─$postconf  | grep inetr
┌──[root@vms81.liruilongs.github.io]-[~]
└─$postconf  | grep inter
address_verify_cache_cleanup_interval = 12h
inet_interfaces = localhost
internal_mail_filter_classes =
local_header_rewrite_clients = permit_inet_interfaces
postscreen_cache_cleanup_interval = 12h
postscreen_whitelist_interfaces = static:all
proxy_interfaces =
┌──[root@vms81.liruilongs.github.io]-[~]
└─$



```

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$postconf myorigin
myorigin = $myhostname
┌──[root@vms81.liruilongs.github.io]-[~]
└─$postconf | grep myhostname
lmtp_lhlo_name = $myhostname
local_transport = local:$myhostname
milter_macro_daemon_name = $myhostname
mydestination = $myhostname, localhost.$mydomain, localhost
myhostname = vms81.liruilongs.github.io
myorigin = $myhostname
smtp_helo_name = $myhostname
smtpd_banner = $myhostname ESMTP $mail_name
smtpd_proxy_ehlo = $myhostname
┌──[root@vms81.liruilongs.github.io]-[~]
└─$postconf -e 'myhostname = vms81.liruilongs.github.io'
┌──[root@vms81.liruilongs.github.io]-[~]
└─$postconf myhostname
myhostname = vms81.liruilongs.github.io
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```


```bash
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$cat STANDARD_CONFIGURATION_README | grep -A 50  "A null client"
A null client is a machine that can only send mail. It receives no mail from
the network, and it does not deliver any mail locally. A null client typically
uses POP, IMAP or NFS for mailbox access.

In this example we assume that the Internet domain name is "example.com" and
that the machine is named "hostname.example.com". As usual, the examples show
only parameters that are not left at their default settings.

    1 /etc/postfix/main.cf:
    2     myhostname = hostname.example.com
    3     myorigin = $mydomain
    4     relayhost = $mydomain
    5     inet_interfaces = loopback-only
    6     mydestination =

Translation:

  * Line 2: Set myhostname to hostname.example.com, in case the machine name
    isn't set to a fully-qualified domain name (use the command "postconf -
    d myhostname" to find out what the machine name is).

  * Line 2: The myhostname value also provides the default value for the
    mydomain parameter (here, "mydomain = example.com").

  * Line 3: Send mail as "user@example.com" (instead of
    "user@hostname.example.com"), so that nothing ever has a reason to send
    mail to "user@hostname.example.com".

  * Line 4: Forward all mail to the mail server that is responsible for the
    "example.com" domain. This prevents mail from getting stuck on the null
    client if it is turned off while some remote destination is unreachable.
    Specify a real hostname here if your "example.com" domain has no MX record.

  * Line 5: Do not accept mail from the network.

  * Line 6: Disable local mail delivery. All mail goes to the mail server as
    specified in line 4.

Postfix on a local network
```


```bash
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf -e 'inet_interfaces = loopback-only'
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf  mynetworks
mynetworks = 127.0.0.0/8 [::1]/128
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf  myorigin
myorigin = $myhostname
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf myhostname
myhostname = vms81.liruilongs.github.io
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf mydestination
mydestination = $myhostname, localhost.$mydomain, localhost
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf -e ' mydestination = '
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf mydestination
mydestination =
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf myhostname
myhostname = vms81.liruilongs.github.io
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf relayhost
relayhost =
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf -e ' relayhost = [smtp.vms81.liruilongs.github.io]'
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf relayhost
relayhost = [smtp.vms81.liruilongs.github.io]
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$systemctl restart postfix
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$tail -n 5 /etc/postfix/main.cf
# readme_directory: The location of the Postfix README files.
#
readme_directory = /usr/share/doc/postfix-2.10.1/README_FILES
myhostname = vms81.liruilongs.github.io
relayhost = [smtp.vms81.liruilongs.github.io]
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$mail
No mail for root
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$mail -s 'init test' tom@vms81.liruilongs.github.io
test null client
.
EOT
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$tail -n 5  /var/log/maillog
Oct 24 11:37:40 vms81 postfix/smtp[121693]: connect to smtp.vms81.liruilongs.github.io[2606:50c0:8000::153]:25: Network is unreachable
Oct 24 11:38:01 vms81 postfix/smtp[121693]: B1720100881CC: lost connection with smtp.vms81.liruilongs.github.io[185.199.110.153] while receiving the initial server greeting
Oct 24 11:38:22 vms81 postfix/smtp[121693]: B1720100881CC: lost connection with smtp.vms81.liruilongs.github.io[185.199.108.153] while receiving the initial server greeting
Oct 24 11:38:22 vms81 postfix/smtp[121693]: connect to smtp.vms81.liruilongs.github.io[2606:50c0:8001::153]:25: Network is unreachable
Oct 24 11:38:22 vms81 postfix/smtp[121693]: B1720100881CC: to=<tom@vms81.liruilongs.github.io>, relay=none, delay=42, delays=0.02/0.01/42/0, dsn=4.4.1, status=deferred (connect to smtp.vms81.liruilongs.github.io[2606:50c0:8001::153]:25: Network is unreachable)
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$journalctl | tail -n 5
Oct 24 11:37:40 vms81.liruilongs.github.io postfix/smtp[121693]: connect to smtp.vms81.liruilongs.github.io[2606:50c0:8000::153]:25: Network is unreachable
Oct 24 11:38:01 vms81.liruilongs.github.io postfix/smtp[121693]: B1720100881CC: lost connection with smtp.vms81.liruilongs.github.io[185.199.110.153] while receiving the initial server greeting
Oct 24 11:38:22 vms81.liruilongs.github.io postfix/smtp[121693]: B1720100881CC: lost connection with smtp.vms81.liruilongs.github.io[185.199.108.153] while receiving the initial server greeting
Oct 24 11:38:22 vms81.liruilongs.github.io postfix/smtp[121693]: connect to smtp.vms81.liruilongs.github.io[2606:50c0:8001::153]:25: Network is unreachable
Oct 24 11:38:22 vms81.liruilongs.github.io postfix/smtp[121693]: B1720100881CC: to=<tom@vms81.liruilongs.github.io>, relay=none, delay=42, delays=0.02/0.01/42/0, dsn=4.4.1, status=deferred (connect to smtp.vms81.liruilongs.github.io[2606:50c0:8001::153]:25: Network is unreachable)
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postqueue -p
-Queue ID- --Size-- ----Arrival Time---- -Sender/Recipient-------
B1720100881CC      492 Mon Oct 24 11:37:40  root@vms81.liruilongs.github.io
(connect to smtp.vms81.liruilongs.github.io[2606:50c0:8001::153]:25: Network is unreachable)
                                         tom@vms81.liruilongs.github.io

-- 0 Kbytes in 1 Request.
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf -e 'mydestination = vms81.liruilongs.github.io'
```
```bash
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postconf -e 'mydestination = vms81.liruilongs.github.io'
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$systemctl restart postfix
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$mail -s 'init test' tom@vms81.liruilongs.github.io
test
.
EOT
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postqueue -p
-Queue ID- --Size-- ----Arrival Time---- -Sender/Recipient-------
B1720100881CC      492 Mon Oct 24 11:37:40  root@vms81.liruilongs.github.io
(connect to smtp.vms81.liruilongs.github.io[2606:50c0:8000::153]:25: Network is unreachable)
                                         tom@vms81.liruilongs.github.io

-- 0 Kbytes in 1 Request.
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$tail -n 5  /var/log/maillog
Oct 24 15:04:15 vms81 postfix/pickup[68570]: D8F59100881CF: uid=0 from=<root>
Oct 24 15:04:15 vms81 postfix/cleanup[69082]: D8F59100881CF: message-id=<20221024070415.D8F59100881CF@vms81.liruilongs.github.io>
Oct 24 15:04:15 vms81 postfix/qmgr[68571]: D8F59100881CF: from=<root@vms81.liruilongs.github.io>, size=480, nrcpt=1 (queue active)
Oct 24 15:04:15 vms81 postfix/local[69084]: D8F59100881CF: to=<tom@vms81.liruilongs.github.io>, relay=local, delay=0.04, delays=0.03/0.01/0/0, dsn=2.0.0, status=sent (delivered to mailbox)
Oct 24 15:04:15 vms81 postfix/qmgr[68571]: D8F59100881CF: removed
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$
```



```bash
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postqueue -p
-Queue ID- --Size-- ----Arrival Time---- -Sender/Recipient-------
B1720100881CC      492 Mon Oct 24 11:37:40  root@vms81.liruilongs.github.io
(connect to smtp.vms81.liruilongs.github.io[2606:50c0:8000::153]:25: Network is unreachable)
                                         tom@vms81.liruilongs.github.io

-- 0 Kbytes in 1 Request.
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postqueue -f
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$postqueue -p
Mail queue is empty
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$cat /var/spool/mail/tom
From root@vms81.liruilongs.github.io  Mon Oct 24 15:04:15 2022
Return-Path: <root@vms81.liruilongs.github.io>
X-Original-To: tom@vms81.liruilongs.github.io
Delivered-To: tom@vms81.liruilongs.github.io
Received: by vms81.liruilongs.github.io (Postfix, from userid 0)
        id D8F59100881CF; Mon, 24 Oct 2022 15:04:15 +0800 (CST)
Date: Mon, 24 Oct 2022 15:04:15 +0800
To: tom@vms81.liruilongs.github.io
Subject: init test
User-Agent: Heirloom mailx 12.5 7/5/10
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit
Message-Id: <20221024070415.D8F59100881CF@vms81.liruilongs.github.io>
From: root@vms81.liruilongs.github.io (root)

test

From root@vms81.liruilongs.github.io  Mon Oct 24 15:11:07 2022
Return-Path: <root@vms81.liruilongs.github.io>
X-Original-To: tom@vms81.liruilongs.github.io
Delivered-To: tom@vms81.liruilongs.github.io
Received: by vms81.liruilongs.github.io (Postfix, from userid 0)
        id B1720100881CC; Mon, 24 Oct 2022 11:37:40 +0800 (CST)
Date: Mon, 24 Oct 2022 11:37:40 +0800
To: tom@vms81.liruilongs.github.io
Subject: init test
User-Agent: Heirloom mailx 12.5 7/5/10
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit
Message-Id: <20221024033740.B1720100881CC@vms81.liruilongs.github.io>
From: root@vms81.liruilongs.github.io (root)

test null client

┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[/usr/share/doc/postfix-2.10.1/README_FILES]
└─$mail -u tom
Heirloom Mail version 12.5 7/5/10.  Type ? for help.
"/var/mail/tom": 2 messages 2 new
>N  1 root                  Mon Oct 24 15:04  18/669   "init test"
 N  2 root                  Mon Oct 24 15:11  18/681   "init test"
& 2
Message  2:
From root@vms81.liruilongs.github.io  Mon Oct 24 15:11:07 2022
Return-Path: <root@vms81.liruilongs.github.io>
X-Original-To: tom@vms81.liruilongs.github.io
Delivered-To: tom@vms81.liruilongs.github.io
Date: Mon, 24 Oct 2022 11:37:40 +0800
To: tom@vms81.liruilongs.github.io
Subject: init test
User-Agent: Heirloom mailx 12.5 7/5/10
Content-Type: text/plain; charset=us-ascii
From: root@vms81.liruilongs.github.io (root)
Status: R

test null client

& q
Held 2 messages in /var/mail/tom
```
## 博文参考



```bash
┌──[root@vms82.liruilongs.github.io]-[/var/spool/mail]
└─$yum -y install postfix
┌──[root@vms82.liruilongs.github.io]-[~]
└─$postconf  mynetworks
mynetworks = 127.0.0.0/8 [::1]/128
┌──[root@vms82.liruilongs.github.io]-[~]
└─$postconf -e 'inet_interfaces = loopback-only'
┌──[root@vms82.liruilongs.github.io]-[~]
└─$postconf -e ' relayhost = [smtp.vms82.liruilongs.github.io]'
┌──[root@vms82.liruilongs.github.io]-[~]
└─$postconf -e 'mydestination = vms82.liruilongs.github.io'
┌──[root@vms82.liruilongs.github.io]-[~]
└─$systemctl restart postfix
┌──[root@vms82.liruilongs.github.io]-[~]
└─$yum -y install mailx
┌──[root@vms82.liruilongs.github.io]-[~]
└─$mail -s 'init test' tom@vms82.liruilongs.github.io
test
.
EOT
┌──[root@vms82.liruilongs.github.io]-[~]
└─$cat /var/spool/mail/tom
From root@vms82.liruilongs.github.io  Mon Oct 24 15:39:25 2022
Return-Path: <root@vms82.liruilongs.github.io>
X-Original-To: tom@vms82.liruilongs.github.io
Delivered-To: tom@vms82.liruilongs.github.io
Received: by vms82.liruilongs.github.io (Postfix, from userid 0)
        id C7A2A1002EF2A; Mon, 24 Oct 2022 15:39:25 +0800 (CST)
Date: Mon, 24 Oct 2022 15:39:25 +0800
To: tom@vms82.liruilongs.github.io
Subject: init test
User-Agent: Heirloom mailx 12.5 7/5/10
MIME-Version: 1.0
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit
Message-Id: <20221024073925.C7A2A1002EF2A@vms82.liruilongs.github.io>
From: root@vms82.liruilongs.github.io (root)

test

```