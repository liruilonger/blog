---
title: 关于K8s 中可观测性工具Pixie 的一些笔记整理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-01-31 14:59:23/关于K8s 中可观测性工具Pixie 的一些笔记整理.html'
mathJax: false
date: 2023-01-31 22:59:23
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


https://docs.px.dev/installing-pixie/requirements/

需要Kubernetes v1.21+。

中央处理器
Pixie 需要一个x86-64架构。



需要注册

![在这里插入图片描述](https://img-blog.csdnimg.cn/6ee8957b28394cad891ed6579e0ed081.png)

登录

![在这里插入图片描述](https://img-blog.csdnimg.cn/72ae8e935c974fa7a72cde3b501d3b2b.png)


### 安装 Pixie CLI
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$bash -c "$(curl -fsSL https://withpixie.ai/install.sh)"

  ___  _       _
 | _ \(_)__ __(_) ___
 |  _/| |\ \ /| |/ -_)
 |_|  |_|/_\_\|_|\___|

==> Info:
Pixie gives engineers access to no-instrumentation, streaming &
unsampled auto-telemetry to debug performance issues in real-time,
More information at: https://www.pixielabs.ai.

This command will install the Pixie CLI (px) in a location selected
by you, and performs authentication with Pixie's cloud hosted control
plane. After installation of the CLI you can easily manage Pixie
installations on your K8s clusters and execute scripts to collect
telemetry from your clusters using Pixie.

Docs:
  https://work.withpixie.ai/docs


==> Terms and Conditions https://www.pixielabs.ai/terms
I have read and accepted the Terms & Conditions [y/n]: y


==> Installing PX CLI:
Install Path [/usr/local/bin]: /root/ansible/pixie
........
==> Next steps:
- PX CLI has been installed to: /root/ansible/pixie. Make sure this directory is in your PATH.
- Run px deploy to deploy Pixie on K8s.
- Run px help to get started, or visit our UI: https://work.withpixie.ai
- Further documentation:
    https://work.withpixie.ai/docs
```



```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$./px  version
Pixie CLI
0.7.17+Distribution.1f8cdfb.20220816051559.3.jenkins
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$mv px  /usr/local/bin/
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$which  px
/usr/local/bin/px
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$px deploy --check_only
Pixie CLI
You must be logged in to perform this operation. Please run `px auth login`.
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$px auth login
Pixie CLI
Starting browser... (if browser-based login fails, try running `px auth login --manual` for headless login)
Fetching refresh token ...
Failed to perform browser based auth. Will try manual auth error=browser failed to open

Please Visit:
         https://work.withpixie.ai:443/login?local_mode=true

Copy and paste token here: eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImQ4b3pRR3VQX2ItZXRqYVNldnJqSTJsLUFfWm1IWVp4YUxOTWR5NWZqWjAifQ......
Fetching refresh token
Authentication Successful
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$
```
https://work.withpixie.ai:443/login?local_mode=true

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$px deploy --check_only
Pixie CLI

Running Cluster Checks:
 ✕    Kernel version > 4.14.0  ERR: kernel version for node (vms100.liruilongs.github.io) not supported ✕    Kernel version > 4.14.0  ERR: kernel version for node (vms100.liruilongs.github.io) not supported. Must have minimum kernel version of (4.14.0)
Check pre-check has failed. To bypass pass in --check=false. error=kernel version for node (vms100.liruilongs.github.io) not supported. Must have minimum kernel version of (4.14.0)
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$hostnamectl
   Static hostname: vms100.liruilongs.github.io
         Icon name: computer-vm
           Chassis: vm
        Machine ID: e93ae3f6cb354f3ba509eeb73568087e
           Boot ID: 5ed408a863df48ae80b51f1b6c4be85f
    Virtualization: vmware
  Operating System: CentOS Linux 7 (Core)
       CPE OS Name: cpe:/o:centos:centos:7
            Kernel: Linux 3.10.0-693.el7.x86_64
      Architecture: x86-64
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$
```
内核版本太低，不被支持，这我们尝试一下。升级一下内核

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$px deploy --check_only
Pixie CLI

Running Cluster Checks:
 ✔    Kernel version > 4.14.0
 ✔    Cluster type is supported
 ✔    K8s version > 1.16.0
 ✔    Kubectl > 1.10.0 is present
 ✔    User can create namespace
INFO[0002] All Required Checks Passed!
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$
```

```bash
Found 5 nodes
 ✔    Installing OLM CRDs
 ✔    Deploying OLM
 ✔    Deploying Pixie OLM Namespace
 ✔    Installing Vizier CRD
 ✔    Deploying OLM Catalog
 ✔    Deploying OLM Subscription
 ✔    Creating namespace
 ✔    Deploying Vizier
 ⠙    Waiting for Cloud Connector to come online
FATA[0365] Timed out waiting for cluster ID assignment
```



```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$kubectl get pods -n px-operator
NAME                         READY   STATUS             RESTARTS   AGE
pixie-operator-index-f7snb   0/1     ImagePullBackOff   0          63m
pixie-operator-index-ldmbm   0/1     ImagePullBackOff   0          25h
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$kubectl describe pods -n px-operator | grep -i event
Events:
Events:
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$kubectl describe pods -n px-operator | grep -i -C 5  event
    DownwardAPI:             true
QoS Class:                   Burstable
Node-Selectors:              kubernetes.io/os=linux
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason   Age                    From     Message
  ----     ------   ----                   ----     -------
  Warning  Failed   18m (x13 over 63m)     kubelet  Failed to pull image "gcr.io/pixie-oss/pixie-prod/operator/bundle_index:0.0.1": rpc error: code = Unknown desc = Error response from daemon: Get "https://gcr.io/v2/": net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)
  Normal   Pulling  13m (x14 over 63m)     kubelet  Pulling image "gcr.io/pixie-oss/pixie-prod/operator/bundle_index:0.0.1"
  Normal   BackOff  3m56s (x250 over 63m)  kubelet  Back-off pulling image "gcr.io/pixie-oss/pixie-prod/operator/bundle_index:0.0.1"
--
    DownwardAPI:             true
QoS Class:                   Burstable
Node-Selectors:              kubernetes.io/os=linux
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason   Age                    From     Message
  ----     ------   ----                   ----     -------
  Warning  Failed   60m (x3 over 61m)      kubelet  Error: ErrImagePull
  Warning  Failed   60m (x6 over 61m)      kubelet  Error: ImagePullBackOff
  Warning  Failed   22m (x12 over 61m)     kubelet  Failed to pull image "gcr.io/pixie-oss/pixie-prod/operator/bundle_index:0.0.1": rpc error: code = Unknown desc = Error response from daemon: Get "https://gcr.io/v2/": net/http: request canceled while waiting for connection (Client.Timeout exceeded while awaiting headers)
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$kubectl get pods -n px-operator  pixie-operator-index-f7snb  -o yaml | grep  -i imagePullPolicy
    imagePullPolicy: Always
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$
```

```bash
┌──[root@vms103.liruilongs.github.io]-[~]
└─$docker login
Authenticating with existing credentials...
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
┌──[root@vms103.liruilongs.github.io]-[~]
└─$docker tag    gcr.io/pixie-oss/pixie-prod/operator/bundle_index:0.0.1  liruilong/bundle_index:0.0.1
┌──[root@vms103.liruilongs.github.io]-[~]
└─$docker push  liruilong/bundle_index:0.0.1
The push refers to repository [docker.io/liruilong/bundle_index]
afd349405d9c: Pushed
16a6b5b421af: Pushed
26895fdeb37e: Pushed
a98a386b6ec2: Pushed
4e7f383eb531: Pushed
bc276c40b172: Pushed
0.0.1: digest: sha256:6eb8d198bd9bc4c9727cb85209ed1e4214e620b1e1064a9c87d25f5983ddcf8d size: 1579
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$px deploy-key create
Pixie CLI
Generated deployment key:
ID: 8c879b81-48d8-465d-84ba-d08c2d1382ab
Key: px-dep-76c62fdc-63c2-4229-a567-51b430560617
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$
```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$px deploy --extract_yaml ./ --deploy_key px-dep-76c62fdc-63c2-4229-a567-51b430560617 --deploy_olm = false
```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie/pixie_yamls]
└─$vim 04_catalog.yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie/pixie_yamls]
└─$kubectl apply  -f 04_catalog.yaml
Warning: resource catalogsources/pixie-operator-index is missing the kubectl.kubernetes.io/last-applied-configuration annotation which is required by kubectl apply. kubectl apply should only be used on resources created declaratively by either kubectl create --save-config or kubectl apply. The missing annotation will be patched automatically.
catalogsource.operators.coreos.com/pixie-operator-index configured
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie/pixie_yamls]
└─$
```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie/pixie_yamls]
└─$cat 04_catalog.yaml
apiVersion: operators.coreos.com/v1alpha1
kind: CatalogSource
metadata:
  name: pixie-operator-index
  namespace: px-operator
spec:
  sourceType: grpc
  image: docker.io/liruilong/bundle_index:0.0.1
  displayName: Pixie Vizier Operator
  publisher: px.dev
  updateStrategy:
    registryPoll:
      interval: 10m
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/pixie]
└─$kubectl apply  -f pixie_yamls/
customresourcedefinition.apiextensions.k8s.io/catalogsources.operators.coreos.com unchanged
customresourcedefinition.apiextensions.k8s.io/clusterserviceversions.operators.coreos.com unchanged
customresourcedefinition.apiextensions.k8s.io/installplans.operators.coreos.com unchanged
customresourcedefinition.apiextensions.k8s.io/operatorconditions.operators.coreos.com unchanged
customresourcedefinition.apiextensions.k8s.io/operatorgroups.operators.coreos.com unchanged
customresourcedefinition.apiextensions.k8s.io/operators.operators.coreos.com unchanged
customresourcedefinition.apiextensions.k8s.io/subscriptions.operators.coreos.com unchanged
customresourcedefinition.apiextensions.k8s.io/viziers.px.dev configured
namespace/olm unchanged
serviceaccount/olm-operator-serviceaccount unchanged
clusterrole.rbac.authorization.k8s.io/system:controller:operator-lifecycle-manager unchanged
clusterrolebinding.rbac.authorization.k8s.io/olm-operator-cluster-binding-olm unchanged
deployment.apps/olm-operator configured
deployment.apps/catalog-operator configured
clusterrole.rbac.authorization.k8s.io/aggregate-olm-edit unchanged
clusterrole.rbac.authorization.k8s.io/aggregate-olm-view unchanged
operatorgroup.operators.coreos.com/olm-operators unchanged
namespace/px-operator unchanged
operatorgroup.operators.coreos.com/global-operators unchanged
catalogsource.operators.coreos.com/pixie-operator-index unchanged
subscription.operators.coreos.com/pixie-operator-subscription unchanged
vizier.px.dev/pixie unchanged
```

```bash
Back-off pulling image "gcr.io/pixie-oss/pixie-prod/operator/bundle:0.0.36"
Back-off pulling image "gcr.io/pixie-oss/pixie-prod/operator/bundle_index:0.0.1"
```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$ansible k8s_node  -m copy -a "src=./pixie/bundle.0.0.36.tar dest=/tmp/" -i host.yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$ansible k8s_node  -m shell "docker load -i /tmp/bundle.0.0.36.tar" -i host.yaml
```

```bash
┌──[root@vms103.liruilongs.github.io]-[~]
└─$docker tag   8b68820209c6   liruilong/bundle:0.0.36
┌──[root@vms103.liruilongs.github.io]-[~]
└─$docker push liruilong/bundle:0.0.36
The push refers to repository [docker.io/liruilong/bundle]
e19924a75eb9: Pushed
d997c1f80fd9: Pushed
0.0.36: digest: sha256:16cbc283c9f151e961128955da700f3eff32f5e037473b9e4263f1c2a221d75b size: 732
```

## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有
***

https://github.com/pixie-io/pixie

https://docs.px.dev/installing-pixie/install-guides/



***
<font color=#999aaa>© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-自由转载-相同方式共享(创意共享3.0许可证)</font>

