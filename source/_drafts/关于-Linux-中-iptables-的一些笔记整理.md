---
title: 关于 Linux 中 iptables 的一些笔记整理
tags:
  - iptables
categories:
  - iptables
toc: true
recommend: 1
keywords: iptables
uniqueId: '2022-12-05 19:10:59/关于 Linux 中 iptables 的一些笔记整理.html'
mathJax: false
date: 2022-12-06 03:10:59
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***





我们来做一些简单的尝试

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -t filter  -I INPUT -p icmp -j REJECT
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
REJECT     icmp --  0.0.0.0/0            0.0.0.0/0            reject-with icmp-port-unreachable

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -t filter  -I INPUT -p icmp -s 192.168.26.153 -j REJECT
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
REJECT     icmp --  192.168.26.153       0.0.0.0/0            reject-with icmp-port-unreachable
REJECT     icmp --  0.0.0.0/0            0.0.0.0/0            reject-with icmp-port-unreachable

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -t filter  -I INPUT -p icmp -d 192.168.26.152 -j REJECT
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
REJECT     icmp --  0.0.0.0/0            192.168.26.152       reject-with icmp-port-unreachable
REJECT     icmp --  192.168.26.153       0.0.0.0/0            reject-with icmp-port-unreachable
REJECT     icmp --  0.0.0.0/0            0.0.0.0/0            reject-with icmp-port-unreachable

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ping -i 0.1 -c 4 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.
From 192.168.26.152 icmp_seq=1 Destination Port Unreachable
From 192.168.26.152 icmp_seq=2 Destination Port Unreachable
From 192.168.26.152 icmp_seq=3 Destination Port Unreachable
From 192.168.26.152 icmp_seq=4 Destination Port Unreachable

--- 192.168.26.152 ping statistics ---
4 packets transmitted, 0 received, +4 errors, 100% packet loss, time 301ms

┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -D INPUT 3
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
REJECT     icmp --  0.0.0.0/0            192.168.26.152       reject-with icmp-port-unreachable
REJECT     icmp --  192.168.26.153       0.0.0.0/0            reject-with icmp-port-unreachable

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -D INPUT 1
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL INPUT
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
REJECT     icmp --  192.168.26.153       0.0.0.0/0            reject-with icmp-port-unreachable
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ping -i 0.1 -c 4 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.
64 bytes from 192.168.26.152: icmp_seq=1 ttl=64 time=0.411 ms
64 bytes from 192.168.26.152: icmp_seq=2 ttl=64 time=0.474 ms
64 bytes from 192.168.26.152: icmp_seq=3 ttl=64 time=0.421 ms
64 bytes from 192.168.26.152: icmp_seq=4 ttl=64 time=0.539 ms

--- 192.168.26.152 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 300ms
rtt min/avg/max/mdev = 0.411/0.461/0.539/0.053 ms
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$

┌──[root@vms153.liruilongs.github.io]-[~]
└─$ping -i 0.1 -c 3 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.
From 192.168.26.152 icmp_seq=1 Destination Port Unreachable
From 192.168.26.152 icmp_seq=2 Destination Port Unreachable
From 192.168.26.152 icmp_seq=3 Destination Port Unreachable

--- 192.168.26.152 ping statistics ---
3 packets transmitted, 0 received, +3 errors, 100% packet loss, time 272ms

┌──[root@vms153.liruilongs.github.io]-[~]
└─$
```



iptables基本用法

+ 可以不指定表，默认为 `filter`表
+ 可以不指定链，默认为对应表的所有链
+ 如果没有匹配的规则，则使用防火墙默认规则
+ 选项/链名/目标操作用大写字母，其余都小写



目标操作
+ ACCEPT：允许通过/放行
+ DROP：直接丢弃，不给出任何回应
+ REJECT：拒绝通过，必要时会给出提示
+ LOG : 记录日志，传递给下一条规则，匹配即停止规则的唯一例外


清空所有的防火墙规则

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -F
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL
Chain INPUT (policy ACCEPT)
target     prot opt source               destination

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -t filter  -I INPUT  -p icmp -j LOG

```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ssh vms153.liruilongs.github.io ping -i 0.1 -c 5 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.
64 bytes from 192.168.26.152: icmp_seq=1 ttl=64 time=0.406 ms
64 bytes from 192.168.26.152: icmp_seq=2 ttl=64 time=0.472 ms
64 bytes from 192.168.26.152: icmp_seq=3 ttl=64 time=0.398 ms
64 bytes from 192.168.26.152: icmp_seq=4 ttl=64 time=0.719 ms
64 bytes from 192.168.26.152: icmp_seq=5 ttl=64 time=1.39 ms

--- 192.168.26.152 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 529ms
rtt min/avg/max/mdev = 0.398/0.677/1.394/0.378 ms
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tail -n 5 /var/log/messages
Dec 17 23:54:26 vms152 kernel: IN=ens32 OUT= MAC=00:0c:29:fe:44:e2:00:0c:29:b1:97:a1:08:00 SRC=192.168.26.153 DST=192.168.26.152 LEN=84 TOS=0x00 PREC=0x00 TTL=64 ID=61021 DF PROTO=ICMP TYPE=8 CODE=0 ID=1961 SEQ=1
Dec 17 23:54:26 vms152 kernel: IN=ens32 OUT= MAC=00:0c:29:fe:44:e2:00:0c:29:b1:97:a1:08:00 SRC=192.168.26.153 DST=192.168.26.152 LEN=84 TOS=0x00 PREC=0x00 TTL=64 ID=61111 DF PROTO=ICMP TYPE=8 CODE=0 ID=1961 SEQ=2
Dec 17 23:54:26 vms152 kernel: IN=ens32 OUT= MAC=00:0c:29:fe:44:e2:00:0c:29:b1:97:a1:08:00 SRC=192.168.26.153 DST=192.168.26.152 LEN=84 TOS=0x00 PREC=0x00 TTL=64 ID=61223 DF PROTO=ICMP TYPE=8 CODE=0 ID=1961 SEQ=3
Dec 17 23:54:26 vms152 kernel: IN=ens32 OUT= MAC=00:0c:29:fe:44:e2:00:0c:29:b1:97:a1:08:00 SRC=192.168.26.153 DST=192.168.26.152 LEN=84 TOS=0x00 PREC=0x00 TTL=64 ID=61228 DF PROTO=ICMP TYPE=8 CODE=0 ID=1961 SEQ=4
Dec 17 23:54:26 vms152 kernel: IN=ens32 OUT= MAC=00:0c:29:fe:44:e2:00:0c:29:b1:97:a1:08:00 SRC=192.168.26.153 DST=192.168.26.152 LEN=84 TOS=0x00 PREC=0x00 TTL=64 ID=61263 DF PROTO=ICMP TYPE=8 CODE=0 ID=1961 SEQ=5
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```
常用的管理选项:
+ 添加规则：A(末尾)，I(头)
+ 查看规则：L(列出所有)，n(数字形式显示地址端口) --line-number 显示规则序号
+ 删除规则：D(指定序号或者内容) F(清空所有)
+ 默认策略： P(设置默认策略)
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -L INPUT
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
REJECT     icmp --  anywhere             vms152.rhce.cc       reject-with icmp-port-unreachable
LOG        icmp --  anywhere             anywhere             LOG level warning
DROP       icmp --  anywhere             anywhere             icmp timestamp-request
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL  INPUT  --line-numbers
Chain INPUT (policy ACCEPT)
num  target     prot opt source               destination
1    REJECT     icmp --  0.0.0.0/0            192.168.26.152       reject-with icmp-port-unreachable
2    LOG        icmp --  0.0.0.0/0            0.0.0.0/0            LOG flags 0 level 4
3    DROP       icmp --  0.0.0.0/0            0.0.0.0/0            icmptype 13
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

-t 指定filter表；-A 指向INPUT链的最后一行追加一条规则，-p 指进入本机的数据包，是通过tcp协议进入的，-j 指对数据包的操作，ACCEPT 允许通过，

查看iptables防火墙规则，没有指定表，默认查看的表是filter表，同样不指定表，则默认向filter表中插入规则


设置默认规则

所有的链的初始的默认规则都为 `ACCEYT`, 可以通过 `-P` 选项来重置默认规则。默认规则只允许设置ACCEPT或 DROP

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -P FORWARD  DROP

┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL  FORWARD
Chain FORWARD (policy DROP)
target     prot opt source               destination
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

## 防火墙匹配规则

### 匹配条件(需要取反条件时，可以直接使用!)：

+ 通用匹配：可直接使用，不依赖于其他条件或扩展，包括网络协议(-p)、IP地址(-s,-d)、网络接口(-i,-o)等条件
+ 隐含匹配：要求以特定的协议匹配作为前提，包括端口(--sport，--dport)、TCP标记、ICMP类型(--icmp-type)等条件

### 过滤规则示例

+ 限制特定的IP或网段的访问:-s 丢弃所有从4.120主机发过来的数据包,-s 丢弃所有从4.0网段的主机发过来的数据包
+ 保护特定网络服务: 允许限制指定源IP访问指定端口

### 禁ping相关策略处理

网络中数据的发送，是有来有回的，发送请求，则一定有回应，否则失败，A主机向B主机发送ping请求 —— echo-request，B主机必须回应A主机ping请求 —— echo-reply

插入一条规则：想要实现禁止其他所有主机通过icmp协议ping本机，但本机可以ping通其他主机

从进站的角度，设置规则，本机可以ping其他主机，其他主机不可以ping本机,其他主机ping本机时，通过icmp协议，发送过来的请求 echo-request,直接丢弃,本机ping其他主机时，其他主机返回的数据包类型不是echo-request的，都接受
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -F
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -A INPUT -p icmp --icmp-type echo-request -j DROP
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -A INPUT -p icmp ! --icmp-type echo-request -j ACCEPT
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL INPUT
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
DROP       icmp --  0.0.0.0/0            0.0.0.0/0            icmptype 8
ACCEPT     icmp --  0.0.0.0/0            0.0.0.0/0            icmp !type 8
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ping -i 0.01 -c 3 vms153.liruilongs.github.io
PING vms153.liruilongs.github.io (192.168.26.153) 56(84) bytes of data.
64 bytes from vms153.liruilongs.github.io (192.168.26.153): icmp_seq=1 ttl=64 time=0.351 ms
64 bytes from vms153.liruilongs.github.io (192.168.26.153): icmp_seq=2 ttl=64 time=0.429 ms
64 bytes from vms153.liruilongs.github.io (192.168.26.153): icmp_seq=3 ttl=64 time=0.385 ms

--- vms153.liruilongs.github.io ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 20ms
rtt min/avg/max/mdev = 0.351/0.388/0.429/0.035 ms
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ssh vms153.liruilongs.github.io ping -i 0.01 -c 3 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.

--- 192.168.26.152 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 46ms

┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

从出站的角度，设置规则，本机可以ping其他主机，其他主机不可以ping本机，本机ping其他主机时，发送数据包的类型为 echo-request，防火墙通过，可以发送出去，本机发送的数据包不是 echo-request, 则代表的是数据的回应，直接丢弃该数据包

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -F
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -A OUTPUT  -p icmp  --icmp-type echo-request -j ACCEPT
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -A OUTPUT -p icmp ! --icmp-type echo-request -j DROP
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -nL OUTPUT
Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
ACCEPT     icmp --  0.0.0.0/0            0.0.0.0/0            icmptype 8
DROP       icmp --  0.0.0.0/0            0.0.0.0/0            icmp !type 8
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```


## 一、主机型/网络防火墙

+ 主机型防火墙：  #规则写在INPUT，OUTPU链内，过滤进站或出站数据，保护自身安全。
+ 网络型防火墙：  #规则写在FORWARD链内，转发数据，过滤外网访问内网的数据，保护内网服务器。

开启内核的IP转发

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
┌──[root@vms152.liruilongs.github.io]-[~]
└─$sysctl -p
net.ipv4.ip_forward = 1
┌──[root@vms152.liruilongs.github.io]-[~]
└─$sysctl -n net.ipv4.ip_forward
1
```

```bash
[root@vms152 ~]# ip a | grep glo
    inet 192.168.29.152/24 brd 192.168.29.255 scope global ens32
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ip a | grep glo
    inet 192.168.26.152/24 brd 192.168.26.255 scope global ens32
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$ip a | grep gl
    inet 192.168.29.154/24 brd 192.168.29.255 scope global ens32
    inet 192.168.26.154/24 brd 192.168.26.255 scope global ens33        
```
这里我们假设 `192.168.29.152/24 `为内网机器，  `192.168.26.152` 为外网机器，


```bash
[root@vms152 ~]# ping -i 0.01 -c 3 192.168.26.154
PING 192.168.26.154 (192.168.26.154) 56(84) bytes of data.
64 bytes from 192.168.26.154: icmp_seq=1 ttl=64 time=0.311 ms
64 bytes from 192.168.26.154: icmp_seq=2 ttl=64 time=0.486 ms
64 bytes from 192.168.26.154: icmp_seq=3 ttl=64 time=0.484 ms

--- 192.168.26.154 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 31ms
rtt min/avg/max/mdev = 0.311/0.427/0.486/0.082 ms
[root@vms152 ~]# ping -i 0.01 -c 3 192.168.29.154
PING 192.168.29.154 (192.168.29.154) 56(84) bytes of data.
64 bytes from 192.168.29.154: icmp_seq=1 ttl=64 time=0.413 ms
64 bytes from 192.168.29.154: icmp_seq=2 ttl=64 time=0.648 ms
64 bytes from 192.168.29.154: icmp_seq=3 ttl=64 time=0.850 ms

--- 192.168.29.154 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 101ms
rtt min/avg/max/mdev = 0.413/0.637/0.850/0.178 ms
[root@vms152 ~]#
[root@vms152 ~]# ping -i 0.01 -c 3 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.


--- 192.168.26.152 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 79ms

[root@vms152 ~]#
[root@vms152 ~]#
```

## 博文参考




## iptables

之所以温习，iptables 在 Docker 和 Kubernetes 网络中应用甚广。例如，Docker容器和宿主机的端口映射、Kubernetes  Service 的默认模式、CNI 的 portmap 插件、Kubernetes 网络策略 等都是通过 iptables 实现的



### 1.5.1 祖师爷netfilter

iptables 的底层实现是 netfilter。netfilter 是 Linux 内核 2.4 版引入的一个子系统，最初由 Linux 内核防火墙和网络的维护者Rusty  Russell提出。

它作为一个通用的、抽象的框架，提供一整套 hook 函数的管理机制，使得数据包过滤、包处理(设置标志位、修改TTL等)、地址伪装、网络地址转换、透明代理、访问控制、基于协议类型的连接跟踪，甚至带宽限速等功能成为可能。

netfilter 的架构就是在整个网络流程的若干位置放置一些钩子，并在每个钩子上挂载一些处理函数进行处理。

![在这里插入图片描述](https://img-blog.csdnimg.cn/7a525ee6a46f47c0ac21da10ff330baa.png)

当网卡收到一个包送到协议栈：
+ 最先经过 netfilter 钩子是 prorouting，如果用户埋了钩子，内核会在这里做 DNAT 转化
+ 之后内核会通过本地路由表决定数据包是走本地进程还是走 forward 钩子。
    + 走 forward 钩子会跳过本地进程相关钩子 `input 和 output`，直接走 postrouting 对应的钩子
    + 不走 forward 钩子走本地进程会通过 `input` 钩子，进入本地进程，生成返回报文经过 `ouptput` 钩子。然后走 postrouting 钩子   
+ 在到 postrouting 钩子之前，还涉及一个路由选择，即决定从那个网卡出去，下一跳地址是哪里，最后出协议栈，经过 `postrouting` 



构建在 netfilter 钩子之上的网络安全策略和连接跟踪的用户态程序就有 ebtables、arptables、(IPv6版本的)ip6tables、iptables、iptables-nftables(iptables的改进版本)、conntrack(连接跟踪)等。

Kubernetes网络之间用到的工具就有 ebtables、iptables/ip6tables 和 conntrack，其中iptables是核心。


###  iptables 的表、链结构

firewalld 的防火墙，最终还是调用 iptables，在 firewalld 中写入防火墙规则，firewalld 会将规则写入到iptables 中,需要注意的是，如果启动  firewall 之后，那么 iptables 通过命名行直接配置是不会生效的。

iptables防火墙存在四张表：

+ `filter表`：过滤表，想要允许或拒绝数据包通过，需要将对应规则写入filter表中;
+ `nat表`：地址转换表，想要实现linux的路由转发功能，需要在nat表中添加对应规则；
+ `mangle表`：包标记表，网络中传输数据，发送的是数据包，每个数据包都有包头信息，想要修改数据包的包头信息，需要将对应规则写入到mangle表中；
+ `raw表`：状态跟踪表，跟踪本机发送的数据包，想要知道哪些人第一次访问本机，哪些人第二次访问本机，那些人第三次访问本机，需要将对应规则写入到raw表中；



![在这里插入图片描述](https://img-blog.csdnimg.cn/52624ed786794deab657578b5bdeca2b.png)


每张表存在着固定的链, 在对应的链中可以添加对应的规则。



### 包过滤匹配流程

规则链内的匹配顺序:顺序比对，匹配即停止(LOG除外),不在匹配剩下的,若无任何匹配，则按该链的默认策略处理

![在这里插入图片描述](https://img-blog.csdnimg.cn/7e50d29436624e5aafcfff19fc21b440.png)

iptables 是基于内核的一个功能
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$whereis  iptables
iptables: /usr/sbin/iptables /usr/share/man/man8/iptables.8.gz
┌──[root@vms152.liruilongs.github.io]-[~]
└─$which  iptables
/usr/sbin/iptables
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```


看下帮助文档，可以看到 `iptables`  的规则配置 使用 `iptables [-t 表名] 选项 [链名] [规则]` 的命令实现。




这里需要注意，如果启动了 `firewall.service` 服务，那么配置 `iptables` 是不会生效的。 

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$iptables -h
iptables v1.4.21

Usage: iptables -[ACD] chain rule-specification [options]
       iptables -I chain [rulenum] rule-specification [options]
       iptables -R chain rulenum rule-specification [options]
       iptables -D chain rulenum [options]
       iptables -[LS] [chain [rulenum]] [options]
       iptables -[FZ] [chain] [options]
       iptables -[NX] chain
       iptables -E old-chain-name new-chain-name
       iptables -P chain target [options]
       iptables -h (print this help information)
Commands:
```




### 1.5.2 iptables 的三板斧：table、chain和rule

iptables 是用户空间的一个程序，通过 netlink 和内核的 netfilter 框架打交道，负责往钩子上配置回调函数。一般情况下用于构建 Linux 内核防火墙，特殊情况下也做服务负载均衡(这是 Kubernetes 的特色操作，我们将在后面章节专门分析)。iptables的工作原理如图1-15所示

我们常说的`iptables  5X5`，即5张表(table)和5条链(chain)。5条链即iptables的5条内置链，对应上文介绍的netfilter的5个钩子。

5条链分别是：
+ `INPUT 链`：一般用于处理输入本地进程的数据包；
+ `OUTPUT 链`：一般用于处理本地进程的输出数据包；
+ `FORWARD 链`：一般用于处理转发到 其他机器/network  namespace 的数据包；
+ `PREROUTING 链`：可以在此处进行DNAT；
+ `POSTROUTING 链`：可以在此处进行SNAT。

除了系统预定义的5条iptables链，`用户还可以在表中定义自己的链`，我们将通过例子详细说明

5张表如下所示。
+ `filter 表`：用于控制到达某条链上的数据包是继续放行、直接丢弃(drop)或拒绝(reject)；
+ `nat 表`：用于修改数据包的源和目的地址；
+ `mangle 表`：用于修改数据包的IP头信息；
+ `raw 表`：iptables是有状态的，即iptables对数据包有连接追踪(connection tracking)机制，而raw是用来去除这种追踪机制的；
+ `security 表`：最不常用的表(通常，我们说iptables只有4张表，security表是新加入的特性)，用于在数据包上应用SELinux。

不是每个链上都能挂表，iptables表与链的对应关系如表

![在这里插入图片描述](https://img-blog.csdnimg.cn/98640cac7d1a4edc95535ede0789d5d0.png)

从图中我们可以看到
+ 对于 mangle 表，即修改数据包IP头部信息，可以挂在任何链
+ 对于 raw 表，即修改报文修改 prerouting 和 output 支持
+ 对于 nat 表分情况，snat 只能挂到 postporting 和 input ，其他的都不行，dnat 只能挂到 prerouting 和 outpit。
+  对于 filter，security 表，可以挂到 forward ， input 和 output。


![在这里插入图片描述](https://img-blog.csdnimg.cn/821d1ff03b6043f2a6e1d74c1d6f631b.png)


iptables 的表是来分类管理 iptables 规则(rule)的，系统所有的 iptables 规则都被划分到不同的表集合中。上文也提到了，filter 表中会有过滤数据包的规则，nat 表中会有做地址转换的规则。因此，iptables 的规则就是挂在 netfilter 钩子上的函数，用来修改数据包的内容或过滤数据包，iptables 的表就是所有规则的5个逻辑集合！



iptables的规则是用户真正要书写的规则，一条 iptables 规则包含两部分信息：匹配条件和动作。

匹配条件：

匹配数据包被这条iptables规则“捕获”的条件，例如协议类型、源IP、目的IP、源端口、目的端口、连接状态等

动作：

+ DROP：直接将数据包丢弃，不再进行后续的处理。应用场景是不让某个数据源意识到你的系统的存在，可以用来模拟宕机；
+ REJECT：给客户端返回一个connection  refused或destination unreachable报文。应用场景是不让某个数据源访问你的系统，善意地告诉他：我这里没有你要的服务内容；
+ QUEUE：将数据包放入用户空间的队列，供用户空间的程序处理；
+ RETURN：跳出当前链，该链里后续的规则不再执行；
+ ACCEPT：同意数据包通过，继续执行后续的规则；
+ JUMP：跳转到其他用户自定义的链继续执行

用户自定义链中的规则和系统预定义的5条链里的规则没有区别。由于自定义的链没有与netfilter里的钩子进行绑定，所以它不会自动触发，只能从其他链的规则中跳转过来，这也是JUMP动作
存在的意义。



### 1.5.3 iptables的常规武器

#### 1.查看所有iptables规则

Kubernetes一个节点上的iptables规则输出如下。输出是iptables的filter表的所有规则。使用iptables命令，必须指定针对哪个表进行操作，默认是filter表。
```bash
┌──[root@vms105.liruilongs.github.io]-[~]
└─$iptables -L -n
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
cali-INPUT  all  --  0.0.0.0/0            0.0.0.0/0            /* cali:Cz_u1IQiXIMmKD4c */
KUBE-PROXY-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes load balancer firewall */
KUBE-NODEPORTS  all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes health check service ports */
KUBE-EXTERNAL-SERVICES  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes externally-visible service portals */
KUBE-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0
..............................

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination
cali-FORWARD  all  --  0.0.0.0/0            0.0.0.0/0            /* cali:wUHhoiAYhphO9Mso */
KUBE-PROXY-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes load balancer firewall */
KUBE-FORWARD  all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes forwarding rules */
KUBE-SERVICES  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes service portals */
KUBE-EXTERNAL-SERVICES  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes externally-visible service portals */
DOCKER-USER  all  --  0.0.0.0/0            0.0.0.0/0
DOCKER-ISOLATION-STAGE-1  all  --  0.0.0.0/0            0.0.0.0/0
.............................

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
cali-OUTPUT  all  --  0.0.0.0/0            0.0.0.0/0            /* cali:tVnHkvAo15HuiPy0 */
KUBE-PROXY-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes load balancer firewall */
KUBE-SERVICES  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes service portals */
KUBE-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0
OUTPUT_direct  all  --  0.0.0.0/0            0.0.0.0/0

...................................

Chain KUBE-FIREWALL (2 references)
target     prot opt source               destination
DROP       all  -- !127.0.0.0/8          127.0.0.0/8          /* block incoming localnet connections */ ! ctstate RELATED,ESTABLISHED,DNAT
DROP       all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes firewall for dropping marked packets */ mark match 0x8000/0x8000

Chain KUBE-FORWARD (1 references)
target     prot opt source               destination
DROP       all  --  0.0.0.0/0            0.0.0.0/0            ctstate INVALID
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes forwarding rules */ mark match 0x4000/0x4000
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes forwarding conntrack rule */ ctstate RELATED,ESTABLISHED
.....................................
```
filter 表上挂了5条链，分别是 INPUT、FORWARD 和 OUTPUT 这三条系统内置链，以及 KUBE-FIREWALL 和 KUBE-FORWARD 这两条用户自定义链, iptables 的内置链都有默认规则

```bash
Chain OUTPUT (policy ACCEPT)
```
用户自己定义的链后面都有一个引用计数，在我们的例子中 KUBE-FIREWALL 和 KUBE-FORWARD 都有一个引用，它们分别在 INPUT 和 FORWARD 中被引用.

iptables 的每条链下面的规则处理顺序是从上到下逐条遍历的，除非中途碰到 DROP，REJECT，RETURN 这些内置动作。如果 iptables 规则前面是自定义链，则意味着这条规则的动作是 JUMP，即跳到这条自定义链遍历其下的所有规则，然后跳回来遍历原来那条链后面的规则。

#### 2.配置内置链的默认策略

当然，我们可以自己配置内置链的默认策略，决定是放行还是丢弃，如下所示
```bash
# INPUT 链默认策略 丢弃
iptables --policy INPUT DROP
# FORWARD 链默认为 丢弃
iptables --policy FORWARD DROP
```


#### 3.配置防火墙规则策略

防火墙策略一般分为通和不通两种。
+ 如果默认策略是“全通”，例如上文的policy  ACCEPT，就要定义一些策略来封堵；即黑名单
+ 如果默认策略是“全不通”，例如上文的policy  DROP，就要定义一些策略来解封。即白名单


1. 配置允许SSH连接

```bash
iptables -A INPUT -s 10.23.23.12/24 -p tcp -dport 22 -j ACCEPT
```
+ -A的意思是以追加(Append)的方式增加这条规则。
+ -A INPUT 表示这条规则挂在 INPUT 链上。
+ -s 10.20.30.40/24 表示允许源(source)地址是 10.20.30.40/24 这个网段的连接。
+ -p  tcp 表示允许 TCP(protocol)包通过。
+ --dport  22 的意思是允许访问的目的端口(destination  port)为 22，即SSH端口。
+ -j  ACCEPT表示接受这样的连接。

综上所述，这条iptables规则的意思是允许源地址是10.20.30.40/24这个网段的包发到本地TCP 22端口。

除了按追加的方式添加规则，还可以使用 `iptables -I［chain］［number］` 将规则插入(Insert)链的指定位置。如果不指定number，则插到链的第一条处.

2. 阻止来自某个IP/网段的所有连接

阻止10.10.10.10上所有的包,也可以使用-j  REJECT，这样就会发一个连接拒绝的回程报文,客户端收到后立刻结束。不像-j  DROP那样不返回任何响应，客户端只能一直等待直到请求超时

```bash
iptables -A INPUT -s 10.10.10.10 -j DROP
```
如果要屏蔽一个网段，例如 10.10.10.0/24，则可以使用 `-s  10.10.10.0/24` 或 `10.10.10.0/255.255.255.0`。

如果要“闭关锁国”，即屏蔽所有的外来包，则可以使用 `-s 0.0.0.0/0`

3. 封锁端口

要阻止从本地1234端口建立对外连接，可以使用以下命令
```bash
iptables -A OUTPUT -p tcp --dport 1234 -j DROP
```
要在 OUTPUT 链上挂规则是因为我们的需求是屏蔽本地进程对外的连接。如果我们要阻止外部连接访问本地1234端口，就需要在INPUT链上挂规则
```bash
iptables -A INPUT -p tcp --dport 1234 DROP
```
4. 端口转发

把服务器的某个端口流量转发给另一个端口，例如，我们对外声称Web服务在80端口上运行，但由于种种原因80端口被人占了，实际的Web服务监听在8080端口上。为了让外部客户端能无感知地依旧访问80端口，可以使用以下命令实现端口转发：
```bash
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
```
`-p tcp--dport 80-j REDIRECT--to-port 8080`的意思是发到TCP 80端口的流量转发(REDIRECT)给8080端口

该规则应该发生在nat表上的PREROUTING链上。至于-i  eth0则表示匹配eth0网卡上接收到的包。

5. 禁用PING

大部分的公有云默认都是屏蔽ICMP的，即禁止ping报文
```bash
iptables -A INPUT -p icmp FROP
```
6. 删除规则

最暴力地清除当前所有的规则命令

```bash
iptables -F
```
要清空特定的表可以使用-t参数进行指定，例如清除nat表所有的规则如下

```bash
iptables -t nat -F
```
删除规则的最直接的需求是解封某条防火墙策略。因此，笔者建议使用 `iptables的-D` 参数,-D 表示从链中删除一条或多条指定的规则，后面跟的就是要删除的
规则
```bash
iptables -D INPUT -s 10.10.10.10 -j DROP
```

当某条链上的规则被全部清除变成空链后，可以使用-X参数删除：删除FOO这条用户自定义的空链，但需要注意的是系统内置链无法删除。

```bash
iptables -X FOO
```
7. 自定义链

自定义链创建

```bash
iptables -N BAR
```
在filter表(因为未指定表，所以可以使用-t参数指定)创建了一条用户自定义的链BAR。


#### 4.DNAT

DNAT根据指定条件 `修改数据包的目标IP地址和目标端口` 。DNAT 的原理和我们上文讨论的端口转发原理差不多，差别是端口转发不修改IP地址。使用iptables做目的地址转换的一个典型例子如下:

```bash
iptables -t nat -A PREROUTING -d 1.2.3.4  -p tcp -dport 80 -j DNAT  --to-destination 10.20.30.40:8080 
```

+ -j DNAT 表示目的地址转换
+ -d 1.2.3.4 -p tcp --dport 80 表示匹配的包,条件是访问目的地址和端口为1.2.3.4:80的TCP包
+ --to-destination 表示将该包的目的地址和端口修改成 10.20.30.40:8080。

同样，DNAT不修改协议。如果要匹配网卡，可以用 `-i  eth0` 指定收到包的网卡(i 是 input 的缩写)。需要注意的是，DNAT 只发生在 nat表的 `PREROUTING` 链，这也是我们要指定收到包的网卡而不是发出包的网卡的原因


当涉及转发的目的IP地址是外机时，需要确保启用 ip forward 功能，即把 Linux :

```bash
echo 1 > /proc/sys/net/ipv4/ip_forward
```

#### 5.SNAT/ 网络地址欺骗

神秘的网络地址欺骗其实是SNAT的一种。SNAT 根据指定条件修改数据包的源IP地址，即 DNAT 的逆操作。与 DNAT 的限制类似，SNAT 策略只能发生在 nat 表的 POSTROUTING 链。

```bash
ipttables -t nat -A POSTROUTING -s 192.168.26.12 -o eth0 -j SNAT -to-source 10.127.16.1 
```
+ -j  SNAT表示源地址转换
+ -s  192.168.1.12 表示匹配的包源地址是 192.168.1.12，
+ --to-source 表示将该包的源地址修改成 10.172.16.1。与DNAT类似
+ -o  eth0(o是output的缩写)匹配发包的网卡


至于网络地址伪装，与SNAT类似,其实就是一种特殊的源地址转换，报文从哪个网卡出就用该网卡上的IP地址替换该报文的源地址，具体用哪个IP地址由内核决定。下面这条规则的意思是：源地址是 10.8.0.0/16 的报文都做一次 `Masq` 。

```bash
iptable -t nat -A POSTROUTING -s 10.8.0.0/16 -j MASQUERADE
```

#### 6.保存与恢复

上述方法对iptables规则做出的改变是临时的，重启机器后就会丢失。如果想永久保存这些更改，则需要运行以下命令：

```bash
iptables-save > iptables.bak
```

可以使用iptables-restore命令还原iptables-save命令备份的iptables配置，原理就是逐条执行文件里的iptables规则

```bash
iptables-restore < iptables.bak
```


