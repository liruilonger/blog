---
title: Kubernetes 面试手册：100 个问答
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-02-04 07:29:57/Kubernetes 手册：100 个问答的综合指南.html'
mathJax: false
date: 2023-02-04 15:29:57
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

## 入门部分

### 1.什么是 Kubernetes？

答： kubernertes 是一个谷歌开源的 容器编排系统，也叫 K8s，用于实现自动化跨主机容器化应用程序部署，扩展和管理， 简单理解，它将原本的基于 docker、podman 管理的多个容器应用所在的主机抽象成 对应的 逻辑单元，在k8s中用 Pod 这个 API 对象来表示。一个 pod 包含一组容器，和一个基础镜像容器，k8s  希望处理容器化应用程序的完整生命周期，包括部署、扩展和管理。它广泛用于云计算环境，并且在本地环境中也越来越受欢迎。


Kubernetes 十分灵活和可扩展，因此可用于管理范围广泛的工作负载，包括微服务、批处理作业和有状态应用程序。它还被设计为具有高可用性，因此即使集群中的各个节点出现问题，它也可以从故障中恢复并继续运行。

k8s 抽象了常见的部署行为， 提供了包括 工作负载，有状态无状态应用，服务，批处理，卷的管理等常见的 API资源，k8s 通过 etcd 存储集群所有数据，


Kubernetes 使用命令行界面 (CLI) 或通过基于 Web 的用户界面 (UI) 进行管理。它可以部署在各种平台上，包括本地服务器、公共云提供商和混合云环境

### 2. Kubernetes 集群的主要组件是什么？

答。Kubernetes 集群由一组运行容器化应用程序的工作机器(称为节点)组成。节点通过网络相互通信，以协调应用程序的部署和扩展。

Kubernetes 集群有几个主要组件：

#### `主节点(master)/控制平面`：

这是集群的中央控制平面，负责管理集群的状态和响应API请求。主节点运行许多组件，包括 API 服务器、调度程序和 etcd(分布式键值存储)。

`Kubernetes API Server (kube-apiserver)`:	提供了HTTP Rest接口的关键服务进程,是Kubernetes里所有资源的增、删、改、查等操作的唯一入口,也是集群控制的入口进程。
`Kubernetes Controller Manager (kube-controller-manager)`:	Kubernetes里所有资源对象的自动化控制中心,可以理解为资源对象的“大总管”。
`Kubernetes Scheduler (kube-scheduler)`:	负责资源调度(Pod调度)的进程,相当于公交公司的“调度室”。
`etcd`:	在Master节点上还需要启动一个etcd服务,因为Kubernetes里的所有资源对象的数据全部是保存在etcd中的



#### `工作节点/数据平面`：

这些是运行容器化应用程序的机器。每个工作节点运行一个容器运行时(如 Docker)，以及  kubelet 和 kube-proxy 组件，它们负责与主节点通信并管理节点上的容器。

`kubelet`	:负责Pod对应的容器的创建、启停等任务,同时与Master节点密切协作,实现集群管理的基本功能。
`kube-proxy`:	实现 Kubernetes Service 的通信与负载均衡机制的重要组件。
`Docker Engine	(docker)`: Docker引擎,负责本机的容器创建和管理工作。



#### Pod：

这些是 Kubernetes 中最小的可部署单元。Pod 是一组部署在同一节点上的一个或多个容器。Pod 是 Kubernetes 应用程序的基本构建块，用于托管容器化应用程序。
#### Service 

这些服务为访问一组 pod 提供稳定的网络端点。服务可用于将一组 pod 暴露给集群的其他部分或外部用户。
#### deployments

这些用于管理一组 pod 副本的生命周期。部署可用于以声明方式创建、更新和扩展 pod。

#### ingress 

这是一种向外部用户公开服务的方式。入口可用于根据传入请求的主机名或路径将流量路由到服务。

Kubernetes 中还有许多其他组件和特性，包括持久卷、秘密和命名空间，可用于进一步组织和管理集群中的应用程序。

### Kubernetes 中的 Pod 是什么？

答。在 Kubernetes 中，pod 是部署的基本单元。它是一组部署在同一个工作节点上的一个或多个容器。Pod 用于在 Kubernetes 中托管容器化应用程序。

每个 pod 在集群内都分配了一个唯一的 IP 地址，并且 pod 内的容器可以使用 localhost 相互通信。Pod 还分配了一个主机名，该主机名派生自 Pod 的名称。

Pod 被设计为短暂的，这意味着它们不会长期存在。如果一个 pod 发生故障或被删除，预计它会被一个新的 pod 取代。这种设计允许集群具有灵活性和弹性，因为它允许应用程序快速从故障中恢复。

Pod 通常使用更高级别的抽象进行管理，例如部署或复制集，这允许您以声明方式指定一组 Pod 的所需状态。




### Kubernetes 中的节点是什么？

答。在 Kubernetes 中，节点是运行容器化应用程序的工作机器。节点是托管组成应用程序的 pod 的机器。

每个节点运行许多组件，包括容器运行时(例如 Docker)、Kubernetes kubelet 和 kube-proxy 进程以及其他支持实用程序。kubelet 负责与 Kubernetes master 节点通信，管理节点上的容器。kube-proxy 是在每个节点上运行的网络代理，负责将流量路由到正确的 pod。

节点可以是物理机也可以是虚拟机，这取决于部署环境。在云环境中，节点通常是由云提供商创建和管理的虚拟机。在本地环境中，节点可以是物理服务器或在管理程序上运行的虚拟机。

Kubernetes 集群中的节点通过网络相互通信，以协调应用程序的部署和扩展。主节点负责管理集群的状态并响应 API 请求，而工作节点则运行容器化应用程序。

### 什么是 Kubernetes 中的集群？

答。在 Kubernetes 中，集群是一组运行容器化应用程序的工作机器，称为节点。节点通过网络相互通信，以协调应用程序的部署和扩展。

Kubernetes 集群通常由一个主节点和一个或多个工作节点组成。主节点是集群的中央控制平面，负责管理集群的状态和响应API请求。工作节点是运行容器化应用程序的机器。

Kubernetes 集群可以部署在各种平台上，包括本地服务器、公共云提供商和混合云环境。它可以使用命令行界面 (CLI) 或通过基于 Web 的用户界面 (UI) 进行管理。

### Kubernetes API 的用途是什么？

Kubernetes API 是用户与 Kubernetes 集群交互的主要方式。它是一个 RESTful API，公开了许多用于创建、修改和删除 Kubernetes 资源(例如 pod、服务和部署)的端点。

API由API服务器实现，是Kubernetes主节点的核心组件。API 服务器负责处理 API 请求并根据这些请求更新集群的状态。

可以使用各种工具访问 Kubernetes API，包括 kubectl 命令行界面 (CLI)、Kubernetes 基于 Web 的用户界面 (UI) 和编程客户端。

Kubernetes API 有多种用途，包括：

在集群中部署和管理容器化应用程序 扩展应用程序的副本数量 更新应用程序的配置 监控应用程序的状态 执行应用程序的滚动更新 Kubernetes API 是 Kubernetes 系统的重要组成部分，因为它允许用户以声明方式指定其应用程序的所需状态，并允许集群自动协调系统的实际状态与所需状态。

### kubectl 有什么用？

答。kubectl 是用于与 Kubernetes 集群交互的命令行界面 (CLI)。它用于在 Kubernetes 集群中部署、管理和排除容器化应用程序的故障。

kubectl 是一种与 Kubernetes API 服务器通信以操纵集群所需状态的工具。它可用于创建、删除和更新资源，例如 pod、服务和部署。它还可用于查看集群的当前状态并解决问题。

kubectl 是一个强大的工具，允许您从命令行管理 Kubernetes 集群和应用程序。对于任何使用 Kubernetes 的人来说，它都是必不可少的工具，并且经常与其他工具结合使用，例如 Helm(Kubernetes 的包管理器)和 Skaffold(用于开发和部署应用程序到 Kubernetes 的工具)

### 什么是 Kubernetes 中的 deployment ？

Deployment 是管理复制应用程序的资源。它确保指定数量的 Pod 副本在任何给定时间都在运行。Deployment 负责创建和更新应用程序的副本。它通过创建和管理负责创建和管理各个 Pod 的 ReplicaSet 来实现这一点。

以下是 YAML 格式的部署示例：
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
spec:
  replicas: 3
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:
      containers:
      - name: my-app
        image: my-app:latest
        ports:
        - containerPort: 80
```
在此示例中，Deployment 创建并管理 my-app Pod 的三个副本。Pod 模板指定应用程序的容器镜像和端口。Deployment 使用 ReplicaSet 来确保指定数量的副本一直在运行，它也可以用来对应用程序执行滚动更新。




### Helm 有什么用？

Kubernetes 中的 helm 是一个包管理器，可帮助您在 Kubernetes 集群上安装、升级和管理应用程序。它使用“图表”(预先配置的 Kubernetes 资源包)来定义和部署应用程序。

例如，要使用 Helm 安装 NGINX Web 服务器，您可以使用以下代码

```bash
$ helm install nginx stable/nginx-ingress
```

这将从“稳定”存储库安装 NGINX chart，并创建所有必要的 Kubernetes 资源，例如 Deployments、Services 和 Ingress 对象，以运行 NGINX Web 服务器。

您还可以通过传入其他参数来自定义图表，例如副本数或要使用的图像标签

```
helm install nginx stable/nginx-ingress --set replicas=2,image.tag=1.17.6
```

这将使用指定的图像标签创建一个包含 2 个 NGINX 网络服务器副本的 Deployment。


### Deployment如何保证一个pod的指定数量的replicas一直在运行？

在 Kubernetes 中，Deployment 是一种管理 Pod 的一组副本的资源。它通过根据需要创建和删除 pod 来确保指定数量的 pod 副本始终运行。

例如，假设您有一个 Deployment，其中包含 3 个运行 NGINX Web 服务器的 pod 的副本集。如果其中一个 pod 由于错误或节点故障而宕机，Deployment 将自动创建一个新的 pod 来替换它，确保始终有 3 个副本在运行。

您可以在 Deployment 配置文件中指定副本数，以及其他详细信息，例如 pod 模板、标签和就绪探测。下面是创建 NGINX pod 的 3 个副本的示例部署配置文件：

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.17.6
        ports:
        - containerPort: 80
          protocol: TCP
```

```bash
kubectl apply -f deployment.yaml
```

这将创建 NGINX pod 的 3 个副本，它们将由 Deployment 自动管理。如果任何 pod 出现故障，Deployment 将创建一个新的 pod 来替换它，确保始终有 3 个副本在运行。



### 什么是 Kubernetes 中的 Service？

答。在 Kubernetes 中，Service 是一种资源，它允许您通过稳定的网络端点访问一组 Pod。它充当负载均衡器，根据服务的配置将流量路由到适当的 pod。

Kubernetes 中有多种类型的服务，每种都有不同的用途。一些常见的类型包括：

+ ClusterIP：在集群内部 IP 上公开服务，该 IP 只能从集群内部访问。
+ NodePort：将Service暴露在集群中每个节点的特定端口上，允许外部流量访问Service。
+ LoadBalancer：在云提供商提供的外部负载均衡器上公开服务。
+ ExternalName：将服务映射到外部 DNS 名称，而不是一组 pod。

下面是一个示例服务配置文件，它为 NGINX pod 的部署创建一个 ClusterIP 服务：

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: nginx-rc
spec:
  replicas: 3
  selector:
    app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.17.6
        ports:
        - containerPort: 80
          protocol: TCP
```
这将创建一个服务，该服务根据服务配置中指定的选择器将流量路由到 NGINX pod。该服务可以从集群内部的集群内部 IP 访问，但不能从集群外部访问。

您还可以根据需要指定不同的端口或类型(例如 NodePort 或 LoadBalancer)来自定义服务。


### 什么是 Kubernetes 中的 Ingress？

答。在 Kubernetes 中，Ingress 是一种资源，可让您将应用程序暴露给外界。它充当反向代理，根据主机名和路径将传入流量路由到适当的服务。

Ingress 可以配置规则，根据传入请求的主机名和路径指定将流量路由到哪个服务。它还可以配置 TLS 证书和其他选项，例如速率限制或身份验证。

下面是一个 Ingress 配置文件示例，它根据主机名将流量路由到 NGINX 服务：

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: nginx-ingress
  annotations:
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - host: example.com
    http:
      paths:
      - path: /
        backend:
          serviceName: nginx
          servicePort: 80
```

这将创建一个 Ingress 资源，它根据主机名将流量路由到 NGINX 服务。如果用户访问位于“example.com”的网站，Ingress 会将请求路由到 NGINX 服务。

您还可以使用 Helm 安装 Ingress 资源。例如，要使用 Helm 安装 NGINX Ingress 控制器，您可以使用以下代码：

```bash
$ helm install nginx stable/nginx-ingress
```

### 什么是 Kubernetes 中的 Volume 卷？

Kubernetes 中的卷是一种在 Pod 的生命周期之后将数据持久保存在 Pod 中的方法。它允许您将数据存储在一个共享存储位置，同一 pod 中的多个容器可以访问该位置。

Kubernetes 中可以使用多种类型的卷，例如 hostPath、emptyDir、configMap、secret 等等。

例如，您可以使用 emptyDir 卷在 pod 中创建临时存储位置：

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
spec:
  containers:
  - name: nginx
    image: nginx:1.17.6
    volumeMounts:
    - name: temp-volume
      mountPath: /tmp/data
  volumes:
  - name: temp-volume
    emptyDir: {}
```

这将创建一个名为“temp-volume”的卷，该卷安装在 pod 中的“/tmp/data”路径中。写入此位置的任何数据都将保留在卷中，并且可供 pod 中的所有容器使用。

您还可以使用 hostPath 卷将主机节点文件系统中的目录挂载到 pod 中：

```bash
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
spec:
  containers:
  - name: nginx
    image: nginx:1.17.6
    volumeMounts:
    - name: host-volume
      mountPath: /var/www/html
  volumes:
  - name: host-volume
    hostPath:
      path: /var/www/html
```

### Kubernetes 中的 PersistentVolume 是什么？

答。Kubernetes 中的 PersistentVolume (PV) 是一块已配置供 Pod 使用的存储。它提供了一种在 pod 的生命周期之后将数据持久保存在 pod 中的方法，允许它被其他 pod 甚至跨不同的部署重用。

PV 可以由不同类型的存储支持，例如本地磁盘、网络附加存储 (NAS) 或基于云的存储解决方案，如 Amazon S3 或 Google Cloud Storage。

要在 Kubernetes 中创建 PV，您可以使用 PersistentVolumeClaim (PVC) 资源。PVC 指定所需的存储大小和类型，以及任何其他相关详细信息，例如访问模式和标签。然后根据 PVC 的规范创建 PV 并绑定到它。

下面是一个 PVC 配置文件示例，它创建了一个具有 1GB 存储空间和“ReadWriteOnce”访问模式的 PV：
```bash
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-1gb
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```
这将创建一个具有 1GB 存储空间的 PV 并将其绑定到 PVC。然后，您可以使用 pod 中的 PVC 将 PV 作为卷挂载，从而允许 pod 访问持久存储。

例如，要在 NGINX pod 中使用 PVC，您可以将卷和卷挂载添加到 pod 的配置文件中：

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: nginx-pod
spec:
  containers:
  - name: nginx
    image: nginx:1.17.6
    ports:
    - containerPort: 80
      protocol: TCP
    volumeMounts:
    - name: pvc
      mountPath: /var/www/html
  volumes:
  - name: pvc
    persistentVolumeClaim:
      claimName: pvc-1gb
```

### 什么是 Kubernetes 中的 PersistentVolumeClaim？

答。在 Kubernetes 中，PersistentVolumeClaim (PVC) 是一种允许 Pod 请求和使用持久存储的资源。它用于从 PersistentVolume (PV) 中申请特定数量的存储，PV 是集群中已配置的一块存储。

PVC 包含对特定存储量以及访问模式(例如读写或只读)的请求。当 Pod 请求 PVC 时，Kubernetes 调度器会将 PVC 绑定到满足请求要求的 PV。

下面是一个请求 1GB 读写存储的 PVC 配置文件示例：
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: nginx-pvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

### 什么是 Kubernetes 中的 SC？

### 什么是 Kubernetes 中的 ConfigMap？

。Kubernetes 中的 ConfigMap 是一种将配置数据存储为键值对的资源。它可用于存储应用程序的配置数据，例如数据库凭证、API 密钥和其他设置。

ConfigMaps 很有用，因为它们允许您将配置数据存储在一个中央位置，并从 Kubernetes 集群中的多个位置引用它，例如 pod、Deployments 和 Services。这使得管理和更新配置数据变得更加容易，因为您只需在一个地方更改它。

要创建 ConfigMap，您可以使用 kubectl 命令并在配置文件中指定键值对，如下所示：

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-config-map
data:
  key1: value1
  key2: value2
  key3: value3
```
要在 pod 中使用 ConfigMap，您可以使用卷挂载或使用环境变量来引用它。例如，要将 ConfigMap 作为卷安装在 pod 中，您可以在 pod 配置文件中包含以下内容：

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - name: my-container
    image: my-image
    volumeMounts:
    - name: config-volume
      mountPath: /etc/config
  volumes:
  - name: config-volume
    configMap:
      name: my-config-map
```

### 什么是 Kubernetes 中的 Secret ？

答。在 Kubernetes 中，Secret 是一种存储敏感信息的资源，例如密码、API 密钥或 SSL 证书。机密以加密形式存储，集群中的 pod 或容器可以使用这些机密来访问敏感资源或服务。

例如，您可以创建一个 Secret 来存储您的应用程序连接到数据库所需的数据库密码。要创建 Secret，您可以使用 kubectl 命令并将敏感信息作为参数传递：

```bash
$ kubectl create secret tls tls-certificate --key=key.pem --cert=cert.pem
$ kubectl create secret generic db-password --from-literal=password=mysecretpassword

```

创建 Secret 后，它可以作为卷安装在 pod 或容器中，并作为文件访问。例如，要在 pod 中使用“db-password”Secret，您可以在 pod 的配置文件中包含以下内容：
```bash
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mycontainer
    image: myimage
    volumeMounts:
    - name: secret-volume
      mountPath: /etc/secrets
  volumes:
  - name: secret-volume
    secret:
      secretName: db-password
```

### 什么是 Kubernetes 中的 StatefulSet？

答。Kubernetes 中的 StatefulSet 是一种管理一组复制的 Pod 的资源，类似于 Deployment。然而，与设计为无状态和短暂的部署不同，StatefulSet 用于管理需要持久存储和稳定网络身份的有状态应用程序。

StatefulSets 提供了几个使其适用于有状态应用程序的特性：

持久存储：StatefulSet 中的每个 pod 都有一个唯一的持久卷声明 (PVC)，它自动绑定到一个持久卷 (PV)。这可确保即使删除或重新安排 pod，也能保留 pod 的数据。稳定的网络身份：StatefulSet 中的每个 pod 都被赋予一个唯一的、稳定的网络身份，例如主机名或 DNS 名称。这使得连接到 pod 和访问它们的数据变得更加容易。有序部署和伸缩：StatefulSet 中的 Pod 按特定顺序部署，并且一次只能向上或向下扩展一个。这允许您以可预测的方式管理有状态应用程序的部署和扩展。要创建 StatefulSet，您可以使用类似于如下所示的配置文件：

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mysql
spec:
  serviceName: mysql
  replicas: 3
  selector:
    matchLabels:
      app: mysql
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
      - name: mysql
        image: mysql:8.0
        env:
        - name: MYSQL_ROOT_PASSWORD
          value: "password"
        ports:
        - containerPort: 3306
          protocol: TCP
        volumeMounts:
        - name: mysql-persistent-storage
          mountPath: /var/lib/mysql
  volumeClaimTemplates:
  - metadata:
      name: mysql-persistent-storage
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 5Gi
```

此配置文件创建一个 StatefulSet，其中包含 MySQL pod 的 3 个副本，每个副本都具有唯一的持久卷声明和网络身份。



### 什么是 Kubernetes 中的 DaemonSet？

答。Kubernetes 中的 DaemonSet 是一种资源，可确保指定数量的 Pod 副本在集群中的每个节点上运行。它用于运行需要出现在每个节点上的后台或“守护进程”进程，例如监视或日志代理。

例如，假设您想在集群中的每个节点上运行 Fluentd 日志收集代理。您可以使用 DaemonSet 来实现这一点

```yaml
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: fluentd-daemonset
spec:
  selector:
    matchLabels:
      app: fluentd
  template:
    metadata:
      labels:
        app: fluentd
    spec:
      containers:
      - name: fluentd
        image: fluentd:latest
        volumeMounts:
        - name: varlog
          mountPath: /var/log
        - name: varlibdockercontainers
          mountPath: /var/lib/docker/containers
          readOnly: true
      volumes:
      - name: varlog
        hostPath:
          path: /var/log
      - name: varlibdockercontainers
        hostPath:
          path: /var/lib/docker/containers
```

将在集群中的每个节点上创建一个 Fluentd pod，确保始终有一个 pod 的副本在每个节点上运行。如果一个节点出现故障或被添加到集群中，DaemonSet 将根据需要自动创建或删除 pod 以维护所需数量的副本。

### 什么是 Kubernetes 中的 job ？

```yaml

```

### 什么是 Kubernetes 中的 CronJob？

### 什么是 Kubernetes 中的命名空间？

### 什么是 Kubernetes 中的标签？

### 什么是 Kubernetes 中的注解？

### 什么是 Kubernetes 中的选择器？

### 什么是 Kubernetes 中的标签选择器？

### 什么是 Kubernetes 中的 ResourceQuota？

### Kubernetes 中的 LimitRange 是什么？

### 什么是 Kubernetes 中的 PodDisruptionBudget？

### 什么是 Kubernetes  中的  Qosclass

### Kubernetes 中的 role 是什么？

### Kubernetes 中的 ClusterRole 是什么？

### 什么是 Kubernetes 中的 RoleBinding？

### 什么是 Kubernetes 中的 ClusterRoleBinding？

### 什么是 Kubernetes 中的 ServiceAccount？

### 什么是 Kubernetes 中的 PodSecurityPolicy？

### Kubernetes 中的 RBAC 是什么？

### Kubernetes 中的 Horizo​​ntal Pod Autoscaler 是什么？

### Kubernetes 中的 Vertical Pod Autoscaler 是什么？

### 什么是 Kubernetes 中的集群自动缩放器？

### Kubernetes 中的 DeploymentRollback 是什么？

### 什么是 Kubernetes 中的 DeploymentStrategy？

### Kubernetes 中的 DeploymentTrigger 是什么？

### Kubernetes 中的 DeploymentSpec 是什么？

### 什么是 Kubernetes 中的 DeploymentStatus？

### Kubernetes 中的 DeploymentCondition 是什么？

### Kubernetes 中的 DeploymentRollback 是什么？

### 什么是 Kubernetes 中的 DeploymentHistory？

### 什么是 Kubernetes 中的 DeploymentStrategy？

### Kubernetes 中的 DeploymentTrigger 是什么？

### Kubernetes 中的 DeploymentSpec 是什么？

### Kubernetes 中的 DeploymentSpec 是什么？

### 什么是 Kubernetes 中的 DeploymentStatus？

### Kubernetes 中的 DeploymentCondition 是什么？

### Kubernetes 中的 DeploymentRollback 是什么？

### 什么是 Kubernetes 中的 DeploymentHistory？

### 什么是 Kubernetes 中的 DeploymentStrategy？

### Kubernetes 中的 DeploymentTrigger 是什么？

### 什么是 Kubernetes 中的 PodSpec？

### 什么是 Kubernetes 中的 PodStatus？

### 什么是 Kubernetes 中的 PodCondition？

### Kubernetes 中的 PodContainerStatus 是什么？

### 什么是 Kubernetes 中的 PodEvent？

### 什么是 Kubernetes 中的 PodIP？

### 什么是 Kubernetes 中的 PodTemplateSpec？

### 什么是 Kubernetes 中的 ReplicationControllerSpec？

### 什么是 Kubernetes 中的 ReplicationControllerStatus？

### Kubernetes 中的 ReplicationControllerCondition 是什么？

### Kubernetes 中的 ServiceSpec 是什么？

### 什么是 Kubernetes 中的服务状态？

### 什么是 Kubernetes 中的服务端口？

### 什么是 Kubernetes 中的端点子集？

### Kubernetes 中的 EndpointPort 是什么？

### 什么是 Kubernetes 中的端点地址？

### Kubernetes 中的 IngressSpec 是什么？

### 什么是 Kubernetes 中的 IngressStatus？

### Kubernetes 中的 IngressRule 是什么？

### Kubernetes 中的 IngressPath 是什么？

### 什么是 Kubernetes 中的 IngressBackend？

### Kubernetes 中的 VolumeMount 是什么？

### 什么是 Kubernetes 中的 PersistentVolumeSpec？

### Kubernetes 中的 PersistentVolumeStatus 是什么？

### 什么是 Kubernetes 中的 PersistentVolumeClaimSpec？

### 什么是 Kubernetes 中的 PersistentVolumeClaimStatus？

### 什么是 Kubernetes 中的 ConfigMapVolumeSource？

### 什么是 Kubernetes 中的 SecretVolumeSource？

### 什么是 Kubernetes 中的 StatefulSetSpec？

### 什么是 Kubernetes 中的 StatefulSetStatus？

### Kubernetes 中的 StatefulSetVolumeClaim 是什么？

### 什么是 Kubernetes 中的 DaemonSetSpec？

### 什么是 Kubernetes 中的 DaemonSetStatus？

### 什么是 Kubernetes 中的 JobSpec？

### Kubernetes 中的 JobStatus 是什么？

### Kubernetes 中的 JobCondition 是什么？

### 什么是 Kubernetes 中的 CronJobSpec？

### 什么是 Kubernetes 中的 CronJobStatus？

### 什么是 Kubernetes 中的 CronJobSuspend？

### 什么是 Kubernetes 中的 CronJobSchedule？

### 什么是 Kubernetes 中的 CronJobConcurrencyPolicy？

### Kubernetes 中的 CronJobStartingDeadlineSeconds 是什么？

### 什么是 Kubernetes 中的 CronJobSuccessfulJobsHistoryLimit？


## 中级部分

###  Kubernetes 中如何实现 Pod 的均匀分布

###  Kubernetes 中的 无头服务如何实现






## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***
https://medium.com/@AceTheCloud-Abhishek/the-kubernetes-handbook-a-comprehensive-guide-of-100-q-a-e680199e6e22

***

© 2018-2023 liruilonger@gmail.com，All rights reserved. 保持署名-非商用-自由转载-相同方式共享(创意共享3.0许可证)

