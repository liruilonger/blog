---
title: SRE之前端服务器的负载均衡
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-08-16 13:56:29/SRE之前端服务器的负载均衡.html'
mathJax: false
date: 2022-08-16 21:56:29
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 今天和小伙伴们分享一些前端服务的负载均衡技术
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

运维大型系统时，将所有鸡蛋放在一个篮子里是引来灾难的最好办法。所以从容灾，负载均衡角度考虑，一般的大型系统，受众较多的，比如电信运营商的系统，都会有多个数据中心，根据流量分布，地域划分，同时考虑地理位置，电费，房价,人力等相关的经济因素，负载均衡的同时，解决了容灾问题。

负载的模式有很多种，主备，轮询，随机，哈希一致性负载等，比如主备负载，正常业务可以走省级别的数据中心，然后数据增量同步给集团数据中心，当省数据中心故障时，可以切到集团的地址。

今天我们来看看高层次的负载均衡，Google是如何在数据中心之间调节用户流量的。

## 有时候硬件并不能解决问题


`用户流量负载均衡(traffic load balancing)系统`是用来决定数据中心中的这些机器中哪一个用来处理某个请求的。

理想情况下，用户流量应该最优地分布于多条网络链路上、多个数据中心中，以及多台服务器上。但是这里的 **“最优”是如何定义的呢？**

并没有一个独立的答案，最优严重依赖于下列几个因素：

+ `逻辑层级(是在全局还是在局部)`
+ `技术层面(硬件层面与软件层面)`
+ `用户流量的天然属性`


讨论以下两个常见的用户流量场景：一个`搜索请求`和一个`视频上传请求`。



用户想要很快地获取搜索结果，所以对 `搜索请求来说最重要的变量是延迟(latency)`。对于视频上传请求来说，用户已经预期该请求将要花费一定的时间，但是同时希望该请求能够一次成功，所以这里`最重要的变量是吞吐量(throughput)`。

两种请求用户的`需求`不同，是我们在 **全局层面** 决完“最优”分配方然的`重要条件`


+ **最小化请求的延迟** :搜索请求将会被发往最近的、可用的数据中心:评价条件是`数据包往返时间(RTT)`，
+ **最大化吞吐量**: 视频上传流将会采取另外一条路径——也许是一条目前带宽没有占满的链路—来最大化吞吐量，同时也许会牺牲一定程度的延迟。


**局部层面**: 在一个数据中心内部，我们经常假设同一个物理建筑物内的所有物理服务器都在同一个网络中，对用户来说都是等距的。因此在这个层面上的`“最优”分配往往关注于优化资源的利用率，避免某个服务器负载过高`。


在现实中，很多其他因素也都在“最优”方案的考虑范围之内：有些请求可能会被指派到某个稍远一点的数据中心，以保障该数据中心的缓存处于有效状态。或者某些非交互式请求会被发往另外一个地理区域，以避免网络拥塞。

负载均衡，尤其是大型系统的负载均衡，是非常复杂和非常动态化的。


Google在多个层面上使用负载均衡策略来解决这些问题：
+

```bash
PS E:\docker> nslookup
默认服务器:  UnKnown
Address:  fe80::1

> set type=ns
> baidu.com.
服务器:  UnKnown
Address:  fe80::1

非权威应答:
baidu.com       nameserver = ns7.baidu.com
baidu.com       nameserver = ns3.baidu.com
baidu.com       nameserver = dns.baidu.com
baidu.com       nameserver = ns2.baidu.com
baidu.com       nameserver = ns4.baidu.com

(root)  ??? unknown type 41 ???
> a.shifen.com.
服务器:  UnKnown
Address:  fe80::1

非权威应答:
a.shifen.com    nameserver = ns5.a.shifen.com
a.shifen.com    nameserver = ns4.a.shifen.com
a.shifen.com    nameserver = ns2.a.shifen.com
a.shifen.com    nameserver = ns1.a.shifen.com
a.shifen.com    nameserver = ns3.a.shifen.com

(root)  ??? unknown type 41 ???
> google.com.
服务器:  UnKnown
Address:  fe80::1

非权威应答:
google.com      nameserver = ns2.google.com
google.com      nameserver = ns3.google.com
google.com      nameserver = ns1.google.com
google.com      nameserver = ns4.google.com
> set type=cname
> baidu.com
服务器:  UnKnown
Address:  fe80::1

baidu.com
        primary name server = dns.baidu.com
        responsible mail addr = sa.baidu.com
        serial  = 2012145855
        refresh = 300 (5 mins)
        retry   = 300 (5 mins)
        expire  = 2592000 (30 days)
        default TTL = 7200 (2 hours)
(root)  ??? unknown type 41 ???
> a.shifen.com.
服务器:  UnKnown
Address:  fe80::1

a.shifen.com
        primary name server = ns1.a.shifen.com
        responsible mail addr = baidu_dns_master.baidu.com
        serial  = 2210170024
        refresh = 5 (5 secs)
        retry   = 5 (5 secs)
        expire  = 2592000 (30 days)
        default TTL = 3600 (1 hour)
(root)  ??? unknown type 41 ???
> google.com.
服务器:  UnKnown
Address:  fe80::1

google.com
        primary name server = ns1.google.com
        responsible mail addr = dns-admin.google.com
        serial  = 481451779
        refresh = 900 (15 mins)
        retry   = 900 (15 mins)
        expire  = 1800 (30 mins)
        default TTL = 60 (1 min)
(root)  ??? unknown type 41 ???
>
```

## 使用DNS进行负载均衡

在某个客户端发送HTTP请求之前，会通过域名获取IP，需要先通过系统底层Socket库的解析器来向DNS服务器发起请求查询IP地址。DNS系统会通过域名和地址的映射表中查找相关的记录，然后返回ip，这就为我们第一层的负载均衡机制提供了一个良好基础：
```bash
PS E:\docker> nslookup www.baidu.com
服务器:  UnKnown
Address:  fe80::1

非权威应答:
名称:    www.a.shifen.com
Addresses:  220.181.38.149
          220.181.38.150
Aliases:  www.baidu.com

PS E:\docker> ping www.baidu.com

正在 Ping www.a.shifen.com [220.181.38.149] 具有 32 字节的数据:
来自 220.181.38.149 的回复: 字节=32 时间=18ms TTL=53
来自 220.181.38.149 的回复: 字节=32 时间=18ms TTL=53
来自 220.181.38.149 的回复: 字节=32 时间=16ms TTL=53
来自 220.181.38.149 的回复: 字节=32 时间=18ms TTL=53

220.181.38.149 的 Ping 统计信息:
    数据包: 已发送 = 4，已接收 = 4，丢失 = 0 (0% 丢失)，
往返行程的估计时间(以毫秒为单位):
    最短 = 16ms，最长 = 18ms，平均 = 17ms
PS E:\docker>
```

**DNS负载均衡**，最简单的方案是在DNS回复中提供多个`A记录或者AAAA记录`(ipv4&ipv6)，由客户端任意选择一个IP地址使用。这种方案虽然看起来简单并且容易实现，但是存在很多问题。


**第一个问题** 是这种机制对客户端行为的约束力很弱：`记录是随机选择的`，也就是每条记录都会引来有基本相同数量的请求流量。如何避免这个问题呢？

理论上我们可以使用`SRV记录来指明每个IP地址的优先级和比重`，但是HTTP协议目前还没有采用SRV记录。(某些互联网协议，如 `IMAP、SIP 和 XMPP`，除了与特定的服务器连接外，还需要连接到一个特定的端口。SRV 记录是在 DNS 中指定端口,优先级和权重等)

`_xmpp._tcp.example.com. 86400 IN SRV 10 5 5223 server.example.com`
```bash
服务 XMPP
原型协议 TCP
域名 example.com
TTL(有效期) 86400
class(网络类型) IN
在提示下键入 SRV
优先级 10
权重 5
端口 5223
目标 server.example.com
```

**另一个问题** 是客户端`无法识别“最近”的地址`。我们可以通过提供一个` anycast DNS服务器`地址，通过DNS请求一般会到达最近的地址这种方式来一定程度上缓解这个问题。内部DNS,服务器可以使用最近的数据中心地址来生成DNS回复。

更进一步的优化方式是，`将所有的网络地址和它们对应的大概物理位置建立一个对照表，按照这个对照表来发送回复。`但是这种解决方案使得我们需要维护一个更加复杂的DNS服务，并且需要维护一个`数据更新流水线(pipeline)`来保证位置信息的正确性。




当然，没有一个很简单的方案，因为这是由`DNS的基本特性`决定的：`最终用户很少直接跟权威域名服务器(authoritive nameserver)直接联系。在用户到权威服务器中间经常有一个递归解析器(recursive nameserver)代理请求。该递归解析器代理用户请求，同时经常提供一定程度的缓存机制。`

这样的DNS中间人机制在用户流量管理上有三个非常重要的影响：
+ 递归方式解析IP
+ 不确定的回复路径
+ 额外的缓存问题



## 负载均衡：虚拟IP

`虚拟IP地址(VIP)不是绑定在某一个特定的网络接口上的，它是由很多设备共享的`。所以说感觉和`代理`是有本质区别的，代理往往是一个绑定到一个特定的网卡,主要用于负载均衡，这里讲的VIP我理解代理是虚拟IP+负载均衡的一种设计。

当然，从用户视角来看，VIP仍然是一个独立的、普通的lP地址。理论上来讲，这种实现可以让我们将底层实现细节隐藏起来(比如某一个VIP背后的机器数量)，无缝进行维护工作。

比如我们可以依次升级某些机器，或者在资源池中增加更多的机器而不影响用户。



数据到达数据中心内部之后，我们可以在内部采用更大的`MTU`来避免碎片重组的发生，但是这种做法需要`网络设备支持`。`尽早进行负载均衡`，以及`分级多次进行`

## 博文参考
