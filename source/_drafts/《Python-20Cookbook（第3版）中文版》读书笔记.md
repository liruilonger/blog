---
title: 《Python Cookbook(第3版)中文版》读书笔记
tags:
  - python
categories:
  - python
toc: true
recommend: 1
keywords: python
uniqueId: '2022-04-22 11:19:31/《Python Cookbook(第3版)中文版》读书笔记.html'
mathJax: false
date: 2022-04-22 19:19:31
output:
  word_document:
    highlight: tango
thumbnail:
---

**<font color="009688"> 我的手机里，最初是有`网抑云`的，上学时，不开心，会听应景的歌，偶尔看评论，虽不会唱，有种被感同身受。后来，手机存储不够，清理，提示卸载不常用的软件就卸载了，恍惚，好久不听歌了，想起在哪看到，`有些人二十岁就死了，等到八十岁才被埋`。犹记高中时日记里写到`希望自己现在就离开，终将会讨厌那个几年后的自己，担心几年后可能不想离开了....`。又，我太懦弱了,害怕极了这个世界--------山河已无恙**</font>

<!-- more -->
## 写在前面

***

+ 给小伙伴推荐`Python`书单，发现了这本书
+ 开卷获益，有一种想全部读完,呼之欲出的感受
+ 这么好的书在身边，居然都没看过,懊悔不已....
+ 遂连夜拜读，受益匪浅，这里整理笔记

**<font color="009688"> 我的手机里，最初是有`网抑云`的，上学时，不开心，会听应景的歌，偶尔看评论，虽不会唱，有种被感同身受。后来，手机存储不够，清理，提示卸载不常用的软件就卸载了，恍惚，好久不听歌了，想起在哪看到，`有些人二十岁就死了，等到八十岁才被埋`。犹记高中时日记里写到`希望自己现在就离开，终将会讨厌那个几年后的自己，担心几年后可能不想离开了....`。又，太懦弱了,害怕这个世界--------山河已无恙**</font>

***

## 第一章 数据结构和算法

Python内置了许多非常有用的`数据结构`，比如`列表(list)、集合(set)`以及`字典(dictionary)、元组(tuple)`。在`collections`模块中也包含了针对各种数据结构的解决方案。

### 将序列分解为单独的变量

**我们有一个包含N个元素的元组或序列,现在想将它分解为N个单独的变量。**

```bash
┌──(liruilong㉿Liruilong)-[/mnt/e/docker]
└─$python3
Python 3.9.10 (main, Jan 16 2022, 17:12:18)
[GCC 11.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> p = (4,5)
>>> x,y = p
>>> x
4
>>> y
5
>>>
```

居然可以这样，长见识了，类似于` JavaScript ES6 `中的`解构赋值`,如果当成函数对象来看，可以看做是`拆包`

```py
>>> data = [ 'ACME',50,91.1,(2012,12,21)]
>>> n,s,p,d = data
>>> n,s
('ACME', 50)
>>> d
(2012, 12, 21)
>>>
```

当然，也支持深层次解构

```py
>>> data = [ 'ACME',50,91.1,(2012,12,21)]
>>> name,shares,price,(year,mon,day)=data
>>> year
2012
>>> mon,day
(12, 21)
>>>
```

如果`元素的数量不匹配`，将得到一个错误提示。

```py
>>> p = (4,5)
>>> x,y,z = p
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: not enough values to unpack (expected 3, got 2)
>>>
```

实际上不仅仅只是`元组或列表`，只要对象恰好是`可迭代`的，那么就可以执行`分解操作`。这包括`字符串、文件、迭代器`以及`生成器`。

```py
>>> s = 'love'
>>> a,b,c,d = s
>>> a,b
('l', 'o')
>>> d
'e'
>>>
```

当做`分解操作`时，想丢弃某些特定的值。可以用 '_'充当占位符，，这个在` JavaScript ES6 `和` Golang `也都支持。

```py
>>> data = [ 'ACME',50,91.1,(2012,12,21)]
>>> _,_,a,_=data
>>> a
91.1
>>>
```

### 从任意长度的可迭代对象中分解元素

**需要从某个`可选代对象`中`分解出N个元素`，但是这个可迭代对象的长度可能超过N，这会导致出现`分解的值过多(too many values to unpack)`的异常。**

```py
>>> record=('Dave','davedexample.com','773-555-1212','1847-555-1212')
>>> name,email,*phone_numbers = record
>>> name
'Dave'
>>> phone_numbers
['773-555-1212', '1847-555-1212']
>>>
```

类似于`Java方法传值的可变参数`一样，但是要比`java高级的多`，java的可变参数只能最后一个,python 则可以在任意位置

```py
>>> record=('Dave','davedexample.com','1224965096@qq.com','773-555-1212')
>>> name,*email,phone = record
>>> name
'Dave'
>>> email
['davedexample.com', '1224965096@qq.com']
>>>
```

`* 式的语法在选代一个变长的元组序列时尤其有用`

```py
records = [
    ('foo', 1, 2),
    ('bar', 'hello'),
    ('foo', 3, 4),
]

def do_foo(x, y):
    print('foo', x, y)

def do_bar(s):
    print('bar', s)

for tag, *args in records:
    if tag == 'foo':
        do_foo(*args)
    elif tag == 'bar':
        do_bar(*args)
```

字符串拆分是的使用 :`''.split('')`

```py
>>> line='nobody:*:-2:-2:Unprivileged User:/var/empty:/usr/bin/false'
>>> uname,*fields,homedir,sh=line.split(':')
>>> uname
'nobody'
>>> sh
'/usr/bin/false'
>>> fields
['*', '-2', '-2', 'Unprivileged User']
>>>
```

也可 `'*' 和 '_'`两个连用，丢弃多个变量

```py
>>> uname,*_,homedir,sh=line.split(':')
>>> sh
'/usr/bin/false'
>>>
```

求和递归

```py
items=[1,10,7,4,5,9]
def sum(items): 
    head,*tail=items 
    return head+sum(tail) if tail else head
```

### 保存最后N个元素(队列)

**我们希望在迭代或是其他形式的处理过程中对最后几项记录做一个有限的历史记录统计。**

保存有限的历史记录可算是`collections.deque`的完美应用场景了

打印满足条件的最后5条记录

```py
#deque(maxlen=N)创建了一个固定长度的双端队列
from collections import deque

def search(lines, pattern, history=5):
    previous_lines=deque(maxlen=history)
    for line in lines:
        if pattern in line:
            # 生成器
            yield line, previous_lines
        previous_lines.append(line)
# Example use on a file
if __name__ == '__main__':
    with open('somefile.txt') as f:
        for line, prevlines in search(f,'python',5):
            for pline in prevlines:
                print(pline, end='')
            print(line, end='')
            print('-1'*20)
```

`deque(maxlen=N)`创建了一个固定长度的双端队列

```py
>>> from collections import deque
>>> q = deque()
>>> q.append(1)
>>> q.append(2)
>>> q.append(3)
>>> q
deque([1, 2, 3])
>>>
```

尽管可以在列表上手动完成这样的操作(append、del)，但队列这种解决方案要优雅得多，运行速度也快得多。从队列两端添加或弹出元素的复杂度都是`O(1)`。这和列表不同，当从列表的头部插入或移除元素时，列表的复杂度为`O(N)`

### 找到最大或最小的N个元素

**我们想在某个集合中找出最大或最小的N个元素。**

`heapq`模块中有两个函数`nlargest()和nsmallest()`,当所要找的`元素数量相对较小`时，函数`nlargest()和nsmallest()`才是最适用的,`nlargest()和nsmallest()`的实际实现会根据使用它们的方式而有所不同，可能会相应作出一些优化措施(比如，`当N的大小同输入大小很接近`时，就会采用`排序`的方法)。

```py
>>> import heapq
>>> nums=[1,8,2,23,7,-4,18,23,42,37,2]
>>> heapq.nlargest(3,nums)
[42, 37, 23]
>>> heapq.nsmallest(3,nums)
[-4, 1, 2]
>>>
```

这两个函数都可以接受一个参数key，从而允许它们工作在更加复杂的数据结构之上

```py
import  heapq

portfoli = [
    {'name': 'AAPL', 'shares': 50, 'price': 543.22},
    {'name': 'FB', 'shares': 200, 'price': 21.09},
    {'name': 'HPQ', 'shares': 35, 'price': 31.75},
    {'name': 'YHOO', 'shares': 45, 'price': 16.35},
    {'name': 'ACME', 'shares': 75, 'price': 115.65}
]
print(heapq.nsmallest(2,portfoli,key=lambda s: s['price']))
print(heapq.nlargest(2,portfoli,key=lambda s: s['shares']))
```

```bash
[{'name': 'YHOO', 'shares': 45, 'price': 16.35}, {'name': 'FB', 'shares': 200, 'price': 21.09}]
[{'name': 'FB', 'shares': 200, 'price': 21.09}, {'name': 'ACME', 'shares': 75, 'price': 115.65}]
```

如果正在寻找最大或最小的N个元素，`且同集合中元素的总数目相比，N很小`，那么`heapq.heapify(heap)`函数可以提供更好的性能。这些函数首先会在底层将数据转化成列表，且元素会以`堆的顺序排列`。

```py
>>> nums
[1, 8, 2, 23, 7, -4, 18, 23, 42, 37, 2]
>>> heap=list(nums)
>>> heapq.heapify(heap)
>>> heap
[-4, 2, 1, 23, 7, 2, 18, 23, 42, 37, 8]
>>> heapq.heappop(heap)
-4
>>> heapq.heappop(heap)
1
>>> heapq.heappop(heap)
2
>>>
```

`堆最重要的特性就是heap[0]总是最小那个的元素`。此外，接下来的元素可依次通过`heapq.heappop`方法轻松找到。该方法会将第一个元素(最小的)弹出，然后以第二小的元素取而代之(这个操作的复杂度是O(logN)，N代表堆的大小)

想找到`最小或最大的元素(N=1时)`，那么用min()和max)会更加快。

```py
>>> min(heap)
2
>>> max(heap)
42
>>> heap
[2, 7, 8, 23, 42, 37, 18, 23]
>>>
```

如果`N和集合本身的大小差不多大`，通常更快的方法是`先对集合排序，然后做切片操作`,使用`sorted(items)[：N]或者sorted(items)[-N:])`。

### 实现优先级队列

**我们想要实现一个队列，它能够以给定的优先级来对元素排序，且每次pop操作时都会返回优先级最高的那个元素。**

heapq模块提供了堆排序算法的实现,heapq有两种方式创建堆，

+ 一种是使用一个空列表，然后使用heapq.heappush()函数把值加入堆中
+ 一种就是使用heap.heapify(list)转换列表成为堆结构

```py
import heapq
class PriorityQueue:
    def __init__(self):
        self._queue = []
        self._index = 0


    def push(self, item, priority):
        # 把一个元组加入到队列里，元组包括优先级，索引,
        heapq.heappush(self._queue, (-priority, self._index, item))
        self._index += 1
    def pop(self):
        return heapq.heappop(self._queue)[-1]

class Item:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        #!r直接反应对象本体
        return 'Item({!r})'.format(self.name)


q = PriorityQueue()
q.push(Item('foo'),1)
q.push(Item('bar'),5)
q.push(Item('spam'),4)
q.push(Item('grok'),1)
print(q.pop())
print(q.pop())
print(q.pop())
```

`队列以元组(-priority，index，item)的形式组成`。把`priority取负值`是为了让`队列能够按元素的优先级从高到低的顺序排列`。一般情况下是最小堆。

变量`index`的作用是为了将具有`相同优先级`的元素以适当的顺序排列。通过维护一个`不断递增的索引`，元素将以它们入`队列时的顺序来排列`。没有哪两个元组会有相同的`index值`(一旦比较操作的结果可以确定，Python就不会再去比较剩下的元组元素了)

如果想将这个队列用于线程间通信，还需要增加适当的锁和信号机制

### 在字典中将键映射到多个值上

**我们想要一个能将键(key)映射到多个值的字典(即所谓的一键多值字典[multidict])**

字典是一种关联容器，每个键都映射到一个单独的值上。如果想让键映射到多个值，需要将这多个值保存到另一个容器如列表或集合中。

为了能方便地创建这样的字典，可以利用`collections模块`中的`defaultdict类`。`defaultdict`的一个特点就是它会`自动初始化第一个值`，这样`只需关注添加元素`即可。

```py
defaultdict(<class 'list'>, {'a': [1, 2], 'b': [4]})
>>> from  collections import defaultdict
>>> d = defaultdict(list)
>>> d['a'].append(1)
>>> d['a'].append(2)
>>> d['b'].append(4)
>>> d
defaultdict(<class 'list'>, {'a': [1, 2], 'b': [4]})
```

```py
>>> d= defaultdict(set)
>>> d['a'].add(1)
>>> d['a'].add(3)
>>> d['a'].add(3)
>>> d
defaultdict(<class 'set'>, {'a': {1, 3}})
>>>
```

用于分组，有一种java8里面Stream的味道

```py
d=defaultdict(list)
for key, value in pairs: 
  d[key]. append(value)
```

### 让字典保持有序

要控制字典中元素的顺序，可以使用`collections模块中的OrderedDict`类。当对字典做迭代时，它会严格按照元素初始添加的顺序进行。例如：

```py
>>> from collections import OrderedDict
>>> d = OrderedDict()
>>> d['1']=1
>>> d['2']=2
>>> d['3']=3
>>> d
OrderedDict([('1', 1), ('2', 2), ('3', 3)])
>>>
```

当想构建一个映射结构以便稍后对其做序列化或编码成另一种格式时，`OrderedDict`就显得特别有用。例如，如果想在进行JSON编码时精确控制各字段的顺序，那么只要首先在OrderedDict中构建数据就可以了。

```py
>>> import json
>>> json.dumps(d)
'{"1": 1, "2": 2, "3": 3}'
>>>
```

`OrderedDict`内部维护了一个`双向链表`，它会根据元素加入的顺序来排列键的位置。第一个`新加入的元素被放置在链表的末尾`。接下来对已存在的键做重新赋值`不会改变键的顺序`。

`OrderedDict的大小是普通字典的2倍多`，这是由于它`额外创建的链表所致`。因此，如果打算构建一个`涉及大量OrderedDict实例的数据结构`(例如从CSV文件中读取100000行内容到OrderedDict列表中)，那么需要认真对应用做需求分析，是否可以用内存换便利

### 与字典有关的计算问题

**我们想在字典上对数据执行各式各样的计算(比如求最小值、最大值、排序等)。**

通常会利用`zip()`将字典的`键和值反转`过来,zip() 函数用于将可迭代对象作为参数，将对象中对应的元素打包成一个个元组，然后返回由这些元组组成的对象。

```py
>>> prices={
... 'ACME':45.23,
... 'AAPL':612.78,
... 'IBM1':205.55,
... 'HPQ':37.20,
... 'FB':10.75
... }
>>>
>>> min(zip(prices.values(),prices.keys()))
(10.75, 'FB')
>>> max(zip(prices.values(),prices.keys()))
(612.78, 'AAPL')
>>>
```

同样，要对数据排序只要使用`zip()`再配合sorted()就可以了

```py
>>> sorted(zip(prices.values(),prices.keys()))
[(10.75, 'FB'), (37.2, 'HPQ'), (45.23, 'ACME'), (205.55, 'IBM1'), (612.78, 'AAPL')]
>>>
```

`zip()`创建了一个迭代器，它的内容只能被消费一次,尝试对字典的值做计算。可以利用字典的`values()方法`来解决这个问题：但是对于K的获取并不方便。

在计算`min()和max()`时，如果碰巧`value`的值相同，则将返回拥有`最小或最大key值`的那个条目。

### 在两个字典中寻找相同点(交集)

**有两个字典，我们想找出它们中间可能相同的地方(相同的键、相同的值等)。**

关于`字典的键`有一个很少有人知道的特性，那就是它们也`支持常见的集合操作`，比如求并集、交集和差集。

如果需要对字典的键做常见的集合操作，那么就能直接使用`keys-view`对象而不必先将它们转化为集合。获取到迭代器，可以直接对字典进行运算交集差集

```py
>>> a = {
... 'x':1,
... 'y':2,
... 'z':3
... }
>>> b = {
... 'w':10,
... 'x':11,
... 'y':2
... }
>>> a.keys() & b.keys()
{'y', 'x'}
>>> a.keys() - b.keys()
{'z'}
```

字典的`items()`方法返回由(key，value)对组成的`items-view`对象。这个对象支持类似的集合操作，可用来完成找出两个字典间有哪些键值对有相同之处的操作。

```py
>>> a.items() & b.items()
{('y', 2)}
>>>
```

也可以对字典进行过滤。

```py
>>> {key:a[key] for key in a.keys() - {'z','W'}}
{'y': 2, 'x': 1}
>>>
```

`字典的values()方法并不支持集合操作`。部分原因是因为在字典中键和值是不同的，从值的角度来看并不能保证所有的值都是唯一的。

### 从序列中移除重复项且保持元素间顺序不变

**我们想去除序列中出现的重复元素，但仍然保持剩下的元素顺序不变。**

如果序列中的值是`可哈希(hashable)`的，那么这个问题可以通过使用`集合`和`生成器`轻松解决。

```py
def dedupe(items):
    seen = set()
    for item in items:
        if item not in seen:
            yield item #生成有序列表
            seen.add(item)


a = [1, 5, 2, 1, 9, 1, 5, 10]
print(list(dedupe(a)))
==========
[1, 5, 2, 9, 10]
```

如果没办法生成哈希值，如果一个对象是可哈希的，那么在它的生存期内必须是不可变的，它需要有一个hash()方法。

整数、浮点数、字符串、元组都是不可变的。

```py

def dedupe(items, key=None):
    seen = set()
    for item in items:
        val = item if key is None  else  key(item)
        if val not in seen:
            yield item
            seen.add(val)



a=[{'x':1,'y':2},{'x':1,'y':3},{'x':1,'y':2},{'x':2,'y':4}]
#转化为元组
print(list(dedupe(a,key=lambda b:(b['x'],b['y']))))
```

这里参数key的作用是指定一个函数用来将序列中的元素转换为可哈希的类型，这么做的目的是为了检测重复项。即行为参数化，lambda表达式的使用。

### 对切片命名

**我们的代码已经变得无法阅读，到处都是硬编码的切片索引，我们想将它们清理干净**

即通过对切片变量的定义，把可变的部分抽离出来。一般来说，内置的`slice()函数`会创建一个切片对象，可以用在任何允许进行切片操作的地方。

```py
>>> recode='343534534532423435346547543534534534534564634534'
>>> int(recode[12:14]) * float(recode[10:13])
13608.0
>>> a = slice(12,14)
>>> b = slice(10,13)
>>> int(recode[a]) * float(recode[b])
13608.0
```

查看切片对象属性

```py
>>> a.start
12
>>> a.stop
14
>>> a.step
>>>
```

通过使用indices(size)方法将切片映射到特定大小的序列上。

```py
>>> s = "liruilong"
>>> a.indices(len(s))
(9, 9, 1)
>>>
```

### 找出序列中出现次数最多的元素

**怎样找出一个序列中出现次数最多的元素呢？**

`collections.Counter`类就是专门为这类问题而设计的，它甚至有一个有用的`most_common()`方法直接给了你答案。

出现频率最高的 3 个单词

```py
>>> words = [
... 'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
... 'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
... 'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
... 'my', 'eyes', "you're", 'under'
... ]
>>> from collections import Counter
>>> word_counts = Counter(words)
>>> word_counts.most_common(3)
[('eyes', 8), ('the', 5), ('look', 4)]
>>> word_counts.most_common(1)
[('eyes', 8)]
```

作为输入，`Counter`对象可以接受任意的`hashable`序列对象。在底层实现上，一个`Counter`对象就是一个字典，将元素映射到它出现的次数上

```py
>>> word_counts
Counter({'eyes': 8, 'the': 5, 'look': 4, 'into': 3, 'my': 3, 'around': 2, 'not': 1, "don't": 1, "you're": 1, 'under': 1})
>>>
```

可以整合其他的容器来统计数据,下面为在原有基础上整合一个列表

```py
>>> morewords = ['why','are','you','not','looking','in','my','eyes']
>>> for word in morewords:
...   word_counts[word] += 1
...
>>> word_counts
Counter({'eyes': 9, 'the': 5, 'look': 4, 'my': 4, 'into': 3, 'not': 2, 'around': 2, "don't": 1, "you're": 1, 'under': 1, 'why': 1, 'are': 1, 'you': 1, 'looking': 1, 'in': 1})
>>>
```

当然，也可以通过`update()`方法,我们可以看到，update方法并没有替换原来的value，而是进行了累加，和字典的update方法有区别

```py
>>> word_counts.update(morewords)
>>> word_counts
Counter({'eyes': 10, 'my': 5, 'the': 5, 'look': 4, 'into': 3, 'not': 3, 'around': 2, 'why': 2, 'are': 2, 'you': 2, 'looking': 2, 'in': 2, "don't": 1, "you're": 1, 'under': 1})
>>>
```

Counter生成的数据字典可以进行数据运算，只是针对相同Key的Value而言

```py
>>> morewords = ['why','are','you','not','looking','in','my','eyes']
>>> words = [
... 'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
... 'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around', 'the',
... 'eyes', "don't", 'look', 'around', 'the', 'eyes', 'look', 'into',
... 'my', 'eyes', "you're", 'under'
... ]
>>> from collections import Counter
>>> Counter(words) + Counter(morewords)
Counter({'eyes': 9, 'the': 5, 'look': 4, 'my': 4, 'into': 3, 'not': 2, 'around': 2, "don't": 1, "you're": 1, 'under': 1, 'why': 1, 'are': 1, 'you': 1, 'looking': 1, 'in': 1})
>>>
```

### 通过某个关键字排序一个字典列表

**你有一个字典列表，你想根据某个或某几个字典字段来排序这个列表。**

通过使用`operator 模块的 itemgetter 函数`，可以非常容易的排序这样的数据结构。感觉和`heapq`模块中的函数`nlargest()和nsmallest()`有些类似

```py
>>> rows = [
... {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003},
... {'fname': 'David', 'lname': 'Beazley', 'uid': 1002},
... {'fname': 'John', 'lname': 'Cleese', 'uid': 1001},
... {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}
... ]
>>> from operator import itemgetter
>>> sorted(rows, key=itemgetter('fname'))
[{'fname': 'Big', 'lname': 'Jones', 'uid': 1004}, {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003}, {'fname': 'David', 'lname': 'Beazley', 'uid': 1002}, {'fname': 'John', 'lname': 'Cleese', 'uid': 1001}]
```

`rows`被传递给接受一个关键字参数的`sorted()`内置函数,参数是 callable 类型，从`rows`中接受一个单一元素，然后返回被用来排序的值,itemgetter() 函数就是负责创建这个 callable 对象的。

```py
>>> sorted(rows, key=itemgetter('uid'))
[{'fname': 'John', 'lname': 'Cleese', 'uid': 1001}, {'fname': 'David', 'lname': 'Beazley', 'uid': 1002}, {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003}, {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}]
```

`itemgetter()`函数也支持多个 keys，如果你传入多个索引参数给 itemgetter() ，它生成的 callable 对象会返回一个包含所有元素值的元组，并且 sorted() 函数会根据这个元组中元素顺序去排序比如下面的代码

```py
>>> sorted(rows, key=itemgetter('lname','fname'))
[{'fname': 'David', 'lname': 'Beazley', 'uid': 1002}, {'fname': 'John', 'lname': 'Cleese', 'uid': 1001}, {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}, {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003}]
>>>
```

itemgetter() 有时候也可以用 lambda 表达式代替

```py
>>> sorted(rows, key=lambda r: r['fname'])
[{'fname': 'Big', 'lname': 'Jones', 'uid': 1004}, {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003}, {'fname': 'David', 'lname': 'Beazley', 'uid': 1002}, {'fname': 'John', 'lname': 'Cleese', 'uid': 1001}]
>>> sorted(rows, key=lambda r: (r['lname'],r['fname']))
[{'fname': 'David', 'lname': 'Beazley', 'uid': 1002}, {'fname': 'John', 'lname': 'Cleese', 'uid': 1001}, {'fname': 'Big', 'lname': 'Jones', 'uid': 1004}, {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003}]
>>>
```

使用 itemgetter() 方式会运行的稍微快点。同样适用于 min() 和 max() 等函数

```py
>>> import  heapq
>>> heapq.nsmallest(2,rows,key=itemgetter('fname'))
[{'fname': 'Big', 'lname': 'Jones', 'uid': 1004}, {'fname': 'Brian', 'lname': 'Jones', 'uid': 1003}]
>>> min(rows,key=itemgetter('fname'))
{'fname': 'Big', 'lname': 'Jones', 'uid': 1004}
>>>
```

### 排序不支持原生比较的对象

**你想排序类型相同的对象，但是他们不支持原生的比较操作**

内置的 sorted() 函数有一个关键字参数 key ，可以传入一个 callable 对象给它,这个 callable 对象对每个传入的对象返回一个值，这个值会被 sorted 用来排序

```py
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   user.py
@Time    :   2022/04/29 19:35:33
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib


class User:
    def __init__(self, user_id):
        self.user_id = user_id

    def __repr__(self):
        return 'User({})'.format(self.user_id)


def sort_notcompare():
    users = [User(23), User(3), User(99)]
    print(users)
    print(sorted(users, key=lambda u: u.user_id))

if __name__ == "__main__" : 
    sort_notcompare()
============
[User(23), User(3), User(99)]
[User(3), User(23), User(99)]    
```

另外一种方式是使用 operator.attrgetter() 来代替 lambda 函数：

```py
   from operator import attrgetter
   print(sorted(users, key=attrgetter('user_id')))
```

attrgetter() 函数通常会运行的快点，并且还能同时允许多个字段进行比较

```py
sorted(users, key=attrgetter('last_name', 'first_name'))
```

跟 operator.itemgetter() 函数作用于字典类型很类似 ,同样适用于像 min() 和 max() 之类

### 通过某个字段将记录分组

**你有一个字典或者实例的序列，然后你想根据某个特定的字段比如 date 来分组迭代访问。**

itertools.groupby() 函数对于这样的数据分组操作非常实用。为了演示，假设你已经有了下列的字典列表

```py
>>> rows = [
... {'address': '5412 N CLARK', 'date': '07/01/2012'},
... {'address': '5148 N CLARK', 'date': '07/04/2012'},
... {'address': '5800 E 58TH', 'date': '07/02/2012'},
... {'address': '2122 N CLARK', 'date': '07/03/2012'},
... {'address': '5645 N RAVENSWOOD', 'date': '07/02/2012'},
... {'address': '1060 W ADDISON', 'date': '07/02/2012'},
... {'address': '4801 N BROADWAY', 'date': '07/01/2012'},
... {'address': '1039 W GRANVILLE', 'date': '07/04/2012'},
... ]
>>> from operator import itemgetter
>>> from itertools import groupby
>>> rows.sort(key=itemgetter('date'))
>>> for date, items in groupby(rows, key=itemgetter('date')):
...     print(date)
...     for i in items:
...             print(' ', i)
...
07/01/2012
  {'address': '5412 N CLARK', 'date': '07/01/2012'}
  {'address': '4801 N BROADWAY', 'date': '07/01/2012'}
07/02/2012
  {'address': '5800 E 58TH', 'date': '07/02/2012'}
  {'address': '5645 N RAVENSWOOD', 'date': '07/02/2012'}
  {'address': '1060 W ADDISON', 'date': '07/02/2012'}
07/03/2012
  {'address': '2122 N CLARK', 'date': '07/03/2012'}
07/04/2012
  {'address': '5148 N CLARK', 'date': '07/04/2012'}
  {'address': '1039 W GRANVILLE', 'date': '07/04/2012'}
>>>
```

`groupby()`函数扫描整个序列并且查找连续相同值(或者根据指定key函数返回值相同)的元素序列。在每次选代的时候,它会返回`一个值和一个选代器对象`,这个`选代器对象`可以生成元素值全部等于上面那个值的组中所有对象。

**一个非常重要的准备步骤是要根据指定的字段将数据排序** 。因为`groupby()仅仅检查连续的元素`

如果你仅仅只是想根据date字段将数据分组到一个大的数据结构中去,并且允许随机访问,最好使用`defaultdict()`来构建一个多值字典

```py
>>> from collections import defaultdict
>>> rows_by_date = defaultdict(list)
>>> rows = [
N CLARK', 'date': '07/03/... {'address': '5412 N CLARK', 'date': '07/01/2012'},
... {'address': '5148 N CLARK', 'date': '07/04/2012'},
... {'address': '5800 E 58TH', 'date': '07/02/2012'},
... {'address': '2122 N CLARK', 'date': '07/03/2012'},
... {'address': '5645 N RAVENSWOOD', 'date': '07/02/2012'},
... {'address': '1060 W ADDISON', 'date': '07/02/2012'},
... {'address': '4801 N BROADWAY', 'date': '07/01/2012'},
... {'address': '1039 W GRANVILLE', 'date': '07/04/2012'},
... ]
>>> for row in rows:
...     rows_by_date[row['date']].append(row)
...
>>> rows_by_date
defaultdict(<class 'list'>, 
{'07/01/2012': [
        {'address': '5412 N CLARK', 'date': '07/01/2012'
        },
        {'address': '4801 N BROADWAY', 'date': '07/01/2012'
        }
    ], '07/04/2012': [
        {'address': '5148 N CLARK', 'date': '07/04/2012'
        },
        {'address': '1039 W GRANVILLE', 'date': '07/04/2012'
        }
    ], '07/02/2012': [
        {'address': '5800 E 58TH', 'date': '07/02/2012'
        },
        {'address': '5645 N RAVENSWOOD', 'date': '07/02/2012'
        },
        {'address': '1060 W ADDISON', 'date': '07/02/2012'
        }
    ], '07/03/2012': [
        {'address': '2122 N CLARK', 'date': '07/03/2012'
        }
    ]
})
>>>
```

### 过滤序列元素

**你有一个数据序列，想利用一些规则从中提取出需要的值或者是缩短序列**

最简单的过滤序列元素的方法就是使用列表推导

```py
>>> mylist = [1, 4, -5, 10, -7, 2, 3, -1]
>>> [n for n in mylist if n > 0]
[1, 4, 10, 2, 3]
>>> [n for n in mylist if n < 0]
[-5, -7, -1]
>>>
```

潜在缺陷就是如果输入非常大的时候会产生一个非常大的结果集，占用大量内存,可以用生成器表达式迭代产生过滤的元素。

```py
>>> pos = (n for n in mylist if n > 0)
>>> pos
<generator object <genexpr> at 0x7f3a14e538e0>
>>> for i in pos:
...     print(i)
...
1
4
10
2
3
>>>
```

过滤规则比较复杂,将过滤代码放到一个函数中，然后使用内建的` filter() `函数

```py
values = ['1', '2', '-3', '-', '4', 'N/A', '5']
def is_int(val):
    try:
        x = int(val)
        return True
    except ValueError:
        return False
# filter() 函数创建了一个迭代器
ivals = list(filter(is_int, values))
print(ivals)
```

在过滤的时候转换数据

```py
>>> mylist = [1, 4, -5, 10, -7, 2, 3, -1]
>>> import math
>>> [math.sqrt(n) for n in mylist if n > 0]
[1.0, 2.0, 3.1622776601683795, 1.4142135623730951, 1.7320508075688772]
>>>
```

过滤操作的一个变种就是将不符合条件的值用新的值代替，

```py
>>> [n if n > 0 else 0 for n in mylist]
[1, 4, 0, 10, 0, 2, 3, 0]
>>> [n if n < 0 else 0 for n in mylist]
[0, 0, -5, 0, -7, 0, 0, -1]
>>>
```

`itertools.compress()`,它以一个`iterable 对象`和一个相对应的`Boolean 选择器`序列作为输入参数.然后输出 iterable 对象中对应选择器为 True 的元素当你需要用另外一个相关联的序列来过滤某个序列的时候，这个函数是非常有用的

```py
>>> addresses = [
... '5412 N CLARK',
... '5148 N CLARK',
... '5800 E 58TH',
... '2122 N CLARK'
... '5645 N RAVENSWOOD',
... '1060 W ADDISON',
... '4801 N BROADWAY',
... '1039 W GRANVILLE',
... ]
>>> counts = [ 0, 3, 10, 4, 1, 7, 6, 1]
>>> from itertools import compress
>>> more5 = [n > 5 for n in counts]
>>> more5
[False, False, True, False, False, True, True, False]
>>> list(compress(addresses, more5))
['5800 E 58TH', '4801 N BROADWAY', '1039 W GRANVILLE']
>>>
```

`compress()`也是返回的一个迭代器

### 从字典中提取子集

**你想构造一个字典，它是另外一个字典的子集**

是使用字典推导

```py
>>> prices = {
... 'ACME': 45.23,
... 'AAPL': 612.78,
... 'IBM': 205.55,
... 'HPQ': 37.20,
... 'FB': 10.75
... }
>>> {key: value for key, value in prices.items() if value > 200}
{'AAPL': 612.78, 'IBM': 205.55}
>>> tech_names = {'AAPL', 'IBM', 'HPQ', 'MSFT'}
>>> {key: value for key, value in prices.items() if key in tech_names}
{'AAPL': 612.78, 'IBM': 205.55, 'HPQ': 37.2}
>>>
```

字典推导能做到的，通过创建一个元组序列然后把它传给`dict()`也能实现,字典推导方式表意更清晰，并且实际上也会运行的更快些

```py
dict((key, value) for key, value in prices.items() if value > 200)
```

### 将名称映射到序列的元素中

**你有一段通过下标访问列表或者元组中元素的代码，但是这样有时候会使得你的代码难以阅读，于是你想通过名称来访问元素。**

collections.namedtuple() 函数通过使用一个普通的元组对象来帮你解决这个问题。

```py
>>> from collections import namedtuple
>>> Subscriber = namedtuple('Subscriber', ['addr', 'joined'])
>>> sub = Subscriber('jonesy@example.com', '2012-10-19')
>>> sub
Subscriber(addr='jonesy@example.com', joined='2012-10-19')
```

`namedtuple()`返回 Python 中标准元组类型子类的一个工厂方法,传递一个类型名和你需要的字段给它，然后它就会返回一个类,虽然看起来像一个类实例，但是是可交换的。

```py
>>> sub.addr
'jonesy@example.com'
>>> sub[1]
'2012-10-19'
>>> addr, joined = sub
>>> addr
'jonesy@example.com'
>>>
```

命名元组的一个主要用途是将你的代码从下标操作中解脱出来,数据查询中返回很大一个元组，通过下标去操作其中的元素有很多可变性，如果使用元组命名则不用考虑

```py
def compute_cost(records):
    total = 0.0
    for rec in records:
        total += rec[1] * rec[2]
    return total

from collections import namedtuple
Stock = namedtuple('Stock', ['name', 'shares', 'price'])
def compute_cost(records):
    total = 0.0
    for rec in records:
        s = Stock(*rec)
        total += s.shares * s.price
    return total

```

命名元组另一个用途就是作为字典的替代，字典存储需要更多的内存空间

需要构建一个非常大的包含字典的数据结构，那么使用命名元组会更加高效

是需要注意一个命名元组是不可更改的.如果你真的需要改变后的属性，那么可以使用命名元组实例的` replace() `方法

```py
>>> sub
Subscriber(addr='jonesy@example.com', joined='2012-10-19')
>>> sub.addr
'jonesy@example.com'
>>> sub.addr = '1@qq.com'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: can't set attribute
>>> sub = sub._replace(addr='1@qq.com')
>>> sub
Subscriber(addr='1@qq.com', joined='2012-10-19')
>>>
```

它会创建一个全新的命名元组并将对应的字段用新的值取代

`_replace()`方法还有一个很有用的特性就是当你的命名元组拥有可选或者缺失字段时候，它是一个非常方便的填充数据的方法。你可以先创建一个包含缺省值的原型元组，然后使用 `_replace()` 方法创建新的值被更新过的实例,类似于类实例的初始化调用构造函数

```py
>>> from collections import namedtuple
>>> Stock = namedtuple('Stock', ['name', 'shares', 'price', 'date', 'time'])
>>> stock_prototype = Stock('', 0, 0.0, None, None)
>>> def dict_to_stock(s):
...     return stock_prototype._replace(**s)
...
>>> a = {'name': 'ACME', 'shares': 100, 'price': 123.45}
>>> dict_to_stock(a)
Stock(name='ACME', shares=100, price=123.45, date=None, time=None)
>>> b = {'name': 'ACME', 'shares': 100, 'price': 123.45, 'date': '12/17/2012'}
>>> dict_to_stock(b)
Stock(name='ACME', shares=100, price=123.45, date='12/17/2012', time=None)
>>>
```

如果你的目标是定义一个需要更新很多实例属性的高效数据结构，那么命名元组并不是你的最佳选择。这时候你应该考虑定义一个包含 slots 方法的类

### 同时对数据做转换和换算

**我们需要调用一个换算(reduction)函数(例如sumO、min)、max))，但首先得对数据做转换或筛选。**

```py
>>> num = [1,2,3,4,5]
>>> sum(x * x for x in num)
55
>>>
```

### 将多个映射合并为单个映射

**我们有多个字典或映射，想在逻辑上将它们合并为一个单独的映射结构，以此执行某些特定的操作，比如查找值或检查键是否存在。**

一种简单的万法是利用`collections模块中的ChainMap类`来解决

```py
>>> a={'x':1,'z':3}
>>> b={'y':2,'z':4}
>>> from collections import ChainMap
>>> ChainMap(a,b)
ChainMap({'x': 1, 'z': 3}, {'y': 2, 'z': 4})
>>>

```

ChainMap可接受多个映射然后在逻辑上使它们表现为一个单独的映射结构。但是，这些`映射在字面上并不会合并在一起。`相反，ChainMap只是简单地维护一个记录底层映射关系的列表，然后重定义常见的字典操作来扫描这个列表。大部分的操作都能正常工作。

```py
>>> len(ChainMap(a,b))
3
>>> list(ChainMap(a,b).keys())
['y', 'z', 'x']
>>> list(ChainMap(a,b).values())
[2, 3, 1]
>>>
```

如果有重复的键，那么这里会采用第一个映射中所对应的值。修改映射的操作总是会作用在列出的第一个映射结构上。

`ChainMap`与带有`作用域`的值，比如编程语言中的变量(即全局变量、局部变量等)一起工作时特别有用。

```py
>>> v = ChainMap()
>>> v['x'] = 1
>>> v = v.new_child()
>>> v['x'] = 2
>>> v = v.new_child()
>>> v['x'] = 3
>>> v
ChainMap({'x': 3}, {'x': 2}, {'x': 1})
>>> v['x']
3
>>> v = v.parents
>>> v['x']...............
2
>>> v = v.parents
>>> v['x']
1
>>> v
ChainMap({'x': 1})
>>>

```

作为`ChainMap`的替代方案，我们可能会考虑利用字典的update()方法将多个字典合并在一起。但是破坏了原始的数据结构，而ChainMap使用的就是原始的字典，因此它不会产生这种令人不悦的行为。

```py
>>> a
{'x': 1, 'z': 3}
>>> b
{'y': 2, 'z': 4}
>>>
>>> c= dict(b)
>>> c.update(a)
>>> c
{'y': 2, 'z': 3, 'x': 1}
```

## 第二章 字符串和文本

本章重点关注`文本的操作处`理，比如`提取字符串，搜索，替换以及解析等`。大部分的问题都能简单的调用字符串的内建方法完成。但是，一些更为复杂的操作可能需要正则表达式或者强大的解析器，所有这些主题我们都会详细讲解。并且在操作 Unicode时候碰到的一些棘手的问题在这里也会被提及到。

### 针对任意多的分隔符拆分字符串

**你需要将一个字符串分割为多个字段，但是分隔符 (还有周围的空格) 并不是固定的**

`string 对象的 split() 方法`只适应于非常简单的字符串分割情形，它并不允许有多个分隔符或者是分隔符周围不确定的空格。当你需要更加灵活的切割字符串的时候，最好使用` re.split() `方法：

```py
>>> line = 'asdf fjdk; afed, fjek,asdf, foo'
>>> import re
>>> re.split(r'[;,\s]\s*', line)
['asdf', 'fjdk', 'afed', 'fjek', 'asdf', 'foo']
>>>
```

函数 re.split()允许你为分隔符指定多个正则模式。  分隔符可以是`逗号，分号或者是空格，并且后面紧跟着任意个的空格`。只要这个模式被找到，那么匹配的分隔符两边的实体都会被当成是结果中的元素返回。返回结果为一个字段列表

```py
>>> re.split(r'(;|,|\s)\s*', line)
['asdf', ' ', 'fjdk', ';', 'afed', ',', 'fjek', ',', 'asdf', ',', 'foo']
>>>
```

使用` re.split() `函数时候，需要特别注意的是正则表达式中是否包含一个括号`捕获分组`。如果使用了捕获分组，那么`被匹配的文本也将出现在结果列表中`。

```py
>>> fields = re.split(r'(;|,|\s)\s*', line)
>>> values = fields[::2]
>>> delimiters = fields[1::2] + ['']
>>> values
['asdf', 'fjdk', 'afed', 'fjek', 'asdf', 'foo']
>>> delimiters
[' ', ';', ',', ',', ',', '']
>>> ''.join(v+d for v,d in zip(values, delimiters))
'asdf fjdk;afed,fjek,asdf,foo'
>>>
```

获取分割字符在某些情况下也是有用的,可能想保留分割字符串，用来在后面重新构造一个新的输出字符串：

如果你不想保留分割字符串到结果列表中去，但仍然需要使用到括号来分组正则表达式的话，确保你的分组是非捕获分组，形如 (?:...) 。

```py
>>> re.split(r'(?:,|;|\s)\s*', line)
['asdf', 'fjdk', 'afed', 'fjek', 'asdf', 'foo']
>>>
```

### 在字符串的开头或结尾处做文本匹配

**你需要通过指定的文本模式去检查字符串的开头或者结尾，比如文件名后缀，URLScheme 等等。**

检查字符串开头或结尾的一个简单方法是使用str.startswith()或者是str.endswith()方法。比如:

```py
>>> filename = 'spam.txt'
>>> filename.endswith('.txt')
True
>>> filename.startswith('file:')
False
>>> url = 'http://www.python.org'
>>> url.startswith('http:')
True
>>>
```

如果你想检查`多种匹配可能`，只需要将所有的匹配项放入到一个`元组`中去，然后`传给 startswith() 或者 endswith() 方法`：

```py
>>> import os
>>> filenames = os.listdir('.')
>>> filenames
['.bash_logout', '.bash_profile', '.cshrc', '.tcshrc', 'anaconda-ks.cfg', 'scp_script.py', 'uagtodata', '.bash_history', 'one-client-install.sh', 'calico.yaml', 'docker', '.mysql_history', 'UagAAA', 'Uag.tar', 'liruilong.snap1', '.python_history', '.cache', 'translateDemo', 'soft', 'jenkins.docker.sh', 'o3J6.txt', 'bak_shell', 'liruilong', 'index.html', 'load_balancing', 'redis-2.10.3.tar.gz', 'redis-2.10.3', '.kube', 'kc1', 'pod-demo.yaml', 'web-liruilong.yaml', 'shell.sh', '.config', 'nohup.out', '.viminfo', '.pki', 'kubectl.1', 'temp', 'go', '.vim', '111.txt', 'uagtodata.tar', 'set.sh', '.Xauthority', 'calico_3_14.tar', '.ssh', '.bashrc', 'db', '.docker', 'UagAAA.tar', 'Uag.war', 'Uag', 'txt.sh', '.lesshst', 'gitlab.docker.sh', 'kubectl', 'rsync', 'percona-toolkit-3.0.13-1.el7.x86_64.rpm', 'redisclear.py', 'Fetch']
>>> [name for name in filenames if name.endswith(('.yaml', '.sh')) ]
['one-client-install.sh', 'calico.yaml', 'jenkins.docker.sh', 'pod-demo.yaml', 'web-liruilong.yaml', 'shell.sh', 'set.sh', 'txt.sh', 'gitlab.docker.sh']
>>> any(name.endswith('.py') for name in filenames)
True
>>>
```

必须要输入一个元组作为参数。如果你恰巧有一个 list 或者 set 类型的选择项，要确保传递参数前先调用 tuple() 将其转换为元组类型

类似的操作也可以使用切片来实现，但是代码看起来没有那么优雅

```py
>>> filename = 'spam.txt'
>>> filename[-4:] == '.txt'
True
>>> url = 'http://www.python.org'
>>> url[:5] == 'http:' or url[:6] == 'https:' or url[:4] == 'ftp:'
True
>>>
```

还可以使用正则表达式去实现

```py
>>> import re
>>> url = 'http://www.python.org'
>>> re.match('http:|https:|ftp:', url)
<_sre.SRE_Match object; span=(0, 5), match='http:'>
>>>
```

### 利用Shell通配符做字符串匹配

**你想使用` Unix Shell `中常用的通配符 (比如 `*.py , Dat[0-9]*.csv` 等) 去匹配文本字符串**

```py
>>> from fnmatch import fnmatch, fnmatchcase
>>> fnmatch('foo.txt', '*.txt')
True
>>> fnmatch('foo.txt', '?oo.txt')
True
>>> fnmatch('Dat45.csv', 'Dat[0-9]*')
True
>>> names = ['Dat1.csv', 'Dat2.csv', 'config.ini', 'foo.py']
>>> [name for name in names if fnmatch(name, 'Dat*.csv')]
['Dat1.csv', 'Dat2.csv']
>>>
```

`fnmatch()`函数使用底层操作系统的大小写敏感规则 (不同的系统是不一样的) 来匹配模式

```py
#winsows10
>>> from fnmatch import fnmatch, fnmatchcase
>>> fnmatch('foo.txt', '*.TXT') 
True
>>>
# Linux
>>> fnmatch('foo.txt', '*.TXT')
False
>>>
```

如果你对这个区别很在意，可以使用 fnmatchcase() 来代替。它完全使用你的模式大小写匹配。

```py
>>> from fnmatch import fnmatch, fnmatchcase
>>> fnmatch('foo.txt', '*.TXT')
True
>>> fnmatchcase('foo.txt', '*.TXT')
False
>>>
```

`fnmatch()` 函数匹配能力介于简单的字符串方法和强大的正则表达式之间.在处理非文件名的字符串时也可以使用

```py
>>> from fnmatch import fnmatchcase
>>> addresses = [
... '5412 N CLARK ST',
... '1060 W ADDISON ST',
... '1039 W GRANVILLE AVE',
... '2122 N CLARK ST',
... '4802 N BROADWAY',
... ]
>>> [addr for addr in addresses if fnmatchcase(addr, '* ST')]
['5412 N CLARK ST', '1060 W ADDISON ST', '2122 N CLARK ST']
>>> [addr for addr in addresses if fnmatchcase(addr, '54[0-9][0-9] *CLARK*')]
['5412 N CLARK ST']
>>>
```

### 文本模式的匹配和查找

**你想匹配或者搜索特定模式的文本**

如果你想匹配的是字面字符串，那么你通常只需要调用基本字符串方法就行，比如`str.find() , str.endswith() , str.startswith()`或者类似的方法：

```py
>>> text = 'yeah, but no, but yeah, but no, but yeah'
>>> text.startswith('yeah')
True
>>> text.endswith('no')
False
>>> text.find('no')
10
>>>
```

对于复杂的匹配需要使用正则表达式和 re 模块

```py
>>> text1 = '11/27/2012'
>>> import re
>>> if re.match(r'\d+/\d+/\d+', text1):
...     print('yes')
... else:
...     print('no')
...
yes
```

如果你想使用同一个模式去做多次匹配，你应该先将模式字符串预编译为模式对象。

```py
>>> datepat = re.compile(r'\d+/\d+/\d+')
>>> if datepat.match(text1):
...     print('yes')
... else:
...     print('no')
...
yes
>>>
```

`match()`总是从字符串开始去匹配，如果你想查找字符串任意部分的模式出现位置，使用`findall()`方法去代替

```py
>>> text = 'Today is 11/27/2012. PyCon starts 3/13/2013.'
>>> datepat.findall(text)
['11/27/2012', '3/13/2013']
>>>
```

在定义正则式的时候，通常会利用括号去捕获分组分别将每个组的内容提取出来

```py
>>> datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
>>> m = datepat.match('11/27/2012')
>>> m
<_sre.SRE_Match object; span=(0, 10), match='11/27/2012'>
>>> m.group(0)
'11/27/2012'
>>> m.group(1)
'11'
>>> m.group(3)
'2012'
>>> m.groups()
('11', '27', '2012')
```

`findall()`方法会搜索文本并以列表形式返回所有的匹配,想以迭代方式返回匹配，可以使用`finditer()`方法来代替

```py
>>> [m.groups()  for m in datepat.finditer(text)]
[('11', '27', '2012'), ('3', '13', '2013')]
>>>
```

### 查找和替换文本

**你想在字符串中搜索和匹配指定的文本模式**

对于简单的字面模式，直接使用` str.repalce() `方法即可

```py
>>> 'yeah, but no, but yeah, but no, but yeah'.replace('yeah', 'yep')
'yep, but no, but yep, but no, but yep'
>>>
```

复杂的模式，请使用 re 模块中的` sub() `函数。`sub() 函数`中的第一个参数是`被匹配的模式`，第二个参数是`替换模式`。`反斜杠数字比如 \3 指向前面模式的捕获组号。`

```py
>>> import re
>>> re.sub(r'(\d+)/(\d+)/(\d+)', r'\3-\1-\2','Today is 11/27/2012. PyCon starts 3/13/2013.')
'Today is 2012-11-27. PyCon starts 2013-3-13.'
>>>
```

如果你打算用相同的模式做多次替换，考虑先编译它来提升性能

```py
>>> import re
>>> datepat = re.compile(r'(\d+)/(\d+)/(\d+)')
>>> datepat.sub(r'\3-\1-\2', text)
'Today is 2012-11-27. PyCon starts 2013-3-13.'
>>>
```

对于更加复杂的替换，可以传递一个替换回调函数来代替

```py
>>> from calendar import month_abbr
>>> def change_date(m):
...     mon_name = month_abbr[int(m.group(1))]
...     return '{} {} {}'.format(m.group(2), mon_name, m.group(3))
...
>>> datepat.sub(change_date, text)
'Today is 27 Nov 2012. PyCon starts 13 Mar 2013.'
>>>
```

想知道有多少替换发生了，可以使用`re.subn()`

```py
>>> newtext, n = datepat.subn(r'\3-\1-\2', text)
>>> newtext
'Today is 2012-11-27. PyCon starts 2013-3-13.'
>>> n
2
>>>
```

### 字符串忽略大小写的搜索替换

**你需要以忽略大小写的方式搜索与替换文本字符串**

类似 Lixnu 中grep的 -i 参数，`python`中需要在使用`re 模块`的时候给这些操作提供`re.IGNORECASE 标志参数`。

```py
>>> text = 'UPPER PYTHON, lower python, Mixed Python'
>>> re.findall('python', text, flags=re.IGNORECASE)
['PYTHON', 'python', 'Python']
```

也可以用于替换

```py
>>> re.sub('python', 'snake', text, flags=re.IGNORECASE)
'UPPER snake, lower snake, Mixed snake'
>>>
```

替换字符串并不会自动跟被匹配字符串的大小写保持一致。为了修复这个，你可能需要一个辅助函数

```py
def matchcase(word):
    def replace(m):
        text = m.group()
        if text.isupper():
            return word.upper()
        elif text.islower():
            return word.lower()
        elif text[0].isupper():
            return word.capitalize()
        else:
            return word
    return replace

re.sub('python', matchcase('snake'), text, flags=re.IGNORECASE)   
========
'UPPER SNAKE, lower snake, Mixed Snake'
```

### 定义实现最短匹配的正则表达式

**用正则表达式匹配某个文本模式，但是它找到的是模式的最长可能匹配。而你想修改它变成查找最短的可能匹配。**

在需要匹配一对分隔符之间的文本的时候,模式 `r'\"(.*)\"'`的意图是匹配被双引号包含的文本

```py
>>> str_pat = re.compile(r'\"(.*)\"')
>>> text1 = 'Computer says "no."'
>>> str_pat.findall(text1)
['no.']
>>> text2 = 'Computer says "no." Phone says "yes."'
>>> str_pat.findall(text2)
['no." Phone says "yes.']
>>> str_pat = re.compile(r'\"(.*?)\"')
>>> str_pat.findall(text2)
['no.', 'yes.']
>>>
```

正则表达式中 `* 操作符是贪婪的`，因此匹配操作会查找最长的可能匹配,可以在模式中的`* 操作符后面加上? 修饰符`,使得匹配变成非贪婪模式

`点 (.)`匹配除了换行外的任何字符,如果你将`点 (.)`号放在开始与结束符 (比如引号) 之间的时候，那么匹配操作会查找符合模式的`最长可能匹配`,在 `* 或者 +`这样的操作符后面添加一个`?`可以强制`匹配算
法`改成寻找`最短的可能匹配`。

### 编写多行模式的正则表达式

**使用正则表达式去匹配一大块的文本，而你需要跨越多行去匹配。**

很典型的出现在当你用点 `(.)`去匹配任意字符的时候，忘记了点`(.)`不能匹配换行符的事实,匹配 C 语言分割的注释：

```py
>>> comment = re.compile(r'/\*(.*?)\*/')
>>> text1 = '/* this is a comment */'
>>> text2 = '''/* this is a
... multiline comment */
... '''
>>> comment.findall(text1)
[' this is a comment ']
>>> comment.findall(text2)
[]
>>>
```

可以修改模式字符串，增加对换行的支持

```py
>>> comment = re.compile(r'/\*((?:.|\n)*?)\*/')
>>> comment.findall(text2)
[' this is a\nmultiline comment ']
>>>
```

在这个模式中，`(?:.|\n)` 指定了一个`非捕获组` (也就是它定义了一个仅仅用来做匹配，而不能通过单独捕获或者编号的组)。

`re.compile() 函数`接受一个标志参数叫 `re.DOTALL`，在这里非常有用。它可以让`正则表达式中的点 (.) 匹配包括换行符在内的任意字符`

```py
>>> comment = re.compile(r'/\*(.*?)\*/', re.DOTALL)
>>> comment.findall(text2)
[' this is a\nmultiline comment ']
>>>
```

### 将Unicode文本统一表示为规范形式

**你正在处理 Unicode 字符串，需要确保所有字符串在底层有相同的表示。**

嗯，这块先记录下，感觉有些鸡肋....

在 Unicode 中，某些字符能够用多个合法的编码表示

```py
>>> s1 = 'Spicy Jalape\u00f1o'
>>> s2 = 'Spicy Jalapen\u0303o'
>>> s1
'Spicy Jalapeño'
>>> s2
'Spicy Jalapeño'
>>> s1 == s2
False
>>> len(s1)
14
>>> len(s2)
15
>>>
```

在需要比较字符串的程序中使用字符的多种表示会产生问题。为了修正这个问题，你可以使用 unicodedata 模块先将文本标准化：

```py
>>> import unicodedata
>>> t1 = unicodedata.normalize('NFC', s1)
>>> t2 = unicodedata.normalize('NFC', s2)
>>> t1 == t2
True
>>> print(ascii(t1))
'Spicy Jalape\xf1o'
>>> t3 = unicodedata.normalize('NFD', s1)
>>> t4 = unicodedata.normalize('NFD', s2)
>>> t3 == t4
True
>>> print(ascii(t3))
'Spicy Jalapen\u0303o'
>>>
```

`normalize()` 第一个参数指定`字符串标准化`的方式。`NFC`表示字符应该是整体组成 (比如可能的话就使用单一编码)，而 `NFD`表示字符应该分解为多个组合字符表示。

Python 同样支持扩展的标准化形式`NFKC 和 NFKD`，它们在处理某些字符的时候增加了额外的兼容特性

```py
>>> s = '\ufb01' # A single character
>>> s
' fi'
>>> unicodedata.normalize('NFD', s)
' fi'
# Notice how the combined letters are broken apart here
>>> unicodedata.normalize('NFKD', s)
'fi'
>>> unicodedata.normalize('NFKC', s)
'fi'
>>>
```

### 用正则表达式处理Unicode字符

**你正在使用正则表达式处理文本，但是关注的是 Unicode 字符处理**。

默认情况下 `re 模块`已经对一些`Unicode 字符`类有了基本的支持。比如，`\\d`已经匹配任意的`unicode`数字字符了

```py
>>> import re
>>> num = re.compile('\d+')
>>> # ASCII digits
>>> num.match('123')
<_sre.SRE_Match object at 0x1007d9ed0>
>>> # Arabic digits
>>> num.match('\u0661\u0662\u0663')
<_sre.SRE_Match object at 0x101234030>
>>>
```

嗯，这个不太懂，先记录下

### 从字符串中去掉不需要的字符

**想去掉文本字符串开头，结尾或者中间不想要的字符，比如空白。**

`strip()`方法能用于删除开始或结尾的字符。 `lstrip() 和 rstrip()`分别从左和从右执行删除操作。

```py
>>> s = ' hello world \n'
>>> s.strip()
'hello world'
>>> s.lstrip()
'hello world \n'
>>> s.rstrip()
' hello world'
>>>
```

默认情况下，这些方法会去除空白字符，但是你也可以指定其他字符

```py
>>> t = '-----hello====='
>>> t.lstrip('-')
'hello====='
>>> t.strip('-=')
'hello'

```

想处理中间的空格，使用` replace() `方法或者是用正则表达式替换

```py
>>> s.replace(' ', '')
'helloworld'
>>> import re
re.sub('\s+', ' ', s)
'hello world'
>>>
```

将字符串 strip 操作和其他迭代操作相结合，利用生成器表达式

```py
with open(filename) as f:
    lines = (line.strip() for line in f)
    for line in lines:
        print(line)
```

### 文本过滤和清理

**一些无聊的幼稚黑客在你的网站页面表单中输入文本”pýtĥöñ”，然后你想将这些字符清理掉。**

文本清理问题会涉及到包括文本解析与数据处理等一系列问题。

```py
>>> s = 'pýtĥöñ\fis\tawesome\r\n'
>>> s
'pýtĥöñ\x0cis\tawesome\r\n'
>>> remap = {
... ord('\t') : ' ',
... ord('\f') : ' ',
... ord('\r') : None # Deleted
... }
>>> s.translate(remap)
'pýtĥöñ is awesome\n'
>>>
```

正如你看的那样，空白字符 \t 和 \f 已经被重新映射到一个空格。回车字符 r 直接被删除。

```py
>>> import unicodedata
>>> import sys
>>> cmb_chrs = dict.fromkeys(c for c in range(sys.maxunicode)
... if unicodedata.combining(chr(c)))
...
>>> b = unicodedata.normalize('NFD', a)
>>> b
'pýtĥöñ is awesome\n'
>>> b.translate(cmb_chrs)
'python is awesome\n'
>>>
```

通过使用`dict.fromkeys()`方法构造一个字典，每个`Unicode 和音符作为键`，对于的值全部为 None

然后使用`unicodedata.normalize()`将原始输入标准化为分解形式字符。然后再调用`translate 函数删除`所有重音符。同样的技术也可以被用来删除其他类型的字符
(比如控制字符等)。

另一种清理文本的技术涉及到 I/O 解码与编码函数。这里的思路是先对文本做一些初步的清理，然后再结合 `encode()`或者`decode()`操作来清除或修改它

```py
>>> a
'pýtĥöñ is awesome\n'
>>> b = unicodedata.normalize('NFD', a)
>>> b.encode('ascii', 'ignore').decode('ascii')
'python is awesome\n'
>>>

```

### 对齐文本字符串

**通过某种对齐方式来格式化字符串**

对于基本的字符串对齐操作，可以使用字符串的`ljust() , rjust() 和 center()方法`。

```py
>>> text = 'Hello World'
>>> text.ljust(20)
'Hello World         '
>>> text.rjust(20)
'         Hello World'
>>> text.center(20)
'    Hello World     '
>>>
```

所有这些方法都能接受一个可选的填充字符。

```py
>>> text = 'Hello World'
>>> text.rjust(20,'=')
'=========Hello World'
>>> num='5'
>>> num.rjust(8,'0')
'00000005'
>>>
```

rjust这类型的方法只针对对字符串，对于int类型不支持

```py
>>> num.rjust(8,'0')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'int' object has no attribute 'rjust'
```

函数` format() `同样可以用来很容易的对齐字符串。你要做的就是使用` <,> `或者`^字符`后面紧跟一个指定的宽度，

```py
>>> num=5
>>> format(num, '>20')
'                   5'
>>> format(num, '0>20')
'00000000000000000005'
>>>
>>> format(num, '0<20')
'50000000000000000000'
>>> format(num, '0^20')
'00000000050000000000'
>>>
```

当格式化多个值的时候，这些格式代码也可以被用在` format() `方法中。

```py
>>> '{:>10s} {:>10s}'.format('Hello', 'World')
'     Hello      World'
>>>
```

`format()`函数的一个好处是它不仅适用于字符串。它可以用来格式化任何值

```py
>>> x = 1.2345
>>> format(x, '>10')
' 1.2345'
>>> format(x, '^10.2f')
' 1.23 '
>>>
>>> '{:>10s} {:>10s}'.format('Hello', 'World')
'     Hello      World'
```

### 字符串连接及合并

**将几个小的字符串合并为一个大的字符串**

如果你想要合并的字符串是在`一个序列或者 iterable` 中，那么最快的方式就是使用` join() `方法

```py
>>> parts = ['Is', 'Chicago', 'Not', 'Chicago?']
>>> ' '.join(parts)
'Is Chicago Not Chicago?'
>>> ','.join(parts)
'Is,Chicago,Not,Chicago?'
>>>
>>> ''.join(parts)
'IsChicagoNotChicago?'
>>>
```

```py
>>> b = 'Not Chicago?'
>>> a + ' ' + b
'Is Chicago Not Chicago?'
>>>
```

如果你想在`源码`中将`两个字面字符串`合并起来，你只需要简单的将它们放到一起，不需要用加号 (+)。

```py
>>> a = 'li' 'rui' 'long'
>>> a
'liruilong'
>>>
```

嗯，字符串变量是不行的，有些天真了哈....,只适用于字面量

```py
>>> a = 'li' 'rui' 'long'
>>> a
'liruilong'
>>> a a
  File "<stdin>", line 1
    a a
      ^
SyntaxError: invalid syntax
>>> a = a a
  File "<stdin>", line 1
    a = a a
          ^
SyntaxError: invalid syntax
>>>
```

**<font color=brown>使用加号 (+) 操作符去连接大量的字符串的时候是非常低效率的，因为加号连接会引起内存复制以及垃圾回收操作</font>**

永远都不应像下面这样写字符串连接代码

```py
s = ''
for p in parts:
    s += p
```

这种写法会比使用`join() 方法`运行的要慢一些，因为每一次执行`+= 操作`的时候会创建一个`新的字符串对象`。你最好是先收集所有的`字符串片段`然后再将它们连接起来。可以利用生成器表达式

```py
>>> data = ['ACME', 50, 91.1]
>>> ','.join(str(d) for  d in data)
'ACME,50,91.1'
>>>
```

同样还得注意不必要的字符串连接操作。

```py
print(a + ':' + b + ':' + c) # Ugly
print(':'.join([a, b, c])) # Still ugly
print(a, b, c, sep=':') # Better
```

当混合使用 I/O 操作和字符串连接操作的时候，有时候需要仔细研究你的程序

```py
# Version 1 (string concatenation)
f.write(chunk1 + chunk2)
# Version 2 (separate I/O operations)
f.write(chunk1)
f.write(chunk2)
```

如果两个字符串很小，那么第一个版本性能会更好些，因为 I/O 系统调用天生就慢。另外一方面，如果两个字符串很大，那么第二个版本可能会更加高效，因为它避免了创建一个很大的临时结果并且要复制大量的内存块数据。

如果你准备编写`构建大量小字符串的输出代码`，你最好考虑下使用`生成器函数`，利用` yield `语句产生输出片段,是它并没有对输出片段到底要怎样组织做出假设

```py
def sample():
    yield 'Is'
    yield 'Chicago'
    yield 'Not'
    yield 'Chicago?'

text = ''.join(sample())    
print (text)
```

### 字符串中插入变量

**你想创建一个内嵌变量的字符串，变量被它的值所表示的字符串替换掉。**

Python 并没有对在字符串中简单替换变量值提供直接的支持(类似shell那样)。但是通过使用字符串的` format() `方法来解决这个问题。可用于sql拼接

```py
>>> s = '{name} has {n} messages.'
>>> s.format(name='Guido', n=37)
'Guido has 37 messages.'
>>>
```

如果要被替换的变量能在变量域中找到，那么你可以结合使用 `format map()和 vars()`

```py
>>> s = '{name} has {n} messages.'
>>> name = 'Guido'
>>> n = 37
>>> s.format_map(vars())
'Guido has 37 messages.'
>>>
```

`vars()`还有一个有意思的特性就是它也适用于`对象实例`。强大到超乎了的想象...

```py
>>> class Info:
...     def __init__(self, name, n):
...         self.name = name
...         self.n = n
...
>>> a = Info('Guido',37)
>>> s.format_map(vars(a))
'Guido has 37 messages.'
>>>
```

*`format 和 format map()`的一个缺陷就是它们并不能很好的处理变量缺失的情况*,一种避免这种错误的方法是另外定义一个含有`missing ()`方法的字典对象，从2.5版本开始，如果派生自dict的子类定义了 `__missing__()`方法，当`访问不存在的键时`，`dict[key]会调用 __missing__() 方法取得默认值`。

```py
class safesub(dict):
""" 防止 key 找不到"""
    def __missing__(self, key):
        return '{' + key + '}'
```

现在你可以利用这个类包装输入后传递给` format map() `

```py
>>> del n # Make sure n is undefined
>>> s.format_map(safesub(vars()))
'Guido has {n} messages.'
>>>
```

```py
import sys
def sub(text):
    return text.format_map(safesub(sys._getframe(1).f_locals))
```

sys._getframe:返回来自调用栈的一个帧对象。如果传入可选整数 depth，则返回从栈顶往下相应调用层数的帧对象。如果该数比调用栈更深，则抛出 ValueError。depth 的默认值是 0，返回调用栈顶部的帧。sys. getframe(1) 返回调用者的栈帧，可以从中访问属性 f_locals 来获得局部变量,

f_locals 是一个复制调用函数的本地变量的字典。尽管你可以改变 f_locals 的内容，但是这个修改对于后面的变量访问没有任何影响。所以，虽说访问一个栈帧看上去很邪恶，但是对它的任何操作不会覆盖和改变调用者本地变量的值。

设置完我们可以这样用。

```py
>>> name = 'Guido'
>>> n = 37
>>> print(sub('Hello {name}'))
Hello Guido
>>> print(sub('You have {n} messages.'))
You have 37 messages.
>>> print(sub('Your favorite color is {color}'))
Your favorite color is {color}
>>>
```

对于Python的字符串替换,如果不使用`format() 和 format map()`还可以有如下方式

```py
>>> name = 'Guido'
>>> n = 37
>>> '%(name) has %(n) messages.' % vars()
'Guido has 37 messages.'
>>>
```

```py
>>> import string
>>> name = 'Guido'
>>> n = 37
>>> s = string.Template('$name has $n messages.')
>>> s.substitute(vars())
'Guido has 37 messages.'
>>>
```

### 以指定列宽格式化字符串

**你有一些长字符串，想以指定的列宽将它们重新格式化。**

使用 `textwrap 模块`来格式化字符串的输出

```py
>>> s = "Look into my eyes, look into my eyes, the eyes, the eyes, \
... the eyes, not around the eyes, don't look around the eyes, \
... look into my eyes, you're under."
>>> import textwrap
>>> print(textwrap.fill(s, 70))
Look into my eyes, look into my eyes, the eyes, the eyes, the eyes,
not around the eyes, don't look around the eyes, look into my eyes,
you're under.
```

```py
>>> print(textwrap.fill(s, 40))
Look into my eyes, look into my eyes,
the eyes, the eyes, the eyes, not around
the eyes, don't look around the eyes,
look into my eyes, you're under.
```

```py
>>> print(textwrap.fill(s, 40, initial_indent=' '))
 Look into my eyes, look into my eyes,
the eyes, the eyes, the eyes, not around
the eyes, don't look around the eyes,
look into my eyes, you're under.
>>> print(textwrap.fill(s, 40, subsequent_indent=' '))
Look into my eyes, look into my eyes,
 the eyes, the eyes, the eyes, not
 around the eyes, don't look around the
 eyes, look into my eyes, you're under.
>>>
```

textwrap 模块对于字符串打印是非常有用的，特别是当你希望输出自动匹配终端大小的时候。你可以使用 os.get terminal size() 方法来获取终端的大小尺寸。比如：

```py
>>> print(textwrap.fill(s, os.get_terminal_size().columns, initial_indent=' '))
 Look into my eyes, look into my eyes, the eyes, the eyes, the eyes, not around
the eyes, don't look around the eyes, look into my eyes, you're under.
>>>
```

### 在字符串中处理 html 和 xml

**你想将 HTML 或者 XML 实体如 &entity; 或 &#code; 替换为对应的文本。再者，你需要转换文本中特定的字符 (比如<, >, 或 &)。**

如果你想替换文本字符串中的 ‘<’ 或者 ‘>’ ，使用`html.escape() 函数`可以很容易的完成。

```py
>>> s = 'Elements are written as "<tag>text</tag>".'
>>> import html
>>> print(s)
Elements are written as "<tag>text</tag>".
>>> print(html.escape(s))
Elements are written as &quot;&lt;tag&gt;text&lt;/tag&gt;&quot;.
>>> # Disable escaping of quotes
>>> print(html.escape(s, quote=False))

```

如果你正在处理的是`ASCII 文本`，并且想将`非 ASCII 文本`对应的`编码`实体嵌入进去，可以给某些 I/O 函数传递参数`errors='xmlcharrefreplace'`来达到这个目。比如：

```py
>>> s = 'Spicy Jalapeño'
>>> s.encode('ascii', errors='xmlcharrefreplace')
b'Spicy Jalape&#241;o'
>>>
```

为了替换文本中的编码实体，你需要使用另外一种方法。如果你正在处理 HTML或者 XML 文本，试着先使用一个合适的 HTML 或者 XML 解析

html ,这个方法被移除了，我的3.9的版本，

```py
>>> from html.parser import HTMLParser
>>> p = HTMLParser()
>>> p.unescape(s)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'HTMLParser' object has no attribute 'unescape'
```

xml

```py
>>> t = 'The prompt is &gt;&gt;&gt;'
>>> from xml.sax.saxutils import unescape
>>> unescape(t)
'The prompt is >>>'
>>>
```

### 字符串令牌解析

时间关系，这个以后再研究

### 编写一个简单的递归下降解析器

时间关系，这个以后再研究

### 在字节串上执行文本操作

需要注意这部分内容在Linux环境和Window环境差别有些大，书里讲的适用于window环境。

你想在字节字符串上执行普通的文本操作 (比如移除，搜索和替换)

```py
>>> data = b'Hello World'
>>> data[0:5]
'Hello'
>>> data.startswith(b'Hello')
True
>>> data.split()
['Hello', 'World']
>>> data.replace(b'Hello', b'Hello Cruel')
'Hello Cruel World'
```

操作同样也适用于字节数组

```py
>>> data = bytearray(b'Hello World')
>>> data[0:5]
bytearray(b'Hello')
>>> data.startswith(b'Hello')
True
>>> data.split()
[bytearray(b'Hello'), bytearray(b'World')]
>>> data.replace(b'Hello', b'Hello Cruel')
bytearray(b'Hello Cruel World')
```

可以使用正则表达式匹配字节字符串,Linux下无论是字节串还是字符串都可以，window下并不是这样，这里和书里有些出入。

Linux下

```py
>>> data = b'FOO:BAR,SPAM'
>>> import re
>>> re.split('[:,]',data)
['FOO', 'BAR', 'SPAM']
>>> re.split(b'[:,]',data)
['FOO', 'BAR', 'SPAM']
>>>
```

window下

```py
>>> data = b'FOO:BAR,SPAM'
>>> import re
>>> re.split('[:,]',data)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "D:\python\Python310\lib\re.py", line 231, in split
    return _compile(pattern, flags).split(string, maxsplit)
TypeError: cannot use a string pattern on a bytes-like object
>>> re.split(b'[:,]',data)
[b'FOO', b'BAR', b'SPAM']
>>>
```

字节字符串不会提供一个美观的字符串表示，也不能很好的打印出来，除非它们先被解码为一个文本字符串,但是这里Linux没有这种情况。

```py
>>> s = b'Hello World'
>>> s
b'Hello World'
>>> s.decode('ascii')
'Hello World'
```

格式化字节字符串，你得先使用标准的文本字符串，然后将其编码为字节字符串,这里也有些区别

```PY
>>> '{:10s} {:10d} {:10.2f}'.format('ACME', 100, 490.1).encode('ascii')
b'ACME              100     490.10'
>>>
```

## 第三章 数字、日期和时间

### 浮点数执行指定精度的舍入运算

**对浮点数执行指定精度的舍入运算。**

对于简单的舍入运算，使用内置的` round(value, ndigits) `函数即可。

```py
┌──[root@liruilongs.github.io]-[~]
└─$python3
Python 3.6.8 (default, Nov 16 2020, 16:55:22)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-44)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> round(1.23, 1)
1.2
>>> round(1.25361,3)
1.254
>>>
```

当一个值刚好在两个边界的中间的时候， round 函数返回离它最近的偶数。对 1.5 或者 2.5 的舍入运算都会得到 2。

```py
>>> round(1.5,0)
2.0
>>> round(2.5,0)
2.0
>>>
```

传给 `round()`函数的 ndigits 参数可以是负数,舍入运算会作用在十位、百位、千位等上面

```py
>>> a = 1627731
>>> round(a, -1)
1627730
>>> round(a, -2)
1627700
>>>
```

不要将`舍入和格式化输出`搞混淆了。如果你的目的只是简单的输出一定宽度的数，你不需要使用` round() `函数。而仅仅只需要在格式化的时候指定精度即可,

```py
>>> x = 1.23456
>>> format(x, '0.2f')
'1.23'
>>> format(x, '0.4f')
'1.2346'
>>> 'value is {:0.3f}'.format(x)
'value is 1.235'
>>>
```

可以看到，输出的数据并不相同，一个是字符串，一个是int等数字类型

```py
>>> type(format(1.5, '0.0f'))
<class 'str'>
>>> type(round(a, -1))
<class 'int'>
>>> type(round(1.25361,3))
<class 'float'>
>>>
```

不要试着去舍入浮点值来” 修正” 表面上看起来正确的问题。大多数使用到浮点的程序，没有必要也不推荐这样做.

```py
>>> 2.1 + 4.2
6.300000000000001
>>> round(2.1 + 4.2,2)
6.3
>>>
```

### 执行精确的浮点数运算

**要对浮点数执行精确的计算操作，并且不希望有任何小误差的出现。**

浮点数的一个普遍问题是它们并不能精确的表示十进制数。并且，即使是最简单的数学运算也会产生小的误差

就那上面的Demo来讲

```py
>>> 2.1 + 4.2
6.300000000000001
>>> round(2.1 + 4.2,2)
6.3
>>> (2.1 + 4.2) == 6.3
False
>>>
```

错误是由`底层 CPU 和 IEEE 754 标准`通过自己的`浮点单位去执行算术`时的特征。由于 Python 的浮点数据类型使用`底层表示存储数据`，因此你没办法去避免这样的误差。在Java里也出现同样的问题

```java
public static void main(String[] args) {
       System.out.println(2.1+4.2);
   }
// 6.300000000000001   
```

如果你想更加精确 (并能容忍一定的性能损耗)，你可以使用`decimal 模块`

```py
>>> from decimal import Decimal
>>> a = Decimal('4.2')
>>> b = Decimal('2.1')
>>> a + b
Decimal('6.3')
>>> print(a + b)
6.3
>>> (a + b) == Decimal('6.3')
True
>>>
```

decimal 模块的一个主要特征是`允许你控制计算的每一方面`，包括`数字位数和四舍五入运算`。为了这样做，你先得创建一个本地上下文并更改它的设置

```py
>>> from decimal import localcontext
>>> a = Decimal('1.3')
>>> b = Decimal('1.7')
>>> print(a / b)
0.7647058823529411764705882353
>>> with localcontext() as ctx:
... ctx.prec = 3
... print(a / b)
...
0.765
>>> with localcontext() as ctx:
... ctx.prec = 50
... print(a / b)
...
0.76470588235294117647058823529411764705882352941176
>>>
```

`decimal 模块`实现了 IBM 的” 通用小数运算规范”,Python 新手会倾向于使用`decimal 模块`来处理浮点数的精确运算。但是要根据业务需求来处理，decimal 模块主要用在涉及到金融的领域。

+ 原生的浮点数计算要快的多
+ 在真实世界中很少会要求精确到普通浮点数能提供的 17 位精度

其他的一些误差，大数和小数的加法运算(在Java里也出现同样的问题)

```py
>>> nums = [1.23e+18, 1, -1.23e+18]
>>> sum(nums)
0.0
>>> 1.23e+18 + 1 + -1.23e+18
0.0
>>> 1.23e+18 +  -1.23e+18 + 1
1.0
>>>
```

```java
   public static void main(String[] args) {
       System.out.println(1.23e+18 + 1 + -1.23e+18);
   } // 0.0
```

py的解决办法

```py
>>> import math
>>> math.fsum(nums)
1.0
>>>
```

### 数字的格式化输出

**你需要将数字格式化后输出，并控制数字的位数、对齐、千位分隔符和其他的细节。**

格式化输出单个数字的时候，可以使用内置的` format() `函数，这个前面也有好的Demo，不多说

```py
>>> x = 1234.56789
>>> format(x, '0.2f')
'1234.57'
>>> format(x, '>10.1f')
'    1234.6'
>>> format(x, '<10.1f')
'1234.6    '
>>> format(x, '0>10.1f')
'00001234.6'
>>> format(x, '^10.1f')
'  1234.6  '
>>> format(x, ',')
'1,234.56789'
>>> format(x, '0,.1f')
'1,234.6'
>>>
```

使用`指数`记法，`将 f 改成 e 或者 E(取决于指数输出的大小写形式)`。

```py
>>> format(x, 'e')
'1.234568e+03'
>>> format(x, '0.2E')
'1.23E+03'
>>>
```

同时`指定宽度和精度`的一般形式是 `'[<>ˆ]?width[,]?(.digits)?'`，其中`width 和 digits 为整数，？代表可选部分。`同样的格式也被用在`字符串`的`format()`方法中。

```py
>>> 'The value is {:0,.2f}'.format(x)
'The value is 1,234.57'
>>>
```

format() 函数 同时适用于浮点数和 decimal模块中的 Decimal 数字对象。

```py
>>> x
1234.56789
>>> format(x, '0.1f')
'1234.6'
>>> format(-x, '0.1f')
'-1234.6'
>>>
```

包含千位符的格式化跟本地化没有关系。如果你需要根据地区来显示千位符，你需要自己去调查下 locale 模块中的函数了。你同样也

可以使用字符串的 translate()方法来交换千位符。

```py
>>> swap_separators = { ord('.'):',', ord(','):'.' }
>>> format(x, ',').translate(swap_separators)
'1.234,56789'
>>> x
1234.56789
>>>
```

也可以直接使用`%`来格式化数字的

```py
>>> x
1234.56789
>>> '%0.2f' % x
'1234.57'
>>> '%10.1f' % x
'    1234.6'
>>> '%-10.1f' % x
'1234.6    '
```

### 二进制、八进制和十六进制整数转化输出

**转换或者输出使用二进制，八进制或十六进制表示的整数。**

为了将`整数`转换为`二进制、八进制或十六进制`的`文本串`，可以分别使用` bin() ,oct() 或 hex() `函数：

```bash
>>> x = 1234
>>> bin(x)
'0b10011010010'
>>> oct(x)
'0o2322'
>>> hex(x)
'0x4d2'
>>>
```

不想输出 0b , 0o 或者 0x 的前缀的话，可以使用` format() `函数

```py
>>> format(x, 'b')
'10011010010'
>>> format(x, 'o')
'2322'
>>> format(x, 'x')
'4d2'
>>>
```

整数是有符号的，所以如果你在处理负数的话，输出结果会包含一个负号

```py
>>> x = -1234
>>> format(x, 'b')
'-10011010010'
>>> format(x, 'x')
'-4d2'
>>>
```

如果你想产生一个无符号值，你需要增加一个指示最大位长度的值。比如为了显示32 位的值，

```py
>>> x = -1234
>>> format(2**32 + x, 'b')
'11111111111111111111101100101110'
>>> format(2**32 + x, 'x')
'fffffb2e'
>>>
```

为了以不同的进制转换整数字符串，简单的使用带有进制的` int() `函数即可：

```py
>>> int('4d2', 16)
1234
>>> int('10011010010', 2)
1234
>>>
```

Python 指定八进制数的语法跟其他语言稍有不同

```py
>>> import os
>>> os.chmod('script.py', 0755)
  File "<stdin>", line 1
    os.chmod('script.py', 0755)
                             ^
SyntaxError: invalid token
>>> os.chmod('app.py', 0o755)
>>
```

### 字节字符串到大整数的相互转化

**你有一个字节字符串并想将它解压成一个整数。或者，你需要将一个大整数转换为一个字节字符串。**

大整数和字节字符串之间的转换操作并不常见，一些场景也会用到，`IPv6 网络地址`使用一个`128 位`的整数表示。如果你要从一个数据记录中提取这样的值的时候

处理一个拥有 128 位长的 16 个元素的字节字符串

```py
>>> data = b'\x00\x124V\x00x\x90\xab\x00\xcd\xef\x01\x00#\x004'
>>> len(data)
16
>>> int.from_bytes(data, 'little')
69120565665751139577663547927094891008
>>> int.from_bytes(data, 'big')
94522842520747284487117727783387188
>>>
```

为了将一个大整数转换为一个字节字符串，使用` int.to_bytes() `方法，并像下面这样指定字节数和字节顺序：

```py
>>> x = 94522842520747284487117727783387188
>>> x.to_bytes(16, 'big')
b'\x00\x124V\x00x\x90\xab\x00\xcd\xef\x01\x00#\x004'
>>> x.to_bytes(16, 'little')
b'4\x00#\x00\x01\xef\xcd\x00\xab\x90x\x00V4\x12\x00'
>>>
```

这里字节顺序规则 (`little 或 big`) 仅仅指定了构建整数时的字节的`低位高位排列方式`。

```py
>>> x = 0x01020304
>>> x.to_bytes(4, 'big')
b'\x01\x02\x03\x04'
>>> x.to_bytes(4, 'little')
b'\x04\x03\x02\x01'
>>>
```

如果你试着将一个`整数打包为字节字符串`，那么它就不合适了，你会得到一个`错误`。如果需要的话，你可以使用 `int.bit_length()`方法来决定`需要多少字节位来存储这个值`。(** 表示乘方，divmod() 函数把除数和余数运算结果结合起来，返回一个包含商和余数的元组(a // b, a % b)。)

```py
>>> x = 523 ** 23
>>> x
335381300113661875107536852714019056160355655333978849017944067
>>> x.to_bytes(16, 'little')
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
OverflowError: int too big to convert
>>> x.bit_length()
208
>>> nbytes, rem = divmod(x.bit_length(), 8)
>>> if rem:
... nbytes += 1
...
>>>
>>> x.to_bytes(nbytes, 'little')
b'\x03X\xf1\x82iT\x96\xac\xc7c\x16\xf3\xb9\xcf\x18\xee\xec\x91\xd1\x98\xa2\xc8\xd9R\xb5\xd0'
>>>
```

### 复数的数学运算

**使用复数来执行一些计算操作。**

复数可以用使用函数` complex(real, imag) `或者是带有后缀` j `的浮点数来指定。

```py
>>> a = complex(2, 4)
>>> b = 3 - 5j
>>> a
(2+4j)
>>> b
(3-5j)
>>>
```

对应的实部、虚部和共轭复数可以很容易的获取。

```py
>>> a.real
2.0
>>> a.imag
4.0
>>> a.conjugate()
(2-4j)
>>>
```

常见的数学四则运算

```py
>>> a + b
(5-1j)
>>> a * b
(26+2j)
>>> a / b
(-0.4117647058823529+0.6470588235294118j)
>>> abs(a)
4.47213595499958
>>>
```

如果要执行其他的`复数函数比如正弦、余弦或平方根`，使用`cmath 模块`：

```py
>>> import cmath
>>> cmath.sin(a)
(24.83130584894638-11.356612711218174j)
>>> cmath.cos(a)
(-11.36423470640106-24.814651485634187j)
>>> cmath.exp(a)
(-4.829809383269385-5.5920560936409816j)
>>>
```

Python 中大部分与数学相关的模块都能处理复数。使用 `numpy` 很容易的构造一个复数数组并在这个数组上执行各种操作

```py
>>> import numpy as np
>>> a = np.array([2+3j, 4+5j, 6-7j, 8+9j])
>>> a
array([ 2.+3.j, 4.+5.j, 6.-7.j, 8.+9.j])
>>> a + 2
array([ 4.+3.j, 6.+5.j, 8.-7.j, 10.+9.j])
>>> np.sin(a)
array([ 9.15449915 -4.16890696j, -56.16227422 -48.50245524j,
-153.20827755-526.47684926j, 4008.42651446-589.49948373j])
>>>
```

Python 的标准数学函数确实情况下并不能产生复数值，因此你的代码中不可能会出现复数返回值

```py
>>> import math
>>> math.sqrt(-1)
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
ValueError: math domain error
>>>
```

如果你想生成一个复数返回结果，你必须显示的使用 cmath 模块

```py
>>> import cmath
>>> cmath.sqrt(-1)
1j
>>>
```

### 处理无穷大和NaN

**你想创建或测试正无穷、负无穷或 NaN(非数字) 的浮点数。**

Python 并没有特殊的语法来表示这些特殊的浮点值，但是可以使用` float() `来创建它们。比如：

```py
>>> a = float('inf')
>>> b = float('-inf')
>>> c = float('nan')
>>> a
inf
>>> b
-inf
>>> c
nan
>>>
```

使用 `math.isinf() 和 math.isnan()`函数来测试

```py
>>> math.isinf(a)
True
>>> math.isnan(c)
True
>>>
```

无穷大数在执行数学计算的时候会传播

```py
>>> a = float('inf')
>>> a + 45
inf
>>> a * 10
inf
>>> 10 / a
0.0
>>>
```

有些操作时未定义的并会返回一个 `NaN`结果

```py
>>> a = float('inf')
>>> a/a
nan
>>> b = float('-inf')
>>> a + b
nan
>>>
```

NaN 值会在所有操作中传播，而不会产生异常

```py
>>> c = float('nan')
>>> c + 23
nan
>>> c / 2
nan
>>> c * 2
nan
>>> math.sqrt(c)
nan
>>>
```

NaN 之间的比较操作总是返回 False

```py
>>> c = float('nan')
>>> d = float('nan')
>>> c == d
False
>>> c is d
False
>>>
```

有时候程序员想改变 `Python 默认行为`，在返回无穷大或 NaN 结果的操作中抛出异常。 `fpectl 模块`可以用来改变这种行为，但是它在标准的 Python 构建中并没有被启用，它是平台相关的，并且针对的是专家级程序员。

### 分数运算

**在一个允许接受分数形式的测试单位并以分数形式执行运算的程序中，直接使用分数可以减少手动转换为小数或浮点数的工作**

`fractions 模块`可以被用来执行包含分数的数学运算。

```py
>>> from fractions import Fraction
>>> a = Fraction(5, 4)
>>> b = Fraction(7, 16)
>>> print(a + b)
27/16
>>> print(a * b)
35/64
>>> c = a * b
>>> c.numerator
35
>>> c.denominator
64
>>> float(c)
0.546875
>>>
```

limit_denominator(max_denominator) 方法可以找到并返回一个 Fraction 使得其值最接近 self 并且分母不大于 max_denominator。

`as_integer_ratio()`用于将浮点数转化为分数

```py
>>> print(c.limit_denominator(8))
4/7
>>> x = 3.75
>>> y = Fraction(*x.as_integer_ratio())
>>> y
Fraction(15, 4)
>>>
```

### 处理大型数组的计算

**在大数据集 (比如数组或网格) 上面执行计算。**

到数组的重量级运算操作，可以使用`NumPy 库`。 NumPy 的一个主要特征是它会给 Python 提供一个数组对象，相比标准的 Python 列表而已更适合用来做数学运算。

额，数学忘光了....这里先记录下,`list`运算并没有遵守矩阵运算规则，肯定不能直接用

```py
>>> # Python lists
>>> x = [1, 2, 3, 4]
>>> y = [5, 6, 7, 8]
>>> x * 2
[1, 2, 3, 4, 1, 2, 3, 4]
>>> x + 10
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
TypeError: can only concatenate list (not "int") to list
>>> x + y
[1, 2, 3, 4, 5, 6, 7, 8]
```

```py
>>> # Numpy arrays
>>> import numpy as np
>>> ax = np.array([1, 2, 3, 4])
>>> ay = np.array([5, 6, 7, 8])
>>> ax * 2
```

NumPy 还为数组操作提供了大量的通用函数，这些函数可以作为 math 模块中类似函数的替代

```py
>>> np.sqrt(ax)
array([ 1. , 1.41421356, 1.73205081, 2. ])
>>> np.cos(ax)
array([ 0.54030231, -0.41614684, -0.9899925 , -0.65364362])
>>>
```

底层实现中， `NumPy 数组使用了 C 或者 Fortran 语言`的机制`分配内存`。也就是说，它们是一个非常大的连续的并由同类型数据组成的`内存区域`。所以，你可以构造一个比普通 Python 列表大的多的数组。比如，如果你想构造一个 `10,000*10,000` 的`浮点数二维网格`，很轻松

```py
>>> import numpy as np
>>> grid = np.zeros(shape=(10000,10000), dtype=float)
>>> grid
array([[0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.],
       ...,
       [0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.]])
>>>
```

所有的普通操作是遵循矩阵运算法则的：

```py
>>> grid += 10
>>> grid
array([[10., 10., 10., ..., 10., 10., 10.],
       [10., 10., 10., ..., 10., 10., 10.],
       [10., 10., 10., ..., 10., 10., 10.],
       ...,
       [10., 10., 10., ..., 10., 10., 10.],
       [10., 10., 10., ..., 10., 10., 10.],
       [10., 10., 10., ..., 10., 10., 10.]])
```

关于 NumPy,它扩展 Python 列表的索引功能 - 特别是对于多维数组,这里和matlab的数组语法有些类似

```py
>>> a = np.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])
>>> a
array([[ 1, 2, 3, 4],
       [ 5, 6, 7, 8],
       [ 9, 10, 11, 12]])
>>> # Select row 1
>>> a[1]
array([5, 6, 7, 8])
>>> # Select column 1
>>> a[:,1]
array([ 2, 6, 10])
>>> # 选择子区域并进行修改
>>> a[1:3, 1:3]
array([[ 6, 7],
[10, 11]])
>>> a[1:3, 1:3] += 10
>>> a
array([[ 1, 2, 3, 4],
      [ 5, 16, 17, 8],
      [ 9, 20, 21, 12]])
>>> # 在所有行的操作上广播行向量  
>>> a + [100, 101, 102, 103]
array([[101, 103, 105, 107],
      [105, 117, 119, 111],
      [109, 121, 123, 115]])
>>> a
array([[ 1, 2, 3, 4],
[ 5, 16, 17, 8],
[ 9, 20, 21, 12]])
>>> # 数组的条件赋值
>>> np.where(a < 10, a, 10)
array([[ 1, 2, 3, 4],
      [ 5, 10, 10, 8],
      [ 9, 10, 10, 10]])
>>>
```

### 矩阵和线性代数的计算

**执行矩阵和线性代数运算，比如矩阵乘法、寻找行列式、求解线性方程组等。**

这些一般做数据分析会用到，但是数据分析需要有一定的数学基础，时间关系这里简单了解下,好后悔之前没好好学,都忘光了...

```py
>>> import numpy as np
>>> m = np.matrix([[1,-2,3],[0,4,5],[7,8,-9]])
>>> m
matrix([[ 1, -2, 3],
[ 0, 4, 5],
[ 7, 8, -9]])
>>> # Return transpose
>>> m.T
matrix([[ 1, 0, 7],
[-2, 4, 8],
[ 3, 5, -9]])
>>> # Return inverse
>>> m.I
matrix([[ 0.33043478, -0.02608696, 0.09565217],
[-0.15217391, 0.13043478, 0.02173913],
[ 0.12173913, 0.09565217, -0.0173913 ]])
>>> # Create a vector and multiply
>>> v = np.matrix([[2],[3],[4]])
>>> v
matrix([[2],
[3],
[4]])
>>> m * v
matrix([[ 8],
[32],
[ 2]])
>>>
```

在` numpy.linalg `子包中找到更多的操作函数

```py
>>> import numpy.linalg
>>> # Determinant
>>> numpy.linalg.det(m)
-229.99999999999983
>>> # Eigenvalues
>>> numpy.linalg.eigvals(m)
array([-13.11474312, 2.75956154, 6.35518158])
>>> # Solve for x in mx = v
>>> x = numpy.linalg.solve(m, v)
>>> x
matrix([[ 0.96521739],
[ 0.17391304],
[ 0.46086957]])
>>> m * x
matrix([[ 2.],
[ 3.],
[ 4.]])
>>> v
matrix([[2],
[3],
[4]])
>>>
```

### 随机选择

**从一个序列中随机抽取若干元素，或者想生成几个随机数。**

`random 模块`有大量的函数用来`产生随机数和随机选择元`素。比如，要想从一`个序列中随机的抽取一个元素`，可以使用 `random.choice()`：

```py
>>> import random
>>> values = [1, 2, 3, 4, 5, 6]
>>> random.choice(values)
2
>>> random.choice(values)
3
>>> random.choice(values)
1
>>> random.choice(values)
4
>>> random.choice(values)
6
>>>
```

为了提取出 `N 个不同元素的样本`用来做进一步的操作，可以使用`random.sample()`

```py
>>> random.sample(values, 2)
[6, 2]
>>> random.sample(values, 2)
[4, 3]
>>> random.sample(values, 3)
[4, 3, 1]
>>> random.sample(values, 3)
[5, 4, 1]
>>>
```

只是想打乱序列中元素的顺序，可以使用` random.shuffle() `：

```py
>>> random.shuffle(values)
>>> values
[2, 4, 6, 5, 3, 1]
>>> random.shuffle(values)
>>> values
[3, 5, 2, 1, 6, 4]
>>>
```

生成随机整数，请使用`random.randint()` ：

```py
>>> random.randint(0,10)
2
>>> random.randint(0,1000)
452
```

为了生成`0 到 1 范围`内均匀分布的`浮点数`，使用`random.random()`

```py
>>> random.random()
0.9406677561675867
>>> random.random()
0.133129581343897
>>> random.random()
0.4144991136919316
>>>
```

要获取 N 位随机位 (二进制) 的整数，使用`random.getrandbits() ：`

```py
>>> random.getrandbits(200)
335837000776573622800628485064121869519521710558559406913275
>>>
```

`random 模块`使用 `Mersenne Twister 算法`来计算生成随机数。这是一个`确定性算法`，但是你可以通过` random.seed() `函数修改`初始化种子`

```py
random.seed() # Seed based on system time or os.urandom()
random.seed(12345) # Seed based on integer given
random.seed(b'bytedata') # Seed based on byte data
```

除了上述介绍的功能，`random 模块`还包含基于`均匀分布`、`高斯分布`和其他分布的`随机数生成函数`。比如，`random.uniform()`计算`均匀分布`随机数，`random.gauss()`计算正态分布随机数。

。。。。概率论的知识,唉，没好好听课

在`random 模块`中的函数不应该用在和密码学相关的程序中。，可以使用`ssl 模块`中相应的函数。比如， `ssl.RAND_bytes()`可以用来生成一个安全的随机字节序列。

### 基本的日期与时间转换

**你需要执行简单的时间转换，比如天到秒，小时到分钟等的转换。**

为了执行不同时间单位的转换和计算，请使用`datetime 模块`。比如，为了表示一个时间段，可以创建一个`timedelta 实例`,可以直接转化天到秒，但是小时的话需要运算

```py
>>> a = timedelta(days=2, hours=6)
>>> b = timedelta(hours=4.5)
>>> c = a + b
>>> c
datetime.timedelta(2, 37800)
>>> c.days
2
>>> c.hours
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'datetime.timedelta' object has no attribute 'hours'
>>>  c.seconds
  File "<stdin>", line 1
    c.seconds
    ^
IndentationError: unexpected indent
>>> c.seconds
37800
>>> c.seconds / 3600
10.5
>>> c.total_seconds() / 3600
58.5
>>>
```

表示指定的日期和时间，先创建一个` datetime `实例然后使用标准的数学运算来操作它们

```py
>>> from datetime import datetime, date, timedelta
>>> from datetime import datetime
>>> a  = datetime(2022,5,4)
>>> a
datetime.datetime(2022, 5, 4, 0, 0)
>>> a + timedelta(days=20)
datetime.datetime(2022, 5, 24, 0, 0)
>>> print(a + timedelta(days=20))
2022-05-24 00:00:00
>>> a - datetime(2021,1,1)
datetime.timedelta(488)
>>> (a - datetime(2021,1,1)).days
488
>>> datetime.today()
datetime.datetime(2022, 5, 4, 12, 21, 29, 867955)
>>> print(datetime.today())
2022-05-04 12:21:40.399207
>>> print(datetime.today() + timedelta(minutes=10))
2022-05-04 12:32:05.150013
>>>
```

需要注意的是 datetime 会自动处理闰年,基本的日期和时间处理问题，`datetime 模块`以及足够了,需要更加复杂的日期操作，可以考虑使用`dateutil 模块`,许多类似的时间计算可以使用`dateutil.relativedelta() 函数代替`

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ python3 -m  pip install python-dateutil
```

```py
>>> a = datetime(2012, 9, 23)
>>> a + timedelta(months=1)
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
TypeError: 'months' is an invalid keyword argument for this function
>>>
>>> from dateutil.relativedelta import relativedelta
>>> a + relativedelta(months=+1)
datetime.datetime(2012, 10, 23, 0, 0)
>>> a + relativedelta(months=+4)
datetime.datetime(2013, 1, 23, 0, 0)
>>>
>>> # Time between two dates
>>> b = datetime(2012, 12, 21)
>>> d = b - a
>>> d
datetime.timedelta(89)
>>> d = relativedelta(b, a)
>>> d
relativedelta(months=+2, days=+28)
>>> d.months
2
>>> d.days
28
>>>
```

### 计算上周5的日期

**你需要查找星期中某一天最后出现的日期，比如星期五。**

Python 的`datetime 模块`中有工具函数和类可以帮助你执行这样的计算

先将`开始日期`和`目标日期`映射到`星期数组`的位置上 (`星期一索引为 0`)，然后通过`模运算计算出目标日期要经过多少天才能到达开始日期`。然后用`开始日期减去那个时间差即得到结果日期`。

```py
from datetime import datetime, timedelta
weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday',
            'Friday', 'Saturday', 'Sunday']


def get_previous_byday(dayname, start_date=None):
    if start_date is None:
        start_date = datetime.today()
    day_num = start_date.weekday()
    day_num_target = weekdays.index(dayname)
    days_ago = (7 + day_num - day_num_target) % 7  #这里不是特别懂
    if days_ago == 0:
        days_ago = 7
    target_date = start_date - timedelta(days=days_ago)
    return target_date
```

可选的`start_date 参数`可以由另外一个`datetime 实例`来提供

```py
datetime.today()

print(get_previous_byday('Monday'))
get_previous_byday('Sunday', datetime(2022, 12, 21))
```

### 找出当月的日期范围

**需要在当前月份中循环每一天，想找到一个计算这个日期范围的高效方法。**

需要事先构造一个包含所有日期的列表。你可以先计算出开始日期和结束日期，然后在你步进的时候使用 `datetime.timedelta 对象`递增这个日期变量即可。

```py

from datetime import datetime, date, timedelta
import calendar


def get_month_range(start_date=None):
    if start_date is None:
        start_date = date.today().replace(day=1)
    _, days_in_month = calendar.monthrange(start_date.year, start_date.month)
    end_date = start_date + timedelta(days=days_in_month)
    return (start_date, end_date)

a_day = timedelta(days=1)
first_day, last_day = get_month_range()
while first_day < last_day:
    print(first_day)
    first_day += a_day
```

对应月份第一天的日期,使用`date 或 datetime 对象`的 replace() 方法简单的将`days`属性设置成`1`即可。`replace()`方法一个好处就是它会创建和你开始传入对象类型相同的对象

使用`calendar.monthrange() 函数`来找出该月的总天数

### 将字符串转换为日期

**应用程序接受字符串格式的输入，但是你想将它们转换为`datetime 对象`以便在上面执行非字符串操作**。

```py
>>> from datetime import datetime
>>> text = '2012-09-20'
>>> y = datetime.strptime(text, '%Y-%m-%d')
>>> z = datetime.now()
>>> print( datetime.now() -  datetime.strptime(text, '%Y-%m-%d'))
19:37:02.113440
>>>
```

`datetime.strptime()`方法支持很多的格式化代码，比如 `%Y 代表 4 位数年份， %m 代表两位数月份`,strptime() 的性能要比你想象中的差很多,如果你已经知道所以日期格式是`YYYY-MM-DD`，你可以像下面这样
实现一个解析函数.

```py
from datetime import datetime
def parse_ymd(s):
  year_s, mon_s, day_s = s.split('-')
  return datetime(int(year_s), int(mon_s), int(day_s))
```

### 处理涉及到时区的日期问题

**你有一个安排在 2012 年 12 月 21 日早上 9:30 的电话会议，地点在芝加哥。而你的朋友在印度的班加罗尔，那么他应该在当地时间几点参加这个会议呢？**

几乎所有涉及到时区的问题，你都应该使用 pytz 模块。这个包提供了 Olson 时区数据库，它是时区信息的事实上的标准，在很多语言和操作系统里面都可以找到。

pytz 模块一个主要用途是将 datetime 库创建的简单日期对象本地化

```py
>>> from datetime import datetime
>>> from pytz import timezone
>>> datetime.today()
datetime.datetime(2022, 5, 4, 18, 58, 56, 843102)
>>> print(datetime.today())
2022-05-04 18:59:12.433695
>>> print(central.localize(datetime.today()).astimezone(timezone('US/Central')))
2022-05-04 06:04:33.518289-05:00
```

本地化日期上执行计算，你需要特别注意`夏令时转换`和其他细节。这个我们不涉及，先不看

处理本地化日期的通常的策略先将所有日期转换为 UTC 时间

```py
>>> import pytz
>>> utc_d = datetime.today().astimezone(pytz.utc)
>>> utc_d
datetime.datetime(2022, 5, 4, 11, 11, 35, 701740, tzinfo=<UTC>)
>>> print(utc_d)
2022-05-04 11:11:35.701740+00:00
>>> from datetime import datetime, date, timedelta
>>> later_utc = utc_d + timedelta(hours=8)
>>> print(later_utc)
2022-05-04 19:11:35.701740+00:00
```

```py
from datetime import datetime
from datetime import timedelta
from datetime import timezone
 
SHA_TZ = timezone(
    timedelta(hours=8),
    name='Asia/Shanghai',
)
 
# 协调世界时
utc_now = datetime.utcnow().replace(tzinfo=timezone.utc)
print("UTC:")
print(utc_now, utc_now.time())
print(utc_now.date(), utc_now.tzname())
 
# 北京时间
beijing_now = utc_now.astimezone(SHA_TZ)
print("Beijing:")
print(beijing_now.strftime('%H_%M_%S'))
print(beijing_now, beijing_now.time())
print(beijing_now.date(), beijing_now.tzname())
 
# 系统默认时区
local_now = utc_now.astimezone()
print("Default:")
print(local_now, local_now.time())
print(local_now.date(), local_now.tzname())
```

## 第四章 迭代器和生成器

迭代是Python最强大的功能之一。

学习环境版本

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$python3
Python 3.6.8 (default, Nov 16 2020, 16:55:22)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-44)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>

```

### 手动访问迭代器中的元素

**你想遍历一个可迭代对象中的所有元素，但是却不想使用 for 循环。**

为了手动的遍历可迭代对象，使用`next() 函数并在代码中捕获 StopIteration 异常`。比如，下面的例子手动读取一个文件中的所有行：

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   Untitled-1.py
@Time    :   2022/05/23 00:18:55
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib


def manual_iter():
    with open('/etc/passwd') as f:
        try:
            while True:
                line = next(f)
                print(line, end='')
        except StopIteration:
            pass
manual_iter()
```

```
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
```

`StopIteration`用来指示迭代的结尾。然而，如果你手动使用上面演示的`next()`函数的话，你还可以通过返回一个指定值来标记结尾，比如 None 。不是使用异常捕获的方式，而是在异常触发前结束while

```py
    with open('/etc/passwd') as f:
        while True:
            line = next(f)
            if line is None:
                break
            print(line, end='')
```

```py
>>> items = [1, 2, 3,None]
>>> it = iter(items)
>>> next(it)
1
>>> next(it)
2
>>> next(it)
3
>>> next(it)
>>> next(it)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
>>>
```

我们来简单看一下迭代器的原理,可以通过`iter()`方法来获取一个可迭代对象的迭代器，通过`next()`方法来获取当前可迭代的元素

```py
>>> items = [1, 2, 3]
>>> it = iter(items)
>>> next(it)
1
>>> next(it)
2
>>> next(it)
3
>>> next(it)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
```

### 委托代理迭代

**构建了一个自定义容器对象，里面包含有列表、元组或其他可迭代对象。你想直接在你的这个新容器对象上执行迭代操作**

实际上你只需要定义一个` iter () `方法，将迭代操作代理到容器内部的对象上去。

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   Untitled-1.py
@Time    :   2022/05/24 00:20:38
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib


class Node:
    def __init__(self, value):
        self._value = value
        self._children = []

    def __repr__(self):
        return 'Node({!r})'.format(self._value)

    def add_child(self, node):
        self._children.append(node)
     
    def __iter__(self):
        return iter(self._children)


# Example
if __name__ == '__main__':
    root = Node(0)
    child1 = Node(1)
    child2 = Node(2)
    root.add_child(child1)
    root.add_child(child2)
    # Outputs Node(1), Node(2)
    for ch in root:
        print(ch)
```

实际上你只需要`定义一个 iter () 方法`，将`迭代操作代理到容器内部的对象`上去,上面的Demo通过

```py
    def __iter__(self):
        return iter(self._children)
```

来获取当前的自定义容器的迭代器

```bash
┌──[root@vms81.liruilongs.github.io]-[~/python_cookbook]
└─$./622.py
Node(1)
Node(2)
```

Python的迭代器协议需要 `__iter__`方法返回一个实现了`next()方法`的`迭代器对象`。如果你只是迭代遍历其他容器的内容，你无须担心底层是怎样实现的。你所要做的只是传递迭代请求既可。

这里的iter()函数的使用简化了代码，iter()只是简单的通过调用s.**iter**()方法来返回对应的迭代器对象，就跟1en(s)会调用s.**len**()原理是一样的。

### 用生成器创建新的迭代模式

实现一个自定义迭代模式，跟普通的内置函数比如` range() , reversed() `不一样。

如果想实现一种新的迭代模式，使用一个`生成器函数`来定义它。下面是一个生产某个范围内浮点数的生成器：

```py
>>> def frange(start, stop, increment):
...     x = start
...     while x < stop:
...         yield x
...         x += increment
...
>>> for n in frange(0, 4, 0.5):
...     print(n)
...
0
0.5
1.0
1.5
2.0
2.5
3.0
3.5
>>>
```

一个函数中需要有一个`yield 语句`即可将其转换为一个生成器。跟普通函数不同的是，生成器只能用于迭代操作。

```py
>>> def countdown(n):
...     print('Starting to count from', n)
...     while n > 0:
...         yield n
...         n -= 1
...     print('Done!')
...
>>> c = countdown(3)
>>> next(c)
Starting to count from 3
3
>>> next(c)
2
>>> next(c)
1
>>> next(c)
Done!
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
>>> c
<generator object countdown at 0x7fd33ac44200>
>>>
```

一个生成器函数主要特征是它只会回应在迭代中使用到的 next 操作。一旦生成器函数返回退出，迭代终止。

### 实现迭代协议

**构建一个能支持迭代操作的自定义对象，并希望找到一个能实现迭代协议的简单方法**

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   Untitled-1.py
@Time    :   2022/05/24 22:41:58
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

class Node:
    def __init__(self, value):
        self._value=value
        self._children=[]
    def __repr__(self):
         return ' Node({!r})'.format(self._value)

    def add_child(self, node):
        self._children. append(node)
    def __iter__(self):
        return iter(self._children)

    def depth_first(self):
        yield self
        for c in self:
            yield from c.depth_first()

# Example

if __name__ == '__main__':

    root = Node(0)
    child1 = Node(1)
    child2 = Node(2)
    root.add_child(child1)
    root.add_child(child2)
    child1.add_child(Node(3))
    child1.add_child(Node(4))
    child2.add_child(Node(5))
    for ch in root.depth_first():
        print(ch)
```

在这段代码中，`depth_first()`方法简单直观。它首先返回`自己本身`并迭代每一个子节点并通过调用子节点的`depth_first()`方法 (使用`yield from`语句) 返回对应元素。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/python_cookbook]
└─$vim 644.py
┌──[root@vms81.liruilongs.github.io]-[~/python_cookbook]
└─$chmod +x 644.py
┌──[root@vms81.liruilongs.github.io]-[~/python_cookbook]
└─$./644.py
 Node(0)
 Node(1)
 Node(3)
 Node(4)
 Node(2)
 Node(5)
```

Python 的`迭代协议`要求一个`iter ()`方法返回一个`特殊的迭代器对象`，这个迭代器对象实现了`next ()`方法并通过 `StopIteration`异常标识迭代的完成。

### 反向迭代

**反方向迭代一个序列**

使用内置的` reversed() `函数，

```py
>>> a= [1,2,3,4]
>>> for i in reversed(a):
...     print(i)
...
4
3
2
1
>>>
```

`反向迭代`仅仅当对象的大小可预先确定或者对象实现了`__reversed__()` 的特殊方法时才能生效。如果两者都不符合，那你必须先将对象转换为一个列表才行.

```py
>>> f = open('/etc/passwd')
>>> for line in reversed(list(f)):
...     print(line,end='')
...
opensips:x:997:993:OpenSIPS SIP Server:/var/run/opensips:/sbin/nologin
oprofile:x:16:16:Special user account to be used by OProfile:/var/lib/oprofile:/sbin/nologin
nfsnobody:x:65534:65534:Anonymous NFS User:/var/lib/nfs:/sbin/nologin
rpcuser:x:29:29:RPC Service User:/var/lib/nfs:/sbin/nologin
rpc:x:32:32:Rpcbind Daemon:/var/lib/rpcbind:/sbin/nologin
......
```

自定义实现反向迭代，通过在自定义类上实现`__reversed()__` 方法来实现反向迭代。

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   Untitled-1.py
@Time    :   2022/05/24 22:41:58
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib


class Countdown:
    def __init__(self, start):
        self.start = start

# Forward iterator
    def __iter__(self):
        n = self.start
        while n > 0:
            yield n
            n -= 1
# Reverse iterator
    def __reversed__(self):
        n = 1
        while n <= self.start:
            yield n
            n += 1



print(format('逆序','*>20'))
for rr in reversed(Countdown(5)):
    print(rr)
print(format('正序','*>20'))
for rr in Countdown(5):
    print(rr)
```

简单分析一下这个逆序的迭代器，魔法方法`__iter__`返回一个可迭代的对象，这里通过生成器来实现，当n>0的时候，通过生成器返回迭代元素。魔法方法`__reversed__`实现一个逆序的迭代器，原理和默认迭代器基本相同，不同的是，默认迭代器是默认调用，而逆向迭代器是主动调用

```bash
┌──[root@vms81.liruilongs.github.io]-[~/python_cookbook]
└─$./653.py
******************逆序
1
2
3
4
5
******************正序
5
4
3
2
1
```

### 定义带有额外状态的生成器函数

**定义一个生成器函数，但是它会调用某个你想暴露给用户使用的外部状态值。**

如果想让生成器暴露外部状态给用户，可以简单的将它实现为一个类，然后把生成器函数放到` __iter__() `方法中过去,简单来讲就是上面我们演示的代码，通过生成器来模拟`next()`方法行为

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   Untitled-1.py
@Time    :   2022/05/24 22:41:58
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

from collections import deque
def count(n):
   while True:
     yield n
     n += 1

class linehistory:
    def __init__(self, lines, histlen=3):
        self.lines = lines
        self.history = deque(maxlen=histlen)

    def __iter__(self):
        for lineno, line in enumerate(self.lines, 1):
            self.history.append((lineno, line))
            yield line

    def clear(self):
        self.history.clear()


if __name__ == "__main__":
    with open('/etc/services') as f:
        lines = linehistory(f)
        for line in lines:
            if '8080' in line:
                for lineno, hline in lines.history:
                    print('{}:{}'.format(lineno, hline), end='')
```

这里的`deque(maxlen=N)创建了一个固定长度的双端队列`,用于存放要保留的数据，把文件的所有的行数据存放大lines里，默认队列的大小是3,然后通过for循环迭代，在获取迭代器的方法里，我们可以看到通过`enumerate`来获取迭代对象和索引，然后放到队列里，通过yield模拟next方法返回迭代元素，所以队列里存放的默认为当前元素的前两个元素，

```bash
┌──[root@vms81.liruilongs.github.io]-[~/python_cookbook]
└─$./662.py
553:xfs             7100/tcp        font-service    # X font server
554:tircproxy       7666/tcp                        # Tircproxy
555:webcache        8080/tcp        http-alt        # WWW caching service
554:tircproxy       7666/tcp                        # Tircproxy
555:webcache        8080/tcp        http-alt        # WWW caching service
556:webcache        8080/udp        http-alt        # WWW caching service
┌──[root@vms81.liruilongs.github.io]-[~/python_cookbook]
└─$(cat -n  /etc/services  | grep -B2  -m 1 8080;cat -n  /etc/services  | grep -B1  -m 2 8080)
   553  xfs             7100/tcp        font-service    # X font server
   554  tircproxy       7666/tcp                        # Tircproxy
   555  webcache        8080/tcp        http-alt        # WWW caching service
   554  tircproxy       7666/tcp                        # Tircproxy
   555  webcache        8080/tcp        http-alt        # WWW caching service
   556  webcache        8080/udp        http-alt        # WWW caching service
```

如果你在迭代操作时不使用`for 循环`语句，那么你得先调用`iter()`函数获取迭代器，然后通过next来获取迭代元素

### 对迭代器做切片操作

**得到一个由迭代器生成的切片对象，但是标准切片操作并不能做到。**

函数` itertools.islice() `正好适用于在迭代器和生成器上做切片操作

```py
>>> def count(n):
...    while True:
...      yield n
...      n += 1
...
>>> c = count(0)
>>> c[10:20]
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'generator' object is not subscriptable
>>> import itertools
>>> for x in itertools.islice(c,5,10):
...    print(x)
...
5
6
7
8
9
>>>
```

`迭代器和生成器`不能使用`标准的切片操作`，因为它们的`长度事先我们并不知道` (并且也没有实现索引)。函数`islice()`返回一个可以生成指定元素的迭代器，它通过遍历并丢弃直到切片开始索引位置的所有元素。然后才开始一个个的返回元素，并直到切片结束索引位置。`islice()`会消耗掉传入的迭代器中的数据。必须考虑到迭代器是不可逆的这个事实。

### 跳过可迭代对象中的前一部分元素

**遍历一个可迭代对象，但是它开始的某些元素你并不感兴趣，想跳过它们**

`itertools 模块`中有一些函数可以完成这个任务。 首先介绍的是`itertools.dropwhile()`函数。使用时，你给它传递一个函数对象和一个可迭代对象。它会返回一个迭代器对象，丢弃原有序列中直到函数返回 True 之前的所有元素，然后返回后面所有元素。

类似一个过滤器，返回满足条件的数据

```bash
┌──[root@vms81.liruilongs.github.io]-[~/python_cookbook]
└─$tee temp.txt <<- EOF
> #sdfsdf
> #dsfsf
> #sdfsd
> sdfdsf
> sdfs
> EOF
#sdfsdf
#dsfsf
#sdfsd
sdfdsf
sdfs
```

```py
>>> with open('temp.txt') as f:
...     for line in dropwhile(lambda line: line.startswith('#'),f):
...         print(line,end=' ')
...
sdfdsf
 sdfs
 >>>
```

如果你已经明确知道了要跳过的元素的个数的话，那么可以使用 `itertools.islice()`来代替

```py
>>> from itertools import islice
>>> items = ['a', 'b', 'c', 1, 4, 10, 15]
>>> for x in islice(items, 3, None):
...         print(x)
...
1
4
10
15
>>>
```

islice() 函数最后那个 None 参数指定了你要获取从第 3 个到最后的所有元素，如果 None 和 3 的位置对调，意思就是仅仅获取前三个元素恰恰相反，(这个跟切片的相反操作 [3:] 和 [:3] 原理是一样的)。

### 迭代所有可能的组合或排列

**想迭代遍历一个集合中元素的所有可能的排列或组合**

`itertools模块`提供了三个函数来解决这类问题。其中一个是`itertools.permutations()`，它`接受一个集合并产生一个元组序列`，每个元组由集合中所有元素的一个可能`排列`组成。也就是说通过打乱集合中元素排列顺序生成一个元组

```py
>>> items = ['a','b','c']
>>> from itertools import permutations
>>> for p in permutations(items):
...     print(p)
...
('a', 'b', 'c')
('a', 'c', 'b')
('b', 'a', 'c')
('b', 'c', 'a')
('c', 'a', 'b')
('c', 'b', 'a')
>>>
```

如果你想得到指定长度的所有排列，你可以传递一个可选的长度参数。

```py
>>> for p in permutations(items,2):
...     print(p)
...
('a', 'b')
('a', 'c')
('b', 'a')
('b', 'c')
('c', 'a')
('c', 'b')
>>>
```

使用` itertools.combinations() `可得到输入集合中元素的所有的`组合`

```py
>>> from itertools import combinations
>>> for i in combinations(items,3):
...   print(i)
...
('a', 'b', 'c')
>>> for i in combinations(items,2):
...   print(i)
...
('a', 'b')
('a', 'c')
('b', 'c')
>>> for i in combinations(items,1):
...   print(i)
...
('a',)
('b',)
('c',)
>>>
```

这里需要注意的一点，combinations必须要指定参与组合的元素个数

```py
>>> for i in combinations(items):
...     print(i)
...
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: Required argument 'r' (pos 2) not found
```

### 以索引-值对的形式迭代序列

**迭代一个序列的同时跟踪正在被处理的元素索引。**

内置的`enumerate()` 函数可以很好的解决这个问题：

```py
>>> my_list = ['a', 'b', 'c']
>>> for idx,item in enumerate(my_list):
...    print(idx,item)
...
0 a
1 b
2 c
>>>
```

可以指定索引

```py
>>> for idx,item in enumerate(my_list,1):
...    print(idx,item)
...
1 a
2 b
3 c
>>>
```

在处理列表嵌套的元组的时候需要注意的问题

```py
>>> data = [ (1, 2), (3, 4), (5, 6), (7, 8) ]
>>> for n, (x, y) in enumerate(data):
...     print(n,x,y)
...
0 1 2
1 3 4
2 5 6
3 7 8
>>> for n, x, y in enumerate(data):
...     print(n,x,y)
...
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: not enough values to unpack (expected 3, got 2)
>>>

```

### 同时迭代多个序列

**同时迭代多个序列，每次分别从一个序列中取一个元素**

为了同时迭代多个序列，使用`zip() 函数`。

```py
>>> xpts = [1, 5, 4, 2, 10, 7]
>>> ypts = [101, 78, 37, 15, 62, 99]
>>> for x, y in zip(xpts, ypts):
...     print(x,y)
...
1 101
5 78
4 37
2 15
10 62
7 99
>>>
```

`zip(a，b)`会生成一个可返回元组`(x，y)`的迭代器，其中x来自a，y来自b。一旦其中某个序列到底结尾，迭代宣告结束。因此迭代长度跟参数中最短序列长度一致。

```py
>>> xpts = [1, 5, 4, 2, 10, 7]
>>> ypts = [101, 78, 37]
>>> for x, y in zip(xpts, ypts):
...     print(x,y)
...
1 101
5 78
4 37
>>>
```

如果这个不是你想要的效果，那么还可以使用` itertools.zip_longest() `函数来代替。

```py
>>> from itertools import zip_longest
>>> for i in zip_longest(xpts,ypts):
...         print(i)
...
(1, 101)
(5, 78)
(4, 37)
(2, None)
(10, None)
(7, None)
>>>
```

使用 zip()可以将两个list打包并生成一个字典：

```py
>>> xpts = [1, 5, 4, 2, 10, 7]
>>> ypts = [101, 78, 37]
>>> dict(zip(xpts,ypts))
{1: 101, 5: 78, 4: 37}
>>>
```

`zip()`会创建一个`迭代器来作为结果返回`。如果你需要将结对的值存储在`列表中`，要使用 `list()` 函数

```py
>>> zip(xpts,ypts)
<zip object at 0x7f413b75e1c8>
>>> list(zip(xpts,ypts))
[(1, 101), (5, 78), (4, 37)]
>>> dict(zip(xpts,ypts))
{1: 101, 5: 78, 4: 37}
>>>
```

### 在不同的容器中进行合并迭代

**想在多个对象执行相同的操作，但是这些对象在不同的容器中，你希望代码在不失可读性的情况下避免写重复的循环。**

`itertools.chain()`方法可以用来简化这个任务。它接受一个可迭代对象列表作为输入，并返回一个迭代器，有效的屏蔽掉在多个容器中迭代细节

```py
>>> from itertools import chain
>>> a = [1, 2, 3, 4]
>>> b = ['x', 'y', 'z']
>>> for x in chain(a, b):
...     print(x)
...
1
2
3
4
x
y
z
>>>
```

其他的容器也可以

```py
# Various working sets of items
active_items = set()
inactive_items = set()
# Iterate over all items
for item in chain(active_items, inactive_items):
# Process item
```

### 创建处理数据的管道

**想以数据管道 (类似 Unix 管道) 的方式迭代处理数据。比如，你有个大量的数据需要处理，但是不能将它们一次性放入内存中**

生成器函数是一个实现管道机制的好办法

```py

```

### 扁平化处理嵌套型的序列

**将一个多层嵌套的序列展开成一个单层列表**

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   Untitled-1.py
@Time    :   2022/05/30 03:10:41
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib

from collections import Iterable


def flatten(items, ignore_types=(str, bytes)):
    for x in items:
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):
            yield from flatten(x)
        else:
            yield x


items = [1, 2, [3, 4, [5, 6], 7], 8]
# Produces 1 2 3 4 5 6 7 8
for x in flatten(items):
    print(x)

items = ['Dave', 'Paula', ['Thomas', 'Lewis']]
for x in flatten(items):
        print(x)
```

我们来简单的梳理一下这个方法，利用递归和yield from 的语法，深度便利所以的元素，返回的遍历的每个元素，需要说明`isinstance(x, Iterable)`用于判断某个元素是否可迭代。

额外的参数`ignore_types`和检测语句`isinstance(x，ignore_types)`用来将`字符串和字节排除在可迭代对象外`，防止将它们再展开成单个的字符。这样的话字符串数组就能最终返回我们所期望的结果了。比如：

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$./listtolist.py
1
2
3
4
5
6
7
8
Dave
Paula
Thomas
Lewis
```

关于语句 yield from 在你想在生成器中调用其他生成器作为子例程的时候非常有用，当然也可以通过for循环来实现

### 合并多个有序序列，再对整个有序序列进行迭代

**有多个排序序列，想将它们合并后得到一个排序序列并在上面迭代遍历**

我们可以通过`heapq.merge()` 函数来实现

```py
>>> import heapq
>>> a = [1, 4, 7, 10]
>>> b = [2, 5, 8, 11]
>>> for c in heapq.merge(a,b):
...   print(c)
...
1
2
4
5
7
8
10
11
>>>
```

heapq.merge 可迭代特性意味着`它不会立马读取所有序列`。这就意味着你可以在非常长的序列中使用它，而不会有太大的开销,但是有一点要强调的是`heapq.merge()`需要所有输入序列必须是`排过序`的。特别的，它并不会预先读取所有数据到堆栈中或者预先排序，也不会对输入做任何的排序检测。它仅仅是检查所有序列的开始部分并返回最小的那个，这个过程一直会持续直到所有输入序列中的元素都被遍历完。

```py
>>> import  heapq
>>> with  open('16943_26281_10022_20220523_DATA.txt','rt') as file1,\
... open('16943_26281_10022_20220523_DATA.txt','rt') as file2,\
... open('16943_26281_10022_20220523_DATA_2022.txt','wt') as file3:
...      for c in heapq.merge(file1,file2):
...           file3.write(c)
...
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$wc -l 16943_26281_10022_20220523_DATA_2022.txt
1810 16943_26281_10022_20220523_DATA_2022.txt
┌──[root@vms81.liruilongs.github.io]-[~]
└─$wc -l 16943_26281_10022_20220523_DATA.txt
905 16943_26281_10022_20220523_DATA.txt
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

### 用迭代器取代while循环

**使用`while 循环`来迭代处理数据，因为它需要调用`某个函数`或者和一般迭代模式不同的测试条件。能不能用迭代器来重写这个循环呢？**

```py
>>> import  sys
>>> f = open('/etc/passwd')
>>> for i in iter(lambda  : f.read(10),''):
...     n = sys.stdout.write(i)
...
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologi
```

`iter函数`一个鲜为人知的特性是它接受一个可选的`callable 对象`和`一个标记(结尾)值`作为输入参数。当以这种方式使用的时候，它会创建`一个迭代器，这个迭代器会不断调用callable对象直到返回值和标记值相等为止。`

我们来看一下对应的方法，通过open函数获取当前文件的内容，我们一般会通过 whit的方式来接收，这里我们通过iter函数，利用lambda表达式获取当前文件的内容，以及结束的标识，反复迭代输出文件内容。

## 第五章 文件和l/O

### 读写文本数据

### 将输出重定向到文件中

### 以不同的分隔符或行结尾符完成打印

### 读写二进制数据

### 对已不存在的文件执行写入操作

### 在字符串上执行I/O操作

### 读写压缩的数据文件

### 对固定大小的记录进行迭代

### 将二进制数据读取到可变缓冲区中

### 对二进制文件做内存映射

### 检测文件是否存在

### 获取目录内容的列表

### 绕过文件名编码

### 打印无法解码的文件名

### 为已经打开的文件添加或修改编码方式

### 将字节数据写入文本文件

### 将已有的文件描述符包装为文件对象

### 创建临时文件和目录

### 同串口进行通信

### 序列化Python对象

## 第七章：函数

### 7.1可接受任意数量参数的函数

**你想构造一个可接受任意数量参数的函数。**

为了能让一个函数接受任意数量的位置参数，可以使用一个 * 参数

```python
def avg(first, *rest):
    return (first + sum(rest)) / (1 + len(rest))

avg(1, 2) # 1.5
avg(1, 2, 3, 4) # 2.5
```

在这个例子中，rest是由所有其他位置参数组成的元组。然后我们在代码中把它当成了一个序列来进行后续的计算。

为了接受任意数量的k-v参数，使用一个以**开头的参数。比如：

```python
import html


def make_element(name, value, **attrs):
    keyvals = [' %s="%s"' % item for item in attrs.items()]
    attr_str = ''.join(keyvals)
    element = '<{name}{attrs}>{value}</{name}>'.format(
        name=name,
        attrs=attr_str,
        value=html.escape(value))
    return element;

#<item size="large" quantity="6">Albatross</item>
print( make_element('item', 'Albatross', size='large', quantity=6))
#<p>&lt;spam&gt;</p>
print(make_element('p', '<spam>'))

```

如果你还希望某个函数能同时接受任意数量的位置参数和关键字参数，可以同时使用*和**。比如：

```python
def anyargs(*args, **kwargs):
    print(args) # A tuple
    print(kwargs) # A dict
```

使用这个函数时，所有位置参数会被放到args元组中，所有关键字参数会被放到字典kwargs中。

一个`*`参数只能出现在函数定义中`最后一个位置参数后面`，而`**`参数只能出现在`最后一个参数`。有一点要注意的是，在`*`参数后面仍然可以定义其他参数。

```python
def a(x, *args, y):
    pass

def b(x, *args, y, **kwargs):
    pass

```

### 7.2只接受关键字参数的函数

**你希望函数的某些参数强制使用关键字参数传递**

将强制关键字参数放到某个 *参数或者当个* 后面就能达到这种效果

```python
def recv(maxsize, *, block):

    'Receives a message'
    pass

recv(1024, True)  # TypeError: recv() takes 1 positional argument but 2 were given 
recv(1024, block=True)  # Ok
```

利用这种技术，我们还能在接受任意多个位置参数的函数中指定关键字参数。比

```python
def mininum(*values, clip=None):
    m = min(values)
    if clip is not None:
        m = clip if clip > m else m
    return m


mininum(1, 5, 2, -5, 10)  # Returns -5
mininum(1, 5, 2, -5, 10, clip=0)  # Returns 0
```

很多情况下，使用强制关键字参数会比使用位置参数表意更加清晰，另外，使用强制关键字参数也会比使用**kwargs 参数更好，因为在使用函数help的时候输出也会更容易理解：

```python
def mininum(*values, clip=None):
    """
    @Time    :   2022/07/08 23:08:07
    @Author  :   Li Ruilong
    @Version :   1.0
    @Desc    :   None
                 Args:
                    *values
                     clip=None
                 Returns:
                     m
    """
    m = min(values)
    if clip is not None:
        m = clip if clip > m else m
    return m
help(mininum)
```

嗯，执行输出

```bash
Help on function mininum in module __main__:

mininum(*values, clip=None)
    @Time    :   2022/07/08 23:08:07
    @Author  :   Li Ruilong
    @Version :   1.0
    @Desc    :   None
                 Args:
    
                 Returns:
                   void


Process finished with exit code 0
```

### 7.3给函数参数增加元信息

**你写好了一个函数，然后想为这个函数的参数增加一些额外的信息，这样的话其他使用者就能清楚的知道这个函数应该怎么使用。**

使用函数参数注解是一个很好的办法，它能提示程序员应该怎样正确使用这个函数。例如，下面有一个被注解了的函数：

```python
Python 3.9.0 (tags/v3.9.0:9cf6752, Oct  5 2020, 15:23:07) [MSC v.1927 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> def add(x:int, y:int) -> int:
...     return x + y
...
>>> help(add)
Help on function add in module __main__:

add(x: int, y: int) -> int

>>> add.__annotations__
{'x': <class 'int'>, 'y': <class 'int'>, 'return': <class 'int'>}
>>>
```

python解释器不会对这些注解添加任何的语义。它们不会被类型检查，运行时跟没有加注解之前的效果也没有任何差距。然而，对于那些阅读源码的人来讲就很有帮助啦。第三方工具和框架可能会对这些注解添加语义。同时它们也会出现在文档中。

尽管你可以使用任意类型的对象给函数添加注解 (例如数字，字符串，对象实例等等)，不过通常来讲使用类或着字符串会比较好点。

### 7.4返回多个值的函数

你希望构造一个可以返回多个值的函数

为了能返回多个值，函数直接 return 一个元组就行了。例如：

```python
>>> def myfun():
...     return 1,2,3
...
>>> myfun()
(1, 2, 3)
>>>
```

```python
>>> a,b,c = myfun()
>>> a
1
>>> c
3
>>>
```

### 7.5定义有默认参数的函数

**你想定义一个函数或者方法，它的一个或多个参数是可选的并且有一个默认值**

```python
>>> def spam(a, b=42):
...     print(a, b)
...
>>> spam(1)
1 42
>>> spam(1, 2)
1 2
>>>
```

如果默认参数是一个可修改的容器比如一个列表、集合或者字典，可以使用None作为默认值，就像下面这样：

```python
>>> def spam(a, b=None):
...     if b is None:
...        print(b)
...
>>> spam(a)
None
>>> spam(a,12)
>>>
```

如果你并不想提供一个默认值，而是想仅仅测试下某个默认参数是不是有传递进来，可以像下面这样写：

```python
>>> _no_value = object()
>>> def spam(a, b=_no_value):
...     if b is _no_value:
...             print('No b value supplied')
...
>>> spam(1)
No b value supplied
>>> spam(1, 2)
>>> spam(1, None)
>>>
```

传递一个 None 值和不传值两种情况是有差别的。

默认参数的值仅仅在函数定义的时候赋值一次

```
>>> x = 42
>>> def spam(a,b = x):
...     print(a,b)
...
>>> spam(1)
1 42
>>> x= 23
>>> spam(1)
1 42
>>>
```

注意到当我们改变 x 的值的时候对默认参数值并没有影响，这是因为在函数定义的时候就已经确定了它的默认值了

其次，默认参数的值应该是不可变的对象，比如None、True、False、数字或字符串。特别的，千万不要像下面这样写代码：

```python
def spam(a, b=[]): # NO!
```

如果你这么做了，当默认值在其他地方被修改后你将会遇到各种麻烦。这些修改会影响到下次调用这个函数时的默认值。比如：

```
>>> def spam(a, b=[]):
... print(b)
... return b
...
>>> x = spam(1)
>>> x
[]
>>> x.append(99)
>>> x.append('Yow!')
>>> x
[99, 'Yow!']
>>> spam(1) # Modified list gets returned!
[99, 'Yow!']
>>>
```

最好是将默认值设为None，然后在函数里面检查它，前面的例子就是这样做的。

在测试 None 值时使用 is 操作符是很重要的，

```python
def spam(a, b=None):
        if not b: # NO! Use 'b is None' instead
            b = []
```

这么写的问题在于尽管None值确实是被当成False，但是还有其他的对象(比如长度为0的字符串、列表、元组、字典等)都会被当做False。因此，上面的代码会误将一些其他输入也当成是没有输入。比如：

```python
>>> spam(1) # OK
>>> x = []
>>> spam(1, x) # Silent error. x value overwritten by default
>>> spam(1, 0) # Silent error. 0 ignored
>>> spam(1, '') # Silent error. '' ignored
>>>
```

一个函数需要测试某个可选参数是否被使用者传递进来。这时候需要小心的是你不能用某个默认值比如None、0或者False值来测试用户提供的值(因为这些值都是合法的值，是可能被用户传递进来的)。因此，你需要其他的解决方案了。

为了解决这个问题，你可以创建一个独一无二的私有对象实例，就像上面的no-value 变量那样。在函数里面，你可以通过检查被传递参数值跟这个实例是否一样来判断。这里的思路是用户不可能去传递这个no-value实例作为输入。因此，这里通过检查这个值就能确定某个参数是否被传递进来了。

这里对object() 的使用看上去有点不太常见。object是python中所有类的基类。
你可以创建object类的实例，但是这些实例没什么实际用处，因为它并没有任何有用的方法，也没有哦任何实例数据(因为它没有任何的实例字典，你甚至都不能设置任何属性值)。你唯一能做的就是测试同一性。这个刚好符合我的要求，因为我在函数中就只是需要一个同一性的测试而已。

### 7.6定义匿名或内联函数

**你想为sort()操作创建一个很短的回调函数，但又不想用def 去写一个单行函数，而是希望通过某个快捷方式以内联方式来创建这个函数。**

当一些函数很简单，仅仅只是计算一个表达式的值的时候，就可以使用lambda表达式来代替了。比如：

```python
>>> add = lambda x, y: x + y
>>> add(2,3)
5
>>> add("li",'ruilong')
'liruilong'
>>>
```

这里和JS里的语法基本相同

```
>>> names = ['David Beazley', 'Brian Jones','Raymond Hettinger', 'Ned Batchelder']
>>> sorted(names, key=lambda name: name.split()[-1].lower())
['Ned Batchelder', 'David Beazley', 'Raymond Hettinger', 'Brian Jones']
>>>
```

尽管lambda表达式允许你定义简单函数，但是它的使用是有限制的。你只能指定单个表达式，它的值就是最后的返回值。也就是说不能包含其他的语言特性了，包括多个语句、条件表达式、迭代以及异常处理等等。

你可以不使用lambda 表达式就能编写大部分python代码。但是，当有人编写大量计算表达式值的短小函数或者需要用户提供回调函数的程序的时候，你就会看到lambda表达式的身影了。

### 7.7匿名函数捕获变量值

**你用 lambda 定义了一个匿名函数，并想在定义时捕获到某些变量的值。**

```python
>>> x = 10
>>> a = lambda y: x + y
>>> x = 20
>>> b = lambda y: x + y
>>> a(10)
30
>>> b(10)
30
>>>
```

这其中的奥妙在于lambda表达式中的x是一个自由变量，在运行时绑定值，而不是定义时就绑定，这跟函数的默认值参数定义是不同的。因此，在调用这个lambda表达式的时候，x的值是执行时的值。例如：

```python
>>> x = 10
>>> a = lambda y: x + y
>>> a(10)
20
>>> x = 3
>>> a(10)
13
>>>
```

如果你想让某个匿名函数在定义时就捕获到值，可以将那个参数值定义成默认参数即可，就像下面这样：

```python
>>> x = 10
>>> a = lambda y, x=x: x + y
>>> x = 20
>>> b = lambda y, x=x: x + y
>>> a(10)
20
>>> b(10)
30
>>>
```

### 7.8减少可调用对象的参数个数

**你有一个被其他 python代码使用的callable 对象，可能是一个回调函数或者是一个处理器，但是它的参数太多了，导致调用时出错。**

如果需要减少某个函数的参数个数，你可以使用`functools.partial()`。
`partial()`函数允许你给一个或多个参数设置固定的值，减少接下来被调用时的参数个数。为了演示清楚，假设你有下面这样的函数：

```
def spam(a, b, c, d):
    print(a, b, c, d)
```

现在我们使用` partial() `函数来固定某些参数值：

```python
>>> from functools import partial
>>> s1 = partial(spam, 1) # a = 1
>>> s1(2, 3, 4)
1 2 3 4
>>> s1(4, 5, 6)
1 4 5 6
>>> s2 = partial(spam, d=42) # d = 42
>>> s2(1, 2, 3)
1 2 3 42
>>> s2(4, 5, 5)
4 5 5 42
>>> s3 = partial(spam, 1, 2, d=42) # a = 1, b = 2, d = 42
>>> s3(3)
1 2 3 42
>>> s3(4)
1 2 4 42
>>> s3(5)
1 2 5 42
>>>
```

可以看出`partial()`固定某些参数并返回一个新的`callable`对象。这个新的`callable`接受未赋值的参数，然后跟之前已经赋值过的参数合并起来，最后将所有参数传递给原始函数。

```python
def distance(p1, p2):
    x1, y1 = p1
    x2, y2 = p2

    return math.hypot(x2 - x1, y2 - y1)
```

现在假设你想以某个点为基点，根据点和基点之间的距离来排序所有的这些点。列表的 sort()方法接受一个关键字参数来自定义排序逻辑，但是它只能接受一个单个参数的函数(distance()很明显是不符合条件的)。现在我们可以通过使用 partial()来解决这个问题：

```
>>> pt = (4, 3)
>>> points.sort(key=partial(distance,pt))
>>> points
[(3, 4), (1, 2), (5, 6), (7, 8)]
>>>
```

partial() 通常被用来微调其他库函数所使用的回调函数的参数

使用 multiprocessing 来异步计算一个结果值，然后这个值被传递给一个接受一个 result 值和一个可选 logging 参数的回调函数

```python
def output_result(result, log=None):
    if log is not None:
        log.debug('Got: %r', result)

# A sample function
def add(x, y):
    return x + y


if __name__ == '__main__':

    import logging
    from multiprocessing import Pool
    from functools import partial
    logging.basicConfig(level=logging.DEBUG)
    log = logging.getLogger('test')
    p = Pool()
    p.apply_async(add, (3, 4), callback=partial(output_result, log=log))
    p.close()
    p.join()
```

当给`apply-async()`提供回调函数时，通过使用partial()传递额外的logging参数。而multiprocessing对这些一无所知——它仅仅只是使用单个值来调用回调函数。

作为一个类似的例子，考虑下编写网络服务器的问题，socketserver ,使用 partial() 就能很轻松的解决——给它传递 ack 参数的值来初始化即可

```python
from socketserver import StreamRequestHandler, TCPServer
from functools import partial

class EchoHandler(StreamRequestHandler):
    def __init__(self, *args, ack, **kwargs):
        self.ack = ack
        super().__init__(*args, **kwargs)

    def handle(self):
        for line in self.rfile:
            self.wfile.write(self.ack + line)


#serv = TCPServer(('', 15000), EchoHandler)
serv = TCPServer(('', 15000), partial(EchoHandler, ack=b'RECEIVED:'))
serv.serve_forever()

```

很多时候partial()能实现的效果，lambda表达式也能实现。比如，之前的几个例子可以使用下面这样的表达式：

```python
points.sort(key=lambda p: distance(pt, p))
p.apply_async(add, (3, 4), callback=lambda result: output_result(result,log))
serv = TCPServer(('', 15000),lambda *args, **kwargs: EchoHandler(*args, ack=b'RECEIVED:', **kwargs))

```

### 7.9将单方法的类转换为函数

你有一个除 init () 方法外只定义了一个方法的类。为了简化代码，你想将它转换成一个函数。

哈，Java的函数式接口

```python
from urllib.request import urlopen

class UrlTemplate:
    def __init__(self, template):
        self.template = template
    def open(self, **kwargs):
        return urlopen(self.template.format_map(kwargs))


baidu = UrlTemplate('https://kaifa.baidu.com/searchPage?wd={names}&module={fields}')
for line in baidu.open(names='python', fields='SUG'):
    print(line.decode('utf-8'))
```

这个类可以被一个更简单的函数来代替：

```python
from urllib.request import urlopen
def urltemplate(template):
    def opener(**kwargs):
        return  urlopen(template.format_map(kwargs))
    return  opener

baidu = urltemplate('https://kaifa.baidu.com/searchPage?wd={names}&module={fields}')
for line in baidu(names='python', fields='SUG'):
    print(line.decode('utf-8'))
```

使用一个内部函数或者闭包的方案通常会更优雅一些。简单来讲，一个闭包就是一个函数，只不过在函数内部带上了一个额外的变量环境。闭包关键特点就是它会记住自己被定义时的环境。因此，在我们的解决方案中，`opener()`函数记住了`template`参数的值，并在接下来的调用中使用它。

任何时候只要你碰到`需要给某个函数增加额外的状态信息的问题`，都可以考虑使用闭包。相比将你的函数转换成一个类而言，闭包通常是一种更加简洁和优雅的方案。Python 装饰器

```

```

### 7.10带额外状态信息的回调函数

你的代码中需要依赖到回调函数的使用(比如事件处理器、等待后台任务完成后的回调等)，并且你还需要让回调函数拥有额外的状态值，以便在它的内部使用到。

会不会很熟悉，好多的js里面也会这样写，

```python
def apply_async(func, args, *, callback):
    # Compute the result
    result = func(*args)
    # Invoke the callback with the result
    callback(result)
```

```python
def apply_async(func, args, *, callback):
    # Compute the result
    result = func(*args)
    # Invoke the callback with the result
    callback(result)

def print_result(result):
    print('Got:', result)

def add(x, y):
    return x + y

apply_async(add, (2, 3), callback=print_result)
apply_async(add, ('hello', 'world'), callback=print_result)
```

print result() 函数仅仅只接受一个参数 result 。不能再传入其他信息,而当你想让回调函数访问其他变量或者特定环境的变量值的时候就会遇到麻烦,为了让回调函数访问外部信息，一种方法是使用一个绑定方法来代替一个简单函数。

```python
class ResultHandler:
    def __init__(self):
        self.sequence = 0

    def handler(self, result):
        self.sequence += 1
        print('[{}] Got: {}'.format(self.sequence, result))
```

使用这个类的时候，你先创建一个类的实例，然后用它的 handler() 绑定方法来

```python
class ResultHandler:
    def __init__(self):
        self.sequence = 0

    def handler(self, result):
        self.sequence += 1
        print('[{}] Got: {}'.format(self.sequence, result))
```

使用闭包方式

```python

def resultHandler():
    sequence = 0
    def handler( result):
        # nonlocal 关键字用于在嵌套函数内部使用变量，其中变量不应属于内部函数。
        nonlocal sequence
        sequence += 1
        print('[{}] Got: {}'.format(sequence, result))

    return handler

r = resultHandler();
apply_async(add, (2, 3), callback=r)
apply_async(add, ('hello', 'world'), callback=r)
```

使用协程来完成同样的事情：

```python
def make_handler():
    sequence = 0
    while True:
        result = yield
        sequence += 1
        print('[{}] Got: {}'.format(sequence, result))
```

对于协程，你需要使用它的 send() 方法作为回调函数

```python
>>> handler = make_handler()
>>> next(handler) # Advance to the yield
>>> apply_async(add, (2, 3), callback=handler.send)
[1] Got: 5
>>> apply_async(add, ('hello', 'world'), callback=handler.send)
[2] Got: helloworld
>>>
```

nonlocal声明语句用来指示接下来的变量会在回调函数中被修改。如果没有这个声明，代码会报错。

至少有两种主要方式来捕获和保存状态信息，你可以在一个对象实例(通过一个绑定方法)或者在一个闭包中保存它。两种方式相比，闭包或许是更加轻量级和自然一点，因为它们可以很简单的通过函数来构造。它们还能自动捕获所有被使用到的变量。

### 7.11内联回调函数

当你编写使用回调函数的代码的时候，担心很多小函数的扩张可能会弄乱程序控制流。你希望找到某个方法来让代码看上去更像是一个普通的执行序列。

通过使用生成器和协程可以使得回调函数内联在某个函数中。为了演示说明，假设你有如下所示的一个执行某种计算任务然后调用一个回调函数的函数 (参考 7.10 小节)：

```python
def apply_async(func, args, *, callback):
    # Compute the result
    result = func(*args)
    # Invoke the callback with the result
    callback(result)
```

它包含了一个`Async`类和一个`inlined async`装饰器：

```python
from queue import Queue
from functools import wraps
class Async:
    def __init__(self, func, args):
        self.func = func
        self.args = args

    def inlined_async(func):
        @wraps(func)
        def wrapper(*args):
            f = func(*args)
            result_queue = Queue()
            result_queue.put(None)
            while True:
                result = result_queue.get()
                try:
                    a = f.send(result)
                    apply_async(a.func, a.args, callback=result_queue.put)
                except StopIteration:
                    break

        return wrapper



def add(x, y):
    return x + y


@inlined_async
def test():
    r = yield Async(add, (2, 3))
    print(r)
    r = yield Async(add, ('hello', 'world'))
    print(r)
    for n in range(10):
        r = yield Async(add, (n, n))
        print(r)
    print('Goodbye')

test()
```

首先，在需要使用到回调的代码中，关键点在于当前计算工作会挂起并在将来的某个时候重启(比如异步执行)。当计算重启时，回调函数被调用来继续处理结果。
apply-async()函数演示了执行回调的实际逻辑，尽管实际情况中它可能会更加复杂(包括线程、进程、事件处理器等等)。
计算的暂停与重启思路跟生成器函数的执行模型不谋而合。具体来讲，yield操作会使一个生成器函数产生一个值并暂停。接下来调用生成器的next()或send()方法又会让它从暂停处继续执行。

根据这个思路，这一小节的核心就在inlineasync()装饰器函数中了。关键点就是，装饰器会逐步遍历生成器函数的所有yield语句，每一次一个。为了这样做，刚开始的时候创建了一个result 队列并向里面放入一个None值。然后开始一个循环操作，从队列中取出结果值并发送给生成器，它会持续到下一个yield 语句，在这里一个Async的实例被接受到。然后循环开始检查函数和参数，并开始进行异步计算apply-async()。然而，这个计算有个最诡异部分是它并没有使用一个普通的回调函数，而是用队列的put()方法来回调。
这时候，是时候详细解释下到底发生了什么了。主循环立即返回顶部并在队列上执行get()操作。如果数据存在，它一定是put()回调存放的结果。如果没有数据，那么先暂停操作并等待结果的到来。这个具体怎样实现是由apply-async()函数来决定的。如果你不相信会有这么神奇的事情，你可以使用multiprocessing库来试一下，在单独的进程中执行异步计算操作，如下所示：

### 7.12访问闭包中定义的变量

你想要扩展函数中的某个闭包，允许它能访问和修改函数的内部变量。

通常来讲，闭包的内部变量对于外界来讲是完全隐藏的。但是，你可以通过编写访问函数并将其作为函数属性绑定到闭包上来实现这个目的

```python
def sample():
    n  = 0
    # Closure function
    def func():
        print('n=', n)
    # Accessor methods for n
    def get_n():
        return n

    def set_n(value):
        nonlocal n
        n = value
    # Attach as function attributes
    func.get_n = get_n
    func.set_n = set_n
    return func
f = sample()
f()
f.set_n(10)
f()
print(f.get_n())

```

nonlocal声明可以让我们编写函数来修改内部变量的值，函数属性允许我们用一种很简单的方式将访问方法绑定到闭包函数上，这个跟实例方法很像(尽管并没有定义任何类)。

还可以进一步的扩展，让闭包模拟类的实例。你要做的仅仅是复制上面的内部函数到一个字典实例中并返回它即可。例如：

```python
import sys

class ClosureInstance:
    def __init__(self, locals=None):
        if locals is None:
            #
            locals = sys._getframe(1).f_locals

        # Update instance dictionary with callables
        self.__dict__.update((key,value) for key, value in locals.items() if callable(value) )

        # Redirect special methods
        def __len__(self):
            return self.__dict__['__len__']()

# Example use
def Stack():
    items = []

    def push(item):
        items.append(item)

    def pop():
        return items.pop()

    def __len__():
        return len(items)

    return ClosureInstance()

s = Stack()
print(s)
s.push(10)
s.push(20)
s.push('Hello')
len(s)
s.pop()
s.pop()

```

结果显示，闭包的方案运行起来要快大概 8%，大部分原因是因为对实例变量的简化访问，闭包更快是因为不会涉及到额外的 self 变量。

总体上讲，在配置的时候给闭包添加方法会有更多的实用功能，比如你需要重置内部状态、刷新缓冲区、清除缓存或其他的反馈机制的时候。

## 第十章：模块与包

模块与包是任何大型程序的核心，就连 Python 安装程序本身也是一个包。本章重点涉及有关模块和包的常用编程技术，例如如何组织包、把大型模块分割成多个文件、创建命名空间包。同时，也给出了让你自定义导入语句的秘籍


### 构建一个模块的层级包



### 控制模块被全部导入的内容

### 使用相对路径名导入包中子模块

### 将模块分割成多个文件

### 利用命名空间导入目录分散的代码

### 运行目录或压缩文件

### 读取位于包中的数据文件

### 将文件夹加入到sys.path

### 通过字符串名导入模块

### 通过钩子远程加载模块

### 导入模块的同时修改模块

### 安装私有的包

### 创建新的Python环境

### 分发包

## 第十一章：网络与Web编程

本章是关于在`网络应用`和`分布式应用`中使用的`各种主题`。主题划分为使用`Python 编写客户端`程序来访问已有的服务，以及使用 `Python 实现网络服务端程序`。也给出了一些常见的技术，用于编写涉及协同或通信的的代码。

### 作为客户端与HTTP服务交互

**你需要通过 HTTP 协议以客户端的方式访问多种服务。例如，下载数据或者与基于 REST 的 API 进行交互。**

对于简单的事情来说，通常使用 urllib.request 模块就够了.一个Get请求的Demo

```py
┌──[root@liruilongs.github.io]-[~]
└─$python3
Python 3.6.8 (default, Nov 16 2020, 16:55:22)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-44)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from urllib import request, parse
>>> url = 'http://httpbin.org/get'
>>> parms = {
...     'name1': 'value1',
...     'name2': 'value2'
... }
>>> querystring = parse.urlencode(parms)
>>> querystring
'name1=value1&name2=value2'
>>> request.urlopen(url+'?' + querystring)
<http.client.HTTPResponse object at 0x7ffa0ef0f710>
>>> u = request.urlopen(url+'?' + querystring)
>>> u.read()
b'{
    "args": {
        "name1": "value1",
        "name2": "value2"
    },
    "headers": {
        "Accept-Encoding": "identity",
        "Host": "httpbin.org",
        "User-Agent": "Python-urllib/3.6",
        "X-Amzn-Trace-Id": "Root=1-62707b15-41a1169c0897c9001a07f948"
    },
    "origin": "39.154.13.139",
    "url": "http://httpbin.org/get?name1=value1&name2=value2"
}'
```

如果你需要使用 POST 方法在请求主体中发送查询参数，可以将参数编码后作为可选参数提供给 urlopen() 函数，就像这样：

```py
>>> from urllib import request, parse
>>> url = 'http://httpbin.org/post'
>>> parms = {
... 'name1' : 'value1',
... 'name2' : 'value2'
... }
>>> querystring = parse.urlencode(parms)
>>> querystring.encode('ascii')
b'name1=value1&name2=value2'
>>> u = request.urlopen(url, querystring.encode('ascii'))
>>> resp = u.read()
>>> resp
b'{
    "args": {},
    "data": "",
    "files": {},
    "form": {
        "name1": "value1",
        "name2": "value2"
    },
    "headers": {
        "Accept-Encoding": "identity",
        "Content-Length": "25",
        "Content-Type": "application/x-www-form-urlencoded",
        "Host": "httpbin.org",
        "User-Agent": "Python-urllib/3.6",
        "X-Amzn-Trace-Id": "Root=1-62707d24-15e9944760d3bbaa36c3714a"
    },
    "json": null,
    "origin": "39.154.13.139",
    "url": "http://httpbin.org/post"
}'
>>>
```

在发出的请求中提供一些自定义的 HTTP 请求首部，创建一个`Request 实例`然后将其传给`urlopen()`

```py
>>> from urllib import request, parse
>>> headers = {
... 'User-agent' : 'none/ofyourbusiness',
... 'Spam' : 'Eggs'
... }
>>> req = request.Request(url, querystring.encode('ascii'), headers=headers)
>>> u = request.urlopen(req)
>>> u.read()
b'{
    "args": {},
    "data": "",
    "files": {},
    "form": {
        "name1": "value1",
        "name2": "value2"
    },
    "headers": {
        "Accept-Encoding": "identity",
        "Content-Length": "25",
        "Content-Type": "application/x-www-form-urlencoded",
        "Host": "httpbin.org",
        "Spam": "Eggs",
        "User-Agent": "none/ofyourbusiness",
        "X-Amzn-Trace-Id": "Root=1-62707f0e-308a8137555e15797d950018"
    },
    "json": null,
    "origin": "39.154.13.139",
    "url": "http://httpbin.org/post"
}'
>>>
```

如果需要交互的服务,可以使用`requests 模块`， 这个不是自带模块，需要安装`python3 -m pip install requests`

```py
>>> import requests
>>> url = 'http://httpbin.org/post'
>>> parms = {
... 'name1' : 'value1',
... 'name2' : 'value2'
... }
>>> headers = {
... 'User-agent' : 'none/ofyourbusiness',
... 'Spam' : 'Eggs'
... }
>>> resp = requests.post(url, data=parms, headers=headers)
>>> resp.text
'{\n  "args": {}, \n  "data": "", \n  "files": {}, \n  "form": {\n    "name1": "value1", \n    "name2": "value2"\n  }, \n  "headers": {\n    "Accept": "*/*", \n    "Accept-Encoding": "gzip, deflate", \n    "Content-Length": "25", \n    "Content-Type": "application/x-www-form-urlencoded", \n    "Host": "httpbin.org", \n    "Spam": "Eggs", \n    "User-Agent": "none/ofyourbusiness", \n    "X-Amzn-Trace-Id": "Root=1-62708080-7a14319e699baa2e35a352fb"\n  }, \n  "json": null, \n  "origin": "39.154.13.139", \n  "url": "http://httpbin.org/post"\n}\n'
>>> resp.json()
{'args': {}, 'data': '', 'files': {}, 'form': {'name1': 'value1', 'name2': 'value2'}, 'headers': {'Accept': '*/*', 'Accept-Encoding': 'gzip, deflate', 'Content-Length': '25', 'Content-Type': 'application/x-www-form-urlencoded', 'Host': 'httpbin.org', 'Spam': 'Eggs', 'User-Agent': 'none/ofyourbusiness', 'X-Amzn-Trace-Id': 'Root=1-62708080-7a14319e699baa2e35a352fb'}, 'json': None, 'origin': '39.154.13.139', 'url': 'http://httpbin.org/post'}
>>>
```

requests 模块支持很多种数据的返会方式，可以直接返回以`Unicode 解码的响应文本`,也可以返回JSON数据

利用 requests 库发起一个 HEAD 请求

```py
>>> import requests
>>> resp = requests.head( 'http://httpbin.org/post')
>>> resp
<Response [405]>
>>> resp = requests.head( 'http://httpbin.org/')
>>> resp
<Response [200]>
>>> resp.status_code
200
>>> resp.headers['content-length']
'9593'
>>> resp.headers['content-type']
'text/html; charset=utf-8'
>>> resp.text
''
>>>
```

如果你决定坚持使用标准的程序库而不考虑像`requests`这样的第三方库，可以使用底层的`http.client 模块`来实现自己的代码。

```py
from http.client import HTTPConnection
from urllib import parse


c = HTTPConnection('www.python.org', 80)
c.request('HEAD', '/index.html')
resp = c.getresponse()
print('Status', resp.status)
for name, value in resp.getheaders():
    print(name, value)
```

测试 HTTP 客户端,考虑使用` httpbin `服务(<http://httpbin.org)。这个站点会接收发出的请求，然后以JSON> 的形式将相应信息回传回来。

```py
>>> import requests
>>> r = requests.get('http://httpbin.org/get?name=Dave&n=37',
... headers = { 'User-agent': 'goaway/1.0' })
>>> resp = r.json()
>>> resp['headers']
{'Accept': '*/*', 'Accept-Encoding': 'gzip, deflate', 'Host': 'httpbin.org', 'User-Agent': 'goaway/1.0', 'X-Amzn-Trace-Id': 'Root=1-62708c06-7c7d8cc4441479c65faea5b4'}
>>>
```

### 创建TCP服务器

**你想实现一个服务器，通过 TCP 协议和客户端通信。**

创建一个 TCP 服务器的一个简单方法是使用 socketserver 库。下面是一个简单的TCP服务器

```py
from socketserver import BaseRequestHandler, TCPServer


class EchoHandler(BaseRequestHandler):
    def handle(self):
        print('Got connection from', self.client_address)
        while True:
            #接收客户端发送的数据, 这次接收数据的最大字节数是8192
            msg = self.request.recv(8192)
            # 接收的到数据在发送回去
            if not msg:
                break
            self.request.send(msg)

if __name__ == '__main__':
    # 20000端口，默认IP为本地IP，监听到消息交个EchoHandler处理器
    serv = TCPServer(('', 20000), EchoHandler)
    serv.serve_forever()
```

Got connection from ('127.0.0.1', 1675)

建立好服务端之后我们看下客户端，AF_INET：表示ipv4,SOCK_STREAM: tcp传输协议

```py
>>> from socket import socket, AF_INET, SOCK_STREAM
>>> s = socket(AF_INET, SOCK_STREAM)
>>> s.connect(('localhost', 20000))
>>> s.send(b'Hello')
5
>>> s.recv(8192)
b'Hello'
>>>
```

使用`StreamRequestHandler 基类`将一个类文件接口放置在底层 socket 上的例子,StreamRequestHandler类支持像操作文件对象那样操作输入套字节;嗯，这个Demo还有问题，时间关系，之前在研究下

```py
from socketserver import StreamRequestHandler, TCPServer


class EchoHandler(StreamRequestHandler):
    def handle(self):
        print('Got connection from', self.client_address)
        #   rfile: 这个文件对应着从 socket 进来的数据
        for line in self.rfile:
            #  wfile: 这个文件对应着从 socket 发送的数据
            self.wfile.write(line)
   

if __name__ == '__main__':
    serv = TCPServer(('', 20000), EchoHandler)
    serv.serve_forever()
```

同时 StreamRequestHandler 支持通过类变量来设置连接参数

```py

import socket 
from socketserver import StreamRequestHandler

class EchoHandler(StreamRequestHandler):
    # Optional settings (defaults shown)
    timeout = 5  # Timeout on all socket operations
    rbufsize = -1  # Read buffer size
    wbufsize = 0  # Write buffer size
    disable_nagle_algorithm = False  # Sets TCP_NODELAY socket option


def handle(self):
    print('Got connection from', self.client_address)
    try:
        for line in self.rfile:
            # self.wfile is a file-like object for writing
            self.wfile.write(line)
    except socket.timeout:
        print('Timed out!')
```

socketserver 默认情况下这种服务器是单线程的，一次只能为一个客户端连接服务。如果你想处理多个客户端，可以初始化一个`ForkingTCPServer`或者是`ThreadingTCPServer`对象。

```py
from socketserver import ThreadingTCPServer

if __name__ == '__main__':
    serv = ThreadingTCPServer(('', 20000), EchoHandler)
    serv.serve_forever()
```

`使用 fork 或线程服务器`有个潜在问题就是它们会为每个客户端连接创建一个新的进程或线程。由于客户端连接数是没有限制的，因此一个`恶意的黑客可以同时发送大量的连接让你的服务器奔溃`。

可以创建一个预先分配大小的 `工作线程池或进程池`
先创建一个普通的非线程服务器，然后在一个线程池中使用` serve forever() `方法来启动它们。

```py
if __name__ == '__main__':
    from threading import Thread
    NWORKERS = 16
    serv = TCPServer(('', 20000), EchoHandler)
    for n in range(NWORKERS):
        t = Thread(target=serv.serve_forever)
        t.daemon = True
        t.start()
    serv.serve_forever()
```

一般来讲，一个TCPServer在实例化的时候会绑定并激活相应的socket，有时候你想通过设置某些选项去调整底下的socket，可以设置参数`bind_and_activate=False`。

```py
if __name__ == '__main__':
serv = TCPServer(('', 20000), EchoHandler, bind_and_activate=False)
# Set up various socket options
serv.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, True)
# Bind and activate
serv.server_bind()
serv.server_activate()
serv.serve_forever()
```

`socket.SO_REUSEADDR`允许服务器重新绑定一个之前使用过的端口号。由于要被经常使用到，它被放置到类变量中，可以直接在`TCPServer`上面设置

也可以不使用一个工具类，直接使用原生的Socket的编写TCP服务端

```py
from socket import socket, AF_INET, SOCK_STREAM


def echo_handler(address, client_sock):
    print('Got connection from {}'.format(address))
    while True:
        msg = client_sock.recv(8192)
        if not msg:
            break
        client_sock.sendall(msg)
    client_sock.close()

def echo_server(address, backlog=5):
    sock = socket(AF_INET, SOCK_STREAM)
    sock.bind(address)
    sock.listen(backlog)
while True:
    client_sock, client_addr = sock.accept()
    echo_handler(client_addr, client_sock)

if __name__ == '__main__':
    echo_server(('', 20000))
```

### 创建UDP服务器

**你想实现一个基于 UDP 协议的服务器来与客户端通信。**

跟 TCP 一样，UDP 服务器也可以通过使用` socketserver `库很容易的被创建。例如，下面是一个简单的时间服务器：

```py
from socketserver import BaseRequestHandler, UDPServer
import time
class TimeHandler(BaseRequestHandler):
    def handle(self):
        print('Got connection from', self.client_address)
        # Get message and client socket request 属性是一个包含了数据报和底层 socket 对象的元组
        msg, sock = self.request
        resp = time.ctime()
        sock.sendto(resp.encode('ascii'), self.client_address)

if __name__ == '__main__':
    serv = UDPServer(('', 20000), TimeHandler)
    serv.serve_forever()
```

测试一下

```py
>>> from socket import socket, AF_INET, SOCK_DGRAM
>>> s = socket(AF_INET, SOCK_DGRAM)
>>> s.sendto(b'', ('localhost', 20000))
0
>>> s.recvfrom(8192)
(b'Tue May  3 11:48:53 2022', ('127.0.0.1', 20000))
>>>
```

对于`UPD协议`而言，对于数据报的传送，你应该使用 socket 的`sendto() 和 recvfrom()`方法

### 通过CIDR地址生成对应的IP地址集

**你有一个 CIDR 网络地址比如“123.45.67.89/27”，你想将其转换成它所代表的所有 IP (比如，“123.45.67.64”, “123.45.67.65”, …, “123.45.67.95”))**

意思获取当前掩码指定网段内的所有可用IP，可以使用 ipaddress 模块很容易的实现这样的计算

```py
>>> net = ipaddress.ip_network('192.168.26.0/24')
>>> for a in net:
...     print(a)
...
192.168.26.0
192.168.26.1
192.168.26.2
192.168.26.3
........
192.168.26.254
192.168.26.255
>>>
```

顺便回顾一下掩码的知识，掩码为27位

```py
>>> net = ipaddress.ip_network('192.168.26.0/27')
>>> for a in net:
...     print(a)
...
192.168.26.0
192.168.26.1
192.168.26.2
....
192.168.26.29
192.168.26.30
192.168.26.31
```

ipv6 同样也可以

```py
>>> net6 = ipaddress.ip_network('12:3456:78:90ab:cd:ef01:23:30/125')
>>> for a in net6:
...     print(a)
...
12:3456:78:90ab:cd:ef01:23:30
12:3456:78:90ab:cd:ef01:23:31
12:3456:78:90ab:cd:ef01:23:32
12:3456:78:90ab:cd:ef01:23:33
12:3456:78:90ab:cd:ef01:23:34
12:3456:78:90ab:cd:ef01:23:35
12:3456:78:90ab:cd:ef01:23:36
12:3456:78:90ab:cd:ef01:23:37
>>>
```

Network 也允许像数组一样的索引取值,还可以执行网络成员检查之类的操作,一个 IP 地址和网络地址能通过一个 IP 接口来指定

```py
>>> net6.num_addresses
8
>>> net6[3]
IPv6Address('12:3456:78:90ab:cd:ef01:23:33')
>>> a in net6
True
>>> a
IPv6Address('12:3456:78:90ab:cd:ef01:23:37')
>>> inet = ipaddress.ip_interface('123.45.67.73/27')
>>> inet.network
IPv4Network('123.45.67.64/27')
>>> inet.ip
IPv4Address('123.45.67.73')
>>>
```

### 创建一个简单的REST接口

使用一个简单的`REST 接口`通过网络远程控制或访问你的应用程序，但是你又不想自己去安装一个`完整的 web 框架。`

构建一个 REST 风格的接口最简单的方法是创建一个`基于 WSGI 标准(PEP 3333)`的很小的库

```py
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   app.py
@Time    :   2022/05/03 14:43:56
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib

import time
import cgi


def notfound_404(environ, start_response):
    start_response('404 Not Found', [('Content-type', 'text/plain')])
    return [b'Not Found']


class PathDispatcher:
    def __init__(self):
        self.pathmap = {}

    def __call__(self, environ, start_response):
        path = environ['PATH_INFO']
        params = cgi.FieldStorage(environ['wsgi.input'],
                                  environ=environ)
        method = environ['REQUEST_METHOD'].lower()
        environ['params'] = {key: params.getvalue(key) for key in params}
        handler = self.pathmap.get((method, path), notfound_404)
        return handler(environ, start_response)

    def register(self, method, path, function):
        self.pathmap[method.lower(), path] = function
        return function


_hello_resp = "wo jiao {name}"


def hello_world(environ, start_response):
    start_response('200 OK', [('Content-type', 'text/html')])
    params = environ['params']
    resp = _hello_resp.format(name=params.get('name'))
    yield resp.encode('utf-8')


_localtime_resp = "dang qian shjian {t}"

# 路由的回调
def localtime(environ, start_response):
    start_response('200 OK', [('Content-type', 'application/xml')])
    resp = _localtime_resp.format(t=time.localtime())
    yield resp.encode('utf-8')


if __name__ == '__main__':
    from wsgiref.simple_server import make_server
    # Create the dispatcher and register functions
    dispatcher = PathDispatcher()
    # 注册路由，对应的回调
    dispatcher.register('GET', '/hello', hello_world)
    dispatcher.register('GET', '/localtime', localtime)
    # Launch a basic server 监听8080端口，注入核心控制器
    httpd = make_server('', 8080, dispatcher)
    print('Serving on port 8080...')
    httpd.serve_forever()
```

测试一下

```bash
┌──[root@liruilongs.github.io]-[~]
└─$coproc (./app.py)
[2] 130447
┌──[root@liruilongs.github.io]-[~]
└─$cutl localhost:8080/
-bash: cutl: 未找到命令
┌──[root@liruilongs.github.io]-[~]
└─$curl  localhost:8080/
127.0.0.1 - - [03/May/2022 16:09:00] "GET / HTTP/1.1" 404 9
Not Found┌──[root@liruilongs.github.io]-[~]
└─$curl  localhost:8080/hello
127.0.0.1 - - [03/May/2022 16:09:12] "GET /hello HTTP/1.1" 200 12
wo jiao None┌──[root@liruilongs.github.io]-[~]
└─$curl  localhost:8080/hello?name=liruilong
127.0.0.1 - - [03/May/2022 16:09:47] "GET /hello?name=liruilong HTTP/1.1" 200 17
wo jiao liruilong┌──[root@liruilongs.github.io]-[~]
└─$jobs
[1]+  已停止               curl -X 'HEAD' -v 'https://www.python.org/index.html'
[2]-  运行中               coproc COPROC ( ./app.py ) &
```

实现一个简单的 REST 接口，你只需让你的程序代码满足 Python 的`WSGI标准`即可。WSGI 被标准库支持，同时也被绝大部分第三方 web 框架支持。

长期运行的程序可能会使用一个 REST API 来实现监控或诊断。大数据应用程序可以使用 REST 来构建一个数据查询或提取系统。REST 还能用来控制硬件设备比如机器人、传感器、工厂或灯泡。更重要的是，REST API 已经被大量客户端编程环境所支持

看到这里，感觉python 的WSGI标准和Java  Web 体系的Servlet规范特别接近，但是Servlet是侵入式的，而WSGI好像对代码的影响很少...感兴趣小伙伴可以研究下.

以一个可调用对象形式来实现路由匹配要操作的方法

```py
import cgi

def wsgi_app(environ, start_response):
    pass
```

`environ 属性是一个字典`，包含了从 web 服务器如 Apache[参考 Internet RFC 3875]提供的 CGI 接口中获取的值。要将这些不同的值提取出来，你可以像这么这样写：

```py
def wsgi_app(environ, start_response):
    method = environ['REQUEST_METHOD']
    path = environ['PATH_INFO']
    # Parse the query parameters
    params = cgi.FieldStorage(environ['wsgi.input'], environ=environ)
```

`start_response 参数`是一个为了初始化一个请求对象而必须被调用的函数。第一个参数是返回的 `HTTP 状态值`，第二个参数是一个`(名, 值) 元组列表`，用来构建返回的`HTTP 头`。

```py
def wsgi_app(environ, start_response):
    pass
    start_response('200 OK', [('Content-type', 'text/plain')])
```

为了返回数据，一个`WSGI 程序`必须返回一个`字节字符串序列`。可以像下面这样使用一个列表来完成

```py
def wsgi_app(environ, start_response):
    pass
    start_response('200 OK', [('Content-type', 'text/plain')])
    resp = []
    resp.append(b'Hello World\n')
    resp.append(b'Goodbye!\n')
    return resp
```

或者，你还可以`使用 yield`

```bash
def wsgi_app(environ, start_response):
    pass
    start_response('200 OK', [('Content-type', 'text/plain')])
    yield b'Hello World\n'
    yield b'Goodbye!\n'

```

最后返回的必须是字节字符串。如果返回结果包含文本字符串，必须先将其编码成字节。图片也是OK的

```py
class WSGIApplication:
    def __init__(self):
        ...
    def __call__(self, environ, start_response)
        ...
```

`PathDispatcher 类`。这个分发器仅仅只是管理一个字典，将 (方法, 路径) 对映射到处理器函数上面。当一个请求到来时，它的方法和路径被提取出来，然后被分发到对应的处理器上面去。

```py
    dispatcher = PathDispatcher()
    # 注册路由，对应的回调
    dispatcher.register('GET', '/hello', hello_world)
    dispatcher.register('GET', '/localtime', localtime)
```

任何查询变量会被解析后放到一个字典中，以`environ['params'] 形式存储`。后面这个步骤太常见，所以建议你在分发器里面完成，这样可以省掉很多重复代码。使用分发器的时候，你只需简单的创建一个实例，然后通过它注册各种 WSGI 形式的函数。编写这些函数应该超级简单了，只要你遵循`start_response() 函数`的编写规则，并且最后返回字节字符串
即可。

WSGI 本身是一个很小的标准。因此它并没有提供一些高级的特性比如认证、cookies、重定向等。这些你自己实现起来也不难。不过如果你想要更多的支持，可以考虑第三方库

### 通过XML-RPC实现简单的远程调用

**你想找到一个简单的方式去执行运行在远程机器上面的 Python 程序中的函数或方法。**

实现一个远程方法调用的最简单方式是`使用 XML-RPC`。下面我们演示一下一个实现了`键 -值存储功能`的简单服务器：

```py
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   app.py
@Time    :   2022/05/03 17:07:02
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib
from xmlrpc.server import SimpleXMLRPCServer

class KeyValueServer:
    _rpc_methods_ = ['get', 'set', 'delete', 'exists', 'keys']

    def __init__(self, address):
        self._data = {}
        self._serv = SimpleXMLRPCServer(address, allow_none=True)
        # 注册方法
        for name in self._rpc_methods_:
            self._serv.register_function(getattr(self, name))

    def get(self, name):
        return self._data[name]

    def set(self, name, value):
        self._data[name] = value

    def delete(self, name):
        del self._data[name]

    def exists(self, name):
        return name in self._data

    def keys(self):
        return list(self._data)

    def serve_forever(self):
        self._serv.serve_forever()

    # Example
if __name__ == '__main__':
    kvserv = KeyValueServer(('', 15001))
    kvserv.serve_forever()
```

```py
PS E:\docker> python
Python 3.9.0 (tags/v3.9.0:9cf6752, Oct  5 2020, 15:23:07) [MSC v.1927 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> from xmlrpc.client import ServerProxy
>>> s = ServerProxy('http://localhost:15000', allow_none=True)
KeyboardInterrupt
>>> from xmlrpc.client import ServerProxy
>>> s = ServerProxy('http://localhost:15001', allow_none=True)
>>> s.set('foo','bar')
>>> s.set('spam', [1, 2, 3])
>>> s.keys()
['foo', 'spam']
>>> s.get('foo')
'bar'
>>> s.get('spam')
[1, 2, 3]
>>> s.delete('spam')
>>> s.exists('spam')
False
>>>
```

`XML-RPC`可以让我们很容易的构造一个简单的远程调用服务。你所需要做的仅仅是创建一个`服务器实例`，通过它的方法`register function()`来注册函数，然后使用方法`serve forever()`启动它。在上面我们将这些步骤放在一起写到一个类中

这并不是必须的。我们还可以像下面这样创建一个服务器：

```py
from xmlrpc.server import SimpleXMLRPCServer

from xmlrpc.server import SimpleXMLRPCServer
def add(x,y):
    return x+y
serv = SimpleXMLRPCServer(('', 15000))
serv.register_function(add)
serv.serve_forever()
```

`XML-RPC`暴露出来的函数只能适用于部分数据类型，比如字符串、整形、列表和字典,你不应该将 XML-RPC 服务以公共 API 的方式暴露出来。

XML-RPC 的一个缺点是它的性能。SimpleXMLRPCServer 的实现是单线程的，所以它不适合于大型程序

由于 XML-RPC 将所有数据都序列化为 XML 格式，所以它会比其他的方式运行的慢一些。但是它也有优点，这种方式的编码可以被绝大部分其他编程语言支持。通过使用这种方式，其他语言的客户端程序都能访问你的服务。

### 在不同的Python解释器之间交互

**你在不同的机器上面运行着多个 Python 解释器实例，并希望能够在这些解释器之间通过消息来交换数据。**

通过使用`multiprocessing.connection 模块`可以很容易的实现解释器之间的通信。这里需要说明的是在window和linux上面测试不通过，但是Linux和Linux是测试通过的，可能是环境版本问题

服务短编码

```py
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   serve.py
@Time    :   2022/07/05 22:41:29
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   服务端
"""

# here put the import lib
from multiprocessing.connection import Listener
import traceback


"""
@Time    :   2022/07/05 22:41:36
@Author  :   Li Ruilong
@Version :   1.0
@Desc    :   获取消息在发送出去
             Args:
               conn
             Returns:
               void
"""
def echo_client(conn):
    try:
        while True:
            msg = conn.recv()
            print('接收的消息：',msg)
            conn.send(msg)
    except EOFError:
        print('Connection closed')


"""
@Time    :   2022/07/05 22:41:54
@Author  :   Li Ruilong
@Version :   1.0
@Desc    :   暴露端口，发布服务
             Args:
               address, authkey
             Returns:
               void
"""

def echo_server(address, authkey):
    serv = Listener(address, authkey=authkey)
    while True:
        try:
            client = serv.accept()
            echo_client(client)
        except Exception:
            traceback.print_exc()

echo_server(('192.168.26.55', 25000), authkey=b'peekaboo')
```

服务端接收消息并应答

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ python3 serve.py
接收的消息： hello
接收的消息： 42
接收的消息： [1, 2, 3, 4, 5]

```

客户端发送消息，并接收服务端反向消息

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ python3
Python 3.6.8 (default, Nov 16 2020, 16:55:22)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-44)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from multiprocessing.connection import Client
>>> c = Client(('192.168.26.55',25000),authkey=b'peekaboo')
>>> c.send('hello')
>>> c.recv()
'hello'
>>> c.send(42)
>>> c.recv()
42
>>> c.send([1, 2, 3, 4, 5])
>>> c.recv()
[1, 2, 3, 4, 5]
>>>
```

跟底层socket不同的是，每个消息会完整保存(每一个通过`send()`发送的对象能通过`recv()`来完整接受)。另外，所有对象会通过`pickle序列化`。因此，任何`兼容pickle的对象`都能在此连接上面被发送和接受。

目前有很多用来实现各种消息传输的包和函数库，比如ZeroMQ、Celery等。

如果你的解释器运行在同一台机器上面，那么你可以使用另外的通信机制，比如Unix 域套接字或者是Windows命名管道。

要想使用UNIX域套接字来创建一个连接，只需简单的将地址改写一个文件名即可：

```py
serv = Listener('/tmp/myconn', authkey=b'peekaboo')
```

要想使用 Windows 命名管道来创建连接，只需像下面这样使用一个文件名

```py
s = Listener(r'\\.\pipe\myconn', authkey=b'peekaboo')
```

一个通用准则是，你不要使用`multiprocessing`来实现一个对外的公共服务。
Client()和Listener()中的authkey 参数用来认证发起连接的终端用户。如果密钥不对会产生一个异常。此外，该模块最适合用来建立长连接(而不是大量的短连接)，例如，两个解释器之间启动后就开始建立连接并在处理某个问题过程中会一直保持连接状态。

如果你需要对底层连接做更多的控制，比如需要支持超时、非阻塞I/O或其他类似的特性，你最好使用另外的库或者是在高层 socket上来实现这些特性。

### 实现远程方法调用

**你想在一个消息传输层如 sockets、multiprocessing.connections或zeroMQ的基础之上实现一个简单的远程过程调用(RPC)。**

将`函数请求`、`参数`和`返回值`使用`pickle编码`后，在不同的解释器直接传送`pickle字节字符串`，可以很容易的实现RPC。下面是一个简单的`PRC处理器`，可以被整合到一个服务器中去：

 RPC 服务端

```py
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   rpcserver.py
@Time    :   2022/07/08 20:16:21
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   远程调用服务
"""

# here put the import lib
import  pickle
from multiprocessing.connection import Listener
from threading import Thread


"""
@Time    :   2022/07/08 20:28:02
@Author  :   Li Ruilong
@Version :   1.0
@Desc    :   None
             Args:
               远程调用处理器
             Returns:
               void
"""

class RPCHandler:

    def __init__(self):
        self._functions = {}

    """
    @Time    :   2022/07/08 20:16:47
    @Author  :   Li Ruilong
    @Version :   1.0
    @Desc    :   函数注册
                 Args:
                   func
                 Returns:
                   void
    """

    def register_function(self, func):
        self._functions[func.__name__] = func

    """
    @Time    :   2022/07/08 20:17:51
    @Author  :   Li Ruilong
    @Version :   1.0
    @Desc    :   调用函数
                 Args:
                   connection
                 Returns:
                   void
    """
    
    def handle_connection(self, connection):
        try:
            while True:
                func_name, args, kwargs = pickle.loads(connection.recv())
                try:
                    print("调用函数：",(func_name, args, kwargs))
                    r = self._functions[func_name](*args,**kwargs)
                    print("返回结果：",r)
                    connection.send(pickle.dumps(r))
                except Exception as e:
                    connection.send(pickle.dumps(e))
        except Exception  as e:
            pass


def rpc_server(handler, address, authkey):
    sock = Listener(address, authkey=authkey)
    while True:
        client = sock.accept()
        t = Thread(target=handler.handle_connection, args=(client,))
        t.daemon = True
        print("函数开始执行")
        t.start()


def add(x, y):
    return x + y


def sub(x, y):
    return x - y

if __name__ == '__main__':
    print(format("开始加载RPC处理器",'》<20'))
    handler = RPCHandler()
    print(format("处理器加载完成，注册函数",'》<20'))
    handler.register_function(add)
    handler.register_function(sub)
    print(format("函数注册成功,服务启动",'》<20'))
    rpc_server(handler, ('localhost', 17000), authkey=b'peekaboo')
```

RPC 客户端

```py
import pickle

from multiprocessing.connection import Client


class RPCProxy:
    def __init__(self, connection):
        self._connection = connection
    def __getattr__(self, name):
        print("开始调用函数",name)
        def do_rpc(*args, **kwargs):
            self._connection.send(pickle.dumps((name, args, kwargs)))
            result = pickle.loads(self._connection.recv())
            print("返回结果",result)
            if isinstance(result, Exception):
                raise result
            return result

        return do_rpc


c = Client(('localhost', 17000), authkey=b'peekaboo')
print(format("建立连接，创建RPC代理",'》<30'),c)
proxy = RPCProxy(c)
print(format("创建代理成功",'》<30'))
print("add(2, 3) = ",proxy.add(2, 3) )
print("sub(2, 3) = ", proxy.sub(2, 3))


```

```py
D:\python\Python310\python.exe D:/python/code/rabbit_mq_demo/rpcserver.py
开始加载RPC处理器》》》》》》》》》》
处理器加载完成，注册函数》》》》》》》》
函数注册成功,服务启动》》》》》》》》》
函数开始执行
调用函数： ('add', (2, 3), {})
返回结果： 5
调用函数： ('sub', (2, 3), {})
返回结果： -1


==============
D:\python\Python310\python.exe D:/python/code/rabbit_mq_demo/RPC.py
建立连接，创建RPC代理》》》》》》》》》》》》》》》》》》 <multiprocessing.connection.Connection object at 0x00DFACA0>
创建代理成功》》》》》》》》》》》》》》》》》》》》》》》》
开始调用函数 add
返回结果 5
add(2, 3) =  5
开始调用函数 sub
返回结果 -1
sub(2, 3) =  -1

Process finished with exit code 0
```

RPCHandler和RPCProxy的基本思路是很比较简单的。

如果一个客户端想要调用一个远程函数，比如foo(1，2，z=3)，代理类创建一个包含了函数名和参数的元组(foo'，(1，2)，{'z'：3})。这个元组被 pickle 序列化后通过网络连接发生出去。

。
由于底层需要依赖 pickle，那么安全问题就需要考虑了(因为一个聪明的黑客可以创建特定的消息，能够让任意函数通过 pickle反序列化后被执行)。因此你永远不要允许来自不信任或未认证的客户端的RPC。特别是你绝对不要允许来自Internet的任意机器的访问，这种只能在内部被使用，位于防火墙后面并且不要对外暴露。

作为pickle的替代，你也许可以考虑使用JSON、XML或一些其他的编码格式来序列化消息。例如，本机实例可以很容易的改写成JSON编码方案。还需要将pickle.1oads()和pickle.dumps()替换成json.1oads()和json.dumps()即可：

```py

# here put the import lib
import json
........
    def handle_connection(self, connection):
        try:
            while True:
                # 反序列化
                func_name, args, kwargs = json.loads(connection.recv())
                try:
                    print("调用函数：",(func_name, args, kwargs))
                    r = self._functions[func_name](*args,**kwargs)
                    print("返回结果：",r)
                    # 序列化发送
                    connection.send(json.dumps(r))
                except Exception as e:
                    connection.send(json.dumps(e))
        except Exception  as e:
            pass

......
```

```
import json

from multiprocessing.connection import Client


class RPCProxy:
    def __init__(self, connection):
        self._connection = connection
    def __getattr__(self, name):
        print("开始调用函数",name)
        def do_rpc(*args, **kwargs):
            print("JSON 序列化后的值",json.dumps((name, args, kwargs)))
            self._connection.send(json.dumps((name, args, kwargs)))
            result = json.loads(self._connection.recv())
            print("返回结果",result)
            if isinstance(result, Exception):
                raise result
            return result

        return do_rpc


c = Client(('localhost', 17000), authkey=b'peekaboo')
print(format("建立连接，创建RPC代理",'》<30'),c)
proxy = RPCProxy(c)
print(format("创建代理成功",'》<30'))
print("add(2, 3) = ",proxy.add(2, 3) )
print("sub(2, 3) = ", proxy.sub(2, 3))

```

可以看到序列化后的结果

```bash
D:\python\Python310\python.exe D:/python/code/rabbit_mq_demo/RPC.py
建立连接，创建RPC代理》》》》》》》》》》》》》》》》》》 <multiprocessing.connection.Connection object at 0x0078AD30>
创建代理成功》》》》》》》》》》》》》》》》》》》》》》》》
开始调用函数 add
JSON 序列化后的值 ["add", [2, 3], {}]
返回结果 5
add(2, 3) =  5
开始调用函数 sub
JSON 序列化后的值 ["sub", [2, 3], {}]
返回结果 -1
sub(2, 3) =  -1
```

实现RPC的一个比较复杂的问题是如何去处理异常。至少，当方法产生异常时服务器不应该奔溃。因此，返回给客户端的异常所代表的含义就要好好设计了。如果你使用pickle，异常对象实例在客户端能被反序列化并抛出。如果你使用其他的协议，那得想想另外的方法了。不过至少，你应该在响应中返回异常字符串。我们在JSON的例子中就是使用的这种方式。

对于其他的RPC实现例子，我推荐你看看在XML-RPC中使用的Simp1eXMLRPCServer 和ServerProxy的实现，也就是11.6小节中的内容。

### 简单的客户端认证

**你想在分布式系统中实现一个简单的客户端连接认证功能，又不想像SSL那样的复杂。**

可以利用hmac模块实现一个连接握手，从而实现一个简单而高效的认证过程。下面是代码示例：

基本原理是当连接建立后，服务器给客户端发送一个随机的字节消息(这里例子中使用了os.urandom()返回值)。客户端和服务器同时利用hmac和一个只有双方知道的密钥来计算出一个加密哈希值。然后客户端将它计算出的摘要发送给服务器，服务器通过比较这个值和自己计算的是否一致来决定接受或拒绝连接。摘要的比较需要使用hmac.compare-digest()函数。使用这个函数可以避免遭到时间分析攻击，不要用简单的比较操作符(==)。为了使用这些函数，你需要将它集成到已有的网络或消息代码中。例如，对于sockets，服务器代码应该类似下面：

```py
import hmac
import os

from socket import socket, AF_INET, SOCK_STREAM

def server_authenticate(connection, secret_key):
    message = os.urandom(32)
    connection.send(message)
    hash = hmac.new(secret_key, message)
    digest = hash.digest()
    response = connection.recv(len(digest))
    return hmac.compare_digest(digest, response)


secret_key = b'peekaboo'
def echo_handler(client_sock):
    if not server_authenticate(client_sock, secret_key):
        client_sock.close()
        return
    while True:
        msg = client_sock.recv(8192)
        if not msg:
            break
        client_sock.sendall(msg)

def echo_server(address):
    s = socket(AF_INET, SOCK_STREAM)
    s.bind(address)
    s.listen(5)
    while True:
        c,a = s.accept()
        echo_handler(c)


echo_server(('', 18000))
```

```py

from socket import socket, AF_INET, SOCK_STREAM
import hmac


def client_authenticate(connection, secret_key):
    '''
    Authenticate client to a remote service.
    connection represents a network connection.
    secret_key is a key known only to both client/server.
    '''
    message = connection.recv(32)
    hash = hmac.new(secret_key, message)
    digest = hash.digest()
    connection.send(digest)

secret_key = b'peekaboo'
s = socket(AF_INET, SOCK_STREAM)
s.connect(('localhost', 18000))
client_authenticate(s, secret_key)
s.send(b'Hello World')
resp = s.recv(1024)
```

### 在网络服务中加入SSL

### 进程间传递Socket文件描述符

### 理解事件驱动的IO

### 发送与接收大型数组

**你要通过网络连接发送和接受连续数据的大型数组，并尽量减少数据的复制操作。**

我们可以利用` memoryviews `来发送和接受大数组：

## 第十二章：并发编程

**你要为需要并发执行的代码创建/销毁线程**

`threading 库`可以在单独的线程中执行任何的在`Python`中可以调用的对象。创建一个`Thread 对象`并将你要执行的对象以 `target 参数`的形式提供给该对象。

当你创建好一个线程对象后，该对象并不会立即执行，除非你调用它的 `start()`方法

```py
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   demo.py
@Time    :   2022/05/02 22:45:33
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib

from threading import Thread
import time


def countdown(n):
    while n > 0:
        print('T-minus', n)
        n -= 1
        time.sleep(5)


# Create and launch a thread
t = Thread(target=countdown, args=(10,))
t.start()
```

Python 中的线程会在一个`单独的系统级线程`中执行(比如说一个POSIX 线程或者一个 Windows 线程)，这些线程将由操作系统来全权管理。线程一旦启动，将独立执行直到目标函数返回。你可以查询一个线程对象的状态，看它是否还在执行：

```py
if t.is_alive():
    print('Still running')
else:
    print('Completed')
```

你也可以将一个线程加入到当前线程，并等待它终止

```py
t.join()
```

`Python 解释器`在所有线程都终止后才继续执行代码剩余的部分。对于需要长时间运行的线程或者需要一直运行的后台任务，你应当考虑使用后台线程。

即对于python来讲，

### 启动与停止线程

### 判断线程是否已经启动

### 线程间通信

### 防止死锁的加锁机制

### 保存线程的状态信息

### 创建一个线程池

### 简单的并行编程

### Python的全局锁问题

### 定义一个Actor任务

### 实现消息发布/订阅模型

### 使用生成器代替线程

### 多个线程队列轮询

### 在Unix系统上面启动守护进程

## 第十三章：脚本编程与系统管理

### 通过重定向/管道/文件接受输入

在bash中编写pytohn脚本接收外部数据的方式，一般情况下，对于一般变量，我们用命令行变量的方式比较多(手动的处理 `sys.argv`)，对于`文件内容或者bash命令输出`直接通过脚本内部获取需要的数据。

其实python 脚本也可以用其他方式来接收 传递给他的`文件数据或者bash命令输出`，包括将`命令行的输出`通过`管道传递`给该脚本、`重定向文件到该脚本`，或在`命令行中传递一个文件名`或`文件名列表`给该脚本。

这里通过`Python 内置的 fileinput 模块`，可以实现重`定向，管道，文佳输出`的方式传递数据到脚本内部

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   filein.py
@Time    :   2022/05/01 06:05:43
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   None
"""

# here put the import lib

import fileinput
with fileinput.input() as f_input:
    for line in f_input:
        print("脚本输出", line, end='')

```

使用`fileinput.input()`方法可以获取当前输入脚本的数据，脚本里面用一个` FileInput `迭代器接收

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$vim filein.py
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$chmod +x filein.py
```

文件直接接收

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./filein.py /etc/passwd
脚本输出 root:x:0:0:root:/root:/bin/bash
脚本输出 bin:x:1:1:bin:/bin:/sbin/nologin
脚本输出 daemon:x:2:2:daemon:/sbin:/sbin/nol
。。。。
```

重定向接收

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./filein.py <  /etc/passwd
脚本输出 root:x:0:0:root:/root:/bin/bash
脚本输出 bin:x:1:1:bin:/bin:/sbin/nologin
。。。。。。
```

管道方式接收

```pash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$df -h
文件系统        容量  已用  可用 已用% 挂载点
/dev/sda1       150G   22G  129G   15% /
devtmpfs        983M     0  983M    0% /dev
tmpfs           993M     0  993M    0% /dev/shm
tmpfs           993M   17M  976M    2% /run
tmpfs           993M     0  993M    0% /sys/fs/cgroup
overlay         150G   22G  129G   15% /var/lib/docker/overlay2/9fbd33d3485f02eadef6907a5b4eaead4a384684b66c572d822a2942a82ca0d5/merged
overlay         150G   22G  129G   15% /var/lib/docker/overlay2/85ff22ccaf2db68a0a863bc404d79d72fa6c8744424f50ba8fb6bfa83d56b56a/merged
tmpfs           199M     0  199M    0% /run/user/0
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$df -h |  ./filein.py
脚本输出 文件系统        容量  已用  可用 已用% 挂载点
脚本输出 /dev/sda1       150G   22G  129G   15% /
脚本输出 devtmpfs        983M     0  983M    0% /dev
脚本输出 tmpfs           993M     0  993M    0% /dev/shm
脚本输出 tmpfs           993M   17M  976M    2% /run
脚本输出 tmpfs           993M     0  993M    0% /sys/fs/cgroup
脚本输出 overlay         150G   22G  129G   15% /var/lib/docker/overlay2/9fbd33d3485f02eadef6907a5b4eaead4a384684b66c572d822a2942a82ca0d5/merged
脚本输出 overlay         150G   22G  129G   15% /var/lib/docker/overlay2/85ff22ccaf2db68a0a863bc404d79d72fa6c8744424f50ba8fb6bfa83d56b56a/merged
脚本输出 tmpfs           199M     0  199M    0% /run/user/0
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

`fileinput.input()`创建并返回一个`FileInput`类的实例，该实例可以被当做一个`上下文管理器`使用。因此，整合起来，如果我们要写一个打印多个文件输出的脚本，那么我们需要在输出中包含文件名和行号

```py
>>> import fileinput
>>> with fileinput.input("/etc/passwd") as f:
...     for line in f:
...             print(f.filename(),f.fileno(),f.lineno(),line,end='')
...
/etc/passwd 3 1 root:x:0:0:root:/root:/bin/bash
/etc/passwd 3 2 bin:x:1:1:bin:/bin:/sbin/nologin
/etc/passwd 3 3 daemon:x:2:2:daemon:/sbin:/sbin/nologin
/etc/passwd 3 4 adm:x:3:4:adm:/var/adm:/sbin/nologin
/etc/passwd 3 5 lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
/etc/passwd 3 6 sync:x:5:0:sync:/sbin:/bin/sync
```

### 终止程序并给出错误信息

**你想向标准错误打印一条消息并返回某个非零状态码来终止程序运行**

通过 `python`的`raise  SystemExit(3)`命令可以主动抛出一个错误,通过`sys.stderr.write`将命令写到标准的输出端

```py
#!/usr/bin/env python3

import sys
sys.stderr.write('It failed!\n')
raise  SystemExit(3)
```

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$vim err.py
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./err.py
It failed!
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$echo $?
3
```

直接将消息作为参数传给` SystemExit() `，那么你可以省略其他步骤

```py
#!/usr/bin/env python3

raise SystemExit('It failed!')
```

抛出一个 `SystemExit` 异常，使用错误消息作为参数,它会将消息在` sys.stderr `中打印，然后程序以状态码` 1 `退出

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./err.py
It failed!
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$echo $?
1
```

### 解析命令行选项

**如何能够解析命令行选项(位于 sys.argv 中)**

`argparse`模块可被用来解析命令行选项

定义一个脚本的说明文档，一般我们写python脚本会通过`if..else`的方式来提供一个脚本文档，python不支持switch。所有很麻烦，其实，我们可以通过`argpar`来编写说明文档。

我们来看看执行一个python脚本

对于熟悉Linux的小伙伴下面的文档在熟悉不过了，这个一个标准Linxu软件包的说明文档，文档中定义是软件包的说明

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./demo.py -h
usage: demo.py [-h] -p pattern [-v] [-o OUTFILE] [--speed {slow,fast}]
               [filename [filename ...]]

Search some files

positional arguments:
  filename

optional arguments:
  -h, --help            show this help message and exit
  -p pattern, --pat pattern
                        text pattern to search for
  -v                    verbose mode
  -o OUTFILE            output file
  --speed {slow,fast}   search speed
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

来看看这个脚本是如何编写的

```py
#!/usr/bin/env python3


import argparse
# 脚本的描述
parser = argparse.ArgumentParser(description='Search some files')
# 脚本接收的全部参数,用`filenames`接收
parser.add_argument(dest='filenames', metavar='filename', nargs='*')

# 脚本接收
parser.add_argument('-p', '--pat', metavar='pattern', required=True,
                    dest='patterns', action='append',
                    help='text pattern to search for')
parser.add_argument('-v', dest='verbose', action='store_true',
                    help='verbose mode')
parser.add_argument('-o', dest='outfile', action='store',
                    help='output file')
parser.add_argument('--speed', dest='speed', action='store',
                    choices={'slow', 'fast'}, default='slow',
                    help='search speed')
args = parser.parse_args()
# Output the collected arguments
print(args.filenames)
print(args.patterns)
print(args.verbose)
print(args.outfile)
print(args.speed)

```

为了解析命令行选项， 首先要创建一个`ArgumentParser`实例， 并使用`add_argument()`方法声明你想要支持的选项。在每个`add-argument()`调用中:

`dest`参数指定解析结果被指派给属性的名字。 `metavar` 参数被用来生成帮助信息。

`action 参数`指定跟属性对应的处理逻辑，通常的`值为 store` , 被用来存储`某个值`或将`多个参数值收集到一个列表中`。

`nargs 参数收集`所有剩余的命令行参数到一个列表中。在本例中它被用来构造一个文件名列表

```py
parser.add_argument(dest='filenames',metavar='filename', nargs='*')
```

```bsh
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py  -p spam --pat=eggs  foo.txt bar.txt
['foo.txt', 'bar.txt']
['spam', 'eggs']
False
None
slow
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

`action='store_true'` 根据参数是否存在来设置一个 Boolean 标志：

```py
parser.add_argument('-v', dest='verbose', action='store_true', help='verbose mode')
```

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py -v -p spam --pat=eggs foo.txt bar.txt
['foo.txt', 'bar.txt']
['spam', 'eggs']
True
None
slow
```

`action='store'` 参数接受一个单独值并将其存储为一个字符串

```py
parser.add_argument('-o', dest='outfile', action='store', help='output file')
```

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py -v -p spam --pat=eggs -o liruilong  foo.txt bar.txt
['foo.txt', 'bar.txt']
['spam', 'eggs']
True
liruilong
slow
```

+ `action='append'` 参数说明`允许某个参数重复出现多次`，并将它们追加到一个`列表`中去。
+ `required 标志`表示`该参数至少要有一个`。`-p`和 `--pat`表示`两个参数名形式`都可使用。

```py
parser.add_argument('-p', '--pat', metavar='pattern', required=True,
                    dest='patterns', action='append',
                    help='text pattern to search for')
```

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py  -p spam   foo.txt bar.txt
['foo.txt', 'bar.txt']
['spam']
False
None
slow
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py  --pat=eggs  foo.txt bar.txt
['foo.txt', 'bar.txt']
['eggs']
False
None
slow
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py  -p spam --pat=eggs  foo.txt bar.txt
['foo.txt', 'bar.txt']
['spam', 'eggs']
False
None
slow
```

如果一个都没有，会提示缺少参数`-p/--pat`

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py   foo.txt bar.txt
usage: demo.py [-h] -p pattern [-v] [-o OUTFILE] [--speed {fast,slow}]
               [filename [filename ...]]
demo.py: error: the following arguments are required: -p/--pat
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

`choices={'slow', 'fast'},` 参数说明接受一个值，但是会将其和可能的选择值做比较，以检测其合法性：

```py
parser.add_argument('--speed', dest='speed', action='store',
                    choices={'slow', 'fast'}, default='slow',
                    help='search speed')
```

```bash

```

一旦参数选项被指定，你就可以执行` parser.parse() `方法了。它会处理` sys.argv `的值并返回一个结果实例。每个参数值会被设置成该实例中` add_argument() `方法的 `dest` 参数指定的属性值。

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py  --pat=eggs --speed 123  foo.txt bar.txt
usage: demo.py [-h] -p pattern [-v] [-o OUTFILE] [--speed {slow,fast}]
               [filename [filename ...]]
demo.py: error: argument --speed: invalid choice: '123' (choose from 'slow', 'fast')
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$python3 demo.py  --pat=eggs --speed fast  foo.txt bar.txt
['foo.txt', 'bar.txt']
['eggs']
False
None
fast
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

还很多种其他方法解析命令行选项。可以会手动的处理 `sys.argv`或者使用`getopt 模块`。但是，如果你采用本节的方式，将会减少很多冗余代码，底层细节`argparse 模块`已经帮你处理了。你可能还会碰到使用`optparse`库解析选项的代码。尽管`optparse 和 argparse 很像`，但是后者更先进，因此在新的程序中你应该使用它。

### 运行时弹出密码输入提示

**你写了个脚本，运行时需要一个密码。此脚本是交互式的，因此不能将密码在脚本中硬编码，而是需要弹出一个密码输入提示，让用户自己输入。**

`Python 的 getpass 模块`正是你所需要的。你可以让你很轻松的弹出密码输入提示，并且不会在用户终端回显密码。

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import getpass


def svc_login(user, passwd):
    return user == passwd


user = getpass.getuser()
passwd = getpass.getpass()
if svc_login(user, passwd):
    print('Yay!')
else:
    print('Boo!')
```

代码中` getpass.getuser() `不会弹出用户名的输入提示。它会根据该`用户的 shell 环境`或者会依据`本地系统的密码库`(支持 pwd 模块的平台)来使用`当前用户的登录名`

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./pass.py
Password: #root
Yay!
```

### 获取终端的大小

**你需要知道当前终端的大小以便正确的格式化输出。**

使用`os.get terminal size() 函数`来做到这一点。

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import os
sz = os.get_terminal_size()
print(sz)
print(sz.columns)
print(sz.lines)
```

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./tim.py
os.terminal_size(columns=99, lines=30)
99
30
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./tim.py
os.terminal_size(columns=165, lines=30)
165
30
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

### 执行外部命令并获取它的输出

**你想执行一个外部命令并以 Python 字符串的形式获取执行结果。**

使用` subprocess.check_output() `函数。

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import subprocess
out_bytes = subprocess.check_output(['netstat','-a'])
out_text = out_bytes.decode('utf-8')
print(out_text)


```

执行下试试

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./py_sh.py
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State
tcp        0      0 localhost:2379          0.0.0.0:*               LISTEN
tcp        0      0 vms55.rhce.cc:2379      0.0.0.0:*               LISTEN
tcp        0      0 localhost:2380          0.0.0.0:*               LISTEN
tcp        0      0 vms55.rhce.cc:2380      0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:webcache        0.0.0.0:*               LISTEN
tcp        0      0 0.0.0.0:http            0.0.0.0:*               LISTEN
```

如果被执行的命令以非零码返回，就会抛出异常。下面的例子捕获到错误并获取返回码：

```py
try:
    out_bytes = subprocess.check_output(['cmd','arg1','arg2'])
except subprocess.CalledProcessError as e:
    out_bytes = e.output # Output generated before error
    code = e.returncode # Return code
```

默认情况下，`check_output()`仅仅返回输入到标准输出的值。如果你需要`同时收集标准输出和错误输出`，使用`stderr`参数：

```py
out_bytes = subprocess.check_output(['cmd','arg1','arg2'],stderr=subprocess.STDOUT)
```

如果你需要用一个超时机制来执行命令，使用 timeout 参数：

```py
try:
    out_bytes = subprocess.check_output(['cmd','arg1','arg2'], timeout=5)
except subprocess.TimeoutExpired as e:
    ....
```

通常来讲，命令的执行`不需要`使用到`底层 shell 环境(比如 sh、bash)`。一个字符串列表会被传递给一个`低级系统命令`，比如`os.execve()` 。

如果你想让`命令被一个shell 执行`，传递一个字符串参数，并设置参数 `shell=True` . 有时候你想要`Python`去执行一个复杂的`shell 命令`的时候这个就很有用了，比如管道流、I/O 重定向和其他特性。例如：

```py
out_bytes = subprocess.check_output('grep python | wc > out', shell=True)
```

是在 shell 中执行命令会存在一定的安全风险，特别是当参数来自于用户输入时。这时候可以使用`shlex.quote() 函数`来将参数正确的用双引用引起来。

使用`check_output() 函数`是执行`外部命令`并获取其`返回值`的最简单方式。但是，如果你需要对`子进程做更复杂的交互`，比如给它发送输入，你得采用另外一种方法。这时候可直接使用`subprocess.Popen`类。

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import subprocess
# Some text to send
text = b'''
hello world
this is a test
goodbye
'''
# Launch a command with pipes
p = subprocess.Popen(['wc'],
                     stdout=subprocess.PIPE,
                     stdin=subprocess.PIPE)
# Send the data and get the output
stdout, stderr = p.communicate(text)
# To interpret as text, decode
out = stdout.decode('utf-8')
err = stderr.decode('utf-8')
```

关于子进程，简单来看下

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$(pwd;echo $BASH_SUBSHELL;ps --forest)
/root/python_demo
1
   PID TTY          TIME CMD
  9324 pts/0    00:00:00 bash
 49906 pts/0    00:00:00  \_ bash
 49907 pts/0    00:00:00      \_ ps
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$pwd;echo $BASH_SUBSHELL;ps --forest
/root/python_demo
0
   PID TTY          TIME CMD
  9324 pts/0    00:00:00 bash
 49908 pts/0    00:00:00  \_ ps
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

也可以进程列表同协程结合的方式。你既可以在子shell中 进行繁重的处理工作，同时也不会让子shell的I/O受制于终端。

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$coproc (sleep 10;ps --forest;sleep 10;ps --forest)
[1] 50326
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$jobs
[1]+  运行中               coproc COPROC ( sleep 10; ps --forest; sleep 10; ps --forest ) &
```

如果直接丢到后台会自动在终端输出IO

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$( sleep 10; ps --forest; sleep 10; ps --forest ) &
[1] 50335
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$ps --forest
   PID TTY          TIME CMD
  9324 pts/0    00:00:00 bash
 50335 pts/0    00:00:00  \_ bash
 50336 pts/0    00:00:00  |   \_ sleep
 50337 pts/0    00:00:00  \_ ps
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$   PID TTY          TIME CMD
  9324 pts/0    00:00:00 bash
 50335 pts/0    00:00:00  \_ bash
 50340 pts/0    00:00:00      \_ ps

[1]+  完成                  ( sleep 10; ps --forest; sleep 10; ps --forest )
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

`subprocess 模块对于依赖 TTY 的外部命令不合适用`。例如，你不能使用它来自动化一个用户输入密码的任务(比如一个 ssh 会话)。这时候，你需要使用到第三方模块了，比如基于著名的 expect 家族的工具(pexpect 或类似的)(pexpect可以理解为Linux下的expect的Python封装、通过pexpect可以实现对ssh、ftp、passwd、telnet等命令行进行自动交互，而无需人工干涉来达到自动化的目的。比如我们可以模拟一个FTP登录时所有交互，包括输入主机地址、用户名、密码、上传文件等，待出现异常还可以进行尝试自动处理。)

### 复制或者移动文件和目录

**复制或移动文件和目录，但是又不想调用 shell 命令。**

`shutil 模块`有很多便捷的函数可以复制文件和目录。使用起来非常简单

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import shutil
# Copy src to dst. (cp src dst)
shutil.copy(src, dst)
# Copy files, but preserve metadata (cp -p src dst)
shutil.copy2(src, dst)
# Copy directory tree (cp -R src dst)
shutil.copytree(src, dst)
# Move src to dst (mv src dst)
shutil.move(src, dst)

```

这里不多讲，熟悉Linux的小伙伴应该不陌生。

默认情况下，对于`符号链接`这些命令处理的是它指向的东西文件。例如，如果`源文件`是一个`符号链接`，那么目标文件将会是`符号链接`指向的文件。如果你只想`复制符号链接本身`，那么需要指定`关键字`参数 `follow_symlinks`

```py
shutil.copytree(src, dst, symlinks=True)
```

copytree() 可以让你在复制过程中选择性的忽略某些文件或目录。你可以提供一个忽略函数，接受一个目录名和文件名列表作为输入，返回一个忽略的名称列表。例如：

```py
def ignore_pyc_files(dirname, filenames):
    return [name in filenames if name.endswith('.pyc')]
shutil.copytree(src, dst, ignore=ignore_pyc_files)
```

对于文件元数据信息，`copy2()`这样的函数只能尽自己最大能力来保留它。`访问时间、创建时间和权限`这些基本信息会被保留，但是`对于所有者、ACLs、资源 fork`和其他更深层次的文件元信息就说不准了

通常不会去使用`shutil.copytree() 函数`来执行`系统备份`。当处理文件名的时候，最好使用`os.path`中的函数来确保最大的可移植性

```py
>>> filename = '/etc/docker/daemon.json'
>>> import os.path
>>> os.path.basename(filename)
'daemon.json'
>>> os.path.dirname(filename)
'/etc/docker'
>>> os.path.split(filename)
('/etc/docker', 'daemon.json')
>>> os.path.join('/new/dir', os.path.basename(filename))
'/new/dir/daemon.json'
>>> os.path.expanduser('~/guido/programs/daemon.json')
'/root/guido/programs/daemon.json'
>>>
```

使用` copytree() `复制文件夹的一个棘手的问题是对于错误的处理,可以使用异常块处理，或者通过 `参数 ignore dangling symlinks=True`忽略掉无效符号链接。

```py
try:
    shutil.copytree(src, dst)
except shutil.Error as e:
    for src, dst, msg in e.args[0]:
    # src is source name
    # dst is destination name
    # msg is error message from exception
    print(dst, src, msg)
```

### 创建和解压归档文件

**创建或解压常见格式的归档文件(比如.tar, .tgz 或.zip)**

shutil 模块拥有两个函数—— make archive() 和 unpack archive() 可派上用场,

```py
>>> import shutil
>>> shutil.unpack_archive('Python-3.3.0.tgz')
>>> shutil.make_archive('py33','zip','Python-3.3.0')
'/Users/beazley/Downloads/py33.zip'

```

`make archive()`的第二个参数是期望的输出格式。可以使用`get archive formats()`获取所有支持的归档格式列表。

```py
>>> import shutil
>>> shutil.get_archive_formats()
[('bztar', "bzip2'ed tar-file"), ('gztar', "gzip'ed tar-file"), ('tar', 'uncompressed tar file'), ('xztar', "xz'ed tar-file"), ('zip', 'ZIP file')]
>>>
```

### 通过文件名查找文件

**你需要写一个涉及到文件查找操作的脚本，比如对日志归档文件的重命名工具，你不想在 Python 脚本中调用 shell，或者你要实现一些 shell 不能做的功能。**

查找文件，可使用`os.walk() 函数`，传一个顶级目录名给它

```py
#!/usr/bin/env python3

import os,sys

def findfile(start, name):
    for relpath, dirs, files in os.walk(start):
        if name in files:
            full_path = os.path.join(start, relpath, name)
            print(os.path.normpath(os.path.abspath(full_path)))

if __name__ == '__main__':
    findfile(sys.argv[1], sys.argv[2])
```

`os.walk() 方法`为我们`遍历目录树`，每次进入一个目录，它会返回一个`三元组`，包含`相对于查找目录的相对路径，一个该目录下的目录名列表，以及那个目录下面的文件名列表。`

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./find.py /etc/ passwd
/etc/passwd
/etc/pam.d/passwd
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

对于每个元组，只需检测一下目标文件名是否在文件列表中。如果是就使用`os.path.join()`合并路径。为了避免奇怪的路径名比如`././foo//bar` ，使用了另外两个函数来修正结果

+ 第一个是` os.path.abspath() `, 它接受一个路径，可能是相对路径，最后返回绝对路径。
+ 第二个是` os.path.normpath() `，用来返回正常路径，可以解决双斜杆、对目录的多重引用的问题等。

`os.walk(start)`还有跨平台的优势。并且，还能很轻松的加入其他的功能。我们再演示一个例子，下面的函数打印所有最近被修改过的文件：

```py
#!/usr/bin/env python3
import os
import time
import sys

def modified_within(top, seconds):
    now = time.time()
    for path, dirs, files in os.walk(top):
        for name in files:
            fullpath = os.path.join(path, name)
            if os.path.exists(fullpath):
                mtime = os.path.getmtime(fullpath)
                if mtime > (now - seconds):
                    print(fullpath)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: {} dir seconds'.format(sys.argv[0]))
        raise SystemExit(1)
    modified_within(sys.argv[1], float(sys.argv[2]))
```

打印10分钟之前被修改的数据

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./find.py /etc/ 10
/etc/mtab
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$ll /etc/mtab
lrwxrwxrwx. 1 root root 17 10月 18 2018 /etc/mtab -> /proc/self/mounts
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$ll /proc/self/mounts
-r--r--r-- 1 root root 0 5月   2 01:18 /proc/self/mounts
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

### 读取配置文件

**怎样读取普通.ini 格式的配置文件？**

`configparser 模块`能被用来读取配置文件

编写配置文件

```ini
; config.ini
; Sample configuration file
[installation]
library=%(prefix)s/lib
include=%(prefix)s/include
bin=%(prefix)s/bin
prefix=/usr/local

# Setting related to debug configuration
[debug]
log_errors=true
show_warnings=False

[server]
port: 8080
nworkers: 32
pid-file=/tmp/spam.pid
root=/www/root
signature:
    =================================
    Brought to you by the Python Cookbook
    =================================
```

```py
>>> from configparser import ConfigParser
>>> cfg = ConfigParser()
>>> cfg.read('config.ini')
['config.ini']
>>> cfg.sections()
['installation', 'debug', 'server']
>>> cfg.get('installation','library')
'/usr/local/lib'
>>> cfg.getboolean('debug','log_errors')
True
>>> cfg.getint('server','port')
8080
>>> cfg.getint('server','nworkers')
32
>>> print(cfg.get('server','signature'))

=================================
Brought to you by the Python Cookbook
=================================
>>>
```

如果有需要，你还能修改配置并使用` cfg.write() `方法将其写回到文件中

```py
>>> from configparser import ConfigParser
>>> cfg = ConfigParser()
>>> cfg.read('config.ini')
['config.ini']
>>> cfg.set('server','port','9000')
>>> cfg.set('debug','log_errors','False')
>>> import sys
>>> cfg.write(sys.stdout)
[installation]
library = %(prefix)s/lib
include = %(prefix)s/include
bin = %(prefix)s/bin
prefix = /usr/local

[debug]
log_errors = False
show_warnings = False

[server]
port = 9000
nworkers = 32
pid-file = /tmp/spam.pid
root = /www/root
signature =
        =================================
        Brought to you by the Python Cookbook
        =================================

>>>
```

+ 配置文件中的名字是不区分大小写
+ 解析值的时候，`getboolean() 方法`查找任何可行的值。
+ `ConfigParser`能一次读取多个配置文件然后合并成一个配置。后面读取的配置文件会覆盖前面的配置文件

### 给简单脚本增加日志功能

**你希望在脚本和程序中将诊断信息写入日志文件。**

python 脚本打印日志最简单方式是使用 logging 模块

```py
#`!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import logging


def main():
    # Configure the logging system
    logging.basicConfig(filename='app.log',
                        level=logging.ERROR)
    # Variables (to make the calls that follow work)
    hostname = 'www.python.org'
    item = 'spam'
    filename = 'data.csv'
    mode = 'r'
    # Example logging calls (insert into your program)
    logging.critical('Host %s unknown', hostname)
    logging.error("Couldn't find %r", item)
    logging.warning('Feature is deprecated')
    logging.info('Opening file %r, mode=%r', filename, mode)
    logging.debug('Got here')
    
if __name__ == '__main__':
    main()                    

```

五个日志调用(`critical(), error(), warning(), info(), debug()`)以降序方式表示不同的严重级别。 `basicConfig()`的`level`参数是一个`过滤器`。所有级别低于此级别的日志消息都会被忽略掉。每个`logging`操作的参数是一个消息字符串，后面再跟一个或多个参数。构造最终的日志消息的时候我们使用了`%`操作符来格式化消息字符串。

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./logger.py
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$cat app.log
CRITICAL:root:Host www.python.org unknown
ERROR:root:Couldn't find 'spam'
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$
```

如果你想使用配置文件，可以像下面这样修改` basicConfig() `调用：

```bash
import logging
import logging.config
def main():
    # Configure the logging system
    logging.config.fileConfig('logconfig.ini')
```

`logconfig.ini`

```ini
[loggers]
keys=root
[handlers]
keys=defaultHandler
[formatters]
keys=defaultFormatter
[logger_root]
level=INFO
handlers=defaultHandler
qualname=root
[handler_defaultHandler]
class=FileHandler
formatter=defaultFormatter
args=('app.log', 'a')
[formatter_defaultFormatter]
format=%(levelname)s:%(name)s:%(message)s

```

在调用日志操作前先执行下`basicConfig() 函数方法`,可以找标准输出或者文件中输出

`basicConfig()` 在程序中只能被执行一次。如果你稍后想改变日志配置，就需要先获取`root logger` ，然后直接修改它。

```py
logging.getLogger().level = logging.DEBUG
```

更多见日志模块文档[https://docs.python.org/3/howto/logging-cookbook.html](https://docs.python.org/3/howto/logging-cookbook.html)

### 给函数库增加日志功能

**你想给某个函数库增加日志功能，但是又不能影响到那些不使用日志功能的程序。**

对于想要执行日志操作的函数库，你应该创建一个专属的` logger `对象，并且像下面这样初始化配置：

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import logging
log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())

# Example function (for testing)
def func():
    log.critical('A Critical Error!')
    log.debug('A debug message')

func()
```

使用这个配置，默认情况下不会打印日志,只有配置过日志系统，那么日志消息打印就开始生效

```py
logging.basicConfig()
```

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./logg.py
CRITICAL:__main__:A Critical Error!
```

通常来讲，不应该在函数库代码中`自己配置日志系统`，或者是已经有个已经存在的日志配置了。调用`getLogger( name )`创建一个和调用模块同名的`logger 模块`。由于`模块`都是唯一的，因此创建的`logger 也将是唯一`的。所以当前进程中只有一个logging会生效。

`log.addHandler(logging.NullHandler())` 操作将一个`空处理器`绑定到刚刚已经创建好的`logger 对象`上。一个空处理器默认会忽略调用所有的日志消息。因此，如果使用该函数库的时候还没有配置日志，那么将不会有消息或警告出现。

在这里，根日志被配置成仅仅`输出 ERROR 或更高级别的消息`。不过，`somelib 的日志级别被单独配置成可以输出 debug 级别的消息，`它的优先级比全局配置高。像这样更改单独模块的日志配置对于调试来讲是很方便的，因为你无需去更改任何的全局日志配置——只需要修改你想要更多输出的模块的日志等级。(这个还有待研究)

### 实现一个计时器

**你想记录程序执行多个任务所花费的时间**

`time 模块`包含很多函数来执行跟时间有关的函数。尽管如此，通常我们会在此基础之上构造一个更高级的接口来模拟一个计时器。

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import time


class Timer:
    def __init__(self, func=time.perf_counter):
        self.elapsed = 0.0
        self._func = func
        self._start = None

    def start(self):
        if self._start is not None:
            raise RuntimeError('Already started')
        self._start = self._func()

    def stop(self):
        if self._start is None:
            raise RuntimeError('Not started')
        end = self._func()
        self.elapsed += end - self._start
        self._start = None

    def reset(self):
        self.elapsed = 0.0

    @property #类的属性私有化，那么可以使用@property 使属性可以被外部访问并修改
    def running(self):
        return self._start is not None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()

```

这个类定义了一个可以被用户根据需要启动、停止和重置的计时器。它会在elapsed 属性中记录整个消耗时间。下面是一个例子来演示怎样使用它：

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import time


class Timer:
    def __init__(self, func=time.perf_counter):
        self.elapsed = 0.0
        self._func = func
        self._start = None

    def start(self):
        if self._start is not None:
            raise RuntimeError('Already started')
        self._start = self._func()

    def stop(self):
        if self._start is None:
            raise RuntimeError('Not started')
        end = self._func()
        self.elapsed += end - self._start
        self._start = None

    def reset(self):
        self.elapsed = 0.0

    @property #类的属性私有化，那么可以使用@property 使属性可以被外部访问并修改
    def running(self):
        return self._start is not None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()

def countdown(n):
    while n > 0:
        n -= 1
# Use 1: Explicit start/stop
t = Timer()
t.start()
countdown(1000000)
t.stop()
print(t.elapsed)
# Use 2: As a context manager
with t:
    countdown(1000000)
print(t.elapsed)
with Timer() as t2:
    countdown(1000000)
print(t2.elapsed)

```

这里通过`__enter__，__exit__`,使用`with 语句`以及上下文管理器协议可以省略计时器打开和关闭操作。(关于上下文管理协议，即with语句，为了让一个对象兼容with语句，必须在这个对象的类中声明`__enter__和__exit__方法,`,`__enter__`在出现with语句被调用，`__exit__`在代码执行完毕被调用,可以参考open()方法)

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./times.py
0.05191648800246185
0.12038616700374405
0.06592946800083155
```

在计时中要考虑一个`底层的时间函数问题`。 一般来说， 使用`time.time()`或`time.clock()`计算的时间精度因操作系统的不同会有所不同。而使用`time.perf_counter()`函数可以确保使用系统上面`最精确的计时器`。

### 限制脚本的内存和CPU的使用量

**你想对在 Unix 系统上面运行的程序设置内存或 CPU 的使用限制。**

#### cpu 限制

`resource 模块`能同时执行这两个任务。例如，要限制 CPU 时间,下面的代码在windows平台执行不了，但是Linux是可以的。

```py
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-

import signal
import resource  
import os
def time_exceeded(signo, frame):
    print("Time's up!")
    raise SystemExit(1)
def set_max_runtime(seconds):
    # 安装信号处理程序并设置资源限制  
    soft, hard = resource.getrlimit(resource.RLIMIT_CPU)
    # 限制CUP使用时间为15秒
    resource.setrlimit(resource.RLIMIT_CPU, (seconds, hard))
    # 进程即将结束，给一个信号量，加一个回调
    signal.signal(signal.SIGXCPU, time_exceeded)

if __name__ == '__main__':
    set_max_runtime(15)
    while True:
        pass
```

程序运行时，`SIGXCPU 信号`在时间过期时被生成，然后执行清理并退出。

```bash
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$vim cpu.py
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$chmod +x cpu.py
┌──[root@liruilongs.github.io]-[~/python_demo]
└─$./cpu.py
Time's up!
```

#### 内存限制

这暂时没有好的Demo...

```bash
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-


import resource

def limit_memory(maxsize):
        soft, hard = resource.getrlimit(resource.RLIMIT_AS)
        resource.setrlimit(resource.RLIMIT_AS, (maxsize, hard))

# 0.5 * 1024 ^ 6 = 576460752303423488 设置最大内存500M
limit_memory(576460752303423488)
```

程序运行到没有多余内存时会抛出 MemoryError 异常。

`setrlimit() 函数`被用来设置特定资源上面的`软限制和硬限制`。

+ `软限制是一个值`，当超过这个值的时候操作系统通常会发送一个信号来限制或通知该进程.

```py
>>> resource.RLIMIT_AS
9
```

+ `硬限制`是用来指定软限制能设定的最大值。通常来讲，这个由系统管理员通过设置系统级参数来决定。尽管硬限制可以改小一点，但是最好不要使用用户进程去修改。

```py
>>> resource.getrlimit(resource.RLIMIT_AS)
(-1, -1)
```

`setrlimit() 函数`还能被用来设置`子进程数量、打开文件数以及类似系统资源的限制(cgroup)`。

### 启动一个WEB浏览器

**通过脚本启动浏览器并打开指定的 URL 网页**

`webbrowser 模块`能被用来启动一个浏览器，并且与平台无关

```bash
Windows PowerShell
版权所有 (C) Microsoft Corporation。保留所有权利。

尝试新的跨平台 PowerShell https://aka.ms/pscore6

PS E:\docker> python
Python 3.9.0 (tags/v3.9.0:9cf6752, Oct  5 2020, 15:23:07) [MSC v.1927 32 bit (Intel)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
>>> import webbrowser
>>> webbrowser.open('http://www.python.org')
True
>>>

```

新窗口打卡网站

```py
webbrowser.open_new('http://www.python.org')
```

当前窗口打开一个tab页

```py
webbrowser.open_new_tab('http://www.python.org')
```

指定浏览器类型，可以使用 webbrowser.get() 函数

```py
>>> c = webbrowser.get('firefox')
>>> c.open('http://www.python.org')
True
>>> c.open_new_tab('http://docs.python.org')
True
>>>
```
