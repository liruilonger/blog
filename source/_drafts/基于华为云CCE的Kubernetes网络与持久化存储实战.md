---
title: 基于华为云CCE的Kubernetes网络与持久化存储实战
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-08-26 14:00:36/基于华为云CCE的Kubernetes网络与持久化存储实战.html'
mathJax: false
date: 2022-08-26 22:00:36
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***




### 购买云容器引擎服务CCE


登录华为云”步骤已登录的华为云控制台界面，鼠标移动到云桌面浏览器页面中左侧菜单栏，点击“服务列表”->“容器”->“云容器引擎CCE”，进入云容器引擎CCE控制台

![在这里插入图片描述](https://img-blog.csdnimg.cn/e605ad0102e9456c84e52417a32518a5.png)

选择合适的产品，这里我们选择CCE集群普通版

![在这里插入图片描述](https://img-blog.csdnimg.cn/449f0c66ceaf49ad99e6061fe43f9101.png)

选择标准版CCE集群，点击“创建”，进入云容器引擎CCE购买页配置参数如下：
+ 计费模式：按需计费
+ 区域：华北北京四
+ 集群名称：cce01
+ 版本：V1.19.10⑤集群管理规模：50节点⑥控制节点数：1勾选协议
+ 虚拟私有云：预置环境预置的VPC
+ 所在子网：预置环境预置的子网
+ 网络模型：容器隧道网络
+ 容器网段：勾选“自动选择”

这里还有其他的配置参数，配置完成单机下一步，最后点提交完成CCE的配置，集群创建预计需要10分钟左右，可以单击“查看集群事件列表”后查看集群详情。

cp /root/kubernetes/client/bin/kubectl /home/
cp kubeconfig.json /home/
cd /home

[root@cce01-node1 .kube]# kubectl config  use-context internal
Switched to context "internal".
[root@cce01-node1 .kube]# 


[root@cce01-node1 .kube]# kubectl cluster-info
Kubernetes master is running at https://192.168.20.67:5443
CoreDNS is running at https://192.168.20.67:5443/api/v1/namespaces/kube-system/services/coredns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
[root@cce01-node1 .kube]# 



## 博文参考


