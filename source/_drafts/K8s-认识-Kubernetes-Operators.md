---
title: K8s;认识 Kubernetes Operators
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-02-12 15:26:49/K8s;认识 Kubernetes Operators .html'
mathJax: false
date: 2023-02-12 23:26:49
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

### 简单介绍

#### Operator  模式

Operator 是 Kubernetes 的扩展软件， 它利用 `CRD` 管理应用及其组件。 Operator 遵循 Kubernetes 的理念，特别是在控制器方面。


Operator 模式 旨在记述(正在管理一个或一组服务的)运维人员的关键目标。 这些运维人员负责一些特定的应用和 Service，他们需要清楚地知道系统应该如何运行、如何部署以及出现问题时如何处理。

在 Kubernetes 上运行工作负载的人们都喜欢通过自动化来处理重复的任务。 Operator 模式会封装你编写的(Kubernetes 本身提供功能以外的)任务自动化代码。



#### Kubernetes 上的 Operator

Kubernetes 为自动化而生。无需任何修改，你即可以从 Kubernetes 核心中获得许多内置的自动化功能。 你可以使用 Kubernetes 自动化部署和运行工作负载，甚至 可以自动化 Kubernetes 自身。

Kubernetes 的 Operator 模式概念允许你在不修改 Kubernetes 自身代码的情况下， 通过为一个或多个自定义资源关联控制器来扩展集群的能力。 Operator 是 Kubernetes API 的客户端， 充当自定义资源的控制器。

Operator 示例

使用 Operator 可以自动化的事情包括：

+ 按需部署应用
+ 获取/还原应用状态的备份
+ 处理应用代码的升级以及相关改动。例如数据库 Schema 或额外的配置设置
+ 发布一个 Service，要求不支持 Kubernetes API 的应用也能发现它
+ 模拟整个或部分集群中的故障以测试其稳定性
+ 在没有内部成员选举程序的情况下，为分布式应用选择首领角色



#### 编写你自己的 Operator

如果生态系统中没可以实现你目标的 Operator，你可以自己编写代码。

你还可以使用任何支持 Kubernetes API 客户端的语言或运行时来实现 Operator(即控制器)。

以下是一些库和工具，你可用于编写自己的云原生 Operator：

+ Charmed Operator Framework
+ Java Operator SDK
+ Kopf (Kubernetes Operator Pythonic Framework)
+ kube-rs (Rust)
+ kubebuilder
+ KubeOps (.NET operator SDK)
+ KUDO(Kubernetes 通用声明式 Operator)
+ Mast
+ Metacontroller，可与 Webhook 结合使用，以实现自己的功能。
+ Operator Framework
+ shell-operator

(以上内容来自 k8s 官方文档：https://kubernetes.io/zh-cn/docs/concepts/extend-kubernetes/operator/)






## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***

https://operatorhub.io/

https://kubernetes.io/zh-cn/docs/concepts/extend-kubernetes/operator/

https://codeburst.io/kubernetes-operators-by-example-99a77ea4ac43

https://github.com/calvin-puram/awesome-kubernetes-operator-resources

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
