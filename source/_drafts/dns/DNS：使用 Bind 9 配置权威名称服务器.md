---
title: DNS： 使用 Bind 9 配置权威名称服务器
tags:
  - Bind9
categories:
  - Bind9
toc: true
recommend: 1
keywords: Bind9
uniqueId: '2023-02-19 11:46:11/使用 Bind 9 配置权威名称服务器.html'
mathJax: false
date: 2023-02-19 19:46:11
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

权威名称服务器存储 DNS 资源记录，并为其管理的`区域`提供`权威答案`。

http://www.isc.org/bind/

RHEL中的BIND (Berkeley Internet Name Domain)软件可以实现权威的名称服务器。`BIND` 允许您将`权威服务器`配置为区域的`主要`服务器或`辅助`服务器。


`zone`中有且只能有一台`主`服务器，但可具有多台`辅助`服务器。辅助服务器定期从主服务器下载区域信息的最新版本。它们执行区域传输的频率以及如何知道其数据是否过时由区域的`SOA资源记录`控制。




向供应商注册新的`域名`时，必须提供该`域`的`公共权威名称服务器的名称和IP地址`。注册服务商将该信息放在`父域的区域文件`中(如NS，A和AAAA记录)，以便`DNS解析器`可以找到您的名称服务器。为了帮助确保可靠性，您应该至少有`两个公共DNS服务器`，并且它们应位于不同的站点，以避免由于网络故障而造成的中断。


外部主机如何与其`缓存名称服务器`和`授权名称服务器`一起工作，以便在示例中对记录执行DNS查找。假设还没有缓存的记录:

外部访问
![在这里插入图片描述](https://img-blog.csdnimg.cn/65d167f7584b4c27b0e7fb7e93d18449.png)

客户的仅缓存名称服务器首先查询一个根名称服务器。它被定向到负责。com域的名称服务器池。其中一个服务器响应example.com域的NS记录，因此仅缓存的名称服务器查询一个面向公共的次要名称服务器。在本例中，主名称服务器实际上不是公共的，但是辅助名称服务器可以从主名称服务器执行区域传输，以便它们拥有关于example.com区域的最新数据。下图说明了对于example.com域内的内部仅缓存名称服务器，该过程是相同的:

 
内部访问
![在这里插入图片描述](https://img-blog.csdnimg.cn/e2715ef3598146b3a1bc2f609c5444ba.png)

更好的方法是提供内部名称服务器可以查询的内部授权辅助服务器。当本地域存在问题时，这消除了外部查询，这更安全。

 

内部访问
![在这里插入图片描述](https://img-blog.csdnimg.cn/454ecd0d19ea422b8871dbaf86573d15.png)

为此，您需要配置内部缓存名称服务器来转发对记录的请求。Com到内部辅助服务器。(例如，使用Unbound时，您需要配置适当的forward-zone块。)

 
1.安装bind软件//bind
2.修改配置文件///etc/named.conf
3.启用并启动服务//named.service
4.开通防火墙//dns

```bash
[root@serverb ~]# yum -y install bind
```

## 配置

named主要配置文件是 `/etc/named.conf`，该文件控制BIND的基本操作，由root用户和named组拥有，具有权限0640，并且具有named_conf_tSELinux类型。

配置DNS服务器需要执行以下步骤：
+ 配置地址匹配列表。
+ 配置named侦听的IP地址。
+ 配置客户端的访问控制。
+ 配置 zone
+ 编写 区域文件。

默认配置：

+ 将服务配置为基本的递归缓存名称服务器。
+ 侦听IPv4和IPV6环回接口的端口 53 UDP/TCP(127.0.0.1和：：1)上的连接。
+ 每个区域的配置文件保存在 `/var/named` 中。

```bash
[student@serverb ~]$ man named.conf
```

### 定义地址匹配列表

使用 `acl` 指令定义地址匹配列表。`acl`指令不是用于控制客户端对服务器的访问，而是使用它们来定义IP地址和网络列表。

条目可以是完整的IP地址或网络，用尾点(192.168.0.)或CIDR表示法(192.168.0/24)，也可以使用先前定义的地址匹配列表的名称。acl语句定义的地址集可以被多个指令引用。
示例：
```bash
acl trusted  {  172.25.250.11; 192.168.0.11; };
acl internal {  172.25.250.0/24; };
acl classroom { 192.168.0.0/24; trusted; };
```

named 中内置了四个ACL：

|ACL：  Description |
|--|
|none ：Matches no hosts.|
|any ：Matches all hosts.|
|localhost ：Matches all IP addresses of the DNS server.|
|localnets ：Matches all hosts from the DNS server's local subnets.|



### 配置客户端的访问控制

options 块中使用以下三个指令配置控制访问：

+ `allow-query`，控制`所有查询`。默认情况下，`allow-query` 设置为 localhost。对于`公开权威服务器`必须定义为`allow-query{any;};`允许任意客户端查询。

```bash
allow-query{localhost;172.25.250.254;192.168.0.0/24;};
```
+ `allow-recursion`，控制`递归查询`。权威服务器`不应允许`递归查询，防止服务器被用于`DNS放大分布式拒绝服务攻击`，并更好地保护其免受缓存中毒攻击。配置此功能最简单的方法是完全关闭递归：`recursion no`;如果必须允许受信任的客户端执行递归，则可以打开递归并为这些特定主机或网络设置：

```bash
allow-recursion{trusted-nets;};
```
+ `allow-transfer`，控制`区域转移`。区域转移允许客户端获取您`区域中所有数据的转储`。区域转移应该受到限制，以使潜在的攻击者更难执行一个DNS查询来快速获取您区域中的所有资源记录。`主服务器必须配置允许转移`，以允许您的`从服务器`执行区域转移。您应该禁止其他主机执行区域传输。您可能允许`localhost`执行区域传输以帮助进行故障排除。


### 配置 zone 


```bash
[root@serverb named]# ls
data     named.ca     named.localhost  slaves
dynamic  named.empty  named.loopback
[root@serverb named]#
```


根域服务器的 zone 文件
```bash
zone "." IN {
        type hint;
        file "named.ca";
};
```
对应 `directory       "/var/named";`中对应的文件


```conf
[root@serverb named]# cat named.ca

; <<>> DiG 9.11.3-RedHat-9.11.3-3.fc27 <<>> +bufsize=1200 +norec @a.root-servers.net
; (2 servers found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 46900
;; flags: qr aa; QUERY: 1, ANSWER: 13, AUTHORITY: 0, ADDITIONAL: 27

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;.                              IN      NS

;; ANSWER SECTION:
.                       518400  IN      NS      a.root-servers.net.
.                       518400  IN      NS      b.root-servers.net.
.                       518400  IN      NS      c.root-servers.net.
.                       518400  IN      NS      d.root-servers.net.
.......
.                       518400  IN      NS      m.root-servers.net.

;; ADDITIONAL SECTION:
a.root-servers.net.     518400  IN      A       198.41.0.4
b.root-servers.net.     518400  IN      A       199.9.14.201
c.root-servers.net.     518400  IN      A       192.33.4.12
d.root-servers.net.     518400  IN      A       199.7.91.13
.......
m.root-servers.net.     518400  IN      A       202.12.27.33
a.root-servers.net.     518400  IN      AAAA    2001:503:ba3e::2:30
b.root-servers.net.     518400  IN      AAAA    2001:500:200::b
c.root-servers.net.     518400  IN      AAAA    2001:500:2::c
d.root-servers.net.     518400  IN      AAAA    2001:500:2d::d
e.root-servers.net.     518400  IN      AAAA    2001:500:a8::e
f.root-servers.net.     518400  IN      AAAA    2001:500:2f::f
.........
m.root-servers.net.     518400  IN      AAAA    2001:dc3::35

;; Query time: 24 msec
;; SERVER: 198.41.0.4#53(198.41.0.4)
;; WHEN: Thu Apr 05 15:57:34 CEST 2018
;; MSG SIZE  rcvd: 811

[root@serverb named]#
```

引入的 zone 文件
```bash

include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";
```

`"/etc/named.rfc1912.zones";` 
```conf
[root@serverb named]# cat /etc/named.rfc1912.zones
// named.rfc1912.zones:
//
// Provided by Red Hat caching-nameserver package
//
// ISC BIND named zone configuration for zones recommended by
// RFC 1912 section 4.1 : localhost TLDs and address zones
// and http://www.ietf.org/internet-drafts/draft-ietf-dnsop-default-local-zones-02.txt
// (c)2007 R W Franks
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

// 当前 zone 的为master 服务器，解析由文件 named.localhost 决定
zone "localhost.localdomain" IN {
        type master;
        file "named.localhost";
        allow-update { none; };
};

zone "localhost" IN {
        type master;
        file "named.localhost";
        allow-update { none; };
};

zone "1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.ip6.arpa" IN {
        type master;
        file "named.loopback";
        allow-update { none; };
};

zone "1.0.0.127.in-addr.arpa" IN {
        type master;
        file "named.loopback";
        allow-update { none; };
};

zone "0.in-addr.arpa" IN {
        type master;
        file "named.empty";
        allow-update { none; };
};

[root@serverb named]#
```

```conf
[root@serverb named]# cat named.localhost
$TTL 1D
@       IN SOA  @ rname.invalid. (              ;@ 是一个特殊的符号，表示当前域名本身
                                        0       ; serial，序列号
                                        1D      ; refresh，刷新时间
                                        1H      ; retry，重试时间
                                        1W      ; expire，过期时间
                                        3H )    ; minimum，最小时间间隔
        NS      @                               ; name server，域名服务器
        A       127.0.0.1                       ;      IPv4 地址
        AAAA    ::1                             ; IPv6 地址
[root@serverb named]#
```

```conf
[root@serverb named]# cat named.loopback
$TTL 1D
@       IN SOA  @ rname.invalid. (
                                        0       ; serial
                                        1D      ; refresh
                                        1H      ; retry
                                        1W      ; expire
                                        3H )    ; minimum
        NS      @
        A       127.0.0.1
        AAAA    ::1
        PTR     localhost.
[root@serverb named]#
```

防火墙放行

```bash
[root@serverb ~]# firewall-cmd --add-service=dns --permanent
success
[root@serverb ~]# firewall-cmd --reload
success
[root@serverb ~]# systemctl enable named.service --now
```

```conf
[root@serverb ~]# cat /etc/named.conf
// named.conf
// 这是一个 DNS 服务器配置文件，用于配置 ISC BIND named(8) DNS 服务器作为一个只提供缓存服务的本地 DNS 解析器。

// 定义一个名为 trusted 的 ACL，包含两个 IP 地址，用于限制可以访问 DNS 服务器的 IP 地址。
acl trusted  {  172.25.250.11; 192.168.0.11; };
// 定义一个名为 internal 的 ACL，包含一个 IP 地址段，用于限制可以访问 DNS 服务器的 IP 地址。
acl internal {  172.25.250.0/24; };
// 定义一个名为 classroom 的 ACL，包含一个 IP 地址段和一个名为 trusted 的 ACL，用于限制可以访问 DNS 服务器的 IP 地址。
acl classroom { 192.168.0.0/24; trusted; };

// 配置选项
options {
        // 监听任意 IPv4 地址的 53 端口
        listen-on port 53 { any; };
        // 监听任意 IPv6 地址的 53 端口
        listen-on-v6 port 53 { any; };
        // 指定 DNS 数据文件的存储目录
        directory       "/var/named";
        // 指定缓存文件的存储路径
        dump-file       "/var/named/data/cache_dump.db";
        // 指定统计信息文件的存储路径
        statistics-file "/var/named/data/named_stats.txt";
        // 指定内存统计信息文件的存储路径
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        // 指定 DNSSEC 安全根密钥文件的存储路径
        secroots-file   "/var/named/data/named.secroots";
        // 指定递归查询缓存文件的存储路径
        recursing-file  "/var/named/data/named.recursing";
        // 允许查询 DNS 数据的客户端 IP 地址
        allow-query     { localhost; classroom; };
        /*
         - 如果你正在构建一个权威 DNS 服务器，请不要启用递归查询。
         - 如果你正在构建一个递归查询（缓存）DNS 服务器，请启用递归查询。
         - 如果你的递归 DNS 服务器有一个公共 IP 地址，你必须启用访问控制，以限制查询到合法的用户。如果不这样做，你的服务器将成为大规模 DNS 放大攻击的一部分。在你的网络中实施 BCP38 将大大减少这种攻击面。
        */
        // 禁止递归查询
        recursion no;

        // 启用 DNSSEC 安全根密钥验证
        dnssec-enable yes;
        // 启用 DNSSEC 数据验证
        dnssec-validation yes;

        // 指定动态密钥的存储目录
        managed-keys-directory "/var/named/dynamic";

        // 指定 PID 文件的存储路径
        pid-file "/run/named/named.pid";
        // 指定会话密钥文件的存储路径
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        // 包含加密策略配置文件
        include "/etc/crypto-policies/back-ends/bind.config";
};

// 配置日志记录
logging {
        // 定义一个名为 default_debug 的日志通道，将日志输出到 data/named.run 文件中
        channel default_debug {
                file "data/named.run";
                // 日志级别为动态调整
                severity dynamic;
        };
};

// 配置根域名服务器
zone "." IN {
        // 指定该区域为“提示”类型，即提示 DNS 服务器根域名服务器的 IP 地址
        type hint;
        // 指定根域名服务器的数据文件
        file "named.ca";
};

// 包含 RFC1912 中定义的标准 DNS 区域文件配置
include "/etc/named.rfc1912.zones";
// 包含根域名服务器的公钥
include "/etc/named.root.key";


[root@serverb ~]#
```
使用 dig 命令在 serverb 上查询 localhost.localdomain 的 DNS 解析结果。其中 @172.25.250.11 表示使用 IP 地址为 172.25.250.11 的 DNS 服务器进行查询。输出信息中的 status: REFUSED 表示查询被拒绝，WARNING: recursion requested but not available 表示递归查询不可用。这通常是由于 DNS 服务器未配置递归查询或未授权查询者进行递归查询所致。
```bash
[student@servera ~]$ dig  @serverb localhost.localdomain @172.25.250.11

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el8 <<>> @serverb localhost.localdomain @172.25.250.11
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: REFUSED, id: 56767
;; flags: qr rd; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
; COOKIE: f08d1204f74faabe21d039db63f384b8068d2e4c06fc6f50 (good)
;; QUESTION SECTION:
;localhost.localdomain.         IN      A

;; Query time: 0 msec
;; SERVER: 172.25.250.11#53(172.25.250.11)
;; WHEN: Mon Feb 20 22:33:28 CST 2023
;; MSG SIZE  rcvd: 78
```

在 servera 上使用 dig 命令查询 localhost.localdomain 的 DNS 解析结果。其中 @192.168.0.11 表示使用 IP 地址为 192.168.0.11 的 DNS 服务器进行查询。输出信息中的 status: NOERROR 表示查询成功，flags: qr aa rd 表示查询是一个回答（answer）查询，使用了授权（authoritative）回答和递归（recursion desired）查询。ANSWER SECTION 中的 127.0.0.1 表示 localhost.localdomain 的 IP 地址为 127.0.0.1。AUTHORITY SECTION 中的 localhost.localdomain. 表示 localhost.localdomain 的授权 DNS 服务器为本地主机。ADDITIONAL SECTION 中的 ::1 表示 localhost.localdomain 的 IPv6 地址为 ::1。

```bash
[student@servera ~]$ dig  localhost.localdomain @192.168.0.11

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el8 <<>> localhost.localdomain @192.168.0.11
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 65182
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
; COOKIE: efa0893e5d57752d3c67034563f388fb9f496bfc771dadb4 (good)
;; QUESTION SECTION:
;localhost.localdomain.         IN      A

;; ANSWER SECTION:
localhost.localdomain.  86400   IN      A       127.0.0.1

;; AUTHORITY SECTION:
localhost.localdomain.  86400   IN      NS      localhost.localdomain.

;; ADDITIONAL SECTION:
localhost.localdomain.  86400   IN      AAAA    ::1

;; Query time: 9 msec
;; SERVER: 192.168.0.11#53(192.168.0.11)
;; WHEN: Mon Feb 20 22:51:39 CST 2023
;; MSG SIZE  rcvd: 136

[student@servera ~]$
```




## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
