---
title: DNS：使用 Unbound 配置 DNS 缓存服务器
tags:
  - DNS
categories:
  - DNS
toc: true
recommend: 1
keywords: DNS
uniqueId: '2023-06-19 07:07:12/DNS：使用 Unbound 配置 DNS 缓存服务器.html'
mathJax: false
date: 2023-02-19 15:07:12
thumbnail:
---

**<font color="009688"> 看过的书很少，《生命中不能承受之轻》 是之一，年少时第一次读，是书中情欲的片段吸引了我，那时读到萨宾娜，想到了《月亮与六便士的》中的布兰奇，奇怪那种生理吸引到底是什么？后来陆续看了其他的章节,记忆犹新，扉页上那一句 '人们一思索，上帝就发笑  ---犹太谚语'，开篇讲的'永劫轮回观'特别适合之后自己，大概是叔本华看多了。特别喜欢书中一句，记得不清楚，大概是 '世人的博爱是以媚俗作态作为基础'**</font>

<!-- more -->
## 写在前面

***
+ 分享一些 DNS 缓存服务器搭建的笔记
+ 理解不足小伙伴帮忙指正



**<font color="009688"> 看过的书很少，《生命中不能承受之轻》 是之一，年少时第一次读，是书中情欲的片段吸引了我，那时读到萨宾娜，想到了《月亮与六便士的》中的布兰奇，奇怪那种生理吸引到底是什么？后来陆续看了其他的章节,记忆犹新，扉页上那一句 '人们一思索，上帝就发笑  ---犹太谚语'，开篇讲的'永劫轮回观'特别适合之后自己，大概是叔本华看多了。特别喜欢书中一句，记得不清楚，大概是 '世人的博爱是以媚俗作态作为基础'**</font>

***

### 简单介绍

`DNS 缓存服务器` 将 `DNS查询结果`存储在本地缓存中，并在它们的 `TTL`过期时从缓存中`删除资源记录`。

在`本地网络`中设置缓存名称服务器，它通过在本地缓存中存储最近访问的域名和 IP 地址的映射关系，减少跨`Internet`的`DNS流量`，这极大地提高了`DNS名称解析`的效率。随着本地缓存数量增加，缓存名称服务器回答越来越多的客户端查询，DNS性能将得到改善。

当用户在浏览器中输入一个网址时，浏览器会向缓存名称服务器发送一个 DNS 查询请求，如果该请求的域名和 IP 地址映射已经存在于缓存中，则缓存名称服务器可以立即返回该映射关系，而无需再向 DNS 服务器发送请求，从而加快了网页的加载速度。

有几个软件包可用于配置缓存名称服务器，包括`bind，dnsmasq`(虚拟化用的多) 和 `unbound`。这里使用 Unbound 安装、配置和管理缓存名称服务器。

### 环境准备

#### 安装 unbound
```bash
[root@servera ]# yum -y install unbound
Last metadata expiration check: 0:53:24 ago on Sun 19 Feb 2023 02:19:20 PM CST.
Installed:
  unbound-1.7.3-8.el8.x86_64
```
查看相关的配置文件
```bash
[root@servera ]# rpm -qlc unbound
/etc/sysconfig/unbound
/etc/unbound/conf.d/example.com.conf
/etc/unbound/keys.d/example.com.key
/etc/unbound/local.d/block-example.com.conf
/etc/unbound/unbound.conf
[root@servera ]#
```
#### 编辑配置文件

编辑配置文件 `/etc/unbound/unbound.conf`

帮助文档查看
```bash
[root@servera ]# man unbound.conf
```

### 常见的配置信息

#### 定义网络监听

在 server 子句中，定义`网络监听`,下面为不同的监听配置,监听IPV4和IPv6 

```bash
interface：172.25.250.10 
interface：2001:db8:1001:f0
interface-automatic:no
```

+ 默认 UNbound 监听 localhost 网络接口。
+ 如果设置监听 `0.0.0.0`或者`::0`，则将会监听所有接口，同时需要设置`interface-automatic`为`yes`(表示 DNS 服务器将自动选择最佳的网络接口来监听 DNS 查询请求)。否则设置`interface-automatic`为`no`
+ 如果此时本地还运行 `libvirtd` 服务，并且 `Unbound` 绑定到所有接口，将导致 `Unbound` 无法启动。因为 `libvirtd`会运行`dnsmasq`，而`dnsmasq`也会在本地接口上监听`53`端口。


网络监听配置Demo
```bash
interface: 0.0.0.0
# interface: ::0
# interface: 192.0.2.153
# interface: 192.0.2.154
# interface: 192.0.2.154@5003
# interface: 2001:DB8::5
#
# for dns over tls and raw dns over port 80
# interface: 0.0.0.0@443
# interface: ::0@443
# interface: 0.0.0.0@80
# interface: ::0@80
# enable this feature to copy the source address of queries to reply.
# Socket options are not supported on all platforms. experimental.
# interface-automatic: yes
#
# NOTE: Enable this option when specifying interface 0.0.0.0 or ::0
# NOTE: Disabled per Fedora policy not to listen to * on default install
# NOTE: If deploying on non-default port, eg 80/443, this needs to be disabled
interface-automatic: yes
```


#### 访问控制列表

在 server 子句中，定义`访问控制列表`。

使用`access-control`选项指定哪些`客户端`可以进行`递归查询`。可以指定网络或IP地址,控制类型：
+ `allow`，允许访问
+ `refuse`，阻止访问并将DNS REFUSED错误发送给客户端
+ `deny`，阻止访问，不发送响应

示例：

```
# access-control: 0.0.0.0/0 refuse
# access-control: 127.0.0.0/8 allow
# access-control: ::0/0 refuse
# access-control: ::1 allow
# access-control: ::ffff:127.0.0.1 allow
access-control: 172.25.250.0/24 allow
```

配置访问控制， **禁止除预期客户端之外的主机使用递归缓存名称服务器** 。

如果允许`Internet`上的任何主机递归查询您的服务器，则攻击者可以使用它对第三方执行DNS放大分布式拒绝服务攻击。详情参考` https://blog.cloudflare.com/deep-inside-a-dns-amplification-ddos-attack/`。


#### 转发请求到其他缓冲名称服务器

转发请求到其他缓冲名称服务器：


如果此名称服务器无法访问`Internet`，但可以访问另外一个连接`Internet`的`DNS服务器`。创建一个 `forward-zone` 子句以指定要转发的域以及将查询转发到的`DNS`服务器。

将名称值设置为`.` 转发所有查询

```bash
forward-zone：
      name:"."
      forward-addr：172.25.250.254
```

对`内部域`的查询直接发送到对该`域`具有`权威性`的`名称服务器`。

为转发区域指定DNS服务器：
+ 使用`forward-host`选项通过主机名
+ 使用`forward-addr`选项通过IP地址。

```bash
# forward-zone:
#       name: "example.com"
#       forward-addr: 192.0.2.68
#       forward-addr: 192.0.2.73@5355  # forward to port 5355.
#       forward-first: no
#       forward-tls-upstream: no
# forward-zone:
#       name: "example.org"
#       forward-host: fwd.example.com
```

`domain-insecure` 用于指定不需要验证 `DNSSEC` 的域名。在这个选项中 example.com 表示不需要验证 example.com 域名的 DNSSEC 安全性。这意味着，当 Unbound 接收到来自 example.com 的 DNS 响应时，它不会对响应进行 `DNSSEC` 验证。如果 example.com 的 DNS 响应被篡改或伪造，那么 Unbound 将不会检测到这种攻击

```bash
domain-insecure: "example.com"
```

#### 证书相关生成

` unbound-control-setup` 在 `/etc/unbound` 目录下生成了 `unbound_server.key` 和 `unbound_control.key` 两个 `RSA` 私钥文件，并生成了 `unbound_server.pem` 和 `unbound_control.pem` 两个`证书文件`。

其中，unbound_server.pem 是自签名的服务器证书，unbound_control.pem 是客户端证书，由服务器证书签名。这个命令还输出了证书的签名信息和主题信息。最后，命令提示您在 unbound.conf 配置文件中启用证书以使用它们。

```bash
[root@servera ]# unbound-control-setup
setup in directory /etc/unbound
generating unbound_server.key
Generating RSA private key, 3072 bit long modulus (2 primes)
..............................++++
......................................................++++
e is 65537 (0x010001)
generating unbound_control.key
Generating RSA private key, 3072 bit long modulus (2 primes)
............................................++++
.............................................................++++
e is 65537 (0x010001)
create unbound_server.pem (self signed certificate)
create unbound_control.pem (signed client certificate)
Signature ok
subject=CN = unbound-control
Getting CA Private Key
Setup success. Certificates created. Enable in unbound.conf file to use
[root@servera ]#
```

#### 配置文件语法检查
```bash
[root@servera ]# unbound-checkconf
unbound-checkconf: no errors in /etc/unbound/unbound.conf
[root@servera ]#
```

#### 启动DNS缓存服务器

开启防火墙
```bash
[root@servera ]# firewall-cmd --add-service=dns --permanent
success
[root@servera ]# firewall-cmd --reload
success
```
配置开启自启动，并启动
```bash
[root@servera ]# systemctl enable unbound --now
Created symlink /etc/systemd/system/multi-user.target.wants/unbound.service → /usr/lib/systemd/system/unbound.service.
```

测试
```bash
[root@servera ]# dig  @servera.lab.example.com workstation.lab.example.com

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el8 <<>> @servera.lab.example.com workstation.lab.example.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 24427
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;workstation.lab.example.com.   IN      A

;; ANSWER SECTION:
workstation.lab.example.com. 600 IN     A       172.25.250.9

;; Query time: 2 msec
;; SERVER: 172.25.250.10#53(172.25.250.10)
;; WHEN: Sun Feb 19 19:21:49 CST 2023
;; MSG SIZE  rcvd: 72

[root@servera ]#
```

+ `HEADER` 部分包含了查询的一些基本信息，如操作码、状态码、ID 等；
+ `QUESTION` 部分表示查询的问题部分，包含了需要查询的主机名和记录类型（A 记录）；
+ `ANSWER` 部分则是查询结果，其中包含了查询到的主机名和对应的 IP 地址。

查询的是 lab.example.com 域名的 NS 记录，使用的 DNS 服务器是 servera.lab.example.com

```bash
[root@servera ]# dig  @servera.lab.example.com NS lab.example.com

; <<>> DiG 9.11.4-P2-RedHat-9.11.4-26.P2.el8 <<>> @servera.lab.example.com NS lab.example.com
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 51183
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 4096
;; QUESTION SECTION:
;lab.example.com.               IN      NS

;; ANSWER SECTION:
lab.example.com.        86400   IN      NS      bastion.lab.example.com.

;; Query time: 2 msec
;; SERVER: 172.25.250.10#53(172.25.250.10)
;; WHEN: Sun Feb 19 19:29:04 CST 2023
;; MSG SIZE  rcvd: 66

[root@servera ]#
```
查询结果显示该域名的 `NS` 记录为 `bastion.lab.example.com`。在查询结果中，
+ `flags` 字段显示 qr rd ra，分别表示该查询为响应（response）、递归查询（recursion desired）和递归可用（recursion available）
+ `Query time` 字段显示查询耗时 2 毫秒
+ `SERVER` 字段显示使用的 DNS 服务器 IP 地址为 172.25.250.10
+ `MSG SIZE rcvd` 字段显示收到的 DNS 响应消息大小为 66 字节。


unbound DNS 服务器的状态输出结果

```bash
[root@servera ]# unbound-control status
version: 1.7.3
verbosity: 1
threads: 4
modules: 3 [ ipsecmod validator iterator ]
uptime: 349 seconds
options: reuseport control(ssl)
unbound (pid 28457) is running...
```
+ `version` 字段显示当前 unbound 版本为 1.7.3
+ `verbosity` 字段显示日志详细程度为 1
+ `threads` 字段显示使用了 4 个线程
+ `modules` 字段显示加载了 3 个模块，分别是 ipsecmod、validator 和 iterator
+ `uptime` 字段显示 unbound 运行时间为 349 秒
+ `options` 字段显示使用了 reuseport 和 control(ssl) 两个选项。
+ `unbound` 进程的 PID 为 28457，并且正在运行。


unbound DNS 服务器的缓存输出结果

```bash
[root@servera ]# unbound-control dump_cache
START_RRSET_CACHE
;rrset 504 1 0 3 3
bastion.lab.example.com.        504     IN      A       172.25.250.254
;rrset 86304 1 0 8 3
lab.example.com.        86304   IN      NS      bastion.lab.example.com.
END_RRSET_CACHE
START_MSG_CACHE
msg lab.example.com. IN NS 33152 1 504 3 1 0 1
lab.example.com. IN NS 0
bastion.lab.example.com. IN A 0
END_MSG_CACHE
EOF
[root@servera ]# unbound-control dump_cache > dns_dump
[root@servera ]#
```
`START_RRSET_CACHE 和 END_RRSET_CACHE` 之间的部分是存储在 unbound 缓存中的 `RRset 记录`，包括了 bastion.lab.example.com 域名的 A 记录和 lab.example.com 域名的 NS 记录。每个 `RRset` 记录包含了多个 `RRSIG` 记录，用于验证该 RRset 记录的真实性。

`START_MSG_CACHE 和 END_MSG_CACHE` 之间的部分则是存储在 unbound 缓存中的 DNS `消息`记录，包括了查询 lab.example.com 域名的 NS 记录的 DNS 消息记录。

```bash
[root@servera ]# dig  @servera.lab.example.com servera.lab.example.com
[root@servera ]# dig  @servera.lab.example.com serverb.lab.example.com
[root@servera ]# dig  @servera.lab.example.com serverc.lab.example.com
[root@servera ]#
```

查看缓存信息

```bash
[root@servera ]# unbound-control dump_cache
START_RRSET_CACHE
;rrset 502 1 0 8 3
serverb.lab.example.com.        502     IN      A       172.25.250.11
;rrset 280 1 0 3 3
bastion.lab.example.com.        280     IN      A       172.25.250.254
;rrset 490 1 0 8 3
servera.lab.example.com.        490     IN      A       172.25.250.10
;rrset 86080 1 0 8 3
lab.example.com.        86080   IN      NS      bastion.lab.example.com.
;rrset 511 1 0 8 3
serverc.lab.example.com.        511     IN      A       172.25.250.12
END_RRSET_CACHE
START_MSG_CACHE
msg serverb.lab.example.com. IN A 33152 1 502 3 1 1 1
serverb.lab.example.com. IN A 0
lab.example.com. IN NS 0
bastion.lab.example.com. IN A 0
msg lab.example.com. IN NS 33152 1 280 3 1 0 1
lab.example.com. IN NS 0
bastion.lab.example.com. IN A 0
msg servera.lab.example.com. IN A 33152 1 490 3 1 1 1
servera.lab.example.com. IN A 0
lab.example.com. IN NS 0
bastion.lab.example.com. IN A 0
msg serverc.lab.example.com. IN A 33152 1 511 3 1 1 1
serverc.lab.example.com. IN A 0
lab.example.com. IN NS 0
bastion.lab.example.com. IN A 0
END_MSG_CACHE
EOF
```

清除缓存
```bash
[root@servera ]# unbound-control flush_zone lab.example.com.
ok removed 5 rrsets, 4 messages and 0 key entries
[root@servera ]# unbound-control dump_cache
START_RRSET_CACHE
END_RRSET_CACHE
START_MSG_CACHE
END_MSG_CACHE
EOF
[root@servera ]#
```

缓存导出和导入

```bash
[root@servera ]# unbound-control load_cache < dns_dump
ok
[root@servera ]# unbound-control dump_cache
START_RRSET_CACHE
;rrset 457 1 0 3 3
bastion.lab.example.com.        457     IN      A       172.25.250.254
;rrset 86257 1 0 8 3
lab.example.com.        86257   IN      NS      bastion.lab.example.com.
END_RRSET_CACHE
START_MSG_CACHE
msg lab.example.com. IN NS 33152 1 457 3 1 0 1
lab.example.com. IN NS 0
bastion.lab.example.com. IN A 0
END_MSG_CACHE
EOF
[root@servera ]#
```



## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


<RH358 授课课堂笔记>

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
