---
title: 关于Linux中SSH会话关闭后台进程终止的一些笔记整理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-11-01 04:24:49/关于Linux中SSH会话关闭后台进程终止的一些笔记整理.html'
mathJax: false
date: 2022-11-01 12:24:49
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

systemd-logind是一个管理用户登录的系统服务。它负责：

跟踪用户和会话、他们的进程和他们的空闲状态。这是通过为下面的每个用户分配一个 systemd 切片单元`user.slice`，并为用户的每个并发会话分配一个作用域单元来实现的。user@.service此外，每个用户的服务管理器作为每个登录用户的系统服务实例启动 。

生成和管理会话 ID。如果审计可用并且已经为会话设置了审计会话 ID，则该 ID 将被重新用作会话 ID。否则，使用独立的会话计数器。
为用户提供基于polkit的访问权限，以进行系统关闭或睡眠等操作
为应用程序实现关闭/睡眠抑制逻辑
电源/睡眠硬件键的处理
多席位管理
会话切换管理
用户设备访问管理

在虚拟控制台激活和用户运行时目录管理时自动生成文本登录 (gettys)

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl cat  systemd-logind
# /usr/lib/systemd/system/systemd-logind.service
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

[Unit]
Description=Login Service
Documentation=man:systemd-logind.service(8) man:logind.conf(5)
Documentation=http://www.freedesktop.org/wiki/Software/systemd/logind
Documentation=http://www.freedesktop.org/wiki/Software/systemd/multiseat
Wants=user.slice
After=nss-user-lookup.target user.slice

# Ask for the dbus socket. If running over kdbus, the socket will
# not be actually used.
Wants=dbus.socket
After=dbus.socket

[Service]
ExecStart=/usr/lib/systemd/systemd-logind
Restart=always
RestartSec=0
BusName=org.freedesktop.login1
CapabilityBoundingSet=CAP_SYS_ADMIN CAP_MAC_ADMIN CAP_AUDIT_CONTROL CAP_CHOWN CAP_KILL CAP_DAC_READ_SEARCH CAP_DAC_OVERR
WatchdogSec=3min

# Increase the default a bit in order to allow many simultaneous
# logins since we keep one fd open per session.
LimitNOFILE=16384
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status  systemd-logind
● systemd-logind.service - Login Service
   Loaded: loaded (/usr/lib/systemd/system/systemd-logind.service; static; vendor preset: disabled)
   Active: active (running) since 日 2022-10-23 01:07:13 CST; 1 weeks 2 days ago
     Docs: man:systemd-logind.service(8)
           man:logind.conf(5)
           http://www.freedesktop.org/wiki/Software/systemd/logind
           http://www.freedesktop.org/wiki/Software/systemd/multiseat
 Main PID: 653 (systemd-logind)
   Status: "Processing requests..."
    Tasks: 1
   Memory: 952.0K
   CGroup: /system.slice/systemd-logind.service
           └─653 /usr/lib/systemd/systemd-logind

11月 01 11:48:20 liruilongs.github.io systemd-logind[653]: New session 1074 of user root.
11月 01 11:57:13 liruilongs.github.io systemd-logind[653]: New session 1076 of user root.
11月 01 11:57:38 liruilongs.github.io systemd-logind[653]: Removed session 1074.
Warning: Journal has been rotated since unit was started. Log output is incomplete or unavailable.
┌──[root@liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@liruilongs.github.io]-[/etc/systemd]
└─$ cat logind.conf
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
# Entries in this file show the compile time defaults.
# You can change settings by editing this file.
# Defaults can be restored by simply deleting this file.
#
# See logind.conf(5) for details.

[Login]
#NAutoVTs=6
#ReserveVT=6
#KillUserProcesses=no
#KillOnlyUsers=
#KillExcludeUsers=root
#InhibitDelayMaxSec=5
#HandlePowerKey=poweroff
#HandleSuspendKey=suspend
#HandleHibernateKey=hibernate
#HandleLidSwitch=suspend
#HandleLidSwitchDocked=ignore
#PowerKeyIgnoreInhibited=no
#SuspendKeyIgnoreInhibited=no
#HibernateKeyIgnoreInhibited=no
#LidSwitchIgnoreInhibited=yes
#IdleAction=ignore
#IdleActionSec=30min
#RuntimeDirectorySize=10%
#RemoveIPC=no
#UserTasksMax=
┌──[root@liruilongs.github.io]-[/etc/systemd]
└─$
```

+ `/etc/systemd/logind.conf`
+ `/etc/systemd/logind.conf.d/*.conf`
+ `/run/systemd/logind.conf.d/*.conf`
+ `/usr/lib/systemd/logind.conf.d/*.conf`


```bash
┌──[root@liruilongs.github.io]-[/etc/systemd]
└─$ cat logind.conf
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
# Entries in this file show the compile time defaults.
# You can change settings by editing this file.
# Defaults can be restored by simply deleting this file.
#
# See logind.conf(5) for details.

[Login]
#NAutoVTs=6
#ReserveVT=6
#KillUserProcesses=no
#KillOnlyUsers=
#KillExcludeUsers=root
#InhibitDelayMaxSec=5
#HandlePowerKey=poweroff
#HandleSuspendKey=suspend
#HandleHibernateKey=hibernate
#HandleLidSwitch=suspend
#HandleLidSwitchDocked=ignore
#PowerKeyIgnoreInhibited=no
#SuspendKeyIgnoreInhibited=no
#HibernateKeyIgnoreInhibited=no
#LidSwitchIgnoreInhibited=yes
#IdleAction=ignore
#IdleActionSec=30min
#RuntimeDirectorySize=10%
#RemoveIPC=no
#UserTasksMax=
```

```bash
┌──[root@liruilongs.github.io]-[/usr/lib/systemd]
└─$ man logind.conf | cat
LOGIND.CONF(5)                                       logind.conf                                       LOGIND.CONF(5)



NAME
       logind.conf, logind.conf.d - Login manager configuration files

SYNOPSIS
       /etc/systemd/logind.conf

       /etc/systemd/logind.conf.d/*.conf

       /run/systemd/logind.conf.d/*.conf

       /usr/lib/systemd/logind.conf.d/*.conf

DESCRIPTION
       These files configure various parameters of the systemd login manager, systemd-logind.service(8).

CONFIGURATION DIRECTORIES AND PRECEDENCE
       Default configuration is defined during compilation, so a configuration file is only needed when it is
       necessary to deviate from those defaults. By default the configuration file in /etc/systemd/ contains
       commented out entries showing the defaults as a guide to the administrator. This file can be edited to create
       local overrides.

       When packages need to customize the configuration, they can install configuration snippets in
       /usr/lib/systemd/*.conf.d/. Files in /etc/ are reserved for the local administrator, who may use this logic to
       override the configuration files installed by vendor packages. The main configuration file is read before any
       of the configuration directories, and has the lowest precedence; entries in a file in any configuration
       directory override entries in the single configuration file. Files in the *.conf.d/ configuration
       subdirectories are sorted by their filename in lexicographic order, regardless of which of the subdirectories
       they reside in. If multiple files specify the same option, the entry in the file with the lexicographically
       latest name takes precedence. It is recommended to prefix all filenames in those subdirectories with a
       two-digit number and a dash, to simplify the ordering of the files.

       To disable a configuration file supplied by the vendor, the recommended way is to place a symlink to /dev/null
       in the configuration directory in /etc/, with the same filename as the vendor configuration file.

OPTIONS
       All options are configured in the "[Login]" section:

       NAutoVTs=
           Takes a positive integer. Configures how many virtual terminals (VTs) to allocate by default that, when
           switched to and are previously unused, "autovt" services are automatically spawned on. These services are
           instantiated from the template unit autovt@.service for the respective VT TTY name, for example,
           autovt@tty4.service. By default, autovt@.service is linked to getty@.service. In other words, login
           prompts are started dynamically as the user switches to unused virtual terminals. Hence, this parameter
           controls how many login "gettys" are available on the VTs. If a VT is already used by some other subsystem
           (for example, a graphical login), this kind of activation will not be attempted. Note that the VT
           configured in ReserveVT= is always subject to this kind of activation, even if it is not one of the VTs
           configured with the NAutoVTs= directive. Defaults to 6. When set to 0, automatic spawning of "autovt"
           services is disabled.

       ReserveVT=
           Takes a positive integer. Identifies one virtual terminal that shall unconditionally be reserved for
           autovt@.service activation (see above). The VT selected with this option will be marked busy
           unconditionally, so that no other subsystem will allocate it. This functionality is useful to ensure that,
           regardless of how many VTs are allocated by other subsystems, one login "getty" is always available.
           Defaults to 6 (in other words, there will always be a "getty" available on Alt-F6.). When set to 0, VT
           reservation is disabled.

       KillUserProcesses=
           Takes a boolean argument. Configures whether the processes of a user should be killed when the user
           completely logs out (i.e. after the user's last session ended). Defaults to "no".

           Note that setting KillUserProcesses=1 will break tools like screen(1).

       KillOnlyUsers=, KillExcludeUsers=
           These settings take space-separated lists of usernames that influence the effect of KillUserProcesses=. If
           not empty, only processes of users listed in KillOnlyUsers= will be killed when they log out entirely.
           Processes of users listed in KillExcludeUsers= are excluded from being killed.  KillExcludeUsers= defaults
           to "root" and takes precedence over KillOnlyUsers=, which defaults to the empty list.

       IdleAction=
           Configures the action to take when the system is idle. Takes one of "ignore", "poweroff", "reboot",
           "halt", "kexec", "suspend", "hibernate", "hybrid-sleep", and "lock". Defaults to "ignore".

           Note that this requires that user sessions correctly report the idle status to the system. The system will
           execute the action after all sessions report that they are idle, no idle inhibitor lock is active, and
           subsequently, the time configured with IdleActionSec= (see below) has expired.

       IdleActionSec=
           Configures the delay after which the action configured in IdleAction= (see above) is taken after the
           system is idle.

       InhibitDelayMaxSec=
           Specifies the maximum time a system shutdown or sleep request is delayed due to an inhibitor lock of type
           "delay" being active before the inhibitor is ignored and the operation executes anyway. Defaults to 5.

       HandlePowerKey=, HandleSuspendKey=, HandleHibernateKey=, HandleLidSwitch=, HandleLidSwitchDocked=
           Controls whether logind shall handle the system power and sleep keys and the lid switch to trigger actions
           such as system power-off or suspend. Can be one of "ignore", "poweroff", "reboot", "halt", "kexec",
           "suspend", "hibernate", "hybrid-sleep", and "lock". If "ignore", logind will never handle these keys. If
           "lock", all running sessions will be screen-locked; otherwise, the specified action will be taken in the
           respective event. Only input devices with the "power-switch" udev tag will be watched for key/lid switch
           events.  HandlePowerKey= defaults to "poweroff".  HandleSuspendKey= and HandleLidSwitch= default to
           "suspend".  HandleLidSwitchDocked= defaults to "ignore".  HandleHibernateKey= defaults to "hibernate". If
           the system is inserted in a docking station, or if more than one display is connected, the action
           specified by HandleLidSwitchDocked= occurs; otherwise the HandleLidSwitch= action occurs.

       PowerKeyIgnoreInhibited=, SuspendKeyIgnoreInhibited=, HibernateKeyIgnoreInhibited=, LidSwitchIgnoreInhibited=
           Controls whether actions triggered by the power and sleep keys and the lid switch are subject to inhibitor
           locks. These settings take boolean arguments. If "no", the inhibitor locks taken by applications in order
           to block the requested operation are respected. If "yes", the requested operation is executed in any case.
           PowerKeyIgnoreInhibited=, SuspendKeyIgnoreInhibited= and HibernateKeyIgnoreInhibited= default to "no".
           LidSwitchIgnoreInhibited= defaults to "yes". This means that the lid switch does not respect suspend
           blockers by default, but the power and sleep keys do.

       RuntimeDirectorySize=
           Sets the size limit on the $XDG_RUNTIME_DIR runtime directory for each user who logs in. Takes a size in
           bytes, optionally suffixed with the usual K, G, M, and T suffixes, to the base 1024 (IEC). Alternatively,
           a numerical percentage suffixed by "%" may be specified, which sets the size limit relative to the amount
           of physical RAM. Defaults to 10%. Note that this size is a safety limit only. As each runtime directory is
           a tmpfs file system, it will only consume as much memory as is needed.

       UserTasksMax=
           Sets the maximum number of OS tasks each user may run concurrently. This controls the TasksMax= setting of
           the per-user slice unit, see systemd.resource-control(5) for details.

       RemoveIPC=
           Controls whether System V and POSIX IPC objects belonging to the user shall be removed when the user fully
           logs out. Takes a boolean argument. If enabled, the user may not consume IPC resources after the last of
           the user's sessions terminated. This covers System V semaphores, shared memory and message queues, as well
           as POSIX shared memory and message queues. Note that IPC objects of the root user are excluded from the
           effect of this setting. Defaults to "no".

SEE ALSO
       systemd(1), systemd-logind.service(8), loginctl(1), systemd-system.conf(5)



systemd 219                                                                                            LOGIND.CONF(5)
┌──[root@liruilongs.github.io]-[/usr/lib/systemd]
└─$
```

## 博文参考


