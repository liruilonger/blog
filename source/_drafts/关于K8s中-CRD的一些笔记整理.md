---
title: 关于K8s中 CRD的一些笔记整理
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-02-23 23:39:10/关于K8s中 CRD的一些笔记整理.html'
mathJax: false
date: 2023-02-24 07:39:10
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***



为什么我需要一个CRD?CRD代表自定义资源定义。这是一种创建自己的kubernetes资源的方法，其工作方式与kubernetes资源(如Pod或Deployment)相同。使用自定义资源，您可以将如何管理资源的知识放到kubernetes集群中，这是非常强大的。例如，Strimzi是kubernetes的一个CRD，用于管理和运行kubernetes上的kafka集群。通过将管理kafka集群的知识放到CRD控制器本身中，它消除了很多运行自己的kafka集群的问题。


您需要一个控制器(在集群上运行的容器中)来管理您的CRD资源。控制器将循环运行，以检查CRD中所描述的(期望的)是否与现实世界中的实际状态相匹配。例如，您希望容器A和B在集群上运行。如果A或B没有运行，控制器将为您创建它。这是一种在有弹性和灵活的分布式环境中管理所需内容的健壮方法。这个CRD被称为“Githook”。它定义了git webhook事件和构建管道。GitHook控制器将订阅webhook事件到git repo，当事件发生时，它将运行CRD中定义的构建管道。

 

 




## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
