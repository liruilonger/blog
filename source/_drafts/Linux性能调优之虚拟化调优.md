---
title: Linux性能调优之虚拟化调优(Virtualization tuned)
tags:
  - Linux
categories:
  - Linux
toc: true
recommend: 1
keywords: java
uniqueId: '2022-06-03 16:34:49/Linux性能调优之虚拟化调优.html'
mathJax: false
date: 2022-06-04 00:34:49
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

## 虚拟化调优配置文件
### 设置虚拟化调优配置文件




  
就像进程可以固定到特定的cpu一样，VM中的虚拟cpu也可以固定到hypervisor上的物理cpu。这可以用于增加缓存命中和/或手动平衡特定工作负载的cpu。可以在虚拟机管理器中使用Details屏幕上的Processor选项卡进行CPU固定，或者在VM的libvirtd xml配置中进行，或者在运行时使用virsh vcpupin VM-name virt-cpu physical-cpu进行固定。要查看当前的cpu分配情况，可以使用virsh vcpuinfo VM-name命令，如下所示:

 