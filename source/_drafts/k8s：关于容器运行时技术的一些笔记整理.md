---
title: k8s：关于容器运行时技术的一些笔记整理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-03-08 07:52:17/ k8s：关于容器运行时技术的一些笔记整理.html'
mathJax: false
date: 2023-03-08 15:52:17
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

---
title: K8s：容器运行时技术深度剖析笔记整理
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-02-25 05:38:15/华为云：容器运行时技术深度剖析笔记整理.html'
mathJax: false
date: 2023-02-25 13:38:15
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 博文内容为结合 `华为云云原生课程` 课堂笔记整理而来
+ 课程是免费的，有华为云账户就可以看，感觉很不错。
+ 有需要的小伙伴可以看看，链接在文末
+ 受版权问题影响，这里仅仅自己整理学习笔记
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***
## 容器运行时技术深度剖析


### 容器引擎和运行时机制原理剖析


这里为什么要从 CRI 讲起，是因为 k8s 集群使用 `kubelet` 通过 CRI 接口和对应的 runtime 交互，控制容器。

那 CRI 是什么？ CRI 是一个 Kubernetes API，它定义了 Kubernetes 与不同容器运行时交互的方式。因为它在规范中是标准化的，所以可以选择要使用的 CRI 实现或编写自己的实现。

![在这里插入图片描述](https://img-blog.csdnimg.cn/594af2c4be3c41bfb1180273ae5f4090.png)


#### CRI

在 `K8s`  生态中通过  `CRI`  接口来对 容器运行时进行管理，从而实现对容器镜像的管理，具体一点，通过 `kubelet` 调用容器运行时的 `grpc` 接口。

面向接口编程，类比 `Java` 中，操作数据库，使用  `JDBC API` 来连接不同的数据库实现 `CRUD`，这里具体的数据操作通过不同数据库的驱动包来实现。 对于 `CRI` 接口，有下面的一些实现。 


`dockershim`: 由于 `docker` 没有实现  CRI 接口，所以 `kubernetes` 在最初对接 `docker api`的时候提供了 `CRI`接口适配器, 但是这是一种冗余的行为，`k8s` 只是需要 `docker` 中的 `containerd`,完全可以直接使用 `containerd`,所以在之后版本中删除了这部分代码,  在 kubernetes 1.21 版本已经将其标注为废弃接口。在 1.24 版本中彻底移除了该部分代码, 不过 如果任然想使用 docker ,可以使用开源项目 `cri-docker` 来代替。在部署时需要单独指定 socket。

![在这里插入图片描述](https://img-blog.csdnimg.cn/8b0c9bca897344a395e62851ce80903d.png)

docker 的前置依赖，可以看到它依赖 `containerd.service`
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$systemctl list-dependencies docker | head -n 3
docker.service
● ├─containerd.service
● ├─docker.socket
```

`CRI-containerd`：通过 `containerd` 中的 `CRI插件实现了CRI接口`，让 containerd 可接对接 containerd 启动容器，无需调用 `docker api`。当前使用最广泛的 `CRI接口接口实现`。

`CRI-O`：专注于在 kubernetes 运行容器的轻量级 CRl 接口实现(不关注开发态)。



CRI 接口由两部分组成

面向对象角度考虑，通过 `RuntimeServiceServer` 属性来描述管理容器生命周期的相关行为，通过 `ImageServiceServer` 属性来描述管理镜像相关的行为。通过对这两个行为接口的委托，实现 CRI 接口。 


```go
type grpcServices interface {
  runtime.RuntimeServiceServer 
  runtime.ImageServiceServer
}
```
+ `运行时`：负责容器的`生命周期管理`，包括容器创建，启动、停止、日志和性能采集等接口。
+ `镜像`：负责容器`镜像的管理`，包括显示镜像、拉取镜像、删除镜像等接口。




### OCI

`OCI` 组织：Linux基金会于2015年6月成立OCI(Open Container Initiative)组织，旨在围绕`容器格式和运行时`制定一个开放的`工业化标准`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/fabcd344cdf6458f970733a5e4fae3d2.png)


目前主要有两个标准文档：`容器运行时标准(runtime spec)`和`容器镜像标准(image spec)`

`Runtime spec`：容器运行时标准，定义了容器状态和配置文件格式，容器生命周期管理命令格式和参数等。

`image spec`：镜像标准包，定义了容器镜像的文件系统、config 文件、manifest 文件、index 文件等。



容器标准中包含：

容器命令：容器生命周期管理命令、包括创建、启动、停止、删除等。

```bash
#runc create <container id>
#runc start <container id>#runc state <container id>
#runc kill <container-id><signal>
#runc delete <container-id>
```

`config.json`：定义容器运行所需要的所有信息，包括rootfs、mounts、进程、cgroups、namespaces、caps等。



### 主流的容器运行时：

这里需要说明一下，我们常讲的 容器运行时是一个 混合概念，包含低级别容器运行时，和高级别容器运行时，比如 `runc lxc gvisor kata`这些，只能管理容器，不能管理镜像，他们被称为 低级别运行时。

高级别运行时比如 docker podman containerd ..，用来调用管理低级别运行时 runc 等，即能管理容器，也能管理镜像


#### runc

runc: docker 捐献给 OCI 社区的一个 runtime spec 的参考实现，docker 容器也是基于 runc 创建的。


![在这里插入图片描述](https://img-blog.csdnimg.cn/92f604db3f5c4d989089d2a81139f6e8.png)

利用 Linux 内核特性，来隔离进程：

+ Namespace：资源和信息的可见性隔离，通过namespace隔离，容器中的应用只能看到分配到该容器的资源、其他主机上的信息在容器中不可见。常用的namespace有PID(进程号)、MNT(挂载点)、NET(网络)、UTS(主机名)和IPC(跨进程通信)等
+ Cgroups：资源使用量的隔离，通过cgroup、限制了容器使用的资源量，通过不同的子系统，限制不同的资源。包括CPU、内存、io带宽、大页、fd数等等
+ Capability：权限限制，通过对进程的capability定义，限制容器中的进程调用某些系统调用，以达到容器进程无法逃逸到主机的目的，比如容器中的进程是不具有以下capability的： SYS ADMIN/MKNOD/SYS RESOURCE/SYS MODULES.…


容器主进程

```bash
     1   1898   1898   1034 ?            -1 Sl       0   0:34 /usr/bin/containerd-shim-runc-v2 -namespace moby -id 057fb
  1898   1957   1957   1957 ?            -1 Ss   65535   0:00  \_ /pause
     1   2013   2013   1034 ?            -1 Sl       0   2:58 /usr/bin/containerd-shim-runc-v2 -namespace moby -id 7d167
  2013   2080   2080   2080 ?            -1 Ss       0   0:17  \_ haproxy -W -db -f /usr/local/etc/haproxy/haproxy.cfg
  2080   2133   2080   2080 ?            -1 Sl       0  48:48      \_ haproxy -W -db -f /usr/local/etc/haproxy/haproxy.c
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/94073d91de1348ddb397d246459fc1b6.png)


#### Kata-runtime

![在这里插入图片描述](https://img-blog.csdnimg.cn/bdb5399aaddb4a13908ffcf0830d2266.png)


Kata-runtime：一种基于 `虚拟化` 的安全隔离的 `OCI runtime spec` 的实现。

1. 虚拟化隔离：每个pod都运行在一个独立的`虚拟机`中，提供虚拟化接口对接不同的虚拟化实现，包括qemu、cloud hypervisor、firecracker 等等
2. 轻量化：为了达到和容器近似的使用体验，需要对各组件进行裁剪，达到轻量化和启动加速的目的，对于hypervisor，去除通用虚拟化的各种不必要的设备、总线等。对于guestkernel，也裁剪了大量不需要的驱动和文件系统模块。而运行在虚拟机中的1号进程(一般为kata-agent)，资源占用可小于1MB。
3. 主机资源访问：通过virtio、vfio等方式访问主机资源，如virtio-blk(块设备)、virtio-fs(文件)、virtio-net(网络)、vfio(物理设备)、vhost-user(用户态网络或存储)


#### gVisor



gVisor：一种基于`系统调用拦截技术`的轻量级安全容器实现。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2894f4efe43c4145b0a681692cdabbcb.png)


1. 虚拟内核：设置进程的4种模式，HRO、HR3、GRO、GR3，通过拦截系统调用，实现了一个虚拟内核，用户进程与host kernel不直接交互
2. 拦截系统调用的方式：ptrace、kvm
3. 优点：额外内存消耗小，容器启动速度快
4. 缺点：系统调用慢，导致I0、网络等性能差，由于是模拟内核，有POSIX兼容性问题





#### runtime V2

![在这里插入图片描述](https://img-blog.csdnimg.cn/1931cee524904af99079c47b49dd1f04.png)

shimv2：新的容器运行时接口，基于`ttrpc`通信。

目的：让运行时更方便维护容器状态和生命周期，减少安全容器实现中，节点的进程数和资源调用。

#### RuntimeClass

`RuntimeClass`:kubernetes中的对象类型，定义了在集群中的某种运行时，并且可以通过overhead和nodeSelector定制某种运行时的资源和调度行为。

`Runtime Plugin`:containerd中的runtime插件配置，定义了runtime名称、二进制路径、传递的annotation、特权容器模式等等。
`runtimeClassName`:pod的中的字段，通过该字段决定用那种运行时启动容器。

![在这里插入图片描述](https://img-blog.csdnimg.cn/d5b57cb2731e41d2ae2cae0addfd66aa.png)








## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


https://connect.huaweicloud.com/courses/learn/learning/sp:cloudEdu_?courseNo=course-v1:HuaweiX+CBUCNXI041+Self-paced&courseType=1&source=1

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)







## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
