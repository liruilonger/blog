---
title: 《Linux性能优化》中文版 读书笔记
tags:
  - Linux
  - 性能优化
categories:
  - Linux
toc: true
recommend: 1
keywords: Linux
uniqueId: '2022-01-12 18:45:07/《Linux性能优化》中文版 读书笔记.html'
mathJax: false
date: 2022-01-13 02:45:07
thumbnail:
---
**<font color="009688"> 不管是牧羊人、海员、还是推销员,总会有一个地方令他们魂奉梦萦,那里会有一个人让他们忘记自由自在周游世界的快乐--------保罗·科埃略《牧羊少年奇幻之旅》**</font>

<!-- more -->
## 写在前面**
*****
+ 博文内容大多来源于`《Linux性能优化》`中文版一书，感兴趣小伙伴可以支持作者一波
+ 个别地方感觉有些拗口，一些愚建，比如`第二章，性能工具：系统CUP`，系统CUP属于硬件，为什么加一个性能工具，也可能原本就是这样写的。
+ 博文涉及内容：
  + CUP监控信息统计：即sar，vmstat，mpstat，top中关于CUP 的统计方式

**<font color=tomato>另： 书很不错，感兴趣小伙伴可以支持译者一波</font>**

**<font color="009688"> 不管是牧羊人、海员、还是推销员,总会有一个地方令他们魂奉梦萦,那里会有一个人让他们忘记自由自在周游世界的快乐--------保罗·科埃略《牧羊少年奇幻之旅》**</font>


***


# <font color=seagreen>第2章性能工具:系统CPU</font>

## <font color=seagreen>2.1 CPU性能统计信息</font>

### <font color=seagreen>2.1.1运行队列统计</font>

>在Linux中,一个进程有可运行的,阻塞的(正在等待一个事件的完成)两种情况。阻塞进程可能在等待的是从I/O设备来的数据,或者是系统调用的结果。

如果进程是可运行的,那就意味着它要和其他也是可运行的进程竞争CPU时间。一个可运行的进程不一定会使用CPU,但是当`Linux调度器决定下一个要运行的进程时,它会从可运行进程队列中挑选`。**<font color=red>如果进程是可运行的,同时又在等待使用处理器,这些进程就构成了运行队列。运行队列越长,处于等待状态的进程就越多</font>** 性能工具通常会给出可运行的进程个数和等待I/O的阻塞进程个数。

#### <font color=brown>平均负载</font>

**<font color=blue>系统的负载是指正在运行和可运行的进程总数</font>**。比如,如果正在运行的进程为两个,而可运行的进程为三个,那么系统负载就是5,平均负载是给定时间内的负载量。

### <font color=blue>2.1.2上下文切换</font>

制造出给定单处理器同时运行多个任务的假象, **<font color=purple>Linux内核就要不断地在不同的进程间切换。这种不同进程间的切换称为上下文切换</font>** 

上下文切换时, CPU要保存旧进程的所有上下文信息,并取出新进程的所有上下文信息。上下文中包含了Linux跟踪新进程的大量信息,其中包括: **<font color=yellowgreen>进程正在执行的指令,分配给进程的内存,进程打开的文件等</font>**

这些上下文切换涉及大量信息的移动,因此,**<font color=tomato>上下文切换的开销可以是相当大的</font>**。

**<font color=amber>上下文切换可以是内核调度的结果。为了保证公平地给每个进程分配处理器时间,内核周期性地中断正在运行的进程,</font>** 在适当的情况下,内核调度器会决定开始另一个进程,而不是让当前进程继续执行。每次这种`周期性中断或定时发生时`,系统都可能进行上下文切换。`每秒定时中断的次数与架构和内核版本有关`。

**<font color=royalblue>一个检查中断频率的简单方法是用`/proc/interrupts`文件,它可以确定`已知时长内发生的中断次数`。</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$cat /proc/interrupts  | grep time; sleep 5 ;cat /proc/interrupts | grep time
   0:        337          0   IO-APIC-edge      timer
 LOC:    9896498    9871317   Local timer interrupts
   0:        337          0   IO-APIC-edge      timer
 LOC:    9901529    9876213   Local timer interrupts
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
上面定时器的启动频率为(9896498-9901529)/5 =1000,即每秒要终端1000次，同时也可以理解为内核为sleep进程分配了1000次CUP，

>如果你的上下文切换明显多于定时器中断,那么这些切换极有可能是由I/O请求或其他长时间运行的系统调用(如休眠)造成的。当应用请求的操作不能立即完成时,内核启动该操作,保存请求进程,并尝试切换到另一个已就绪进程。这能让处理器尽量保持忙状态。


### <font color=camel> 2.1.3中断</font>

**<font color=yellowgreen>处理器还周期性地从硬件设备接收中断。当设备有事件需要内核处理时,它通常就会触发这些中断。</font>**

>比如,如果磁盘控制器刚刚完成从驱动器取数据块的操作,并准备好提供给内核,那么磁盘控制器就会触发一个中断。对内核收到的每个中断,如果已经有相应的已注册的中断处理程序,就运行该程序,否则将忽略这个中断。

**<font color=camel>中断处理程序在系统中具有很高的运行优先级,并且通常执行速度也很快</font>**。查看`/proc/interrupts`文件可以显示出哪些CPU上触发了哪些中断。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$cat /proc/interrupts
            CPU0       CPU1
   0:        337          0   IO-APIC-edge      timer
   1:         10          0   IO-APIC-edge      i8042
   8:          1          0   IO-APIC-edge      rtc0
   9:          0          0   IO-APIC-fasteoi   acpi
  12:         16          0   IO-APIC-edge      i8042
  14:          0          0   IO-APIC-edge      ata_piix
  15:          0          0   IO-APIC-edge      ata_piix
  17:      57939          0   IO-APIC-fasteoi   ioc0
  18:         14       9800   IO-APIC-fasteoi   ens32
  24:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  25:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  26:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  27:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  28:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  29:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  30:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  31:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  32:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  33:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  34:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  35:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  36:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  37:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  38:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  39:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  40:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  41:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  42:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  43:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  44:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  45:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  46:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  47:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  48:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  49:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  50:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  51:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  52:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  53:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  54:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  55:          0          0   PCI-MSI-edge      PCIe PME, pciehp
  56:          0          0   PCI-MSI-edge      0000:02:02.0
  57:       1447          0   PCI-MSI-edge      vmw_vmci
  58:          0          0   PCI-MSI-edge      vmw_vmci
 NMI:        325        328   Non-maskable interrupts
 LOC:   10545138   10518297   Local timer interrupts
 SPU:          0          0   Spurious interrupts
 PMI:        325        328   Performance monitoring interrupts
 IWI:      83868      83296   IRQ work interrupts
 RTR:          0          0   APIC ICR read retries
 RES:    3385530    3378243   Rescheduling interrupts
 CAL:       7225       7533   Function call interrupts
 TLB:     136855     136473   TLB shootdowns
 TRM:          0          0   Thermal event interrupts
 THR:          0          0   Threshold APIC interrupts
 DFR:          0          0   Deferred Error APIC interrupts
 MCE:          0          0   Machine check exceptions
 MCP:         37         37   Machine check polls
 ERR:          0
 MIS:          0
 PIN:          0          0   Posted-interrupt notification event
 PIW:          0          0   Posted-interrupt wakeup event
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
### <font color=orange>2.1.4 CPU使用率</font>

|<font color=chocolate>在任何给定的时间, CPU可以执行以下七件事情中的一个</font>|
|--|
|CPU可以是`空闲的`,这意味着处理器实际上没有做任何工作,并且等待有任务可以执行。|
|CPU可以`运行用户代码`,即指定的“用户”时间。|
|CPU可以`执行Linux内核中的应用程序代码`,这就是“系统”时间。|
|CPU可以`执行“比较友好”的或者优先级被设置为低于一般进程的用户代码`。|
|CPU可以`处于iowait状态`,即系统正在等待10 (如磁盘或网络)完成。|
|CPU可以`处于irq状态`,即它正在用高优先级代码处理硬件中断。|
|CPU可以`处于softirq模式`,即系统正在执行同样由中断触发的内核代码,只不过其运行于较低优先级(下半部代码)|

一个具有 **<font color=purple>高“系统”百分比</font>** 的系统表明其大部分时间都消耗在了内核上。像oprofile一样的工具可以帮助确定时间都消耗在了哪里。具有 **<font color=camel>高“用户”时间</font>** 的系统则将其大部分时间都用来运行应用程序。

## <font color=royalblue>2.2 Linux性能工具: CPU </font>

### <font color=amber>2.2.1 vmstat (虚拟内存统计)</font>

`vmstat`是一个很有用的命令,它能获取整个系统性能的粗略信息,包括:
+ 正在运行的进程个数
+ CPU的使用情况。
+ CPU接收的中断个数。
+ 调度器执行的上下文切换次数

```bash
┌──(liruilong㉿Liruilong)-[/mnt/c/Users/lenovo]
└─$ vmstat -help

Usage:
 vmstat [options] [delay [count]]

Options:
 -a, --active           active/inactive memory
 -f, --forks            number of forks since boot
 -m, --slabs            slabinfo
 -n, --one-header       do not redisplay header
 -s, --stats            event counter statistics
 -d, --disk             disk statistics
 -D, --disk-sum         summarize disk statistics
 -p, --partition <dev>  partition specific statistics
 -S, --unit <char>      define display unit
 -w, --wide             wide output
 -t, --timestamp        show timestamp

 -h, --help     display this help and exit
 -V, --version  output version information and exit

For more details see vmstat(8).

┌──(liruilong㉿Liruilong)-[/mnt/c/Users/lenovo]
└─$
```

#### <font color=seagreen>CPU性能相关的选项</font>

```bash
$vmstat [-n] [-s] [delay [count ]]
```
`vmstat`运行于两种模式:`采样模式`和`平均模式`。如果`不指定参数`,则vmstat统计运行于`平均模式`下.

>vmstat显示从系统启动以来所有统计数据的均值。但是,如果指定了延迟,那么第一个采样仍然是系统启动以来的均值,但之后vmstat按延迟秒数采样系统并显示统计数据。

|参数|
|--|
|![《Linux性能优化_[美] 菲利普G.伊佐特》](https://img-blog.csdnimg.cn/cab20b15858d411784b20260cac58851.png)|

**<font color=purple>-n 参数的区别</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$vmstat 1
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 2  0      0 1064772   2072 1764796    0    0    60    26 1501  832  8  4 87  1  0
 0  0      0 1064964   2072 1764800    0    0     0    70 2993 4939 19  8 73  0  0
 0  0      0 1064400   2072 1764800    0    0     0     0 2832 6916 24 10 66  0  0
..............
 0  0      0 1064156   2072 1764952    0    0     0    32 2449 3900  3  2 95  0  0
 0  0      0 1064196   2072 1764860    0    0     0    16 2647 4124  6  2 91  0  0
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 4  0      0 1058828   2072 1764860    0    0     0    24 3121 5182  8  4 88  0  0
 1  0      0 1064608   2072 1764820    0    0     0     0 3070 5297 10  6 83  0  0
 0  0      0 1064608   2072 1764820    0    0     0    32 2674 4136  3  2 95  0  0
 0  0      0 1064296   2072 1764820    0    0     0     0 2560 3943  3  1 95  0  0
^C
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$vmstat -n  1
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 1  0      0 1063952   2072 1764808    0    0    59    26 1501  838  8  4 87  1  0
 7  0      0 1064472   2072 1764812    0    0     0    38 2869 4611  9  4 87  0  0
 2  0      0 1063476   2072 1764876    0    0     0    28 3467 8176 31 15 54  0  0
 0  0      0 1063476   2072 1764876    0    0     0     0 2482 3629 14  2 84  0  0
 0  0      0 1063476   2072 1764876    0    0     0    28 2459 3691  3  2 96  0  0
 0  0      0 1063368   2072 1764864    0    0     0    36 2691 4061  5  1 94  0  0
 0  0      0 1062864   2072 1764864    0    0     0    44 2714 4420  6  2 92  0  0
 0  0      0 1063104   2072 1764864    0    0     0    32 2963 4514  8  3 88  0  0
............
 1  0      0 1063868   2072 1765000    0    0     0   107 2657 6989 33 14 52  0  0
 0  0      0 1063868   2072 1765000    0    0     0     0 2830 4315  6  3 91  0  0
 0  0      0 1063868   2072 1765000    0    0     0    28 2430 3908  5  3 92  0  0
 0  0      0 1063868   2072 1765000    0    0     0     0 2358 3551  2  2 96  0  0
.........
 1  0      0 1064136   2072 1765112    0    0     0     0 2736 4000  6  3 91  0  0
 4  0      0 1063764   2072 1765112    0    0     0    48 2492 3779  5  2 93  1  0
 0  0      0 1063484   2072 1765112    0    0     0     0 2650 4071  4  2 94  0  0
 0  0      0 1063304   2072 1765116    0    0     0    99 2727 4430  7  3 90  0  0
 0  0      0 1063392   2072 1765116    0    0     0    12 2554 3811  2  1 97  0  0
 0  0      0 1064012   2072 1765120    0    0     0    48 2585 4291 14  5 81  0  0
 0  0      0 1064136   2072 1765120    0    0     0     0 2576 4057  4  2 94  0  0
^C
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
**<font color=chocolate>-s 的统计</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$vmstat -s
      4030172 K total memory
      1199020 K used memory
      1277200 K active memory
      1428408 K inactive memory
      1064028 K free memory
         2072 K buffer memory
      1765052 K swap cache
            0 K total swap
            0 K used swap
            0 K free swap
       206387 non-nice user cpu ticks
            3 nice user cpu ticks
       100381 system cpu ticks
      2184710 idle cpu ticks
        12604 IO-wait cpu ticks
            0 IRQ cpu ticks
         4699 softirq cpu ticks
            0 stolen cpu ticks
      1473622 pages paged in
       652550 pages paged out
            0 pages swapped in
            0 pages swapped out
     37633050 interrupts
     64361523 CPU context switches
   1642162657 boot time
       210962 forks
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
**<font color=brown>CUP相关的列名含义</font>**

|CUP相关的列名含义|
|--|
|![《Linux性能优化_[美] 菲利普G.伊佐特》](https://img-blog.csdnimg.cn/9ed5b50d459b46cfad57e32bc8eb417a.png)|

>`vmstat`的开销很低,可以让它在控制台上或窗口中持续运行,甚至是在负载非常重的服务器上是很实用的。


```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$vmstat
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 2  0      0 1057540   2072 1761012    0    0    68    27 1501  595  8  4 87  1  0
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
**<font color=tomato>查看CUP平均值，当前系统中，可运行进程为2(r)，被阻塞的进程为0(b)，系统发生中断次数1501(in),系统发生上下文切换次数为595(cs)，系统代码消耗CPU为4%(sy)，用户代码消耗CPU为8%(us)，系统空闲占比87%(id),剩余1%属于等待IO消耗的CUP空闲状态(wa)，</font>**

>**<font color=blue>上下文切换的数量小于中断的数量。调度器切换进程的次数少于定时器中断触发的次数</font>** 。这很可能是因为系统基本上是空闲的,**<font color=green>在定时器中断触发的大多数时候,调度器没有任何工作要做,因此它也不需要从空闲进程切换出去。</font>**

**<font color=brown>CPU 统计的含义</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$vmstat -s
    ..........
       206387 non-nice user cpu ticks
            3 nice user cpu ticks
       100381 system cpu ticks
      2184710 idle cpu ticks
        12604 IO-wait cpu ticks
            0 IRQ cpu ticks
         4699 softirq cpu ticks
            0 stolen cpu ticks
    ............
     37633050 interrupts
     64361523 CPU context switches
    ...........
       210962 forks
```
这里的ticks为一个时间单位，相关数据为自系统启动时间以来的数据。`forks`表示系统创建以来的进程数，`CPU context switches`表示上下文文切换次数，`interrupts`即中断次数，剩下的参数对应上面的列理解即可，stolen cpu 这个不太理解

### <font color=brown>top(3.0.X 版本)</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$top -help
  procps-ng version 3.3.10
Usage:
  top -hv | -bcHiOSs -d secs -n max -u|U user -p pid(s) -o field -w [cols]
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
**<font color=yellowgreen>top呈现为一个降序列表,排在最前面的是最占用CPU的进程。</font>**

#### <font color=tomato>CPU性能相关的选项</font>
```bash
$top [-d delay] [-n iter] [-i] [-b]
```
>top实际有两种模式的选项:`命令行选项`和`运行时选项`。
##### <font color=brown>命令行选项决定top如何显示其信息。</font>
|命令行选项|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/cb95a5a7bd2c4abfb57a28e3d02f03e5.png)|

**<font color=camel>下面问间隔3s统计2次的top信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$top -d 3 -n 2
top - 09:26:22 up 13:08,  1 user,  load average: 0.56, 0.48, 0.55
Tasks: 215 total,   1 running, 214 sleeping,   0 stopped,   0 zombie
%Cpu(s):  9.9 us,  5.5 sy,  0.0 ni, 84.3 id,  0.2 wa,  0.0 hi,  0.2 si,  0.0 st
KiB Mem :  4030172 total,  1079708 free,  1204872 used,  1745592 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  2470484 avail Mem

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
  2195 root      20   0 1174904 380988  47392 S  10.0  9.5  95:15.71 kube-apiserver
   963 root      20   0 1777988 142376  59968 S   5.6  3.5  41:04.34 kubelet
  2204 root      20   0  823728  98312  36136 S   5.6  2.4  47:06.26 kube-controller
  4348 root      20   0 1657820  47488  21052 S   5.3  1.2  42:28.29 calico-node
  2266 root      20   0 10.696g  77632  19904 S   3.0  1.9  23:51.16 etcd
  1121 root      20   0 1369820  85076  28388 S   2.7  2.1  15:55.04 dockerd
   973 root      20   0 1099192  43512  16860 S   0.7  1.1   4:32.81 containerd
  3057 root      20   0  737756  29904  12424 S   0.7  0.7   3:34.82 speaker
  3564 root      20   0  713096  15152   4312 S   0.7  0.4   1:50.31 containerd-shim
  4313 root      20   0  713096  15596   4256 S   0.7  0.4   1:25.70 containerd-shim
     9 root      20   0       0      0      0 S   0.3  0.0   2:05.06 rcu_sched
    13 root      20   0       0      0      0 S   0.3  0.0   0:07.26 ksoftirqd/1
  2214 root      20   0  754268  41856  19352 S   0.3  1.0   4:25.88 kube-scheduler
  3774 root      20   0  751232  34032  15932 S   0.3  0.8   1:25.77 coredns
  4261 root      20   0  751488  30832  15880 S   0.3  0.8   1:30.06 coredns
  4369 polkitd   20   0  745532  33232  14124 S   0.3  0.8   0:21.00 kube-controller
     1 root      20   0  191024   4028   2516 S   0.0  0.1   0:47.38 systemd
     2 root      20   0       0      0      0 S   0.0  0.0   0:00.05 kthreadd
     3 root      20   0       0      0      0 S   0.0  0.0   0:07.85 ksoftirqd/0
     4 root      20   0       0      0      0 S   0.0  0.0   0:06.99 kworker/0:0
.......
```
**<font color=blue>下面为只显示非空闲进程</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$top -i
top - 09:35:45 up 13:18,  1 user,  load average: 0.17, 0.45, 0.53
Tasks: 216 total,   3 running, 213 sleeping,   0 stopped,   0 zombie
%Cpu(s):  7.4 us,  3.2 sy,  0.0 ni, 89.1 id,  0.0 wa,  0.0 hi,  0.2 si,  0.0 st
KiB Mem :  4030172 total,  1070632 free,  1213464 used,  1746076 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  2461980 avail Mem

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
  2195 root      20   0 1174904 380988  47392 S  14.3  9.5  96:25.19 kube-apiserver
  2204 root      20   0  823728  98312  36136 S   6.3  2.4  47:40.78 kube-controller
  4348 root      20   0 1657820  47516  21052 S   5.3  1.2  42:59.36 calico-node
   963 root      20   0 1777988 141712  59968 S   4.3  3.5  41:34.55 kubelet
  2266 root      20   0 10.696g  78684  19904 S   3.3  2.0  24:08.17 etcd
 40370 root      20   0  745236  17168   8388 R   2.3  0.4   0:00.07 calico
  1121 root      20   0 1369820  85496  28388 S   1.7  2.1  16:06.78 dockerd
  2214 root      20   0  754268  41856  19352 S   0.7  1.0   4:28.98 kube-scheduler
  3057 root      20   0  737756  30136  12424 S   0.7  0.7   3:37.40 speaker
     9 root      20   0       0      0      0 S   0.3  0.0   2:06.53 rcu_sched
  3023 root      20   0  713096  12892   3636 S   0.3  0.3   0:05.56 containerd-shim


┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
**<font color=orange>-b 显示全部的进程，默认只显示当前的页能容纳的进程</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$top -d 3 -n 1 -b
top - 09:31:42 up 13:14,  1 user,  load average: 1.21, 0.70, 0.61
Tasks: 215 total,   1 running, 214 sleeping,   0 stopped,   0 zombie
%Cpu(s):  3.6 us,  3.6 sy,  0.0 ni, 92.9 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  4030172 total,  1078040 free,  1206028 used,  1746104 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  2469136 avail Mem

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
   963 root      20   0 1777988 141592  59968 S  12.5  3.5  41:21.46 kubelet
 36906 root      20   0  157716   2124   1496 R  12.5  0.1   0:00.02 top
  2195 root      20   0 1174904 380988  47392 S   6.2  9.5  95:55.13 kube-apiserver
  2266 root      20   0 10.696g  78684  19904 S   6.2  2.0  24:00.85 etcd
  3057 root      20   0  737756  30460  12424 S   6.2  0.8   3:36.32 speaker
  4348 root      20   0 1657820  47304  21052 S   6.2  1.2  42:45.70 calico-node
     1 root      20   0  191024   4028   2516 S   0.0  0.1   0:47.68 systemd
     2 root      20   0       0      0      0 S   0.0  0.0   0:00.05 kthreadd
     3 root      20   0       0      0      0 S   0.0  0.0   0:07.90 ksoftirqd/0
     4 root      20   0       0      0      0 S   0.0  0.0   0:07.03 kworker/0:0
    ........
    31 root      20   0       0      0      0 S   0.0  0.0   0:00.00 kswapd0
    32 root      25   5       0      0      0 S   0.0  0.0   0:00.00 ksmd
    33 root      39  19       0      0      0 S   0.0  0.0   0:01.02 khugepaged
    34 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 crypto
    42 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kthrotld
    44 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kmpath_rdacd
    45 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kpsmoused
    46 root      20   0       0      0      0 S   0.0  0.0   0:00.00 kworker/0:2
    ........
   259 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 scsi_tmf_4
   260 root      20   0       0      0      0 S   0.0  0.0   0:00.00 scsi_eh_6
   261 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 scsi_tmf_6
   262 root      20   0       0      0      0 S   0.0  0.0   0:00.00 scsi_eh_7
   .........
   273 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 scsi_tmf_12
   274 root      20   0       0      0      0 S   0.0  0.0   0:00.00 scsi_eh_13
   275 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 scsi_tmf_13
   276 root      20   0       0      0      0 S   0.0  0.0   0:00.00 scsi_eh_14
   277 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 scsi_tmf_14
   278 root      20   0       0      0      0 S   0.0  0.0   0:00.00 scsi_eh_15
   279 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 scsi_tmf_15
  ...........
  4297 root      20   0    4228    400    328 S   0.0  0.0   0:00.00 runsv
  4313 root      20   0  713096  15340   4256 S   0.0  0.4   1:26.26 containerd-shim
  4344 root      20   0 1141184  35748  16600 S   0.0  0.9   0:08.70 calico-node
  4345 root      20   0 1583576  39300  18152 S   0.0  1.0   0:08.09 calico-node
  4346 root      20   0 1141184  36808  17568 S   0.0  0.9   0:11.15 calico-node
  4369 polkitd   20   0  745532  33232  14124 S   0.0  0.8   0:21.13 kube-controller
  4526 root      20   0     756    260    196 S   0.0  0.0   0:24.63 bird6
  4534 root      20   0     772    520    340 S   0.0  0.0   0:24.92 bird
 14142 root      20   0       0      0      0 S   0.0  0.0   0:00.00 kworker/u256:1
 21599 root      20   0       0      0      0 S   0.0  0.0   0:00.21 kworker/1:1
 26382 root      20   0       0      0      0 S   0.0  0.0   0:00.49 kworker/1:0
 35491 root      20   0       0      0      0 S   0.0  0.0   0:00.00 kworker/1:2
 77429 root      20   0  148316   5532   4232 S   0.0  0.1   0:00.50 sshd
 77553 root      20   0  120900   7556   1704 S   0.0  0.2   0:00.18 bash
 83235 root      20   0       0      0      0 S   0.0  0.0   0:01.76 kworker/u256:0
 87183 postfix   20   0   89648   4024   3012 S   0.0  0.1   0:00.01 pickup
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```


##### <font color=purple>运行时top</font>

|运行时top|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/b038f4ed3706400a8a01adb35ec26f3e.png)|


**<font color=royalblue>输入 A 后，显示为各个系统资源的最大消耗者，这个看不太懂，以后再研究</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$top -i
1:Def - 09:40:01 up 13:22,  1 user,  load average: 0.17, 0.35, 0.47
Tasks: 215 total,   1 running, 214 sleeping,   0 stopped,   0 zombie
%Cpu(s): 11.6 us,  6.6 sy,  0.0 ni, 81.6 id,  0.0 wa,  0.0 hi,  0.2 si,  0.0 st
KiB Mem :  4030172 total,  1076864 free,  1206808 used,  1746500 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  2468492 avail Mem

1   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
   2204 root      20   0  823728  98312  36136 S   6.3  2.4  47:55.47 kube-controller
   4348 root      20   0 1657820  47444  21052 S   5.3  1.2  43:12.72 calico-node
   1121 root      20   0 1369820  85496  28388 S   2.6  2.1  16:11.81 dockerd
   2266 root      20   0 10.696g  78948  19904 S   2.6  2.0  24:15.45 etcd
    973 root      20   0 1099192  43784  16860 S   1.0  1.1   4:37.63 containerd
   2214 root      20   0  754268  41856  19352 S   0.7  1.0   4:30.34 kube-scheduler
      1 root      20   0  191024   4028   2516 S   0.3  0.1   0:48.12 systemd
2   PID   PPID     TIME+  %CPU %MEM  PR  NI S    VIRT    RES   UID COMMAND
  83235      2   0:01.82   0.0  0.0  20   0 S       0      0     0 kworker/u256:0
  77553  77429   0:00.20   0.0  0.2  20   0 S  120900   7556     0 bash
  77429    966   0:00.71   0.0  0.1  20   0 S  148316   5532     0 sshd
  43368   1085   0:00.02   0.0  0.1  20   0 S   89648   4024    89 pickup
  40247      2   0:00.17   0.3  0.0  20   0 S       0      0     0 kworker/1:1
  40239  77553   0:01.26   0.3  0.1  20   0 R  157720   2332     0 top                                      35491      2   0:00.00   0.0  0.0  20   0 S       0      0     0 kworker/1:2
3   PID %MEM    VIRT    RES   CODE    DATA    SHR nMaj nDRT  %CPU COMMAND
   2195  9.5 1174904 380988  60052 1052908  47392   33    0   8.9 kube-apiserver
    963  3.5 1778244 141228  88848 1618204  59968  520    0   7.0 kubelet
   2204  2.4  823728  98312  56280  708052  36136   33    0   6.3 kube-controller
   1121  2.1 1369820  85496  57240 1239752  28388   34    0   2.6 dockerd
   2266  2.0 10.696g  78948  11824  706828  19904    6    0   2.6 etcd
   4348  1.2 1657820  47444  30688 1590736  21052    0    0   5.3 calico-node
    973  1.1 1099192  43784  33752 1041340  16860  488    0   1.0 containerd
   2214  1.0  754268  41856  23924  706288  19352   16    0   0.7 kube-scheduler
4   PID   PPID   UID USER     RUSER    TTY          TIME+  %CPU %MEM S COMMAND
    975      1    29 rpcuser  rpcuser  ?          0:00.01   0.0  0.0 S rpc.statd
    567      1    32 rpc      rpc      ?          0:00.19   0.0  0.0 S rpcbind
      1      0     0 root     root     ?          0:48.12   0.3  0.1 S systemd
      2      0     0 root     root     ?          0:00.05   0.0  0.0 S kthreadd
      3      2     0 root     root     ?          0:07.97   0.0  0.0 S ksoftirqd/0
      4      2     0 root     root     ?          0:07.09   0.0  0.0 S kworker/0:0
      5      2     0 root     root     ?          0:00.00   0.0  0.0 S kworker/0:0H
      7      2     0 root     root     ?          0:01.09   0.0  0.0 S migration/0
```
**<font color=tomato>输入F时进入配置界面,通过界面显示可以配置详细信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$top -i
top - 09:48:03 up 13:30,  1 user,  load average: 0.85, 0.64, 0.57
Tasks: 215 total,   1 running, 214 sleeping,   0 stopped,   0 zombie
%Cpu(s):  3.4 us,  3.4 sy,  0.0 ni, 93.1 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  4030172 total,  1076900 free,  1206184 used,  1747088 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  2469016 avail Mem

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
  2204 root      20   0  823728  98312  36136 S  12.5  2.4  48:24.22 kube-controller
   963 root      20   0 1778244 142228  59968 S   6.2  3.5  42:12.13 kubelet
  2266 root      20   0 10.696g  79468  19904 S   6.2  2.0  24:29.30 etcd
  4348 root      20   0 1657820  47396  21052 S   6.2  1.2  43:37.87 calico-node
 50999 root      20   0  157720   2200   1500 R   6.2  0.1   0:00.01 top
Fields Management for window 1:Def, whose current sort field is %CPU
   Navigate with Up/Dn, Right selects for move then <Enter> or Left commits,
   'd' or <Space> toggles display, 's' sets sort.  Use 'q' or <Esc> to end!

* PID     = Process Id             SUPGIDS = Supp Groups IDs
* USER    = Effective User Name    SUPGRPS = Supp Groups Names
* PR      = Priority               TGID    = Thread Group Id
* NI      = Nice Value             ENVIRON = Environment vars
* VIRT    = Virtual Image (KiB)    vMj     = Major Faults delta
* RES     = Resident Size (KiB)    vMn     = Minor Faults delta
* SHR     = Shared Memory (KiB)    USED    = Res+Swap Size (KiB)
* S       = Process Status         nsIPC   = IPC namespace Inode
* %CPU    = CPU Usage              nsMNT   = MNT namespace Inode
* %MEM    = Memory Usage (RES)     nsNET   = NET namespace Inode
* TIME+   = CPU Time, hundredths   nsPID   = PID namespace Inode
* COMMAND = Command Name/Line      nsUSER  = USER namespace Inode
  PPID    = Parent Process pid     nsUTS   = UTS namespace Inode
  UID     = Effective User Id
  RUID    = Real User Id
  RUSER   = Real User Name
  SUID    = Saved User Id
  SUSER   = Saved User Name
  GID     = Group Id
  GROUP   = Group Name
  PGRP    = Process Group Id
  TTY     = Controlling Tty
  TPGID   = Tty Process Grp Id
  SID     = Session Id
  nTH     = Number of Threads
  P       = Last Used Cpu (SMP)
  TIME    = CPU Time
  SWAP    = Swapped Size (KiB)
  CODE    = Code Size (KiB)
  DATA    = Data+Stack (KiB)
  nMaj    = Major Page Faults
  nMin    = Minor Page Faults
  nDRT    = Dirty Pages Count
  WCHAN   = Sleeping in Function
  Flags   = Task Flags <sched.h>
  CGROUPS = Control Groups
```
>**<font color=tomato>在配置统计信息时,所有当前被选择的字段将会以大写形式显示在Current Field. Order行,并在其名称旁出现一个星号(*),使用d来选择删除，使用s保存，q退出</font>**。

**<font color=purple>修改只留5个列名展示</font>**
```bash
Fields Management for window 1:Def, whose current sort field is %CPU
   Navigate with Up/Dn, Right selects for move then <Enter> or Left commits,
   'd' or <Space> toggles display, 's' sets sort.  Use 'q' or <Esc> to end!

* PID     = Process Id             SUPGIDS = Supp Groups IDs
* USER    = Effective User Name    SUPGRPS = Supp Groups Names
  PR      = Priority               TGID    = Thread Group Id
  NI      = Nice Value             ENVIRON = Environment vars
  VIRT    = Virtual Image (KiB)    vMj     = Major Faults delta
  RES     = Resident Size (KiB)    vMn     = Minor Faults delta
  SHR     = Shared Memory (KiB)    USED    = Res+Swap Size (KiB)
  S       = Process Status         nsIPC   = IPC namespace Inode
* %CPU    = CPU Usage              nsMNT   = MNT namespace Inode
* %MEM    = Memory Usage (RES)     nsNET   = NET namespace Inode
  TIME+   = CPU Time, hundredths   nsPID   = PID namespace Inode
* COMMAND = Command Name/Line      nsUSER  = USER namespace Inode
  PPID    = Parent Process pid     nsUTS   = UTS namespace Inode
  UID     = Effective User Id
  RUID    = Real User Id
  RUSER   = Real User Name
  SUID    = Saved User Id
  SUSER   = Saved User Name
  GID     = Group Id
  GROUP   = Group Name
  PGRP    = Process Group Id
  TTY     = Controlling Tty
.....
```
**<font color=blue>返回后发现只显示了5列</font>**
```bash
top - 09:55:38 up 13:38,  1 user,  load average: 2.16, 1.20, 0.80
Tasks: 215 total,   2 running, 213 sleeping,   0 stopped,   0 zombie
%Cpu(s):  8.1 us,  4.7 sy,  0.0 ni, 87.0 id,  0.0 wa,  0.0 hi,  0.2 si,  0.0 st
KiB Mem :  4030172 total,  1085120 free,  1197260 used,  1747792 buff/cache
KiB Swap:        0 total,        0 free,        0 used.  2477772 avail Mem

   PID USER      %CPU %MEM COMMAND
  2195 root      14.3  9.5 kube-apiserver
  2266 root       8.0  1.7 etcd
  2204 root       6.0  2.4 kube-controller
  4348 root       5.0  1.2 calico-node
   963 root       4.7  3.5 kubelet
  1121 root       1.3  2.1 dockerd
  3057 root       0.7  0.8 speaker
     9 root       0.3  0.0 rcu_sched
   542 root       0.3  0.2 vmtoolsd
   973 root       0.3  1.1 containerd
  2214 root       0.3  1.0 kube-scheduler
 49390 root       0.3  0.0 kworker/1:2
```


**<font color=yellowgreen>top性能统计信息</font>**
|top性能统计信息|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/5911bab348f34e7e9b1b3eccbfa8e710.png)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/764ddf0e6bd940ccb97a9cedc2e2f21f.png)|

**<font color=orange>%Cpu(s):  5.3 us(用户消耗),  2.8 sy(系统消耗),  0.0 ni(友好值进程消耗), 91.9 id(空闲CPU),  0.0 wa(等待I/O的CPU时间),  0.0 hi(irp处理程序消耗的CPU时间),  0.0 si(softirq处理消耗),  0.0 st(这个不知道)</font>**

**<font color=chocolate>top - 09:26:22 up 13:08,  1 user,  load average: 0.56, 0.48, 0.55(1分钟、5分钟和15分钟的平均负载)</font>**

**<font color=tomato>列名 command为当前进程执行的命令，更多小伙伴可以结合命令列表理解</font>**


### <font color=blue> 2.2.4procinfo (从/proc文件系统显示信息)</font>

**<font color=red>`procinfo`也为系统整体信息特性提供总览，它提供的有些信息与vmstat相同,但它还会给出CPU从每个设备接收的中断数量</font>**
#### CPU性能相关的选项
```bash
$procinfo [-f] [-d] [-D] [-n sec] [-f file]
```
>嗯，这个命令没找到，以后再研究下

### <font color=orange>2.2.5 gnome-system-monitor</font>

>嗯，这个也以后再研究下

### <font color=royalblue>2.2.6 mpstat (多处理器统计)</font>

**<font color=royalblue>`mpstat`是一个相当简单的命令,向你展示`随着时间变化的CPU行为`。mpstat最大的优点是在统计信息的旁边显示时间,由此,你可以找出CPU使用率与时间的关系。</font>**

>如果你有多个CPU或超线程CPU, mpstat还能够把CPU使用率按处理器进行区分,因此你可以发现与其他处理器相比,是否某个处理器做了更多的工作。你可以选择想要监控的单个处理器,也可以要求mpstat对所有的处理器都进行监控。

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ mpstat -help
用法: mpstat [ 选项 ] [ <时间间隔> [ <次数> ] ]
选项:
[ -A ] [ -u ] [ -V ] [ -I { SUM | CPU | SCPU | ALL } ]
[ -P { <cpu> [,...] | ON | ALL } ]
┌──[root@liruilongs.github.io]-[~]
└─$
```
####  <font color=purple>CPU性能相关的选项</font>
```bash
mpstat -P { cpu | ALL} | delay [ count ]]
```
|CPU性能相关的选项|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/9c66abfe6b57493daf9a6f468f0b8422.png)|

**<font color=blue>使用-P监控0所在的CUP</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ mpstat  -P 0
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时31分09秒  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
11时31分09秒    0    0.53    0.00    0.53    0.06    0.00    0.06    0.00    0.00    0.00   98.82
┌──[root@liruilongs.github.io]-[~]
└─$ mpstat  -P 1
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时31分13秒  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
11时31分13秒    1    0.52    0.00    0.53    0.06    0.00    0.05    0.00    0.00    0.00   98.84
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=red>监控所有的CPU</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ mpstat  -P ALL
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时35分17秒  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
11时35分17秒  all    0.53    0.00    0.53    0.06    0.00    0.06    0.00    0.00    0.00   98.83
11时35分17秒    0    0.53    0.00    0.53    0.06    0.00    0.06    0.00    0.00    0.00   98.82
11时35分17秒    1    0.52    0.00    0.52    0.06    0.00    0.05    0.00    0.00    0.00   98.84
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=seagreen>监控3次，间隔1秒</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ mpstat  -P 0 1 3
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时33分15秒  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
11时33分16秒    0    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
11时33分17秒    0    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
11时33分18秒    0    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
平均时间:    0    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
┌──[root@liruilongs.github.io]-[~]
└─$
```

|CUP统计信息|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/6a2231f374f5491e9e4a16bd61bba23c.png)|

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ mpstat  -P ALL 1 2
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时37分52秒  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
11时37分53秒  all    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
11时37分53秒    0    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
11时37分53秒    1    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00

11时37分53秒  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
11时37分54秒  all    0.51    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.49
11时37分54秒    0    1.02    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00   98.98
11时37分54秒    1    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00

平均时间:  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest  %gnice   %idle
平均时间:  all    0.26    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.74
平均时间:    0    0.51    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00   99.49
平均时间:    1    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00    0.00  100.00
┌──[root@liruilongs.github.io]-[~]
└─$
```
### <font color=blue>2.2.7 sar (系统活动报告)</font>

**<font color=green>sar是一种低开销的、记录系统执行情况信息的方法</font>**

>`sar`命令可以用于记录性能信息,回放之前的记录信息,以及显示当前系统的实时信息。sar命令的输出可以进行格式化,使之易于导入数据库,或是输送给其他Linux命令进行处理。

**<font color=amber>如果没有命令，可以启动服务看看</font>**

```bash
┌──[root@liruilongs.github.io]-[/usr/lib/systemd/system]
└─$ pwd
/usr/lib/systemd/system
┌──[root@liruilongs.github.io]-[/usr/lib/systemd/system]
└─$ cat sysstat.service
# /usr/lib/systemd/system/sysstat.service
# (C) 2012 Peter Schiffer (pschiffe <at> redhat.com)
#
# sysstat-10.1.5 systemd unit file:
#        Insert a dummy record in current daily data file.
#        This indicates that the counters have restarted from 0.

[Unit]
Description=Resets System Activity Logs

[Service]
Type=oneshot
RemainAfterExit=yes
User=root
ExecStart=/usr/lib64/sa/sa1 --boot

[Install]
WantedBy=multi-user.target

┌──[root@liruilongs.github.io]-[/usr/lib/systemd/system]
└─$ systemctl status  sysstat.service
● sysstat.service - Resets System Activity Logs
   Loaded: loaded (/usr/lib/systemd/system/sysstat.service; enabled; vendor preset: enabled)
   Active: active (exited) since Wed 2021-10-13 01:53:41 CST; 1 weeks 3 days ago
 Main PID: 584 (code=exited, status=0/SUCCESS)
   CGroup: /system.slice/sysstat.service

Oct 13 01:53:41 liruilongs.github.io systemd[1]: Starting Resets System Activity Logs...
Oct 13 01:53:41 liruilongs.github.io systemd[1]: Started Resets System Activity Logs.

```
**<font color=plum>命令帮助文档</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar  -h
用法: sar [ 选项 ] [ <时间间隔> [ <次数> ] ]
主选项和报告：
        -b      I/O 和传输速率信息状况
        -B      分页状况
        -d      块设备状况
        -F [ MOUNT ]
                Filesystems statistics
        -H      交换空间利用率
        -I { <中断> | SUM | ALL | XALL }
                中断信息状况
        -m { <关键词> [,...] | ALL }
                电源管理统计信息
                关键字:
                CPU     CPU 频率
                FAN     风扇速度
\t\tFREQ\tCPU 平均时钟频率
                IN      输入电压
                TEMP    设备温度
\t\tUSB\t连接的USB 设备
        -n { <关键词> [,...] | ALL }
                网络统计信息
                关键词可以是：
                DEV     网卡
                EDEV    网卡 (错误)
                NFS     NFS 客户端
                NFSD    NFS 服务器
                SOCK    Sockets (套接字)        (v4)
                IP      IP 流   (v4)
                EIP     IP 流   (v4) (错误)
                ICMP    ICMP 流 (v4)
                EICMP   ICMP 流 (v4) (错误)
                TCP     TCP 流  (v4)
                ETCP    TCP 流  (v4) (错误)
                UDP     UDP 流  (v4)
                SOCK6   Sockets (套接字)        (v6)
                IP6     IP 流   (v6)
                EIP6    IP 流   (v6) (错误)
                ICMP6   ICMP 流 (v6)
                EICMP6  ICMP 流 (v6) (错误)
                UDP6    UDP 流  (v6)
        -q      队列长度和平均负载
        -r      内存利用率
        -R      内存状况
        -S      交换空间利用率
        -u [ ALL ]
                CPU 利用率
        -v      Kernel table 状况
        -w      任务创建与系统转换统计信息
        -W      交换信息
        -y      TTY 设备状况
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=seagreen>红帽8没有，需要自己开启，红帽7有，下面为历史监控的存放位置</font>**
```bash
┌──[root@liruilongs.github.io]-[/]
└─$ cd /var/log/sa
┌──[root@liruilongs.github.io]-[/var/log/sa]
└─$ ls
sa01  sa03  sa04  sa05  sa06  sa08  sa09  sa13  sa14  sa23  sa24  sa30  sar03
```
#### <font color=green>CPU性能相关的选项</font>
```bash
$sar [options] [] delay [ count ]]
```
>**<font color=brown>尽管sar的报告涉及Linux多个不同领域,其统计数据有两种不同的形式。一组统计数据是采样时的瞬时值。另一组则是自上一次采样后的变化值。</font>**

|sar命令行选项|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/eece517f17664fc18928e2233ab5acd5.png)|
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时00分01秒     CPU     %user     %nice   %system   %iowait    %steal     %idle
11时10分01秒     all      0.12      0.00      0.12      0.00      0.00     99.76
11时20分01秒     all      0.11      0.00      0.11      0.00      0.00     99.78
11时30分01秒     all      0.13      0.00      0.13      0.00      0.00     99.73
11时40分01秒     all      0.11      0.00      0.11      0.00      0.00     99.78
平均时间:     all      0.12      0.00      0.12      0.00      0.00     99.76
```
**<font color=brown>使用  -o 选项输出到指定文件</font>**，
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -o /tmp/apache_tets 1 3
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

18时02分24秒     CPU     %user     %nice   %system   %iowait    %steal     %idle
18时02分25秒     all      0.00      0.00      0.56      0.00      0.00     99.44
18时02分26秒     all      0.00      0.00      0.00      0.00      0.00    100.00
18时02分27秒     all      0.00      0.00      0.00      0.00      0.00    100.00
平均时间:     all      0.00      0.00      0.18      0.00      0.00     99.82
```
**<font color=purple>通过 -f 来查看指定文件信息</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -f /tmp/apache_tets
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

18时02分24秒     CPU     %user     %nice   %system   %iowait    %steal     %idle
18时02分25秒     all      0.00      0.00      0.56      0.00      0.00     99.44
18时02分26秒     all      0.00      0.00      0.00      0.00      0.00    100.00
18时02分27秒     all      0.00      0.00      0.00      0.00      0.00    100.00
平均时间:     all      0.00      0.00      0.18      0.00      0.00     99.82
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=orange>使用 -P指定CUP</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -P 0  | head -5
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时00分01秒     CPU     %user     %nice   %system   %iowait    %steal     %idle
11时10分01秒       0      0.12      0.00      0.12      0.01      0.00     99.75
11时20分01秒       0      0.12      0.00      0.11      0.00      0.00     99.77
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=amber>-w  上下文切换次数</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -P 0 -w | head -5
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时00分01秒    proc/s   cswch/s
11时10分01秒      0.92    313.58
11时20分01秒      0.81    316.81
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=chocolate>-q  运行队列长度和平均负载</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar  -q | head -5
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年01月15日  _x86_64_        (2 CPU)

11时00分01秒   runq-sz  plist-sz   ldavg-1   ldavg-5  ldavg-15   blocked
11时10分01秒         0       219      0.01      0.08      0.53         0
11时20分01秒         0       219      0.03      0.03      0.29         0
```
|sar CPU统计信息|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/7b73246a5fc74509a58de3705af9bd16.png)|



### 2.2.8 oprofile

**<font color=chocolate>oprofile是性能工具包,它利用几乎所有现代处理器都有的性能计数器来跟踪系统整体以及单个进程中CPU时间的消耗情况。除了测量CPU周期消耗在哪里之外, oprofile还可以测量关于CPU执行的非常底层的信息。</font>**

>根据由底层处理器支持的事件,它可以测量的内容包括: cache缺失、分支预测错误和内存引用,以及浮点操作。profile不会记录发生的每个事件,相反,它与处理器性能硬件一起工作,每count个事件采样一次,这里的count是一个数值,由用户在启动oprofile时指定。count的值越·低,结果的准确度越高,而oprofile的开销越大。若count保持在一个合理的数值,那么,oprofile不仅运行开销非常低,并且还能以令人惊讶的准确性描述系统性能。


```bash
┌──[root@liruilongs.github.io]-[~]
└─$ yum -y install oprofile
```

#### CPU性能相关的选项

`oprofile`实际上是一组协同工作的组件,用于收集`CPU`性能统计信息。`oprofile`主要有三个部分:

`oprofile`核心模块控制处理器并允许和禁止采样,
`oprofile`后台模块收集采样,并将它们保存到磁盘。
`oprofile`报告工具获取收集的采样,并向用户展示它们与在系统上运行的应用程序的关系

`oprofile`工具包使用`opcontrol`命令中。`opcontrol`命令用于选择处理器采样的事件并启动采样。进行后台控制时,你可以使用如下命令行调用
```bash
$opcontrol [--start] [--stop] [--dump]
```
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ opcontrol -s
ATTENTION: Use of opcontrol is discouraged.  Please see the man page for operf.
No vmlinux file specified. You must specify the correct vmlinux file, e.g.
opcontrol --vmlinux=/path/to/vmlinux
If you do not have a vmlinux file, use
opcontrol --no-vmlinux
Enter opcontrol --help for full options
```
>这个貌似有些复杂，之后遇到机会再学习，关系CUP的性能调优工具学到这里      



# 第3章 性能工具：系统内存
## 3.1 内存性能统计信息

每一种系统级Linux性能工具都提供了不同的方式来提取类似的统计结果。

### 3.1.1 内存子系统和性能

在现代处理器中，与CPU执行代码或处理信息相比，`向内存子系统保存信息或从中读取信息一般花费的时间更长`。通常，在`CPU执行指令`或处理数据前，它会消耗相当多的空闲时间来`等待从内存中取出指令和数据`。处理器用不同层次的`高速缓存(cache)`来弥补这种缓慢的内存性能。

### 3.1.2 内存子系统(虚拟存储器)

任何给定的Linux系统都有一定容量的`RAM或物理内存`。在这个物理内存中寻址时，Linux将其分成`块`或`内存“页”`。当对内存进分配或传送时，Linux`操作的单位是页`，而不是`单个字节`。

在报告一些内存统计数据时，Linux内核报告的是`每秒页面的数量`，该值根据其运行的架构可以发生变化。

对IA32架构而言(“`Intel32位体系结构`”(Intel Architecture 32-bit)，而我们常说的X86-64就是IA32的64位拓展。)，页面大小为`4KB`。极少数情况下，这些页面大小的内存块会导致极高的`跟踪开销`，所以，内核用更大的`块来操作内存`，这些块被称为`HugePage(大页面)`。


它们的容量为`2048KB`，而不是`4KB`，这大大降低了管理庞大内存的开销。某些应用，如Oracle，用这些大页面加载内存中的大量数据，同时又`最小化Linux内核的管理开销`。如果`HugePage`不能完全被填满，就会浪费相当多的内存。`一个半填充的普通页面浪费2KB内存`，而`一个半填充的HugePage就会浪费1024KB的内存`。


Linux内核可以分散收集这些物理页面，向应用程序呈现出一个精心设计的虚拟内存空间。

#### 3.1.2.1交换(物理内存不足)

所有系统`RAM芯片的物理内存容量`都是固定的。即使应用程序需要的内存容量大于可用的物理内存，Linux内核仍然允许这些程序运行。Linux内核使用`硬盘作为临时存储器`，这个硬盘空间被称为`交换分区(swap space)`。

尽管交换是让进程运行的极好的方法，但它却慢的要命。与使用物理内存相比，应用程序使用交换的速度可以慢到一千倍。`如果系统性能不佳，确定系统使用了多少交换通常是有用的。`

#### 3.1.2.2 缓冲区(buffer)和缓存(cache)(物理内存太多)

##### 缓存(cache)
相反，如果你的`系统物理内存容量超过了应用程序的需求`，Linux就会在`物理内存中缓存近期使用过的文件`，这样，后续访问这些文件时就不用去访问硬盘了。

对要频繁访问硬盘的应用程序来说，这可以显著加速其速度，显然，对经常启动的应用程序而言，这是特别有用的。

应用程序首次启动时，它需要从硬盘读取；但是，如果应用程序留着缓存中，那它就需要从更快速的物理内存读取。

`这个硬盘缓存不同于前面章节提到的处理器高速缓存(cache)`

##### 缓冲区(buffer)

Linux还使用了额外的存储作为缓冲区。为了进一步优化应用程序，`Linux为需要被写回硬盘的数据预留了存储空间。这些预留空间被称为缓冲区`。如果应用程序要将`数据写回硬盘`，通常需要花费`较长时间`，Linux让应用程序`立刻继续执行`，但将`文件数据`保存到`内存缓冲区`。在之后的某个时刻，`缓冲区被刷新到硬盘`，而应用程序可以`立即继续`。



`高速缓存和缓冲区`的使用使得系统内`空闲的内存`很少，默认情况下，Linux试图尽可能多的使用你的内存。这是好事。

如果`Linux侦测`到有`空闲内存`，它就会`将应用程序和数据缓存到这些内存以加速未来的访问`。由于访问内存的速度比访问硬盘的速度`快了几个数量级`，因此，这就可以显著地提升`整体性能`。

如果系统需要缓存空间做更重要的事情，那么缓存空间将被擦除并交给系统。之后，对原来被缓存对象的访问就需要转向硬盘来满足。

#### 3.1.2.3活跃与非活跃内存

+ `活跃内存`是指当前被进程使用的内存。
+ `不活跃内存`是指已经被分配了，但暂时还未使用的内存。
  
这两种类型的内存没有本质上的区别。需要时，Linux找出进程最近最少使用的`内存页面`，并将它们从活跃列表移动到不活跃列表。当要选择把`哪个内存页交换到硬盘`时，`内核就从不活跃内存列表中进行选择`。

#### 3.1.2.4高端与低端内存

对拥有1GB或更多物理内存的32位处理器(比如lA32)来说，Linux管理内存时必须将其分为`高端与低端内存`。

高端内存`不能`直接被`Linux内核`访问，而是必须在使用前`映射到低端`内存范围内。

64位处理器(比如AMD64/EM6T、Alpha或Itanium)没有这个问题，因为它们可以直接寻址当前系统可用的额外内存。

#### 3.1.2.5内核的内存使用情况(分片)

除了应用程序需要分配内存外，`Linux内核`也会为了`记账`的目的消耗一定量的内存。

记账包括，比如跟踪从网络或磁盘I/O来的数据，以及跟踪哪些进程正在运行，哪些正在休眠。`为了管理记账，内核有一系列缓存`，包含`了一个或多个内存分片`。每个分片为一组对象，个数可以是一个或多个。`内核消耗的内存分片数量取决于使用的是Linux内核的哪些部分`，而且还可以随着机器负载类型的变化而变化。

## 3.2 Linux性能工具：CPU与内存

### 3.2.1 vmstat

+ 使用了多少交换分区。
+ 物理内存是如何被使用的。
+ 有多少空闲内存。

vmstat除了提供CPU统计信息外，你还可以通过如下命令行调用vmstat来调查内存统计信息：

`vmstat[-a][-s][-m]`


|选项|说明|
|--|--|
|-a |该项改变内存统计信息的默认输出以表示活跃/非活跃内存量，而不是缓冲区和高速缓存使用情况的信息|
|-s(oroc0s 3.2或更高版本)|打印输出vm表。自系统启动开始的综合统计信息。该项不能用于采样模式，它包含了内存和CPU的统计数据|
|-m(procps3.2或更高版本)|该项输出内核分片信息。键入cat/proc/slabinfo可以获得同样的信息。信息详细展示了内核内存是如何分配的，并有助于确定哪部分内核消耗内存最多|

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 3  0      0 27270048   3104 984432    0    0    38    23  315  294  4  2 94  0  0
```

vmstat调用时没有使用任何命令行选项，它显示的是从`系统启动开始的性能统计数据的均值(si和so)`，以及其他统计信息的瞬时值(swpd、free、buff和cache)

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat  1 100
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 1  0      0 23365320   3104 962048    0    0   176    23  305  296  2  3 95  0  0
 1  0      0 23365164   3104 962088    0    0     0     0  768  854  0  0 100  0  0
 0  0      0 23364900   3104 962088    0    0     0     0  836  993  0  0 100  0  0
^C
```

+ **swpd** :当前`交换到硬盘的内存总量`
+ **free** :`未被操作系统或应用程序使用的物理内存总量`
+ **buff** : `系统缓冲区大小(单位为KB)，或用于存放等待保存到硬盘的数据的内存大小(单位为KB)`。该存储区允许`应用程序向Linux内核发出写调用后立即继续执行`(而不是等待直到数据被提交到硬盘)
+ **cache** :用于保存之前`从硬盘读取的数据的系统高速缓存或内存的大小(单位为KB)`。如果应用程序再次需要该数据，内核可以从内存而非硬盘抓取数据，由此可提高性能

**vmstat显示活跃与非活跃页面的数量信息。**

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat -a
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free  inact active   si   so    bi    bo   in   cs us sy id wa st
 0  0      0 23225336 1987728 6884184    0    0    31   145  267  282  1  1 98  0  0
```
|列|说明(Kb)|
|--|--|
|active| 被使用的`活跃内存量`。活跃/不活跃的统计数据与缓冲区/高速缓存的是正交的；缓冲区和高速缓存可以是活跃的，也可以是不活跃的|
|inactive|`不活跃的内存总量`(单位为KB)，或一段时间未被使用，适合交换到硬盘的内存量|
|si|上一次采样中，从硬盘进来的内存交换速率(单位为KB/s)|
|so|上一次采样中，到硬盘去的内存交换速率(单位为KB/s)|


```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat -s
     32931532 K total memory
      8498760 K used memory
      8329368 K active memory
       497280 K inactive memory
     23470020 K free memory
         3104 K buffer memory
       959648 K swap cache
     10485756 K total swap
            0 K used swap
     10485756 K free swap
         8162 non-nice user cpu ticks
            0 nice user cpu ticks
        10277 system cpu ticks
       220249 idle cpu ticks
          653 IO-wait cpu ticks
            0 IRQ cpu ticks
          435 softirq cpu ticks
            0 stolen cpu ticks
       682275 pages paged in
        83668 pages paged out
            0 pages swapped in
            0 pages swapped out
       907353 interrupts
       828939 CPU context switches
   1658547198 boot time
         9260 forks
```
|列|说明|
|--|--|
|pages paged in |从硬盘读人系统缓冲区的内存总量(单位为页)|
|pages paged out |从系统高速缓存写到硬盘的内存总量(单位为页)|
|pages swapped in |从交换分区读入系统内存的内存总量(单位为页)|
|pages swapped out| 从系统内存写到交换分区的内存总量(单位为页)|
|used swap |Linux内核目前使用的交换分区容量|
|free swap |当前可用的交换分区容量|
|total swap|系统的交换分区总量，即used swap与free swap之和|

vmstat可以提供关于Linux内核如何分配其内存的信息。如前所述，Linux内核有一系列`“分片”`来保存其动态数据结构。

vmstat显示每一个`分片(Cache)`，展示使用了多少`元素(Num)`，分配了多少`(Total)`，每个元素的`大小(Size)`，整个分片使用了`多少内存页(Pages)`。这些信息有助于跟踪内核究竟是怎样使用其内存的。



```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat -m
Cache                       Num  Total   Size  Pages
nf_conntrack_ffff8807fbe22880    153    153    320     51
nf_conntrack_ffff8807f2650000    306    306    320     51
nf_conntrack_ffffffff81ad9d40    306    306    320     51
xfs_dqtrx                     0      0    528     62
xfs_icr                       0      0    152     53
xfs_inode                 37298  37298    960     34
xfs_efd_item                760    800    408     40
.............
┌──[root@liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /proc/slabinfo
slabinfo - version: 2.1
# name            <active_objs> <num_objs> <objsize> <objperslab> <pagesperslab> : tunables <limit> <batchcount> <sharedfactor> : slabdata <active_slabs> <num_slabs> <sharedavail>
nf_conntrack_ffff8807fbe22880    153    153    320   51    4 : tunables    0    0    0 : slabdata      3      3      0
nf_conntrack_ffff8807f2650000    306    306    320   51    4 : tunables    0    0    0 : slabdata      6      6      0
nf_conntrack_ffffffff81ad9d40    306    306    320   51    4 : tunables    0    0    0 : slabdata      6      6      0
xfs_dqtrx              0      0    528   62    8 : tunables    0    0    0 : slabdata      0      0      0
xfs_icr                0      0    152   53    2 : tunables    0    0    0 : slabdata      0      0      0
xfs_inode          37298  37298    960   34    8 : tunables    0    0    0 : slabdata   1097   1097      0
xfs_efd_item         760    800    408   40    4 : tunables    0    0    0 : slabdata     20     20      0
xfs_buf_item         408    408    240   68    4 : tunables    0    0    0 : slabdata      6      6      0
xfs_btree_cur        234    234    208   39    2 : tunables    0    0    0 : slabdata      6      6      0
xfs_log_ticket       352    352    184   44    2 : tunables    0    0    0 : slabdata      8      8      0
bio-1                408    408    320   51    4 : tunables    0    0    0 : slabdata      8      8      0
ip6_dst_cache        252    252    448   36    4 : tunables    0    0    0 : slabdata      7      7      0
RAWv6                624    624   1216   26    8 : tunables    0    0    0 : slabdata     24     24      0
...........

```





### 3.2.2  top(2.x和3.x)

默认情况下，top展示的是对进程的CPU消耗量进行降序排列的列表，但它也可以调整为按内存使用总量排序，以便你能跟踪到哪个进程使用的内存最多。



学习版本
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ top -v
  procps-ng version 3.3.10
Usage:
  top -hv | -bcHiOSs -d secs -n max -u|U user -p pid(s) -o field -w [cols]
```


top运行时切换项

| 选项|说明|
|--|--|
|m|该项切换是否将内存使用量信息显示到屏幕|
|M|按任务使用的内存量排序。由于分配给进程的内存量可能会大于其使用量，因此，该项按驻留集大小排序。驻留集大小是指进程实际使用量，而不是简单的进程请求量|

```bash
top - 11:48:07 up 14 min,  1 user,  load average: 0.07, 0.10, 0.13
Tasks: 273 total,   2 running, 271 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem : 32931532 total, 23319380 free,  8643596 used,   968556 buff/cache
KiB Swap: 10485756 total, 10485756 free,        0 used. 23759444 avail Mem

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
   889 etcd      20   0 10.559g  29332  11008 S   1.3  0.1   0:10.94 etcd
  8089 nginx     20   0   41636  12148   1588 S   1.3  0.0   0:03.35 redis-server
```

|选项|说明|
|--|--|
|%MEM|进程使用内存量占系统物理内存的百分比|
|VIRT(v3.x)/SIME(v2.x)|进程虚拟内存使用总量。其中包括了应用程序分配到但未使用的全部内存|
|SWAP|进程使用的交换区(单位为KB)总量|
|RSS(v2.x)/RES(v3.x)|应用程序实际使用的物理内存总量|
|SHARE(v 2.x)/SHR(V 3.x) |可与其他进程共享的内存总量(单位为KB)|
|Mem:total，used，free |对物理内存来说，该项表示的是其总量、使用量和空闲量|
|swap:total，used，free |对交换分区来说，该项表示的是其总量、使用量和空闲量|
|buff/cache |用于缓冲区写人硬盘的数值和缓存的物理内存总量(单位为KB)|

```bash
top - 11:47:42 up 14 min,  1 user,  load average: 0.11, 0.11, 0.13
Tasks: 273 total,   1 running, 272 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.1 us,  0.2 sy,  0.0 ni, 99.6 id,  0.0 wa,  0.0 hi,  0.1 si,  0.0 st
KiB Mem : 27.9/32931532 [|||||||||||||||||||||                                                      ]
KiB Swap:  0.0/10485756 [                                                                           ]

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
   889 etcd      20   0 10.559g  29332  11008 S   1.0  0.1   0:10.70 etcd
  8073 992       20   0  762040 109952  11972 S   1.0  0.3   0:06.59 prometheus
  8079 chrony    20   0  860404 575904   9968 S   1.0  1.7   0:30.24 bundle
  8082 992       20   0  542292  15620   5476 S   0.3  0.0   0:00.77 node_exporter
  8089 nginx     20   0   41636  12148   1588 S   0.3  0.0   0:03.26 redis-server
  8093 chrony    20   0  305636  30176   4964 S   0.3  0.1   0:03.33 gitlab-mon
  8099 nginx     20   0  398856  11900   3784 S   0.3  0.0   0:00.53 redis_exporter
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/6711fb4848b14535a37debc9dddf3396.png)


### 3.2.3  procinfo(I)
命令无，以后在研究
### 3.2.4 gnome-system-monitor(I)
图形化页面，以后在看
### 3.2.5free


free提供的是系统使用内存的总体情况，包括空闲内存量。

`free [-l][·t][-s delay 1[-c count]`

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free -V
free from procps-ng 3.3.10
┌──[root@liruilongs.github.io]-[~]
└─$ free --help

Usage:
 free [options]

Options:
 -b, --bytes         show output in bytes
 -k, --kilo          show output in kilobytes
 -m, --mega          show output in megabytes
 -g, --giga          show output in gigabytes
     --tera          show output in terabytes
     --peta          show output in petabytes
 -h, --human         show human-readable output
     --si            use powers of 1000 not 1024
 -l, --lohi          show detailed low and high memory statistics
 -t, --total         show total for RAM + swap
 -s N, --seconds N   repeat printing every N seconds
 -c N, --count N     repeat printing N times, then exit
 -w, --wide          wide output

     --help     display this help and exit
 -V, --version  output version information and exit

For more details see free(1).
```
常用命令
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free
              total        used        free      shared  buff/cache   available
Mem:       32931532     8820980    22388436       27388     1722116    23579356
Swap:      10485756           0    10485756
┌──[root@liruilongs.github.io]-[~]
└─$ free -h
              total        used        free      shared  buff/cache   available
Mem:            31G        8.4G         21G         26M        1.6G         22G
Swap:            9G          0B          9G
```

`-l `向你展示使用了多少高端内存和多少低端内存

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free -l
              total        used        free      shared  buff/cache   available
Mem:       32931532     4835004    24441680       35596     3654848    27657976
Low:       32931532     8489852    24441680
High:             0           0           0
Swap:      10485756           0    10485756
```

`-t  `命令可以查看内存的统计信息
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free -t
              total        used        free      shared  buff/cache   available
Mem:       32931532     5005552    24808440       35588     3117540    27487560
Swap:      10485756           0    10485756
Total:     43417288     5005552    35294196
```


|统计信息|说明|
|--|--|
|Total|物理内存与交换空间的总量|
|Used|使用的物理内存和交换分区的容量|
|Free|未使用的物理内存和交换分区的容量|
|shared|进程共享内存使用量，该项已过时，应忽略|
|Buff|用作`硬盘写缓冲区`的物理内存的容量|
|Cache|用作`硬盘读缓存`的物理内存的容量|
|High|`高端内存`或`不能被内核直接访问`的内存总量|
|Low|`低端内存`或`能被内核直接访问`的内存总量|
|Totals|对Total、Used和Free列，该项显示的是该列中物理内存和交换分区的总和|


选项说明
+ `-s delay `:使free按每delay秒的间隔输出新的内存统计数据
+ `-c count `:使free 输出count次新的统计数据

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free -s 2 -c 2
              total        used        free      shared  buff/cache   available
Mem:       32931532     5405500    24408372       35588     3117660    27087612
Swap:      10485756           0    10485756

              total        used        free      shared  buff/cache   available
Mem:       32931532     5405460    24408388       35588     3117684    27087652
Swap:      10485756           0    10485756
┌──[root@liruilongs.github.io]-[~]
└─$
```
### 3.2.6 slabtop

`slabtop实时显示内核是如何分配其各种缓存的`，以及这些缓存的被占用情况。在内部，内核有一系列的缓存，它们由一个或多个`分片(slab)`构成。每个分片包括一组对象，对象个数为一个或多个。

这些对象可以是活跃的(使用的)或非活跃的(未使用的)。slabtop向你展示的是不同分片的状况。它显示了这些分片的被占用情况，以及它们使用了多少内存。

slabtop可以一窥Linux内核的数据结构。每一种分片类型都与Linux内核紧密相关。如果某个特定分片使用了大量的内核内存，那么阅读Linux内核源代码和搜索互联网是找出这些分片用在哪里的最好的两种方法。
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ slabtop --help

Usage:
 slabtop [options]

Options:
 -d, --delay <secs>  delay updates
 -o, --once          only display once, then exit
 -s, --sort <char>   specify sort criteria by character (see below)

 -h, --help     display this help and exit
 -V, --version  output version information and exit

The following are valid sort criteria:
 a: sort by number of active objects
 b: sort by objects per slab
 c: sort by cache size
 l: sort by number of slabs
 v: sort by number of active slabs
 n: sort by name
 o: sort by number of objects (the default)
 p: sort by pages per slab
 s: sort by object size
 u: sort by cache utilization

For more details see slabtop(1).
```


```bash
 Active / Total Objects (% used)    : 2172003 / 2195023 (99.0%)
 Active / Total Slabs (% used)      : 49649 / 49649 (100.0%)
 Active / Total Caches (% used)     : 69 / 95 (72.6%)
 Active / Total Size (% used)       : 466189.06K / 477588.18K (97.6%)
 Minimum / Average / Maximum Object : 0.01K / 0.22K / 8.00K

  OBJS ACTIVE  USE OBJ SIZE  SLABS OBJ/SLAB CACHE SIZE NAME
727440 727339  99%    0.19K  17320       42    138560K dentry
571038 570600  99%    0.10K  14642       39     58568K buffer_head
305088 304062  99%    0.06K   4767       64     19068K kmalloc-64
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ slabtop -d 2 -s c
```
### 3.2.7 sar(Ⅱ)

`sar [-B][-rl[-R]`

|选项|描述|
|--|--|
|-B|报告的信息为内核与磁盘之间交换的块数。此外，对v2.5之后的内核版本，该项报告的信息为缺页数量|
|-W|报告的是系统交换的页数|
|-r|报告系统使用的内存信息。它包括总的空闲内存、正在使用的交换分区、缓存和缓冲区的信息|

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -W
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年07月23日  _x86_64_        (6 CPU)

11时33分39秒       LINUX RESTART

11时40分01秒  pswpin/s pswpout/s
11时50分01秒      0.00      0.00
12时00分01秒      0.00      0.00
12时10分01秒      0.00      0.00
12时20分01秒      0.00      0.00
平均时间:      0.00      0.00
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -B
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年07月23日  _x86_64_        (6 CPU)

11时33分39秒       LINUX RESTART

11时40分01秒  pgpgin/s pgpgout/s   fault/s  majflt/s  pgfree/s pgscank/s pgscand/s pgsteal/s    %vmeff
11时50分01秒     12.52    125.74    677.40      0.02    692.72      0.00      0.00      0.00      0.00
12时00分01秒    138.94    533.55   1253.45      0.17   1110.67      0.00      0.00      0.00      0.00
12时10分01秒    118.52   5527.63  35379.48      0.20  19923.40      0.00      0.00      0.00      0.00
12时20分01秒     15.85    122.99   3750.36      0.02   1989.95      0.00      0.00      0.00      0.00
平均时间:     71.46   1578.02  10268.63      0.10   5931.11      0.00      0.00      0.00      0.00
┌──[root@liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -r 1 3
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年07月23日  _x86_64_        (6 CPU)

12时39分27秒 kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit  kbactive   kbinact   kbdirty
12时39分28秒  23253572   9677960     29.39      3104   2789588   5138464     11.84   6864288   1980388        96
12时39分29秒  23253572   9677960     29.39      3104   2789620   5138464     11.84   6864300   1980416        96
12时39分30秒  23253572   9677960     29.39      3104   2789620   5138464     11.84   6864300   1980416        96
平均时间:  23253572   9677960     29.39      3104   2789609   5138464     11.84   6864296   1980407        96
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -R 1 3
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年07月23日  _x86_64_        (6 CPU)

12时39分44秒   frmpg/s   bufpg/s   campg/s
12时39分45秒    -96.00      0.00      6.00
12时39分46秒     29.00      0.00      0.00
12时39分47秒    -33.00      0.00      0.00
平均时间:    -33.33      0.00      2.00
┌──[root@liruilongs.github.io]-[~]
└─$

```
### 3.2.8/proc/meminfo


Linux内核提供用户可读文本文件`/proc/meminfo`来显示当前系统范围内的内存性能统计信息，

它提供了系统范围内`内存统计数据`的超集，包括了`vmstat、top、free和procinfo`的信息，但是使用起来有一定的难度。如果你想定期更新，就需要自己写一个脚本或一些代码来实现这个功能。如果你想保存内存性能信息或是将其与CPU统计信息相协调，就必须创建一个新的工具或是写一个脚本。


```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /proc/meminfo
MemTotal:       32931532 kB
MemFree:        23182352 kB
MemAvailable:   25878308 kB
Buffers:            3104 kB
Cached:          2790760 kB
SwapCached:            0 kB
Active:          6933748 kB
Inactive:        1981384 kB
Active(anon):    6124200 kB
Inactive(anon):    32624 kB
Active(file):     809548 kB
Inactive(file):  1948760 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:      10485756 kB
SwapFree:       10485756 kB
Dirty:                84 kB
Writeback:             0 kB
AnonPages:       6121244 kB
Mapped:           188304 kB
Shmem:             35556 kB
Slab:             481624 kB
SReclaimable:     340480 kB
SUnreclaim:       141144 kB
KernelStack:       12944 kB
PageTables:        31076 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    26951520 kB
Committed_AS:    5158652 kB
VmallocTotal:   34359738367 kB
VmallocUsed:      225072 kB
VmallocChunk:   34359310332 kB
HardwareCorrupted:     0 kB
AnonHugePages:   5437440 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
DirectMap4k:      161600 kB
DirectMap2M:     7178240 kB
DirectMap1G:    28311552 kB
```






# 第六章 性能工具：磁盘I/O



本章介绍的性能工具能帮助你评估磁盘I/O子系统的使用情况。这些工具可以展示哪些磁盘或分区已被使用，每个磁盘处理了多少I/O，发给这些磁盘的I/O请求要等多久才被处理。

阅读本章后，你将能够：
+ 确定系统内磁盘I/O的总量和类型(读/写)(vmstat)。
+ 确定哪些设备服务了大部分的磁盘I/O(vmstat，iostat，sar)。
+ 确定特定磁盘处理I/O请求的有效性(iostat)。
+ 确定哪些进程正在使用一组给定的文件(1sof)。


## 6.1磁盘I/O介绍

在深入性能工具之前，有必要了解Linux磁盘I/O系统是怎样构成的。大多数现代Linux系统都有一个或多个磁盘驱动。

如果是IDE驱动，那么常常将被命名为hda、hdb、hdc等；而SCSI驱动则常常被命名为sda、sdb、sdc等。

磁盘通常要分为多个分区，分区设备名称的创建方法是在基础驱动名称的后面直接添加分区编号。

比如，系统中首个IDE硬驱动的第二个分区通常被标记为/dev/hda2。

一般每个独立分区要么包含一个文件系统，要么包含一个交换分区。这些分区被挂载到Linux根文件系统，该系统由/etc/fstab指定。

这些被挂载的文件系统包含了应用程序要读写的文件。

关于IO阻塞的问题，在涉及的并发的问题中，我们常常要考虑线程I/O阻塞的情况来调整线程优先级，在JDK1.4 的版本中,提供NIO采用内存映射文件的方式处理，将文件或文件的一段区域映射到内存中，提高访问效率。

当一个应用程序进行读写时，Linux内核可以在其高速缓存或缓冲区中保存文件的副本，并且可以在不访问磁盘的情况下返回被请求的信息。但是，如果Linux内核没有在内存中保存数据副本，那它就向磁盘 I/O 队列添加一个请求。`若Linux内核注意到多个请求都指向磁盘内相邻的区域，它会把它们合并为一个大的请求。这种合并能消除第二次请求的寻道时间`, 以此来提高磁盘整体性能。当请求被放入磁盘队列，而磁盘当前不忙时，它就开始为/O请求服务。如果磁盘正忙，则请求就在队列中等待，直到该设备可用，请求将被服务。

![](https://img-blog.csdnimg.cn/3baffb6c16dc43cc92b87baad5700594.png)

Linux 存储相关的名词

磁盘：磁盘一般指一块较完整的物理内存，是硬件范畴的存储，一个磁盘可以有4个分区，三个主分区，一个扩展分区。


## 6.2磁盘I/O性能工具

本节讨论各种各样的磁盘I/O性能工具，它们能使你调查一个给定应用程序是如何使用磁盘1/O子系统的，包括每个磁盘被使用的程度，内核的磁盘高速缓存的工作情况，以及特定应用程序“打开”了哪些文件。

### 6.2.1 vmstat(III)

vmstat是一个强大的工具，它能给出系统在性能方面的总览图。除了CPU和内存统计信息之外，vmstat还可以提供系统整体上的I/O性能情况。
我们用的版本
```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$vmstat  -V
vmstat from procps-ng 3.3.10
┌──[root@vms81.liruilongs.github.io]-[/]
└─$which vmstat
/usr/bin/vmstat
```


#### 6.2.1.1磁盘I/O性能相关的选项和输出

在使用vmstat从系统获取磁盘1/0统计信息时,要按照如下方式进行调用:

`vmstat [-D] [-d] [-p partition] [interval [count]]`

表6-1说明的命令行选项能影响vmstat显示的磁盘10统计信息。

|选项|描述|
|--|--|
|-D|显示Linux I/O子系统总的统计数据。它可以让你很好地了解你的IO子系统是如何被使用的,但它不会给出单个磁盘的统计数据。显示的统计数据是从系统启动.开始的总信息,而不是两次采样之间的发生量 |
|-d|按每interval一个样本的速率显示单个磁盘的统计数据。这些统计信息是从系统启动开始的总信息,而不是两次采样之间的发生量|
|-p partition |按照每interval一个采样的速率显示给定分区的性能统计数据。这些统计信息是从系统启动开始的总信息,而不是两次采样之间的发生量|
|interval|采样之间的时间间隔|
|count|所取的样本总数|

##### 整个系统的IO统计数据

`在用-D模式运行时, vmstat提供的是系统内磁盘10系统的总体统计数据。`

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$vmstat -D
            1 disks
            2 partitions
        14661 total reads
           33 merged reads
      3369384 read sectors
        16091 milli reading
       521150 writes
         5093 merged writes
      5189564 written sectors
       110797 milli writing
            0 inprogress IO
           86 milli spent IO
```

|参数|说明|
|--|--|
|disks |系统中的磁盘总数|
|partitions |系统中的分区总数|
|total reads |读请求总数|
|merged reads |为了提升性能而被合并的不同读请求数量,这些读请求访问的是磁盘上的相邻位置|
|read sectors |从磁盘读取的扇区总数(一个扇区通常为512字节)|
|milli reading |磁盘读所花费的时间(以毫秒为单位)|
|writes|写请求的总数|
|merged writes |为了提升性能而被合并的不同写请求数量,这些写请求访问的是磁盘上的相邻位置|
|written sectors |向磁盘写入的扇区总数(一个扇区通常为512字节)|
|milli writing|磁盘写所花费的时间(以毫秒为单位)|
|inprogress IO|当前正在处理的10总数。请注意,最近版本(v3.2)的vmstat在这里有个漏洞,除以1000时其结果是错误的,几乎总是得到。|
|milli spent IO|等待1/0完成所花费的毫秒数。请注意,最近版本(v3.2)的vmstat在这里有个漏洞,其数值为1/0花费的秒数,而非毫秒数|




##### 整个系统的IO性能情况

如果你在运行vmstat时只使用了[interval]和[count]参数,其他参数没有使用,那么显示的就是默认输出。该输出中包含了三列与磁盘1/0性能相关的内容: bo, bi和wa。这些统计信息的说明如表6-2所示。

统计数据bobiwa表6-2 vmstat的磁盘1/0统计信息(默认模式)
```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$vmstat  1 2
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 1  0      0 243708   2072 1748600    0    0    13    21   55  107  2  3 95  0  0
 0  0      0 242768   2072 1748604    0    0     0    32 3352 5026  4  3 93  0  0
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

|列名|说明|
|--|--
|bo|表示前次间隔中被写入磁盘的总块数(vmstat内磁盘的典型块大小为1024字节)|
|bi|表示前次间隔中从磁盘读出的块数(vmstat内磁盘的典型块大小为1024字节)|
|wa|表示等待1/0完成所消耗的CPU时间。每秒写磁盘块的速率 |


使用vmstat取样3个样本，时间间隔为1秒
```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$vmstat 1 3
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 1  0      0 209244   2072 1751828    0    0    13    20   88  160  2  3 95  0  0
 0  0      0 209040   2072 1751828    0    0     0    12 3182 4760  2  3 95  0  0
 0  0      0 209028   2072 1751828    0    0     0    58 3053 4668  2  3 96  0  0
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```
我们可以看到，在第一秒的时候，io读取13块，写入了20块，第二读取0，写入12块，磁盘块的大小为1024字节，即系统读取速率为13/1024，等待IO的时间为O，说明当前系统IO读写轻松

##### 每个磁盘的统计信息 

`vmstat的-d选项显示的是每一个磁盘的I/O统计信息。这些统计数据与-D选项的数据类似`，表6-4对它们进行了解释。

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$vmstat  -d
disk- ------------reads------------ ------------writes----------- -----IO------
       total merged sectors      ms  total merged sectors      ms    cur    sec
sda    14661     33 3369384   16091 523799   5120 5213233  111242      0     86
```

|统计数据|说明|
|--|--|
|reads:total|读请求的总数|
|reads:merged|为了提升性能而被合并的不同读请求数量，这些读请求访问的是磁盘上的相邻位置|
|reads:sectors| 从磁盘读取的扇区总数|
|reads:ms|磁盘读所花费的时间(以毫秒为单位)|
|writes:total|写请求的总数|
|writes:merged|为了提升性能而被合并的不同写请求数量，这些写请求访问的是磁盘上的相邻位置|
|writes:sectors| 向磁盘写人的扇区总数(一个扇区通常为512字节)|
|writes:ms|磁盘写所花费的时间(以毫秒为单位)当前正在处理的I/O总数|
|IO:cur|当前正在处理的I/O总数。|
|10：sec|等待1/0完成所花费的秒数|


```bash
[root@foundation0 ~]# vmstat -d 1 2
disk- ------------reads------------ ------------writes----------- -----IO------
       total merged sectors      ms  total merged sectors      ms    cur    sec
nvme0n1  72865      5 6560405   54580 138514   1015 6715558   94026      0    149
sr0        0      0       0       0      0      0       0       0      0      0
loop0     49      0    2308      10      0      0       0       0      0      0
loop1     93      0   19822     146      0      0       0       0      0      0
loop2     77      0    3764      20      0      0       0       0      0      0
loop3     88      0   20882      73      0      0       0       0      0      0
loop4    463      0   64058    1904      0      0       0       0      0      0
nvme0n1  72865      5 6560405   54580 138514   1015 6715558   94026      0    149
sr0        0      0       0       0      0      0       0       0      0      0
loop0     49      0    2308      10      0      0       0       0      0      0
loop1     93      0   19822     146      0      0       0       0      0      0
loop2     77      0    3764      20      0      0       0       0      0      0
loop3     88      0   20882      73      0      0       0       0      0      0
loop4    463      0   64058    1904      0      0       0       0      0      0
[root@foundation0 ~]#
```

##### 统计磁盘分区的读写情况
```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$vmstat -p sda1 1 3
sda1          reads   read sectors  writes    requested writes
               15036    3381024     545880    5414724
               15036    3381024     545894    5414837
               15036    3381024     545895    5414846
```
上面我们可以看到，对于sda1物理分区来讲，有14(545880-545894)个写提交了

vmstat超过其他I/O工具的主要优势是：几乎所有的Linux发行版本都包含该工具。

vmstat提供的扩展磁盘统计信息只用于内核版本高于2.5.70的Linux系统

虽然vmstat提供了单个磁盘/分区的统计信息，但是它只给出其总量，却不给出在采样过程中的变化率。因此，要分辨哪个设备的统计数据在采样期间发生了明显的变化就显得很困难。

### 6.2.2 iostat 

iostat与vmstat相似，但它是一个专门用于显示磁盘1/0子系统统计信息的工具。

iostat提供的信息细化到每个设备和每个分区从特定磁盘读写了多少个块。(iostat中块大小一般为512字节。)

此外，iostat还可以提供大量的信息来显示磁盘是如何被利用的，以及Linux花费了多长时间来等待将请求提交到磁盘。



#### 6.2.2.1磁盘I/O性能相关的选项和输出

iostat用如下命令行调用：

`iostat [-d] [-k] [-x] [devicel] [interval[count]]`

与vmstat很相似，iostat可以定期显示性能统计信息。不同的选项可以改变iostat显示的统计数据，如表6-6所示。

iostat是一个有用的工具，它提供了迄今为止我所发现的最完整的磁盘I/O性能统计信息。虽然vmstat非常普及，并且提供了一些基本的统计信息，但是iostat更加完备。如果你的系统已经安装了iostat并且可用，那么当系统存在磁盘I/O性能问题时，首先使用的工具就应该是iostat。

|选项|说明|
|--|--|
|-d|只显示磁盘I/O的统计信息，而不是默认信息。默认信息中还包括了CPU使用情况|
|-k|按KB显示统计数据，而不是按块显示|
|-x|显示扩展性能I/O统计信息|
|device|若指定设备，则iostat只显示该设备的信息|
|interval|采样间隔时间|
|count|获取的样本总数|


iostat设备统计信息
```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$iostat
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        06/14/22        _x86_64_        (2 CPU)

avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           2.42    0.00    2.94    0.01    0.00   94.63

Device:            tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn
sda               8.18        24.59        39.45    1694748    2718599

```
|统计数据|说明|
|--|--|
|tps| 每秒传输次数。该项为每秒对设备/分区读写请求的次数|
|kB_read/s|每秒读取磁盘块的速率|
|kB_wrtn/s|每秒写入磁盘块的速率|
|kB_read|在时间间隔内读取块的总数量|
|kB_wrtn|在时间间隔内写入块的总数量|

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$iostat  -d 1 3
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        06/14/22        _x86_64_        (2 CPU)

Device:            tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn
sda               8.17        24.33        39.40    1694756    2744300

Device:            tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn
sda               9.00         0.00        37.00          0         37

Device:            tps    kB_read/s    kB_wrtn/s    kB_read    kB_wrtn
sda               0.00         0.00         0.00          0          0

┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

当你使用-x参数调用iostat时，它会显示更多关于磁盘I/O子系统的统计信息。这些扩展的统计信息如表所示。
```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$iostat -x  -dk  1 3 /dev/sda1
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        06/14/22        _x86_64_        (2 CPU)

Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
sda1              0.00     0.08    0.22    7.96    24.21    39.39    15.57     0.00    0.23    1.10    0.21   0.16   0.13

Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
sda1              0.00     0.00    0.00   15.00     0.00    60.00     8.00     0.00    0.27    0.00    0.27   0.27   0.40

Device:         rrqm/s   wrqm/s     r/s     w/s    rkB/s    wkB/s avgrq-sz avgqu-sz   await r_await w_await  svctm  %util
sda1              0.00     0.00    0.00    8.00     0.00    32.00     8.00     0.00    0.25    0.00    0.25   0.25   0.20

┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

iostat的扩展磁盘统计信息
|统计数据|说明|
|--|--|
|rrqm/s|在提交给磁盘前，被合并的读请求的数量|
|wrqm/s|在提交给磁盘前，被合并的写请求的数量|
|r/s|每秒提交给磁盘的读请求数量|
|w/s|每秒提交给磁盘的写请求数量|
|rsec/s| 每秒读取的磁盘扇区数|
|wsec/s|每秒写入的磁盘扇区数|
|rkB/s|每秒从磁盘读取了多少KB的数据|
|wkB/s|每秒向磁盘写入了多少KB的数据|
|avgrq-sz |磁盘请求的平均大小(按扇区计)|
|avgqu-sz|磁盘请求队列的平均大小。|
|await| 完成对一个请求的服务所需的平均时间(按毫秒计),该平均时间为请求在磁盘队列中等待的时间加上磁盘对其服务所需的时间|
|svctm|提交到磁盘的请求的平均服务时间(按毫秒计)。该项表明磁盘完成一个请求所花费的平均时间。与await不同，该项不包含在队列中等待的时间|
| %util|利用率|

`当avgqu-sz的值特别大的时候，且请求等待时间await远远高于请求服务svctm所花费时间，且利用率%util为100%的时候，表明该磁盘处于饱和状态。`

### 6.2.3 sar(lll)

sar可以收集Linux系统多个不同方面的性能统计信息。除了CPU和内存之外，它还可以收集关于磁盘I/O子系统的信息。

#### 6.2.3.1 磁盘I/O性能相关的选项和输出

当使用sar 来监视磁盘I/O统计数据时，你可以用如下命令行来调用它：

`sar -d [interval [count] ]`

通常，sar显示的是系统中CPU使用的相关信息。若要显示磁盘使用情况的统计信息，你必须使用-d选项。sar只能在高于2.5.70的内核版本中显示磁盘I/O统计数据。

显示信息进行了说明。
```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$sar -d 1 3
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        06/14/22        _x86_64_        (2 CPU)

23:31:21          DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
23:31:22       dev8-0      7.00      0.00     56.00      8.00      0.00      0.29      0.29      0.20

23:31:22          DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
23:31:23       dev8-0      1.00      0.00     10.00     10.00      0.00      0.00      0.00      0.00

23:31:23          DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
23:31:24       dev8-0     12.00      0.00     96.00      8.00      0.00      0.33      0.25      0.30

Average:          DEV       tps  rd_sec/s  wr_sec/s  avgrq-sz  avgqu-sz     await     svctm     %util
Average:       dev8-0      6.67      0.00     54.00      8.10      0.00      0.30      0.25      0.17
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```


|统计数据|说明|
|--|--|
|tps|每秒传输数。该项为每秒对设备/分区进行读写的次数|
|rd_sec/s|每秒读取的磁盘扇区数|
|wr_sec/s|每秒写入的磁盘扇区数|
|avgrq-sz |磁盘请求的平均大小(按扇区计)|
|avgqu-sz|磁盘请求队列的平均大小。|
|await| 完成对一个请求的服务所需的平均时间(按毫秒计),该平均时间为请求在磁盘队列中等待的时间加上磁盘对其服务所需的时间|
|svctm|提交到磁盘的请求的平均服务时间(按毫秒计)。该项表明磁盘完成一个请求所花费的平均时间。与await不同，该项不包含在队列中等待的时间|
| %util|利用率|

### 6.2.4Isof(列出打开文件)

1sof提供了一种方法来确定哪些进程打开了一个特定的文件。除了跟踪单个文件的用户外，lsof还可以显示使用了特定目录下文件的进程。同时，它还可以递归搜索整个目录树，并列出使用了该目录树内文件的进程。在要筛选哪些应用程序产生了I/O时，lsof是很有用的。

#### 6.2.4.1磁盘I/O性能相关的选项和输出

你可以使用如下命令行调用lsof来找出进程打开了哪些文件：

`1sof [-r delay] [+D directory] [+d directory] [file]`

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$yum -y install  lsof
```

通常，lsof显示的是使用给定文件的进程。但是，通过使用+d和+D选项，它可以显示多个文件的相关信息。表6-10解释了1sof的命令行选项，它们可用于追踪I/O性能问题。

sof 命令行选项
|选项|说明|
|--|--|
|-r delay|使得1sof每间隔delay秒输出一次统计数据|
|+D directory |使得1sof递归搜索给定目录下的所有文件，并报告哪些进程正在使用它们|
|+d directory|使得1sof报告哪些进程正在使用给定目录下的文件|


```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$lsof  -r 2 +D /usr/bin/
COMMAND      PID USER  FD   TYPE DEVICE  SIZE/OFF      NODE NAME
dbus-daem    539 dbus txt    REG    8,1    442080 268592945 /usr/bin/dbus-daemon
VGAuthSer    544 root txt    REG    8,1    128488 268979997 /usr/bin/VGAuthService
vmtoolsd     545 root txt    REG    8,1     48976 268980000 /usr/bin/vmtoolsd
firewalld    636 root txt    REG    8,1      7136 268591652 /usr/bin/python2.7
tuned        951 root txt    REG    8,1      7136 268591652 /usr/bin/python2.7
kubelet      957 root txt    REG    8,1 153350040 270317939 /usr/bin/kubelet
container    961 root txt    REG    8,1  48766984 268964765 /usr/bin/containerd
dockerd     1073 root txt    REG    8,1  96765968 268963237 /usr/bin/dockerd
container   1735 root txt    REG    8,1   8617984 268592410 /usr/bin/containerd-shim-runc-v2
container   1782 root txt    REG    8,1   8617984 268592410 /usr/bin/containerd-shim-runc-v2
container   1853 root txt    REG    8,1   8617984 268592410 /usr/bin/containerd-shim-runc-v
```

|统计数据|说明|
|--|--|
|COMMAND|打开该文件的命令的名称|
|PID|打开该文件的命令的PID |
|USER|打开文件的用户|
|FD|该文件的描述符。txt表示可执行文件，mem表示内存映射文件TYPE文件类型，REG表示常规文件|
|DEVICE|用主设备号和次设备号表示的设备编号|
|SIZE|文件的大小|
|NODE|文件的索引节点|

可以通过DEVICE来查看对应的设备位置
```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$ls -la /dev/ | grep sd
brw-rw----   1 root disk      8,   0 Jun 12 14:58 sda
brw-rw----   1 root disk      8,   1 Jun 12 14:58 sda1
brw-rw----   1 root disk      8,   2 Jun 12 14:58 sda2
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

`当我们通过前面的命令确定了饱和的磁盘设备。那么我们可以通过lsof来更具磁盘找到对应的文件，从而确认对应的PID`



# 第七章 网络

+ 确定系统内以太网设备的速度和双工设置(mii-tool、ethtool)。
+ 确定流经每个以太网接口的网络流量(ifconfig、sar、gkrellm、iptraf、netstat、etherape)。
+ 确定流入和流出系统的IP流量的类型(gkrellm、iptraf、netstat、etherape)。
+ 确定流入和流出系统的每种类型的IP流量(gkrellm、iptraf、etherape)。
+ 确定是哪个应用程序产生了IP流量(netstat)。

## 7.1网络1/0介绍

`Linux`和其他主流操作系统中的`网络流量`被抽象为一系列的硬件和软件层次。`链路层`，也就是最低一层，包含网络硬件，如`以太网设备`。在传送网路流量时，这一层并`不区分流量类型`，而仅仅以尽可能快的速度发送和接收`数据包(或帧)。`


链路层的上面是`网络层`。使用`互联网协议(IP)`和`网际控制报文协议(ICMP)`在机器间`寻址并路由`数据包。`IP/ICMP`尽其最大努力尝试在机器之间传递数据包，但是它们不能保证数据包是否能真正达到其目的地。
`网络层`的上面是`传输层`，它定义了`传输控制协议(TCP)和用户数据报协议(UDP)`。


+ TCP是一个可靠协议，它可以保证消息通过网络送达，如果消息无法送达它就会产生一个错误。

+ TCP的同级协议UDP，则是一个不可靠协议，它无法保证信息能够送达(为了获得最高的数据传输速率)。

UDP和TCP为IP增加了`服务`的概念。UDP和TCP接收有编号`“端口”`的消息。按照惯例，每个类型的网络服务都被分配了不同的编号。

+ 超文本传输协议(HTTP)通常为端口80
+ 安全外壳(SSH)通常为端口22
+ 文件传输协议(FTP)通常为端口23。

```bash
┌──[root@foundation0.ilt.example.com]-[~]
└─$cat  /etc/services | grep -E "^http\\s|^ssh\\s|^ftp\\s" | sort
ftp             21/sctp                 # FTP
ftp             21/tcp
ftp             21/udp          fsp fspd
http            80/sctp                         # HyperText Transfer Protocol
http            80/tcp          www www-http    # WorldWideWeb HTTP
http            80/udp          www www-http    # HyperText Transfer Protocol
ssh             22/sctp                 # SSH
ssh             22/tcp                          # The Secure Shell (SSH) Protocol
ssh             22/udp                          # The Secure Shell (SSH) Protocol
┌──[root@foundation0.ilt.example.com]-[~]
└─$
```

在`Linux`系统中，文件`/etc/services`定义了`全部的端口以及它们提供的服务类型`。


最上一层为应用层。这一层包含了各种应用程序，它们使用下面各层在网络上传输数据包。

在`Linux内核实现或控制`的是`最低三层(链路层、网络层和传输层)`。内核可以提供每层的`性能统计信息`，包括`数据流经每一层时的带宽`使用情况信息和`错误`计数信息。


### 7.1.1链路层的网络流量

在网络层次结构的最低几层，`Linux`可以侦测到`流经链路层`的数据流量的速率。

链路层，通常是`以太网`，以`帧序列`的形式将信息发送到网络上。不管应用层的交互方式是什么，链路层也会将它们分割为帧，再发送到网络上。`数据帧的最大尺寸`被称为`最大传输单位(MTU)`。可以使用网络配置工具，如`ip或ifconfig`来设置MTU。



以太网而言，最大大小一般为1500字节，虽然有些硬件支持的巨型帧可以高达9000字节。MTU的大小对网络效率有直接影响。链路层上的每一个帧都有一个小容量的头部，因此，使用大尺寸的MTU就提高了用户数据对开销(头部)的比例。但是，使用大尺寸的MTU，每个数据帧被损坏或丢弃的几率会更高。对清洁物理链路来说，大尺寸MTU通常会带来更好的性能，因为它需要的开销更小；反之，对嘈杂的链路来说，更小的MTU则通常会提升性能，因为，当单个帧被损坏时，它要重传的数据更少。

在物理层，帧流经物理网络，`Linux内核`可以收集大量有关`帧数量和类型的不同统计数据`：

+ 发送/接收：如果一个帧成功地流出或流入机器，那么它就会被计为一个已发送或已接收的帧。
+ 错误:有错误的帧(可能是因为网络电缆坏了，或双工不匹配)。
+ 丢弃:被丢弃帧的(很可能是因为内存或缓冲区容量小)。
+ 溢出:由于内核或网卡有过多的帧，因此被网络丢弃的帧。通常这种情况不应该发生。
+ 帧:由于物理级问题导致被丢弃的帧。其原因可能是循环冗余校验(CRC)错误或其他低级别的问题。
+ 多播: 这些帧不直接寻址到当前系统，而是同时广播到一组节点。
+ 压缩:一些底层接口，如`点对点协议(PPP)`或`串行线路网际协议(SLIP)设备`在把帧发送到网络上之前，会对其进行压缩。该值表示的就是被压缩帧的数量。

有些Linux网络性能工具能够显示通过每一个`网络设备`的每一种`类型的帧数`。这些工具通常需要`设备名`，因此，熟悉Linux如何对`网络设备命名`以便搞清楚哪个名字代表了哪个设备是很重要的。


`以太网设备`被命名为`ethN`，其中，`eth0`指的是第一个设备，`ethl`指的是第二个设备，以此类推。与以太网设备命名方式相同，PPP设备被命名为`pppN`。环回设备，用于与本机联网，被命名为`lo`。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ifconfig lo
lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1  (Local Loopback)
        RX packets 10250705  bytes 2227288333 (2.0 GiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 10250705  bytes 2227288333 (2.0 GiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```


### 7.1.2 协议层(传输层)网络流量

对TCP或UDP流量而言，Linux使用套接字/端口来抽象两台机器的连接。当与远程机器连接时，本地应用程序用一个网络套接字来打开远程机器上的一个端口。

Linux网络性能工具可以跟踪流经特定网络端口的数据量。由于每个服务的端口号具有唯一性，因此有可能确定流向特定服务的物理流量。

## 7.2 网络性能工具
### 7.2.1 mi-tool(媒体无关接口工具)
`mii-tool`是以太网专用硬件工具，主要用于设置以太网设备，但它也可以提供有关当前设置的信息。诸如链接速度和双工设置，对于追踪性能不佳设备的成因是非常有用。

#### 网络I/O性能相关的选项
清单7.1显示的是系统上eth0的配置信息。第一行告诉我们网络设备正在使用100BASE-T全双工连接。接下来的几行描述了机器网卡的功能，以及该网卡检测到的线路另一端网络设备的功能。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$mii-tool -v ens32
ens32: negotiated 1000baseT-FD flow-control, link ok
  product info: Yukon 88E1011 rev 3
  basic mode:   autonegotiation enabled
  basic status: autonegotiation complete, link ok
  capabilities: 1000baseT-FD 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD
  advertising:  1000baseT-FD 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD
  link partner: 1000baseT-HD 1000baseT-FD 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD
```
mi-tool提供了关于如何配置以太网设备物理层的底层信息。

### 7.2.2 ethtool

在配置和显示以太网设备统计数据方面，`ethtool`提供了与`mii-tool`相似的功能。不过，`ethtool`更加强大，包含了更多配置选项和设备统计信息。

#### 网络I/O性能相关的选项
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ethtool  ens32
Settings for ens32:
        Supported ports: [ TP ]
        Supported link modes:   10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Supported pause frame use: No
        Supports auto-negotiation: Yes
        Advertised link modes:  10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Advertised pause frame use: No
        Advertised auto-negotiation: Yes
        Speed: 1000Mb/s
        Duplex: Full
        Port: Twisted Pair
        PHYAD: 0
        Transceiver: internal
        Auto-negotiation: on
        MDI-X: off (auto)
        Supports Wake-on: d
        Wake-on: d
        Current message level: 0x00000007 (7)
                               drv probe link
        Link detected: yes
```
Speed: 1000Mb/s  


### 7.2.3 ifconfig(接口配置)

ifconfig的主要工作就是在Linux机器上安装和配置网络接口。它还提供了系统中所有网络设备的基本性能统计信息。ifconfig几乎在所有联网的Linux机器上都是可用的。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ifconfig  ens32
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.81  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:fead:e393  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:ad:e3:93  txqueuelen 1000  (Ethernet)
        RX packets 507331  bytes 69923393 (66.6 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 556567  bytes 308574743 (294.2 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

|列|说明|
|--|--|
|RX packets |设备已接收的数据包数|
|TX packets |设备已发送的数据包数|
|errors     |发送或接收时的错误数|
|dropped    |发送或接收时丢弃的数据包数|
|overruns   |网络设备没有足够的缓冲区来发送或接收一个数据包的次数|
|frame      |底层以太网帧错误的数量|
|carrier    |由于链路介质故障(如故障电缆)而丢弃的数据包数量|

ifconfig提供的统计数据显示的是自系统启动开始的累计数值。如果你将一个网络设备下线，之后又让其上线，其统计数据也不会重置。如果你按规律的间隔来运行ifconfig，就可以发现各种统计数据的变化率。这一点可以通过watch命令或shell脚本来自动实现，这两种方式我们将在下一章讨论。

### 7.2.4 ip

一些网络工具，如ifconfig，正在被淘汰，取而代之的是新的命令：ip。ip不仅可以让你对Linux联网的多个不同方面进行配置，还可以显示每个网络设备的性能统计信息。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$/sbin/ip -s link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    RX: bytes  packets  errors  dropped overrun mcast
    2300230015 10592123 0       0       0       0
    TX: bytes  packets  errors  dropped carrier collsns
    2300230015 10592123 0       0       0       0
2: ens32: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT qlen 1000
    link/ether 00:0c:29:ad:e3:93 brd ff:ff:ff:ff:ff:ff
    RX: bytes  packets  errors  dropped overrun mcast
    71388252   517977   0       0       0       0
    TX: bytes  packets  errors  dropped carrier collsns
    314758766  568063   0       0       0       0
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN mode DEFAULT
    link/ether 02:42:b7:e9:fd:fb brd ff:ff:ff:ff:ff:ff
    RX: bytes  packets  errors  dropped overrun mcast
    0          0        0       0       0       0
    TX: bytes  packets  errors  dropped carrier collsns
    0          0        0       0       0       0
    .......
```
|列|说明|
|--|--|
|bytes     |发送或接收的字节数|
|packets   |发送或接收的数据包数|
|errors    |发送或接收时发生的错误数|
|dropped   |由于网卡缺少资源，导致未发送或接收的数据包数|
|overruns  |网络没有足够的缓冲区空间来发送或接收更多数据包的次数|
|mcast     |已接收的多播数据包的数量|
|carrier   |由于链路介质故障(如故障电缆)而丢弃的数据包数量|
|co11sns   |传送时设备发生的冲突次数。当多个设备试图同时使用网络时就会发生冲突|

### 7.2.5 sar(IV)
sar提供了链路级的网络性能数据。但是，它同时还提供了一些关于传输层打开的套接字数量的基本信息。
sar使用如下命令行来收集网络统计信息：
```
sar[-n DEV | EDEV | SOCK | FULL ] [DEVICE] [linterval][count]
```

|选项|说明|
|--|--|
|-n DEV |显示每个设备发送和接收的数据包数和字节数信息|
|-n EDEV|显示每个设备的发送和接收错误信息|
|-n SoCK|显示使用套接字(TCP、UDP和RAW)的总数信息|
|-n FULL|显示所有的网络统计信息|
|interval|采样间隔时长|
|count   |采样总数|

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$sar -n DEV 1 1
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        2022年05月14日  _x86_64_        (2 CPU)

22时46分16秒     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
22时46分17秒     ens32      1.00      1.00      0.11      0.09      0.00      0.00      0.00
22时46分17秒 cali86e7ca9e9c2      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒 cali13a4549bf1e      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒 cali5a282a7bbb0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒 cali12cf25006b5      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒 cali45e02b0b21e      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒        lo    224.00    224.00     27.57     27.57      0.00      0.00      0.00
22时46分17秒 calicb34164ec79      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒     tunl0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒   docker0      0.00      0.00      0.00      0.00      0.00      0.00      0.00

平均时间:     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
平均时间:     ens32      1.00      1.00      0.11      0.09      0.00      0.00      0.00
平均时间: cali86e7ca9e9c2      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali13a4549bf1e      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali5a282a7bbb0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali12cf25006b5      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali45e02b0b21e      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:        lo    224.00    224.00     27.57     27.57      0.00      0.00      0.00
平均时间: calicb34164ec79      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:     tunl0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:   docker0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
|列|说明|
|--|--|
|rxpck/s |数据包接收速率|
|txpck/s |数据包发送速率|
|rxkB/s |kb接收速率|
|txkB/s |kb发送速率|
|rxcmp/s |压缩包接收速率|
|txcmp/s |压缩包发送速率|
|rxmcst/s|多播包接收速率|

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$sar -n EDEV 1 1
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        2022年05月14日  _x86_64_        (2 CPU)

22时53分07秒     IFACE   rxerr/s   txerr/s    coll/s  rxdrop/s  txdrop/s  txcarr/s  rxfram/s  rxfifo/s  txfifo/s
22时53分08秒     ens32      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali86e7ca9e9c2      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali13a4549bf1e      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali5a282a7bbb0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali12cf25006b5      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali45e02b0b21e      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 calicb34164ec79      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒     tunl0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒   docker0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

平均时间:     IFACE   rxerr/s   txerr/s    coll/s  rxdrop/s  txdrop/s  txcarr/s  rxfram/s  rxfifo/s  txfifo/s
平均时间:     ens32      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali86e7ca9e9c2      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali13a4549bf1e      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali5a282a7bbb0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali12cf25006b5      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali45e02b0b21e      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: calicb34164ec79      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:     tunl0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:   docker0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

|列|说明|
|--|--|
|rxerr/s  |接收错误率|
|txerr/s  |发送错误率|
|co11/s   |发送时的以太网冲突率|
|rxdrop/s |由于Linux内核缓冲区不足而导致的接收帧丢弃率|
|txdrop/s |由于Linux内核缓冲区不足而导致的发送帧丢弃率|
|txcarr/s |由于载波错误而导致的发送帧丢弃率|
|rxfram/s |由于帧对齐错误而导致的接收帧丢弃率|
|rxfifo/s |由于FIFO错误而导致的接收帧丢弃率|
|txfifo/s |由于FIFO错误而导致的发送帧丢弃率|

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$sar -n SOCK 1 3
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        2022年05月14日  _x86_64_        (2 CPU)

22时56分23秒    totsck    tcpsck    udpsck    rawsck   ip-frag    tcp-tw
22时56分24秒      3487       245         9         0         0       163
22时56分25秒      3487       245         9         0         0       165
22时56分26秒      3487       245         9         0         0       167
平均时间:      3487       245         9         0         0       165
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

|列|说明|
|--|--|
|totsck    |当前正在被使用的套接字总数|
|tcpsck    |当前正在被使用的TCP套接字总数|
|udpsck    |当前正在被使用的UDP套接字总数|
|rawsck    |当前正在被使用的RAW套接字总数|
|ip-frag   |IP分片的总数|

### 7.2.6 gkrellm

gkrellm是一个图形化监视器，有时间我们在看
### 7.2.7 iptraf

iptraf是一个实时网络监控工具。它提供了相当多的模式来监控网络接口和流量。iptraf是一种控制台应用程序，但其用户界面则是基于光标的一组菜单和窗口。

iptraf可以提供有关每个网络设备发送帧速率的信息。同时，它还能够显示TCP/IP数据包的类型和大小信息，以及哪些端口被用于网络流量。

需要装包
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$yum -y install iptraf
................
Running transaction
  正在安装    : iptraf-ng-1.1.4-7.el7.x86_64                                                               1/1
  验证中      : iptraf-ng-1.1.4-7.el7.x86_64                                                               1/1

已安装:
  iptraf-ng.x86_64 0:1.1.4-7.el7

完毕！
```
iptraf用如下命令行调用：

`iptraf[-d interface][-s interface][-t <minutes>]`

如果调用iptraf时不带参数，就会显示一个菜单，让你选择监控界面以及想要监控的信息类型。这些选项用于观察特定接口或网络服务上的网络流量。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/c7d4381b389449ac87e056dae9d26084.png)

观察所有接口的网络流量信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/af5ab4e9a28b4aadb702878a8ae1b1b0.png)

|选项|说明|
|--|--|
|-d interface|接口的详细统计信息，包括：接收信息、发送信息以及错误率信息|
|-s interface|关于接口上哪些IP端口正在被使用，以及有多少字节流经它们的统计信息|
|-t<minutes>|iptraf退出前运行的分钟数|

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng -d ens32 -t 1
```
这条命令指定iptraf显示以太网设备ets32 的详细信息并在运行1分钟后退出。我们可以看到，当前网络设备接收速率为6.13kbps，发送速率为42.81kbps

![在这里插入图片描述](https://img-blog.csdnimg.cn/2e936d8771f641e1b9222634fb892c5a.png)

iptraf显示每个UDP和TCP端口上的网络流量信息。通过端口我们可以看到每个端口对应的服务处理了多少流量，下图我们可以看到，有278kb的流量被22端口接收，有362kb的ssh数据从当前网卡发送出去

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng -s ens32 -t 10
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/9ce45a73f038434b9ce5668b2decc6c4.png)


### 7.2.8 netstat

netstat是一种基本的网络性能工具，它几乎出现在每一个联网的Linux机器上。可以用它抽取的信息包括：

+ 当前正在使用的网络套接字的数量和类型，
+ 有关流入和流出当前系统的UDP和TCP数据包数量的特定接口统计数据。
+ 能将一个套接字回溯到其特定进程或PID，这在试图确定哪个应用程序要对网络流量负责时是很有用的。

```
netstat [-p][-c] [-interfaces=cname>][-s][-t][-u] I-w]
```

如果netstat 调用时不带任何参数，它将显示系统范围内的套接字使用情况以及`Internet域`和`UNIX域套接字`的信息。(UNIX域套接字用于本机的进程通信。)为了能检索所有其可以显示的统计信息，需要从根目录运行`netstat`。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$netstat  | sed -n  '20,30p'
tcp        0      0 localhost:2379          localhost:51396         ESTABLISHED
tcp        0      0 localhost:2379          localhost:33432         ESTABLISHED
tcp        0      0 localhost:2379          localhost:33392         ESTABLISHED
tcp        0      0 localhost:35008         localhost:9099          TIME_WAIT
tcp        0      0 localhost:51154         localhost:2379          ESTABLISHED
tcp        0      0 localhost:51390         localhost:2379          ESTABLISHED
tcp        0      0 vms81.liruilongs.:53982 10.96.0.1:https         ESTABLISHED
tcp        0      0 localhost:51266         localhost:2379          ESTABLISHED
tcp        0      0 localhost:51482         localhost:2379          ESTABLISHED
tcp        0      0 localhost:2379          localhost:52920         ESTABLISHED
tcp        0      0 localhost:2379          localhost:traceroute    ESTABLISHED
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$netstat -p  | sed -n  '20,30p'
tcp        0      0 localhost:2379          localhost:51396         ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:33432         ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:33392         ESTABLISHED 2025/etcd
tcp        0      0 localhost:35008         localhost:9099          TIME_WAIT   -
tcp        0      0 localhost:51154         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:51390         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 vms81.liruilongs.:53982 10.96.0.1:https         ESTABLISHED 108260/calico-node
tcp        0      0 localhost:51266         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:51482         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:2379          localhost:52920         ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:traceroute    ESTABLISHED 2025/etcd
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$netstat -p -c | sed -n  '20,30p'
tcp        0      0 localhost:2379          localhost:33432         ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:33392         ESTABLISHED 2025/etcd
tcp        0      0 localhost:35008         localhost:9099          TIME_WAIT   -
tcp        0      0 localhost:51154         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:51390         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 vms81.liruilongs.:53982 10.96.0.1:https         ESTABLISHED 108260/calico-node
tcp        0      0 localhost:51266         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:51482         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:2379          localhost:52920         ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:traceroute    ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:51464         ESTABLISHED 2025/etcd
^C
```


|选项|说明|
|--|--|
|-p     | 给出打开每个被显示套接字的PID/程序名|
|-c      | 每秒持续更新显示信息|
|--interfaces=<name> | 显示指定接口的网络统计信息|
|-statistics/ -s     | IP/UDP/ICMP/TCP统计信息|
|--tcp / -t  |仅显示TCP套接字相关信息|
|--udp / -u  |仅显示UDP套接字相关信息|
|-raw  / -w   |仅显示RAW套接字相关信息(IP和ICMP)|

### 7.2.9 etherape
找不到包，图形化工具，先不看
## 7.3本章小结

## 9.6 优化内存使用

一般，要使用大量内存的应用程序通常会导致其他一些性能问题的产生，比如`cache缺失、转换后援缓冲器(TLB)缺失以及交换`。
图9-4展示了我们在试图弄清楚系统内存使用情况时的决策流程。

### 9.6.1内核的内存使用量在增加吗？

要追踪谁使用了系统内存，首先要确定`内核自身`是否`分配内存`。运行`slabtop`查看内核的内存总大小是否增加。如果增加了，则跳到9.6.2节。

如果内核的内存使用量没有增加，那么可能是`特定进程导致了用量增加`。要追踪是哪个`进程`该为内存使用量的增加负责，转到9.6.3节。

### 9.6.2内核使用的内存类型是什么？

如果内核的内存使用量在增加，就再次运行`slabtop`来确定内核分配的内存类型。分片的名字多少会暗示一下内存被分配的原因。通过Web搜索，你可以找到内核源代码中每个分片名字的更多详细信息。只需在内核源代码中搜索该分片的名字，并确定它被用于哪些文件，就有可能弄清楚它被分配的原因。在明确了哪些子系统分配了所有的内存后，可以尝试调整特定子系统可以消耗的最大内存量，或者减少该子系统的使用量。转到9.9节。


## 9.8 优化网络IO使用情况

当知道网络发生了问题时，Linux提供了一组工具来确定哪些应用程序涉及其中。但是，在与外部机器连接时，对网络问题的修复就不完全由你控制了。

![在这里插入图片描述](https://img-blog.csdnimg.cn/59ccbb544a9e4d029c0dc446810f8002.png)

### 网络设备发送/接收量接近理论极限了吗？

要做的第一件事就是用`ethtool`来确定每个`Ethernet`设备设置的硬件速度是多少。通过下面的配置文件我们可以看到，设置当前网卡带宽为`1000Mb/s`
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ethtool ens32
Settings for ens32:
        Supported ports: [ TP ]
        Supported link modes:   10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Supported pause frame use: No
        Supports auto-negotiation: Yes
        Advertised link modes:  10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Advertised pause frame use: No
        Advertised auto-negotiation: Yes
        Speed: 1000Mb/s
        Duplex: Full
        Port: Twisted Pair
        PHYAD: 0
        Transceiver: internal
        Auto-negotiation: on
        MDI-X: off (auto)
        Supports Wake-on: d
        Wake-on: d
        Current message level: 0x00000007 (7)
                               drv probe link
        Link detected: yes
```
如果有这些信息的记录，就可以调查是否有网络设备处于饱和状态。Ethernet设备和/或交换机容易`被误配置`，`ethtool显示每个设备认为其应运行的速度`。在确定了`每个Ethernet设备的理论极限后`，使用`iptraf(甚至是ifconfig)来明确流经每个接口的流量`。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ifconfig ens32
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.81  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:fead:e393  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:ad:e3:93  txqueuelen 1000  (Ethernet)
        RX packets 628172  bytes 109448643 (104.3 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 674109  bytes 362438519 (345.6 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```
如果有任何网络设备表现出饱和，转到下面两个节点。我们可以看到当前网卡接收的数据量为104M,发送的数据量为345M。这里我们可以通过watch监听的方式看计算每秒的流量数据。

也可以使用`iptraf`来实现，下面的命令统计流量的进出sulv
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng -d ens32 -t 1
````
![在这里插入图片描述](https://img-blog.csdnimg.cn/71e2f6a1f89543aabf979cbf6481b2c8.png)


### 网络设备产生了大量错误吗？

网络流量减缓的原因也可能是大量的网络错误。用`ifconfig`来确定是否有接口产生了大量的错误。大量错误可能是不匹配的`Ethernet卡/Ethernet交换机`设置的结果。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ifconfig ens32 | grep err
        RX errors 0  dropped 0  overruns 0  frame 0
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

#### 设备上流量的类型是什么？
如果特定设备正在服务大量的数据，使用`iptraf`可以跟踪该设备发送和接收的流量类型。当知道了设备处理的流量类型后，转到下面的节点
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng -s ens32 -t 10
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/87562c56ba8e4455903f1d0613704ded.png)


#### 特定进程要为流量负责吗？

接下来，我们想要确定是否有特定进程要为这个流量负责。使用`netstat的 -p 选项`来查看是否有进程在处理流经网络端口的类型流量。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$netstat -p | grep 2379
tcp        0      0 localhost:33354         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:33416         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:51498         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:53062         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:2379          localhost:52520         ESTABLISHED 2025/etcd
```
如果有应用程序要对此负责，转到`[流量是哪个远程系统发送的]`节点。如果没有这样的程序，则转到`[哪个应用程序套接字要为流量负责]`。

#### 流量是哪个远程系统发送的？

如果没有应用程序应对这个流量负责，那么就可能是网络上的某些系统用无用的流量攻击了你的系统。要确定是哪些系统发送了这些流量，要使用`iptraf或etherape`。


如果可能的话，请与系统所有者联系，并尝试找出发生这种情况的原因。如果所有者无法联系上，可以在Linux内核中设置ipfilters，永久丢弃这个特定的流量，或者是在远程机与本地机之间建立防火墙来拦截该流量。


#### 哪个应用程序套接字要为流量负责？

确定使用了哪个套接字要分两步。这部分完全看不懂，先记录下，

+ 第一步，用`strace -e trace=file`跟踪应用程序所有的`I/0系统`调用。这能显示进程是从哪些文件描述符进行读写的。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$yum -y install strace
```
跟踪执行`kubectl get nodes`涉及到的文件读写
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get nodes
NAME                         STATUS     ROLES                  AGE    VERSION
vms81.liruilongs.github.io   Ready      control-plane,master   153d   v1.22.2
vms82.liruilongs.github.io   Ready      <none>                 153d   v1.22.2
vms83.liruilongs.github.io   NotReady   <none>                 153d   v1.22.2
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$strace -e trace=file  kubectl get nodes
execve("/usr/bin/kubectl", ["kubectl", "get", "nodes"], 0x7ffc888b4e40 /* 22 vars */) = 0
openat(AT_FDCWD, "/sys/kernel/mm/transparent_hugepage/hpage_pmd_size", O_RDONLY) = -1 ENOENT (没有那个文件或目 录)
readlinkat(AT_FDCWD, "/proc/self/exe", "/usr/bin/kubectl", 128) = 16
openat(AT_FDCWD, "/usr/bin/kubectl", O_RDONLY|O_CLOEXEC) = 6
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
openat(AT_FDCWD, "/root/.kube/cache/discovery/192.168.26.81_6443/policy/v1beta1/serverresources.json", O_RDONLY|O_CLOEXEC) = 6
..........
```
+ 第二步，通过查看proc文件系统，将这些文件描述符映射回套接字。`/proc/<pid>/fd/`中的文件是从文件描述符到实际文件或套接字的符号链接。该目录下的`1s-1a`会显示特定进程全部的文件描述符。名字中带有socket的是网络套接字。之后就可以利用这些信息来确定程序中的哪个套接字产生了这些通信。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ps -elF | grep etcd
4 S root       2025   2004  2  80   0 - 2803899 futex_ 96656 1 5月14 ?       00:33:14 etcd --advertise-client-urls=https://192.168.26.81:2379 --cert-file=/etc/kubernetes/pki/etcd/server.crt --client-cert-auth=true --data-dir=/var/lib/etcd --initial-advertise-peer-urls=https://192.168.26.81:2380 --initial-cluster=vms81.liruilongs.github.io=https://192.168.26.81:2380 --key-file=/etc/kubernetes/pki/etcd/server.key --listen-client-urls=https://127.0.0.1:2379,https://192.168.26.81:2379 --listen-metrics-urls=http://127.0.0.1:2381 --listen-peer-urls=https://192.168.26.81:2380 --name=vms81.liruilongs.github.io --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt --peer-client-cert-auth=true --peer-key-file=/etc/kubernetes/pki/etcd/peer.key --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt --snapshot-count=10000 --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
4 S root      14196  14020 10  80   0 - 311578 futex_ 445044 0 5月14 ?       01:54:30 kube-apiserver --advertise-address=192.168.26.81 --allow-privileged=true --token-auth-file=/etc/kubernetes/pki/liruilong.csv --authorization-mode=Node,RBAC --client-ca-file=/etc/kubernetes/pki/ca.crt --enable-admission-plugins=NodeRestriction --enable-bootstrap-token-auth=true --etcd-cafile=/etc/kubernetes/pki/etcd/ca.crt --etcd-certfile=/etc/kubernetes/pki/apiserver-etcd-client.crt --etcd-keyfile=/etc/kubernetes/pki/apiserver-etcd-client.key --etcd-servers=https://127.0.0.1:2379 --kubelet-client-certificate=/etc/kubernetes/pki/apiserver-kubelet-client.crt --kubelet-client-key=/etc/kubernetes/pki/apiserver-kubelet-client.key --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname --proxy-client-cert-file=/etc/kubernetes/pki/front-proxy-client.crt --proxy-client-key-file=/etc/kubernetes/pki/front-proxy-client.key --requestheader-allowed-names=front-proxy-client --requestheader-client-ca-file=/etc/kubernetes/pki/front-proxy-ca.crt --requestheader-extra-headers-prefix=X-Remote-Extra- --requestheader-group-headers=X-Remote-Group --requestheader-username-headers=X-Remote-User --secure-port=6443 --service-account-issuer=https://kubernetes.default.svc.cluster.local --service-account-key-file=/etc/kubernetes/pki/sa.pub --service-account-signing-key-file=/etc/kubernetes/pki/sa.key --service-cluster-ip-range=10.96.0.0/12 --tls-cert-file=/etc/kubernetes/pki/apiserver.crt --tls-private-key-file=/etc/kubernetes/pki/apiserver.key
0 S root      24735  24319  0  80   0 - 28170 pipe_w   980   0 02:08 pts/1    00:00:00 grep --color=auto etcd
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cd  /proc/2025/fd/
┌──[root@vms81.liruilongs.github.io]-[/proc/2025/fd]
└─$ls
0    102  109  116  121  126  131  136  18  22  27  31  36  40  45  5   54  59  64  71  78  83  9   95
1    103  11   117  122  127  132  14   19  23  28  32  37  41  46  50  55  6   65  73  79  84  90  97
.......
```

#### 最后的手段
当你看到这里的时候，你的问题可能得到也可能没有得到解决，但是，你会获取大量描述它的信息。在搜索引擎上看看他们是如何解决问题的。尝试一个解决方案，并观察系统或应用程序的行为是否发生了变化。每次尝试新方案时，请转到流程最开始重新开始系统诊断，因为，每一个修复都可能会让应用程序的行为发生变化。