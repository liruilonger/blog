---
title: 关于Git的一些笔记
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-07-07 11:05:31/关于Git的一些笔记.html'
mathJax: false
date: 2022-07-07 19:05:31
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 今天和小伙伴们分享一些Git分支的笔记
+ 学习的原因，关于Git分支之前简单看了下，就直接开始玩了，结果Git日志就变成了下面的样子
+ ![在这里插入图片描述](https://img-blog.csdnimg.cn/bcc6434006844d46b0f51ed8f9913b90.png)

+ 看着很糟心，所以觉得有必要系统的学习下
+ 博文为`《Pro Git》`读书笔记整理
+ 感谢开源这本书的作者和把这本书翻译为中文的大佬们
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

#### 名词解释

你工作目录下的每一个文件都不外乎这两种状态：已跟踪 或 未跟踪。

+ 已跟踪：Git已经知道的文件，被纳入了GIt版本控制的文件，上一次快照中有它们的记录,以跟踪的文件状态可能是未修改，已修改或已放入`暂存区`。

+ 未跟踪：既不存在于上次快照的记录中，也没有被放入`暂存区`。工作目录下新建一个文件，不做任何操作，此时这个文件为未跟踪，初次克隆某个仓库的时候，工作目录中的所有文件都属于已跟踪文件，未跟踪不可以通过`git status` 命令来查看状态，只有通过`git add`命令添加后，才可以开始跟踪




### 常见配置

Git自带一个`git config`的工具来帮助设置控制 Git外观和行为的配置变量。
```
┌──[root@liruilongs.github.io]-[~/git_demo]
└─$ git config
用法：git config [选项]

配置文件位置
    --global              使用全局配置文件
    --system              使用系统级配置文件
    --local               使用版本库级配置文件
    -f, --file <文件>     使用指定的配置文件
    --blob <blob-id>      read config from given blob object

操作
    --get                 获取值：name [value-regex]
    --get-all             获得所有的值：key [value-regex]
    --get-regexp          根据正则表达式获得值：name-regex [value-regex]
    --replace-all         替换所有匹配的变量：name value [value_regex]
    --add                 添加一个新的变量：name value
    --unset               删除一个变量：name [value-regex]
    --unset-all           删除所有匹配项：name [value-regex]
    --rename-section      重命名小节：old-name new-name
    --remove-section      删除一个小节：name
    -l, --list            列出所有
    -e, --edit            打开一个编辑器
    --get-color <slot>    找到配置的颜色：[默认]
    --get-colorbool <slot>
                          找到颜色设置：[stdout-is-tty]

类型
    --bool                值是 "true" 或 "false"
    --int                 值是十进制数
    --bool-or-int         值是 --bool or --int
    --path                值是一个路径(文件或目录名)

其它
    -z, --null            终止值是NUL字节
    --includes            查询时参照 include 指令递归查找

```

这些变量存储在三个不同的位置：

+ /etc/gitconfig文件：包含系统上每一个用户及他们仓库的通用配置。如果在执行git config时带上
--system选项，那么它就会读写该文件中的配置变量。(由于它是系统配置文件，因此你需要管理员或超级用户权限来修改它。)


+ ~/.gitconfig或~/.config/git/config文件：只针对当前用户。你可以传递--g1oba1选项让Git读写此文件，这会对你系统上所有的仓库生效。


+ 当前使用仓库的Git目录中的config文件(即.git/config)：针对该仓库。你可以传递--1ocal选项让Git 强制读写此文件，虽然默认情况下用的就是它。。(当然，你需要进入某个Git 仓库中才能让该选项生效。)每一个级别会覆盖上一级别的配置，所以.git/config的配置变量会覆盖/etc/gitconfig中的配置变量。


在Windows系统中，Git会查找SHOME目录下(一般情况下是C:\Users\SUSER)的.gitconfig文件。
```bash
PS C:\Users\lenovo> pwd

Path
----
C:\Users\lenovo

PS C:\Users\lenovo> cat .\.gitconfig
[winUpdater]
        recentlySeenVersion = 2.36.1.windows.1
[gui]
        encoding = utf-8
[user]
        name = 2027000909
        email = Li.ruilong@iwhalecloud.com
PS C:\Users\lenovo>
```
如果你在Windows 上使用Git2.x以后的版本，那么还有一个系统级的配置文件,在C:\ProgramData\Git\config
```bash
PS C:\ProgramData\Git> cat .\config
[core]
        symlinks = false
        autocrlf = true
        fscache = true
[color]
        diff = auto
        status = auto
        branch = auto
        interactive = true
[help]
        format = html
[rebase]
        autosquash = true
PS C:\ProgramData\Git>
```

你可以通过以下命令查看所有的配置以及它们所在的文件！
```bash
PS C:\Users\lenovo> git config --list --show-origin
file:"C:\\ProgramData/Git/config"       core.symlinks=false
file:"C:\\ProgramData/Git/config"       core.autocrlf=true
file:"C:\\ProgramData/Git/config"       core.fscache=true
file:"C:\\ProgramData/Git/config"       color.diff=auto
file:"C:\\ProgramData/Git/config"       color.status=auto
file:"C:\\ProgramData/Git/config"       color.branch=auto
file:"C:\\ProgramData/Git/config"       color.interactive=true
file:"C:\\ProgramData/Git/config"       help.format=html
file:"C:\\ProgramData/Git/config"       rebase.autosquash=true
file:F:/VersionControl/Git/mingw64/etc/gitconfig        http.sslbackend=openssl
file:F:/VersionControl/Git/mingw64/etc/gitconfig        http.sslcainfo=F:/VersionControl/Git/mingw64/ssl/certs/ca-bundle.crt
file:C:/Users/lenovo/.gitconfig winupdater.recentlyseenversion=2.36.1.windows.1
file:C:/Users/lenovo/.gitconfig gui.encoding=utf-8
file:C:/Users/lenovo/.gitconfig user.name=2027000909
file:C:/Users/lenovo/.gitconfig user.email=Li.ruilong@iwhalecloud.com
file:.git/config        core.repositoryformatversion=0
file:.git/config        core.filemode=false
file:.git/config        core.bare=false
file:.git/config        core.logallrefupdates=true
file:.git/config        core.symlinks=false
file:.git/config        core.ignorecase=true
file:.git/config        gui.wmstate=normal
file:.git/config        gui.geometry=805x483+78+78 163 218
PS C:\Users\lenovo>
```
#### 检查配置信息
如果想要检查你的配置，可以使用 git config --list 命令来列出所有 Git 当时能找到的配置。

你可能会看到重复的变量名，因为 Git 会从不同的文件中读取同一个配置(例如：/etc/gitconfig 与
~/.gitconfig)。 这种情况下，Git 会使用它找到的每一个变量的最后一个配置。


```bash
$ git config --list
```
你可以通过输入 git config <key>： 来检查 Git 的某一项配置
```bash
$ git config user.name
```


想获得 git config 命令的手册
```bash
 git help config
```

查询 Git 中该变量的 原始 值，它会告诉你哪一个配置文件最后设置了该值：
```bash
$ git config --show-origin rerere.autoUpdate
```






### 获取帮助
```bash
$ git help <verb>
$ git <verb> --help
$ man git-<verb>
```

用户信息
```bash
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

#### 状态简览
使用 git status -s 命令或 git status --short 命令，你将得到一种格式更为紧凑的输出。

```bash
$ git status -s
 M README
MM Rakefile
A lib/git.rb
M lib/simplegit.rb
?? LICENSE.txt
```
+ 新添加的未跟踪文件前面有`？？`标记，
+ 新添加到暂存区中的文件前面有`A`标记
+ 修改过的文件前面有`M`标记。

输出中有两栏，`左栏指明了暂存区的状态，右栏指明了工作区的状态`。例如，上面的状态报告显示：

+ README文件在工作区已修改但尚未暂存，
+ 1ib/simplegit.rb文件已修改且已暂存。
+ Rakefile文件已修，暂存后又作了修改，因此该文件的修改中既有已暂存的部分，又有未暂存的部分。


```bash
┌──[root@liruilongs.github.io]-[~]
└─$ git clone  http://192.168.26.55/root/blog.git
正克隆到 'blog'...
remote: Counting objects: 1256, done.
remote: Compressing objects: 100% (444/444), done.
remote: Total 1256 (delta 430), reused 1224 (delta 403)
接收对象中: 100% (1256/1256), 17.56 MiB | 14.28 MiB/s, done.
处理 delta 中: 100% (430/430), done.
```

```bash
┌──[root@liruilongs.github.io]-[~/blog]
└─$ git status
# 位于分支 master
无文件要提交，干净的工作区
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```
# Git 基础

通常有两种获取 Git 项目仓库的方式：
1. 将尚未进行版本控制的本地目录转换为 Git 仓库；
2. 从其它服务器 克隆 一个已存在的 Git 仓库。

两种方式都会在你的本地机器上得到一个工作就绪的 Git 仓库。


克隆远程仓库

如果你想在克隆远程仓库的时候，自定义本地仓库的名字，你可以通过额外的参数指定新的目录名：
```bash
$ git clone https://github.com/libgit2/libgit2 mylibgit
```

添加远程仓库

运行 git remote add <shortname> <url> 添加一个新的远程 Git 仓库，同时指定一个方便使用的简写
```bash
$ git remote
origin
$ git remote add pb https://github.com/paulboone/ticgit
$ git remote -v
origin https://github.com/schacon/ticgit (fetch)
origin https://github.com/schacon/ticgit (push)
pb https://github.com/paulboone/ticgit (fetch)
pb https://github.com/paulboone/ticgit (push)

```


![在这里插入图片描述](https://img-blog.csdnimg.cn/0b9a51c90a614ba2a25610202a7f4bb3.png)

# Git 分支

关于Git分支管理的一些建议，一般可以在本地解决的问题要在本地解决，本地合并(要申请合并到的远程分支)，本地解决冲突，如果自己的分支，只顾着开发，不做合并或者变基的操作，在自己的分支上越走越远，慢慢的和远程主主分支差的太远，申请合并一堆冲突...，那是一件很糟糕的事，尤其是对代码的审核者而言，会认为申请合并者是一个不会使用的Git的开发者。

在学习Git分支之前，我们来了解一些理论知识

## 分支理论

Git 保存的不是`文件`的`变化或者差异`，而是一系列不同`时刻的快照`。

在进行`提交`操作时，`Git`会保存一个`提交对象(commit object)`。包括：
+ 一个指向`暂存内容快照`的`指针`
+ 作者的`姓名`和`邮箱`
+ 提交时`输入`的信息
+ 指向它的`父对象`的`指针`

`首次提交`产生的提交对象没有`父对象`，`普通提交`操作产生的提交对象有一个`父对象`，而由`多个分支合并产生的提交对象`有`多个父对象`

假设现在有一个工作目录，里面包含了三个将要被`暂存(add)和提交(commit)的文件`。暂存操作会为`每一个文件`计算`校验和`(使用SHA-1哈希算法)，然后会把当前版本的`文件快照保存到Git仓库中(Git使用blob对象来保存它们)`，最终将`校验和`加入到`暂存区域`等待`提交`：


当使用`git commit `进行提交操作时，Git会先计算每一个`子目录(本例中只有项目根目录)的校验和`，然后在 Git 仓库中这些校验和保存为树对象。随后，Git 便会创建一个提交对象，它除了包含上面提到的那些信息外，还包含指向这个树对象(项目根目录)的指针。如此一来，Git就可以在需要的时候重现此次保存的快照。

现在，Git仓库中有五个对象：`三个blob对象(保存着文件快照)`、`一个树对象(记录着目录结构和blob对象索引)`以及`一个提交对象(包含着指向前述树对象的指针和所有提交信息)`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/a4490ca65e474ed197b55763e6435530.png)



做些修改后再次提交，那么这次产生的`提交对象`会包含一个指向`上次提交对象(父对象)的指针`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/d8b72b2d967e4528a9196d5bb6e8dc17.png)


`Git的分支，其实本质上仅仅是指向提交对象的可变指针`。Git的默认分支名字是`master`。在多次提交操作之后，你其实已经有一个指向最后那个提交对象的`master分支`。`master分支`会在每次提交时自动向前移动。

`Git的master分支`并不是一个特殊分支。它就跟其它分支完全没有区别。之所以几乎每一个仓库都有`master分支`，是因为`git init`命令默认创建它，并且大多数人都懒得去改动

```bash
PS E:\docker\git_example> git init
Initialized empty Git repository in E:/docker/git_example/.git/
PS E:\docker\git_example> git status
On branch master
.........
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/76afb07cf72b4bca9ca56901d9480bcf.png)



## 分支创建

**Git 是怎么创建新分支的呢？**

很简单，它只是`为你创建了一个可以移动的新的指针`。在当前所在的`提交对象`上创建一个指针。比如，创建一个testing分支，你需要使用`git branch`命令：

```bash
git branch testing
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/aefff249844744139e98d50e31bbc7d9.png)

**Git 又是怎么知道当前在哪一个分支上呢？** 

也很简单，它有一个名为` HEAD 的特殊指针`。在Git中，它是一个指针，`指向当前所在的本地分支`(译注：将HEAD想象为当前分支的别名)。`git branch`命令仅仅创建一个新分支，并不会自动切换到新分支中去。

![在这里插入图片描述](https://img-blog.csdnimg.cn/a7fbf3493de74c78b8bb7c316b9fced4.png)

你可以简单地使用` git log `命令查看各个分支当前所指的对象。 提供这一功能的参数是` --decorate`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/f2437111aeeb496e83db2a91468cf2b4.png)

```bash
$ git log --oneline --decorate
f30ab (HEAD -> master, testing) add feature #32 - ability to add new
formats to the central interface
34ac2 Fixed bug #1328 - stack overflow under certain conditions
98ca9 The initial commit of my project
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/fb4d5d14afc7408c9934add08395cea8.png)

## 分支切换

要切换到一个已存在的分支，你需要使用` git checkout `命令。 我们现在切换到新创建的 testing 分支去：这样 HEAD 就指向 testing 分支了。

```
$ git checkout testing
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/0b7deece082641d89b63df9ad319dad3.png)


在切换分支之后重新提交，新的提交会提交到新的分支

```bash
$ vim test.rb
$ git commit -a -m 'made a change'
```

你的`testing分支`向前移动了，但是`master分支`却没有，它仍然指向运行`git checkout`时所指的对象。


![在这里插入图片描述](https://img-blog.csdnimg.cn/2ecefa0b11ad4cf88b023f314772ee6c.png)

现在我们切换回master分支看看：

```bash
$ git checkout master
```


![在这里插入图片描述](https://img-blog.csdnimg.cn/05fbd7a7183a4b67a680c59f5f443fde.png)

这条命令做了两件事。

+ 一是使`HEAD指回master分支`
+ 二是将`工作目录恢复成master分支所指向的快照内容`

也就是说，你现在做修改的话，项目将始于一个`较旧的版本`。本质上来讲，这就是忽略testing分支所做的修改，以便于向另一个方向进行开发。


需要注意的是，分支切换会改变你工作目录中的文件，

在切换分支时，一定要注意你工作目录里的文件会被改变。即便有被跟踪但是没有提交的文件会被自动覆盖掉，如果是切换到一个较旧的分支，你的工作目录会恢复到该分支最后一次提交时的样子。


**如果Git不能干净利落地完成这个任务，它将禁止切换分支。**


```
$ vim test.rb
$ git commit -a -m 'made other changes'
```
现在，这个项目的提交历史已经产生了分叉，因为刚才你创建了一个新分支，并切换过去进行了一些工作，随后又切换回master分支进行了另外一些工作。上述两次改动针对的是不同分支：

你可以在不同分支间不断地来回切换和工作，并在时机成熟时将它们合并起来。而所有这些工作，你需要的命令只有`branch、checkout和commit。`

![在这里插入图片描述](https://img-blog.csdnimg.cn/86638918e0a24e93a7e35936b0ea60f8.png)

你可以简单地使用git log命令查看分叉历史。运行`git log --oneline  --decorate  --graph  --al1`，它会输出你的提交历史、各个分支的指向以及项目的分支分叉情况。

![在这里插入图片描述](https://img-blog.csdnimg.cn/bc852cc915d94e919d1a294041379f4f.png)


由于Git的分支实质上仅是`包含所指对象校验和(长度为40的SHA-1值字符串)的文件`，所以它的创建和销毁都异常高效。创建一个新分支就相当于`往一个文件中写入41个字节(40个字符和1个换行符)`，如此的简单能不快吗？


创建新分支的同时切换过去

通常我们会在创建一个新分支后立即切换过去，这可以用` git checkout -b  <newbranchname>` 一条命令搞定。

来看一个Demo 

## 分支的新建与合并


1. 开发某个网站。
2. 为实现某个新的用户需求，创建一个分支。
3. 在这个分支上开展工作。

正在此时，你突然接到一个电话说有个很严重的问题需要紧急修补。 你将按照如下方式来处理：

1. 切换到你的线上分支(production branch)。
2. 为这个紧急任务新建一个分支，并在其中修复它。
3. 在测试通过之后，切换回线上分支，然后合并这个修补分支，最后将改动推送到线上分支。
4. 切换回你最初工作的分支上，继续工作


### 新建分支

首先，我们假设你正在你的项目上工作，并且在 master 分支上已经有了一些提交。

![在这里插入图片描述](https://img-blog.csdnimg.cn/75d19a882078471ab6c13fc8ab5de490.png)

现在，你已经决定要解决你的公司使用的问题追踪系统中的`#53问题`。想要新建一个分支并同时切换到那个分支上，你可以运行一个带有`-b`参数的`git checkout`命令：

```bash
$ git checkout -b iss53
Switched to a new branch "iss53"
```
你继续在` #53 `问题上工作，并且做了一些提交。 在此过程中，iss53 分支在不断的向前推进，因为你已经检出到该分支 (也就是说，你的 HEAD 指针指向了 iss53 分支)
```
$ vim index.html
$ git commit -a -m 'added a new footer [issue 53]'

```

![在这里插入图片描述](https://img-blog.csdnimg.cn/3cf4906a64b9415bb8456b04b685e136.png)


现在你接到那个电话，有个紧急问题等待你来解决。有了Git的帮助，你不必把这个紧急问题和iss53的修改混在一起，你也不需要花大力气来还原关于`53#问题`的修改，然后再添加关于这个紧急问题的修改，最后将这个修改提交到线上分支。你所要做的仅仅是`切换回master分支`。

但是，在你这么做`之前`，要留意你的`工作目录`和`暂存区`里那些还没有`被提交的修改`，它可能会和你即将`检出的分支产生冲突`从而`阻止Git切换到该分支`。最好的方法是，在你切换分支之前，保持好一个干净的状态。

有一些方法可以绕过这个问题(即，`暂存(stashing)和清理(clean)`)。现在，我们假设你已经把你的`修改全部提交`了，这时你可以切换回`master`分支了：

这里需要注意的是一定要切回master分支之后在新建分支，不要在iss53的分支上新建分支。
```
$ git checkout master
Switched to branch 'master'
```
这个时候，你的工作目录和你在`开始#53问题之前`一模一样，现在你可以专心修复紧急问题了。请牢记：`当你切换分支的时候，Git会重置你的工作目录，使其看起来像回到了你在那个分支上最后一次提交的样子`。Git会自动添加、删除、修改文件以确保此时你的工作目录和这个分支最后一次提交时的样子一模一样。



接下来，你要修复这个紧急问题。我们来建立一个`hotfix分支`，在该分支上工作直到问题解决：
```
$ git checkout -b hotfix
Switched to a new branch 'hotfix'
$ vim index.html
$ git commit -a -m 'fixed the broken email address'
[hotfix 1fb7853] fixed the broken email address
 1 file changed, 2 insertions(+)
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/760ac4dade3f4ed4b8dc40e07907bf9a.png)

你可以运行你的测试，确保你的修改是正确的，然后将`hotfix分支合并回你的master分支`来部署到线上。

这里需要注意，在分支合并的时候，当前分支是合并后的分支，被合并的分支为`git merge xxx` 指定的分支。要把B合并到A，那么当前分支应该为A，使用`git merge B`的命令合并

你可以使用git merge命令来达到上述目的：

```bash
$ git checkout master
$ git merge hotfix
Updating f42c576..3a0874c
Fast-forward
 index.html | 2 ++
 1 file changed, 2 insertions(+)
```

**Fast-forward** :在合并的时候，你应该注意到了“快进(fast-forward)”这个词。由于你想要合并的分支hotfix所指向的提交C4是你所在的提交C2的直接后继，因此Git会直接将指针向前移动。

换句话说，当你`试图合并两个分支时，如果顺着一个分支走下去能够到达另一个分支`，那么Git在合并两者的时候，只会简单的`将指针向前推进(指针右移)`，因为这种情况下的合并操作`没有需要解决的分歧`——这就叫做“`快进(fast-forward)`”。


现在，最新的修改已经在master分支所指向的提交快照中，你可以着手发布该修复了。
![在这里插入图片描述](https://img-blog.csdnimg.cn/982b8fd635d540a78903ccea66aaa5e0.png)

关于这个紧急问题的解决方案发布之后，你准备回到被打断之前时的工作中。然而，你应该先`删除hotfix分支`，因为你已经不再需要它了,它master分支已经指向了同一个位置。你可以使用带`-d选项的git branch命令来删除分支`：

```bash
$ git branch -d hotfix
Deleted branch hotfix (3a0874c).
```
现在你可以切换回你正在工作的分支继续你的工作，也就是针对 #53 问题的那个分支(iss53 分支)。

```bash
$ git checkout iss53
Switched to branch "iss53"
$ vim index.html
$ git commit -a -m 'finished the new footer [issue 53]'
[iss53 ad82d7a] finished the new footer [issue 53]
1 file changed, 1 insertion(+)
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/4e1fd5f28c23435787f544d8be0f08e4.png)


你在`hotfix`分支上所做的工作并没有包含到`iss53分支`中。如果你需要`拉取hotfix所做的修改`，

你可以使用`git merge master`命令将`master分支合并入iss53分支`，或者你也可以等到iss53分支完成其使命，再将其合并回`master分支`。


假设你已经修正了#53问题，并且打算将你的工作合并入master分支。为此，你需要`合并iss53分支到master分支`，这和之前你合并hotfix分支所做的工作差不多。你只需要`检出到你想合并入的分支，然后运行git merge命令`：
```
$ git checkout master
Switched to branch 'master'
$ git merge iss53
Merge made by the 'recursive' strategy.
index.html | 1 +
1 file changed, 1 insertion(+)
```
这和你之前合并hotfix分支的时候看起来有一点不一样。在这种情况下，你的开发历史从一个更早的地方开始分叉开来(diverged)。因为，`master分支所在提交并不是iss53分支所在提交的直接祖先`，Git不得不做一些额外的工作。

出现这种情况的时候，Git会使用`两个分支的未端所指的快照(C4和C5)`以及这两个分支的`公共祖先(C2)`，做一个简单的`三方合并`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/9d0f07d99c424591a04039204e4108b4.png)

和之前将分支指针向前推进所不同的是，`Git将此次三方合并的结果做了一个新的快照并且自动创建一个新的提交指向它`。这个被称作`一次合并提交`，它的特别之处在于他有`不止一个父提交`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/5c50b68f2c1e419fbf7b76357c18ed71.png)

既然你的修改已经合并进来了，就不再需要iss53分支了。现在你可以在任务追踪系统中关闭此项任务，并删除这个分支。

```
$ git branch -d iss53
```

上面讲到有一些方法可以绕过这个问题(即，`暂存(stashing)和clean)`，我们来看下

### 贮藏与清理

有时，当你在项目的一部分上已经工作一段时间后，所有东西都进入了混乱的状态， 而这时你想要切换到另一个分支做一点别的事情。 问题是，你不想仅仅因为过会儿回到这一点而为做了一半的工作创建一次提交。 针对这个问题的答案是` git stash `命令。

`贮藏(stash)`会处理工作目录的`脏的状态`——即`跟踪文件的修改与暂存的改动`——然后将`未完成的修改保存到一个栈`上，而你可以在任何时候`重新应用这些改动(甚至在不同的分支上)`。

#### 贮藏工作

运行 git status，可以看到有改动的状态：
```bash
$ git status
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
  modified: index.html
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working
directory)
  modified: lib/simplegit.rb
```

**现在想要切换分支，但是还不想要提交之前的工作；所以贮藏修改。将新的贮藏推送到栈上，运行`git stash`或`git stash push`：**

```bash
$ git stash
Saved working directory and index state \
  "WIP on master: 049d078 added the index file"
HEAD is now at 049d078 added the index file
(To restore them type "git stash apply")

```
可以看到工作目录是干净的了：
```bash
$ git status
# On branch master
nothing to commit, working directory clean
```

此时，你可以切换分支并在其他地方工作；你的修改被存储在栈上。要查看贮藏的东西，可以使用`git stash list：`

```bash
$ git stash list
stash@{0}: WIP on master: 049d078 added the index file
stash@{1}: WIP on master: c264051 Revert "added file_size"
stash@{2}: WIP on master: 21d80a5 added number to log

```
在本例中，有两个之前的贮藏，所以你接触到了三个不同的贮藏工作。可以通过原来 `stash`命令的帮助提示中的命令将你刚刚贮藏的工作重新应用：`git stash apply`。

如果想要应用其中一个更旧的贮藏，可以通过名字指定它，像这样：`git stash apply stashe@{2}`。如果不指定一个贮藏，Git认为指定的是最近的贮藏：

```bash
$ git stash apply
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working
directory)
  modified: index.html
  modified: lib/simplegit.rb
no changes added to commit (use "git add" and/or "git commit -a")
```

`通过stash存储修改和新建分支比起来`，一大优点是，它在应用时并不是必须要有一个干净的工作目录，或者要应用到同一分支才能成功应用贮藏。

`stash`可以在一个分支上保存一个贮藏，切换到另一个分支，然后尝试重新应用这些修改。当应用贮藏时工作目录中也可以有修改与未提交的文件——如果有任何东西不能干净地应用，`Git会产生合并冲突`。

文件的改动被重新应用了，但是之前`暂存的文件(add)却没有重新暂存`。想要那样的话，必须使用`--index`选项来运行`git stash apply`命令，来尝试重新应用暂存的修改。如果已经那样做了，那么你将回到原来的位置：
```bash
$ git stash apply --index
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
  modified: index.html
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working
directory)
  modified: lib/simplegit.rb
```
应用选项只会尝试应用贮藏的工作——在堆栈上还有它。可以运行`git stash drop`加上将要移除的贮藏的名字来移除它：

```bash
$ git stash list
stash@{0}: WIP on master: 049d078 added the index file
stash@{1}: WIP on master: c264051 Revert "added file_size"
stash@{2}: WIP on master: 21d80a5 added number to log
$ git stash drop stash@{0}
Dropped stash@{0} (364e91f3f268f0900bc3ee613f9f733e82aaed43)
```

可能有小伙伴会说，如过stash之后，我忘记了有过stash的操作，通过status命令想看到文件状态？

```bash
$ git status -s
M index.html
 M lib/simplegit.rb
```
`git stash`命令的`--keep-index`选项。它告诉`Git不仅要贮藏所有已暂存的内容，同时还要将它们保留在索引中。`，即我们可以通过status命令开查看状态
```bash
$ git stash --keep-index
Saved working directory and index state WIP on master: 1b65b17 added the
index file
HEAD is now at 1b65b17 added the index file
```

```bash
$ git status -s
M index.html
```
默认情况下，`git stash`只会`贮藏已修改`和`暂存的已跟踪文件`。如果指定`--include-untracked`或`-u`选项，Git也会`贮藏任何未跟踪文件`。
```bash
$ git status -s
M index.html
 M lib/simplegit.rb
?? new-file.txt
$ git stash -u
Saved working directory and index state WIP on master: 1b65b17 added the
index file
HEAD is now at 1b65b17 added the index file
$ git status -s
$
```
然而，在贮藏中包含未跟踪的文件仍然不会包含`明确忽略`的文件。要额外包含忽略的文件，请使用`--al1`或`-a`选项。

#### 从贮藏创建一个分支

如果贮藏了一些工作，将它留在那儿了一会儿，然后继续在贮藏的分支上工作，在重新应用工作时可能会有问题。如果应用尝试修改刚刚修改的文件，你会得到一个合并冲突并不得不解决它。

如果想要一个轻松的方式来再次测试贮藏的改动，可以运行`git stash branch <new branchname>`以你指定的分支名创建一个新分支，检出贮藏工作时所在的提交，重新在那应用工作，然后在应用成功后丢弃贮藏：

```bash
$ git stash branch testchanges
M index.html
M lib/simplegit.rb
Switched to a new branch 'testchanges'
On branch testchanges
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
  modified: index.html
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working
directory)
  modified: lib/simplegit.rb
Dropped refs/stash@{0} (29d385a81d163dfd45a452a2ce816487a6b8b014)
```

这是在新分支轻松恢复贮藏工作并继续工作的一个很不错的途径

### 清理工作目录


对于工作目录中一些工作或文件，你想做的也许不是贮藏而是移除。` git clean `命令就是用来干这个的。

`需要谨慎地使用这个命令，因为它被设计为从工作目录中移除未被追踪的文件。`

如果你改变主意了，你也不一定能找回来那些文件的内容。 一个更安全的选项是运行 `git stash --all `来移除每一样东西并存放在栈中。

你可以使用` git clean `命令`去除冗余文件或者清理工作目录`。 使用` git clean -f -d `命令来移除工作目录中所有`未追踪的文件`以及`空的子目录`。 -f 意味着“强制(force)”或“确定要移除”，使用它需要 Git 配置变量 clean.requireForce 没有显式设置为 false。


如果只是想要看看它会做什么，可以使用 `--dry-run `或 `-n `选项来运行命令， 这意味着“`做一次演习然后告诉你 将要 移除什么`”。

```bash
$ git clean -d -n
Would remove test.o
Would remove tmp/
```
默认情况下，git clean 命令只会移除没有忽略的未跟踪文件。 

```bash
$ git status -s
 M lib/simplegit.rb
?? build.TMP
?? tmp/
```
任何与 .gitignore 或其他忽略文件中的模式匹配的文件都不会被移除。 如果你也想要移除那些文件，
```bash
$ git clean -n -d
Would remove build.TMP
Would remove tmp/
```
例如为了做一次完全干净的构建而移除所有由构建生成的 .o 文件， 可以给 clean 命令增加一个` -x `选项。
```bash
$ git clean -n -d -x
Would remove build.TMP
Would remove test.o
Would remove tmp/
```

## 分支管理

`git branch `命令不只是可以创建与删除分支。 如果不加任何参数运行它，会得到当前所有分支的一个列表：

![在这里插入图片描述](https://img-blog.csdnimg.cn/3875aa3ef99b443cbdcbccdebe08d66e.png)

`git branch -a `命令可以查看` 本地和远程的分支`

![在这里插入图片描述](https://img-blog.csdnimg.cn/2478fbce2e8544d69b93a174c6c07cd0.png)

注意 master 分支前的 * 字符：它代表现在检出的那一个分支(也就是说，当前 HEAD 指针所指向的分支)。

这意味着如果在这时候提交，master 分支将会随着新的工作向前移动。 如果需要查看每一个分支的最后一次提交，可以运行 `git branch -v `命令：

![在这里插入图片描述](https://img-blog.csdnimg.cn/11fb5a47f86f44b2af37ffdfee8b7d0a.png)


`--merged与--no-merged`这两个有用的选项可以过滤这个列表中`已经合并`或`尚未合并`到`当前分支的分支`。

+ 查看哪些分支已经合并到当前分支，可以运行`git branch--merged：`可以使用`git branch-d `删除掉；你已经将它们的工作整合到了另一个分支，所以并不会失去任何东西。
+ 查看所有包含未合并工作的分支，可以运行` git branch --no-merged：`因为它包含了还未合并的工作，尝试使用`git branch-d`命令删除它时会失败：如果真的想要删除分支并丢掉那些工作，如同帮助信息里所指出的，可以使用`-D`选项强制删除它。

![在这里插入图片描述](https://img-blog.csdnimg.cn/cb11e4f220ab42518da82a00b320ba2d.png)



## 分支开发工作流

许多使用Git的开发者都喜欢使用这种方式来工作，比如只在`master分支上保留完全稳定的代码`——有可能仅仅是已经发布或即将发布的代码。

他们还有一些名为`develop`或者`next`的`平行分支`，被用来做后续开发或者测试稳定性——这些分支`不必保持绝对稳定`，但是一旦`达到稳定状态`，它们就可以被`合并入master分支`了。



这样，在确保这些已完成的主题分支(短期分支，比如之前的iss53分支)能够通过所有测试，并且不会引入更多bug之后，就可以合并入主干分支中，等待下一次的发布。

事实上我们刚才讨论的，是随着你的提交而不断右移的指针。稳定分支的指针总是在提交历史中落后一大截，而前沿分支的指针往往比较靠前。

![在这里插入图片描述](https://img-blog.csdnimg.cn/daf660fa60e84e0ab800f3ab9f194282.png)

通常把他们想象成流水线(work silos)可能更好理解一点，那些经过测试考验的提交会被遴选到更加稳定的流水线上去。

![在这里插入图片描述](https://img-blog.csdnimg.cn/fd4e9c04d4c5438385a28c0696a94a11.png)


可以用这种方法维护`不同层次的稳定性`。一些大型项目还有一个`proposed(建议)`或`pu:proposed updates(建议更新)分支`，它可能因包含一些不成熟的内容而不能进入`next`或者`master分支`。这么做的目的是使你的分支具有`不同级别的稳定性`；

当它们具有一定程度的稳定性后，再把它们合并入具有更高级别稳定性的分支中。通过分支实现的工作流不是必须，但是对于复杂的项目往往很有帮助


### 主题分支


你在master分支上工作到`C1`，这时为了解决一个问题而新建`iss91分支`，在`iss91分支`上工作到`C4`，然而对于那个问题你又有了新的想法，于是你再新建一个`iss91v2`分支试图用另一种方法解决那个问题，接着你回到`master`分支工作了一会儿，你又冒出了一个不太确定的想法，你便在`C10`的时候新建一个`dumbidea`分支，并在上面做些实验。你的提交历史看起来像下面这个样子：

![在这里插入图片描述](https://img-blog.csdnimg.cn/1e5238a2f3284832a579d2ef7d67dae8.png)


现在，我们假设两件事情：你决定使用第二个方案来解决那个问题，即使用在`iss91v2分支`中方案。


另外，你将`dumbidea分支`拿给你的同事看过之后，结果发现这是个惊人之举。这时你可以抛弃`iss91分支`(即丢弃C5和C6提交)，然后把`另外两个分支合并入主干分支`。最终你的提交历史看起来像下面这个样子：

![在这里插入图片描述](https://img-blog.csdnimg.cn/fe6901a7165e4db18335b0da03b923e3.png)


当然这么多操作的时候，这些分支全部都存于本地。 当你新建和合并分支的时候，所有这一切都只发生在你本地的 Git 版本库中,没有与服务器发生交互。


### 远程分支

远程引用是对远程仓库的引用(指针)，你可以通过`git 1s-remote<remote>`来显式地获得远程引用的完整列表，或者通过`git remote show <remote>`获得远程分支的更多信息。然而，一个更常见的做法是利用`远程跟踪分支`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/be926fbb95a844fab4669fb982457bcb.png)


`远程跟踪分支是远程分支状态的引用`。通俗的讲，希望在本地可以看到远程分支的状态，它们是你无法移动的本地引用。一旦你进行了网络通信，`Git就会为你移动它们以精确反映远程仓库的状态`

远程跟踪分支`以<remote>/<branch>` 的形式命名。例如，如果你想要看你最后一次与远程仓库`origin `通信时`master`分支的状态，你可以查看`origin/master`分支。

IED 上 git 插件的展示，local Branches为本地分支，Remote Branches 为远程分支

![local Branches为本地分支，Remote Branches 为远程分支](https://img-blog.csdnimg.cn/7703f8eeaf2744459b1a770c2de72723.png)

**当你在Github或者Gitlab,Gitee上克隆一个项目，Git的`clone`命令会为你自动将其命名为`origin`，拉取它的所有数据，创建一个指向它的`master分支的指针`，并且在本地将其命名为`origin/master`。Git也会给你一个与origin的master分支在指向同一个地方的本地`master分支`，这样你就有工作的基础。**

下图上面为远程厂库的分支情况，下面为克隆到本地的情况。

![在这里插入图片描述](https://img-blog.csdnimg.cn/34ae8412c4d74272be1371677721fb8f.png)



`master` 是当你运行`git init`时默认的起始分支名字，原因仅仅是它的广泛使用，`origin`是当你运行git clone时默认的远程仓库名字。如果你运行`git clone -o booyah`，那么你默认的远程分支名字将会是`booyah/master`。



如果你在本地的master分支做了一些工作，在同一段时间内有其他人推送提交到 `git.ourcompany.com` 并且更新了它的master分支，这就是说你们的提交历史已走向不同的方向。即便这样，只要你保持`不与origin 服务器连接(并拉取数据)`，你的`origin/master `指针就不会移动。

![在这里插入图片描述](https://img-blog.csdnimg.cn/3b3b43763cbd4119a92f2ef03c00624b.png)


**如果要与给定的`远程仓库同步数据`**，运行`git fetch <remote>命令`(在本例中为`git fetch origin`)。这个命令查找“origin”是哪一个服务器(在本例中，它是git.ourcompany.com)，从中抓取本地没有的数据，并且更新本地数据库，移动`origin/master`指针到更新之后的位置。

![在这里插入图片描述](https://img-blog.csdnimg.cn/1a312df3151d49d89460393408fbcc5f.png)


为了演示有`多个远程仓库与远程分支`的情况，我们假定你有另一个内部Git服务器，仅服务于你的某个敏捷开发团队。这个服务器位于`git.teaml.ourcompany.com`。你可以运行`git remote add`命令添加一个新的`远程仓库引用到当前的项目`，将这个远程仓库命名为teamone


![在这里插入图片描述](https://img-blog.csdnimg.cn/7b8bce7a8a584441adbe67201f6362ce.png)

现在，可以运行`git fetch teamone`来抓取远程仓库teamone有而本地没有的数据。因为那台服务器上现有的数据是origin服务器上的一个子集，所以Git并不会抓取数据而是会设置`远程跟踪分支teamone/master `指向`teamone的master分支`。


![在这里插入图片描述](https://img-blog.csdnimg.cn/d9f46e4ffdc74cd19bdeb12dadd286e1.png)


### 推送本地分支到远程


当你想要公开分享一个分支时，需要将其推送到有`写入权限的远程仓库`上。如果你在本地新建的分支并做了commit，服务端会有一个申请合并的消息，在我日常的开发中，大都也是以这种方式来提交代码，

`本地的分支并不会自动与远程仓库同步`—-你必须显式地推送想要分享的分支。这样，你就可以把不愿意分享的内容放到私人分支上，而将需要和别人协作的内容推送到公开分支。

如果希望和别人一起在名为`serverfix`的分支上工作，你可以像推送第一个分支那样推送它。运行`git push<remote><branch>`

```bash
$ git push origin serverfix
Counting objects: 24, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (15/15), done.
Writing objects: 100% (24/24), 1.91 KiB | 0 bytes/s, done.
Total 24 (delta 2), reused 0 (delta 0)
To https://github.com/schacon/simplegit
 * [new branch] serverfix -> serverfix
```

也可以运行`git push origin serverfix:serverfix`，推送本地的 serverfix分支，将其作为远程仓库的serverfix分支

如果并不想让远程仓库上的分支叫做 serverfix，可以运行`git push origin serverfix:awesomebranch `来将本地的`serverfix分支`推送到远程仓库上的`awesomebranch分支`。


### 如何使用推送的远程分支

下一次其他协作者从服务器上抓取数据时` git fetch origin`，他们会在本地生成一个远程分支` origin/serverfix`，指向服务器的` serverfix `分支的引用：
```bash
$ git fetch origin
remote: Counting objects: 7, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 3 (delta 0)
Unpacking objects: 100% (3/3), done.
From https://github.com/schacon/simplegit
 * [new branch] serverfix -> origin/serverfix
```

要特别注意的一点是当抓取到新的远程跟踪分支时，本地不会自动生成一份可编辑的副本(拷贝)。换一句话说，这种情况下，`不会有一个新的serverfix分支——只有一个不可以修改的origin/serverfix指针。`



可以运行`git merge origin/serverfix`将这些`工作合并到当前所在的分支`。

如果想要在自己的`serverfix分支上工作`可以新建分支`在远程跟踪分支之上`：
```bash
$ git checkout -b serverfix origin/serverfix
Branch serverfix set up to track remote branch serverfix from origin.
Switched to a new branch 'serverfix'
```

这会给你一个用于工作的本地分支`serverfix`，并且起点位于 `origin/serverfix`。


### 跟踪分支

从一个`远程跟踪分支检出一个本地分支`会自动创建所谓的`“跟踪分支”`(它跟踪的分支叫做“上游分支”)。`跟踪分支是与远程分支有直接关系的本地分支`。如果在一个跟踪分支上输入`git pu11`，Git能自动地识别去哪91个服务器上抓取、合并到哪个分支。


当克隆一个仓库时，它通常会自动地创建一个跟踪`origin/master`的`master分支`。然而，如果你愿意的话可以设置其他的跟踪分支，或是一个在其他远程仓库上的跟踪分支，又或者不跟踪master分支。最简单的实例就是像之前看到的那样，运行`git checkout-b <branch><remote>/<branch>`。

```bash
$ git checkout --track origin/serverfix
Branch serverfix set up to track remote branch serverfix from origin.
Switched to a new branch 'serverfix'

```

这是一个十分常用的操作所以Git 提供了`--track`快捷方式,该捷径本身还有一个捷径,如果你尝试检处的分支不存在且刚好有一个远程分支与之对应，那么Git就会为你创建一个远跟踪分支。

```bash
$ git checkout serverfix
Branch serverfix set up to track remote branch serverfix from origin.
Switched to a new branch 'serverfix'
```
设置已有的本地分支跟踪一个刚刚拉取下来的远程分支，或者想要修改正在跟踪的上游分支，你可以在任意时间使用`-u或--set-upstream-to`选项运行`git branch`来显式地设置。

```bash
$ git branch -u origin/serverfix
Branch serverfix set up to track remote branch serverfix from origin.
```

### 拉取 fetch和pull的区别


当`git fetch`命令从服务器上抓取本地没有的数据时，`它并不会修改工作目录中的内容。它只会获取数据然后让你自己合并`。

`git pull`在大多数情况下它的含义是一个`git fetch紧接着一个git merge命令`。


由于`git pull`的魔法经常令人困惑所以通常单独显式地使用fetch与merge命令会更好一些。


### 删除远程分支

可以运行带有` --delete `选项的` git push `命令来删除一个远程分支。 如果想要从服务器上删除 `serverfix 分支`，运行下面的命令：

```bash
$ git push origin --delete serverfix
To https://github.com/schacon/simplegit
 - [deleted] serverfix
```
基本上这个命令做的只是从服务器上移除这个指针。` Git 服务器通常会保留数据一段时间直到垃圾回收运行`，所以如果不小心删除掉了，通常是很容易恢复的。

## 变基

在Git中整合来自不同分支的修改主要有两种方法：merge以及rebase。在本节中我们将学习什么是“变基”，怎样使用“变基”，并将展示该操作的惊艳之处，以及指出在何种情况下你应避免使用它。

### 变基的基本操作


请回顾之前在分支的合并中的一个例子，你会看到开发任务分叉到两个不同分支，又各自提交了更新。

![在这里插入图片描述](https://img-blog.csdnimg.cn/eeaa44a5b3c74613a1c6e894d7db4233.png)


之前介绍过，整合分支最容易的方法是`merge命令`。它会把`两个分支的最新快照(C3和C4)以及二者最近的共同祖先(C2)进行三方合并，合并的结果是生成一个新的快照(并提交)。`

![在这里插入图片描述](https://img-blog.csdnimg.cn/5261bf783646480bb96aa9dac8a7bc77.png)

其实，还有一种方法：你可以`提取在C4中引入的补丁和修改`，然后在C3的基础上应用一次。在Git中，这种操作就叫做`变基(rebase)`。你可以使用`rebase命令将提交到某一分支上的所有修改都移至另一分支上`，就好像“重新播放”一样。

在这个例子中，你可以检出experiment分支，然后将它变基到master分支上：
```bash
$ git checkout experiment
$ git rebase master
First, rewinding head to replay your work on top of it...
Applying: added staged command
```


它的原理是首先找到这两个分支(即`当前分支experiment`、变基操作的`目标基底分支master`)的最近共同祖先`C2`，然后对比当前分支相对于该祖先的历次提交，提取相应的修改并存为临时文件，然后将当前分支指向目标基底C3，最后以此将之前另存为临时文件的修改依序应用。(译注：写明了commitid，以便理解，下同)




# 分布式Git

这一章中，你将会学习如何作为贡献者或整合者，在一个分布式协作的环境中使用Git。你会学习`为一个项目成功地贡献代码`，并接触一些最佳实践方式，让你和项目的维护者能轻松地完成这个过程。另外，你也会学到如何管理有很多开发者提交贡献的项目。


## 分布式工作流程
与传统的集中式版本控制系统(CVCS)相反，Git的分布式特性使得开发者间的协作变得更加灵活多样。

在集中式系统中，每个开发者就像是连接在集线器上的节点，彼此的工作方式大体相像。而在Git中，每个开发者同时扮演着节点和集线器的角色——也就是说，每个开发者既可以将自己的代码贡献到其他的仓库中，同时也能维护自己的公开仓库，让其他人可以在其基础上工作并贡献代码。

由此，Git的分布式协作可以为你的项目和团队衍生出种种不同的工作流程，接下来的章节会介绍几种利用了Git的这种灵活性的常见应用方式。我们将讨论每种方式的优点以及可能的缺点；你可以选择使用其中的某一种，或者将它们的特性混合搭配使用。


### 集中式工作流

集中式系统中通常使用的是`单点协作模型——集中式工作流`。一个中心集线器，或者说仓库，可以接受代码，所有人将自己的工作与之同步。若干个开发者则作为节点，即作为中心仓库的消费者与中心仓库同步。

![在这里插入图片描述](https://img-blog.csdnimg.cn/a2daae4eb76349b489e9c46389ffdf2d.png)


例如John和Jessica同时开始工作。John完成了他的修改并推送到服务器。接着Jessica尝试提交她自己的修改，却遭到服务器拒绝。她被告知她的修改正通过`非快进式(non-fast-forward)的方式推送`，只有将数据抓取下来并且`合并`后方能推送。这种模式的工作流程的使用非常广泛，因为大多数人对其很熟悉也很习惯。

当然这并不局限于小团队。利用Git的分支模型，通过同时在多个分支上工作的方式，即使是上百人的开发团队也可以很好地在单个项目上协作。

### 集成管理者工作流

Git允许多个远程仓库存在，使得这样一种工作流成为可能：

`每个开发者拥有自己仓库的写权限和其他所有人仓库的读权限`。这种情形下通常会有个代表`“官方”项目的权威的仓库`。

要为这个项目做贡献，你需要从该项目`克隆出一个自己的公开仓库`，然后将自己的修改推送上去。接着你可以请求官方仓库的`维护者拉取更新合并`到主项目。

维护者可以将你的仓库作为远程仓库添加进来，在本地测试你的变更，将其合并入他们的分支并推送回官方仓库。这一流程的工作方式如下所示


![在这里插入图片描述](https://img-blog.csdnimg.cn/c4a5691dc17a426b992269f908dd2389.png)


1. 项目维护者推送到主仓库。
2. 贡献者克隆此仓库，做出修改。
3. 贡献者将数据推送到自己的公开仓库。
4. 贡献者给维护者发送邮件，请求拉取自己的更新。
5. 维护者在自己本地的仓库中，将贡献者的仓库加为远程仓库并合并修改。
6. 维护者将合并后的修改推送到主仓库。

这是GitHub和GitLab等集线器式(hub-based)工具最常用的工作流程。人们可以容易地将某个项目派生成为自己的公开仓库，向这个仓库推送自己的修改，并为每个人所见。这么做最主要的优点之一是你可以持续地工作，而主仓库的维护者可以随时拉取你的修改。贡献者不必等待维护者处理完提交的更新——每一方都可以按照自己的节奏工作。


## 主管与副主管工作流

这其实是多仓库工作流程的变种。一般拥有`数百位协作开发者的超大型项目`才会用到这样的工作方式，例如著名的`Linux内核项目`。被称为副主管`(lieutenant)`的各个`集成管理者`分别负责集成项目中的特定部分。所有这些副主管头上还有一位称为`主管(dictator)`的总集成管理者负责统筹。主管维护的仓库作为参考仓库，为所有协作者提供他们需要拉取的项目代码。整个流程看起来是这样的(见主管与副主管工作流。)：

![在这里插入图片描述](https://img-blog.csdnimg.cn/3fb33ff8929d4eeca51d41f01a7b114d.png)

1. 普通开发者在自己的`主题分支`上工作，并根据`master分支`进行`变基`。这里是主管推送的参考仓库的master分支。
2. 副主管将普通开发者的主题分支合并到自己的`master分支`中。
3. 主管将所有副主管的`master分支`并入自己的`master分支`中。
4. 最后，主管将集成后的`master分支`推送到参考仓库中，以便所有其他开发者以此为基础进行`变基`。



这种工作流程并不常用，只有当项目极为庞杂，或者需要多级别管理时，才会体现出优势。利用这种方式，项目总负责人(即主管)可以把大量分散的集成工作委托给不同的小组负责人分别处理，然后在不同时刻将大块的代码子集统筹起来，用于之后的整合。





如何避免每次输入密码
如果你正在使用 HTTPS URL 来推送，Git 服务器会询问用户名与密码。 默认情况下它会在终
端中提示服务器是否允许你进行推送。
如果不想在每一次推送时都输入用户名与密码，你可以设置一个 “credential cache”。 最
简单的方式就是将其保存在内存中几分钟，可以简单地运行 git config --global
credential.helper cache 来设置它。




# Git 常用命令

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ git
usage: git [--version] [--help] [-c name=value]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p|--paginate|--no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

最常用的 git 命令有：
   add        添加文件内容至索引
   bisect     通过二分查找定位引入 bug 的变更
   branch     列出、创建或删除分支
   checkout   检出一个分支或路径到工作区
   clone      克隆一个版本库到一个新目录
   commit     记录变更到版本库
   diff       显示提交之间、提交和工作区之间等的差异
   fetch      从另外一个版本库下载对象和引用
   grep       输出和模式匹配的行
   init       创建一个空的 Git 版本库或重新初始化一个已存在的版本库
   log        显示提交日志
   merge      合并两个或更多开发历史
   mv         移动或重命名一个文件、目录或符号链接
   pull       获取并合并另外的版本库或一个本地分支
   push       更新远程引用和相关的对象
   rebase     本地提交转移至更新后的上游分支中
   reset      重置当前HEAD到指定状态
   rm         从工作区和索引中删除文件
   show       显示各种类型的对象
   status     显示工作区状态
   tag        创建、列出、删除或校验一个GPG签名的 tag 对象

命令 'git help -a' 和 'git help -g' 显示可用的子命令和一些指南。参见
'git help <命令>' 或 'git help <指南>' 来查看给定的子命令帮助或指南。
┌──[root@liruilongs.github.io]-[~]
└─$ git help -a
用法：git [--version] [--help] [-c name=value]
           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
           [-p|--paginate|--no-pager] [--no-replace-objects] [--bare]
           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
           <command> [<args>]

在 '/usr/libexec/git-core' 下可用的 git 命令

  add                       clean                     fast-import               init                      merge-tree                receive-pack              revert                    tar-tree
  add--interactive          clone                     fetch                     init-db                   mergetool                 reflog                    rm                        unpack-file
  am                        column                    fetch-pack                log                       mktag                     relink                    send-pack                 unpack-objects
  annotate                  commit                    filter-branch             lost-found                mktree                    remote                    sh-i18n--envsubst         update-index
  apply                     commit-tree               fmt-merge-msg             ls-files                  mv                        remote-ext                shell                     update-ref
  archive                   config                    for-each-ref              ls-remote                 name-rev                  remote-fd                 shortlog                  update-server-info
  bisect                    count-objects             format-patch              ls-tree                   notes                     remote-ftp                show                      upload-archive
  bisect--helper            credential                fsck                      mailinfo                  pack-objects              remote-ftps               show-branch               upload-pack
  blame                     credential-cache          fsck-objects              mailsplit                 pack-redundant            remote-http               show-index                var
  branch                    credential-cache--daemon  gc                        merge                     pack-refs                 remote-https              show-ref                  verify-pack
  bundle                    credential-store          get-tar-commit-id         merge-base                patch-id                  remote-testpy             stage                     verify-tag
  cat-file                  describe                  grep                      merge-file                peek-remote               repack                    stash                     web--browse
  check-attr                diff                      hash-object               merge-index               prune                     replace                   status                    whatchanged
  check-ignore              diff-files                help                      merge-octopus             prune-packed              repo-config               stripspace                write-tree
  check-ref-format          diff-index                http-backend              merge-one-file            pull                      request-pull              submodule
  checkout                  diff-tree                 http-fetch                merge-ours                push                      rerere                    submodule--helper
  checkout-index            difftool                  http-push                 merge-recursive           quiltimport               reset                     subtree
  cherry                    difftool--helper          imap-send                 merge-resolve             read-tree                 rev-list                  symbolic-ref
  cherry-pick               fast-export               index-pack                merge-subtree             rebase                    rev-parse                 tag

命令 'git help -a' 和 'git help -g' 显示可用的子命令和一些指南。参见
'git help <命令>' 或 'git help <指南>' 来查看给定的子命令帮助或指南。
┌──[root@liruilongs.github.io]-[~]
└─$ git help -g
最常用的 Git 向导有：

   attributes   定义路径的属性
   glossary     Git 词汇表
   ignore       忽略指定的未跟踪文件
   modules      定义子模组属性
   revisions    指定 Git 的版本和版本范围
   tutorial     一个 Git 教程(针对 1.5.1 或更新版本)
   workflows    Git 推荐的工作流概览

命令 'git help -a' 和 'git help -g' 显示可用的子命令和一些指南。参见
'git help <命令>' 或 'git help <指南>' 来查看给定的子命令帮助或指南。
┌──[root@liruilongs.github.io]-[~]
└─$
```



# <font color=red>三、Gitlab搭建</font>


>服务器： liruilongs.github.io：192.168.26.55

## <font color=tomato>一、docker 环境安装</font>
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ yum -y install docker-ce
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl enable docker --now
```
**<font color=brown>配置docker加速器</font>**
```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://2tefyfv7.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

## <font color=tomato>二、安装GitLab</font>

### <font color=green>1.安装GitLab 并配置</font>
**<font color=green>拉取镜像</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker pull beginor/gitlab-ce
```
|--|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/933c165cc3464588956f6af222cf0250.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

### <font color=plum>2.创建共享卷目录</font>
**<font color=amber>创建共享卷目录，用于持久化必要的数据和更改相关配置</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ mkdir -p /data/gitlab/etc/ /data/gitlab/log /data/gitlab/data
┌──[root@liruilongs.github.io]-[~]
└─$ chmod 777 /data/gitlab/etc/ /data/gitlab/log /data/gitlab/data
```
### <font color=brown>3.创建 Gitlab 容器</font>
**<font color=royalblue>这里的访问端口一定要要设置成80，要不push项目会提示没有报错，如果宿主机端口被占用，需要把这个端口腾出来</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker run -itd --name=gitlab --restart=always --privileged=true   -p 8443:443  -p 80:80 -p 222:22 -v  /data/gitlab/etc:/etc/gitlab -v  /data/gitlab/log:/var/log/gitlab -v  /data/gitlab/data:/var/opt/gitlab  beginor/gitlab-ce
acc95b2896e8475915275d5eb77c7e63f63c31536432b68508f2f216d4fec634
┌──[root@liruilongs.github.io]-[~]
└─$ docker ps
CONTAINER ID   IMAGE               COMMAND             CREATED          STATUS                             PORTS                                                                                                             NAMES
acc95b2896e8   beginor/gitlab-ce   "/assets/wrapper"   53 seconds ago   Up 51 seconds (health: starting)   0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:222->22/tcp, :::222->22/tcp, 0.0.0.0:8443->443/tcp, :::8443->443/tcp   gitlab
┌──[root@liruilongs.github.io]-[~]
└─$
┌──[root@liruilongs.github.io]-[~]
└─$# 
```

### <font color=plum>4.关闭容器修改相关配置文件</font>
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker stop gitlab
gitlab
```
**<font color=royalblue>external_url    'http://192.168.26.55'</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep external_url
##! For more details on configuring external_url see:
# external_url 'GENERATED_EXTERNAL_URL'
# registry_external_url 'https://registry.gitlab.example.com'
# pages_external_url "http://pages.example.com/"
# gitlab_pages['artifacts_server_url'] = nil # Defaults to external_url + '/api/v4'
# mattermost_external_url 'http://mattermost.example.com'
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/external_url 'GENERATED_EXTERNAL_URL'/a external_url\t'http://192.168.26.55' "  /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep external_url
##! For more details on configuring external_url see:
# external_url 'GENERATED_EXTERNAL_URL'
external_url    'http://192.168.26.55'
# registry_external_url 'https://registry.gitlab.example.com'
# pages_external_url "http://pages.example.com/"
# gitlab_pages['artifacts_server_url'] = nil # Defaults to external_url + '/api/v4'
# mattermost_external_url 'http://mattermost.example.com'
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=orange>gitlab_rails['gitlab_ssh_host'] = '192.168.26.55'</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_ssh_host
# gitlab_rails['gitlab_ssh_host'] = 'ssh.host_example.com'
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/gitlab_ssh_host/a gitlab_rails['gitlab_ssh_host'] = '192.168.26.55' "  /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~] 
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_ssh_host
# gitlab_rails['gitlab_ssh_host'] = 'ssh.host_example.com'
gitlab_rails['gitlab_ssh_host'] = '192.168.26.55'
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=blue>gitlab_rails[gitlab_shell_ssh_port] = 222</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_shell_ssh
# gitlab_rails['gitlab_shell_ssh_port'] = 22
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/gitlab_shell_ssh_port/a gitlab_rails['gitlab_shell_ssh_port'] = 222" /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_shell_ssh
# gitlab_rails['gitlab_shell_ssh_port'] = 22
gitlab_rails[gitlab_shell_ssh_port] = 222
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=purple>/data/gitlab/data/gitlab-rails/etc/gitlab.yml</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vim /data/gitlab/data/gitlab-rails/etc/gitlab.yml
┌──[root@liruilongs.github.io]-[~]
└─$
##############################
 gitlab:
    ## Web server settings (note: host is the FQDN, do not include http://)
    host: 192.168.26.55
    port: 80
    https: false
```

|--|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/8b98142d3a1c4f54b2096ce7af378324.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
**<font color=yellowgreen>修改完配置文件之后。直接启动容器</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker start gitlab
```
#### <font color=green>5.访问测试</font>
|访问测试|
|--|
|**<font color=red>在宿主机所在的物理机访问，`http://192.168.26.55/` ，会自动跳转到修改密码(root用户),如果密码设置的没有满足一定的复杂性，则会报500，需要从新设置</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/2f11a407499c4f73bb01c3d513d345bb.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/c05f68349fb14087b8c023b2c43c1c71.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=seagreen>登录进入仪表盘</font>**|