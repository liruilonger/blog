---
title: K8s：关于 Argo CD 的一些笔记整理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-05-07 06:35:23/K8s：关于 Argo CD 的一些笔记整理.html'
mathJax: false
date: 2023-05-07 14:35:23
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


https://argo-cd.readthedocs.io/en/stable/getting_started/

```bash
kubectl create namespace argocd
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml
```

```bash
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get svc -n argocd
NAME                                      TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)                      AGE
argocd-applicationset-controller          ClusterIP      10.98.182.74     <none>           7000/TCP,8080/TCP            23h
argocd-dex-server                         ClusterIP      10.96.189.201    <none>           5556/TCP,5557/TCP,5558/TCP   23h
argocd-metrics                            ClusterIP      10.108.219.106   <none>           8082/TCP                     23h
argocd-notifications-controller-metrics   ClusterIP      10.105.41.12     <none>           9001/TCP                     23h
argocd-redis                              ClusterIP      10.96.153.48     <none>           6379/TCP                     23h
argocd-repo-server                        ClusterIP      10.100.105.220   <none>           8081/TCP,8084/TCP            23h
argocd-server                             LoadBalancer   10.107.105.12    192.168.26.224   80:31839/TCP,443:32383/TCP   23h
argocd-server-metrics                     ClusterIP      10.96.199.215    <none>           8083/TCP                     23h
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get deployments.apps -n argocd
NAME                               READY   UP-TO-DATE   AVAILABLE   AGE
argocd-applicationset-controller   1/1     1            1           23h
argocd-dex-server                  1/1     1            1           23h
argocd-notifications-controller    1/1     1            1           23h
argocd-redis                       1/1     1            1           23h
argocd-repo-server                 1/1     1            1           23h
argocd-server                      1/1     1            1           23h
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```

```bash
https://github.com/argoproj/argo-cd/releases/download/v2.7.1/argocd-linux-amd64
```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$mv argocd-linux-amd64 argocd
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$chmod  +x argocd
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$mv argocd  /usr/local/bin/
```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$kubectl get secrets  -n argocd argocd-initial-admin-secret -o yaml | egrep -C 2  -i ^data
apiVersion: v1
data:
  password: dmlRUmhmZ2l2UllXYU0ycw==
kind: Secret
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$ echo  "dmlRUmhmZ2l2UllXYU0ycw==" | base64 -d
viQRhfgivRYWaM2s┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$argocd admin initial-password -n argocd
viQRhfgivRYWaM2s

 This password must be only used for first time login. We strongly recommend you update the password using `argocd account update-password`.
```


```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$argocd cluster list
FATA[0000] Argo CD server address unspecified
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$argocd login --core
Context 'kubernetes' updated
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$argocd cluster list
SERVER                          NAME        VERSION  STATUS   MESSAGE                                                  PROJECT
https://kubernetes.default.svc  in-cluster           Unknown  Cluster has no applications and is not being monitored.
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$kubectl config set-context --current --namespace=argocd
Context "kubernetes-admin@kubernetes" modified.
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$argocd app create guestbook --repo https://github.com/argoproj/argocd-example-apps.git --path guestbook --dest-server https://kubernetes.default.svc --dest-namespace default
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/59de06b3d785488f95274c034671a89c.png)



用户需要 Kubernetes 访问权限来管理 Argo CD，因此必须使用下面的命令来配置 argocd CLI：



## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
