---
title: 《 Kubernetes 网络权威指南：基础、原理与实践》 读书笔记
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-03-24 11:46:56/《Kubernetes网络权威指南：基础、原理与实践》 读书笔记.html'
mathJax: false
date: 2023-03-24 19:46:56
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***
# 第1章 夯实基础：Linux网络虚拟化
## 1.1 网络虚拟化基石：network namespace

`network namespace` 可以说是整个 Linux网络虚拟化技术的基石,其作用就是“隔离内核资源”

Linux内核自2.4.19 版本接纳第一个 namespace：Mount namespace(用于隔离文件系统挂载点)起，到3.8版本的user namespace(用于隔离用户权限)，总共实现了上文提到的6种不同类型的 namespace (Mount  namespace、UTS  namespace(主机名)、IPC namespace(进程通信mq)、PID namespace、network namespace和user namespace)。

Linux 的 namespace 给里面的进程造成了两个错觉：
+ 它是系统里唯一的进程。
+ 它独享系统的所有资源。

默认情况下，Linux 进程处在和主机相同的 namespace，即初始的根 namespace 里，默认享有全局系统资源。

`network  namespace` 在Linux内核 2.6 版本引入，作用是隔离 Linux 系统的设备，以及 IP地址、端口、路由表、防火墙规则等网络资源。因此，每个网络 namespace 里都有自己的网络设备(如IP地址、路由表、端口范围、/proc/net目录等)。

从网络的角度看，`network namespace ` 使得容器非常有用，一个直观的例子就是：由于每个容器都有自己的(虚拟)网络设备，并且容器里的进程可以放心地绑定在端口上而不必担心冲突，这就使得`在一个主机上同时运行多个监听80端口的Web服务器变为可能`.


### 1.1.1 初识 network namespace

network  namespace 可以通过系统调用来创建, 当前 `network  namespace` 的增删改查功能已经集成到Linux的ip工具的netns子命令中。

创建一个名为 netns_demo 的network namespace
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip netns add netns_demo
┌──[root@liruilongs.github.io]-[~]
└─$ip netns list # 查看当前系统的 network namespace 
netns_demo
```
创建了一个network  namespace时，系统会在 `/var/run/netns` 路径下面生成一个挂载点

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ls /var/run/netns/netns_demo 
/var/run/netns/netns_demo
```

挂载点的作用一方面是方便对 namespace 的管理，另一方面是使namespace即使没有进程运行也能继续存在。

一个 network namespace 被创建出来后，可以使用ip netns exec命令进入，做一些网络查询/配置的工作。
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec netns_demo ip a
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
┌──[root@liruilongs.github.io]-[~]
└─$ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens32: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
    link/ether 00:0c:29:e5:68:68 brd ff:ff:ff:ff:ff:ff
    inet 192.168.26.152/24 brd 192.168.26.255 scope global dynamic ens32
       valid_lft 1198sec preferred_lft 1198sec
    inet6 fe80::20c:29ff:fee5:6868/64 scope link
       valid_lft forever preferred_lft forever
┌──[root@liruilongs.github.io]-[~]
└─$
```
没有任何配置，因此只有一块系统默认的本地回环设备lo。

删除network namespace，可以通过以下命令实现：

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip netns delete netns_demo
┌──[root@liruilongs.github.io]-[~]
└─$ip netns list
┌──[root@liruilongs.github.io]-[~]
└─$
```
注意，上面这条命令实际上并没有删除netns1这个network namespace，它只是移除了这个network  namespace对应的挂载点。只要里面还有进程运行着，network  namespace便会一直存在。



### 1.1.2 配置network namespace

当尝试访问创建的 网络 命名空间的本地回环地址时，网络也是不通的
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec netns_demo ip a
1: lo: <LOOPBACK> mtu 65536 qdisc noop state DOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec netns_demo ip  link set dev lo up
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec netns_demo ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec netns_demo ping 127.0.0.1
PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.047 ms
64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.047 ms
64 bytes from 127.0.0.1: icmp_seq=3 ttl=64 time=0.048 ms
^C
--- 127.0.0.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2022ms
rtt min/avg/max/mdev = 0.047/0.047/0.048/0.005 ms
┌──[root@liruilongs.github.io]-[~]
└─$
```

仅有一个本地回环设备是没法与外界通信的。如果我们想与外界(比如主机上的网卡)进行通信，就需要在 namespace 里再 `创建一对虚拟的以太网卡`，即所谓的 `veth pair`。顾名思义，veth pair总是成对出现且相互连接，它就像Linux的双向管道(pipe)，报文从veth  pair一端进去就会由另一端收到
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip link add veth0 type veth peer name veth1
┌──[root@liruilongs.github.io]-[~]
└─$ip link set veth1 netns  netns_demo
```
创建了veth0和veth1这么一对虚拟以太网卡。在默认情况下，它们都在主机的根network  namespce中，将其中一块虚拟网卡veth1通过ip link set命令移动到netns1 network namespace。
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec netns_demo ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
3: veth1@if4: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN qlen 1000
    link/ether 5a:af:75:80:a6:ba brd ff:ff:ff:ff:ff:ff link-netnsid 0
```
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: ens32: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP qlen 1000
    link/ether 00:0c:29:e5:68:68 brd ff:ff:ff:ff:ff:ff
    inet 192.168.26.152/24 brd 192.168.26.255 scope global dynamic ens32
       valid_lft 1377sec preferred_lft 1377sec
    inet6 fe80::20c:29ff:fee5:6868/64 scope link
       valid_lft forever preferred_lft forever
4: veth0@if3: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN qlen 1000
    link/ether be:2a:81:c1:e4:24 brd ff:ff:ff:ff:ff:ff link-netnsid 0
┌──[root@liruilongs.github.io]-[~]
└─$
```
这两块网卡刚创建出来还都是DOWN状态，需要手动把状态设置成UP。

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec netns_demo ifconfig veth1 10.1.1.1/24 up
┌──[root@liruilongs.github.io]-[~]
└─$ifconfig veth0 10.1.1.2/24 up
```
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec  netns_demo ping 10.1.1.2 -c 3
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=1 ttl=64 time=0.058 ms
64 bytes from 10.1.1.2: icmp_seq=2 ttl=64 time=0.060 ms
64 bytes from 10.1.1.2: icmp_seq=3 ttl=64 time=0.060 ms

--- 10.1.1.2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2020ms
rtt min/avg/max/mdev = 0.058/0.059/0.060/0.006 ms
┌──[root@liruilongs.github.io]-[~]
└─$
```


不同network  namespace之间的路由表和防火墙规则等也是隔离的，因此我们刚刚创建的netns1  network  namespace没法和主机共享路由表和防火墙
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec  netns_demo  route
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
10.1.1.0        0.0.0.0         255.255.255.0   U     0      0        0 veth1
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec  netns_demo  iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
┌──[root@liruilongs.github.io]-[~]
└─$route
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
default         gateway         0.0.0.0         UG    0      0        0 ens32
10.1.1.0        0.0.0.0         255.255.255.0   U     0      0        0 veth0
link-local      0.0.0.0         255.255.0.0     U     1002   0        0 ens32
192.168.26.0    0.0.0.0         255.255.255.0   U     0      0        0 ens32
┌──[root@liruilongs.github.io]-[~]
└─$iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
ACCEPT     all  --  anywhere             anywhere             ctstate RELATED,ESTABLISHED
ACCEPT     all  --  anywhere             anywhere
INPUT_direct  all  --  anywhere             anywhere
INPUT_ZONES_SOURCE  all  --  anywhere             anywhere
INPUT_ZONES  all  --  anywhere             anywhere
DROP       all  --  anywhere             anywhere             ctstate INVALID
REJECT     all  --  anywhere             anywhere             reject-with icmp-host-prohibited
```

从netns1  network  namespace发包到因特网也是徒劳的
想连接因特网，有若干解决方法。例如，可以在主机的根network  namespace创建一个Linux网桥并绑定veth pair的一端到网桥上；也可以通过适当的NAT(网络地址转换)规则并辅以Linux的IP转发功能(配置net.ipv4.ip_forward=1)

需要注意的是，用户可以随意将虚拟网络设备分配到自定义的 network  namespace 里，而连接真实硬件的物理设备则只能放在系统的根  network  namesapce 中。并且，任何一个网络设备最多只能存在于一个 network namespace 中。

进程可以通过 Linux 系统调用clone()、unshare()和setns进入 network  namespace,

+ 非root进程被分配到network namespace后只能访问和配置已经存在于该network  namespace的设备
+ root进程可以在network  namespace里创建新的网络设备
+ network  namespace里的root进程还能把本network  namespace的虚拟网络设备分配到其他network  namespac

### 1.1.3 network namespace API的使用

1.创建namespace的黑科技：clone系统调用

用户可以使用clone()系统调用创建一个namespace。

2.维持namespace存在：/proc/PID/ns目录的奥秘

每个Linux进程都拥有一个属于自己的/proc/PID/ns，这个目录下的每个文件都代表一个类型的namespace。

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ls -l /proc/$$/ns
总用量 0
lrwxrwxrwx 1 root root 0 3月  25 12:51 ipc -> ipc:[4026531839]
lrwxrwxrwx 1 root root 0 3月  25 12:51 mnt -> mnt:[4026531840]
lrwxrwxrwx 1 root root 0 3月  25 12:51 net -> net:[4026531956]
lrwxrwxrwx 1 root root 0 3月  25 12:51 pid -> pid:[4026531836]
lrwxrwxrwx 1 root root 0 3月  25 12:51 user -> user:[4026531837]
lrwxrwxrwx 1 root root 0 3月  25 12:51 uts -> uts:[4026531838]
┌──[root@liruilongs.github.io]-[~]
└─$
```
在Linux内核3.8 版本以前，`/proc/PID/ns` 目录下的文件都是硬链接(hard  link)，而且只有ipc、net和uts这三个文件,从Linux内核3.8版本开始，每个文件都是一个特殊的符号链接文件

符号链接的其中一个用途是确定某两个进程是否属于同一个namespace。如果两个进程在同一个namespace中，那么这两个进程/proc/PID/ns目录下对应符号链接文件的inode数字(即上文例子中［］内的数字，例如 4026531839  会是一样的。

/proc/PID/ns目录下的文件还有一个作用当我们打开这些文件时，只要文件描述符保持open状态，对应的namespace就会一直存在，哪怕这个namespace里的所有进程都终止运行了

3.往namespace里添加进程：setns系统调用

Linux系统调用setns()把一个进程加入一个已经存在的namespace中。

4.帮助进程逃离namespace：unshare系统调用


通过Linux的network  namespace 技术可以自定义一个独立的网络栈，简单到只有loopback设备，复杂到具备系统完整的网络能力，这就使得 network  namespace 成为 Linux 网络虚拟化技术的基石——不论是虚拟机还是容器时代。

network  namespace的另一个隔离功能在于，系统管理员一旦禁用namespace中的网络设备，即使里面的进程拿到了一些系统特权，也无法和外界通信。最后，网络对安全较为敏感，即使network  namespace能够提供网络资源隔离的机制，用户还是会结合其他类型的namespace一起使用，以提供更好的安全隔离能力。


## 1.2 千呼万唤始出来：veth pair

部署过 Docker 或 Kubernetes 的读者肯定有这样的经历：在主机上输入ifconfig或ip  addr命令查询网卡信息的时候，总会出来一大堆 vethxxxx 之类的网卡名，这些是Docker/Kubernetes为容器而创建的虚拟网卡。

veth是虚拟以太网卡(Virtual  Ethernet)的缩写，veth设备总是成对的，因此我们称之为veth pair

veth pair 一端发送的数据会在另外一端接收，非常像 Linux 的双向管道。根据这一特性，veth  pair常被用于跨
network  namespace之间的通信，即分别将 veth  pair 的两端放在不同的 namespace里。

仅有veth  pair设备，容器是无法访问外部网络的。为什么呢？因为从容器发出的数据包，实际上是直接进了veth  pair设备的协议栈。

如果容器需要访问网络，则需要使用网桥等技术将veth  pair 设备接收的数据包通过某种方式转发出去。

注：虽然我们在容器化的场景里经常看到veth网卡，但veth  pair在传统 的虚拟化技术(比如KVM等)中的应用也非常广泛。

veth pair的创建和使用

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip  link add veth3 type veth peer name veth4
```
创建的veth pair在主机上表现为两块网卡，我们可以通过ip link命令查看：
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip link list
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: ens32: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT qlen 1000
    link/ether 00:0c:29:e5:68:68 brd ff:ff:ff:ff:ff:ff
4: veth0@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT qlen 1000
    link/ether be:2a:81:c1:e4:24 brd ff:ff:ff:ff:ff:ff link-netnsid 0
5: veth4@veth3: <BROADCAST,MULTICAST,M-DOWN> mtu 1500 qdisc noop state DOWN mode DEFAULT qlen 1000
    link/ether de:3a:5e:42:2d:ff brd ff:ff:ff:ff:ff:ff
6: veth3@veth4: <BROADCAST,MULTICAST,M-DOWN> mtu 1500 qdisc noop state DOWN mode DEFAULT qlen 1000
    link/ether 06:8f:b5:e8:e9:f6 brd ff:ff:ff:ff:ff:ff
```

新创建的veth pair设备的默认mtu是1500，设备初始状态是DOWN。我们同样可以使用ip  link命令将这两块网卡的状态设置为UP。

veth pair设备同样可以配置IP地址，命令如下： 可以将veth pair设备放到namespace中。让我们来温习命令的使用：

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip link set dev veth3 up
┌──[root@liruilongs.github.io]-[~]
└─$ip link set dev veth4 up
┌──[root@liruilongs.github.io]-[~]
└─$ifconfig veth3 10.20.30.40/24
┌──[root@liruilongs.github.io]-[~]
└─$ifconfig veth4 10.20.30.41/24
┌──[root@liruilongs.github.io]-[~]
└─$
```
可以将veth pair设备放到namespace中。让我们来温习命令的使用

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ip link  set veth4 netns netns_demo
┌──[root@liruilongs.github.io]-[~]
└─$ip netns exec netns_demo ip link list
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
3: veth1@if4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP mode DEFAULT qlen 1000
    link/ether 5a:af:75:80:a6:ba brd ff:ff:ff:ff:ff:ff link-netnsid 0
5: veth4@if6: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT qlen 1000
    link/ether de:3a:5e:42:2d:ff brd ff:ff:ff:ff:ff:ff link-netnsid 0
┌──[root@liruilongs.github.io]-[~]
└─$ifconfig
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.152  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:fee5:6868  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:e5:68:68  txqueuelen 1000  (Ethernet)
        RX packets 5943  bytes 438293 (428.0 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1727  bytes 202922 (198.1 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

veth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.1.1.2  netmask 255.255.255.0  broadcast 10.1.1.255
        inet6 fe80::bc2a:81ff:fec1:e424  prefixlen 64  scopeid 0x20<link>
        ether be:2a:81:c1:e4:24  txqueuelen 1000  (Ethernet)
        RX packets 22  bytes 1796 (1.7 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 22  bytes 1796 (1.7 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

veth3: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 10.20.30.40  netmask 255.255.255.0  broadcast 10.20.30.255
        inet6 fe80::48f:b5ff:fee8:e9f6  prefixlen 64  scopeid 0x20<link>
        ether 06:8f:b5:e8:e9:f6  txqueuelen 1000  (Ethernet)
        RX packets 8  bytes 648 (648.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 8  bytes 648 (648.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

┌──[root@liruilongs.github.io]-[~]
└─$
```
veth pair设备的原理较简单，就是向veth pair设备的一端输入数据，数据通过内核协议栈后从veth pair的另一端出来。veth pair的基本工作原理:
```bash
物理网卡
eth0   veth0 veth1 veth3 veth4 
     Linux  网络协议栈                           
```
### 1.2.1 veth pair内核实现

在veth  pair设备上，任意一端(RX)接收的数据都会在另一端(TX)发送出去，veth  pair在转发过程中不会篡改数据包的内容



### 1.2.2 容器与host veth pair的关系

经典容器组网模型就是 `veth  pair+bridge` 的模式。容器中的 `eth0` 实际上和外面 `host` 上的某个 veth 是成对的(pair)关系，那么，有没有办法知道host上的vethxxx和哪个container eth0是成对的关系呢？

方法一
容器里面看：

然后，在主机上遍历/sys/claas/net下面的全部目录，查看子目录 ifindex 的值和容器里查出来的 iflink 值相当的 veth 名字，这样就找到了容器和主机的veth  pair关系。


```bash
┌──[root@liruilongs.github.io]-[/]
└─$ docker exec -it  6471704fd03a sh
/ # cat /sys/class/net/eth0/if
ifalias  ifindex  iflink
/ # cat /sys/class/net/eth0/iflink
95
/ # exit
┌──[root@liruilongs.github.io]-[/]
└─$ grep -c 95 /sys/class/net/*/ifindex | grep 1
/sys/class/net/veth2e08884/ifindex:1
```

#### 方法二

从上面的命令输出可以看到116:eth0@if117，其中116是eth0接口的
index，117是和它成对的veth的index。
当host执行下面的命令时，可以看到对应117的veth网卡是哪一个，
这样就得到了容器和veth pair的关系

在目标容器里执行以下命令

```bash
┌──[root@liruilongs.github.io]-[/]
└─$ docker exec -it  6471704fd03a sh
/ # ip link show eth0
94: eth0@if95: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue
    link/ether 02:42:ac:11:00:03 brd ff:ff:ff:ff:ff:ff
/ # exit
┌──[root@liruilongs.github.io]-[/]
└─$ ip link show | grep 94
95: veth2e08884@if94: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP mode DEFAULT
┌──[root@liruilongs.github.io]-[/]
└─$ ip link show | grep 95
95: veth2e08884@if94: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP mode DEFAULT
┌──[root@liruilongs.github.io]-[/]
└─$
```


## 1.3 连接你我他：Linux bridge

两个network  namespace可以通过veth  pair连接，但要做到两个以上network  namespace相互连接，veth  pair就显得捉襟见肘了。这就轮到本节的主角Linux bridge出场了

Linux  bridge 就是 Linux系统中的网桥，但是Linux  bridge 的行为更像是一台虚拟的 `网络交换机`，任意的真实物理设备(例如 eth0)和虚拟设备(例如，前面讲到的veth  pair和后面即将介绍的tap设备)都可以连接到Linux  bridge上。需要注意的是，Linux  bridge不能跨机连接网络设备.


Linux  bridge与Linux上其他网络设备的区别在于，普通的网络设备只有两端，从一端进来的数据会从另一端出去。例如，物理网卡从外面网络中收到的数据会转发给内核协议栈，而从协议栈过来的数据会转发到外面的物理网络中。

Linux bridge则有多个端口，数据可以从任何端口进来，进来之后从哪个口出去取决于目的MAC地址，原理和物理交换机差不多

### 1.3.1 Linux bridge初体验


我们先用iproute2软件包里的ip命令创建一个bridge

```bash
┌──[root@liruilongs.github.io]-[/]
└─$ ip link  add name br0 type  bridge
┌──[root@liruilongs.github.io]-[/]
└─$ ip link show br0
96: br0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT qlen 1000
    link/ether 66:1b:4b:65:57:c1 brd ff:ff:ff:ff:ff:ff
┌──[root@liruilongs.github.io]-[/]
└─$ ip link set br0 up
```
除了ip命令，我们还可以使用 `bridge-utils` 软件包里的 brctl 工具管理网桥，例如新建一个网桥
```bash
brctl addbr br0
```

刚创建一个 bridge 时，它是一个独立的网络设备，只有一个端口连着协议栈，其他端口什么都没连接，这样的 bridge 其实没有任何实际功能.

```bash
   网络协议栈
   eth0  br0
   物理网络 
```
假设eth0是我们的物理网卡，IP地址是1.2.3.4，并且假设实验室环境网关地址是 1.2.3.1 (下文会用到)
```bash
┌──[root@liruilongs.github.io]-[/]
└─$ ip link add veth0 type veth peer name veth1
┌──[root@liruilongs.github.io]-[/]
└─$ ip addr add 1.2.3.101/24 dev veth0
┌──[root@liruilongs.github.io]-[/]
└─$ ip addr add 1.2.3.102/24 dev veth1
┌──[root@liruilongs.github.io]-[/]
└─$ ip link set veth0 up
┌──[root@liruilongs.github.io]-[/]
└─$ ip link set veth1 up
┌──[root@liruilongs.github.io]-[/]
└─$
```
通过下面的命令将 veth0 连接到 br0 上
```bash
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ ip link set dev veth0 master br0
```
也可以使用 `brctl` 命令添加一个设备到网桥
```bash
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ brctl addif br0 veth0
device veth0 is already a member of a bridge; can't enslave it to bridge br0.
```


可以通过bridge  link(bridge也是iproute2的组成部分)命令查看当前网桥上都有哪些网络设备
```bash
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ bridge link
95: veth2e08884 state UP @(null): <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 master docker0 state forwarding priority 32 cost 2
98: veth0 state UP @veth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 master br0 state forwarding priority 32 cost 2
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$
```
这里我们看到两个，是应该有一个 是 docker 安装时默出时默认时默认创建的网桥

也可以使用brctl命令显示当前存在的网桥及其所连接的网络端口，这个命令的输出和bridge link的输出有所区别，命令如下所示

```bash
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ brctl show
bridge name     bridge id               STP enabled     interfaces
br-0e0cdf9c70b0         8000.02428a21648b       no
br-4b3da203747c         8000.02427ae111f5       no
br-997c6faa4c40         8000.024240d52ad0       no
br-ad20ce87f585         8000.0242ea8e92f6       no
br0             8000.6e47d8158ce4       no              veth0
docker0         8000.0242ef65af08       no              veth2e08884
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/f43138757b374cb8bf3d8ec8de8194c3.png)

br0 和 veth0 相连之后发生了如下变化：
+ br0 和 veth0 之间连接起来了，并且是双向的通道；
+ 协议栈和veth0之间变成了单通道，协议栈能发数据给veth0，但veth0从外面收到的数据不会转发给协议栈；
+ br0的MAC地址变成了veth0的MAC地址。

这就好比Linux  bridge在veth0和协议栈之间做了一次拦截，在veth0上面做了点小动作，将veth0本来要转发给协议栈的数据拦截，全部转发给bridge。同时，bridge也可以向veth0发数据



```bash
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ ping -c 1 -I veth0 1.2.3.102
PING 1.2.3.102 (1.2.3.102) from 1.2.3.101 veth0: 56(84) bytes of data.
^C
--- 1.2.3.102 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms
```
veth0 收到应答包后没有给协议栈，而是给了 br0，于是协议栈得不到veth1的MAC地址，导致通信失败。

### 1.3.2 把IP让给Linux bridge

把veth0的IP地址“让给”Linux bridge：
```bash
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ ip addr del 1.2.3.101/24 dev veth0
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ ip addr add 1.2.3.101/24 dev br0
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$
```
将原本分配给 veth0 的IP地址配置到 br0 上。于是，绑定IP地址的bridge设备的网络拓扑如图

![在这里插入图片描述](https://img-blog.csdnimg.cn/c88be6d5da1c4ae5be82e02df4a8ac0e.png)


协议栈和veth0之间的联系去掉了，veth0相当于一根网线,通过br0 ping veth1，结果成功收到了ICMP的回程报文

```bash
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ ping -c 2 -I br0 1.2.3.102
```
### 1.3.3 将物理网卡添加到 Linux bridge

```bash
┌──[root@liruilongs.github.io]-[/sys/class/net/br-0e0cdf9c70b0]
└─$ ip link set dev ens32 master  br0
```

Linux bridge 不会区分接入进来的到底是物理设备还是虚拟设备，对它来说没有区别。因此，eth0加入br0后，落得和上面veth0一样的 “下场”，从外面网络收到的数据包将无条件地转发给br0，自己变成了一根网线。

这时，通过eth0  ping网关失败。因为br0通过eth0这根网线连上了外面的物理交换机，所以连在br0上的设备都能ping通网关，这里连上的设备就是veth1和br0自己，veth1是通过eth0这根网线连上去的，而br0有一块自带的网卡。

eth0的功能已经和网线差不多，所以在eth0上配置IP没有意义，还会影响协议栈的路由选择。例如，如果ping的时候不指定网卡，则协议栈有可能优先选择eth0，导致ping不通。因此，需要将eth0上的IP去掉。在以上测试过程中，由于eth0上有IP，在访问1.2.3.0/24网段时，会优先选择eth0。

eth0接入了br0，因此它收到的数据包都会转发给br0，于是协议栈收不到ARP应答包，导致ping失败,将eth0上的IP删除,再从eth0 ping一次网关，成功收到ICMP响应报文：

删除eth0的IP后，路由表里就没有它了，于是数据包会从veth1出去。

原来的默认路由进过eth0，eth0的IP被删除后，默认路由不见了，想要连接1.2.3.0/24以外的网段，需要手动将默认网关加回来。
```bash
sudo ip route add default via 192.168.26.1
```
将物理网卡添加到bridge设备的网络拓扑如图1-7所示。

![在这里插入图片描述](https://img-blog.csdnimg.cn/88bf874aa4af4e5a8ce9ca116d5d1d81.png)


要完成以上所有实验步骤，需要打开eth0网卡的混杂模式(下文会详细介绍Linux  bridge的混杂模式)，



### 1.3.4 Linux bridge 在网络虚拟化中的应用

Linux bridge的两种常见的部署方式

#### 1.虚拟机

虚拟机通过 `tun/tap` 或者其他类似的虚拟网络设备，将虚拟机内的网卡同 `br0` 连接起来，这样就达到和`真实交换机`一样的效果.

虚拟机发出去的数据包先到达`br0`，然后由 `br0` 交给 `eth0` 发送出去，数据包都不需要经过host机器的协议栈，效率高，如图1-8所示。

![在这里插入图片描述](https://img-blog.csdnimg.cn/361c141384c541d4824823e63fc21ed8.png)


如果有多个虚拟机，那么这些虚拟机通过 tun/tap 设备连接到网桥。


#### 2. 容器

容器运行在自己单独的 network  namespace 里，因此都有自己单独的协议栈。Linux bridge在容器场景的组网和上面的虚拟机场景差不多，但也存在一些区别。

+ 容器使用的是 `veth  pair` 设备，而虚拟机使用的是 `tun/tap` 设备。
+ 在虚拟机场景下，我们给主机物理网卡 eth0 分配了IP地址；而在容器场景下，我们一般不会对宿主机 eth0 进行配置。
+ 在虚拟机场景下，虚拟器一般会和主机在同一个网段；而在容器场景下，容器和物理网络不在同一个网段内。


![在这里插入图片描述](https://img-blog.csdnimg.cn/33a99f37422e45eeabfc6d13163b90e6.png)


在容器中配置其网关地址为 br0，在我们的例子中即1.2.3.101(容器网络网段是1.2.3.0/24)。

因此，从容器发出去的数据包先到达br0，然后交给host机器的协议栈。由于目的IP是外网IP，且host机器开启了IP forward功能，数据包会通过eth0发送出去。因为容器所分配的网段一般都不在物理网络网段内(在我们的例子中，物理网络网段是10.20.30.0/24)

所以一般发出去之前会先做NAT转换(NAT转换需要自己配置，可以使用iptables，1.5节会介绍iptables)





### 1.3.5 网络接口的混杂模式

网络接口的混杂模式在Kubernetes网络也有应用，本节将重点讨论网络接口的混杂模式.

混杂模式(Promiscuous  mode)，简称Promisc  mode，俗称“监听模式”。

混杂模式通常被网络管理员用来诊断网络问题，但也会被无认证的、想偷听网络通信的人利用。根据维基百科的定义，混杂模式是指一个网卡会把它接收的所有网络流量都交给CPU，而不是只把它想转交的部分交给CPU。

在IEEE  802定的网络规范中，每个网络帧都有一个目的MAC地址。在非混杂模式下，网卡只会接收目的MAC地址是它自己的单播帧，以及多播及广播帧；在混杂模式下，网卡会接收经过它的所有帧！


使用ifconfig或者netstat-i命令查看一个网卡是否开启了混杂模式,当输出包含PROMISC时，表明该网络接口处于混杂模式。

+ 启用网卡的混杂模式，可以使用下面这条命令：`ifconfig eth0 promisc`
+ 使网卡退出混杂模式，可以使用下面这条命令：`ifconfig eth0 -promisc`

将网络设备加入Linux bridge后，会自动进入混杂模式。可以通过下
面的小实验来说明：

```bash
brctl addif br0 beth0
dmesg | grep promis
```

如上所示，veth设备加入Linux  bridge后，可以通过查看内核日志看到veth0自动进入混杂模式，而且无法退出，直到将 `veth0` 从 `Linux  bridge` 中移除。即使手动将网卡设置为非混杂模式，实际上还是没有退出混杂模
```bash
brctl delif br0 veth0
```




## 1.4 给用户态一个机会：tun/tap设备

tun/tap设备是理解 flannel 的基础，而 flannel 是一个重要的Kubernetes网络插件.

#### tun/tap设备到底是什么？

从Linux文件系统的角度看，它是用户可以用文件句柄操作的字符设备；从网络虚拟化角度看，它是虚拟网卡，一
端连着网络协议栈，另一端连着用户态程序。

如果把veth  pair称为设备孪生，那么tun/tap就像是一对表兄弟。它们的区别。

+ tun表示虚拟的点对点设备
+ tap表示虚拟的是以太网设备

这两种设备针对网络包实施不同的封装。

#### tun/tap设备有什么作用呢？

tun/tap设备可以将TCP/IP协议栈处理好的网络包发送给任何一个使用tun/tap驱动的进程，由进程重新处理后发到物理链路中。

tun/tap设备就像是埋在用户程序空间的一个钩子，我们可以很方便地将对网络包的处理程序挂在这个钩子上，OpenVPN、Vtun、flannel都是基于它实现隧道包封装的。

### 1.4.1 tun/tap设备的工作原理

物理设备上的数据是如何通过 Linux 网络栈送达用户态程序的，tun/tap 设备的基本原理如图1-10所示

![在这里插入图片描述](https://img-blog.csdnimg.cn/3f037e780c83412fbeea7c11241e5cb9.png)

一个经典的、通过Socket调用实现用户态和内核态数据交互的过程。物理网卡从网线接收数据后送达网络协议栈，而进程通过Socket创建特殊套接字，从网络协议栈读取数据


tun/tap设备其实就是利用Linux的设备文件实现内核态和用户态的数据交互，而访问设备文件则会调用设备驱动相应的例程，要知道设备驱动也是内核态和用户态的一个接口。tun设备的工作模式如图1-11所示。




### 1.4.2 利用tun设备部署一个VPN


### 1.4.3 tun设备编程


## 1.5 iptables

iptables 在 Docker 和 Kubernetes 网络中应用甚广。例如，Docker容器和宿主机的端口映射、Kubernetes  Service 的默认模式、CNI 的 portmap 插件、Kubernetes 网络策略 等都是通过 iptables 实现的

### 1.5.1 祖师爷netfilter

iptables 的底层实现是 netfilter。netfilter 是 Linux 内核 2.4 版引入的一个子系统，最初由 Linux 内核防火墙和网络的维护者Rusty  Russell提出。

它作为一个通用的、抽象的框架，提供一整套 hook 函数的管理机制，使得数据包过滤、包处理(设置标志位、修改TTL等)、地址伪装、网络地址转换、透明代理、访问控制、基于协议类型的连接跟踪，甚至带宽限速等功能成为可能。

netfilter 的架构就是在整个网络流程的若干位置放置一些钩子，并在每个钩子上挂载一些处理函数进行处理。

![在这里插入图片描述](https://img-blog.csdnimg.cn/7a525ee6a46f47c0ac21da10ff330baa.png)

当网卡收到一个包送到协议栈：
+ 最先经过 netfilter 钩子是 prorouting，如果用户埋了钩子，内核会在这里做 DNAT 转化
+ 之后内核会通过本地路由表决定数据包是走本地进程还是走 forward 钩子。
    + 走 forward 钩子会跳过本地进程相关钩子 `input 和 output`，直接走 postrouting 对应的钩子
    + 不走 forward 钩子走本地进程会通过 `input` 钩子，进入本地进程，生成返回报文经过 `ouptput` 钩子。然后走 postrouting 钩子   
+ 在到 postrouting 钩子之前，还涉及一个路由选择，即决定从那个网卡出去，下一跳地址是哪里，最后出协议栈，经过 `postrouting` 


构建在 netfilter 钩子之上的网络安全策略和连接跟踪的用户态程序就有 ebtables、arptables、(IPv6版本的)ip6tables、iptables、iptables-nftables(iptables的改进版本)、conntrack(连接跟踪)等。

Kubernetes网络之间用到的工具就有 ebtables、iptables/ip6tables 和 conntrack，其中iptables是核心。




### 1.5.2 iptables 的三板斧：table、chain和rule

iptables 是用户空间的一个程序，通过 netlink 和内核的 netfilter 框架打交道，负责往钩子上配置回调函数。一般情况下用于构建 Linux 内核防火墙，特殊情况下也做服务负载均衡(这是 Kubernetes 的特色操作，我们将在后面章节专门分析)。iptables的工作原理如图1-15所示

我们常说的`iptables  5X5`，即5张表(table)和5条链(chain)。5条链即iptables的5条内置链，对应上文介绍的netfilter的5个钩子。

5条链分别是：
+ `INPUT 链`：一般用于处理输入本地进程的数据包；
+ `OUTPUT 链`：一般用于处理本地进程的输出数据包；
+ `FORWARD 链`：一般用于处理转发到 其他机器/network  namespace 的数据包；
+ `PREROUTING 链`：可以在此处进行DNAT；
+ `POSTROUTING 链`：可以在此处进行SNAT。

除了系统预定义的5条iptables链，`用户还可以在表中定义自己的链`，我们将通过例子详细说明

5张表如下所示。
+ `filter 表`：用于控制到达某条链上的数据包是继续放行、直接丢弃(drop)或拒绝(reject)；
+ `nat 表`：用于修改数据包的源和目的地址；
+ `mangle 表`：用于修改数据包的IP头信息；
+ `raw 表`：iptables是有状态的，即iptables对数据包有连接追踪(connection tracking)机制，而raw是用来去除这种追踪机制的；
+ `security 表`：最不常用的表(通常，我们说iptables只有4张表，security表是新加入的特性)，用于在数据包上应用SELinux。

不是每个链上都能挂表，iptables表与链的对应关系如表

![在这里插入图片描述](https://img-blog.csdnimg.cn/98640cac7d1a4edc95535ede0789d5d0.png)

从图中我们可以看到
+ 对于 mangle 表，即修改数据包IP头部信息，可以挂在任何链
+ 对于 raw 表，即修改报文修改 prerouting 和 output 支持
+ 对于 nat 表分情况，snat 只能挂到 postporting 和 input ，其他的都不行，dnat 只能挂到 prerouting 和 outpit。
+  对于 filter，security 表，可以挂到 forward ， input 和 output。


![在这里插入图片描述](https://img-blog.csdnimg.cn/821d1ff03b6043f2a6e1d74c1d6f631b.png)


iptables 的表是来分类管理 iptables 规则(rule)的，系统所有的 iptables 规则都被划分到不同的表集合中。上文也提到了，filter 表中会有过滤数据包的规则，nat 表中会有做地址转换的规则。因此，iptables 的规则就是挂在 netfilter 钩子上的函数，用来修改数据包的内容或过滤数据包，iptables 的表就是所有规则的5个逻辑集合！



iptables 的规则是用户真正要书写的规则，一条 iptables 规则包含两部分信息：匹配条件和动作。

匹配条件：

匹配数据包被这条iptables规则“捕获”的条件，例如协议类型、源IP、目的IP、源端口、目的端口、连接状态等

动作：

+ DROP：直接将数据包丢弃，不再进行后续的处理。应用场景是不让某个数据源意识到你的系统的存在，可以用来模拟宕机；
+ REJECT：给客户端返回一个connection  refused或destination unreachable报文。应用场景是不让某个数据源访问你的系统，善意地告诉他：我这里没有你要的服务内容；
+ QUEUE：将数据包放入用户空间的队列，供用户空间的程序处理；
+ RETURN：跳出当前链，该链里后续的规则不再执行；
+ ACCEPT：同意数据包通过，继续执行后续的规则；
+ JUMP：跳转到其他用户自定义的链继续执行

用户自定义链中的规则和系统预定义的5条链里的规则没有区别。由于自定义的链没有与netfilter里的钩子进行绑定，所以它不会自动触发，只能从其他链的规则中跳转过来，这也是JUMP动作
存在的意义。



### 1.5.3 iptables的常规武器

#### 1.查看所有iptables规则

Kubernetes一个节点上的iptables规则输出如下。输出是iptables的filter表的所有规则。使用iptables命令，必须指定针对哪个表进行操作，默认是filter表。
```bash
┌──[root@vms105.liruilongs.github.io]-[~]
└─$iptables -L -n
Chain INPUT (policy ACCEPT)
target     prot opt source               destination
cali-INPUT  all  --  0.0.0.0/0            0.0.0.0/0            /* cali:Cz_u1IQiXIMmKD4c */
KUBE-PROXY-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes load balancer firewall */
KUBE-NODEPORTS  all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes health check service ports */
KUBE-EXTERNAL-SERVICES  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes externally-visible service portals */
KUBE-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0
..............................

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination
cali-FORWARD  all  --  0.0.0.0/0            0.0.0.0/0            /* cali:wUHhoiAYhphO9Mso */
KUBE-PROXY-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes load balancer firewall */
KUBE-FORWARD  all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes forwarding rules */
KUBE-SERVICES  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes service portals */
KUBE-EXTERNAL-SERVICES  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes externally-visible service portals */
DOCKER-USER  all  --  0.0.0.0/0            0.0.0.0/0
DOCKER-ISOLATION-STAGE-1  all  --  0.0.0.0/0            0.0.0.0/0
.............................

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination
cali-OUTPUT  all  --  0.0.0.0/0            0.0.0.0/0            /* cali:tVnHkvAo15HuiPy0 */
KUBE-PROXY-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes load balancer firewall */
KUBE-SERVICES  all  --  0.0.0.0/0            0.0.0.0/0            ctstate NEW /* kubernetes service portals */
KUBE-FIREWALL  all  --  0.0.0.0/0            0.0.0.0/0
OUTPUT_direct  all  --  0.0.0.0/0            0.0.0.0/0

...................................

Chain KUBE-FIREWALL (2 references)
target     prot opt source               destination
DROP       all  -- !127.0.0.0/8          127.0.0.0/8          /* block incoming localnet connections */ ! ctstate RELATED,ESTABLISHED,DNAT
DROP       all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes firewall for dropping marked packets */ mark match 0x8000/0x8000

Chain KUBE-FORWARD (1 references)
target     prot opt source               destination
DROP       all  --  0.0.0.0/0            0.0.0.0/0            ctstate INVALID
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes forwarding rules */ mark match 0x4000/0x4000
ACCEPT     all  --  0.0.0.0/0            0.0.0.0/0            /* kubernetes forwarding conntrack rule */ ctstate RELATED,ESTABLISHED
.....................................
```
filter 表上挂了5条链，分别是 INPUT、FORWARD 和 OUTPUT 这三条系统内置链，以及 KUBE-FIREWALL 和 KUBE-FORWARD 这两条用户自定义链, iptables 的内置链都有默认规则

```bash
Chain OUTPUT (policy ACCEPT)
```
用户自己定义的链后面都有一个引用计数，在我们的例子中 KUBE-FIREWALL 和 KUBE-FORWARD 都有一个引用，它们分别在 INPUT 和 FORWARD 中被引用.

iptables 的每条链下面的规则处理顺序是从上到下逐条遍历的，除非中途碰到 DROP，REJECT，RETURN 这些内置动作。如果 iptables 规则前面是自定义链，则意味着这条规则的动作是 JUMP，即跳到这条自定义链遍历其下的所有规则，然后跳回来遍历原来那条链后面的规则。

#### 2.配置内置链的默认策略

当然，我们可以自己配置内置链的默认策略，决定是放行还是丢弃，如下所示
```bash
# INPUT 链默认策略 丢弃
iptables --policy INPUT DROP
# FORWARD 链默认为 丢弃
iptables --policy FORWARD DROP
```


#### 3.配置防火墙规则策略

防火墙策略一般分为通和不通两种。
+ 如果默认策略是“全通”，例如上文的policy  ACCEPT，就要定义一些策略来封堵；即黑名单
+ 如果默认策略是“全不通”，例如上文的policy  DROP，就要定义一些策略来解封。即白名单


1. 配置允许SSH连接

```bash
iptables -A INPUT -s 10.23.23.12/24 -p tcp -dport 22 -j ACCEPT
```
+ -A的意思是以追加(Append)的方式增加这条规则。
+ -A INPUT 表示这条规则挂在 INPUT 链上。
+ -s 10.20.30.40/24 表示允许源(source)地址是 10.20.30.40/24 这个网段的连接。
+ -p  tcp 表示允许 TCP(protocol)包通过。
+ --dport  22 的意思是允许访问的目的端口(destination  port)为 22，即SSH端口。
+ -j  ACCEPT表示接受这样的连接。

综上所述，这条iptables规则的意思是允许源地址是10.20.30.40/24这个网段的包发到本地TCP 22端口。

除了按追加的方式添加规则，还可以使用 `iptables -I［chain］［number］` 将规则插入(Insert)链的指定位置。如果不指定number，则插到链的第一条处.

2. 阻止来自某个IP/网段的所有连接

阻止10.10.10.10上所有的包,也可以使用-j  REJECT，这样就会发一个连接拒绝的回程报文,客户端收到后立刻结束。不像-j  DROP那样不返回任何响应，客户端只能一直等待直到请求超时

```bash
iptables -A INPUT -s 10.10.10.10 -j DROP
```
如果要屏蔽一个网段，例如 10.10.10.0/24，则可以使用 `-s  10.10.10.0/24` 或 `10.10.10.0/255.255.255.0`。

如果要“闭关锁国”，即屏蔽所有的外来包，则可以使用 `-s 0.0.0.0/0`

3. 封锁端口

要阻止从本地1234端口建立对外连接，可以使用以下命令
```bash
iptables -A OUTPUT -p tcp --dport 1234 -j DROP
```
要在 OUTPUT 链上挂规则是因为我们的需求是屏蔽本地进程对外的连接。如果我们要阻止外部连接访问本地1234端口，就需要在INPUT链上挂规则
```bash
iptables -A INPUT -p tcp --dport 1234 DROP
```
4. 端口转发

把服务器的某个端口流量转发给另一个端口，例如，我们对外声称Web服务在80端口上运行，但由于种种原因80端口被人占了，实际的Web服务监听在8080端口上。为了让外部客户端能无感知地依旧访问80端口，可以使用以下命令实现端口转发：
```bash
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
```
`-p tcp--dport 80-j REDIRECT--to-port 8080`的意思是发到TCP 80端口的流量转发(REDIRECT)给8080端口

该规则应该发生在nat表上的PREROUTING链上。至于-i  eth0则表示匹配eth0网卡上接收到的包。

5. 禁用PING

大部分的公有云默认都是屏蔽ICMP的，即禁止ping报文
```bash
iptables -A INPUT -p icmp FROP
```
6. 删除规则

最暴力地清除当前所有的规则命令

```bash
iptables -F
```
要清空特定的表可以使用-t参数进行指定，例如清除nat表所有的规则如下

```bash
iptables -t nat -F
```
删除规则的最直接的需求是解封某条防火墙策略。因此，笔者建议使用 `iptables的-D` 参数,-D 表示从链中删除一条或多条指定的规则，后面跟的就是要删除的
规则
```bash
iptables -D INPUT -s 10.10.10.10 -j DROP
```

当某条链上的规则被全部清除变成空链后，可以使用-X参数删除：删除FOO这条用户自定义的空链，但需要注意的是系统内置链无法删除。

```bash
iptables -X FOO
```
7. 自定义链

自定义链创建

```bash
iptables -N BAR
```
在filter表(因为未指定表，所以可以使用-t参数指定)创建了一条用户自定义的链BAR。


#### 4.DNAT

DNAT根据指定条件 `修改数据包的目标IP地址和目标端口` 。DNAT 的原理和我们上文讨论的端口转发原理差不多，差别是端口转发不修改IP地址。使用iptables做目的地址转换的一个典型例子如下:

```bash
iptables -t nat -A PREROUTING -d 1.2.3.4  -p tcp -dport 80 -j DNAT  --to-destination 10.20.30.40:8080 
```

+ -j DNAT 表示目的地址转换
+ -d 1.2.3.4 -p tcp --dport 80 表示匹配的包,条件是访问目的地址和端口为1.2.3.4:80的TCP包
+ --to-destination 表示将该包的目的地址和端口修改成 10.20.30.40:8080。

同样，DNAT不修改协议。如果要匹配网卡，可以用 `-i  eth0` 指定收到包的网卡(i 是 input 的缩写)。需要注意的是，DNAT 只发生在 nat表的 `PREROUTING` 链，这也是我们要指定收到包的网卡而不是发出包的网卡的原因


当涉及转发的目的IP地址是外机时，需要确保启用 ip forward 功能，即把 Linux :

```bash
echo 1 > /proc/sys/net/ipv4/ip_forward
```

#### 5.SNAT/ 网络地址欺骗

神秘的网络地址欺骗其实是SNAT的一种。SNAT 根据指定条件修改数据包的源IP地址，即 DNAT 的逆操作。与 DNAT 的限制类似，SNAT 策略只能发生在 nat 表的 POSTROUTING 链。

```bash
ipttables -t nat -A POSTROUTING -s 192.168.26.12 -o eth0 -j SNAT -to-source 10.127.16.1 
```
+ -j  SNAT表示源地址转换
+ -s  192.168.1.12 表示匹配的包源地址是 192.168.1.12，
+ --to-source 表示将该包的源地址修改成 10.172.16.1。与DNAT类似
+ -o  eth0(o是output的缩写)匹配发包的网卡


至于网络地址伪装，与SNAT类似,其实就是一种特殊的源地址转换，报文从哪个网卡出就用该网卡上的IP地址替换该报文的源地址，具体用哪个IP地址由内核决定。下面这条规则的意思是：源地址是 10.8.0.0/16 的报文都做一次 `Masq` 。

```bash
iptable -t nat -A POSTROUTING -s 10.8.0.0/16 -j MASQUERADE
```

#### 6.保存与恢复

上述方法对iptables规则做出的改变是临时的，重启机器后就会丢失。如果想永久保存这些更改，则需要运行以下命令：

```bash
iptables-save > iptables.bak
```

可以使用iptables-restore命令还原iptables-save命令备份的iptables配置，原理就是逐条执行文件里的iptables规则

```bash
iptables-restore < iptables.bak
```




## 1.6 初识Linux隧道：ipip

1.6.1 测试ipip隧道
1.6.2 ipip隧道测试结果复盘
1.6.3 小结
1.7 Linux隧道网络的代表：VXLAN
1.7.1 为什么需要VXLAN
1.7.2 VXLAN协议原理简介
1.7.3 VXLAN组网必要信息
1.7.4 VXLAN基本配置命令
1.7.5 VXLAN网络实践
1.7.6 分布式控制中心
1.7.7 自维护VTEP组
1.7.8 小结
1.8 物理网卡的分身术：Macvlan
1.8.1 Macvlan五大工作模式解析
1.8.2 测试使用Macvlan设备
1.8.3 Macvlan的跨机通信
1.8.4 Macvlan与overlay对比
1.8.5 小结
1.9 Macvlan的救护员：IPvlan
1.9.1 IPvlan简介
1.9.2 测试IPvlan
1.9.3 Docker IPvlan网络
1.9.4 小结
# 第2章 饮水思源：Docker网络模型简介


## 2.1 主角登场：Linux容器
### 2.1.1 容器是什么

容器不是模拟一个完整的操作系统，而是对进程进行隔离，隔离用到的技术就是1.1节介绍的Linux  namespace。对容器里的进程来说，接触到的各种资源看似是独享的，但在底层其实是隔离的。容器是进程级别的隔离技术，因此相比虚拟机有启动快、占用资源少、体积小等优点。


目前最流行的`Linux容器`非Docker莫属，它是对Linux底层容器技术的一种封装，提供简单易用的容器使用接口。

Docker将应用程序与该程序的依赖打包在同一个文件里，即所谓的 Docker image。

运行Docker image 就会生成一个 Docker 容器。程序在这个虚拟容器里运行就像是在物理机上或虚拟机上运行一样

### 2.1.2 容器与虚拟机对比
### 2.1.3 小结

从虚拟化层面来看，传统虚拟化技术是对硬件资源的虚拟，容器技术则是对进程的虚拟

从架构来看，容器比虚拟机少了Hypervisor层和Guest OS层，使用Docker Engine进行资源分配调度并调用Linux内核namespace API进行隔离，所有应用共用主机操作系统。


## 2.2 打开万花筒：Docker的四大网络模式

利用 `network namespace`，可以为 Docker 容器创建隔离的网络环境,容器具有完全`独立的网络栈`，与宿主机隔离。用户也可以让 Docker 容器共享主机或者其他容器的 network namespace。

容器的网络方案可以分为三大部分：
+ 单机的容器间通信；
+ 跨主机的容器间通信；
+ 容器与主机间通信。

可以使用--network选项指定容器的网络模式。Docker有以下4种网络模式：

+ bridge 模式，通过 `--network=bridge` 指定
+ host 模式，通过 `--network=host` 指定
+ container 模式，通过 `--n--network=bridgeetwork=container:NAME_or_ID` 指定，即 joiner 容器
+ none 模式，通过 `--network=none` 指定。


### 2.2.1 bridge模式

Docker在安装时会创建一个名为 docker0 的 Linux网桥

bridge模式是 Docker 默认的网络模式，在不指定 --network 的情况下，Docker会为每一个容器分配 network  namespace、设置IP等，并将Docker容器连接到  docker0 网桥上


创建的容器的 `veth  pair` 中的一端桥接到` docker0` 上。 `docker0` 网桥是普通的 Linux 网桥，而非 OVS 网桥(虚拟交换机技术)，因此我们通过brctl命令查看该网桥信息：
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ yum -y install  bridge-utils  
┌──[root@liruilongs.github.io]-[~]
└─$ brctl  show
bridge name     bridge id               STP enabled     interfaces
br-0e0cdf9c70b0         8000.0242fbd2f076       no
br-48f9bbb59d44         8000.024203d1afd0       no
br-4b3da203747c         8000.0242c4f54716       no
br-6ace9aa59cb0         8000.02425ed235a1       no              veth34f60a3
                                                        veth4378a0e
                                                        veth4ce5ae7
                                                        veth81b1c4d
                                                        vethb470a91
br-7099c25e475d         8000.024219e0c539       no
docker0         8000.02429c075e6e       no              veth09d1f7b
                                                        veth2da3880
```

### 2.2.2 host模式
### 2.2.3 container模式
### 2.2.4 none模式
## 2.3 最常用的Docker网络技巧
### 2.3.1 查看容器IP
### 2.3.2 端口映射
### 2.3.3 访问外网
### 2.3.4 DNS和主机名
### 2.3.5 自定义网络
### 2.3.6 发布服务
### 2.3.7 docker link：两两互联
## 2.4 容器网络的第一个标准：CNM
### 2.4.1 CNM标准
### 2.4.2 体验CNM接口
### 2.4.3 Libnetwork
### 2.4.4 Libnetwork扩展
### 2.4.5 小结
## 2.5 天生不易：容器组网的挑战
### 2.5.1 容器网络挑战综述
### 2.5.2 Docker的解决方案
### 2.5.3 第三方容器网络插件
### 2.5.4 小结
## 2.6 如何做好技术选型：容器组网方案沙场点兵
### 2.6.1 隧道方案
### 2.6.2 路由方案
### 2.6.3 容器网络组网类型
### 2.6.4 关于容器网络标准接口
### 2.6.5 小结
第3章 标准的胜利：Kubernetes网络原理与实践
3.1 容器基础设施的代言人：Kubernetes
3.1.1 Kubernetes简介
3.1.2 Kubernetes能做什么
3.1.3 如何用Kubernetes
3.1.4 Docker在Kubernetes中的角色
3.2 终于等到你：Kubernetes网络
3.2.1 Kubernetes网络基础
3.2.2 Kubernetes网络架构综述
3.2.3 Kubernetes主机内组网模型
3.2.4 Kubernetes跨节点组网模型
3.2.5 Pod的hosts文件
3.2.6 Pod的hostname
3.3 Pod的核心：pause容器
3.4 打通CNI与Kubernetes：Kubernetes网络驱动
3.4.1 即将完成历史使命：Kubenet
3.4.2 网络生态第一步：CNI
3.5 找到你并不容易：从集群内访问服务
3.5.1 Kubernetes Service详解
3.5.2 Service的三个port
3.5.3 你的服务适合哪种发布形式
3.5.4 Kubernetes Service发现
3.5.5 特殊的无头Service
3.5.6 怎么访问本地服务
3.6 找到你并不容易：从集群外访问服务
3.6.1 Kubernetes Ingress
3.6.2 小结
3.7 你的名字：通过域名访问服务
3.7.1 DNS服务基本框架
3.7.2 域名解析基本原理
3.7.3 DNS使用
3.7.4 调试DNS
3.8 Kubernetes网络策略：为你的应用保驾护航
3.8.1 网络策略应用举例
3.8.2 小结
3.9 前方高能：Kubernetes网络故障定位指南
3.9.1 IP转发和桥接
3.9.2 Pod CIDR冲突
3.9.3 hairpin
3.9.4 查看Pod IP地址
3.9.5 故障排查工具
3.9.6 为什么不推荐使用SNAT
# 第4章 刨根问底：Kubernetes网络实现机制



## 4.1 岂止 iptables：Kubernetes Service 官方实现细节探秘

下面我们介绍Kubernetes  Service的工作原理，主要涉及的 Kubernetes 组件有 Controller Manager(包括Service Controller 和 Endpoints Controller)和 Kube-proxy。

当用户创建 Service  和对应的后端 Pod 时，Endpoints Controller 会监控 Pod 的状态变化， 当 Pod 处于 Running 并且准备就绪的时候，Endpoints Controller 会生成Endpoints对象。

![在这里插入图片描述](https://img-blog.csdnimg.cn/57dedcde1a7d4726a3e9570aae0f0f8f.png)

运行在每个节点上的 Kube-proxy 会监控 Servier 和 Endpoints 的更新，并调用其 Load Balancer 模块在`主机上刷新路由转发规则`

每个 Pod 都可以通过 Liveness Probe 和 Readiness Probe 探针解决健康检查和服务可用性问题，当有 Pod 处于非就绪状态时，即服务不可用时， Kube-proxy 会删除对应的转发规则。


需要注意的是，Kube-proxy 的 Load  Balancer 模块并不那么智能，如果转发的 Pod 不能正常提供服务，它不会重试或尝试连接其他Pod

Kube-proxy的 Load  Balancer 模块实现有 userspace、iptables 和 IPVS 三种，当前主流的实现方式是 iptables 和 IPVS。随着iptables在大规模环境越来越多的厂商开始使用IPVS模式

Kube-proxy的转发模式可以通过启动参数--proxy-mode进行配置，有userspace、iptables、ipvs等可选项。




### 4.1.1 userspace 模式


Kube-proxy 的 userspace 模式是通过 Kube-proxy 用户态程序实现 Load Balancer 的代理服务。

userspace 模式是 Kube-proxy  1.0 之前版本的默认模式。由于转发发生在用户态，效率自然不太高，而且容易丢包。

为什么要介绍本应被废弃的特性呢？ 因为iptables和IPVS模式都依赖Linux内核的能力，尤其是IPVS，而且iptables和IPVS都要求较高版本的内核和iptables版本。那些使用低版本内核的操作系统(例如 SUSE  11)用不了iptables和IPVS模式，但又希望拥有基本的服务转发能力，这时 userspace 模式就派上用场了。


userspace 模式的 Kube-proxy  的工作原理

![在这里插入图片描述](https://img-blog.csdnimg.cn/af7682f071ba486fbbf835d4ce097632.png)


+ 访问服务的请求到达节点后首先会进入内核 iptables 
+ 然后回到用户空间，由 Kube-proxy 完成后端 Pod 的选择，并建立一条到后端 Pod 的连接，完成代理转发工作。
+ 这样流量从用户空间进出内核将带来不小的性能损耗。
+ 这也是Kubernetes 1.0及之前版本中对Kube-proxy质疑最大的一点，于是也就有了后来的iptables模式。

为什么 userspace 模式要建立 iptables 规则，原因是 Kube-proxy 进程只监听一个端口，而且这个端口并不是服务的访问端口也不是服务的NodePort

因此需要一层 iptables 把访问服务的连接重定向给 Kube-proxy 进程的一个临时端口。Kube-proxy 在代理客户端请求时会开放一个临时端口，以便后端 Pod 的响应返回给 Kube-proxy ，然后 Kube-proxy 再返回
给客户端。


以一个Service为例，讲解userspace模式下的iptables规则

Service 的类型是 NodePort，服务端口是 2222，NodePort 端口是 30239，Cluster IP是10.254.132.107

`NodePort` 的工作原理与 `Cluster IP` 大致相同，`是发送到 Node 上指定端口的数据`，通过 iptables 重定向到 `Kube-proxy` 对应的端口上。然后由 Kube-proxy 把数据发送到其中一个Pod上。


Kube-proxy 针对不同的服务类型各自创建了iptables 入口链。例如:

+ 容器访问 `NodePort` 的入口 `KUBE-NODEPORT-CONTAINER`
+ 节点上进程访问 `NodePort` 的入口是 `KUBE-NODEPORT-HOST`
+ 容器访问 `Cluster  IP:Por`t 的入口是 `KUBE-PORTALS-CONTAINER`
+ 节点上进程访问 `Cluster  IP:Port` 的入口是 `KUBE-PORTALS-HOST`。

无一例外，访问这 4个入口的流量都被 `DNAT/重定向` 到本机的 `36463` 端口上。36463 端口实际上被 Kube-proxy 监听，流量进入 Kube-proxy 用户态程序后再转发给后端的 Pod。


### 4.1.2 iptables模式

从Kubernetes  1.1 版本开始，增加了 iptables 模式，在 1.2 版本中它正式替代 userspace 模式成为默认模式。

我们在前面的章节介绍过 iptables 是用户空间应用程序，通过配置 Netfilter 规则表(Xtables)构建 Linux 内核防火墙。Kube-proxy iptables模式的原理如图4-3所示

![在这里插入图片描述](https://img-blog.csdnimg.cn/30ccd193531c4f9cbf91d345b18d2bef.png)

iptables 模式与 userspace 模式最大的区别在于，`Kube-proxy 利用 iptables 的 DNAT 模块，实现了 Service 入口地址到 Pod 实际地址的转换，免去了一次内核态到用户态的切换。`


下面我们将以一个 Service 为例，分析 Kube-proxy 创建的 iptables 规则。

如上所示，在本例中我们创建了一个名为mysql-service的服务。该服务的访问端口是 3306，NodePort 是 30964，对应的后端Pod的端口也是3306。

```yaml
┌──[root@vms100.liruilongs.github.io]-[~/docker]
└─$kubectl get svc -n kube-system kube-dns -o yaml
apiVersion: v1
kind: Service
metadata:
  annotations:
    prometheus.io/port: "9153"
    prometheus.io/scrape: "true"
  creationTimestamp: "2023-01-26T11:28:00Z"
  labels:
    k8s-app: kube-dns
    kubernetes.io/cluster-service: "true"
    kubernetes.io/name: CoreDNS
  name: kube-dns
  namespace: kube-system
  resourceVersion: "256"
  uid: ec55d5bf-be92-461f-bbda-8d8a694eb25a
spec:
  clusterIP: 10.96.0.10
  clusterIPs:
  - 10.96.0.10
  internalTrafficPolicy: Cluster
  ipFamilies:
  - IPv4
  ipFamilyPolicy: SingleStack
  ports:
  - name: dns
    port: 53
    protocol: UDP
    targetPort: 53
  - name: dns-tcp
    port: 53
    protocol: TCP
    targetPort: 53
  - name: metrics
    port: 9153
    protocol: TCP
    targetPort: 9153
  selector:
    k8s-app: kube-dns
  sessionAffinity: None
  type: ClusterIP
status:
  loadBalancer: {}
```

+ 该服务的 `Cluster  IP` 是 `10.254.162.44`。
+ `mysql-service` 服务有两个后端 `Pod`，IP分别是 `192.168.125.129` 和 `192.168.125.131`。

Kube-proxy 为该服务创建的 iptables 如下所示：
```bash
┌──[root@vms100.liruilongs.github.io]-[~/docker]
└─$iptables -S -t nat | grep  kube-dns
-A KUBE-SEP-DHR7P54HYQ2XZLIB -s 10.244.239.180/32 -m comment --comment "kube-system/kube-dns:metrics" -j KUBE-MARK-MASQ
-A KUBE-SEP-DHR7P54HYQ2XZLIB -p tcp -m comment --comment "kube-system/kube-dns:metrics" -m tcp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination 0.0.0.0:0
-A KUBE-SEP-EXV2TBBOQI4PJ6BU -s 10.244.239.180/32 -m comment --comment "kube-system/kube-dns:dns-tcp" -j KUBE-MARK-MASQ
-A KUBE-SEP-EXV2TBBOQI4PJ6BU -p tcp -m comment --comment "kube-system/kube-dns:dns-tcp" -m tcp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination
-A KUBE-SEP-FWGM64AFGGHPYAZF -s 10.244.239.180/32 -m comment --comment "kube-system/kube-dns:dns" -j KUBE-MARK-MASQ
-A KUBE-SEP-FWGM64AFGGHPYAZF -p udp -m comment --comment "kube-system/kube-dns:dns" -m udp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination
-A KUBE-SEP-LFDUPWVFYGWAZ4R3 -s 10.244.239.181/32 -m comment --comment "kube-system/kube-dns:dns-tcp" -j KUBE-MARK-MASQ
-A KUBE-SEP-LFDUPWVFYGWAZ4R3 -p tcp -m comment --comment "kube-system/kube-dns:dns-tcp" -m tcp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination
-A KUBE-SEP-SFOMQPZHZQCHPEUA -s 10.244.239.181/32 -m comment --comment "kube-system/kube-dns:metrics" -j KUBE-MARK-MASQ
-A KUBE-SEP-SFOMQPZHZQCHPEUA -p tcp -m comment --comment "kube-system/kube-dns:metrics" -m tcp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination 0.0.0.0:0
-A KUBE-SEP-U2Y7HR2YOTSRNSOV -s 10.244.239.181/32 -m comment --comment "kube-system/kube-dns:dns" -j KUBE-MARK-MASQ
-A KUBE-SEP-U2Y7HR2YOTSRNSOV -p udp -m comment --comment "kube-system/kube-dns:dns" -m udp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination
-A KUBE-SERVICES -d 10.96.0.10/32 -p tcp -m comment --comment "kube-system/kube-dns:dns-tcp cluster IP" -m tcp --dport 53 -j KUBE-SVC-ERIFXISQEP7F7OF4
-A KUBE-SERVICES -d 10.96.0.10/32 -p udp -m comment --comment "kube-system/kube-dns:dns cluster IP" -m udp --dport 53 -j KUBE-SVC-TCOU7JCQXEZGVUNU
-A KUBE-SERVICES -d 10.96.0.10/32 -p tcp -m comment --comment "kube-system/kube-dns:metrics cluster IP" -m tcp --dport 9153 -j KUBE-SVC-JD5MR3NA4I4DYORP
-A KUBE-SVC-ERIFXISQEP7F7OF4 ! -s 10.244.0.0/16 -d 10.96.0.10/32 -p tcp -m comment --comment "kube-system/kube-dns:dns-tcp cluster IP" -m tcp --dport 53 -j KUBE-MARK-MASQ
-A KUBE-SVC-ERIFXISQEP7F7OF4 -m comment --comment "kube-system/kube-dns:dns-tcp -> 10.244.239.180:53" -m statistic --mode random --probability 0.50000000000 -j KUBE-SEP-EXV2TBBOQI4PJ6BU
-A KUBE-SVC-ERIFXISQEP7F7OF4 -m comment --comment "kube-system/kube-dns:dns-tcp -> 10.244.239.181:53" -j KUBE-SEP-LFDUPWVFYGWAZ4R3
-A KUBE-SVC-JD5MR3NA4I4DYORP ! -s 10.244.0.0/16 -d 10.96.0.10/32 -p tcp -m comment --comment "kube-system/kube-dns:metrics cluster IP" -m tcp --dport 9153 -j KUBE-MARK-MASQ
-A KUBE-SVC-JD5MR3NA4I4DYORP -m comment --comment "kube-system/kube-dns:metrics -> 10.244.239.180:9153" -m statistic --mode random --probability 0.50000000000 -j KUBE-SEP-DHR7P54HYQ2XZLIB
-A KUBE-SVC-JD5MR3NA4I4DYORP -m comment --comment "kube-system/kube-dns:metrics -> 10.244.239.181:9153" -j KUBE-SEP-SFOMQPZHZQCHPEUA
-A KUBE-SVC-TCOU7JCQXEZGVUNU ! -s 10.244.0.0/16 -d 10.96.0.10/32 -p udp -m comment --comment "kube-system/kube-dns:dns cluster IP" -m udp --dport 53 -j KUBE-MARK-MASQ
-A KUBE-SVC-TCOU7JCQXEZGVUNU -m comment --comment "kube-system/kube-dns:dns -> 10.244.239.180:53" -m statistic --mode random --probability 0.50000000000 -j KUBE-SEP-FWGM64AFGGHPYAZF
-A KUBE-SVC-TCOU7JCQXEZGVUNU -m comment --comment "kube-system/kube-dns:dns -> 10.244.239.181:53" -j KUBE-SEP-U2Y7HR2YOTSRNSOV
┌──[root@vms100.liruilongs.github.io]-[~/docker]
└─$
```



```yaml
  - name: dns
    port: 53
    protocol: UDP
    targetPort: 53
  - name: dns-tcp
    port: 53
    protocol: TCP
    targetPort: 53
  - name: metrics
    port: 9153
    protocol: TCP
    targetPort: 9153
```

#### 去的报文

Cluster  IP的访问方式。Cluster  IP的访问入口链是 `KUBE-SERVICES`。直接访问ClusterIP(10.96.0.10/32)的声明的端口会跳转到对应的链见下面

```bash
-A KUBE-SERVICES -d 10.96.0.10/32 -p tcp -m comment --comment "kube-system/kube-dns:dns-tcp cluster IP" -m tcp --dport 53 -j KUBE-SVC-ERIFXISQEP7F7OF4
-A KUBE-SERVICES -d 10.96.0.10/32 -p udp -m comment --comment "kube-system/kube-dns:dns cluster IP" -m udp --dport 53 -j KUBE-SVC-TCOU7JCQXEZGVUNU
-A KUBE-SERVICES -d 10.96.0.10/32 -p tcp -m comment --comment "kube-system/kube-dns:metrics cluster IP" -m tcp --dport 9153 -j KUBE-SVC-JD5MR3NA4I4DYORP
```
这里我们找其中一条链看一下 
```bash
-A KUBE-SERVICES -d 10.96.0.10/32 -p tcp -m comment --comment "kube-system/kube-dns:dns-tcp cluster IP" -m tcp --dport 53 -j KUBE-SVC-ERIFXISQEP7F7OF4
```
最后跳转到了 `-j KUBE-SVC-ERIFXISQEP7F7OF4`, 然后我们看下 这条链上有什么规则

```bash
-A KUBE-SVC-ERIFXISQEP7F7OF4 -m comment --comment "kube-system/kube-dns:dns-tcp -> 10.244.239.180:53" -m statistic --mode random --probability 0.50000000000 -j KUBE-SEP-EXV2TBBOQI4PJ6BU
-A KUBE-SVC-ERIFXISQEP7F7OF4 -m comment --comment "kube-system/kube-dns:dns-tcp -> 10.244.239.181:53" -j KUBE-SEP-LFDUPWVFYGWAZ4R3
```

这里利用了 iptables 的 random 模块，使连接有 50% 的概率进入 `KUBE-SEP-EXV2TBBOQI4PJ6BU` 链，50% 的概率进入  `KUBE-SEP-LFDUPWVFYGWAZ4R3` 链。因此，Kube-proxy的iptables模式采用 `随机数` 实现了服务的负载均衡.

这里的  `KUBE-SEP-XXXXX` 对应为 DNS 服务创建的两个端点(Endpoint)的名称，该端点对应了后端 Pod 的 IP 地址和端口。

```bash
┌──[root@vms100.liruilongs.github.io]-[~/docker]
└─$kubectl describe endpoints -n kube-system kube-dns
Name:         kube-dns
Namespace:    kube-system
Labels:       k8s-app=kube-dns
              kubernetes.io/cluster-service=true
              kubernetes.io/name=CoreDNS
Annotations:  endpoints.kubernetes.io/last-change-trigger-time: 2023-03-28T07:36:31Z
Subsets:
  Addresses:          10.244.239.180,10.244.239.181
  NotReadyAddresses:  <none>
  Ports:
    Name     Port  Protocol
    ----     ----  --------
    dns-tcp  53    TCP
    dns      53    UDP
    metrics  9153  TCP

Events:  <none>
┌──[root@vms100.liruilongs.github.io]-[~/docker]
└─$
```

```bash
-A KUBE-SEP-EXV2TBBOQI4PJ6BU -p tcp -m comment --comment "kube-system/kube-dns:dns-tcp" -m tcp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination
-A KUBE-SEP-LFDUPWVFYGWAZ4R3 -p tcp -m comment --comment "kube-system/kube-dns:dns-tcp" -m tcp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination
```

将来自 Kubernetes 集群中 kube-dns 组件的 TCP 流量，转发到本机的一个空闲端口上进行处理。并保持持久(persistent)

这里和书里的有些出入，书里的是直接 SNAT 到具体的 提供服务能力的端口。这个转发到一个随机的端口，这里 kube-proxy 会完全随机地选择一个端口，将请求转发到该端口上。因此，该端口是由 kube-proxy 动态分配的，不是固定的，可以通过命令 sudo netstat -tunlp 查看空闲端口。


综上所述，iptables模式最主要的链是
+ `KUBE-SERVICES`、
+ `KUBE-SVC-*`
+ `KUBE-SEP-*`。

`KUBE-SERVICES` 链是访问集群内服务的数据包入口点，它会根据匹配到的目标IP:port将数据包分发到相应的KUBE-SVC-*链；

`KUBE-SVC-*`链相当于一个负载均衡器，它会将数据包平均分发到 `KUBE-SEP-*` 链。每个 `KUBE-SVC-*` 链后面的 `KUBE-SEP-*` 链都和 `Service` 的后端Pod数量一样


KUBE-SEP-*链通过DNAT将连接的目的地址和端口从Service的IP:port替换为后端Pod的IP:port，从而将流量转发到相应的Pod。

最后，我们用图4-4总结Kube-proxy  iptables模式的工作流。

![在这里插入图片描述](https://img-blog.csdnimg.cn/cc2b1e9d45c846d9b8b538d3cc333b4c.png)


图4-4演示了从客户端Pod到不同节点上的服务器Pod的流量路径。客户端通过 172.16.12.100:80连接到服务。Kubernetes  API  Server会维护一个运行应用的后端Pod列表。每个节点上的Kube-proxy进程会根据Service和对应的Endpoints创建一系列的iptables规则，以将流量重定向到相应Pod(例如10.255.255.202:8080)。整个过程客户端Pod无须感知集群的拓扑或集群内Pod的任何IP信息。




####  回程报文:iptables的SNAT


既然利用 iptables 做了一次 DNAT，为了保证回程报文能够顺利返回，需要做一次 SNAT。Kube-proxy 创建的对应iptables规则形如：


```bash
-A KUBE-SEP-LFDUPWVFYGWAZ4R3 -s 10.244.239.181/32 -m comment --comment "kube-system/kube-dns:dns-tcp" -j KUBE-MARK-MASQ
-A KUBE-SEP-EXV2TBBOQI4PJ6BU -s 10.244.239.180/32 -m comment --comment "kube-system/kube-dns:dns-tcp" -j KUBE-MARK-MASQ
```


```bash
┌──[root@vms100.liruilongs.github.io]-[~/docker]
└─$iptables -S -t nat | grep  0x4000/0x4000
-A KUBE-MARK-MASQ -j MARK --set-xmark 0x4000/0x4000
-A KUBE-POSTROUTING -m mark ! --mark 0x4000/0x4000 -j RETURN
```

### 4.1.3 IPVS模式
### 4.1.4 iptables VS.IPVS
### 4.1.5 conntrack
### 4.1.6 小结

4.2 Kubernetes极客们的日常：DIY一个Ingress Controller
4.2.1 Ingress Controller的通用框架
4.2.2 Nginx Ingress Controller详解
4.2.3 小结
4.3 沧海桑田：Kubernetes DNS架构演进之路
4.3.1 Kube-dns的工作原理
4.3.2 上位的CoreDNS
4.3.3 Kube-dns VS.CoreDNS
4.3.4 小结
4.4 你的安全我负责：使用Calico提供Kubernetes网络策略
4.4.1 部署一个带Calico的Kubernetes集群
4.4.2 测试Calico网络策略
第5章 百花齐放：Kubernetes网络插件生态
5.1 从入门到放弃：Docker原生网络的不足
5.2 CNI标准的胜出：从此江湖没有CNM
5.2.1 CNI与CNM的转换
5.2.2 CNI的工作原理
5.2.3 为什么Kubernetes不使用Libnetwork
5.3 Kubernetes网络插件鼻祖flannel
5.3.1 flannel简介
5.3.2 flannel安装配置
5.3.3 flannel backend详解
5.3.4 flannel与etcd
5.3.5 小结
## 5.4 全能大三层网络插件：Calico

Calico 提供了纯3层的网络模型，三层通信模型表示每个容器都通过IP直接通信，中间通过路由转发找到对方。

在这个过程中，容器所在的节点类似于传统的路由器，提供了路由查找的功能。

要想路由能够正常工作，每个容器所在的主机节点扮演了虚拟路由器(vRouter)的功能，而且这些vRouter必须有某种方法，能够知道整个集群的路由信息。

Calico 通过标准的 BGP 路由协议。相当于在每个节点上模拟出一个额外的路由器，由于采用的是标准协议，Calico模拟路由器的路由表信息可以被传播到网络的其他路由设备中，这样就实现了在三层网络上的高速跨节点网络。




### 5.4.1 Calico简介

Calico以其性能、灵活性闻名于Kubernetes生态系统。Calico的功能更全面，正如Calico的slogan
提到的： `为容器和虚拟机工作负载提供一个安全的网络连接。`


Calico以其丰富的网络功能著称，不仅提供容器的网络解决方案，还可以用在虚拟机网络上



Calico基于iptables实现了Kubernetes的网络策略，通过在各个节点上应用ACL(访问控制列表)提供工作负载的多租户隔离、安全组及其他可达性限制等功能。

Calico还可以与服务网格Istio集成，以便在服务网格层和网络基础架构层中解释和实施集群内工作负载的网络策略。

Calico还支持容器漂移。因为Calico配置的三层网络使用BGP路由协议在主机之间路由数据包。


Calico在每一个计算节点利用Linux内核的一些能力实现了一个高效的vRouter负责数据转发，而每个vRouter通过BGP把自己运行的工作负载的路由信息向整个Calico网络传播。小规模部署可以直接互联，大规
模下可以通过指定的BGP  Route  Reflector 完成.

#### 1. 名词解释

在深入解析Calico原理前，让我们先来熟悉Calico的几个重要名词：
+ Endpoint：接入Calico网络中的网卡(IP)；
+ AS：网络自治系统，通过BGP与其他AS网络交换路由信息；
+ iBGP：AS内部的BGP Speaker，与同一个AS内部的iBGP、eBGP交换路由信息
+ eBGP：AS边界的BGP Speaker，与同一个AS内部的iBGP、其他AS的eBGP交换路由信息；
+ workloadEndpoint：虚拟机和容器端点，一般指它们的IP地址；
+ hostEndpoint：宿主机端点，一般指它们的IP地址。



### 5.4.2 Calico的隧道模式
### 5.4.3 安装Calico
### 5.4.4 Calico报文路径
### 5.4.5 Calico使用指南
### 5.4.6 为什么Calico网络选择BGP
### 5.4.7 小结
5.5 Weave：支持数据加密的网络插件
5.5.1 Weave简介
5.5.2 Weave实现原理
5.5.3 Weave安装
5.5.4 Weave网络通信模型
5.5.5 Weave的应用示例
5.5.6 小结
5.6 Cilium：为微服务网络连接安全而生
5.6.1 为什么使用Cilium
5.6.2 以API为中心的微服务安全
5.6.3 BPF优化的数据平面性能
5.6.4 试用Cilium：网络策略
5.6.5 小结
5.7 Kubernetes多网络的先行者：CNI-Genie
5.7.1 为什么需要CNI-Genie
5.7.2 CNI-Genie功能速递
5.7.3 容器多IP
第6章 Kubernetes网络下半场：Istio
6.1 微服务架构的大地震：sidecar模式
6.1.1 你真的需要Service Mesh吗
6.1.2 sidecar模式
6.1.3 Service Mesh与sidecar
6.1.4 Kubernetes Service VS.Service Mesh
6.1.5 Service Mesh典型实现之Linkerd
6.2 Istio：引领新一代微服务架构潮流
6.2.1 Istio简介
6.2.2 Istio安装
6.2.3 Istio路由规则的实现
6.3 一切尽在不言中：Istio sidecar透明注入
6.3.1 Init容器
6.3.2 sidecar注入示例
6.3.3 手工注入sidecar
6.3.4 自动注入sidecar
6.3.5 从应用容器到sidecar代理的通信
6.4 不再为iptables脚本所困：Istio CNI插件
6.5 除了微服务，Istio还能做更多







## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
