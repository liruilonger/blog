---
title: DO447之Ansible实战
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-08-26 04:30:09/DO447之Ansible实战.html'
mathJax: false
date: 2022-08-26 12:30:09
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

### 一、 创建用户

>workstation.lab.example.com 节点上为用户管理员配置 git:
git 用户名称: git  
git 用户电子邮件: git@workstation.lab.example.com
默认 push 方式: simple
git 项目 create_users 负责管理人员与硬件
使用以下信息来相应地更新项目存储库：
可以点击 http://git.lab.example.com:8081/git/create_users.git 找到 create_users 的git项目
create_users.yml 是该项目使用的剧本 的文件名
developer 组中添加用户 greg
在dev组中增加 serverc 节点
除上面列出的以外，请勿进行任何其他更改



用户的批量创建
```bash
$ sudo yum -y install git vim
$ git config --global user.name 'git'
$ git config --global user.email git@workstation.lab.example.com
$ git config --global push.default simple
$ git config -l
user.name=gituser
user.email=git@workstation.lab.example.com
push.default=simple
$ git clone http://git.lab.example.com:8081/git/create_users.git
$ cd create_users
$ vim user_list.yml
```

```bash
[student@workstation create_users]$ cat inventory create_users.yml
[dev]
servera
serverc
[prod]
serverb
---
- name: add groups
  hosts: dev prod
  become: yes

  vars_files:
    - user_list.yml

  tasks:
    - name: add developer accounts
      group:
        name: "{{ item.group }}"
        state: present
      with_items: "{{ users }}"

- name: create user accounts
  hosts: dev prod
  become: yes

  vars_files:
    - user_list.yml

  tasks:
    - name: add user accounts
      user:
        name: "{{ item.name }}"
        password: "{{ 'flectrag' | password_hash('sha512') }}"
        groups: "{{ item.group }}"
        append: yes
      with_items: "{{ users }}"
```
提交代码

### 2. 管理 web 服务器

>在 Git 存储库 httpd_alias 中管理的 Web 服务器配置需要添加别名，使用以下信息相应的更新项目仓库文件: 
可以点击 http://git.lab.example.com:8081/git/httpd_alias.git 找到用来管理 http 别名的 Git 项目
部署新别名的剧本 文件是 install_httpd_alias.yml
只有在安装别名时，才会重启 httpd 服务器。也就是说，如果已经安装了别名，再运行剧本，则不会重新启动 httpd 服务。
除上面列出的以外，请勿进行任何其他更改


```bash
$ cd ~
$ git clone \
http://git.lab.example.com:8081/git/httpd_alias.git
$ cd httpd_alias/
$ vim install_httpd_alias.yml
```
```yaml
[student@workstation httpd_alias]$ cat install_httpd_alias.yml
---
- name: Add Apache alias
  hosts: prod
  become: yes

  tasks:
    - name: copy alias file
      copy:
        src: alias.conf
        dest: /etc/httpd/conf.d
      notify: restart httpd
  handlers:
    - name: restart httpd
      service:
         name: httpd
         state: restarted


```
提交代码


### 3. 管理网站内容

>在部署到生产之前，dev web 服务器用于测试网站内容。
Git 项目 httpd_alias 包含一个不完整的剧本 ，文件名为 manage_content.yml ，用于管理 dev web
服务器的内容。在 Git 仓库中( http://git.lab.example.com:8081/git/manage_content.git )完
善该剧本，实现：
当使用标签 alpha 来运行该剧本 时，将生成内容“ Where there is a will, there is a way.”并部
署到 dev 主机上的 /var/www/html/index.html 文件中
当使用标签 beta 来运行该剧本 时，将生成以下信息“Experience is the mother of wisdom. ”，并
且保存到 dev 主机的 /var/www/html/index.html 文件中。
如果没有使用以上任何一个标签运行该剧本，则在受管主机上既不产生也不保存任何信息。
除上面列出的以外，请勿进行任何其他更改


```bash
$ cd ~
$ git clone http://git.lab.example.com:8081/git/manage_content.git
$ cd manage_content/
$ vim manage_content.yml
```

```yaml
---
- name: Deploy content
  hosts: dev
  become: yes
  tasks:
    - name: deploy on dev alpha
      copy:
        content: "Where there is a will, there is a way.\n"
        dest: /var/www/html/index.html
      tags:
          - alpha
          - never
    - name: deoy on dev beta
      copy:
        content: "Experience is the mother of wisdom.\n"
        dest:  /var/www/html/index.html
      tags:
          - beta
          - never
```

```bash
$ ansible-playbook manage_content.yml
$ ansible-playbook -t beta manage_content.yml
$ curl http://servera
$ ansible-playbook -t alpha manage_content.yml
$ curl http://serverb
$ git add .
$ git commit -m "Q3"
$ git push
```
### 4. Ansible 调优

>按照以下要求更新 Git 仓库( http://git.lab.example.com:8081/git/tune_ansible.git )中的
Ansible 配置文件:
默认情况下，gathering of facts 是被禁用的；
最大并发主机连接数为 45
除上面列出的以外，请勿进行任何其他更改。

```bash
$ cd ~
$ git clone http://git.lab.example.com:8081/git/tune_ansible.git
$ cd tune_ansible/
$ vim ansible.cfg
[defaults]
inventory = ./inventory
remote_user = devops
ask_pass = false
forks = 45
gathering = explicit
[privilege_escalation]
become = false
become_method = sudo
become_user = root
become_ask_pass = false
```
配置查找方式
```bash
[student@workstation ~]$ cat /etc/ansible/ansible.cfg | grep forks
#forks          = 5
[student@workstation ~]$ cat /etc/ansible/ansible.cfg | grep gather
# plays will gather facts by default, which contain information about
# smart - gather by default, but don't regather if already gathered
# implicit - gather by default, turn off with gather_facts: False
# explicit - do not gather by default, must say gather_facts: True
#gathering = implicit
# This only affects the gathering done by a play's gather_facts directive,
# by default gathering retrieves all facts subsets
# all - gather all subsets
# network - gather min and network facts
# hardware - gather hardware facts (longest facts to retrieve)
# virtual - gather min and virtual facts
# A minimal set of facts is always gathered.
#gather_subset = all
# gather_timeout = 10
[student@workstation ~]$
```


### 5. 从列表创建用户
>Git 仓库(http://git.lab.example.com:8081/git/create_users_complex.git )包含以下资源：
user_list.yml ，这是一个用户账户清单，该文件包含多字段:
name 字段指定账户的用户名和登录 ID；
first 字段指定用户的 first name；
middle 字段指定用户的 middle name；
last 字段指定用户的 last name；
uid 字段指定账户关联的用户 ID；
inventory 是本任务涉及的主机清单文件。
不要对以上文件做任何修改。
利用以上创建剧本，实现以下操作:
剧本 文件名为 create_users.yml ，在 inventory 规定的主机上运行时，该剧本 会根据
user_list.yml 文件内容，使用指定的用户 ID 创建用户账户
针对每个账户，该剧本 会按照以下要求生成随机的 6 位数字密码
必须使用 SHA-512 对密码进行加密
密码的纯文本版本和用于生成密码的随机值“salt”必须存储在名为“password-<name>”的文件中，其
中<name>是与该帐户关联的用户名。例如，针对名为“frederick”用户，密码和“salt”存储在文件
password-frederick 中。
剧本 需要在其运行的目录中生成 password-<name>文件。
针对每个账户，user comment (GECOS)字段需要按照以下格式要求为用户设置恰当的名字:First Middle
Last(中间有单空格符)，如上所示，名字的每个组成部分都必须大写。
需要将完整的剧本 提交并上传到仓库中。

```bash
$ cd ~
$ git clone http://git.lab.example.com:8081/git/create_users_complex.git
$ cd create_users_complex/
$ vim create_users.yml
```
```yaml
[student@workstation create_users_complex]$ cat create_users.yml
---
- name: create users
  hosts: dev,prod
  become: true
  vars_files:
     - user_list.yml
  tasks:
     - name: create user
       user:
         name: "{{ item.name }}"
         uid: "{{ item.uid }}"
         comment: "{{ item.first | capitalize }} {{ item.middle | capitalize }} {{ item.last | capitalize }}"
         password: "{{ lookup('password', 'password-{{item.name}} chars=digits length=6') | password_hash('sha512') }}"
       loop: '{{ users }}'
```

```bash
[student@workstation create_users_complex]$ ansible-doc -t lookup password
[student@workstation create_users_complex]$ cat /usr/lib/python3.6/site-packages/jinja2/filters.py | grep capi
[student@workstation ~]$ more /usr/lib/python3.6/site-packages/ansible/plugins/filter/core.py ^C
[student@workstation ~]$ more /usr/lib/python3.6/site-packages/jinja2/filters.py ^C
[student@workstation create_users_complex]$ ansible-doc user
```


### 6. 配置剧本资源统计信息
>git仓库(http://git.lab.example.com:8081/git/resource_stat.git)包含以下资源
完整的剧本,文件名为install_vim.yml
不完整的ansible.cfg
inventory和install_vim.yml关联的主机清单文件
使用以上资源按要求完成以下事项：
创建第二个剧本,完善已经提供的配置文件，实现以下要求：
剧本的文件名为create_cgroup.yml
create_cgroup.yml只在本机上运行
create_cgroup.yml会创建控制组do447,包含以下参数
拥有控制组文件的用户与用户组均是devops
控制器列表：
- cpuacct
- memory
- pids
- 相对路径: do447_stats
更新配置以便在控制组do447_stats中运行剧本install_vim.yml时，显示该剧本中任务的
系统活动。报告的活动应包括 CPU 时间和内存使用情况的摘要。
完成后的剧本create_cgroup.yml 和对配置所做的任何更改都应提交并推送到存储库
除上面列出的以外，请勿进行任何其他更改


```bash
$ cd ~
$ git clone http://git.lab.example.com:8081/git/resource_stat.git
$ cd resource_stat/
$ vim create_cgroup.yml
```
```yaml
---
- name: Create cgroup
hosts: localhost
become: true
tasks:
- name: install libcgroup-tools
  yum:
    name: libcgroup-tools
    state: latest
- name: create cgroup
  shell: 'cgcreate -a devops:devops -t devops:devops -g cpuacct,memory,pids:do447_stats'
```

```bash
$ vim ansible.cfg
[defaults]
inventory = ./inventory
remote_user = devops
ask_pass = false
callback_whitelist = timer, profile_tasks, profile_roles, cgroup_perf_recap
[callback_cgroup_perf_recap]
control_group = do447_stats
[privilege_escalation]
become = false
become_method = sudo
become_user = root
become_ask_pass = false
$ ansible-playbook install_vim.yml
$ git add .
$ git commit -m "Q6"
$ git push
```

```bash
[student@workstation ~]$ yum provides cgcreate
[student@workstation ~]$ ansible-doc yum^C
[student@workstation ~]$

```

## 博文参考


