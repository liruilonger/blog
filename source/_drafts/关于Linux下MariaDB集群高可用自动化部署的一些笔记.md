---
title: 关于Linux下MariaDB集群高可用自动化部署的一些笔记
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-09-30 05:43:19/关于Linux下MariaDB集群高可用自动化部署的一些笔记.html'
mathJax: false
date: 2022-09-30 13:43:19
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***
一、MHA 概述与环境准备
1.1 MHA概述
1.1.0 集群
什么是集群？
使用多台服务器提供相同的服务
集群的类型？
LB(负载均衡集群) HA(高可用集群) 主 备用
集群软件？
负载均衡(haproxy、LVS、Nginx)、高可用(keepalived)
配置MySQL集群高可用？
MHA软件 + MySQL主从同步结构
1.1.1 MHA简介
MHA(Master High Availability)
由日本DeNA公司 youshimaton开发
是一套优秀的实现 MySQL高可用的解决方案
数据库的自动故障切换操作能做到在 0~30 秒之间完成
MHA能确保在故障切换过程中最大限度保证数据的一致性，以达到真正意义的高可用。
1.1.2 MHA组成
MHA Manager (管理节点)
管理所有数据库服务器
可以单独部署在一台独立的机器上
也可以部署在某台数据库服务器上
MHA Node(数据节点)
存储数据的MySQL服务器
运行在每台MySQL服务器上
1.1.3 MHA工作过程
具体如下：
由Manager 定时探测集群中的master 节点；
当master故障时，manager自动将拥有最新数据的slave提升为新的master；


MHA集群部署完成以后：
  #1》客户端【client50】通过VIP(虚拟机IP)访问主服务器【host51】;
  #2》当主服务器【host51】宕机以后，MHA的管理主机【mgm57】会将VIP分配给拥有最新数据的从服
务器【host52】,客户端用户则是通过VIP地址访问从服务器【host52】；
  #3》当从服务器【host52】也宕机以后，MHA的管理主机【mgm57】又会将VIP地址分配给最新数据的
从服务器【host53】,客户端用户又会通过VIP地址访问从服务器【host53】；
通过上述过程达到数据库服务器高可用的目的。



```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$ssh-copy-id root@192.168.26.154
┌──[root@vms153.liruilongs.github.io]-[~]
└─$ssh-copy-id root@192.168.26.155
┌──[root@vms153.liruilongs.github.io]-[~]
└─$
```
```bash
┌──[root@vms154.liruilongs.github.io]-[~]
└─$ssh-copy-id root@192.168.26.153
┌──[root@vms154.liruilongs.github.io]-[~]
└─$ssh-copy-id root@192.168.26.155
┌──[root@vms154.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms155.liruilongs.github.io]-[~]
└─$ssh-copy-id  root@192.168.26.153
┌──[root@vms155.liruilongs.github.io]-[~]
└─$ssh-copy-id  root@192.168.26.154
┌──[root@vms155.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms155.liruilongs.github.io]-[~]
└─$mysql -uroot -pliruilong
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 7
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> change master to
    -> master_host="192.168.26.153",
    -> master_user="repluser",
    -> master_password="repluser",
    -> master_log_file="master153.000008",
    -> master_log_pos=1487;
ERROR 1198 (HY000): This operation cannot be performed with a running slave; run STOP SLAVE first
MariaDB [(none)]> stop slave
    -> ;
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> change master to
    -> change master to master_host="192.168.26.153", master_user="repluser", master_password="repluser", master_log_file="master153.000008", master_log_pos=1487;
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'change master to master_host="192.168.26.153", master_user="repluser", master_pa' at line 2
MariaDB [(none)]> change master to master_host="192.168.26.153", master_user="repluser", master_password="repluser", master_log_file="master153.000008", master_log_pos=1487;
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> start slave;
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]> show slave status\G
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.26.153
                  Master_User: repluser
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: master153.000008
          Read_Master_Log_Pos: 1487
               Relay_Log_File: mariadb-relay-bin.000002
                Relay_Log_Pos: 529
        Relay_Master_Log_File: master153.000008
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB:
          Replicate_Ignore_DB:
           Replicate_Do_Table:
       Replicate_Ignore_Table:
      Replicate_Wild_Do_Table:
  Replicate_Wild_Ignore_Table:
                   Last_Errno: 0
                   Last_Error:
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 1487
              Relay_Log_Space: 825
              Until_Condition: None
               Until_Log_File:
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File:
           Master_SSL_CA_Path:
              Master_SSL_Cert:
            Master_SSL_Cipher:
               Master_SSL_Key:
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error:
               Last_SQL_Errno: 0
               Last_SQL_Error:
  Replicate_Ignore_Server_Ids:
             Master_Server_Id: 153
1 row in set (0.00 sec)

MariaDB [(none)]>
```

```bash

```
案例2：配置MySQL一主多从
1. 配置主机51为主服务器
2. 配置主机52为51的从服务器
3. 配置主机53为51的从服务器
小结
配置主服务器
配置从服务器
测试配置


三、配置MHA管理主机
3.0 配置MHA管理主机的步骤
1. 安装软件包
2. 创建并编辑主配置文件
3. 创建故障切换脚本并编辑
4. 把 vip 地址指定在当前的主服务器host51
3.1 安装软件
3.1.1 安装软件

```bash
┌──[root@vms156.liruilongs.github.io]-[~]
└─$wget https://github.com/yoshinorim/mha4mysql-manager/releases/download/v0.58/mha4mysql-manager-0.58.tar.gz
--2022-09-30 20:59:01--  https://github.com/yoshinorim/mha4mysql-manager/releases/download/v0.58/mha4mysql-manager-0.58.tar.gz
正在解析主机 github.com (github.com)... 20.205.243.166
正在连接 github.com (github.com)|20.205.243.166|:443... 已连接。
已发出 HTTP 请求，正在等待回应... 302 Found
位置：https://objects.githubusercontent.com/github-production-release-asset-2e65be/2093236/ec72c8ae-2de4-11e8-97d2-23e26f1f9623?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20220930%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20220930T125909Z&X-Amz-Expires=300&X-Amz-Signature=194a64e6609527d7da86435d71625ce864e4dbfeaed35ec154e2db2381f1860d&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=2093236&response-content-disposition=attachment%3B%20filename%3Dmha4mysql-manager-0.58.tar.gz&response-content-type=application%2Foctet-stream [跟随至新的 URL]
--2022-09-30 20:59:09--  https://objects.githubusercontent.com/github-production-release-asset-2e65be/2093236/ec72c8ae-2de4-11e8-97d2-23e26f1f9623?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20220930%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20220930T125909Z&X-Amz-Expires=300&X-Amz-Signature=194a64e6609527d7da86435d71625ce864e4dbfeaed35ec154e2db2381f1860d&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=2093236&response-content-disposition=attachment%3B%20filename%3Dmha4mysql-manager-0.58.tar.gz&response-content-type=application%2Foctet-stream
正在解析主机 objects.githubusercontent.com (objects.githubusercontent.com)... 185.199.110.133, 185.199.109.133, 185.199.108.133, ...
正在连接 objects.githubusercontent.com (objects.githubusercontent.com)|185.199.110.133|:443... 已连接。
已发出 HTTP 请求，正在等待回应... 200 OK
长度：119801 (117K) [application/octet-stream]
正在保存至: “mha4mysql-manager-0.58.tar.gz”

100%[=======================================================================================================================================================================>] 119,801     73.8KB/s 用时 1.6s

2022-09-30 20:59:12 (73.8 KB/s) - 已保存 “mha4mysql-manager-0.58.tar.gz” [119801/119801])

┌──[root@vms156.liruilongs.github.io]-[~]
└─$wget https://github.com/yoshinorim/mha4mysql-manager/releases/download/v0.58/mha4mysql-manager-0.58.tar.gz
--2022-09-30 21:14:43--  https://github.com/yoshinorim/mha4mysql-manager/releases/download/v0.58/mha4mysql-manager-0.58.tar.gz
正在解析主机 github.com (github.com)... 20.205.243.166
正在连接 github.com (github.com)|20.205.243.166|:443... 已连接。
已发出 HTTP 请求，正在等待回应... 302 Found
位置：https://objects.githubusercontent.com/github-production-release-asset-2e65be/2093236/ec72c8ae-2de4-11e8-97d2-23e26f1f9623?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20220930%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20220930T131444Z&X-Amz-Expires=300&X-Amz-Signature=fa5b68958ecec92a9dbdb24ae5445a0be768f81852002c3b539fa171d75cb3a9&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=2093236&response-content-disposition=attachment%3B%20filename%3Dmha4mysql-manager-0.58.tar.gz&response-content-type=application%2Foctet-stream [跟随至新的 URL]
--2022-09-30 21:14:44--  https://objects.githubusercontent.com/github-production-release-asset-2e65be/2093236/ec72c8ae-2de4-11e8-97d2-23e26f1f9623?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20220930%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20220930T131444Z&X-Amz-Expires=300&X-Amz-Signature=fa5b68958ecec92a9dbdb24ae5445a0be768f81852002c3b539fa171d75cb3a9&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=2093236&response-content-disposition=attachment%3B%20filename%3Dmha4mysql-manager-0.58.tar.gz&response-content-type=application%2Foctet-stream
正在解析主机 objects.githubusercontent.com (objects.githubusercontent.com)... 185.199.108.133, 185.199.111.133, 185.199.110.133, ...
正在连接 objects.githubusercontent.com (objects.githubusercontent.com)|185.199.108.133|:443... 已连接。
已发出 HTTP 请求，正在等待回应... 200 OK
长度：119801 (117K) [application/octet-stream]
正在保存至: “mha4mysql-manager-0.58.tar.gz.1”

100%[=======================================================================================================================================================================>] 119,801      165KB/s 用时 0.7s

2022-09-30 21:14:46 (165 KB/s) - 已保存 “mha4mysql-manager-0.58.tar.gz.1” [119801/119801])

┌──[root@vms156.liruilongs.github.io]-[~]
└─$
┌──[root@vms156.liruilongs.github.io]-[~]
└─$ls
anaconda-ks.cfg  calico_3_14.tar  calico.yaml  ifcfg-lo  mha4mysql-manager-0.58.tar.gz  mha4mysql-manager-0.58.tar.gz.1  one-client-install.sh  set.sh  sysctl.conf
┌──[root@vms156.liruilongs.github.io]-[~]
└─$wget https://github.com/yoshinorim/mha4mysql-node/releases/download/v0.58/mha4mysql-node-0.58-0.el7.centos.noarch.rpm
--2022-09-30 21:16:41--  https://github.com/yoshinorim/mha4mysql-node/releases/download/v0.58/mha4mysql-node-0.58-0.el7.centos.noarch.rpm
正在解析主机 github.com (github.com)... 20.205.243.166
正在连接 github.com (github.com)|20.205.243.166|:443... 已连接。
已发出 HTTP 请求，正在等待回应... 302 Found
位置：https://objects.githubusercontent.com/github-production-release-asset-2e65be/2093258/993094a0-2de4-11e8-9c8a-f5a050f404a9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20220930%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20220930T131641Z&X-Amz-Expires=300&X-Amz-Signature=849187d5a4af49bd7254d2e4cc789824a538bd97ecf439267aa5a01b7b4b019c&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=2093258&response-content-disposition=attachment%3B%20filename%3Dmha4mysql-node-0.58-0.el7.centos.noarch.rpm&response-content-type=application%2Foctet-stream [跟随至新的 URL]
--2022-09-30 21:16:42--  https://objects.githubusercontent.com/github-production-release-asset-2e65be/2093258/993094a0-2de4-11e8-9c8a-f5a050f404a9?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20220930%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20220930T131641Z&X-Amz-Expires=300&X-Amz-Signature=849187d5a4af49bd7254d2e4cc789824a538bd97ecf439267aa5a01b7b4b019c&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=2093258&response-content-disposition=attachment%3B%20filename%3Dmha4mysql-node-0.58-0.el7.centos.noarch.rpm&response-content-type=application%2Foctet-stream
正在解析主机 objects.githubusercontent.com (objects.githubusercontent.com)... 185.199.108.133, 185.199.111.133, 185.199.110.133, ...
正在连接 objects.githubusercontent.com (objects.githubusercontent.com)|185.199.108.133|:443... 已连接。
已发出 HTTP 请求，正在等待回应... 200 OK
长度：36328 (35K) [application/octet-stream]
正在保存至: “mha4mysql-node-0.58-0.el7.centos.noarch.rpm”

100%[=======================================================================================================================================================================>] 36,328      79.9KB/s 用时 0.4s

2022-09-30 21:16:44 (79.9 KB/s) - 已保存 “mha4mysql-node-0.58-0.el7.centos.noarch.rpm” [36328/36328])

┌──[root@vms156.liruilongs.github.io]-[~]
└─$rpm -ivh mha4mysql-node-0.58-0.el7.centos.noarch.rpm
准备中...                          ################################# [100%]
正在升级/安装...
   1:mha4mysql-node-0.58-0.el7.centos ################################# [100%]
┌──[root@vms156.liruilongs.github.io]-[~]
└─$tar -xf mha4mysql-manager-0.58.tar.gz
┌──[root@vms156.liruilongs.github.io]-[~]
└─$per
perl        perl5.16.3  perlbug     perldoc     perlthanks  perror
┌──[root@vms156.liruilongs.github.io]-[~]
└─$perl ./mha4mysql-manager-0.58/Makefile.PL
Can't locate inc/Module/Install.pm in @INC (@INC contains: /usr/local/lib64/perl5 /usr/local/share/perl5 /usr/lib64/perl5/vendor_perl /usr/share/perl5/vendor_perl /usr/lib64/perl5 /usr/share/perl5 .) at ./mha4mysql-manager-0.58/Makefile.PL line 1.
BEGIN failed--compilation aborted at ./mha4mysql-manager-0.58/Makefile.PL line 1.
```
```bash 
┌──[root@vms156.liruilongs.github.io]-[~]
└─$yum install -y perl-DBD-MySQL perl-ExtUtils-MakeMaker perl-CPAN
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$yum -y install epel-release perl-CPAN* perl-DBD-MySQL  perl-Config-Tiny perl-Log-Dispatch perl-Parallel-ForkManager
┌──[root@vms156.liruilongs.github.io]-[~]
└─$yum -y install perl-ExtUtils*
┌──[root@vms156.liruilongs.github.io]-[~]
└─$yum -y install perl-CPAN*
┌──[root@vms156.liruilongs.github.io]-[~]
└─$ yum -y install perl-*.rpm
```

```bash
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$perl Makefile.PL
*** Module::AutoInstall version 1.06
*** Checking for Perl dependencies...
[Core Features]
- DBI                   ...loaded. (1.627)
- DBD::mysql            ...loaded. (4.023)
- Time::HiRes           ...loaded. (1.9725)
- Config::Tiny          ...loaded. (2.14)
- Log::Dispatch         ...loaded. (2.41)
- Parallel::ForkManager ...loaded. (1.18)
- MHA::NodeConst        ...loaded. (0.58)
*** Module::AutoInstall configuration finished.
Writing Makefile for mha4mysql::manager
Writing MYMETA.yml and MYMETA.json
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$make && make install
Skip blib/lib/MHA/ManagerUtil.pm (unchanged)
Skip blib/lib/MHA/Config.pm (unchanged)
Skip blib/lib/MHA/HealthCheck.pm (unchanged)
Skip blib/lib/MHA/ServerManager.pm (unchanged)
Skip blib/lib/MHA/ManagerConst.pm (unchanged)
Skip blib/lib/MHA/FileStatus.pm (unchanged)
Skip blib/lib/MHA/ManagerAdmin.pm (unchanged)
Skip blib/lib/MHA/ManagerAdminWrapper.pm (unchanged)
Skip blib/lib/MHA/MasterFailover.pm (unchanged)
Skip blib/lib/MHA/MasterRotate.pm (unchanged)
Skip blib/lib/MHA/MasterMonitor.pm (unchanged)
Skip blib/lib/MHA/Server.pm (unchanged)
Skip blib/lib/MHA/SSHCheck.pm (unchanged)
Skip blib/lib/MHA/DBHelper.pm (unchanged)
cp bin/masterha_stop blib/script/masterha_stop
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_stop
cp bin/masterha_conf_host blib/script/masterha_conf_host
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_conf_host
cp bin/masterha_check_repl blib/script/masterha_check_repl
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_check_repl
cp bin/masterha_check_status blib/script/masterha_check_status
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_check_status
cp bin/masterha_master_monitor blib/script/masterha_master_monitor
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_master_monitor
cp bin/masterha_check_ssh blib/script/masterha_check_ssh
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_check_ssh
cp bin/masterha_master_switch blib/script/masterha_master_switch
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_master_switch
cp bin/masterha_secondary_check blib/script/masterha_secondary_check
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_secondary_check
cp bin/masterha_manager blib/script/masterha_manager
/usr/bin/perl "-Iinc" -MExtUtils::MY -e 'MY->fixin(shift)' -- blib/script/masterha_manager
Manifying blib/man1/masterha_stop.1
Manifying blib/man1/masterha_conf_host.1
Manifying blib/man1/masterha_check_repl.1
Manifying blib/man1/masterha_check_status.1
Manifying blib/man1/masterha_master_monitor.1
Manifying blib/man1/masterha_check_ssh.1
Manifying blib/man1/masterha_master_switch.1
Manifying blib/man1/masterha_secondary_check.1
Manifying blib/man1/masterha_manager.1
Appending installation info to /usr/lib64/perl5/perllocal.pod
```

```bash
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$masterha_
masterha_check_repl       masterha_check_status     masterha_manager          masterha_master_switch    masterha_stop
masterha_check_ssh        masterha_conf_host        masterha_master_monitor   masterha_secondary_check
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$masterha_
masterha_check_repl       masterha_check_status     masterha_manager          masterha_master_switch    masterha_stop
masterha_check_ssh        masterha_conf_host        masterha_master_monitor   masterha_secondary_check
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$cd /root/mha4mysql-manager-0.58/
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$mkdir /etc/mha/
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$cp samples/conf/app1.cnf  /etc/m
machine-id                mailcap                   man_db.conf               mime.types                modprobe.d/               motd                      my.cnf
magic                     makedumpfile.conf.sample  mha/                      mke2fs.conf               modules-load.d/           mtab                      my.cnf.d/
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$cp samples/conf/app1.cnf  /etc/mha/
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$


```

```bash
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$cat /etc/mha//app1.cnf
[server default]
manager_workdir=/var/log/masterha/mha/
manager_log=/var/log/masterha/mha/manager.log
master_ip_failover_script=/etc/mha/master_ip_failover

ssh_user=root
ssh_port=22

repl_user=repluser
repl_password=repluser

user=root
password=liruilong

[server1]
hostname=192.168.26.153
port=3306
candidate_master=1

[server2]
hostname=192.168.26.154
port=3306
candidate_master=1

[server3]
hostname=192.168.26.155
port=3306
candidate_master=1
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$
```

## 博文参考


```bash
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$ls  samples/scripts/
master_ip_failover  master_ip_online_change  power_manager  send_report
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$cp samples/scripts/master_ip_failover /etc/mha/
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$chmod +x /etc/mha/master_ip_failover
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$vim +35 /etc/mha/master_ip_failover
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$vim +35 /etc/mha/master_ip_failover
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$head -n  40 /etc/mha/master_ip_failover | grep -v ^#



use strict;
use warnings FATAL => 'all';

use Getopt::Long;
use MHA::DBHelper;

my (
  $command,        $ssh_user,         $orig_master_host,
  $orig_master_ip, $orig_master_port, $new_master_host,
  $new_master_ip,  $new_master_port,  $new_master_user,
  $new_master_password
);
my $vip = '192.168.26.201/24';
my $key = '1';
my $ssh_start_vip = "/sbin/ifconfig ens32:$key $vip";
my $ssh_stop_vip = "/sbin/ifconfig ens32:$key down";

GetOptions(
  'command=s'             => \$command,
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$

```

```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$ifconfig  ens32:1
ens32:1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.201  netmask 255.255.255.0  broadcast 192.168.26.255
        ether 00:0c:29:b1:97:a1  txqueuelen 1000  (Ethernet)

┌──[root@vms153.liruilongs.github.io]-[~]
└─$

```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible db_node -m shell -a 'wget https://github.com/yoshinorim/mha4mysql-node/releases/download/v0.58/mha4mysql-node-0.58-0.el7.centos.noarch.rpm'
[WARNING]: Consider using the get_url or uri module rather than running 'wget'.  If you need to use command because get_url or uri is insufficient you can add 'warn: false' to this command task or set
'command_warnings=False' in ansible.cfg to get rid of this message.
192.168.26.154 | CHANGED | rc=0 >>
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible db_node -m shell -a 'rpm -ivh mha4mysql-node-0.58-0.el7.centos.noarch.rpm'
[WARNING]: Consider using the yum, dnf or zypper module rather than running 'rpm'.  If you need to use command because yum, dnf or zypper is insufficient you can add 'warn: false' to this command task or set
'command_warnings=False' in ansible.cfg to get rid of this message.
192.168.26.155 | CHANGED | rc=0 >>
准备中...                          ########################################
正在升级/安装...
mha4mysql-node-0.58-0.el7.centos      ########################################
192.168.26.153 | CHANGED | rc=0 >>
准备中...                          ########################################
正在升级/安装...
mha4mysql-node-0.58-0.el7.centos      ########################################
192.168.26.154 | CHANGED | rc=0 >>
准备中...                          ########################################
正在升级/安装...
mha4mysql-node-0.58-0.el7.centos      ########################################
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ANS
```
```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$mysql -uroot -pliruilong
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 70
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> grant all on *.* to root@"%" identified by "liruilong";
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]>
```
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible db_node -m  shell -a 'mysql -uroot -pliruilong -e"select user,host from mysql.user;"'
192.168.26.153 | CHANGED | rc=0 >>
user    host
liruilong       %
maxscalemon     %
maxscaleroute   %
repluser        %
root    %
root    127.0.0.1
root    ::1
root    localhost
root    vms153.liruilongs.github.io
192.168.26.154 | CHANGED | rc=0 >>
user    host
liruilong       %
maxscalemon     %
maxscaleroute   %
root    %
tom     %
root    127.0.0.1
root    ::1
root    localhost
root    vms154.liruilongs.github.io
192.168.26.155 | CHANGED | rc=0 >>
user    host
liruilong       %
maxscalemon     %
maxscaleroute   %
repluser        %
root    %
root    127.0.0.1
root    ::1
root    localhost
root    vms153.liruilongs.github.io
```

```bash
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$mysql -h192.168.26.153 -uroot -pliruilong -e'show databases;'
+--------------------+
| Database           |
+--------------------+
| information_schema |
| liruilong_db       |
| mysql              |
| performance_schema |
| test               |
+--------------------+
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$mysql -h192.168.26.154 -uroot -pliruilong -e'show databases;'
+--------------------+
| Database           |
+--------------------+
| information_schema |
| liruilong_db       |
| mysql              |
| performance_schema |
| test               |
+--------------------+
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$mysql -h192.168.26.155 -uroot -pliruilong -e'show databases;'
+--------------------+
| Database           |
+--------------------+
| information_schema |
| liruilong_db       |
| mysql              |
| performance_schema |
| test               |
+--------------------+
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$
```

```bash
┌──[root@vms154.liruilongs.github.io]-[~]
└─$mysql -uroot -pliruilong
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 64
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> grant replication slave on *.* to repluser@"%" identified by 'repluser'
    -> ;
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]>
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible db_node -m  shell -a 'mysql -uroot -pliruilong -e"select user,host from mysql.user;"'
192.168.26.153 | CHANGED | rc=0 >>
user    host
liruilong       %
maxscalemon     %
maxscaleroute   %
repluser        %
root    %
root    127.0.0.1
root    ::1
root    localhost
root    vms153.liruilongs.github.io
192.168.26.154 | CHANGED | rc=0 >>
user    host
liruilong       %
maxscalemon     %
maxscaleroute   %
repluser        %
root    %
tom     %
root    127.0.0.1
root    ::1
root    localhost
root    vms154.liruilongs.github.io
192.168.26.155 | CHANGED | rc=0 >>
user    host
liruilong       %
maxscalemon     %
maxscaleroute   %
repluser        %
root    %
root    127.0.0.1
root    ::1
root    localhost
root    vms153.liruilongs.github.io
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$cat /etc/my.cnf
[mysqld]

plugin-load=rpl_semi_sync_master=semisync_master.so;rpl_semi_sync_slave=semisync_slave.so
rpl_semi_sync_slave_enabled=1
rpl_semi_sync_master_enabled=1
relay_log_purge=0




server_id=153
log_bin=master153
# 字符集
init_connect='SET collation_connection = utf8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake

datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
# Settings user and group are ignored when systemd is used.
# If you need to run mysqld under a different user or group,
# customize your systemd unit file for mariadb according to the
# instructions in http://fedoraproject.org/wiki/Systemd

[mysqld_safe]
log-error=/var/log/mariadb/mariadb.log
pid-file=/var/run/mariadb/mariadb.pid

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d

┌──[root@vms153.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$systemctl restart mariadb.service
┌──[root@vms153.liruilongs.github.io]-[~]
└─$mysql -uroot -pliruilong -e'show variables like "%semi%";'
+------------------------------------+-------+
| Variable_name                      | Value |
+------------------------------------+-------+
| rpl_semi_sync_master_enabled       | ON    |
| rpl_semi_sync_master_timeout       | 10000 |
| rpl_semi_sync_master_trace_level   | 32    |
| rpl_semi_sync_master_wait_no_slave | ON    |
| rpl_semi_sync_slave_enabled        | ON    |
| rpl_semi_sync_slave_trace_level    | 32    |
+------------------------------------+-------+
┌──[root@vms153.liruilongs.github.io]-[~]
└─$





```

```bash
┌──[root@vms154.liruilongs.github.io]-[~]
└─$vim /etc/my.cnf
┌──[root@vms154.liruilongs.github.io]-[~]
└─$cat /etc/my.cnf
[mysqld]

plugin-load="rpl_semi_sync_master=semisync_master.so;rpl_semi_sync_slave=semisync_slave.so"
rpl_semi_sync_master_enabled=1
rpl_semi_sync_slave_enabled=1
relay_log_purge=0

server_id=154
log_bin=master154
log_slave_updates
# 字符集
init_connect='SET collation_connection = utf8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake

datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
# Settings user and group are ignored when systemd is used.
# If you need to run mysqld under a different user or group,
# customize your systemd unit file for mariadb according to the
# instructions in http://fedoraproject.org/wiki/Systemd

[mysqld_safe]
log-error=/var/log/mariadb/mariadb.log
pid-file=/var/run/mariadb/mariadb.pid

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d
┌──[root@vms154.liruilongs.github.io]-[~]
└─$systemctl restart  mariadb.service
┌──[root@vms154.liruilongs.github.io]-[~]
└─$mysql -uroot -pliruilong -e'show variables like "%semi%";'
+------------------------------------+-------+
| Variable_name                      | Value |
+------------------------------------+-------+
| rpl_semi_sync_master_enabled       | ON    |
| rpl_semi_sync_master_timeout       | 10000 |
| rpl_semi_sync_master_trace_level   | 32    |
| rpl_semi_sync_master_wait_no_slave | ON    |
| rpl_semi_sync_slave_enabled        | ON    |
| rpl_semi_sync_slave_trace_level    | 32    |
+------------------------------------+-------+
┌──[root@vms154.liruilongs.github.io]-[~]
└─$mysql -uroot -pliruilong -e'show slave status\G'
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.26.153
                  Master_User: repluser
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: master153.000009
          Read_Master_Log_Pos: 245
               Relay_Log_File: mariadb-relay-bin.000018
                Relay_Log_Pos: 529
        Relay_Master_Log_File: master153.000009
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB:
          Replicate_Ignore_DB:
           Replicate_Do_Table:
       Replicate_Ignore_Table:
      Replicate_Wild_Do_Table:
  Replicate_Wild_Ignore_Table:
                   Last_Errno: 0
                   Last_Error:
                 Skip_Counter: 0
          Exec_Master_Log_Pos: 245
              Relay_Log_Space: 1953
              Until_Condition: None
               Until_Log_File:
                Until_Log_Pos: 0
           Master_SSL_Allowed: No
           Master_SSL_CA_File:
           Master_SSL_CA_Path:
              Master_SSL_Cert:
            Master_SSL_Cipher:
               Master_SSL_Key:
        Seconds_Behind_Master: 0
Master_SSL_Verify_Server_Cert: No
                Last_IO_Errno: 0
                Last_IO_Error:
               Last_SQL_Errno: 0
               Last_SQL_Error:
  Replicate_Ignore_Server_Ids:
             Master_Server_Id: 153
┌──[root@vms154.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms155.liruilongs.github.io]-[~]
└─$vim /etc/my.cnf
┌──[root@vms155.liruilongs.github.io]-[~]
└─$systemctl restart  mariadb.service
Job for mariadb.service failed because the control process exited with error code. See "systemctl status mariadb.service" and "journalctl -xe" for details.
┌──[root@vms155.liruilongs.github.io]-[~]
└─$vim /etc/my.cnf
┌──[root@vms155.liruilongs.github.io]-[~]
└─$systemctl restart  mariadb.service
┌──[root@vms155.liruilongs.github.io]-[~]
└─$mysql -uroot -pliruilong -e'show variables like "%semi%";'
+------------------------------------+-------+
| Variable_name                      | Value |
+------------------------------------+-------+
| rpl_semi_sync_master_enabled       | ON    |
| rpl_semi_sync_master_timeout       | 10000 |
| rpl_semi_sync_master_trace_level   | 32    |
| rpl_semi_sync_master_wait_no_slave | ON    |
| rpl_semi_sync_slave_enabled        | ON    |
| rpl_semi_sync_slave_trace_level    | 32    |
+------------------------------------+-------+
┌──[root@vms155.liruilongs.github.io]-[~]
└─$mysql -uroot -pliruilong -e'show slave status\G' | head -n 15
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.26.153
                  Master_User: repluser
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: master153.000009
          Read_Master_Log_Pos: 245
               Relay_Log_File: mariadb-relay-bin.000006
                Relay_Log_Pos: 529
        Relay_Master_Log_File: master153.000009
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB:
          Replicate_Ignore_DB:
┌──[root@vms155.liruilongs.github.io]-[~]
└─$
┌──[root@vms155.liruilongs.github.io]-[~]
└─$cat /etc/my.cnf
[mysqld]

plugin-load="rpl_semi_sync_master=semisync_master.so;rpl_semi_sync_slave=semisync_slave.so"
rpl_semi_sync_master_enabled=1
rpl_semi_sync_slave_enabled=1
relay_log_purge=0


server_id=155
log_bin=master155
# 字符集
init_connect='SET collation_connection = utf8_unicode_ci'
init_connect='SET NAMES utf8'
character-set-server=utf8
collation-server=utf8_unicode_ci
skip-character-set-client-handshake

datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0
# Settings user and group are ignored when systemd is used.
# If you need to run mysqld under a different user or group,
# customize your systemd unit file for mariadb according to the
# instructions in http://fedoraproject.org/wiki/Systemd

[mysqld_safe]
log-error=/var/log/mariadb/mariadb.log
pid-file=/var/run/mariadb/mariadb.pid

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d
┌──[root@vms155.liruilongs.github.io]-[~]
└─$
```

```bash 
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$masterha_check_ssh --conf=/etc/mha/app1.cnf
Sat Oct  1 02:15:23 2022 - [warning] Global configuration file /etc/masterha_default.cnf not found. Skipping.
Sat Oct  1 02:15:23 2022 - [info] Reading application default configuration from /etc/mha/app1.cnf..
Sat Oct  1 02:15:23 2022 - [info] Reading server configuration from /etc/mha/app1.cnf..
Sat Oct  1 02:15:23 2022 - [info] Starting SSH connection tests..
Sat Oct  1 02:15:23 2022 - [debug]
Sat Oct  1 02:15:23 2022 - [debug]  Connecting via SSH from root@192.168.26.153(192.168.26.153:22) to root@192.168.26.154(192.168.26.154:22)..
Sat Oct  1 02:15:23 2022 - [debug]   ok.
Sat Oct  1 02:15:23 2022 - [debug]  Connecting via SSH from root@192.168.26.153(192.168.26.153:22) to root@192.168.26.155(192.168.26.155:22)..
Sat Oct  1 02:15:23 2022 - [debug]   ok.
Sat Oct  1 02:15:24 2022 - [debug]
Sat Oct  1 02:15:23 2022 - [debug]  Connecting via SSH from root@192.168.26.154(192.168.26.154:22) to root@192.168.26.153(192.168.26.153:22)..
Sat Oct  1 02:15:24 2022 - [debug]   ok.
Sat Oct  1 02:15:24 2022 - [debug]  Connecting via SSH from root@192.168.26.154(192.168.26.154:22) to root@192.168.26.155(192.168.26.155:22)..
Sat Oct  1 02:15:24 2022 - [debug]   ok.
Sat Oct  1 02:15:24 2022 - [debug]
Sat Oct  1 02:15:24 2022 - [debug]  Connecting via SSH from root@192.168.26.155(192.168.26.155:22) to root@192.168.26.153(192.168.26.153:22)..
Sat Oct  1 02:15:24 2022 - [debug]   ok.
Sat Oct  1 02:15:24 2022 - [debug]  Connecting via SSH from root@192.168.26.155(192.168.26.155:22) to root@192.168.26.154(192.168.26.154:22)..
Sat Oct  1 02:15:24 2022 - [debug]   ok.
Sat Oct  1 02:15:24 2022 - [info] All SSH connection tests passed successfully.
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$
```

```bash
Checking if super_read_only is defined and turned on..DBD::mysql::st execute failed: Unknown system variable 'super_read_only' at /usr/share/perl5/vendor_perl/MHA/SlaveUtil.pm line 245.
原因是在5.5.56-MariaDB版本中虽然从节点设置了read_only选项，但是对于管理员权限的用户这点不生效，所以在MySQL5.6(Mariadb10.1)后新增了super_read_only选项，但当前版本中没有这个选项，所以报错。

解决办法是最后将MHA的版本换成mha4mysql-0.56。

而且版本的mha4mysql-0.57和mha4mysql-0.58都不能解决这个问题，测试之后只有mha4mysql-0.56能够解决。
```

```bash
Sat Oct  1 02:56:57 2022 - [info]   Connecting to root@192.168.26.154(192.168.26.154:22)..
mysqlbinlog: unknown variable 'default-character-set=utf8'
mysqlbinlog version command failed with rc 7:0, please verify PATH, LD_LIBRARY_PATH, and client options
 at /usr/bin/apply_diff_relay_logs line 532.

┌──[root@vms154.liruilongs.github.io]-[~]
└─$sed 's/default-character-set=utf8//g' /etc/my.cnf.d/client.cnf -i
使用mysqlbinlog工具查看二进制日志时会重新读取的mysql的配置文件my.cnf，而不是服务器已经加载进内存的配置文件。
只要修改并保存了my.cnf文件，而不需要重起mysql服务器。
所以这里可以使用此方法：把client选项组中default-character-set=utf8选项屏蔽掉，如
 
#default-character-set=utf8
 
然后运行mysqlbinlog工具，则不会产生任何问题了。当然记得在不在使用mysqlbinlog工具时，把#default-character-set=utf8
选项恢复为default-character-set=utf8；
-----------------------------------
```
```bash
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$mysql --version
mysql  Ver 15.1 Distrib 5.5.68-MariaDB, for Linux (x86_64) using readline 5.1
┌──[root@vms156.liruilongs.github.io]-[~/mha4mysql-manager-0.58]
└─$
```

```bash
┌──[root@vms156.liruilongs.github.io]-[~]
└─$masterha_check_repl --conf=/etc/mha/app1.cnf
Sat Oct  1 20:25:48 2022 - [warning] Global configuration file /etc/masterha_default.cnf not found. Skipping.
Sat Oct  1 20:25:48 2022 - [info] Reading application default configuration from /etc/mha/app1.cnf..
Sat Oct  1 20:25:48 2022 - [info] Reading server configuration from /etc/mha/app1.cnf..
Sat Oct  1 20:25:48 2022 - [info] MHA::MasterMonitor version 0.58.
Sat Oct  1 20:25:49 2022 - [info] GTID failover mode = 0
Sat Oct  1 20:25:49 2022 - [info] Dead Servers:
Sat Oct  1 20:25:49 2022 - [info] Alive Servers:
Sat Oct  1 20:25:49 2022 - [info]   192.168.26.153(192.168.26.153:3306)
Sat Oct  1 20:25:49 2022 - [info]   192.168.26.154(192.168.26.154:3306)
Sat Oct  1 20:25:49 2022 - [info]   192.168.26.155(192.168.26.155:3306)
Sat Oct  1 20:25:49 2022 - [info] Alive Slaves:
Sat Oct  1 20:25:49 2022 - [info]   192.168.26.154(192.168.26.154:3306)  Version=5.5.68-MariaDB (oldest major version between slaves) log-bin:enabled
Sat Oct  1 20:25:49 2022 - [info]     Replicating from 192.168.26.153(192.168.26.153:3306)
Sat Oct  1 20:25:49 2022 - [info]     Primary candidate for the new Master (candidate_master is set)
Sat Oct  1 20:25:49 2022 - [info]   192.168.26.155(192.168.26.155:3306)  Version=5.5.68-MariaDB (oldest major version between slaves) log-bin:enabled
Sat Oct  1 20:25:49 2022 - [info]     Replicating from 192.168.26.153(192.168.26.153:3306)
Sat Oct  1 20:25:49 2022 - [info]     Primary candidate for the new Master (candidate_master is set)
Sat Oct  1 20:25:49 2022 - [info] Current Alive Master: 192.168.26.153(192.168.26.153:3306)
Sat Oct  1 20:25:49 2022 - [info] Checking slave configurations..
Sat Oct  1 20:25:49 2022 - [info]  read_only=1 is not set on slave 192.168.26.154(192.168.26.154:3306).
Sat Oct  1 20:25:49 2022 - [info]  read_only=1 is not set on slave 192.168.26.155(192.168.26.155:3306).
Sat Oct  1 20:25:49 2022 - [info] Checking replication filtering settings..
Sat Oct  1 20:25:49 2022 - [info]  binlog_do_db= , binlog_ignore_db=
Sat Oct  1 20:25:49 2022 - [info]  Replication filtering check ok.
Sat Oct  1 20:25:49 2022 - [info] GTID (with auto-pos) is not supported
Sat Oct  1 20:25:49 2022 - [info] Starting SSH connection tests..
Sat Oct  1 20:25:50 2022 - [info] All SSH connection tests passed successfully.
Sat Oct  1 20:25:50 2022 - [info] Checking MHA Node version..
Sat Oct  1 20:25:50 2022 - [info]  Version check ok.
Sat Oct  1 20:25:50 2022 - [info] Checking SSH publickey authentication settings on the current master..
Sat Oct  1 20:25:51 2022 - [info] HealthCheck: SSH to 192.168.26.153 is reachable.
Sat Oct  1 20:25:51 2022 - [info] Master MHA Node version is 0.58.
Sat Oct  1 20:25:51 2022 - [info] Checking recovery script configurations on 192.168.26.153(192.168.26.153:3306)..
Sat Oct  1 20:25:51 2022 - [info]   Executing command: save_binary_logs --command=test --start_pos=4 --binlog_dir=/var/lib/mysql,/var/log/mysql --output_file=/var/tmp/save_binary_logs_test --manager_version=0.58 --start_file=master153.000009
Sat Oct  1 20:25:51 2022 - [info]   Connecting to root@192.168.26.153(192.168.26.153:22)..
  Creating /var/tmp if not exists..    ok.
  Checking output directory is accessible or not..
   ok.
  Binlog found at /var/lib/mysql, up to master153.000009
Sat Oct  1 20:25:51 2022 - [info] Binlog setting check done.
Sat Oct  1 20:25:51 2022 - [info] Checking SSH publickey authentication and checking recovery script configurations on all alive slave servers..
Sat Oct  1 20:25:51 2022 - [info]   Executing command : apply_diff_relay_logs --command=test --slave_user='root' --slave_host=192.168.26.154 --slave_ip=192.168.26.154 --slave_port=3306 --workdir=/var/tmp --target_version=5.5.68-MariaDB --manager_version=0.58 --relay_log_info=/var/lib/mysql/relay-log.info  --relay_dir=/var/lib/mysql/  --slave_pass=xxx
Sat Oct  1 20:25:51 2022 - [info]   Connecting to root@192.168.26.154(192.168.26.154:22)..
  Checking slave recovery environment settings..
    Opening /var/lib/mysql/relay-log.info ... ok.
    Relay log found at /var/lib/mysql, up to mariadb-relay-bin.000020
    Temporary relay log file is /var/lib/mysql/mariadb-relay-bin.000020
    Checking if super_read_only is defined and turned on.. not present or turned off, ignoring.
    Testing mysql connection and privileges..
 done.
    Testing mysqlbinlog output.. done.
    Cleaning up test file(s).. done.
Sat Oct  1 20:25:51 2022 - [info]   Executing command : apply_diff_relay_logs --command=test --slave_user='root' --slave_host=192.168.26.155 --slave_ip=192.168.26.155 --slave_port=3306 --workdir=/var/tmp --target_version=5.5.68-MariaDB --manager_version=0.58 --relay_log_info=/var/lib/mysql/relay-log.info  --relay_dir=/var/lib/mysql/  --slave_pass=xxx
Sat Oct  1 20:25:51 2022 - [info]   Connecting to root@192.168.26.155(192.168.26.155:22)..
  Checking slave recovery environment settings..
    Opening /var/lib/mysql/relay-log.info ... ok.
    Relay log found at /var/lib/mysql, up to mariadb-relay-bin.000006
    Temporary relay log file is /var/lib/mysql/mariadb-relay-bin.000006
    Checking if super_read_only is defined and turned on.. not present or turned off, ignoring.
    Testing mysql connection and privileges..
 done.
    Testing mysqlbinlog output.. done.
    Cleaning up test file(s).. done.
Sat Oct  1 20:25:51 2022 - [info] Slaves settings check done.
Sat Oct  1 20:25:51 2022 - [info]
192.168.26.153(192.168.26.153:3306) (current master)
 +--192.168.26.154(192.168.26.154:3306)
 +--192.168.26.155(192.168.26.155:3306)

Sat Oct  1 20:25:51 2022 - [info] Checking replication health on 192.168.26.154..
Sat Oct  1 20:25:51 2022 - [info]  ok.
Sat Oct  1 20:25:51 2022 - [info] Checking replication health on 192.168.26.155..
Sat Oct  1 20:25:51 2022 - [info]  ok.
Sat Oct  1 20:25:51 2022 - [info] Checking master_ip_failover_script status:
Sat Oct  1 20:25:51 2022 - [info]   /etc/mha/master_ip_failover --command=status --ssh_user=root --orig_master_host=192.168.26.153 --orig_master_ip=192.168.26.153 --orig_master_port=3306
Sat Oct  1 20:25:51 2022 - [info]  OK.
Sat Oct  1 20:25:51 2022 - [warning] shutdown_script is not defined.
Sat Oct  1 20:25:51 2022 - [info] Got exit code 0 (Not master dead).

MySQL Replication Health is OK.
┌──[root@vms156.liruilongs.github.io]-[~]
└─$
```


启动管理服务

```bash
┌──[root@vms156.liruilongs.github.io]-[~]
└─$masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf  --ignore_last_failover
Sat Oct  1 20:22:58 2022 - [warning] Global configuration file /etc/masterha_default.cnf not found. Skipping.
Sat Oct  1 20:22:58 2022 - [info] Reading application default configuration from /etc/mha/app1.cnf..
Sat Oct  1 20:22:58 2022 - [info] Reading server configuration from /etc/mha/app1.cnf..
```

查看服务状态
```bash
┌──[root@vms156.liruilongs.github.io]-[~]
└─$masterha_check_status --conf=/etc/mha/app1.cnf
app1 (pid:5972) is running(0:PING_OK), master:192.168.26.153
┌──[root@vms156.liruilongs.github.io]-[~]
└─$ls /etc/mha/
app1.cnf  master_ip_failover
┌──[root@vms156.liruilongs.github.io]-[~]
└─$ls /var/log/ma
maillog   mariadb/  masterha/
┌──[root@vms156.liruilongs.github.io]-[~]
└─$ls /var/log/masterha/mha/
app1.master_status.health  manager.log
┌──[root@vms156.liruilongs.github.io]-[~]
└─$tail -n 5 /var/log/masterha/mha/manager.log
Sat Oct  1 20:23:01 2022 - [warning] shutdown_script is not defined.
Sat Oct  1 20:23:01 2022 - [info] Set master ping interval 3 seconds.
Sat Oct  1 20:23:01 2022 - [warning] secondary_check_script is not defined. It is highly recommended setting it to check master reachability from two or more routes.
Sat Oct  1 20:23:01 2022 - [info] Starting ping health check on 192.168.26.153(192.168.26.153:3306)..
Sat Oct  1 20:23:01 2022 - [info] Ping(SELECT) succeeded, waiting until MySQL doesn't respond..
┌──[root@vms156.liruilongs.github.io]-[~]
└─$
```


```bash
┌──[root@vms156.liruilongs.github.io]-[~]
└─$tail -n 5 /var/log/masterha/mha/app1.master_status.health
5972    0:PING_OK       master:192.168.26.153┌──[root@vms156.liruilongs.github.io]-[~]
└─$
┌──[root@vms156.liruilongs.github.io]-[~]
└─$masterha_stop  --conf=/etc/mha/app1.cnf
Stopped app1 successfully.
┌──[root@vms156.liruilongs.github.io]-[~]
└─$
```


```bash
┌──[root@vms156.liruilongs.github.io]-[~]
└─$masterha_check_status --conf=/etc/mha/app1.cnf
app1 is stopped(2:NOT_RUNNING).
┌──[root@vms156.liruilongs.github.io]-[~]
└─$nohup masterha_manager --conf=/etc/mha/app1.cnf --remove_dead_master_conf  --ignore_last_failover &
[1] 7780
┌──[root@vms156.liruilongs.github.io]-[~]
└─$nohup: 忽略输入并把输出追加到"nohup.out"

┌──[root@vms156.liruilongs.github.io]-[~]
└─$

┌──[root@vms156.liruilongs.github.io]-[~]
└─$masterha_check_status --conf=/etc/mha/app1.cnf
app1 (pid:7720) is running(0:PING_OK), master:192.168.26.153
┌──[root@vms156.liruilongs.github.io]-[~]
└─$
```


```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$mysql -h192.168.26.153 -uroot -pliruilong -e'create database mha;'
┌──[root@vms152.liruilongs.github.io]-[~]
└─$mysql -h192.168.26.153 -uroot -pliruilong -e'create table mha.user(id int(8));'
┌──[root@vms152.liruilongs.github.io]-[~]
```


```bash
MariaDB [(none)]> grant select,insert on mha.* to liruilong@"%" identified by "liruilong";
Query OK, 0 rows affected (0.00 sec)

MariaDB [(none)]>
```

```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$mysql -uliruilong -pliruilong
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 100
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> show grants;
+----------------------------------------------------------------------------------------------------------+
| Grants for liruilong@%                                                                                   |
+----------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'liruilong'@'%' IDENTIFIED BY PASSWORD '*73CA7DD1B0BD11DCA665AB9C635C2188533331B3' |
| GRANT SELECT, INSERT ON `mha`.* TO 'liruilong'@'%'                                                       |
| GRANT ALL PRIVILEGES ON `liruilong_db`.* TO 'liruilong'@'%'                                              |
+----------------------------------------------------------------------------------------------------------+
3 rows in set (0.00 sec)

```


```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$mysql -h192.168.26.153 -uliruilong -pliruilong
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 101
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> install into mha.user values(1001);
ERROR 1064 (42000): You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'into mha.user values(1001)' at line 1
MariaDB [(none)]> insert into mha.user values(1001);
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> exit
Bye
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible db_node -m  shell -a 'mysql -uliruilong -pliruilong -e"select * from mha.user"'
192.168.26.154 | CHANGED | rc=0 >>
id
1001
192.168.26.153 | CHANGED | rc=0 >>
id
1001
192.168.26.155 | CHANGED | rc=0 >>
id
1001
┌──[root@vms152.liruilongs.github.io]-[~]
└─$

```


测试高可用
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$mysql -h192.168.26.201 -uliruilong -pliruilong
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 103
Server version: 5.5.68-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> select * from mha.user
    -> ;
+------+
| id   |
+------+
| 1001 |
+------+
1 row in set (0.00 sec)

MariaDB [(none)]>
```



```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible db_node -m  shell -a 'mysql -uroot -pliruilong -e"show slave status\G" | head -n 15'
192.168.26.154 | CHANGED | rc=0 >>
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.26.153
                  Master_User: repluser
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: master153.000010
          Read_Master_Log_Pos: 245
               Relay_Log_File: mariadb-relay-bin.000002
                Relay_Log_Pos: 529
        Relay_Master_Log_File: master153.000010
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB:
          Replicate_Ignore_DB:
192.168.26.153 | CHANGED | rc=0 >>

192.168.26.155 | CHANGED | rc=0 >>
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.26.153
                  Master_User: repluser
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: master153.000010
          Read_Master_Log_Pos: 245
               Relay_Log_File: mariadb-relay-bin.000002
                Relay_Log_Pos: 529
        Relay_Master_Log_File: master153.000010
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB:
          Replicate_Ignore_DB:
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible 192.168.26.153 -m shell -a 'systemctl stop mariadb.service'
192.168.26.153 | CHANGED | rc=0 >>

```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible db_node -m  shell -a 'mysql -uroot -pliruilong -e"show slave status\G" | head -n 15'
192.168.26.155 | CHANGED | rc=0 >>
*************************** 1. row ***************************
               Slave_IO_State: Waiting for master to send event
                  Master_Host: 192.168.26.154
                  Master_User: repluser
                  Master_Port: 3306
                Connect_Retry: 60
              Master_Log_File: master154.000004
          Read_Master_Log_Pos: 391
               Relay_Log_File: mariadb-relay-bin.000002
                Relay_Log_Pos: 529
        Relay_Master_Log_File: master154.000004
             Slave_IO_Running: Yes
            Slave_SQL_Running: Yes
              Replicate_Do_DB:
          Replicate_Ignore_DB:
192.168.26.154 | CHANGED | rc=0 >>

192.168.26.153 | CHANGED | rc=0 >>
ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/var/lib/mysql/mysql.sock' (2)
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```



```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible db_node -m  shell -a 'mysql -uroot -pliruilong -e"show master status"'
192.168.26.155 | CHANGED | rc=0 >>
File    Position        Binlog_Do_DB    Binlog_Ignore_DB
master155.000007        245
192.168.26.154 | CHANGED | rc=0 >>
File    Position        Binlog_Do_DB    Binlog_Ignore_DB
master154.000004        391
192.168.26.153 | FAILED | rc=1 >>
ERROR 2002 (HY000): Can't connect to local MySQL server through socket '/var/lib/mysql/mysql.sock' (2)non-zero return code
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```



































































需要的安装包
https://drive.google.com/drive/folders/0B1lu97m8-haWeHdGWXp0YVVUSlk?resourcekey=0-0gVDCHGv91pv0V95b3e0vQ
https://github.com/yoshinorim/mha4mysql-node/releases/
https://github.com/yoshinorim/mha4mysql-node/releases/tag/v0.58


https://blog.csdn.net/zhuoweichen1/article/details/126599763