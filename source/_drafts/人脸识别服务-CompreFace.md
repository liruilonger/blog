---
title: 人脸识别服务 CompreFace
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-05-05 10:38:26/人脸识别服务 CompreFace.html'
mathJax: false
date: 2023-05-05 18:38:26
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


CompreFace是一种免费的开源人脸识别服务，无需机器学习技能即可轻松集成到任何系统中。 CompreFace 提供用于人脸识别、人脸验证、人脸检测、地标检测、面具检测、头部姿势检测、年龄和性别识别的 REST API，并且可以通过 docker 轻松部署。


系统提供REST API，用于人脸识别、人脸验证、人脸检测、地标检测、面具检测、头部姿势检测、年龄和性别识别。 该解决方案还具有角色管理系统，可让您轻松控制谁有权访问您的人脸识别服务。

CompreFace作为docker-compose配置提供，并支持在CPU和GPU上运行的不同模型。 我们的解决方案基于最先进的方法和库，如FaceNet和InsightFace。

优势：

+ 同时支持 CPU 和 GPU，易于扩展
+ 开源自托管，为数据安全提供额外保障
+ 可以部署在云中或本地
+ 无需机器学习专业知识即可设置和使用
+ 使用FaceNet和InsightFace库，它们使用最先进的人脸识别方法
+ 只需一个 docker 命令即可快速启动
:



功能
人脸检测
人脸识别
人脸验证
地标检测插件
年龄识别插件
性别识别插件
口罩检测插件
头部姿势插件


```bash
下载 CompreFace_1.1.0.zip存档或运行：
wget -q -O tmp.zip 'https://github.com/exadel-inc/CompreFace/releases/download/v1.1.0/CompreFace_1.1.0.zip' && unzip tmp.zip && rm tmp.zip
要启动 CompreFace 运行：
docker-compose up -d
在浏览器中打开：http://localhost:8000/login
要停止正常运行：
docker-compose stop
要重新开始运行：
docker-compose start
```

```bash
liruilonger@cloudshell:~$ wget -q -O tmp.zip 'https://github.com/exadel-inc/CompreFace/releases/download/v1.1.0/CompreFace_1.1.0.zip' && unzip tmp.zip && rm tmp.zip
Archive:  tmp.zip
  inflating: .env                    
   creating: custom-builds/
   creating: custom-builds/FaceNet/
  inflating: custom-builds/FaceNet/.env  
  inflating: custom-builds/FaceNet/docker-compose.yml  
   creating: custom-builds/Mobilenet/
   creating: custom-builds/Mobilenet-gpu/
  inflating: custom-builds/Mobilenet-gpu/.env  
  inflating: custom-builds/Mobilenet-gpu/docker-compose.yml  
  inflating: custom-builds/Mobilenet/.env  
  inflating: custom-builds/Mobilenet/docker-compose.yml  
  inflating: custom-builds/README.md  
   creating: custom-builds/Single-Docker-File/
  inflating: custom-builds/Single-Docker-File/Dockerfile  
  inflating: custom-builds/Single-Docker-File/docker-compose.yml  
  inflating: custom-builds/Single-Docker-File/nginx.conf  
  inflating: custom-builds/Single-Docker-File/postgresql.conf  
  inflating: custom-builds/Single-Docker-File/startup.sh  
  inflating: custom-builds/Single-Docker-File/supervisord.conf  
   creating: custom-builds/SubCenter-ArcFace-r100/
   creating: custom-builds/SubCenter-ArcFace-r100-gpu/
  inflating: custom-builds/SubCenter-ArcFace-r100-gpu/.env  
  inflating: custom-builds/SubCenter-ArcFace-r100-gpu/docker-compose.yml  
  inflating: custom-builds/SubCenter-ArcFace-r100/.env  
  inflating: custom-builds/SubCenter-ArcFace-r100/docker-compose.yml  
  inflating: docker-compose.yml      
liruilonger@cloudshell:~$ 
```

```bash
liruilonger@cloudshell:~$ docker-compose up -d
Creating network "liruilonger_default" with the default driver
Creating volume "liruilonger_postgres-data" with default driver
Pulling compreface-postgres-db (exadel/compreface-postgres-db:1.1.0)...
1.1.0: Pulling from exadel/compreface-postgres-db
80369df48736: Pull complete
ffc097878b09: Pull complete
3106d02490d4: Pull complete
88d1fc513b8f: Pull complete
26bf23ba2b27: Pull complete
09ead80f0070: Pull complete
8006c235917f: Pull complete
Digest: sha256:258933c9d362f93da8dbebc0c483813c895e8895530c5070c3140886daed8c5a
Status: Downloaded newer image for exadel/compreface-postgres-db:1.1.0
Pulling compreface-api (exadel/compreface-api:1.1.0)...
1.1.0: Pulling from exadel/compreface-api
bb79b6b2107f: Pull complete
00028440d132: Pull complete
ebd07266fb43: Pull complete
4194f179c57f: Pull complete
2efe8efba2ac: Pull complete
Digest: sha256:4c0e3f11d7713473cf1ea399314b1d898ba7f18f10ea9770d7b4d0af4b7829fb
Status: Downloaded newer image for exadel/compreface-api:1.1.0
Pulling compreface-admin (exadel/compreface-admin:1.1.0)...
1.1.0: Pulling from exadel/compreface-admin
bb79b6b2107f: Already exists
00028440d132: Already exists
ebd07266fb43: Already exists
4194f179c57f: Already exists
f1317a55096f: Pull complete
Digest: sha256:07173bbca66d7a539bc2b8b997d8e151e9cd9af93d200ea802ff380a57fc8bc7
Status: Downloaded newer image for exadel/compreface-admin:1.1.0
Pulling compreface-fe (exadel/compreface-fe:1.1.0)...
1.1.0: Pulling from exadel/compreface-fe
a330b6cecb98: Pull complete
5ef80e6f29b5: Pull complete
f699b0db74e3: Pull complete
0f701a34c55e: Pull complete
3229dce7b89c: Pull complete
ddb78cb2d047: Pull complete
2256cc350caf: Pull complete
68eddc611617: Pull complete
cfd9a2a19e30: Pull complete
Digest: sha256:b4565bc22137e49b06715ed49e86a5100431e9c568ab444fa608818d5554023a
Status: Downloaded newer image for exadel/compreface-fe:1.1.0
Pulling compreface-core (exadel/compreface-core:1.1.0)...
1.1.0: Pulling from exadel/compreface-core
3f9582a2cbe7: Pull complete
57d9937f91c0: Pull complete
e7f1bb2e27cc: Pull complete
22765ac5f463: Pull complete
da7b9537ddd2: Pull complete
3bf90946c93c: Pull complete
6af57e473b43: Pull complete
71964d6a237c: Pull complete
cdecbbb29df3: Pull complete
1c945436f7fe: Pull complete
d96e3d5dabb1: Pull complete
f7b64384d90e: Pull complete
19fc5f0f889d: Pull complete
116519c61959: Pull complete
c74cd1a1a565: Pull complete
72845da7e351: Pull complete
858df442d47a: Pull complete
Digest: sha256:01cc278b40ab453a43315f13dc345f16081a835dd98a088de85beefff5a7d6f8
Status: Downloaded newer image for exadel/compreface-core:1.1.0
Creating compreface-core        ... done
Creating compreface-postgres-db ... done
Creating compreface-api         ... done
Creating compreface-admin       ... done
Creating compreface-ui          ... done
liruilonger@cloudshell:~$ docker ps
CONTAINER ID   IMAGE                                 COMMAND                  CREATED          STATUS          PORTS                  NAMES
30573cf54fe9   exadel/compreface-fe:1.1.0            "/docker-entrypoint.…"   12 seconds ago   Up 10 seconds   0.0.0.0:8000->80/tcp   compreface-ui
2e590b439517   exadel/compreface-admin:1.1.0         "sh -c 'java $ADMIN_…"   13 seconds ago   Up 11 seconds                          compreface-admin
852eff593cdf   exadel/compreface-api:1.1.0           "sh -c 'java $API_JA…"   15 seconds ago   Up 13 seconds                          compreface-api
6c8fa08c6d76   exadel/compreface-postgres-db:1.1.0   "docker-entrypoint.s…"   16 seconds ago   Up 14 seconds   5432/tcp               compreface-postgres-db
eedab7ebc77c   exadel/compreface-core:1.1.0          "uwsgi --ini uwsgi.i…"   16 seconds ago   Up 14 seconds   3000/tcp               compreface-core
liruilonger@cloudshell:~$ 
```


```bash
liruilonger@cloudshell:~$ docker ps
CONTAINER ID   IMAGE                                 COMMAND                  CREATED          STATUS          PORTS                  NAMES
30573cf54fe9   exadel/compreface-fe:1.1.0            "/docker-entrypoint.…"   12 seconds ago   Up 10 seconds   0.0.0.0:8000->80/tcp   compreface-ui
2e590b439517   exadel/compreface-admin:1.1.0         "sh -c 'java $ADMIN_…"   13 seconds ago   Up 11 seconds                          compreface-admin
852eff593cdf   exadel/compreface-api:1.1.0           "sh -c 'java $API_JA…"   15 seconds ago   Up 13 seconds                          compreface-api
6c8fa08c6d76   exadel/compreface-postgres-db:1.1.0   "docker-entrypoint.s…"   16 seconds ago   Up 14 seconds   5432/tcp               compreface-postgres-db
eedab7ebc77c   exadel/compreface-core:1.1.0          "uwsgi --ini uwsgi.i…"   16 seconds ago   Up 14 seconds   3000/tcp               compreface-core
```






## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***

https://github.com/exadel-inc/CompreFace
***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
