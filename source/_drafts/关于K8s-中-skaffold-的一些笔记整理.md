---
title: 关于K8s 中 skaffold 的一些笔记整理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-02-04 13:49:41/关于K8s 中 skaffold 的一些笔记整理.html'
mathJax: false
date: 2023-02-04 21:49:41
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


Skaffold 是一个开源的命令行工具，可促进 Kubernetes 应用程序的持续开发。您可以在本地迭代应用程序源代码，然后部署到本地或远程 Kubernetes 集群。Skaffold 处理构建、推送和部署应用程序的工作流程。它还提供构建块并描述 CI/CD 管道的自定义。


## 安装 脚手架

https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/skaffold]
└─$ls
skaffold-linux-amd64
┌──[root@vms100.liruilongs.github.io]-[~/ansible/skaffold]
└─$install skaffold-linux-amd64  /usr/local/bin/skaffold
┌──[root@vms100.liruilongs.github.io]-[~/ansible/skaffold]
└─$skaffold version
v2.1.0
```
### 官方 Demo

```bash
git clone https://github.com/GoogleContainerTools/skaffold

```

###　初始化脚手架

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/skaffold/skaffold/examples/buildpacks-node-tutorial]
└─$skaffold  init
WARN[0000] Skipping Jib: no JVM: [java -version] failed: exec: "java": executable file not found in $PATH  subtask=-1 task=DevLoop
? Choose the builder to build image skaffold-buildpacks-node Buildpacks (package.json)
? Which builders would you like to create kubernetes resources for?
apiVersion: skaffold/v4beta2
kind: Config
metadata:
  name: buildpacks-node-tutorial
build:
  artifacts:
  - image: skaffold-buildpacks-node
    buildpacks:
      builder: gcr.io/buildpacks/builder:v1
manifests:
  rawYaml:
  - k8s/web.yaml

? Do you want to write this configuration to skaffold.yaml? Yes
Configuration skaffold.yaml was written
You can now run [skaffold build] to build the artifacts
or [skaffold run] to build and deploy
or [skaffold dev] to enter development mode, with auto-redeploy
┌──[root@vms100.liruilongs.github.io]-[~/ansible/skaffold/skaffold/examples/buildpacks-node-tutorial]
└─$cat skaffold.yaml
apiVersion: skaffold/v4beta2
kind: Config
metadata:
  name: buildpacks-node-tutorial
build:
  artifacts:
  - image: skaffold-buildpacks-node
    buildpacks:
      builder: gcr.io/buildpacks/builder:v1
manifests:
  rawYaml:
  - k8s/web.yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/skaffold/skaffold/examples/buildpacks-node-tutorial]
└─$
```
### 使用 Skaffold 进行持续开发 

Skaffold 通过在您的代码更改时自动构建和部署应用程序来加速您的开发循环。
```bash
skaffold dev
```
Skaffold 现在正在监视任何文件更改，并将自动重建您的应用程序。让我们看看实际效果。


退出开发模式 Ctrl+C

### 使用 Skaffold 进行持续集成

Skaffold 在持续开发方面大放异彩，同时也可用于持续集成 (CI)。让我们使用 Skaffold 来构建和测试容器镜像。

构建图像

您的 CI 管道可以运行skaffold build以构建、标记容器映像并将其推送到注册表。
```bash
export STATE=$(git rev-list -1 HEAD --abbrev-commit)
skaffold build --file-output build-$STATE.json
```
测试图像
Skaffold 还可以在部署之前针对您的图像运行测试。让我们通过创建一个简单的自定义测试来尝试一下。

打开你的skaffold.yaml并在底部添加以下测试配置，没有任何额外的缩进：

test:
- image: skaffold-buildpacks-node
  custom:
    - command: echo This is a custom test commmand!
现在您已经设置了一个简单的自定义测试，它将运行 bash 命令并等待成功响应。

运行以下命令以使用 Skaffold 执行此测试：

skaffold test --build-artifacts build-$STATE.json


使用 Skaffold 进行持续交付
让我们了解 Skaffold 如何处理持续交付 (CD)。

一步部署
对于简单的部署，运行skaffold deploy：

skaffold deploy -a build-$STATE.json


## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


https://github.com/GoogleContainerTools/skaffold

https://skaffold.dev/docs/install/#standalone-binary
***

© 2018-2023 liruilonger@gmail.com，All rights reserved. 保持署名-非商用-自由转载-相同方式共享(创意共享3.0许可证)

