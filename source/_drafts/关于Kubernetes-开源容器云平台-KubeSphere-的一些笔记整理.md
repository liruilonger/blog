---
title: 关于Kubernetes  开源容器云平台 KubeSphere   的一些笔记整理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-01-14 01:17:20/关于Kubernetes  开源容器云平台 KubeSphere   的一些笔记整理.html'
mathJax: false
date: 2023-01-14 09:17:20
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***
KubeSphere 愿景是打造一个以 Kubernetes 为内核的 云原生分布式操作系统，它的架构可以非常方便地使第三方应用与云原生生态组件进行即插即用(plug-and-play)的集成，支持云原生应用在多云与多集群的统一分发和运维管理。 KubeSphere 也是一个多租户容器平台，提供全栈的 IT 自动化运维的能力，简化企业的 DevOps 工作流。KubeSphere 提供了运维友好的向导式操作界面，帮助企业快速构建一个强大和功能丰富的容器云平台，详情请参阅 平台功能 。





## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知
***

https://github.com/kubesphere/kubesphere/blob/master/README_zh.md

***
<font color=#999aaa>© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-自由转载-相同方式共享(创意共享3.0许可证)</font>

