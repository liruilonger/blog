---
title: 关于Linux性能调优中指标收集的一些笔记
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-09-14 15:25:18/关于Linux性能调优中指标收集的一些笔记.html'
mathJax: false
date: 2022-09-14 23:25:18
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***


单位和单位

转换显示多少是多少?在测量性能时，准确地测量和报告数量是很重要的。


现代公制，也被称为国际单位制(或由其法语缩写SI)，是世界上最常见的测量系统。然而，IT行业对SI的十进制前缀系统的使用和误用多年来一直是混乱和不准确的根源。早期的计算机制造商和用户注意到，二进制数20(= 1024)非常接近十进制数10(= 1000)。因此，人们通常把1千字节的数据称为1024字节的数据，尽管按照“千”前缀的标准理解，1千字节在技术上应该是1000字节的数据。更糟糕的是，十进制前缀和SI前缀的二进制解释之间的差异只会随着表示10的更高次方的前缀的使用而增加。1998年，国际电工委员会(IEC)采用了一种非官方的标准来区分SI的十进制前缀和一组新的二进制前缀。他们构造这些新的二进制前缀的名字时，取十进制前缀的前两个字母(例如，ki-由kilo-衍生而来)，并在后面加上字母-bi-表示“二进制”。国际单位制(SI)前缀是十进制的:


vmstat:虚拟内存统计

在排除内存相关性能问题时，vmstat是最有用的工具之一。

vmstat命令是procps包的一部分，procps包还包括其他有用的性能分析命令，如free和top。如果没有提供参数，vmstat命令将打印自启动以来各种系统统计数据的平均值。

vmstat命令接受两个参数。第一个是延迟，第二个是计数。延迟是一个值，单位为秒。如果没有给出计数，vmstat将持续报告统计数据。默认情况下，内存统计信息在KiB中报告。-S选项允许你将其更改为以kB、MB或MiB为单位，分别使用-S k、-S m和-S m。我们将在后面的课程中讨论这种输出的不同部分可能在适当的单位中有用的方式。下表概述了各个字段的含义，以供参考。



使用awk格式化数据

许多性能分析工具都可以提供基于文本的输出格式。如果有一些实用程序，可以方便地操作和重新格式化文本输出，以实现数据可视化，那就很有用了。

awk是一个非常有用的工具，可以帮助处理和重新格式化数据。Awk是一种模式扫描和处理语言，由gawk包提供。它在处理列式数据(如sar的输出)时特别有用，在本节中，我们将快速介绍一下awk的基础知识，并展示一些使用awk的方法。基本语法awk的通用语法如下:

 基本语法awk的通用语法如下


在awk中最有用的命令是print命令。Print可用于打印某些字段。$1是第一个字段，$2是第二个字段，$3是第三个字段，以此类推。最后一个字段用NF(字段数)表示，$(NF-1)是倒数第二个字段(倒数第二个)，$(NF-2)是倒数第三个字段，以此类推。$0会打印整行代码。
 
awk使用的默认字段分隔符是空格(制表符和/或空格)。要更改字段分隔符，请使用- F选项。例如，您可能非常熟悉/etc/passwd文件。/etc/passwd中使用的字段分隔符是冒号(:)，因此使用awk -F:命令来解析/etc/passwd文件。

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$awk -F: '{print $1}' /etc/passwd
root
bin
daemon
adm
lp
sync
shutdown
halt
mail
operator
games
ftp
nobody
systemd-network
dbus
polkitd
postfix
sshd
chrony
tom
apache
named
rpc
rpcuser
nfsnobody
oprofile
opensips
┌──[root@vms81.liruilongs.github.io]-[~]
└─$awk -F: '{print $1,$NF}' /etc/passwd
root /bin/bash
bin /sbin/nologin
daemon /sbin/nologin
adm /sbin/nologin
lp /sbin/nologin
sync /bin/sync
shutdown /sbin/shutdown
halt /sbin/halt
mail /sbin/nologin
operator /sbin/nologin
games /sbin/nologin
ftp /sbin/nologin
nobody /sbin/nologin
systemd-network /sbin/nologin
dbus /sbin/nologin
polkitd /sbin/nologin
postfix /sbin/nologin
sshd /sbin/nologin
chrony /sbin/nologin
tom /bin/bash
apache /sbin/nologin
named /sbin/nologin
rpc /sbin/nologin
rpcuser /sbin/nologin
nfsnobody /sbin/nologin
oprofile /sbin/nologin
opensips /sbin/nologin
┌──[root@vms81.liruilongs.github.io]-[~]
└─$awk -F: 'BEGIN {printf "liruilong.....\n"}
> {print $1,$(NF-1)}
> END {printf "Done.\n"}' /etc/passwd
liruilong.....
root /root
bin /bin
daemon /sbin
adm /var/adm
lp /var/spool/lpd
sync /sbin
shutdown /sbin
halt /sbin
mail /var/spool/mail
operator /root
games /usr/games
ftp /var/ftp
nobody /
systemd-network /
dbus /
polkitd /
postfix /var/spool/postfix
sshd /var/empty/sshd
chrony /var/lib/chrony
tom /home/tom
apache /usr/share/httpd
named /var/named
rpc /var/lib/rpcbind
rpcuser /var/lib/nfs
nfsnobody /var/lib/nfs
oprofile /var/lib/oprofile
opensips /var/run/opensips
Done.
┌──[root@vms81.liruilongs.github.io]-[~]
└─$awk -F: '/^r/ {print $1,$NF}' /etc/passwd
root /bin/bash
rpc /sbin/nologin
rpcuser /sbin/nologin
┌──[root@vms81.liruilongs.github.io]-[~]
└─$awk -F: '/^r/ {print $1 "*" $NF}' /etc/passwd
root*/bin/bash
rpc*/sbin/nologin
rpcuser*/sbin/nologin
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```



## 博文参考



```bash


```┌──[root@vms152.liruilongs.github.io]-[~]
└─$ifconfig  ens32
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.152  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:fefe:44e2  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:fe:44:e2  txqueuelen 1000  (Ethernet)
        RX packets 79  bytes 8983 (8.7 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 73  bytes 9507 (9.2 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat inventory
[master]
192.168.26.152
[node]
192.168.26.153
192.168.26.154
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible node -m shell -a "yum -y install httpd"
```


```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible-playbook httpd.yaml

PLAY [httpd init] ***********************************************************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************************************************************
ok: [192.168.26.154]
ok: [192.168.26.153]

TASK [httpd content] ********************************************************************************************************************************************************************************************
changed: [192.168.26.153]
changed: [192.168.26.154]

TASK [Restart service httpd, in all cases] **********************************************************************************************************************************************************************
changed: [192.168.26.154]
changed: [192.168.26.153]

TASK [firewall] *************************************************************************************************************************************************************************************************
changed: [192.168.26.153]
changed: [192.168.26.154]

PLAY RECAP ******************************************************************************************************************************************************************************************************
192.168.26.153             : ok=4    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
192.168.26.154             : ok=4    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible node -m shell -a 'hostname;cat /var/www/html/index.html'
192.168.26.154 | CHANGED | rc=0 >>
vms154.liruilongs.github.io
vms154.liruilongs.github.io
192.168.26.153 | CHANGED | rc=0 >>
vms153.liruilongs.github.io
vms153.liruilongs.github.io
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat httpd.yaml
---
- name: httpd init
  hosts: node
  tasks:
    - name: httpd content
      shell: "echo `hostname` > /var/www/html/index.html"
    - name: Restart service httpd, in all cases
      service:
        name: httpd
        state: restarted
    - name: firewall
      shell: firewall-cmd --set-default-zone=trusted
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible node -m yum -a 'name=keepalived state=installed'
............
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible node -m yum -a 'name=keepalived state=installed'
192.168.26.154 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "msg": "",
    "rc": 0,
    "results": [
        "keepalived-1.3.5-19.el7.x86_64 providing keepalived is already installed"
    ]
}
192.168.26.153 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "msg": "",
    "rc": 0,
    "results": [
        "keepalived-1.3.5-19.el7.x86_64 providing keepalived is already installed"
    ]
}
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat keepalived.conf.j2
! Configuration File for keepalived

global_defs {
   router_id LVS_DEVEL #设置路由ID,可以和主机名相同，也可以随便定义
   vrrp_iptables       #手动添加(禁止设置防火墙规则，keepalved每次启动都会自动添加防火墙拒绝所有的规则)
}

vrrp_instance VI_1 {
    state {{ role }}
    interface ens32 #定义网络接口，根据自己虚拟机上的网卡修改
    virtual_router_id 51  #主备服务器VRID号必须保持一致
    priority {{ priority }} #服务器优先级，优先级高则优先获得浮动IP
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        192.168.26.200
    }
}
```

```yaml
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat keepalived.yaml
---
- name: vms153.liruilongs.github.io config
  hosts: 192.168.26.153
  tags:
    - master
  vars:
    role: MASTER
    priority: 100
  tasks:
    - name: copy keeplived config
      template:
        src: keepalived.conf.j2
        dest: /etc/keepalived/keepalived.conf

    - name: restart keeplived
      service:
        name: keepalived
        state: restarted


- name: vms154.liruilongs.github.io config
  hosts: 192.168.26.154
  tags:
    - backup
  vars:
    role: BACKUP
    priority: 50
  tasks:
    - name: copy keepalived config
      template:
        src: keepalived.conf.j2
        dest: /etc/keepalived/keepalived.conf

    - name: restart keepalived
      service:
        name: keepalived
        state: restarted

```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible-playbook keepalived.yaml

PLAY [vms153.liruilongs.github.io config] ***********************************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************************************************************
ok: [192.168.26.153]

TASK [copy keeplived config] ************************************************************************************************************************************************************************************
ok: [192.168.26.153]

TASK [restart keeplived] ****************************************************************************************************************************************************************************************
changed: [192.168.26.153]

PLAY [vms154.liruilongs.github.io config] ***********************************************************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************************************************************************
ok: [192.168.26.154]

TASK [copy keepalived config] ***********************************************************************************************************************************************************************************
ok: [192.168.26.154]

TASK [restart keepalived] ***************************************************************************************************************************************************************************************
changed: [192.168.26.154]

PLAY RECAP ******************************************************************************************************************************************************************************************************
192.168.26.153             : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
192.168.26.154             : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

┌──[root@vms152.liruilongs.github.io]-[~]
└─$ssh root@192.168.26.200 "ifconfig ens32"
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.153  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:feb1:97a1  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:b1:97:a1  txqueuelen 1000  (Ethernet)
        RX packets 15831  bytes 15431643 (14.7 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 7138  bytes 900731 (879.6 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible 192.168.26.153 -m service -a 'name=keepalived state=stopped'
┌──[root@vms152.liruilongs.github.io]-[~/.ssh]
└─$ssh root@192.168.26.200 ifconfig ens32
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.154  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:fe5b:f625  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:5b:f6:25  txqueuelen 1000  (Ethernet)
        RX packets 34043  bytes 43662926 (41.6 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 8089  bytes 891947 (871.0 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

┌──[root@vms152.liruilongs.github.io]-[~/.ssh]
└─$
```


```yaml
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat keepalived.yaml
---
- name: httpd  keepalived init
  hosts: node
  tasks:
    - name: install
      yum:
        name:
          - httpd
          - keepalived
        state: installed

    - name: httpd content
      shell: "echo `hostname` > /var/www/html/index.html"

    - name: Restart service httpd, in all cases
      service:
        name: httpd
        state: restarted

    - name: firewall clons
      shell: firewall-cmd --set-default-zone=trusted


- name: vms153.liruilongs.github.io config
  hosts: 192.168.26.153
  tags:
    - master
  vars:
    role: MASTER
    priority: 100
  tasks:
    - name: copy keeplived config
      template:
        src: keepalived.conf.j2
        dest: /etc/keepalived/keepalived.conf

    - name: restart keeplived
      service:
        name: keepalived
        state: restarted


- name: vms154.liruilongs.github.io config
  hosts: 192.168.26.154
  tags:
    - backup
  vars:
    role: BACKUP
    priority: 50
  tasks:
    - name: copy keepalived config
      template:
        src: keepalived.conf.j2
        dest: /etc/keepalived/keepalived.conf

    - name: restart keepalived
      service:
        name: keepalived
        state: restarted

```
