---
title: 《Java并发编程的艺术》读书笔记
tags:
  - Ansible
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-05-14 03:26:44/《Java并发编程的艺术》读书笔记.html'
mathJax: false
date: 2022-05-14 11:26:44
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***



# 第2章 Java并发机制的底层实现原理

Java代码在编译后会变成Java字节码，字节码被类加载器加载到JVM里，JVM执行字节码，最终需要转化为汇编指令在CPU上执行，Java中所使用的并发机制依赖于`JVM的实现和CPU的指令`。本章我们捋深入底层一起探索下Java并发机制的底层实现原理。


## 2.1 volatile的应用

在多线程并发编程中`synchronized和volatile`都扮演着重要的角色，volatile是轻量级的synchronized，它在多处理器开发中保证了共享变量的“可见性”。

可见性的意思是当一个线程修改一个共享变量时，另外一个线程能读到这个修改的值。如果volatile变量修饰符使用恰当的话，它比synchronized的使用和执行成本更低，因为它不会引起线程上下文的切换和调度。

本文捋深入分析在硬件层面上Intel处理器是如何实现volatile的，通过深入分析帮助我们正确地使用volatile变量。

我们先从了解volatile的定义开始。


1.volatile的定义与实现原理

Java语言规范第3版中对volatile的定义如下：Java编程语言允许线程访问共享变量，为了确保共享变量能被准确和一致地更新，线程应该确保通过排他锁单独获得这个变量。Java语言提供了volatile，在某些情况下比锁要更加方便。如果一个字段被声明成volatile，Java线程内存模型确保所有线程看到这个变量的值是一致的。

在了解volatile实现原理之前，我们先来看下与其实现原理相关的CPU术语与说明。表2-1是CPU术语的定义。

|术语|英文单词|术语描述|
|--|--|--|
|内存屏障|memory barriers|是一组处理器指令，用于实现对内存操作的顺序限制|
|缓冲行|cacheline |缓存中可以分配的最小存储单位。处理器填写缓存线时会加载整个缓存线，需要使用多个主内存读周期|
|原子操作|atomic operations不可中断的一个或一系列操作|
|缓存行填充|cace line fill |当处理器识别到从内存中读取操作数是可缓存的，处理器读取整个缓存行到适当的缓存(LI.2.3的或所有)|
|缓存命中|cache hit|如果进行高速缓存行填充操作的内存位置仍然是下次处理器访问的地址时，处理器从缓存中读取操作数，而不是从内存读取|
|写命中|write hit|当处理器将操作数写回到一个内存缓存的区域时，它首先会检查这个缓存的内存地址是否在缓存行中，如果存在一个有效的缓存行，则处理器将这个操作数写回到缓存，而不是写回到内存，这个操作被称为写命中|
|写缺失|write misses the cache |一个有效的缓存行被写入到不存在的内存区域|


volatile是如何来保证可见性的呢？让我们在X86处理器下通过工具获取JIT编译器生成的汇编指令来查看对volatile进行写操作时，CPU会做什么事情。


```java
 // instance是volatile变量
instance = new Singleton();
// 转变成汇编代码，如下。
0x01a3de1d: movb $0×0,0×1104800(%esi);0x01a3de24: lock addl $0×0,(%esp);
```



有`volatile变量修饰`的`共享变量`进行写操作的时候会多出`第二行汇编代码`，通过查IA-32架构软件开发者手册可知，Lock前缀的指令在多核处理器下会引发了两件事情]。

1. 捋当前处理器缓存行的数据写回到系统内存。
2. 这个写回内存的操作会使在其他CPU里缓存了该内存地址的数据无效。


为了提高处理速度，处理器不直接和内存进行通信，而是先捋系统内存的数据读到内部缓存(L1，L2或其他)后再进行操作，但操作完不知道何时会写到内存。

如果对声明了volatile的变量进行写操作，JVM就会向处理器发送一条Lock前缀的指令，捋这个变量所在缓存行的数据写回到系统内存。但是，就算写回到内存，如果其他处理器缓存的值还是旧的，再执行计算操作就会有问题。所以，在多处理器下，为了保证各个处理器的缓存是一致的，就会实现`缓存一致性协议`，每个处理器通过`嗅探在总线上传播的数据`来检查自己`缓存`的值是不是`过期`了，当处理器发现自己缓存行对应的`内存地址`被修改，就会捋当前处理器的`缓存行设置成无效状态`，当处理器对这个数据进行修改操作的时候，会重新从`系统内存`中把数据读到`处理器缓存`里。

讲到这里，是否会联系到Linux的缓存清理

下面来具体讲解volatile的两条实现原则。

1. `Lock前缀指令会引起处理器缓存回写到内存。`

Lock前缀指令导致在执行指令期间，声言处理器的LOCK#信号。在多处理器环境中，LOCK#信号确保在声言该信号期间，处理器可以独占任何共享内存[2]。但是，在最近的处理器里，`LOCK#信号一般不锁总线，而是锁缓存`，毕竟锁总线开销的比较大。

对于Intel486和Pentium处理器，在锁操作时，总是在总线上声言LOCK#信号。但在P6和目前的处理器中，如果访问的内存区域已经缓存在处理器内部，则不会声言LOCK#信号。相反，它会`锁定这块内存区域的缓存并回写到内存`，并使用`缓存一致性机制`来确保`修改的原子性`，此操作被称为“`缓存锁定`”`，缓存一致性机制会阻止同时修改由两个以上处理器缓存的内存区域数据。

2. `一个处理器的缓存回写到内存会导致其他处理器的缓存无效`。

IA-32处理器和Intel64处理器使用`MESI(修改、独占、共享、无效)控制协议`去维护`内部缓存`和其他`处理器缓存`的一致性。在多核处理器系统中进行操作的时候，IA-32和Intel64处理器能嗅探其他处理器访问系统内存和它们的内部缓存。处理器使用嗅探技术保证它的内部缓存、系统内存和其他处理器的缓存的数据在总线上保持一致。例如，在Pentium和P6family处理器中，如果通过嗅探一个处理器来检测其他处理器打算写内存地址，而这个地址当前处于共享状态，那么正在嗅探的处理器捋使它的缓存行无效，在下次访问相同内存地址时，强制执行缓存行填充。



## 2.2 volatile的使用优化

著名的Java并发编程大师`Doug lea`在JDK7的并发包里新增一个队列集合类`LinkedTransferQueue`，它在使用`volatile变量`时，用一种追加字节的方式来优化队列出队和人队的性能。Linked TransferQueue的代码如下。


## 2.2 synchronized的实现原理与应用

在多线程并发编程中synchronized一直是元老级角色，很多人都会称呼它为重量级锁。但是，随着Java SE 1.6对synchronized进行了各种优化之后，有些情况下它就并不那么重了。

本文详细介绍JavaSE1.6中为了减少获得锁和释放锁带来的性能消耗而引人的偏向锁和轻量级锁，以及锁的存储结构和升级过程。

先来看下利用synchronized实现同步的基础：Java中的每一个对象都可以作为锁。具体表现为以下3种形式。
+ 对于普通同步方法，锁是当前实例对象。
+ 对于静态同步方法，锁是当前类的Class对象。
+ 对于同步方法块，锁是Synchonized括号里配置的对象。

当一个线程试图访问同步代码块时，它首先必须得到锁，退出或抛出异常时必须释放锁。那么锁到底存在哪里呢？锁里面会存储什么信息呢？


从JVM规范中可以看到Synchonized在JVM里的实现原理，JVM基于`进入和退出Monitor对象`来实现`方法同步`和`代码块同步`，但两者的实现细节不一样。

`代码块同步`是使用`monitorenter和monitorexit`指合实现的，而方法同步是使用另外一种方式实现的，细节在JVM规范里并没有详细说明。但是，方法的同步同样可以使用这两个指合来实现。
monitorenter指命是在编译后插入到同步代码块的开始位置，而monitorexit是插入到方法结束处和异常处，JVM要保证每个monitorenter必须有对应的monitorexit与之配对。任何对象都有一个monitor与之关联，当且一个monitor被持有后，它捋处于锁定状态。线程执行到monitorenter指合时，捋会尝试获取对象所对应的monitor的所有权，即尝试获得对象的锁。




# 第5章Java中的锁

本章将介绍Java并发包中与锁相关的API和组件，以及这些API和组件的使用方式和实现细节。内容主要围绕两个方面：

+ 使用：通过示例演示这些组件的使用方法以及详细介绍与锁相关的API；
+ 实现：通过分析源码来剖析实现细节，因为理解实现的细节方能更加得心应手且正确地使用这些组件。

## 5.1Lock接口

锁是用来控制多个线程访问共享资源的方式，一般来说，一个锁能够防止多个线程同时访问共享资源(但是有些锁可以允许多个线程并发的访问共享资源，比如`读写锁`)。

在Lock接口出现之前，Java程序是靠`synchronized`关键字实现`锁功能`的，而`JavaSE5`之后，并发包中新增了`Lock接口`(以及相关实现类)用来实现锁功能

提供了与synchronized关键字类似的同步功能，只是在使用时需要`显式地获取和释放锁`。虽然它缺少了(通过synchronized块或者方法所提供的)`隐式获取释放锁`的便捷性，但是却拥有了`锁获取与释放的可操作性`、`可中断的获取锁以及超时获取锁`等多种synchronized关键字所不具备的同步特性。


使用synchronized关键字将会`隐式地获取锁`，但是它将`锁的获取和释放固化`了，也就是先获取再释放。当然，这种方式简化了同步的管理，可是扩展性没有显示的锁获取和释放来的好。

例如，针对一个场景，手把手进行锁获取和释放，先获得锁A，然后再获取锁B，当锁B获得后，释放锁A同时获取锁C，当锁C获得后，再释放B同时获取锁D，以此类推。这种场景下，synchronized关键字就不那么容易实现了，而使用Lock却容易许多。

`哈，画重点，面试题，synchronized和kock的区别是什么？`



Lock的使用也很简单
```java
package lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class LockUseCase {

    public static void main(String[] args) {

        Lock lock = new ReentrantLock();
        lock.lock();
        try {

            System.out.println("锁");

        } finally {
            lock.unlock();
        }
    }

}


```
在finally块中释放锁，目的是保证在获取到锁之后，最终能够被释放。

不要将获取锁的过程写在try块中，因为如果在获取锁(自定义锁的实现)时发生了异常，异常抛出的同时，也会导致锁无故释放。


Lock接口提供的synchronized关键字所不具备的主要特性

+ 尝试非阻塞地获取锁：当前线程尝试获取锁，如果这一时刻锁没有被其他线程获取到，则成功获取并持有锁
+ 能被中断地获取锁：与syachronized不同，获取到锁的线程能够响应中断，当获取到锁的线程被中断时，中断异常将会被抛出，同时锁会被释放
+ 超时获取锁： 在指定的截止时间之前获取锁，如果截止时间到了仍旧无法获取锁，则返回

`Lock是一个接口`，它定义了`锁获取和释放`的基本操作

|方法名称|描述|
|--|--|
|`void lock()`|获取锁，调用该方法当前线程将会获取锁，当锁获得后，从该方法返回|
|`void lockInterruptibly() throws InterruptedException`|可中断地获取锁，和lock()方法的不同之处在于该方法会响应中断，即在锁的获取中可以中断当前线程|
|`boolean tryLocko()`| 尝试非阻塞的获取锁，调用该方法后立刻返回，如果能够获取则返回true，否则返回false|
|`boolean tryLock(long time，Timeunit unit)throws InterupledException`|超时的获取锁，当前线程在以下3种情况下会返回:`当前线程在超时时间内获得了锁 / 当前线程在超时时间内被中断 / 超时时间结束，返回false` |
|`void unlock()`|释放锁|
|`Condition newCondition()`|获取等待通知组件，该组件和当前的锁绑定，当前线程只有获得了锁，才能调用该组件的wait()方法，而调用后，当前线程将释放锁|

这里先简单介绍一下`Lock接口`的API，随后的章节会详细介绍`同步器AbstractQueuedSynchronizer`以及常用`Lock接口`的实现`ReentrantLock`。`Lock接口`的实现基本都是通过`聚合了一个同步器的子类`来完成`线程访问控制`的。

## 5.2 队列同步器

队列同步器`AbstractQueuedSynchronizer`(以下简称同步器)，是用来构建锁或者其他同步组件的基础框架，它使用了一个`int成员变量`表示`同步状态`，通过内置的`FIFO队列`来完成资源获取线程的排队工作，`并发包`的作者`(Doug Lea)`期望它能够成为实现大部分同步需求的基础。


`同步器`的主要使用方式是`继承`，子类通过继承同步器并实现它的抽象方法来管理同步状态，在抽象方法的实现过程中免不了要对同步状态进行更改，这时就需要使用同步器提供的3个方法
+ `getState()`
+ `setState(int newState)`
+ `compareAndSetState(int expect，int update)`
  
来进行操作，因为它们能够保证状态的改变是安全的。

`子类推荐被定义为自定义同步组件的静态内部类`，同步器自身没有实现任何同步接口，它仅仅是定义了若干同步状态获取和释放的方法来供自定义同步组件使用，同步器既可以支持独占式地获取同步状态，也可以支持共享式地获取同步状态，这样就可以`方便实现`不同类型的同步组件(`ReentrantLock`、`ReentrantReadWriteLock`和`CountDownLatch`等)。

`同步器`是`实现锁(也可以是任意同步组件)`的关键，在锁的实现中聚合同步器，利用同步器实现锁的语义。

可以这样理解二者之间的关系：`锁是面向使用者的，它定义了使用者与锁交互的接口(比如可以允许两个线程并行访问)`，隐藏了实现细节；`同步器面向的是锁的实现者`，它简化了锁的实现方式，屏蔽了同步状态管理、线程的排队、等待与唤醒等底层操作。`锁和同步器很好地隔离了使用者和实现者所需关注的领域。`

## 5.2.1 队列同步器的接口与示例

同步器的设计是基于`模板方法模式`的，也就是说，使用者需要继承同步器并重写指定的方法，随后将同步器组合在自定义同步组件的实现中，并调用同步器提供的模板方法，而这些模板方法将会调用使用者重写的方法。

重写同步器指定的方法时，需要使用同步器提供的如下3个方法来访问或修改同步状态。

+ getState()：获取当前同步状态。

```java
protected final int getState() {
        return state;
    }
```

+ setState(int newState)：设置当前同步状态。

```java
protected final void setState(int newState) {
        state = newState;
    }
```

+ compareAndSetState(int expect，int update)：使用CAS设置当前状态，该方法能够保证状态设置的原子性。

```java
protected final boolean compareAndSetState(int expect, int update) {
        // See below for intrinsics setup to support this
        return unsafe.compareAndSwapInt(this, stateOffset, expect, update);
    }
```

同步器可重写的方法与描述如表所示。

|方法名称|描述|
|--|--|
|`protected boolean tryAcquire(int arg)` |独占式获取同步状态，实现该方法需要查询当前状态并判断同步状态是否符合预期，然后再进行CAS设置同步状态|
|`protected boolean tryRelease(int arg)`|独占式释放同步状态，等待获取同步状态的线程将有机会获取同步状态|
|`protected int tryAcquireShared(int arg)`|共享式获取同步状态，返回大于等于0的值，表示获取成功，反之，获取失败|
|`protected boolean tryReleaseShared(int arg)`|共享式释放同步状态|
|`protected boolean isHeldExclusively()`|当前同步器是否在独占模式下被线程占用，一般该方法表示是否被当前线程所独占|





实现自定义同步组件时，将会调用同步器提供的模板方法，这些(部分)模板方法与描述

同步器提供的模板方法


`void acquire(int arg)`|`独占式`获取同步状态，如果当前线程获取同步状态成功，则由该方法返回，否则，将会进入同步队列等待，该方法将会调用重写的`tryAcquire(arg)`方法

```java
public final void acquire(int arg) {
        if (!tryAcquire(arg) &&
            acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
            selfInterrupt();
    }
```

`void acquireInterruptibly(int arg)`|与`acquire(int arg)`相同，但是该方法响应中断，当前线程未获取到同步状态而进入同步队列中，如果当前线程被中断，则该方法会抛出InteruptedException 并返回

```java
    public final void acquireInterruptibly(int arg)
            throws InterruptedException {
        if (Thread.interrupted())
            throw new InterruptedException();
        if (!tryAcquire(arg))
            doAcquireInterruptibly(arg);
    }
```

`boolean tryAcquireNanos(int arg, long nanosTimeout)`| 在`acquireInterruptibly(int arg)`基础上增加了超时限制，如果当前线程在超时时间内没有获取到同步状态，那么将会返回false，如果获取到了返回true

```java
    public final boolean tryAcquireNanos(int arg, long nanosTimeout)
            throws InterruptedException {
        if (Thread.interrupted())
            throw new InterruptedException();
        return tryAcquire(arg) ||
            doAcquireNanos(arg, nanosTimeout);
    }
```

`void acquireShared`| `共享式`的获取同步状态，如果当前线程未获取到同步状态，将会进入同步队列等待，与独占式获取的主要区别是但同一时刻可以有多个线程获取到同步状态

```java
    public final void acquireShared(int arg) {
        if (tryAcquireShared(arg) < 0)
            doAcquireShared(arg);
    }
```

`void acquireSharedInterruptibly(int arg)`|与acquireShared(int arg)相同，该方法响应中断

```java
    public final void acquireSharedInterruptibly(int arg)
            throws InterruptedException {
        if (Thread.interrupted())
            throw new InterruptedException();
        if (tryAcquireShared(arg) < 0)
            doAcquireSharedInterruptibly(arg);
    }
```

`boolean tryAcquireSharedNanos(int arg, long nanosTimeout)`|在 `acquireSharedInterruptibly(int arg)`基础上增加了超时限制

```java
    public final boolean tryAcquireSharedNanos(int arg, long nanosTimeout)
            throws InterruptedException {
        if (Thread.interrupted())
            throw new InterruptedException();
        return tryAcquireShared(arg) >= 0 ||
            doAcquireSharedNanos(arg, nanosTimeout);
    }
```

`boolean release(int arg)`|独占式的释放同步状态，该方法会在释放同步状态之后，将同步队列中第一个节点包含的线程唤醒

```java
   public final boolean release(int arg) {
        if (tryRelease(arg)) {
            Node h = head;
            if (h != null && h.waitStatus != 0)
                unparkSuccessor(h);
            return true;
        }
        return false;
    }
```

`boolean releaseShared(int arg) `|共享式的释放同步状态

```java
    public final boolean releaseShared(int arg) {
        if (tryReleaseShared(arg)) {
            doReleaseShared();
            return true;
        }
        return false;
    }
```
`Collection<Thread> getQueuedThreads()`|获取等待在同步队列上的线程集合|

```java
    public final Collection<Thread> getQueuedThreads() {
        ArrayList<Thread> list = new ArrayList<Thread>();
        for (Node p = tail; p != null; p = p.prev) {
            Thread t = p.thread;
            if (t != null)
                list.add(t);
        }
        return list;
    }
```

同步器提供的模板方法基本上分为3类：
+ 独占式获取与释放同步状态、
+ 共享式获取与释放同步状态
+ 查询同步队列中的等待线程情况。
  
自定义同步组件将使用同步器提供的模板方法来实现自己的同步语义。

只有掌握了同步器的工作原理能更加深入地理解并发包中其他的并发组件，所以下面通过一个独占锁的示例来深入了解一下同步器的工作原理。

顾名思义，独占锁就是在同一时刻只能有一个线程获取到锁，而其他获取锁的线程只能处于同步队列中等待，只有获取锁的线程释放了锁，后继的线程才能够获取锁，

```java
package lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Mutex implements Lock {

    // static inner class ,user-defined synchro
    private static class Sync extends AbstractQueuedSynchronizer {

        // 独占式获取同步状态 获取锁 当状态为0 的时候，
        @Override
        protected boolean tryAcquire(int arg) {

            if (compareAndSetState(0, 1)) {
                setExclusiveOwnerThread(Thread.currentThread());
                return true;
            }
            return false;
            //return super.tryAcquire(arg);
        }

        // 独占式释放同步状态 释放锁，将状态设置为 0
        @Override
        protected boolean tryRelease(int arg) {

            if (getState() == 0) throw new IllegalMonitorStateException();
            setExclusiveOwnerThread(null);
            setState(0);
            return  true;
            //return super.tryRelease(arg);
        }

        // 当前同步器是否在独占模式下被线程占用  是否 处于占用状态
        @Override
        protected boolean isHeldExclusively() {
            return getState() == 1;
            //return super.isHeldExclusively();
        }
        // 返回一个Condition，每个condition都包含了一个condition队列
        Condition newCondition(){
            return new ConditionObject();
        }
    }


    private final Sync sync = new Sync();

    // 获取锁
    public void lock() {
        // `独占式`获取同步状态
        sync.acquire(1);
    }

    // 可中断地获取锁
    public void lockInterruptibly() throws InterruptedException {
        // 与`acquire(int arg)`相同，但是该方法响应中断
        sync.acquireInterruptibly(1);
    }

    // 尝试非阻塞的获取锁
    public boolean tryLock() {
        // 独占式获取同步状态
        return sync.tryAcquire(1);
    }

    // 超时的获取锁
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return sync.tryAcquireNanos(1,unit.toNanos(time));
    }

    // 释放锁
    public void unlock() {
        // 独占式的释放同步状态
        sync.release(1);
    }

    // 获取等待通知组件
    public Condition newCondition() {
        return sync.newCondition();
    }

    public boolean isLocked(){
        // 当前同步器是否在独占模式下被线程占用
        return sync.isHeldExclusively();
    }

    public  boolean hasQueuedThreads() {
        return sync.hasQueuedThreads();
    }

}

```
上述示例中，`独占锁Mutex`是一个自定义同步组件，它在同一时刻只允许一个线程占有锁。Mutex中定义了一个`静态内部类`，该内部类`继承了同步器`并实现了`独占式获取和释放同步状态`。

在tryAcquire(int acquires)方法中，如果经过CAS设置成功(同步状态设置为1)，则代表获取了同步状态，
在tryRelease(int releases)方法中只是将同步状态重置为0。

用户使用Mutex时并不会直接和内部同步器的实现打交道，而是调用Mutex提供的方法，在Mutex的实现中，以获取锁的lock()方法为例，只需要在方法实现中调用同步器的模板方法acquire(intargs)即可，当前线程调用该方法获取同步状态失败后会被加入到同步队列中等待，这样就大大降低了实现一个可靠自定义同步组件的门槛。
