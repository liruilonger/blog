---
title: 关于Linux下Redis数据持久化备份和恢复的一些笔记
tags:
  - Redis
categories:
  - Redis
toc: true
recommend: 1
keywords: Redis
uniqueId: '2022-11-02 12:07:31/关于Linux下Redis数据持久化备份和恢复的一些笔记.html'
mathJax: false
date: 2022-11-02 20:07:31
thumbnail:
---

**<font color="009688"> 投我以木瓜，报之以琼琚。匪报也，永以为好也！——《卫风·木瓜》**</font>


<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 投我以木瓜，报之以琼琚。匪报也，永以为好也！——《卫风·木瓜》**</font>

***

## 数据持久化

相关文档：[https://redis.io/docs/manual/persistence/](https://redis.io/docs/manual/persistence/)

Redis 常用到的持久化策略：

+ `RDB(Redis 数据库)`：RDB 持久性以指定的时间间隔执行数据集的时间点`快照`。
+ `AOF(Append Only File)`：AOF 持久化记录服务器接收到的每个写操作。然后可以在服务器启动时再次重播这些操作，重建原始数据集。命令使用与 Redis 协议本身相同的格式记录。
+ `无持久性`：您可以完全禁用持久性。这有时在缓存时使用。
+ `RDB + AOF`：您也可以将 AOF 和 RDB 组合在同一个实例中。

这部分官方文档很全，简单看下

### RDB



### AOF

快照不是很耐用。如果运行 Redis 的计算机停止，或者电路出现故障，或者不小心 `kill -9` 您的实例，最新写入 Redis 的数据将会丢失。

`Append Only File`：以追加方式记录写操作的文件，`记录 redis 服务所有写操作`，不断的将新的写操作，追加到文件的末尾，默认没有启用

在配置文件中打开 `AOF`,可以通过命令行的方式设置，然后写入配置文件，或者直接修改配文件，重启实例

#### 命令行的方式修改
```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$redis-cli  -h 192.168.26.153  -p 6350
192.168.26.153:6350> auth liruilong
OK
192.168.26.153:6350> keys *
(empty array)
192.168.26.153:6350> config set appendonly yes  #开启AOF持久化功能
OK
192.168.26.153:6350> CONFIG REWRITE #将临时配置写到配置文件中
OK
192.168.26.153:6350> exit
┌──[root@vms153.liruilongs.github.io]-[~]
└─$
```
从现在开始，每次 Redis 接收到更改数据集的命令(例如SET)时，它都会将其附加到 AOF。当您重新启动 Redis 时，它将重新播放 AOF 以重建状态。

可以进入到 redis的数据目录下，存在了 aof 持久化数据文件

```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$ls /var/lib/redis/6379/
appendonlydir  dump.rdb
```
#### 相关配置

查看配置确认，aof 开启，并且指定文件名和目录
```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$cat /etc/redis/6379.conf | grep appendonly
appendonly yes
# For example, if appendfilename is set to appendonly.aof, the following file
# - appendonly.aof.1.base.rdb as a base file.
# - appendonly.aof.1.incr.aof, appendonly.aof.2.incr.aof as incremental files.
# - appendonly.aof.manifest as a manifest file.
appendfilename "appendonly.aof"  
appenddirname "appendonlydir"
```

AOF文件记录写操作的方式,可以配置 Redis 将 fsync 数据存储在磁盘上的次数

```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$cat /etc/redis/6379.conf | grep appendfsync
# appendfsync always
appendfsync everysec
# appendfsync n
# the same as "appendfsync no". In practical terms, this means that it is
no-appendfsync-on-rewrite no
┌──[root@vms153.liruilongs.github.io]-[~]
└─$
```
+ `appendfsync always`：在redis中每进行一次写操作, 都会将记录存入appendonly.aof中，并将数据持久化到
dump.rdb文件中
+ `appendfsync everysec`: 将写操作记录到appendonly.aof中，每秒执行一次，并将数据持久化到 dump.rdb
文件中
+ `appendfsync no`：只把写操作记录到appendonly.aof中，不会进行RDB持久化(节省系统资源)



# 备份与恢复

## 物理备份 RDB


## 逻辑备份 AOF


备份 appendonly.aof 文件到其他位置，这里备份 目录下的所有文件。
```bash
┌──[root@vms153.liruilongs.github.io]-[/var/lib/redis/6379]
└─$tar -cf redis.bak.tar ./
```
清空恢复节点 154 的对应目录。
```bash
┌──[root@vms153.liruilongs.github.io]-[/var/lib/redis]
└─$ssh 192.168.26.154
Last login: Sun Oct  2 10:54:55 2022 from 192.168.26.1
┌──[root@vms154.liruilongs.github.io]-[~]
└─$ps -C redis-server
   PID TTY          TIME CMD
 10238 ?        00:00:01 redis-server
┌──[root@vms154.liruilongs.github.io]-[~]
└─$/etc/init.d/redis_6379 stop
Stopping ...
Redis stopped
┌──[root@vms154.liruilongs.github.io]-[~]
└─$ps -C redis-server
   PID TTY          TIME CMD
┌──[root@vms154.liruilongs.github.io]-[~]
└─$cd /var/lib/redis/6379/
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$ls
dump.rdb
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$rm -rf *
```
上传文件，并解压。
```bash
┌──[root@vms153.liruilongs.github.io]-[/var/lib/redis]
└─$scp ./redis.bak.tar  root@192.168.26.154:/var/lib/redis/6379/
redis.bak.tar                                                       100%   10KB   9.1MB/s   00:00
┌──[root@vms153.liruilongs.github.io]-[/var/lib/redis]
└─$ssh 192.168.26.154
Last login: Sun Oct  2 11:30:54 2022 from 192.168.26.153
┌──[root@vms154.liruilongs.github.io]-[~]
└─$cd /var/lib/redis/6379/
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$ls
redis.bak.tar
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$tar -xf redis.bak.tar
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$ls
appendonlydir  dump.rdb  redis.bak.tar
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$
```
修改恢复节点的 持久化方式，开启 `AOF`, 如果持久化文件的名字没有改的，那就不需要改。
```bash
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$sed '/^appendonly no$/c appendonly yes' /etc/redis/6379.conf |  grep -i appendonly
appendonly yes
# For example, if appendfilename is set to appendonly.aof, the following file
# - appendonly.aof.1.base.rdb as a base file.
# - appendonly.aof.1.incr.aof, appendonly.aof.2.incr.aof as incremental files.
# - appendonly.aof.manifest as a manifest file.
appendfilename "appendonly.aof"
appenddirname "appendonlydir"
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$sed '/^appendonly no$/c appendonly yes' /etc/redis/6379.conf  -i
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$
```
重启启动redis服务，会重新加载数据目录下的 appendonly.aof
```bash
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$/etc/init.d/redis_6379 start
Starting Redis server...
┌──[root@vms154.liruilongs.github.io]-[/var/lib/redis/6379]
└─$/etc/init.d/redis_6379 status
Redis is running (10381)
```
查看备份数据是否恢复
```bash
┌──[root@vms153.liruilongs.github.io]-[/var/lib/redis]
└─$redis-cli  -h 192.168.26.153  -p 6350
192.168.26.153:6350> auth liruilong
OK
192.168.26.153:6350> get name
"sy"
192.168.26.153:6350>

```

#### 优化配置

日志文件会不断增大，何时触发日志重写？
+ auto-aof-rewrite-min-size 64mb //首次重写触发值
+ auto-aof-rewrite-percentage 100 //到达增长百分比，再次执行重写


```bash
┌──[root@vms153.liruilongs.github.io]-[~]
└─$cat /etc/redis/6379.conf | grep auto-aof-rewrite
auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
┌──[root@vms153.liruilongs.github.io]-[~]
└─$
```


生产环境的使用： RDB和AOF持久化   #需要同时开启，`RDB做持久化，AOF记录redis执行命令`

#### AOF优点与缺点
+ AOF优点:可以灵活设置持久化方式,出现意外宕机时，仅可能丢失1秒的数据
+ AOF缺点:持久化文件的体积通常会大于 RDB 方式,执行 fsync 策略时的速度可能会比 RDB 方式慢

## 博文参考


