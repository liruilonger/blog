---
title: OpenShift与Kubernetes的关系
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-04-26 01:29:28/OpenShift与Kubernetes的关系.html'
mathJax: false
date: 2023-04-26 09:29:28
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***



OpenShift与Kubernetes之间的关系可以阐述为OpenShift因Kubernetes而重生、Kubernetes因OpenShift走向企业级PaaS平台。在正式介绍两者之间的关系前，我们先简单介绍容器的发展史。



容器运行时接口（OCI）标准提出以后，红帽考虑到Kubernetes在企业中的应用，专门为Kubernetes做了一个轻量级的容器运行时，决定重用了runC等基本组件来启动容器，并实现了一个最小的CRI称为CRI-O，CRI-O是CRI的一种标准实现。2017年10月，CRI-O正式发布。

当红帽开发CRI-O时，Docker也在研究CRI标准，这导致了另一个
名为Containerd的运行时的出现（实际上它是从Docker Engine剥离出
来的）。所以从1.12版本开始，Docker会多一层Containerd组件。
Kubernetes将Containerd接入CRI的标准中。即cri-containerd。

从概念上，从PaaS顶层到底层的调用关系是：
Orchestration API→Container Engine API→Kernel API
旧版本的PaaS平台（如OpenShift 3）的调用架构：
Kubernetes Master→Kubelet→Docker
Engine→Containerd→runC→Linux Kernel
红帽OpenShift最新的调用架构：
Kubernetes Master→Kubelet→CRI-O→runC→Linux kernel
详细的调用架构如图2-1所示。



采用CRI-O运行时OpenShift对底层的调用链路更短、
效率更高、稳定性更强。而很重要的一点是CRI-O的运行不依赖于守护
进程，也就是说，即使OpenShift节点上的CRI-O的Systemd进程终止，
所有运行的Pod也不受影响，




### OpenShift发展简史

OpenShift在2011年诞生之初，核心架构采用Gear。2014年
Kubernetes诞生以后，红帽决定对OpenShift进行重构，正是这一决定
彻底改变了OpenShift的命运以及后续PaaS市场的格局


2015年6月红帽推出基于Kubernetes 1.0的OpenShift 3.0，它构
建在三大强有力的支柱上：

·Linux：红帽RHEL作为OpenShift 3基础，保证了其基础架构的
稳定性和可靠性。
·容器：旨在提供⾼效、不可变和标准化的应⽤程序打包，从⽽实
现跨混合云环境的应⽤程序可移植性。
·Kubernetes：提供强⼤的容器编排和管理功能，并成为过去⼗年
中发展最快的开源项⽬之⼀


在2018年1月，红帽收购CoreOS公司。在随后的一年时间里，红帽将CoreOS优秀的功能和组件迅速融合到OpenShif中




### OpenShift对Kubernetes的增强


#### 稳定性的提升

Kubernetes是一个开源项目，面向容器调度；OpenShift是企业级
软件，面向企业PaaS平台。OpenShift除了包含Kubernetes，还包含很
多其他企业级组件，如认证集成、日志监控等。

Kubernetes每年发布4个版本，OpenShift通常使用次新版本的
Kubernetes为最新版本产品的组件，这样保证客户企业级PaaS产品的
稳定性。

### OpenShift实现了一个集群承载多租户和多应用

红帽推
动了Kubernetes RBAC和资源限制Quota的开发，以便多个租户可以共
享一个Kubernetes集群，并可以做资源限制。



#### OpenShift实现了应用程序的简单和安全部署


红帽在OpenShift 3.0（基于Kubernetes
1.0）中开发了DeploymentConfig，以提供参数化部署输入、执行滚动
部署、启用回滚到先前部署状态，以及通过触发器驱动自动部署
（BuildConfig执行完毕触发DeploymentConfig）。红帽OpenShift
DeploymentConfig中许多功能最终将成为Kubernetes Deployments功
能集的一部分，目前OpenShift也完全支持Kubernetes Deployments




Kubernetes通过Pod安全策略来提升安全性。例如我们可以设置Pod以非root用户方式运行。Pod安全策略是Kubernetes中的较新的功
能，这也是受OpenShift SCC（安全上下文约束）的启发。


红帽为Kubernetes开发了CRI-
O，这是一个轻量级、稳定且更安全的容器运行时，基于OCI规范并通
过Kubernetes CRI集成。目前已经在OpenShift中正式发布



#### OpenShift帮助Kubernetes运行更多类型的应用负载

有状态应用在Kubernetes上运行的最基本要求就是数据持久
化。为此，红帽创建了OpenShift存储Scrum团队来专注此领域，并为
上游的Kubernetes,储卷插件做出贡献,推出了
OpenShift Container Storage等创新解决方案。红帽还参与了
Kubernetes容器存储接口（CSI）开源项目，以实现Pod与后端存储的
松耦合。


#### OpenShift实现应用的快速访问

Kubernetes 1.0中没有Ingress的概念，因此将入站流量路由到
Kubernetes Pod和Service是一项非常复杂、需要手工配置的任务。在
OpenShift 3.0中，红帽开发了Router，以提供入口请求的自动负载平
衡。Router是现在Kubernetes Ingress控制器的前身，当然，
OpenShift也支持Kubernetes Ingress。



Kubernetes本身不包括SDN和虚拟网络隔离，而OpenShift包括集
成了OVS(Open vSwitch)的SDN，并实现虚拟网络隔离。此外，红帽还帮助推动了
Kubernetes容器网络接口开发，为Kubernetes集群提供了丰富的第三
方SDN插件生态系统。目前，OpenShift的OVS支持Network Policy模
式，其网络隔离性更强，而且默认使用Network Policy模式，极大提
升了容器的网络安全。


#### OpenShift实现了容器镜像的便捷管理

OpenShift使用ImageStreams管理容器镜像。一个ImageStream是
一类应用镜像的集合，而ImageStreams Tag则指向实际的镜像。



### OpenShift对Kubernetes生态的延伸

#### OpenShift实现了与CI/CD工具的完美集成

目前OpenShift Pipeline默认使用Tekton。Tekton是一个功能强
大且灵活的Kubernetes原生开源框架，用于创建持续集成和交付
（CI/CD）。通过抽象底层实现细节，用户可以跨多云平台和本地系统
进行构建、测试和部署。


虽然OpenShift默认使用Tekton实现Pipeline，但OpenShift会继
续发布并支持与Jenkins的集成。OpenShift与Jenkins的集成，体现在
以下几个方面：

统⼀认证：OpenShift和部署在OpenShift中的Jenkins实现了
SSO。根据OpenShift⽤户在Project中的⾓⾊，可以⾃动映射与之匹配
的Jenkins⾓⾊（view、edit或admin）。

·OpenShift提供四个版本的Jenkins：默认已经提供了⼀键部署
Jenkins的四个模板，

⾃动同步Secret：在同⼀个项⽬中，OpenShift的Secret与
Jenkins凭证⾃动同步，以便Jenkins可以使⽤⽤户名/密码、SSH密钥
或Secret文本，⽽不必在Jenkins中单独创建。
·Pipeline的集成：可以在Jenkins中定义Pipeline来调⽤OpenShift
API，完成⼀些应⽤构建和发布等操作。



### OpenShift实现开发运维一体化

在Kubernetes刚发布时，红帽主要想借助Kubernetes构建企业级开发平台。为了全面提升OpenShift的运维能力，红帽收购CoreOS，将其中优秀的运维工具纳入OpenShift中。CoreOS麾下能大幅提升OpenShift运维能力的组件有：

#### Tectonic：企业级Kubernetes平台。

Tectonic 是一种基于 Kubernetes 的容器管理平台，由 CoreOS 公司开发和维护。它提供了一系列工具和服务，帮助企业用户快速部署、管理和运行 Kubernetes 集群，并提供与底层基础设施（如 OpenStack 和 AWS）的集成。

Tectonic 最大的特点是它提供了一种“自动化一切”的管理方式，通过内置的操作系统自动更新机制和全生命周期管理功能，帮助用户轻松地管理和维护 Kubernetes 集群。此外，Tectonic 还包括了一些高级功能，如流程管道（pipeline）、监控告警、日志聚合和可视化等，使得用户可以更加方便地使用 Kubernetes 平台进行应用程序的开发和部署。

Tectonic 是基于开源软件项目 Kubernetes 构建的，因此它完全兼容于 Kubernetes 的 API 和生态系统，并且具有广泛的社区支持和活跃的开发者社区。同样，它也可以在各种云平台、虚拟化平台和物理服务器上使用，并与现有的 IT 基础架构无缝集成，为企业用户提供了一种简单、灵活和可扩展的容器管理解决方案。


#### Container Linux：适合运⾏容器负载的Linux操作系统CoreOS。

`CoreOS` 是一个轻量级的Linux发行版，专门设计用于构建和运行容器化应用程序。它基于开源技术，如Docker、Kubernetes和etcd，并采用自动更新和修正漏洞的机制。

CoreOS具有以下特点：
+ 轻量级： CoreOS非常小巧，只包含最基本的系统和服务，使其更易于管理和保护。
+ 自动更新： CoreOS可以自动更新和修正漏洞，从而减少人工干预和降低风险。
+ 高度安全： CoreOS采用了许多安全技术，如SELinux、AppArmor和内核命名空间，以提高安全性并降低攻击面。
+ 容器优化： CoreOS专门针对容器化应用程序进行了优化，使其更易于使用Docker和Kubernetes等容器技术。
+ 云原生： CoreOS支持云原生应用程序开发，并提供了一些工具和服务来简化云原生应用程序的部署和管理。

#### Quay：企业级镜像仓库。

Quay是一个基于云的容器注册表，提供了一种安全、可靠和可扩展的方式来存储和分发容器映像。它支持Docker和OCI（Open Container Initiative）格式，并且可以在公共云、私有云或混合云环境中部署。

以下是Quay的主要特点：
+ 安全性： Quay具有高级安全功能，如镜像签名、漏洞扫描、访问控制和审计日志等，以保护你的容器映像免受攻击和滥用。
+ 可靠性： Quay使用分布式架构和冗余存储来确保数据的安全性和可靠性，并且具有内置的高可用性和故障转移机制，以确保最小化停机时间。
+ 可扩展性： Quay可以在公共云、私有云或混合云环境中部署，并且可以轻松地扩展以适应不断增长的需求。
+ 自动构建： Quay使用内置的构建工具来自动构建和发布容器映像，从而加快开发速度并减少人为错误。
+ 集成： Quay可以与其他DevOps工具集成，如CI/CD工具、Kubernetes和Helm等，以简化开发和交付流程。

Quay由CoreOS（现在是Red Hat的一部分）创建并维护，它已经成为许多企业和组织中流行的容器注册表之一。


#### Operator：有状态应⽤的⽣命周期管理⼯具。

Kubernetes Operator是一种自定义控制器，它扩展了Kubernetes API以支持自定义资源。Operator具有自我管理的能力，可以在Kubernetes集群中自动化管理应用程序和服务。

以下是Operator的主要特点：
+ 自定义资源： Operator可以扩展Kubernetes API并引入自定义资源，这些资源包含了应用程序或服务的状态和行为。
+ 自动化操作： Operator可以自我管理，并根据自定义资源的规范执行自动化操作。例如，如果应用程序出现故障，则Operator可以自动重启应用程序或还原先前的状态。
+ 适应性： Operator可以根据应用程序或服务的需求进行调整，并自动适应变化。例如，如果负载增加，则Operator可以自动扩展部署。
+ 可编程性： Operator可以使用开发人员熟悉的编程语言来编写，并使用Kubernetes API和其他工具进行集成。
+ 可扩展性： Operator可以轻松地扩展以适应不断增长的需求，并且可以在多个Kubernetes集群之间共享。

通过使用Operator，开发人员和运维团队可以将应用程序和服务的管理任务自动化，从而提高效率、可靠性和安全性，并减少需要进行手动干预的机会。目前，许多企业和组织都在使用Operator来管理其Kubernetes应用程序和服务。

#### Prometheus：容器监控平台

Prometheus是一个开源的系统和服务监控工具包，由Google创建并维护。它使用基于拉模型（pull-based）的方式监控目标，并存储所有数据以进行查询、分析和可视化。

以下是Prometheus的主要特点：

+ 多维度数据模型： Prometheus支持多维度数据模型，可以对多个维度进行数据聚合和分组查询。
+ 基于拉模型： Prometheus使用基于拉模型的方式监控目标，这意味着它会定期从目标中获取数据。这种方式比推送模型更加灵活且易于管理。
+ 数据存储： Prometheus将所有数据存储为时间序列，并提供了一些内置的函数和操作符，以便对数据进行处理、聚合和转换。
+ 灵活性： Prometheus具有高度灵活性，可以轻松地与其他工具集成，并支持自定义指标和告警规则。
+ 可视化： Prometheus提供了内置的图表和仪表板，以便对数据进行实时可视化。

通过使用Prometheus，您可以对系统和服务进行全面监控，并快速识别和解决问题。Prometheus已成为云原生应用程序堆栈中最受欢迎的监控工具之一，并得到了广泛的应用






## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)

