---
title: 记一次k8s高可用集群断电部分master节点挂掉ETCD数据不一致问题处理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-01-26 02:07:25/记一次k8s高可用集群断电部分master节点挂掉问题处理.html'
mathJax: false
date: 2023-01-26 10:07:25
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get nodes -o wide
NAME                          STATUS   ROLES    AGE   VERSION   INTERNAL-IP      EXTERNAL-IP   OS-IMAGE                KERNEL-VERSION          CONTAINER-RUNTIME
vms100.liruilongs.github.io   Ready    <none>   20m   v1.25.1   192.168.26.100   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms101.liruilongs.github.io   Ready    <none>   20m   v1.25.1   192.168.26.101   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms102.liruilongs.github.io   Ready    <none>   20m   v1.25.1   192.168.26.102   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms103.liruilongs.github.io   Ready    <none>   20m   v1.25.1   192.168.26.103   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms105.liruilongs.github.io   Ready    <none>   20m   v1.25.1   192.168.26.105   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms106.liruilongs.github.io   Ready    <none>   20m   v1.25.1   192.168.26.106   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms107.liruilongs.github.io   Ready    <none>   20m   v1.25.1   192.168.26.107   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms108.liruilongs.github.io   Ready    <none>   20m   v1.25.1   192.168.26.108   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get nodes -o wide
NAME                          STATUS     ROLES           AGE    VERSION   INTERNAL-IP      EXTERNAL-IP   OS-IMAGE                KERNEL-VERSION          CONTAINER-RUNTIME
vms100.liruilongs.github.io   NotReady   control-plane   5d7h   v1.25.1   192.168.26.100   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms101.liruilongs.github.io   NotReady   control-plane   5d5h   v1.25.1   192.168.26.101   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms102.liruilongs.github.io   NotReady   control-plane   5d5h   v1.25.1   192.168.26.102   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms103.liruilongs.github.io   NotReady   <none>          5d5h   v1.25.1   192.168.26.103   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms105.liruilongs.github.io   NotReady   <none>          5d5h   v1.25.1   192.168.26.105   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms106.liruilongs.github.io   NotReady   <none>          5d5h   v1.25.1   192.168.26.106   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms107.liruilongs.github.io   NotReady   <none>          5d5h   v1.25.1   192.168.26.107   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
vms108.liruilongs.github.io   NotReady   <none>          5d5h   v1.25.1   192.168.26.108   <none>        CentOS Linux 7 (Core)   3.10.0-693.el7.x86_64   docker://20.10.22
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```


```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get pods -A  -o wide | grep etcd
kube-system   etcd-vms100.liruilongs.github.io                      1/1     Running            10 (16m ago)   13m   192.168.26.100   vms100.liruilongs.github.io   <none>           <none>
kube-system   etcd-vms101.liruilongs.github.io                      0/1     CrashLoopBackOff   8 (37s ago)    14m   192.168.26.101   vms101.liruilongs.github.io   <none>           <none>
kube-system   etcd-vms102.liruilongs.github.io                      1/1     Running            1 (16m ago)    14m   192.168.26.102   vms102.liruilongs.github.io   <none>           <none>
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get pods -A  -o wide | grep etcd
kube-system             etcd-vms100.liruilongs.github.io                         1/1     Running            9 (4d22h ago)   5d7h    192.168.26.100   vms100.liruilongs.github.io   <none>           <none>
kube-system             etcd-vms101.liruilongs.github.io                         1/1     Running            0               5d5h    192.168.26.101   vms101.liruilongs.github.io   <none>           <none>
kube-system             etcd-vms102.liruilongs.github.io                         1/1     Running            0               5d5h    192.168.26.102   vms102.liruilongs.github.io   <none>           <none>
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get pods -A  -o wide | grep etcd
kube-system   etcd-vms100.liruilongs.github.io                      1/1     Running            10 (16m ago)    13m   192.168.26.100   vms100.liruilongs.github.io   <none>           <none>
kube-system   etcd-vms101.liruilongs.github.io                      0/1     CrashLoopBackOff   8 (41s ago)     14m   192.168.26.101   vms101.liruilongs.github.io   <none>           <none>
kube-system   etcd-vms102.liruilongs.github.io                      1/1     Running            1 (16m ago)     14m   192.168.26.102   vms102.liruilongs.github.io   <none>           <none>
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl options | grep -i -C2 api

    --password='':
        Password for basic authentication to the API server

    --profile='none':
--

    -s, --server='':
        The address and port of the Kubernetes API server

    --skip-headers=false:
--

    --token='':
        Bearer token for authentication to the API server

    --user='':
--

    --username='':
        Username for basic authentication to the API server

    -v, --v=0:
┌──[root@vms100.liruilongs.github.io]-[~]
└─$

```
100
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get pods -A  -o wide  -s https://192.168.26.100:6443 | grep etcd
kube-system   etcd-vms100.liruilongs.github.io                      1/1     Running            10 (38m ago)     35m   192.168.26.100   vms100.liruilongs.github.io   <none>           <none>
kube-system   etcd-vms101.liruilongs.github.io                      0/1     CrashLoopBackOff   12 (2m10s ago)   36m   192.168.26.101   vms101.liruilongs.github.io   <none>           <none>
kube-system   etcd-vms102.liruilongs.github.io                      1/1     Running            1 (38m ago)      36m   192.168.26.102   vms102.liruilongs.github.io   <none>           <none>
```
101
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get pods -A  -o wide  -s https://192.168.26.101:6443 | grep etcd
The connection to the server 192.168.26.101:6443 was refused - did you specify the right host or port?
```
102
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get pods -A  -o wide  -s https://192.168.26.102:6443 | grep etcd
kube-system             etcd-vms100.liruilongs.github.io                         1/1     Running            9 (4d22h ago)   5d7h    192.168.26.100   vms100.liruilongs.github.io   <none>           <none>
kube-system             etcd-vms101.liruilongs.github.io                         1/1     Running            0               5d5h    192.168.26.101   vms101.liruilongs.github.io   <none>           <none>
kube-system             etcd-vms102.liruilongs.github.io                         1/1     Running            0               5d5h    192.168.26.102   vms102.liruilongs.github.io   <none>           <none>
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```
现在我们可以初步确定， 100 正常，但是数据丢失了，101 master 整个节点丢失， 102 master etcd 历史数据存在，但是数据不一致，用的还是旧数据。

当一个成员永久失败时，无论是由于硬件故障还是磁盘损坏，它都会失去对集群的访问权限。如果集群永久丢失超过(N-1)/2 个成员，那么它将灾难性地失败，不可挽回地失去仲裁。一旦仲裁丢失，集群就无法达成共识，因此无法继续接受更新。

```bash

```
查看对应的数据文件，

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$ansible k8s_master  -m shell -a "tree -h /var/lib/etcd/member" -i host.yaml
192.168.26.102 | CHANGED | rc=0 >>
/var/lib/etcd/member
├── [ 246]  snap
│   ├── [9.4K]  0000000000000004-000000000008b750.snap
│   ├── [9.4K]  0000000000000004-000000000008de61.snap
│   ├── [9.4K]  0000000000000004-0000000000090572.snap
│   ├── [9.4K]  0000000000000004-0000000000092c84.snap
│   ├── [ 10K]  0000000000000005-0000000000095395.snap
│   └── [ 29M]  db
└── [ 309]  wal
    ├── [ 61M]  0000000000000005-0000000000056062.wal
    ├── [ 61M]  0000000000000006-00000000000671bf.wal
    ├── [ 62M]  0000000000000007-00000000000783ee.wal
    ├── [ 61M]  0000000000000008-0000000000083a04.wal
    ├── [9.6M]  0000000000000009-00000000000930b6.wal
    ├── [ 61M]  0000000000000009-00000000000930b6.wal.broken
    ├── [ 61M]  0.tmp
    └── [ 61M]  1.tmp

2 directories, 14 files
192.168.26.101 | CHANGED | rc=0 >>
/var/lib/etcd/member
├── [ 246]  snap
│   ├── [9.4K]  0000000000000004-0000000000088e5f.snap
│   ├── [9.4K]  0000000000000004-000000000008b570.snap
│   ├── [9.4K]  0000000000000004-000000000008dc81.snap
│   ├── [9.4K]  0000000000000004-0000000000090392.snap
│   ├── [9.4K]  0000000000000004-0000000000092aa3.snap
│   └── [ 29M]  db
└── [ 244]  wal
    ├── [ 61M]  0000000000000005-0000000000055bad.wal
    ├── [ 61M]  0000000000000006-0000000000066d37.wal
    ├── [ 61M]  0000000000000007-0000000000077f71.wal
    ├── [ 61M]  0000000000000008-00000000000837c3.wal
    ├── [ 61M]  0000000000000009-00000000000929f8.wal
    └── [ 61M]  1.tmp

2 directories, 12 files
192.168.26.100 | CHANGED | rc=0 >>
/var/lib/etcd/member
├── [ 246]  snap
│   ├── [9.4K]  0000000000000004-000000000008b2c9.snap
│   ├── [9.4K]  0000000000000004-000000000008d9da.snap
│   ├── [9.4K]  0000000000000004-00000000000900eb.snap
│   ├── [9.4K]  0000000000000004-00000000000927fc.snap
│   ├── [ 10K]  0000000000000005-0000000000094f0d.snap
│   └── [ 28M]  db
└── [ 257]  wal
    ├── [ 61M]  0000000000000005-00000000000521c0.wal
    ├── [ 61M]  0000000000000006-00000000000635ca.wal
    ├── [ 61M]  0000000000000007-0000000000074b68.wal
    ├── [ 61M]  0000000000000008-00000000000822de.wal
    ├── [ 61M]  0000000000000009-000000000008f9b7.wal
    ├── [ 61M]  0.tmp
    └── [ 61M]  1.tmp

2 directories, 13 files
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$
```

先解决 102 节点 master  数据不一致的问题。 把 master 的所有数据删掉，会重其他节点同步最新的数据。
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.102  -m shell -a 'rm -rf /var/lib/etcd/member/*' -i host.yaml
[WARNING]: Consider using the file module with state=absent rather than running 'rm'.  If you need to use command because file is insufficient you can add 'warn: false' to this command task or set
'command_warnings=False' in ansible.cfg to get rid of this message.
192.168.26.102 | CHANGED | rc=0 >>

```
查看etcd pod 状态，确认数据是否一致
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$kubectl get pods -A  -o wide  -s https://192.168.26.102:6443 | grep etcd
kube-system   etcd-vms100.liruilongs.github.io                      1/1     Running            10 (85m ago)     82m   192.168.26.100   vms100.liruilongs.github.io   <none>           <none>
kube-system   etcd-vms101.liruilongs.github.io                      0/1     CrashLoopBackOff   21 (2m38s ago)   83m   192.168.26.101   vms101.liruilongs.github.io   <none>           <none>
kube-system   etcd-vms102.liruilongs.github.io                      1/1     Running            2 (34s ago)      82m   192.168.26.102   vms102.liruilongs.github.io   <none>           <none>
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$
```

确实没问题之后，我们处理 101 节点的问题


```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$kubectl describe  pods -n kube-system  etcd-vms101.liruilongs.github.io | grep -i -A 5 event
Events:
  Type     Reason   Age                  From     Message
  ----     ------   ----                 ----     -------
  Normal   Pulled   45m (x16 over 101m)  kubelet  Container image "registry.aliyuncs.com/google_containers/etcd:3.5.4-0" already present on machine
  Warning  BackOff  8s (x534 over 101m)  kubelet  Back-off restarting failed container
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$

```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$kubectl logs   -n kube-system  etcd-vms101.liruilongs.github.io
{"level":"info","ts":"2023-01-26T03:35:45.804Z","caller":"etcdmain/etcd.go:73","msg":"Running: ","args":["etcd","--advertise-client-urls=https://192.168.26.101:2379","--cert-file=/etc/kubernetes/pki/etcd/server.crt","--client-cert-auth=true","--data-dir=/var/lib/etcd","--experimental-initial-corrupt-check=true","--experimental-watch-progress-notify-interval=5s","--initial-advertise-peer-urls=https://192.168.26.101:2380","--initial-cluster=vms100.liruilongs.github.io=https://192.168.26.100:2380,vms101.liruilongs.github.io=https://192.168.26.101:2380","--initial-cluster-state=existing","--key-file=/etc/kubernetes/pki/etcd/server.key","--listen-client-urls=https://127.0.0.1:2379,https://192.168.26.101:2379","--listen-metrics-urls=http://127.0.0.1:2381","--listen-peer-urls=https://192.168.26.101:2380","--name=vms101.liruilongs.github.io","--peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt","--peer-client-cert-auth=true","--peer-key-file=/etc/kubernetes/pki/etcd/peer.key","--peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt","--snapshot-count=10000","--trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt"]}
{"level":"info","ts":"2023-01-26T03:35:45.804Z","caller":"etcdmain/etcd.go:116","msg":"server has been already initialized","data-dir":"/var/lib/etcd","dir-type":"member"}
{"level":"info","ts":"2023-01-26T03:35:45.804Z","caller":"embed/etcd.go:131","msg":"configuring peer listeners","listen-peer-urls":["https://192.168.26.101:2380"]}
{"level":"info","ts":"2023-01-26T03:35:45.804Z","caller":"embed/etcd.go:479","msg":"starting with peer TLS","tls-info":"cert = /etc/kubernetes/pki/etcd/peer.crt, key = /etc/kubernetes/pki/etcd/peer.key, client-cert=, client-key=, trusted-ca = /etc/kubernetes/pki/etcd/ca.crt, client-cert-auth = true, crl-file = ","cipher-suites":[]}
{"level":"info","ts":"2023-01-26T03:35:45.805Z","caller":"embed/etcd.go:139","msg":"configuring client listeners","listen-client-urls":["https://127.0.0.1:2379","https://192.168.26.101:2379"]}
{"level":"info","ts":"2023-01-26T03:35:45.805Z","caller":"embed/etcd.go:308","msg":"starting an etcd server","etcd-version":"3.5.4","git-sha":"08407ff76","go-version":"go1.16.15","go-os":"linux","go-arch":"amd64","max-cpu-set":2,"max-cpu-available":2,"member-initialized":true,"name":"vms101.liruilongs.github.io","data-dir":"/var/lib/etcd","wal-dir":"","wal-dir-dedicated":"","member-dir":"/var/lib/etcd/member","force-new-cluster":false,"heartbeat-interval":"100ms","election-timeout":"1s","initial-election-tick-advance":true,"snapshot-count":10000,"snapshot-catchup-entries":5000,"initial-advertise-peer-urls":["https://192.168.26.101:2380"],"listen-peer-urls":["https://192.168.26.101:2380"],"advertise-client-urls":["https://192.168.26.101:2379"],"listen-client-urls":["https://127.0.0.1:2379","https://192.168.26.101:2379"],"listen-metrics-urls":["http://127.0.0.1:2381"],"cors":["*"],"host-whitelist":["*"],"initial-cluster":"","initial-cluster-state":"existing","initial-cluster-token":"","quota-size-bytes":2147483648,"pre-vote":true,"initial-corrupt-check":true,"corrupt-check-time-interval":"0s","auto-compaction-mode":"periodic","auto-compaction-retention":"0s","auto-compaction-interval":"0s","discovery-url":"","discovery-proxy":"","downgrade-check-interval":"5s"}
panic: freepages: failed to get all reachable pages (page 4473: multiple references)

goroutine 139 [running]:
go.etcd.io/bbolt.(*DB).freepages.func2(0xc00008a600)
        /go/pkg/mod/go.etcd.io/bbolt@v1.3.6/db.go:1056 +0xe9
created by go.etcd.io/bbolt.(*DB).freepages
        /go/pkg/mod/go.etcd.io/bbolt@v1.3.6/db.go:1054 +0x1cd
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$
```
etcd

```bash
panic: freepages: failed to get all reachable pages (page 4473: multiple references)
```

kubelet
```bash

```

## 博文参考

***

