---
title: DNS：DNS问题故障排查
tags:
  - DNS
categories:
  - DNS
toc: true
recommend: 1
keywords: DNS
uniqueId: '2023-07-05 14:35:37/DNS：DNS问题故障排查.html'
mathJax: false
date: 2023-07-05 22:35:37
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***



## 排故DNS问题

名称解析遇到问题时，应考虑以下几点:

+ 客户端上  `/etc/hosts` 和 `/etc/resolv.conf`
+ 客户端使用的`缓存名称服务器`的操作
+ 向缓存名称服务器提供数据的`权威名称服务器`的操作
+ 权威名称服务器上的数据。
+ 用于在这些系统之间通信的网络配置


### DNS 解析顺序
DNS是系统最常用的名称解析方法， 但DNS不是系统解析主机名和IP地址的唯一方法。`/etc/nsswitch.conf` 文件中的hosts行控制查找主机名的方式。`hosts: files dns myhostname`


```bash
hosts: files dns  myhostname
```

+ 首先在本地 `/etc/hosts` 文件中查找，
+ 然后执行DNS域名解析查找`/etc/resolv.conf`
+ 最后使用查找`本地配置`的`系统主机名`。


模拟普通应用程序 DNS 解析过程


`glibc-common` 软件包中的 `getent` 命令，会按照`/etc/nsswitch.conf`所指定的主机名称解析顺序执行名称解析。这种解析过程也是大多数应用程序解析的过程。

```bash
root@servera ~]# getent hosts classroom.example.com
172.25.254.254 classroom.example.com classroom
```







## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
