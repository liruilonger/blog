---
title: 'K8s:渐进式入门服务网格 Istio (二)'
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-02-12 14:14:47/"K8s:渐进式入门服务网格 Istio (二)".html'
mathJax: false
date: 2023-02-12 22:14:47
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***




### 虚拟服务

虚拟服务(Virtual Service) 和目标规则(Destination Rule) 是 Istio 流量路由功能的关键拼图。虚拟服务让您配置如何在服务网格内将请求路由到服务，这基于 Istio 和平台提供的基本的连通性和服务发现能力。每个虚拟服务包含一组路由规则，Istio 按顺序评估它们，Istio 将每个给定的请求匹配到虚拟服务指定的实际目标地址。您的网格可以有多个虚拟服务，也可以没有，取决于您的使用场景。





### 网关

使用网关来管理网格的入站和出站流量，可以让您指定要进入或离开网格的流量。网关配置被用于运行在网格边缘的独立 Envoy 代理，而不是与服务工作负载一起运行的 sidecar Envoy 代理。

与 Kubernetes Ingress API 这种控制进入系统流量的其他机制不同，Istio 网关让您充分利用流量路由的强大能力和灵活性。您可以这么做的原因是 Istio 的网关资源可以配置 4-6 层的负载均衡属性，如对外暴露的端口、TLS 设置等。然后，您无需将应用层流量路由 (L7) 添加到同一 API 资源，而是将常规 Istio 虚拟服务绑定到网关。这让您可以像管理网格中其他数据平面的流量一样去管理网关流量。

网关主要用于管理进入的流量，但您也可以配置出口网关。出口网关让您为离开网格的流量配置一个专用的出口节点，这可以限制哪些服务可以或应该访问外部网络，或者启用出口流量安全控制为您的网格添加安全性。您也可以使用网关配置一个纯粹的内部代理。

Istio 提供了一些预先配置好的网关代理部署(istio-ingressgateway 和 istio-egressgateway)供您使用——如果使用我们的演示安装它们都已经部署好了；如果使用默认安装则只部署了入口网关。可以将您自己的网关配置应用到这些部署或配置您自己的网关代理。

```yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: ext-host-gwy
spec:
  selector:
    app: my-gateway-controller
  servers:
  - port:
      number: 443
      name: https
      protocol: HTTPS
    hosts:
    - ext-host.example.com
    tls:
      mode: SIMPLE
      serverCertificate: /tmp/tls.crt
      privateKey: /tmp/tls.key

```


```yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio]
└─$cat istio-1.16.2/samples/bookinfo/networking/bookinfo-gateway.yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: bookinfo-gateway
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: bookinfo
spec:
  hosts:
  - "*"
  gateways:
  - bookinfo-gateway
  http:
  - match:
    - uri:
        exact: /productpage
    - uri:
        prefix: /static
    - uri:
        exact: /login
    - uri:
        exact: /logout
    - uri:
        prefix: /api/v1/products
    route:
    - destination:
        host: productpage
        port:
          number: 9080
```







流量管理是Istio的核心功能之一。如果您使用 Istio 来管理Kubernetes上的微服务，您可以对它们之间的交互方式进行细粒度控制。这也将帮助您定义流量如何流经您的服务网格



### 将流量路由到一个服务版本

Istio 使用虚拟服务来定义路由规则。运行以下命令以应用将所有流量路由到v1每个微服务的虚拟服务：

目标规则



与虚拟服务一样，目标规则也是 Istio 流量路由功能的关键部分。您可以将虚拟服务视为将流量如何路由到给定目标地址，然后使用目标规则来配置该目标的流量。在评估虚拟服务路由规则之后，目标规则将应用于流量的“真实”目标地址。

特别是，您可以使用目标规则来指定命名的服务子集，例如按版本为所有给定服务的实例分组。然后可以在虚拟服务的路由规则中使用这些服务子集来控制到服务不同实例的流量。


应用目的地规则

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$kubectl apply  -f samples/bookinfo/networking/destination-rule-all.yaml
destinationrule.networking.istio.io/productpage created
destinationrule.networking.istio.io/reviews created
destinationrule.networking.istio.io/ratings created
destinationrule.networking.istio.io/details created
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio]
└─$kubectl get destinationrules.networking.istio.io
NAME          HOST          AGE
details       details       11m
productpage   productpage   11m
ratings       ratings       11m
reviews       reviews       11m
```

基于虚拟服务选择的目标子集，Istio 会将请求路由到所有标有所选子集指定版本的微服务 pod。

```yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$cat samples/bookinfo/networking/destination-rule-all.yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: productpage
spec:
  host: productpage
  subsets:
  - name: v1
    labels:
      version: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: reviews
spec:
  host: reviews
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
  - name: v3
    labels:
      version: v3
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: ratings
spec:
  host: ratings
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
  - name: v2-mysql
    labels:
      version: v2-mysql
  - name: v2-mysql-vm
    labels:
      version: v2-mysql-vm
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: details
spec:
  host: details
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
---
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$
```
负载均衡选项
默认情况下，Istio 使用轮询的负载均衡策略，实例池中的每个实例依次获取请求。Istio 同时支持如下的负载均衡模型，可以在 DestinationRule 中为流向某个特定服务或服务子集的流量指定这些模型。

随机：请求以随机的方式转发到池中的实例。
权重：请求根据指定的百分比转发到池中的实例。
最少请求：请求被转发到最少被访问的实例。

```yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: my-destination-rule
spec:
  host: my-svc
  trafficPolicy:
    loadBalancer:
      simple: RANDOM
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
    trafficPolicy:
      loadBalancer:
        simple: ROUND_ROBIN
  - name: v3
    labels:
      version: v3

```


```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$kubectl apply  -f samples/bookinfo/networking/virtual-service-all-v1.yaml
virtualservice.networking.istio.io/productpage created
virtualservice.networking.istio.io/reviews created
virtualservice.networking.istio.io/ratings created
virtualservice.networking.istio.io/details created

┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$

```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio]
└─$kubectl get virtualservice.networking.istio.io
NAME          GATEWAYS               HOSTS             AGE
bookinfo      ["bookinfo-gateway"]   ["*"]             28h
details                              ["details"]       43s
productpage                          ["productpage"]   43s
ratings                              ["ratings"]       43s
reviews                              ["reviews"]       43s
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio]
└─$
```
定义了四个虚拟服务，并且指定了对应的 路由路径和版本
```yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$cat samples/bookinfo/networking/virtual-service-all-v1.yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage
spec:
  hosts:
  - productpage
  http:
  - route:
    - destination:
        host: productpage
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
  - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings
spec:
  hosts:
  - ratings
  http:
  - route:
    - destination:
        host: ratings
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: details
spec:
  hosts:
  - details
  http:
  - route:
    - destination:
        host: details
        subset: v1
---
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$
```

将 Istio 配置为路由到v1Bookinfo 微服务的版本，最重要的是reviews服务版本 1

![在这里插入图片描述](https://img-blog.csdnimg.cn/2203a84101ca46d6a60edd21dcbd407e.png)


```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$kubectl apply   -f samples/bookinfo/networking/virtual-service-reviews-v3.yaml
virtualservice.networking.istio.io/reviews configured
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$
```
```yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$cat samples/bookinfo/networking/virtual-service-reviews-v3.yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v3
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/8224d0523b3a44889b3a8c854a058a7d.png)


### 流量拆分

蓝绿部署，我们慢慢地将流量从旧版本(蓝色)转移到新版本(绿色)。

让我们尝试在微服务的蓝色版本v2和绿色版本之间平均分配流量。

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$kubectl apply -f samples/bookinfo/networking/virtual-service-reviews-v2-v3.yaml
virtualservice.networking.istio.io/reviews configured
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/9fa73cce38324a7bab613326f352db91.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/8224d0523b3a44889b3a8c854a058a7d.png)

```yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$cat samples/bookinfo/networking/virtual-service-reviews-v2-v3.yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v2
      weight: 50
    - destination:
        host: reviews
        subset: v3
      weight: 50
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$
```

### 基于用户身份的路由

假设您不确定您的新微服务能否在生产环境中正常运行。因此，您首先要向业务测试人员推出新服务。一旦业务测试人员感到满意，您就可以将其推广给所有用户。

```yaml
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$cat samples/bookinfo/networking/virtual-service-reviews-test-v2.yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
  - match:
    - headers:
        end-user:
          exact: jason
    route:
    - destination:
        host: reviews
        subset: v2
  - route:
    - destination:
        host: reviews
        subset: v1
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$
```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/istio/istio-1.16.2]
└─$kubectl apply -f samples/bookinfo/networking/virtual-service-reviews-test-v2.yaml

virtualservice.networking.istio.io/reviews configured
```

刷新页面，你应该看到所有请求都路由到v1微服务，因为你还没有登录：

![在这里插入图片描述](https://img-blog.csdnimg.cn/54105c6b2539448899b08975085a8dee.png)


![在这里插入图片描述](https://img-blog.csdnimg.cn/5f88ca917eb9458fbc1ef16ecea3525a.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/0e537d516c5e43b2ae966ec838797608.png)





![在这里插入图片描述](https://img-blog.csdnimg.cn/c94ba5a45bfb4626ba446f8ee0c7bc8a.png)






## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
