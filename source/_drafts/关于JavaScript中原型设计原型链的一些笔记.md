---
title: 关于JavaScript中原型设计原型链的一些笔记
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-09-04 04:03:41/关于JavaScript中原型设计原型链的一些笔记.html'
mathJax: false
date: 2022-09-04 12:03:41
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

也就是说，可以多次调用 Object.defineProperty() 方法修改同一个属性，但在把 configurable
特性设置为 false 之后就会有限制了。
在调用 Object.defineProperty() 方法时，如果不指定， configurable 、 enumerable 和
writable 特性的默认值都是 false 。多数情况下，可能都没有必要利用 Object.defineProperty()
方法提供的这些高级功能。不过，理解这些概念对理解 JavaScript 对象却非常有用。

2. 访问器属性
访问器属性不包含数据值；它们包含一对儿 getter 和 setter 函数(不过，这两个函数都不是必需的)。
在读取访问器属性时，会调用 getter 函数，这个函数负责返回有效的值；在写入访问器属性时，会调用
setter 函数并传入新值，这个函数负责决定如何处理数据。访问器属性有如下 4 个特性。
 [[Configurable]] ：表示能否通过 delete 删除属性从而重新定义属性，能否修改属性的特
性，或者能否把属性修改为数据属性。对于直接在对象上定义的属性，这个特性的默认值为
true 。
 [[Enumerable]] ：表示能否通过 for-in 循环返回属性。对于直接在对象上定义的属性，这
个特性的默认值为 true 。
 [[Get]] ：在读取属性时调用的函数。默认值为 undefined 。
 [[Set]] ：在写入属性时调用的函数。默认值为 undefined 。
## 博文参考


   
        // 虽然可以通过对象实例访问保存在原型中的值，但却不能通过对象实例重写原型中的值。,当为对象实例添加一个属性时，这个属性就会屏蔽原型对象中保存的同名属性

      // 要取得对象上所有可枚举的实例属性，可以使用 ECMAScript 5 的 Object.keys() 方法。这个方法接收一个对象作为参数，返回一个包含所有可枚举属性的字符串数组

      //要修改属性默认的特性，必须使用 ECMAScript 5 的 Object.defineProperty() 方法。这个方法接收三个参数：属性所在的对象、属性的名字和一个描述符对象。其中，描述符(descriptor)对象的属性必须是： configurable 、 enumerable 、 writable 和 value 。设置其中的一或多个值，可以修改对应的特性值。
