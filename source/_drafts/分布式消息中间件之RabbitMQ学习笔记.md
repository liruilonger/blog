---
title: 分布式消息中间件之RabbitMQ学习笔记
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-04-20 01:52:11/分布式消息中间件之RabbitMQ学习笔记.html'
mathJax: false
date: 2022-04-20 09:52:11
thumbnail:
---

**<font color="009688"> 户外依然大雨滂沱，只是这回彷彿不仅命运一人独自哭泣，不晓得由来，窗外的雨水似乎渗上我心头，有些寒冻，有些缩麻，还有些苦涩。城市万家灯火，橘黄街灯与家户里的温暖流洩，我总觉得这时候的我，最脆弱。 -----《Unser Leben Unser Traum》**</font>

<!-- more -->
## 写在前面
***
+ 嗯，陆续的整理一些中间件的笔记
+ 今天和小伙伴们分享`RabbitMQ`相关的笔记
+ 博文内容涉及：
    + `《分布式消息中间件实践》` `RabbitMQ`部分读书笔记
    + `RabbitMQ`的简单介绍
    + `AMQP`协议标准介绍
    + `RabbitMQ  Demo`


**<font color="009688"> 户外依然大雨滂沱，只是这回彷彿不仅命运一人独自哭泣，不晓得由来，窗外的雨水似乎渗上我心头，有些寒冻，有些缩麻，还有些苦涩。城市万家灯火，橘黄街灯与家户里的温暖流洩，我总觉得这时候的我，最脆弱。 -----《Unser Leben Unser Traum》**</font>
 ***


# RabbitMQ

## RabbitMQ简介

`RabbitMQ`是一个由`Erlang`语言开发的基于`AMOP标准`的`开源`消息中间件。`RabbitMQ`最初起源于`金融系统`,用于在`分布式系统`中存储转发消息,在`易用性`、`扩展性`、`高可用性`等方面表现不俗。其具体`特点`包括:

+ <font color=brown>保证可靠性( Reliability)</font>, `RabbitMQ`使用一些机制来保证可靠性,如`持久化、传输确认、发布确认`等。
+ <font color=orange>具有灵活的`路由(Flexible Routing)功能`</font>。在消息进入队列之前,是通过`Exchange (交换器)`来`路由消息`的。对于典型的<font color=royalblue>路由功能</font>, `RabbitMQ`已经提供了一些内置的`Exchange`来实现。针对更复杂的路由功能,可以将`多个Exchange`绑定在一起,也可以通过插件机制来实现自己的`Exchange`.
+ <font color=green>支持消息集群(Clustering)</font>,多台`RabbiMQ`服务器可以组成一个集群,形成一个逻辑`Broker`.
+ <font color=read>具有高可用性(Highly Available)</font>,队列可以在`集群中`的机器上进行`镜像`,使得在部分节点出现问题的情况下队列仍然可用。
+ <font color=read>支持多种协议(Multi-protocol)</font>, `RabbitMQ`除支持`AMQP`协议之外,还通过插件的方式支持其他消息队列协议,比如`STOMP, MQTT`等。
+ <font color=chocolate>支持多语言客户端(Many Client)</font>,`RabbitMQ`几乎支持所有常用的语言,比如`Java`. `.NET`, `Ruby`等
+ <font color=read>提供管理界面(Management UI)</font>, `RabbitMQ`提供了一个易用的用户界面,使得用户可以监控和管理消息Broker的许多方面
+ <font color=camel>提供跟踪机制(Tracing)</font>, `RabbitMQ`提供了消息跟踪机制,如果消息异常,使用者可以查出发生了什么情况。
+ <font color=purple>提供插件机制(Plugin System)</font>, `RabbitMQ`提供了许多插件,从多方面进行扩展,也可以编写自己的插件.

我们先来看一下`AMOP协议`，了解消息中间件的小伙伴，这个协议应该不陌生，这里我们简单了解下

##  AMQP标准

在2004年,`摩根大通`和`iMatrix`开始着手`Advanced Message Queuing Protocol (AMQP)`开放标准的开发。`2006`年,发布了`AMQP规范`。目前`AMQP`协议的版本为`1.0`。

**一般来说,将`AMQP`协议的内容分为三部分:`基本概念`、`功能命令`和`传输层协议`**。

+ 基本概念：指`AMQP内部定义的各组件及组件的功能说明`。

+ 功能命令：指该协议所定义的一系列命令,应用程序可以基于这些命令来实现相应的功能。

+ 传输层协议(TCP/UDP)：是一个网络级协议,它定义了数据的传输格式,消息队列的客户端可以`基于这个协议与消息代理和AMQP的相关模型`进行交互通信,该协议的内容包括`数据帧处理、信道复用、内容编码、心跳检测、数据表示和错误处理`等。

### **<font color=brown>主要概念</font>**

`Message (消息)` :消息服务器所处理数据的`原子单元`。消息可以携带`内容`,从格式上看,消息包括一个`内容头`、`一组属性`和`一个内容体`。

>这里所说的消息可以对应到许多不同`应用程序的实体`,比如一个`应用程序级消息`、一个`传输文件`、一个`数据流帧`等。消息可以被保存到磁盘上,这样即使发生严重的网络故障、服务器崩溃也可确保投递消息可以有优先级,高优先级的消息会在等待同一个消息队列时在低优先级的消息之前发送,当消息必须被丢弃以确保消息服务器的服务质量时,服务器将会优先丢弃低优先级的消息。`消息服务器不能修改所接收到的并将传递给消费者应用程序的消息内容体。消息服务器可以在内容头中添加额外信息,但不能删除或修改现有信息。`

`Publisher (消息生产者)`:也是一个`向交换器发布消息`的`客户端`应用程序。

`Exchange (交换器)`:用来`接收消息`生产者所发送的消息并将这些消息`路由`给服务器中的`队列`。

`Binding (绑定)`:用于`消息队列和交换器之间的关联`。一个绑定就是基于`路由键`将`交换器和消息队列连接起来的路由规则`

>所以可以将交换器理解成一个由绑定构成的`路由表`(路由控制表，IP寻址)。

`Virtual Host (虚拟主机)`:它是`消息队列以及相关对象`的集合,是共享同一个身份验证和加密环境的独立服务器域。每个虚拟主机本质上都是一个`mini版的消息服务器`,拥有自己的队列、交换器、绑定和权限机制。


`Broker (消息代理)`:表示`消息队列服务器`,接受客户端连接,实现`AMQP消息队列和路由功能的过程`。

`Routing Key (路由规则)`:虚拟机可用它来确定如何路由一个特定消息。

`Queue (消息队列)`:用来保存消息直到发送给消费者。它是消息的容器,也是消息的终点。一个消息可被投入一个或多个队列中。消息一直在队列里面,等待消费者连接到这个队列将其取走。

`Connection (连接)`:可以理解成客户端和消息队列服务器之间的一个`TCP连接`。

`Channel (信道)`:仅仅当创建了连接后,若客户端还是不能发送消息,则需要为连接`创建一个信道`。`信道`是一条独立的`双向数据流通道`,它是建立在真实的`TCP连接内的虚拟连接`。

>` AMQP命令`都是通过`信道`发出去的,不管是`发布消息、订阅队列还是接收消息`,它们都通过信道完成。`一个连接`可以包含`多个信道`,之所以需要`信道`,<font color=amber>是因为`TCP连接`的`建立`和`释放`都是十分昂贵的,如果客户端的每一个线程都需要与消息服务器交互,如果每一个线程都建立了一个TCP连接,则暂且不考虑TCP连接是否浪费,就算操作系统也无法承受每秒建立如此多的TCP连接</font>。

`Consumer (消息消费者)`:表示一个从消息队列中取得消息的客户端应用程序。
  
#### **<font color=purple>核心组件的生命周期</font>**

**<font color=orange>消息的生命周期,一条消息的流转过程通常是这样的</font>**: 

+ `Publisher(消息生产者)`产生一条数据,发送到`Broker(消息代理)`, `Broker`中的`Exchange(交换器)`可以被理解为一个规则表(<font color=read>Routing Key和Queue的映射关系-Binding</font>), `Broker`收到消息后根据`Routing Key`查询投递的目标`Queue`.
+ `Consumer`向`Broker`发送订阅消息时会指定自己监听哪个`Queue`,当有数据到达`Queue`时`Broker`会推送数据到`Consumer`.
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210706101803720.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

**<font color=royalblue>交换器的生命周期</font>**

每台`AMQP服务器`都预先创建了许多交换器实例,它们在服务器启动时就存在并且不能被销毁。如果你的应用程序有特殊要求,则可以选择自己创建交换器,并在完成工作后进行销毁。


**<font color=read>队列的生命周期</font>**

这里主要有两种消息队列的生命周期,即`持久化消息队列`和`临时消息队列`。持久化消息队列可被`多个消费者共享`,不管是否有消费者接收,它们都可以独立存在。临时消息队列对`某个消费者是私有`的,只能绑定到此消费者,当消费者断开连接时,该消息队列将被删除。

### **<font color=tomato>功能命令</font>**

`AMQP协议文本`是分层描述的,在不同主版本中划分的层次是有一定区别的。

**<font color=royalblue>0-9</font>** 版本共分两层: `Functional Layer (功能层)`和`Transport Layer (传输层)`
+ `功能层`定义了`一系列命令`,这些命令按功能逻辑组合成不同的类(Class),客户端应用可以利用它们来实现自己的业务功能。
+ `传输层`将功能层所接收的消息传递给服务器经过相应处理后再返回,处理的事情包括`信道复用、帧同步、内容编码、心跳检测、数据表示和错误处理`等.

**<font color=amber>0-10</font>** 版本则分为三层: `Model Layer (模型层)`、`Session Layer (会话层)`和`Transport Layer(传输层)`。
+ `模型层`定义了一套命令,客户端应用利用这些命令来实现业务功能。
+ `会话层`负责将命令从客户端应用传递给服务器,再将服务器的响应返回给客户端应用,会话层为这个传递过程提供了可靠性、同步机制和错误处理。
+ `传输层`负责提供帧处理、信道复用、错误检测和数据表示

### <font color=tomato>传输协议</font>

#### **<font color=orange> 消息数据格式</font>**

所有的消息`必须有特定的格式来支持`,这部分就是在`传输层中定义`的。`AMQP是二进制协议`,协议的不同版本在该部分的描述有所不同。`0-9-1版本`为例,看一下该版本中的消息格式

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210706103848554.png)
所有的`消息数据`都被组织成`各种类型的帧(Frame)`,帧可以携带`协议方法`和`其他信息`,所有`帧`都有同样的格式,都由一个`帧头(header, 7个字节)`、`任意大小的负载(payload)`和`一个检测错误的结束帧(frame-end)字节`组成。其中:
+ `帧头`包括一个`type字段`、一个`channel`字段和一个`size`字段;
+ `帧负载`的格式依赖帧类型(type)

要读取一个`帧`需要三步。
+ ①读取帧头,检查`帧类型`和`通道(channel)`.
+ ②根据`帧类型`读取`帧负载`并进行处理。
+ ③读取结束帧字节。

`AMQP`定义了如下帧类型。
+ type=1, "METHOD":方法帧; 
+ type=2, "HEADER":内容头帧; 
+ type=3,"BODY":内容体帧; 
+ type=4, "HEARTBEAT":心跳帧通道

编号为0的代表`全局连接中的所有帧`, 1-65535代表`特定通道的帧`。`size`字段是指`帧负载`的大小,它的数值不包括`结束帧字节`。`AMQP`使用`结束帧`来`检测`错误客户端和服务器实现引起的错误。

##  RabbitMQ基本概念

如图是RabbitMQ的整体架构图。

![在这里插入图片描述](https://img-blog.csdnimg.cn/13c36a0f28dc4e9c81f566ae9b4bfd09.png)

`Message (消息)`:消息是不具名的,它由`消息头`和`消息体`组成。消息体是不透明的,而消息头则由一系列可选属性组成,这些属性包括 `routing-key (路由键)`,`priority (相对于其他消息的优先级)`、 `delivery-mode (指出该消息可能需要持久化存储)等`。

`Publisher (消息生产者)`:一个向交换器发布消息的客户端应用程序。

`Exchange (交换器)`:用来`接收生产者`发送的消息,并将这些`消息路由给服务器`中的`队列`。. 

`RabbitMQ`是`AMQP`协议的一个`开源实现`,所以其`基本概念`也就是`AMQPt`中的基本概念。关于其他的概念小伙伴可以看上面。 

### (1) AMQP中的消息路由

在`AMQP`中增加了`Exchange`和`Binding`的角色。生产者需要把消息发布到`Exchange`上,消息最终到达队列并被消费者接收,而`Binding`决定`交换器上`的消息应该被发送到哪个`队列`中。

### (2)交换器类型

不同类型的`交换器分发消息`的`策略`也不同,目前交换器有4种类型: `Direct`, `Fanout`, `Topic`,`Headers`。其中`Headers`交换器匹配`AMQP`消息的`Header`而不是`路由键`。此外, `Headers`交换器和`Direct`交换器完全一致,但性能相差很多,目前几乎不用了,所以下面我们看另外三种类型。

#### Direct交换器
**<font color=read>如果消息中的路由键(routing key)和Binding中的绑定键(binding key)一致,交换器就将消息发送到对应的队列中</font>**.

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210709173604580.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
路由键与队列名称要`完全匹配`,如果将一个队列绑定到交换机要求路由键为“dog",则只转发routing key标记为"dog"的消息,不会转发"dog.puppy"消息,也不会转发"dog.guard "消息等。`Direct交换器是完全匹配、单播的模式`。




### Fanout交换器
**<font color=brown>Fanout交换器不处理路由键,只是简单地将队列绑定到交换器</font>**
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210709173823881.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
发送到交换器的每条消息都会被转发到与该交换器绑定的所有队列中,这很像`子网广播`,子网内的每个主机都获得了一份复制的消息。通过Fanout交换器转发消息是最快的。

### Topic交换器

**<font color=tomato>Topic交换器通过模式匹配分配消息的路由键属性,将路由键和某种模式进行匹配,此时队列需要绑定一种模式。</font>**

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210709174146868.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
Topic交换器将`路由键和绑定键的字符串切分成单词`,这些单词之间用点<font color=brown>"."</font>隔开,该交换器会识别两个通配符: `“#”和“*”,其中“#”匹配0个或多个单词, “*”匹配不多不少一个单词。`

##  RabbitMQ Demo

RabbitMQ官网：[https://www.rabbitmq.com/](https://www.rabbitmq.com/)

### RabbitMQ服务安装

基于Docker的安装：

RabbitMQ镜像 ：[https://registry.hub.docker.com/_/rabbitmq?tab=description&page=2&ordering=last_updated](https://registry.hub.docker.com/_/rabbitmq?tab=description&page=2&ordering=last_updated)

```bash
# 启动docker服务
[root@liruilong ~]# systemctl restart docker
# 查看镜像
[root@liruilong ~]# docker images
#指定版本，该版本包含了web控制页面
[root@liruilong ~]# docker pull rabbitmq:management
```
运行容器：

方式一：默认guest 用户，密码也是 guest
```bash
[root@liruilong ~]# docker run -d --hostname my-rabbit --name rabbit -p 15672:15672 -p 5672:5672 rabbitmq:management
```
方式二：设置用户名和密码
```bash
[root@liruilong ~]# docker run -d --hostname my-rabbit --name rabbit -e RABBITMQ_DEFAULT_USER=user -e RABBITMQ_DEFAULT_PASS=password -p 15672:15672 -p 5672:5672 rabbitmq:management
```
发布服务，将端口映射到15672，5672
```bash
[root@liruilong ~]# docker run -d --hostname my-rabbit --name rabbit -p 15672:15672 -p 5672:5672 rabbitmq:management
2189f2fa53f1e76306a2ad422e0fa33bca1ae0f3ee77514573d71aca9ce24801
[root@liruilong ~]# 
```

**<font color=brown>这里需要注意的是端口绑定，需要把`访问`端口和`管理`端口同时绑定。如果是ESC的话，需要配置安全组</font>**

访问路径：http://localhost:15672/ 登录

![在这里插入图片描述](https://img-blog.csdnimg.cn/d6eebd8dc0b54467b72f782a5035fe34.png)


下面我们通过具体的Demo来深入学习下Rabbit相关概念

### Hello World!


**Java客户端访问RabbitMQ实例**

`RabbitMQ`支持多种语言访问。使用java需要添加的`maven`依赖，下面我们看一个简单的 `Hello World!` Demo
```xml
        <dependency> 
            <groupId>com.rabbitmq</groupId>
            <artifactId>amqp-client</artifactId>
            <version>4.1.0</version>
        </dependency>
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/4d2fb4e6d2f749e7870cf8af718339ba.png)

**<font color=yellowgreen>消息生产者</font>**
```java
package msg_queue.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author Liruilong
 * @Description TODO 消息生产者
 * @date 2022/4/20  20:46
 **/
public class Producer {

    public static void main(String[] args) throws IOException, TimeoutException {
        //创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        //设置 RabbitMQ 地址
        factory.setHost("localhost");
        //默认访问5672端口  factory.setPort(5672);
        factory.setVirtualHost("/");
        //建立到代理服务器到连接

        try (Connection conn = factory.newConnection();
            //创建信道
            Channel channel = conn.createChannel()) {
            //声明交换器
            String exchangeName = "hello-exchange";
            // 交换器类型为direct
            channel.exchangeDeclare(exchangeName, "direct", true);
            // 定义 路由键
            String routingKey = "testRoutingKey";
            //发布消息
            byte[] messageBodyBytes = "学习Rabbitmq".getBytes();

            channel.basicPublish(exchangeName, routingKey, null, messageBodyBytes);
        }
    }
}
```
首先创建一个连接工厂,再根据连接工厂创建连接,之后从连接中创建信道,接着声明一个交换器和指定路由键,然后才发布消息,最后将所创建的信道、连接等资源关闭。`代码中的ConnectionFactory, Connection、 Channel都是RabbitMQ提供的API中最基本的类。`

+ `ConnectionFactory`是Connection的制造工厂
+ `Connection`代表RabbitMQ的Socket连接,它封装了Socket操作的相关逻辑。
+ `Channel`是与RabbitMQ打交道的最重要的接口,大部分业务操作都是在Channel中完成的,比如定义队列、定义交换器、队列与交换器的绑定、发布消息等。

**<font color=amber>消息消费者</font>**
```java
package msg_queue.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author Liruilong
 * @Description TODO 消息消费者
 * @date 2022/4/20  20:48
 **/
public class Consumer {

    public static void main(String[] args) throws IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setHost("127.0.0.1");
        factory.setVirtualHost("/");
        //建立到代理服务器到连接
        try (Connection conn = factory.newConnection();
             //创建信道
             final Channel channel = conn.createChannel()) {
            //声明交换器
            String exchangeName = "hello-exchange";
            // true 设置是否持久化
            channel.exchangeDeclare(exchangeName, "direct", true);
            //获取队列，不带任何参数的queueDeclare()方法，默认会创建一个由rabbitmq命名的(形如amq.gen-LhQzlgv3GhDOv8PIDabOXA)、排他的、自动删除的、非持久化的队列。
            String queueName = channel.queueDeclare().getQueue();
            String routingKey = "testRoutingKey";
            //绑定队列，通过键 testRoutingKey 将队列和交换器绑定起来
            channel.queueBind(queueName, exchangeName, routingKey);
            //消费消息
            while (true) {
                // 设置是否自动确认,当消费者接收到消息后要告诉 mq 消息已接收，如果将此参数设置为 true 表示会自动回复 mq，如果设置为 false，要通过编程实现回复
                boolean autoAck = false;
                channel.basicConsume(queueName, autoAck
                        // 设置消费者获取消息成功的回调函数
                        , (consumerTag, delivery) -> {
                            System.out.printf("消费的消息体内容：%s\n", new String(delivery.getBody(), "UTF-8"));
                            System.out.println("消费的路由键：" + delivery.getEnvelope().getRoutingKey());
                            System.out.println("消费的内容类型：" + delivery.getProperties().getContentType());
                            System.out.println("consumerTag:"+consumerTag);
                            //确认消息
                            channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                        // 设置消费者获取消息失败的回调函数
                        }, consumerTag -> {
                            System.out.println("consumerTag:"+consumerTag);
                        });
            }
        }
    }
}
```

```bash
消费的消息体内容：学习Rabbitmq
消费的路由键：testRoutingKey
消费的内容类型：null
consumerTag:amq.ctag-rC_49IlY-Awwj7G_hXIR_Q
```
消息消费者同样需要创建一个连接工厂,再根据连接工厂创建连接,之后从连接中创建信道，然后创建交换器，路由建，创建队列，通过路由建将交换器和队列绑定。


###  通道
消息客户端和消息服务器之间的`通信是双向`的,不管是对客户端还是服务器来说,保持它们之间的网络连接是很耗费资源的。为了在不占用大量TCP/P连接的情况下也能有大量的逻辑连接, AMQP增加了通道(Channel)的概念..

RabbitMQ支持并鼓励在一个连接中创建多个通道,因为相对来说创建和销毁通道的代价会小很多。需要提醒的是,作为经验法则,应该尽量避免在线程之间共享通道,你的应用应该使用每个线程单独的通道,而不是在多个线程上共享同一个通道,因为大多数客户端不会让通道线程安全(因为这将对性能产生严重的负面影响)。


### 工作队列(又名：任务队列)

上面的Demo是一个一对一的消息中间件模式，即一个消费者只对应一个生产者，生产者指定路由键把消息发生到交换器，消费者通过路由键绑定交换器和工作队列，从而获取工作队列的消息。但是在实际的情况中，往往并不是这样。一个生产者要对应好多个消费者，比如我们需要导出数据量相对较大的excel或者pdf文件，这是一个相对耗时的操作，有时可能还会涉及到IO阻塞，所以一般会放到异步处理，如果把这种行为当中是一种任务来看，常见的处理方式是，通过消息队列或者定时任务，或者一个调度框架来处理。任务状态放到数据库里。


一般情况下通过消息队列是一种很好的解决办法,因为我们可以起多个工作进程来处理工作队列中任务。

来看一个Demo，这里我们体验下 **python的Rabbitmq客户端**

需要安装依赖包 `pika`
```py
python -m pip install pika --upgrade
```
在消费者里面使用`python`的`sleep方法`来模拟执行时间，在生产中通过'....'来声明任务的执行时间。

下面的代码是一个生产者，用于生产消息.即创建任务
```python
#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
"""
@File    :   send.py
@Time    :   2022/04/21 00:09:19
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   rabbitmq 消息生产者
"""

# here put the import lib
import pika
import sys

# 建立连接
connection = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.26.55'))
# 建立信道
channel = connection.channel()

message = ' '.join(sys.argv[1:]) or "Hello World!"
# 声明队列,设置durable参数为True,表示在server重启中要“活下来”，持久化。
channel.queue_declare(queue='hello', durable=True)
# 发送消息到队列，这里使用默认交换器，指定路由建和消息实体
channel.basic_publish(
    exchange = ''
    , routing_key = 'hello'
    , body = message)

print(" [x] Sent %r" % message)

connection.close()

```
运行多次脚本生成不同的耗时的任务。
```bash
D:\python\code\rabbit_mq_demo>
D:\python\code\rabbit_mq_demo>py send.py 这是一本100的pdf...
 [x] Sent '这是一本100的pdf...'

D:\python\code\rabbit_mq_demo>py send.py 这是一本200的pdf......
 [x] Sent '这是一本200的pdf......'

D:\python\code\rabbit_mq_demo>py send.py 这是一本400的pdf.............
 [x] Sent '这是一本400的pdf.............'

D:\python\code\rabbit_mq_demo>py send.py 这是一本500的pdf................
 [x] Sent '这是一本500的pdf................'

D:\python\code\rabbit_mq_demo>

```
然后我们创建消费者进程来执行工作队列中的任务

```py
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   receive.py
@Time    :   2022/04/21 00:11:58
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   消费者
"""

# here put the import lib
import pika, sys, os
import time

def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.26.55'))
    channel = connection.channel()
    #  获取队列
    channel.queue_declare(queue='hello',durable=True)
    print(' [*] Waiting for messages. To exit press CTRL+C')


    # 成功接收消息的回调
    def callback(ch, method, properties, body):
        print(" [x] 开始生成文件 %r" % body.decode())
        # 有几个点就睡眠几秒
        time.sleep(body.count(b'.'))
        print(" [x] 生成文件结束")


    # 接收消息
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)


    channel.start_consuming()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
```
#### 循环调度

这里我们运行两个消费者进程，可以发现工作队列被多个消费者顺序消费。在一些PDF文件生成中我们也可以使用类似的方式
```bash
D:\python\code\rabbit_mq_demo>python receive.py
 [*] Waiting for messages. To exit press CTRL+C
 [x] 开始生成文件 '这是一本200的pdf......'
 [x] 生成文件结束
 [x] 开始生成文件 '这是一本500的pdf................'
 [x] 生成文件结束
=============================
D:\python\code\rabbit_mq_demo>python receive.py
 [*] Waiting for messages. To exit press CTRL+C
 [x] 开始生成文件 '这是一本100的pdf...'
 [x] 生成文件结束
 [x] 开始生成文件 '这是一本400的pdf.............'
 [x] 生成文件结束


```
默认情况下，RabbitMQ 会按顺序将每条消息发送给下一个消费者。平均而言，每个消费者都会收到相同数量的消息。这种分发消息的方式称为循环。

#### 消息确认

消费者消费消息的时候需要一定的时间，如果在这个时间内，消费者挂掉，但是生产队列并不知道消费者挂掉，正常我们希望如果一个消费者挂掉，我们希望将任务交付给另一个消费者。

在 RabbitMQ 通过 `消息确认` 来实现。当消费者处理完任务是，会返回一个 ack(nowledgement) ，告诉 RabbitMQ 一个特定的任务已经被接收、处理并且 RabbitMQ 可以自由地删除它。当消费者挂掉，没有发生ack时(其通道关闭、连接关闭或 TCP 连接丢失)，RabbitMQ 将认为消息没有完全处理并将消息重新排队。如果同时有其他消费者在线，它会迅速将其重新发送给另一个消费者。这样，即使消费者挂掉，也可以确保不会丢失任何消息。

对消费者的ack心跳默认为 30 分钟，通过这种机制，这有助于检测异常的消费者。

默认情况下，`消息自动确认是打开的`。在前面的示例中，我们通过`auto_ack=True `关闭了它。
```python
#channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)
channel.basic_consume(queue='hello', on_message_callback=callback)
```

#### 消息持久性

如果 RabbitMQ 服务器停止，我们的任务仍然会丢失。当 RabbitMQ 退出或崩溃时，它会忘记队列和消息，除非你告诉它不要这样做。确保消息不会丢失需要做两件事：我们需要将队列和消息都标记为持久的。

**队列**
```python
channel.queue_declare(queue='task_queue',durable=True)
```
这里需要注意的是：
+ RabbitMQ 不允许`使用不同的参数重新定义现有队列`，并且会向任何尝试这样做的程序返回`错误`。
+ 通过`queue_declare`获取队列，当更改时需要同时应用于`生产者和消费者`代码。


通过上面的配置，即使 RabbitMQ 重新启​​动，task_queue队列也不会丢失。将息标记为持久的需要设置`delivery_mode`为`pika.spec.PERSISTENT_DELIVERY_MODE`值

**关于消息持久性的注意事项**

将消息标记为持久性`并不能完全保证消息不会丢失`。虽然它告诉 RabbitMQ 将消息保存到磁盘，但是当 RabbitMQ 接受消息并且还没有保存它时，仍然有很短的时间窗口，如果您需要更强的保证，那么您可以使用 `发布者确认`。

**消息**
```python
channel.basic_publish(
    exchange=''
    , routing_key='hello'
    , body=message
    , properties=pika.BasicProperties(
        delivery_mode=pika.spec.PERSISTENT_DELIVERY_MODE
    ))
```


#### 公平调度

循环调度调度的一个很大的缺点就是，不会衡量任务的执行时间，当某些耗时长的任务恰好落在了一个消费者身上，那对那个消费者太不公平，所以我们希望可以有一种公平的调度机制。正常的解决方案，我们可以设置不同的调度策略，通过算法计算，利用不同消费者指标值，为每个消费者打分，选择合适的。

那么 RabbitMQ  又是如何处理的？

RabbitMQ 使用带有`prefetch_count=1`设置的`Channel#basic_qos`通道方法 。这使用`basic.qos`协议方法来告诉 `RabbitMQ `一次不要给一个` worker `多个消息。或者，换句话说，在工作人员处理并确认之前的消息之前，不要向工作人员发送新消息。相反，它将把它分派给下一个不忙的消费者

```py
    # 接收消息
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue='hello', on_message_callback=callback)

```



###  总结
个人认为, `RabbitMQ`最大的`优势`在于提供了`比较灵活的消息路由策略`、`高可用性`、`可靠性`,以及`丰富的插件`、`多种平台支持和完善的文档`。不过,由于`AMQP协议`本身导致它的实现比较重量,从而使得与其他`MQ (比如Kafka)对比其吞吐量处于下风`。在选择MQ时关键还是看需求-是更看重`消息的吞吐量`、`消息堆积能力`还是`消息路由的灵活性`、`高可用性`、可靠性等方面,先确定场景,再对不同产品进行有针对性的测试和分析,最终得到的结论才能作为技术选型的依据




### 参考博文书籍
***
`《分布式消息中间件实践》` `RabbitMQ`部分

RabbitMQ官网：https://www.rabbitmq.com/

