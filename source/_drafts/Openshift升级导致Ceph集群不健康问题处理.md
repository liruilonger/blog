---
title: Openshift升级导致Ceph集群不健康问题处理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-04-24 07:52:07/Openshift升级导致Ceph集群不健康问题处理.html'
mathJax: false
date: 2023-04-24 15:52:07
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

升级openshift 4.12，使用的  kubernetes1.25 的版本，目前是1.24 ，k8s api接口会有些变动


升级相关文档： https://access.redhat.com/mt/zh-hans/articles/6955381

#### Kubernetes 存储解决方案介绍

#### OCS

OCS 是 OpenShift Container Storage 的缩写，它是一个 Kubernetes 存储解决方案，提供了持久化存储和数据管理功能。OCS 可以将本地存储和云存储资源池化，以便在整个集群中进行分配和管理。OCS 包括多个组件，其中 "ocs-storagecluster-storagesystem" 是其中之一，它负责管理存储系统的配置和状态。

OCS 提供了多种存储选项，包括文件、块和对象存储。它还提供了多种存储类别，包括高性能、容错和低成本存储。OCS 还支持多种存储协议，包括 NFS、iSCSI 和 S3。

OCS 的主要优点是它可以与 OpenShift 集成，使得存储管理变得更加简单和高效。


![在这里插入图片描述](https://img-blog.csdnimg.cn/56f059b19fad4acd920d6b8735de31bc.png)




## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
