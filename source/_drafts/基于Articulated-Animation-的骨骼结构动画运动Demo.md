---
title: 基于Articulated Animation 的骨骼结构动画运动 Demo 
tags:
  - Articulated-Animation
categories:
  - Articulated-Animation
toc: true
recommend: 1
keywords: Articulated-Animation
uniqueId: '2023-06-12 02:15:12/基于Articulated Animation 的骨骼结构动画运动Demo.html'
mathJax: false
date: 2023-06-12 10:15:12
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 工作中遇到，简单整理
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

模型文件下载：
[https://drive.google.com/drive/folders/1jCeFPqfU_wKNYwof0ONICwsj3xHlr_tb](https://drive.google.com/drive/folders/1jCeFPqfU_wKNYwof0ONICwsj3xHlr_tb)


克隆项目 [https://github.com/snap-research/articulated-animation](https://github.com/snap-research/articulated-animation)
```bash
(base) test@test:~$ mkdir ArticulatedAnimation
(base) test@test:~$ cd ArticulatedAnimation
(base) test@test:~/ArticulatedAnimation$ git clone  https://github.com/snap-research/articulated-animation.git
Cloning into 'articulated-animation'...
...
Resolving deltas: 100% (19/19), done.
```
创建虚拟环境
```bash
(base) test@test:~/ArticulatedAnimation$ conda create -n ArticulatedAnimation python==3.7
```

切换虚拟环境
```bash
(base) test@test:~/ArticulatedAnimation$ conda activate ArticulatedAnimation
(ArticulatedAnimation) test@test:~/ArticulatedAnimation$ ls
articulated-animation
(ArticulatedAnimation) test@test:~/ArticulatedAnimation$ cd articulated-animation/
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation$ ls
animate.py       config      demo.py            logger.py  reconstruction.py  sup-mat         train.py
augmentation.py  data        frames_dataset.py  modules    requirements.txt   sync_batchnorm  web
checkpoints      demo.ipynb  LICENSE.md         README.md  run.py             train_avd.py
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation$ pip install -r requirements.txt -i http://pypi.douban.com/simple/ --trusted-host pypi.douban.com
Looking in indexes: http://pypi.douban.com/simple/
Collecting imageio==2.3.0
  Downloading http://pypi.doubanio.com/packages/a7/1d/33c8686072148b3b0fcc12a2e0857dd8316b8ae20a0fa66c8d6a6d01c05c/imageio-2.3.0-py2.py3-none-any.whl (3.3 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 3.3/3.
```

运行默认 Demo
```bash
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation$ python demo.py --config ./config/vox256.yaml  --checkpoint vox256.pth --cpu
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/skimage/transform/_warps.py:105: UserWarning: The default mode, 'constant', will be changed to 'reflect' in skimage 0.15.
  warn("The default mode, 'constant', will be changed to 'reflect' in "
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/skimage/transform/_warps.py:110: UserWarning: Anti-aliasing will be enabled by default in skimage 0.15 to avoid aliasing artifacts when down-sampling images.
  warn("Anti-aliasing will be enabled by default in skimage 0.15 to "
demo.py:36: YAMLLoadWarning: calling yaml.load() without Loader=... is deprecated, as the default Loader is unsafe. Please read https://msg.pyyaml.org/load for full details.
  config = yaml.load(f)
  0%|                                                                                                                                                                                    | 0/133 [00:00<?, ?it/s]/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/torch/nn/functional.py:2705: UserWarning: Default grid_sample and affine_grid behavior has changed to align_corners=False since 1.3.0. Please specify align_corners=True if the old behavior is desired. See the documentation of grid_sample for details.
  warnings.warn("Default grid_sample and affine_grid behavior has changed "
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/torch/nn/functional.py:2506: UserWarning: Default upsampling behavior when mode=bilinear is changed to align_corners=False since 0.4.0. Please specify align_corners=True if the old behavior is desired. See the documentation of nn.Upsample for details.
  "See the documentation of nn.Upsample for details.".format(mode))
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/torch/nn/functional.py:1351: UserWarning: nn.functional.sigmoid is deprecated. Use torch.sigmoid instead.
  warnings.warn("nn.functional.sigmoid is deprecated. Use torch.sigmoid instead.")
100%|██████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 133/133 [02:03<00:00,  1.08it/s]
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/skimage/util/dtype.py:130: UserWarning: Possible precision loss when converting from float32 to uint8
  .format(dtypeobj_in, dtypeobj_out))
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation$ ls
```

在 Demo 文件中，作者写了默认的素材路径，所以可以直接执行,如果使用自己的素材，报下面的错误，有可能是分辨率不一致的问题
```bash
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation$ python demo.py --config ./config/ted384.yaml  --checkpoint ted384.pth --cpu
Traceback (most recent call last):
  File "demo.py", line 134, in <module>
    main(parser.parse_args())
  File "demo.py", line 104, in main
    source_image = imageio.imread(opt.source_image)
  File "/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/imageio/core/functions.py", line 206, in imread
    reader = read(uri, format, 'i', **kwargs)
  File "/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/imageio/core/functions.py", line 126, in get_reader
    'in mode %r' % mode)
ValueError: Could not find a format to read the specified file in mode 'i'
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation$ python demo.py --config ./config/ted384.yaml  --checkpoint ted384.pth --cpu
Traceback (most recent call last):
  File "demo.py", line 134, in <module>
    main(parser.parse_args())
  File "demo.py", line 104, in main
    source_image = imageio.imread(opt.source_image)
  File "/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/imageio/core/functions.py", line 206, in imread
    reader = read(uri, format, 'i', **kwargs)
  File "/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/imageio/core/functions.py", line 126, in get_reader
    'in mode %r' % mode)
ValueError: Could not find a format to read the specified file in mode 'i'

```
关于上面执行报错的问题，运行 Demo 需要把 图片的分别率和视屏的分辨率设置成一样，通过 ff 命令可以查看分辨率大小
```bash
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation/sup-mat$ ls
driving.mp4  driving.mp4_  source.png  source.png_  teaser.gif
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation/sup-mat$ ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=p=0 driving.mp4
376,620
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation/sup-mat$ ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 source.png
1024x1449
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation/sup-mat$ ffmpeg -i source.png -vf scale=376:620 outpu.png

```
这里修改图片分辨率大小为视屏大小
```bash
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation/sup-mat$ ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=s=x:p=0 source.png
376x620
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation/sup-mat$ ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=p=0 driving.mp4
376,620
```
重新测试

```bash
(ArticulatedAnimation) test@test:~/ArticulatedAnimation/articulated-animation$ python demo.py --config ./config/ted384.yaml  --checkpoint ted384.pth --cpu
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/skimage/transform/_warps.py:105: UserWarning: The default mode, 'constant', will be changed to 'reflect' in skimage 0.15.
  warn("The default mode, 'constant', will be changed to 'reflect' in "
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/skimage/transform/_warps.py:110: UserWarning: Anti-aliasing will be enabled by default in skimage 0.15 to avoid aliasing artifacts when down-sampling images.
  warn("Anti-aliasing will be enabled by default in skimage 0.15 to "
demo.py:36: YAMLLoadWarning: calling yaml.load() without Loader=... is deprecated, as the default Loader is unsafe. Please read https://msg.pyyaml.org/load for full details.
  config = yaml.load(f)
  0%|                                                                                                                                                                                    | 0/207 [00:00<?, ?it/s]/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/torch/nn/functional.py:2705: UserWarning: Default grid_sample and affine_grid behavior has changed to align_corners=False since 1.3.0. Please specify align_corners=True if the old behavior is desired. See the documentation of grid_sample for details.
  warnings.warn("Default grid_sample and affine_grid behavior has changed "
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/torch/nn/functional.py:2506: UserWarning: Default upsampling behavior when mode=bilinear is changed to align_corners=False since 0.4.0. Please specify align_corners=True if the old behavior is desired. See the documentation of nn.Upsample for details.
  "See the documentation of nn.Upsample for details.".format(mode))
/home/test/anaconda3/envs/ArticulatedAnimation/lib/python3.7/site-packages/torch/nn/functional.py:1351: UserWarning: nn.functional.sigmoid is deprecated. Use torch.sigmoid instead.
  warnings.warn("nn.functional.sigmoid is deprecated. Use torch.sigmoid instead.")
 63%|██████████████████████████████████████████████████████████████████████████████████████████████████████████▊                                                               | 130/207 [02:00<01:11,  1.08it/s]
```





## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***

https://github.com/LIRUILONGS/articulated-animation_demo

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
