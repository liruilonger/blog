---
title: 《Go web 编程》读书笔记
tags:
  - test1
  - test2
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2022-02-12 19:32:25/《Go web 编程》读书笔记.html'
mathJax: false
date: 2022-02-13 03:32:25
thumbnail:
---
> 摘要
首页显示摘要内容(替换成自己的)
<!-- more -->
## 写在前面
***
+ 用`Markdown`写博客一年多了，最开始是用富文本写，后来发现`Markdown`更适合我，而且`CSDN`提供了导入导出的功能，图片可以云存储，所以导出了博文在本地也可以直接看，尤其是笔记类型很方便。所以这里总结一下自己常用模板和小伙伴们分享下
+ 笔记主要是关于自己使用`Morkdown`的一些`常用模板`的`版式`总结。<font color="#C0C4CC">[这里写整理笔记的具体方式，是读那本书笔记，看那个视频笔记，或者对于那个问题的笔记]
+ 笔记由两部分内容构成: `笔记类模板`和`技术点问题类模板`


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font><font color="#C0C4CC">[这里写一句鼓励自己的话,可以加一个颜色(可选)]</font>
 ***

<font color="#C0C4CC">博客内容</font>

# 1 GO 环境配置
Go是一种新的语言,一种并发的、带垃圾回收的、快速编译的语言。它具有以下特点:

可以在一台计算机上用几秒钟的时间编译一个大型的Go程序。
Go为软件构造提供了一种模型,它使依赖分析更加容易,且避免了大部分C风格include文件与库的开头。
Go是静态类型的语言,它的类型系统没有层级。因此用户不需要在定义类型之间的关系上花费时间,这样感觉起来比典型的面向对象语言更轻量级。
Go完全是垃圾回收型的语言,并为并发执行与通信提供了基本的支持。按照其设计,
Go打算为多核机器上系统软件的构造提供一种方法。
Go是一种编译型语言,它结合了解释型语言的游刃有余,动态类型语言的开发效率,以及静态类型的安全性。
它也打算成为现代的,支持网络与多核计算的语言

## 1.1 Go安装

Go的三种安装方式
Go源码安装(不读讲)
Go标准包安装(安装工具包)
第三方工具安装(yum,apt等)

## 1.2 GOPATH 与工作空间
### GOPATH 设置
go 命令依赖一个重要的环境变量：$GOPATH(这个不是 Go 安装目录)

+ src 存放源代码(比如：.go .c .h .s 等)
+ pkg 编译后生成的文件(比如：.a)
+ bin 编译后生成的可执行文件(为了方便，可以把此目录加入到 $PATH 变量中)



1.3 Go 命令
Go命令
go buildgo cleango fmtgo getgo installgo testgo doc其它命令1.5总结 