---
title: 关于Ansibel 中 DO447 考试 的一些笔记
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: '2023-03-06 12:48:52/ 关于Ansibel 中 DO447 考试 的一些笔记.html'
mathJax: false
date: 2023-03-06 20:48:52
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 考试整理笔记
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

```bash
git config --global user.name git
git config --global user.email   git@workstation.lab.example.com
git config --global push.default simple
```
```bash
git clone http://git.lab.example.com:8081/git/create_users.git 
cd create_users
```

```bash
notify: restart http

handlers:
  - name:  restart http
    service:
      name: httpd
      state: restarted
```

```bash
---
- name: Deploy content
  hosts: dev
  become: yes
  tasks:
    - name: deploy on dev alpha
      copy: 
        content: 
        dest:
      tags:
        - never
        - alpha
    - name: deploy on dev alpha
      copy: 
        content: 
        dest:
      tags:
        - never
        - beta    

```
```bash
forks = 45
gathering = explicit

```

```bash
---
- name: create user complex
  hosts: dev prod
  become: true
  vars_files:
    - user_list.yaml
  tasks:
    - name: create user use complex envirt
      user:
        name: "{{ item.name }}"
        uid: "{{ item.uid }}"
        comment: "{{ item.  | }} {{ item.  | }} {{ item.  | }}"
        password: "{{ lookup( 'password', 'password={{ item.name }} chars=digits  length=6' ) | password_hash('sha512') }}"
      loop: "{{ users }}"  
```









## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
