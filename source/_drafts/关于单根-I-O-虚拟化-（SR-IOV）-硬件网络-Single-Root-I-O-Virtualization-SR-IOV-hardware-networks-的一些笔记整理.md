---
title: >-
  关于单根 I/O 虚拟化 （SR-IOV） 硬件网络(Single Root I/O Virtualization (SR-IOV) hardware
  networks)的一些笔记整理
tags:
  - test1
categories:
  - test3
toc: true
recommend: 1
keywords: java
uniqueId: >-
  2023-04-19 02:55:44/关于单根 I/O 虚拟化 （SR-IOV） 硬件网络(Single Root I/O Virtualization
  (SR-IOV) hardware networks)的一些笔记整理.html
mathJax: false
date: 2023-04-19 10:55:44
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

## 简单介绍


SR-IOV（单根 I/O 虚拟化）是一种基于 `硬件的虚拟化` 技术，它允许多个 `虚拟机（VM）共享单个物理网络接口卡（NIC）`，而不会影响性能。

简单来说，SR-IOV 可以将`单个物理网络卡分成多个虚拟网络卡`，每个虚拟网络卡可以分配给不同的虚拟机。这样可以更好地利用 `硬件资源并提高网络性能` 。

要实现 `SR-IOV`，物理 `NIC` 必须支持该技术，而且 `hypervisor` 也必须支持它。一旦启用，`hypervisor` 可以在物理 NIC 上创建虚拟功能（VF），并将其分配给各个虚拟机。每个 `VF` 都会出现为虚拟机的单独物理` NIC`，使其能够直接与物理网络通信。

总之，`SR-IOV` 是一种基于硬件的虚拟化技术，它允许通过将单个物理网络卡分成多个虚拟网络卡来更好地利用硬件资源并提高网络性能。





单根 I/O 虚拟化 （SR-IOV） 规范是一种 PCI 设备分配类型的标准，该分配可以与多个容器共享单个设备。

SR-IOV 可以将在主机节点上识别为 物理功能 （PF） 的合规网络设备划分为多个虚拟功能 （VF）。 VF 的使用方式与任何其他网络设备相同。 设备的 SR-IOV 网络设备驱动程序确定 VF 在容器中的公开方式：

+ `netdevice` 驱动程序：容器中的常规内核网络设备 netns

+ `vfio-pci` 驱动程序：容器中装载的字符设备

对于需要高带宽或低延迟的应用，您可以将 SR-IOV 网络设备与安装在裸机或红帽 OpenStack 平台 （RHOSP） 基础架构上的 OpenShift 容器平台集群上的其他网络配合使用。

您可以为 `SR-IOV` 网络配置多网络策略。对此的支持是技术预览版，`SR-IOV` 附加网络仅支持`内核网卡`。数据平面开发工具包 （DPDK） 应用程序不支持它们。


```bash
$ oc label node <node_name> feature.node.kubernetes.io/network-sriov.capable="true"
```




## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***

https://docs.openshift.com/container-platform/4.12/networking/hardware_networks/about-sriov.html
***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
