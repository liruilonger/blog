---
title: GO语言实战之接口是什么
tags:
  - GO
categories:
  - GO
toc: true
recommend: 1
keywords: java
uniqueId: '2022-04-28 13:02:19/GO语言实战之接口是什么.html'
mathJax: false
date: 2022-04-28 21:02:19
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 用`Markdown`写博客一年多了


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>
 ***

<font color="#C0C4CC">博客内容</font>


在 Go 中的接口和 Java 等面向对象类型的接口概念基本一样，通过接口来描述一类行为，或者标识一类行为.

Go 标准库中的接口实现

```go
// Sample program to show how to write a simple version of curl using
// the io.Reader and io.Writer interface support.
package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

// init is called before main.
func init() {
	if len(os.Args) != 2 {
		fmt.Println("Usage: ./example2 <url>")
		os.Exit(-1)
	}
}

// main is the entry point for the application.
func main() {
	// Get a response from the web server.
	r, err := http.Get(os.Args[1])
	if err != nil {
		fmt.Println("请求异常：", err)
		return
	}

	// Copies from the Body to Stdout.
	io.Copy(os.Stdout, r.Body)
	if err := r.Body.Close(); err != nil {
		fmt.Println(err)
	}
}
```
这里的 `io.Copy()` 方式参数类型如下
```go
func Copy(dst Writer, src Reader) (written int64, err error) {
	return copyBuffer(dst, src, nil)
}
```
`src Reader` 的类型为  `Body io.ReadCloser`

```go
type ReadCloser interface {
	Reader
	Closer
}

```



