---
title: Kubernetes:如何配置基于内存的 emptyDir 卷大小
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: emptyDir
uniqueId: '2022-12-27 22:17:38/关于Kubernetes 中如何配置调整基于内存的emptyDir 卷的一些笔记.html'
mathJax: false
date: 2022-12-28 06:17:38
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

`emptyDir` 在 Pod 启动时为空，存储空间来自本地的 `kubelet` 根目录(通常是根磁盘)或内存

默认情况下， 即 `mptyDir:{}` 卷存储在该节点所使用的介质上； 此处的介质可以是磁盘、SSD 或网络存储，这取决于你的环境，比如当前节点物理存储使用的是磁盘，那 `mptyDir` 就映射的对应的节点的磁盘路径

可以通过 `docker` 命令来查看具体的映射位置。一般位于 `kubelet` 目录下。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.83 -m shell -a "docker inspect dcbf5c63263f | grep -A5 Mounts"
192.168.26.83 | CHANGED | rc=0 >>
        "Mounts": [
            {
                "Type": "bind",
                "Source": "/var/lib/kubelet/pods/76b518f6-9575-4412-b161-f590ab3c3135/volumes/kubernetes.io~empty-dir/volume1",
                "Destination": "/liruilong",
                "Mode": "",
```

当emptyDir 配置由内存支持时 `emptyDir.medium` 字段设置为 `Memory` ，卷由 `tmpfs` 文件系统支持，这意味着它们将存储在内存中而不是节点的物理存储中。 虽然 tmpfs 速度非常快，但是要注意它与磁盘不同： tmpfs 在节点重启时会被清除， 并且你所写入的所有文件都会计入容器的内存消耗，受容器内存限制约束。



使用内存支持的 emptyDir卷时，其大小与节点上可用的内存量成正比。它默认为 Linux 节点上内存的 50%。您可以在以下文档中阅读有关tmpfs文件系统及其行为的更多信息。

这是一个带有内存支持的emptyDir卷的 POD YAML 示例


```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$cat emptydir-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: liruilong-emptydir
spec:
  containers:
    - name:  liruilong-emptydir-pod
      image: busybox
      command: ["sleep", "infinity"]
      volumeMounts:
      - mountPath: /var/lib/containers
        name: container-storage
  volumes:
  - name: container-storage
    emptyDir:
     medium: Memory

```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl get pods  liruilong-emptydir  -o wide
NAME                 READY   STATUS    RESTARTS   AGE    IP             NODE                         NOMINATED NODE   READINESS GATES
liruilong-emptydir   1/1     Running   0          2d2h   10.244.70.32   vms83.liruilongs.github.io   <none>           <none>
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$echo "192.168.26.83" > host_list
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$ansible 192.168.26.83 -m shell -a "free -h" -i host_list
192.168.26.83 | CHANGED | rc=0 >>
              total        used        free      shared  buff/cache   available
Mem:           7.8G        576M        5.2G        382M        2.0G        6.5G
Swap:            0B          0B          0B
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl exec -it liruilong-emptydir -- sh
/ # free -h
              total        used        free      shared  buff/cache   available
Mem:           3.8G      646.4M        2.3G           0      889.7M        3.0G
Swap:             0           0           0
/ # df -h /var/lib/containers/
Filesystem                Size      Used Available Use% Mounted on
tmpfs                     3.7G         0      3.7G   0% /var/lib/containers
/ #
```
卷大小约为总节点内存的 100%。

如果您想确保内存支持的emptyDir卷的特定大小，而不管它运行在哪个节点，这就会带来挑战。默认内存支持的卷大小与运行 pod 的节点的强耦合是不可取的。

从 K8s 1.22 版本开始，默认启用此功能门控

使用以下 YAML 创建示例 POD

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$cat memorylimit.yaml
apiVersion: v1
kind: Pod
metadata:
  name: liruilong-emptydir
spec:
  volumes:
  - name: container-storage
    emptyDir:
     medium: Memory
     sizeLimit: 1Gi
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$cat kustomization.yaml
resources:
- emptydir-pod.yaml
patchesStrategicMerge:
- memorylimit.yaml
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl kustomize  ./
apiVersion: v1
kind: Pod
metadata:
  name: liruilong-emptydir
spec:
  containers:
  - command:
    - sleep
    - infinity
    image: busybox
    name: liruilong-emptydir-pod
    volumeMounts:
    - mountPath: /var/lib/containers
      name: container-storage
  volumes:
  - emptyDir:
      medium: Memory
      sizeLimit: 1Gi
    name: container-storage
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl apply -k  ./
pod/liruilong-emptydir created
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl get  pods liruilong-emptydir
NAME                 READY   STATUS    RESTARTS   AGE
liruilong-emptydir   1/1     Running   0          114s
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl exec -it liruilong-emptydir -- sh
/ # free -h
              total        used        free      shared  buff/cache   available
Mem:           3.8G      647.5M        2.3G           0      890.0M        3.0G
Swap:             0           0           0
/ # df -h /var/lib/containers/
Filesystem                Size      Used Available Use% Mounted on
tmpfs                     1.0G         0      1.0G   0% /var/lib/containers
/ #
```

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$cat set_memory.yaml
apiVersion: v1
kind: Pod
metadata:
  name: liruilong-emptydir
spec:
  containers:
    - name: liruilong-emptydir-pod
      resources:
        limits:
          memory: 2Gi
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$cat kustomization.yaml
resources:
- emptydir-pod.yaml
patchesStrategicMerge:
- set_memory.yaml          
```

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl kustomize ./
apiVersion: v1
kind: Pod
metadata:
  name: liruilong-emptydir
spec:
  containers:
  - command:
    - sleep
    - infinity
    image: busybox
    name: liruilong-emptydir-pod
    resources:
      limits:
        memory: 2Gi
    volumeMounts:
    - mountPath: /var/lib/containers
      name: container-storage
  volumes:
  - emptyDir:
      medium: Memory
    name: container-storage
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl apply  -k ./
pod/liruilong-emptydir created
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl exec -it liruilong-emptydir -- sh
/ # df -h /var/lib/containers/
Filesystem                Size      Used Available Use% Mounted on
tmpfs                     2.0G         0      2.0G   0% /var/lib/containers
/ # exit
```



```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$cat kustomization.yaml
resources:
- emptydir-pod.yaml
patchesStrategicMerge:
- memorylimit.yaml
- set_memory.yaml
```

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl kustomize ./
apiVersion: v1
kind: Pod
metadata:
  name: liruilong-emptydir
spec:
  containers:
  - command:
    - sleep
    - infinity
    image: busybox
    name: liruilong-emptydir-pod
    resources:
      limits:
        memory: 2Gi
    volumeMounts:
    - mountPath: /var/lib/containers
      name: container-storage
  volumes:
  - emptyDir:
      medium: Memory
      sizeLimit: 1Gi
    name: container-storage
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl apply  -k ./
pod/liruilong-emptydir created
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl exec -it liruilong-emptydir -- sh
/ # df -h /var/lib/containers/
Filesystem                Size      Used Available Use% Mounted on
tmpfs                     1.0G         0      1.0G   0% /var/lib/containers
/ # exit
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl delete  -k ./
```

## 博文参考

***

https://medium.com/@pradiptabanerjee/how-to-size-a-memory-backed-kubernetes-emptydir-volume-cdfe39d1b7e5