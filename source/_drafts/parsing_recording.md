---
output:
  word_document:
    highlight: tango
---


# 录音解析数据入表操作手册

需求：提取文件解析然后把解析结果入表

实现： 分四个任务实现：高内聚低耦合，每个任务高内聚，任务之间低耦合
+ 提取文件:
+ 调用解析接口:
+ 调用查询解析结果接口
+ 入表

每个步骤都封装了对应的 service ，通过 systemd 来管理，前三个步骤 通过 shell 脚本实现，每个 shell 封装为 service，第一个步骤通过定时任务 timer 触发，之后的三个步骤会监听前一个步骤的 日志文件，当日志文件发生变动时会触发 `service` 执行对应的 `shell` 脚本。第三个步骤完成时，会触发 service 使用 `docker` 启动一个临时容器任务进行数据更新入表。


### 定时任务配置

确保定时任务 `timer` 存活。 其他的全自动调用。

```bash
[root@ddzx_file ivr_ly]# systemctl status  get_recording.timer
● get_recording.timer - "每天凌晨执行一次"
   Loaded: loaded (/etc/systemd/system/get_recording.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Tue 2023-02-28 17:37:57 CST; 1 day 16h ago

Feb 28 17:37:57 ddzx_file systemd[1]: Started "每天凌晨执行一次".
```

查看 定时任务执行,下一次执行时间，基本信息, 下一次执行时间  `2023-03-28 07:30:00 ` , 执行的 service 是  `get_recording.service`
```bash
[root@ddzx_file ivr_ly]# systemctl list-timers get_recording.timer
NEXT                         LEFT     LAST PASSED UNIT                ACTIVATES
Tue 2023-03-28 07:30:00 CST  16h left n/a  n/a    get_recording.timer get_recording.service

1 timers listed.
Pass --all to see loaded but inactive timers, too.
[root@ddzx_file ivr_ly]#
```
定时任务配置文件，修改时间，直接从这里修改
```bash
[root@ddzx_file ivr_ly]# systemctl  cat  get_recording.timer
# /etc/systemd/system/get_recording.timer
[Unit]
Description="每天凌晨执行一次"

[Timer]
OnBootSec=3s
OnCalendar=*-*-* 07:30:00
Unit=get_recording.service

[Install]
WantedBy=multi-user.target
[root@ddzx_file ivr_ly]#

```



判断任务正常： 定时任务，和对应的 path 文件监听 unit 为运行状态。

```bash
[root@ddzx_file ivr_ly]# systemctl status  get_recording.timer
```
```bash
[root@ddzx_file ivr_ly]# systemctl  status change_get_recording.path
● change_get_recording.path - " 录音文件提取完成触发"
   Loaded: loaded (/etc/systemd/system/change_get_recording.path; enabled; vendor preset: disabled)
   Active: active (waiting) since Wed 2023-03-01 11:37:19 CST; 22h ago

Mar 01 11:37:19 ddzx_file systemd[1]: Started " 录音文件提取完成触发".

```
```bash
[root@ddzx_file ivr_ly]# systemctl status  change_parsing_recording.path
● change_parsing_recording.path - "调用解析接口完成触发"
   Loaded: loaded (/etc/systemd/system/change_parsing_recording.path; enabled; vendor preset: disabled)
   Active: active (waiting) since Wed 2023-03-01 11:37:37 CST; 22h ago

Mar 01 11:37:37 ddzx_file systemd[1]: Started "调用解析接口完成触发".
[root@ddzx_file ivr_ly]#

```





## 录音文件提取相关

提取录音文件脚本

```bash
#!/bin/bash

#@File    :   get_recording.sh
#@Time    :   2023/02/23 14:40:39
#@Author  :   Li Ruilong
#@Version :   1.0
#@Desc    :   提取录音文件
#@Contact :   1224965096@qq.com



STR_DATE=$(date -d last-day +%Y%m%d)
FTP_USER=mobile
FTP_PASS=UniApp_17


RECORD_CURL="ftp://136.255.249.30:21/$STR_DATE/"

HOME_DIR="/app/ivr_ly"
#RECORD_DIR="/app/ivr_ly"
RECORD_DIR="/app/ali_asr/v2.7.10service_202208hotfix/service/data/servicedata/nls-filetrans"


#  创建录音目录
if [ ! -d $RECORD_DIR/$STR_DATE/ ];then
   mkdir -p $RECORD_DIR/$STR_DATE/
fi

## 生成录音列表
curl -s -u $FTP_USER:$FTP_PASS ftp://136.255.249.30:21/$STR_DATE/ | awk '{if(length($NF) > 10)  print $NF }' \
     > $RECORD_DIR/$STR_DATE/${STR_DATE}_list.txt

echo -e " ========> 查询到录音文件数: $(cat  $RECORD_DIR/$STR_DATE/${STR_DATE}_list.txt | wc -l) \n"

## 通过 FTP 下载录音文件

for  FTP_URL in $(cat  $RECORD_DIR/$STR_DATE/${STR_DATE}_list.txt );do
   echo "curl  -s -u $FTP_USER:$FTP_PASS ${RECORD_CURL}${FTP_URL} -o  $RECORD_DIR/$STR_DATE/$FTP_URL"
   curl  -s -u $FTP_USER:$FTP_PASS ${RECORD_CURL}${FTP_URL} -o  $RECORD_DIR/$STR_DATE/$FTP_URL 
done

SUM=$(ls $RECORD_DIR/$STR_DATE/  | grep T | wc -l)
echo -e " ========> 下载录音文件数: $SUM  \n"
echo  " `date +%Y%m%d %H:%M` 提取录音文件完成,共提取录音文件 ：$SUM" >> $HOME_DIR/get_recording.log

```

get_recording.service ：执行提取录音的 Service  

```bash
# /etc/systemd/system/get_recording.service
[Unit]
Description= "获取录音文件"
After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/bash /app/ivr_ly/get_recording.sh

[Install]
WantedBy=multi-user.target
```

get_recording.timer 配置定时任务：每天午夜执行一次录音文件

```bash
# /etc/systemd/system/get_recording.timer
[Unit]
Description="每天凌晨执行一次"

[Timer]
OnBootSec=3s
OnCalendar=*-*-* 00:30:00
Unit=get_recording.service

[Install]
WantedBy=multi-user.target
```



## 解析录音文件相关


```bash
#!/bin/bash

#@File    :   parsing_recording.sh
#@Time    :   2023/02/23 17:11:25
#@Author  :   Li Ruilong
#@Version :   1.0
#@Desc    :   解析录音文件
#@Contact :   1224965096@qq.com



STR_DATE=$(date -d last-day +%Y%m%d)


HOME_DIR="/app/ivr_ly"

RECORD_DIR="/app/ali_asr/v2.7.10service_202208hotfix/service/data/servicedata/nls-filetrans"



touch $RECORD_DIR/$STR_DATE/${STR_DATE}_TASK_ID_MAPPER_list.txt
cat /dev/null > $RECORD_DIR/$STR_DATE/${STR_DATE}_TASK_ID_MAPPER_list.txt

## 遍历录音文件文件调用解析接口生成任务ID和文件名的映射列表
for  FTP_URL in $(cat  $RECORD_DIR/$STR_DATE/${STR_DATE}_list.txt );do
   RES=$(curl -s -H "Content-Type:application/json" -XPOST "http://10.218.31.165:8101/stream/v1/filetrans" -d '{ "token":"default", "appkey":"default",  "auto_split":false,  "file_link":"file:/home/admin/nls-filetrans/disk/'$STR_DATE'/'$FTP_URL' "}' )
   echo -e "解析返回响应：$RES \n"
   STATUS=$(echo $RES | awk -F','  '{print $3 }'| awk -F':' '{print $2}') #21050000
   if [ $STATUS == 21050000 ];then
      TASK_ID=$(echo $RES | awk -F','  '{print $4 }'| awk -F':' '{print $2}')
      TASK_ID=${TASK_ID%\"}
      TASK_ID=${TASK_ID#\"}
      # 写入映射关系
      echo -e "写入映射关系: 任务ID： $TASK_ID    |   文件名称：  $FTP_URL  \n"
      echo $TASK_ID@$FTP_URL >> $RECORD_DIR/$STR_DATE/${STR_DATE}_TASK_ID_MAPPER_list.txt 
   else
      echo -e " ========> 录音文件调用解析接口失败，请排查：文件位置：$RECORD_DIR/$STR_DATE/$FTP_URL \n 返回报文：$RES "
   fi   
    sleep 1
done


SUM=$(cat  $RECORD_DIR/$STR_DATE/${STR_DATE}_TASK_ID_MAPPER_list.txt  | wc -l)

echo -e " ========> 解析录音文件数: $SUM  \n"

echo  " `date +%Y%m%d%H%M` 解析录音文件接口调用完成,共解析录音文件 ：$SUM" >> $HOME_DIR/parsing_recording.log

```




path 单元监听提取日志文件变化，当提取完成日志写入时，触发解析 Service，运行解析脚本

```bash
# /etc/systemd/system/change_get_recording.path
[Unit]
Description="录音文件提取完成触发"
After=network-online.target

[Path]
Unit=parsing_recording.service
PathChanged=/app/ivr_ly/get_recording.log

[Install]
WantedBy=multi-user.target
```
解析 Service ，运行解析脚本，触发执行，也可以单独执行。parsing_recording


```bash
# /etc/systemd/system/parsing_recording.service
[Unit]
Description= "解析录音"
After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/bash /app/ivr_ly/parsing_recording.sh


[Install]
WantedBy=multi-user.target
```

## 提取解析结果相关

```bash
#!/bin/bash

#@File    :   get_parsing.sh
#@Time    :   2023/02/24 11:30:39
#@Author  :   Li Ruilong
#@Version :   1.0
#@Desc    :   获取解析结果
#@Contact :   1224965096@qq.com

# 睡眠一小时后执行，调用完解析接口会触发这shell，预留一小时时间解析，1小时后提取解析结果。
sleep 36

STR_DATE=$(date -d last-day +%Y%m%d)


HOME_DIR="/app/ivr_ly"

RECORD_DIR="/app/ali_asr/v2.7.10service_202208hotfix/service/data/servicedata/nls-filetrans"


# 解析文件存放位置
touch $RECORD_DIR/$STR_DATE/${STR_DATE}_ORDER_TEXT_list.txt
cat /dev/null > $RECORD_DIR/$STR_DATE/${STR_DATE}_ORDER_TEXT_list.txt


## 提取解析结果

for  TASK_ORDER in $(cat  $RECORD_DIR/$STR_DATE/${STR_DATE}_TASK_ID_MAPPER_list.txt  );do

   FILE_NAME=$(echo $TASK_ORDER | awk -F'@' '{print $2}')
   TASK_ID=$(echo $TASK_ORDER  | awk -F'@' '{print $1}')
   ORDER_CODE=$(echo $TASK_ORDER | awk  -F'@' '{print $2}'| awk -F'_' '{print $3 }')
   RES=$(curl -s -XGET "http://10.218.31.165:8101/stream/v1/filetrans?task_id=$TASK_ID")

   echo -e "获取解析结果返回响应：$RES \n"
   
   STATUS=$(echo $RES | awk -F',' '{print $2}'| awk -F':' '{print $2}') #21050000
   
   if [ $STATUS == 21050000 ];then 
      TEXT=${RES##*payload\":}
      TEXT=${TEXT%%,\"solve_time*}

      echo -e "写入订单号和解析文本: 订单号: $ORDER_CODE    |  文件名: $FILE_NAME  | 解析文本：  $TEXT  \n"
      echo $ORDER_CODE@$FILE_NAME@$TEXT >> $RECORD_DIR/$STR_DATE/${STR_DATE}_ORDER_TEXT_list.txt 

   else
      echo " ========> 解析文本获取失败，请排查：解析报文：$RES"
   fi   
   sleep 1
done

SUM=$(cat $RECORD_DIR/$STR_DATE/${STR_DATE}_ORDER_TEXT_list.txt  | wc -l)

echo -e " ========> 获取录音解析文本数: $SUM  \n"
echo  " `date +%Y%m%d%H%M` 录音解析文本获取完成,共提取文本数 ：$SUM" >> $HOME_DIR/get_parsing.log

```

创建对应的 get_parsing.service，提取录音结果

```bash
# /etc/systemd/system/get_parsing.service
[Unit]
Description= "提取解析结果"
After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/bash /app/ivr_ly/get_parsing.sh


[Install]
WantedBy=multi-user.target
```
创建触发事件，当调用解析接口完成后，写入日志，触发获取解析结果脚本服务

```bash
# /etc/systemd/system/change_parsing_recording.path
[Unit]
Description="调用解析接口完成触发"
After=network-online.target

[Path]
Unit=get_parsing.service
PathChanged=/app/ivr_ly/parsing_recording.log

[Install]
WantedBy=multi-user.target
```


### 数据入库

重做镜像，用不到
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$docker commit bcdd82ca5b48  myPython:latest
invalid reference format: repository name must be lowercase
┌──[root@vms100.liruilongs.github.io]-[~]
└─$docker commit bcdd82ca5b48  my-python:latest
sha256:cb7c9965c541dfc794f78eb06ae1c4af0c77bb87c92e5e6e768c7770eb61a5bb
┌──[root@vms100.liruilongs.github.io]-[~]
└─$docker save  my-python:latest -o ./my-python.tar
```

入表任务的容器运行方式

```bash
docker run --rm --network=host -v /app/ivr_ly/:/app/ivr_ly/ -v  /app/ali_asr/v2.7.10service_202208hotfix/service/data/servicedata/nls-filetrans:/app/ali_asr/v2.7.10service_202208hotfix/service/data/servicedata/nls-filetrans  my-python  python  /app/ivr_ly/ddzx/update.py
```


创建触发事件，当获取解析结果完成，写入日志，启动 dockers 临时容器 执行 pg 更新操作

```bash
# /etc/systemd/system/change_get_parsing.path
[Unit]
Description="获取解析结果完成触发"
After=network-online.target

[Path]
Unit=start_update_table.service
PathChanged=/app/ivr_ly/get_parsing.log

[Install]
WantedBy=multi-user.target
```

```bash
# /etc/systemd/system/start_update_table.service
[Unit]
Description= "提取解析结果"
After=network-online.target

[Service]
Type=oneshot
ExecStart=/usr/bin/docker run --rm --network=host -v /app/ivr_ly/:/app/ivr_ly/ -v  /app/ali_asr/v2.7.10service_202208hotfix/service/data/servicedata/nls-filetrans:/app/ali_asr/v2.7.10service_202208hotfix/service/data/servicedata/nls-filetrans  my-python  python  /app/ivr_ly/ddzx/update.py


[Install]
WantedBy=multi-user.target
```

### 数据入库相关代码

核心代码：逻辑非常简单

```bash
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
@File    :   update.py
@Time    :   2023/02/28 11:50:25
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   录音数据解析文本同步调度
"""

# here put the import lib


import yaml_util
import pg_units
import json
import time


# 数据库连接相关配置

conn =  pg_units.get_pg_db()
local_file_path = yaml_util.get_yaml_config()["local_file_path"]

data = set()
file_name_template = yaml_util.get_yaml_config()["file_name_template"]

# 获取前一天
TIME = time.strftime("%Y%m%d", time.localtime(time.time() - 86400 ))

## 更新数据准备
def to_date():
    # 拼接文件名
    abs_file_name =TIME +'_'+ file_name_template
    abs_file_name = local_file_path + TIME  +  "/" + abs_file_name
    print("读取文件：",abs_file_name)
    try:
    # 读取文件，生成批量更新的数据
        with open(abs_file_name, 'rt',encoding='utf-8') as f:
            print("读取文件成功.....")
            for line in f:
                line_str = line.split('@')
                # 每句话拼接
                sentences = json.loads(line_str[2])['payload']
                if 'sentences' in sentences:
                    texts = [i['text'] for i in sentences['sentences']]
                    texts_ = []
                    for index, char in enumerate(texts, start=1):
                        if index % 2 == 1:
                            char = 'A : ' + char;
                        else:
                            char = 'B : ' + char;
                        texts_.append(char)
                    data.add((line_str[0], '<br/>'.join(texts_)))
                else:
                    data.add((line_str[0], "录音没有声音"))
        print("生成入表数据",data)
    except Exception as  e:
        print("数据解析异常",e)

if  __name__ == "__main__":
    # 生成数据
    to_date()
    ## 调用 批量更新
    conn.update_db(data=data,table_name='ids_busi.ivr_call_customer',id_name="work_order_id",set_name="record_text")
```

