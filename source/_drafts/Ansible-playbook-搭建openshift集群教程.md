---
title: Ansible playbook 搭建OpenShift集群教程
tags:
  - OpenShift
categories:
  - OpenShift
toc: true
recommend: 1
keywords: OpenShift
uniqueId: '2022-06-05 04:49:31/Ansible playbook 搭建openshift集群教程.html'
mathJax: false
date: 2022-06-05 12:49:31
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***


