---
title: K8s：Kubernetes 面试问答速记
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-05-03 15:22:24/K8s：Kubernetes 面试问答速记.html'
mathJax: false
date: 2023-05-03 11:22:24
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***

+
+ 理解不足小伙伴帮忙指正

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

### 什么是 Kubernetes？

Kubernetes，通常缩写为 K8s，是一个开源`容器编排平台`，用于容器化应用程序的自动化部署、扩展和管理。它最初由`Google`开发，现在由云原生计算基金会（`CNCF`）维护。

通过Kubernetes，用户可以轻松地部署、管理和扩展容器化应用程序。Kubernetes将应用程序打包成一个或多个容器，然后在一组物理或虚拟服务器上部署这些容器。它提供了许多功能，如`负载均衡、自动伸缩、自我修复、自动部署`等，这些功能可以使应用程序更加可靠、高效和易于管理。

### 什么是容器？

容器是一种轻量级的虚拟化技术，它允许将应用程序及其所有依赖项打包到一个独立的、可移植的运行环境中，从而实现跨平台的应用程序开发和部署。

容器通常由以下几个组件构成：

+ 镜像（Image）：一个只读的模板，用于创建容器。
+ 容器（Container）：一个运行时的实例，基于镜像创建而来。
+ 仓库（Repository）：用于存储和共享镜像的位置。

与传统的虚拟化技术不同，容器不需要模拟整个操作系统的硬件层，而是利用主机操作系统的内核来提供进程隔离和资源管理

### Kubernetes 的主要组件是什么？

#### Master节点

+ `API Server`：提供了Kubernetes API的前端接口，用于与Kubernetes集群进行通信。
+ `etcd`：一个分布式键值存储数据库，用于存储Kubernetes集群的所有配置信息。
+ `Controller Manager`：控制器管理器，负责运行控制器（如副本控制器、节点控制器和服务控制器）来保证集群的状态符合用户定义的期望状态。
+ `Scheduler` ：调度器，负责将Pod调度到可用的Node节点上。

#### Node节点

+ `Kubelet`：每个Node节点上的代理服务，用于管理该节点上的Pod，并向Master节点汇报节点和Pod的状态。
+ `kube-proxy`：在每个Node节点上运行的网络代理，用于实现Kubernetes中的服务发现和负载均衡。
+ `容器运行时（如Docker）`：负责在容器内部启动应用程序进程并管理容器的生命周期。

#### 其他辅助组件

+ `DNS`：Kubernetes提供了自己的DNS服务器，用于为Kubernetes集群中的服务和Pod分配域名。
+ `Dashboard`：提供了一个Web界面，用于管理和监控Kubernetes集群。
+ `Ingress Controller`：用于将外部流量路由到Kubernetes集群中的服务。
+ `Volume插件`：用于提供持久化存储解决方案，使得容器可以在不同的Node节点之间迁移而不丢失数据。
+ `CNI插件`：用于实现Kubernetes集群中的网络管理和Pod间的通信。

### 什么是Pod？

在 Kubernetes 中，Pod 是最小的计算单位，是一组共同部署的容器集合。Pod 内的多个容器共享相同的网络命名空间（network namespace），存储资源和进程运行环境等，并且通常作为一个整体部署、调度和管理。

Pod 可以包含一个或多个紧密耦合的容器，这些容器可以共享文件系统、进程和网络命名空间等资源。当容器之间需要共享数据或协同工作时，使用 Pod 可以更加方便和灵活地进行管理和部署。例如，一个 Web 应用程序可能会使用一个 Nginx 容器作为 Web 服务器，另一个容器则用于运行应用程序后端的代码。

Pod 可以由多个人工配置的容器组成，也可以由 Init 容器构成。Init 容器是在主容器启动之前运行的预处理容器，它们可以执行初始化任务、配置环境变量和准备必要的资源等操作。

Kubernetes 使用 Pod 作为最小的部署单元，因为它具有以下优点：

+ 多容器支持：Pod 支持多个容器的部署，这些容器可以共享相同的 Namespace 和 Volume。
+ 网络隔离：每个 Pod 都分配了一个唯一的 IP 地址，这使得容器之间可以互相访问并同步工作。
+ 存储管理：Pod 可以使用 Volume 进行数据持久化，确保容器之间的数据不会丢失。
+ 共享资源：Pod 内共享计算和存储资源，这使得它们可以更好地进行利用和管理。

因此，Pod 是 Kubernetes 中最基本的部署单元，与容器紧密耦合，提供了一种灵活、可扩展和可靠的应用程序管理方式。

### 什么是 ReplicaSet？

ReplicaSet 是 Kubernetes 中的一种控制器（Controller），它用于确保指定数量的 Pod 副本正常运行。如果某个 Pod 发生故障或被删除，ReplicaSet 将自动创建新的 Pod 实例，以维持所需的副本数。

与 Replication Controller 相比，ReplicaSet 提供了更灵活和强大的功能，例如支持根据标签选择器来管理 Pod 实例、支持多维度的 Pod 伸缩、以及更新策略等。因此，在 Kubernetes 的早期版本中，ReplicaSet 已经取代了 Replication Controller 成为了 Kubernetes 中应用程序副本管理的主要工具。

ReplicaSet 可以使用 Deployment 对象进行部署和管理，这样可以简化应用程序的部署、升级和回滚等操作。Deployment 允许用户定义应用程序的期望状态，并在需要时自动创建、删除或更新 ReplicaSet 的副本数，从而实现了无缝的应用程序管理和更新方式。

在 ReplicaSet 中，用户可以通过以下几种方式来管理 Pod 的副本数：

+ 手动指定副本数：用户可以手动指定 ReplicaSet 中所需的 Pod 副本数。
+ 根据 CPU 或内存负载自动调整副本数：用户可以设置自动扩缩容策略，根据 Pod 资源使用情况自动调整 Pod 实例的副本数。
+ 根据标签选择器自动管理副本数：用户可以使用标签选择器来定义 Pod 副本的筛选条件，从而实现更细粒度的 Pod 管理。

### 什么是 Deployment ？

Deployment 是 Kubernetes 中最常用的控制器（Controller）之一，它用于管理 Pod 和 ReplicaSet 对象的自动化部署、扩展和更新。Deployment 允许用户声明应用程序期望的状态，并提供了一种无缝的方式来创建、删除或更新副本集。

Deployment 基于 ReplicaSet 实现，可以方便地指定所需的 Pod 副本数，并支持滚动更新、回滚操作等高级功能。使用 Deployment 可以确保应用程序在 Kuberentes 集群中以恰当的方式进行部署和运行，从而实现了应用程序的自动化管理。

Deployment 的主要优点包括：

+ 简单易用：Deployment 提供了一种简单且易于使用的方式来定义应用程序的期望状态，并自动创建、删除或更新副本集。
+ 应用程序升级：通过 Deployment 可以实现无缝的应用程序更新和回滚操作，从而确保应用程序始终处于最新状态。
+ 滚动更新：Deployment 支持滚动更新，即在不中断服务的情况下逐步替换 Pod 副本。
+ 自动伸缩：Deployment 可以与自动扩缩容机制配合使用，根据资源使用情况自动调整 Pod 的副本数。
+ 健康检查：Deployment 可以通过健康检查来监测 Pod 的状态，并在 Pod 出现故障或不健康时自动进行调整。

### 什么是 Service？

在 Kubernetes 中，Service 是一种抽象的逻辑概念，用于定义一组 Pod 实例的访问方式。它为应用程序提供了一个稳定的 IP 地址和 DNS 名称，并支持负载均衡和服务发现等功能。

Service 通过将多个 Pod 实例封装在一个抽象的逻辑单元中来实现负载均衡，这些 Pod 实例可以位于不同的节点上。当 Service 收到请求时，它会自动将请求路由到可用的 Pod 实例中，从而实现了对后端 Pod 的透明负载均衡。

Service 还通过标签选择器实现了服务发现的功能，它可以根据特定的标签选择器查询所有与之匹配的 Pod 实例，并自动更新其访问地址和负载均衡策略等信息。这使得应用程序可以方便地进行扩展、升级和迁移等操作，而无需修改客户端应用程序或配置文件。

Service 可以绑定到一个固定的 IP 地址或 DNS 名称，并且可以跨命名空间进行访问控制。另外，Service 还支持多种负载均衡模式，包括 Round Robin、Session Affinity 和 External Load Balancer 等。用户还可以通过设置 Service 的类型来决定其暴露方式，如 ClusterIP、NodePort 和 LoadBalancer 等。

### 什么是  ConfigMaps and Secrets？

在 Kubernetes 中，ConfigMaps 和 Secrets 是两种重要的资源对象，用于管理应用程序需要的配置信息和敏感数据。它们提供了一种集中式、灵活和安全的方式来管理和分发这些数据，以确保应用程序能够以正确的方式工作。

#### ConfigMaps
ConfigMaps 是 Kubernetes 中的一种资源对象，用于存储应用程序所需的配置信息（如环境变量、配置文件等）。ConfigMaps 可以通过 YAML 文件或 kubectl 命令创建和管理，用户可以将其挂载到 Pod 的容器中，并让应用程序从其中获取配置信息。

在使用 ConfigMaps 时，用户可以采用以下两种方式：

+ 直接从 ConfigMap 获取配置信息：Pod 容器可以直接从 ConfigMap 中获取配置信息，不需要将其写入容器内部的文件系统。
+ 将 ConfigMap 映射到文件系统：Pod 容器可以将 ConfigMap 映射到容器内部的文件系统中，从而使得容器内的程序可以像本地文件一样访问配置信息。

#### Secrets
Secrets 是 Kubernetes 中的另一种资源对象，用于存储应用程序需要的敏感数据，例如密码、证书和 API 密钥等。与 ConfigMaps 类似，Secrets 也可以通过 YAML 文件或 kubectl 命令进行创建和管理，用户可以将其挂载到 Pod 的容器中，并让应用程序从其中获取敏感数据。

Secrets 的主要优势在于它们能够加密和保护应用程序所需的敏感数据，并且只有授权的用户才能访问它们。Secrets 可以使用多种加密方式（如 Base64 编码、TLS、SSH 等），从而确保敏感数据不会暴露给未经授权的用户。

在使用 Secrets 时，用户可以采用以下两种方式：

+ 直接从 Secret 获取敏感数据：Pod 容器可以直接从 Secret 中获取敏感数据，不需要将其写入容器内部的文件系统。
+ 将 Secret 映射到文件系统：Pod 容器可以将 Secret 映射到容器内部的文件系统中，从而使得容器内的程序可以像本地文件一样访问敏感数据。

### 什么是Ingress？

Ingress 是一种资源对象，用于管理应用程序的入口流量，并将其路由到正确的后端服务。它提供了一种灵活、可扩展和安全的方式来控制应用程序的网络流量。

通常情况下，Kubernetes 集群中的服务只能通过 ClusterIP 或 NodePort 的方式访问，而 Ingress 则允许用户定义 HTTP 和 HTTPS 流量的路由规则，并将其映射到 Service 对象上。Ingress 可以根据不同的 URL 路径或主机名来路由请求，并支持多种负载均衡算法，如轮询、IP 均衡等。

使用 Ingress 可以实现以下几个目标：

+ 实现域名绑定：可以为不同的域名设置不同的路由规则，从而实现灵活的域名绑定。
+ 实现路径匹配：可以将不同的 URL 路径映射到不同的后端服务上，从而实现更加细粒度的流量控制。
+ 支持 SSL/TLS 加密：可以通过 Ingress 提供的 TLS 证书管理机制，为服务提供 SSL/TLS 加密功能，从而保护数据传输的安全性。
+ 实现流量控制：可以通过 QoS（Quality of Service）策略设置流量限制和流量转发规则，从而实现更加灵活的流量控制。

### 什么是StatefulSets？

StatefulSets 是用于管理有状态应用程序的 Kubernetes 对象，确保每个 Pod 都有一个唯一且稳定的主机名，如 web-0、web-1 等。它们还提供有关 Pod 的顺序和唯一性的保证。

### 什么是DaemonSets？

DaemonSets 是 Kubernetes 对象，用于确保特定 Pod 的副本在集群中的所有（或某些）节点上运行，通常用于系统级服务，如日志收集器或监控代理。

### 什么是 Kubernetes API？

Kubernetes API 是与 Kubernetes 集群通信和管理的主要接口。它公开了一个用于创建、更新和删除 Kubernetes 对象的 RESTful 接口。

### 什么是kubectl？

Kubectl 是用于与 Kubernetes API 服务器交互的命令行工具，允许您管理 Kubernetes 资源。

### 什么是labels 和selectors？

标签是附加到 Kubernetes 对象的键值对，用于组织和选择对象的子集。选择器是根据对象的标签匹配对象的查询，使您能够对特定对象组进行筛选和执行操作。

### 什么是命名空间？

 命名空间是一种在多个用户或团队之间划分群集资源的方法。它们提供名称的作用域，允许您在不同的命名空间中具有多个具有相同名称的对象。

### 什么是水平容器自动缩放程序 （HPA）？

 水平 Pod 自动缩放程序 （HPA） 是一个 Kubernetes 组件，可根据观察到的 CPU 或自定义指标利用率自动缩放部署或副本集中的 Pod 数量。

### 什么是垂直容器自动缩放程序 （VPA）？

垂直 Pod 自动缩放器 （VPA） 是一个 Kubernetes 组件，可根据 Pod 的实际资源使用情况自动调整分配给 Pod 的 CPU 和内存资源。

### 什么是头盔？

 Helm 是 Kubernetes 的包管理器，允许您使用 Helm 图表定义、安装和管理 Kubernetes 应用程序，这些图表是版本控制的预配置应用程序包。

### 什么是自定义资源定义 （CRD）？

 自定义资源定义 （CRD） 允许您在 Kubernetes 中定义和管理自定义资源，使用针对您的特定用例量身定制的新 API 对象扩展其功能。

### 什么是 Kubernetes Operator？

Kubernetes 运算符是一种使用自定义资源和自定义控制器扩展 Kubernetes 功能的方法。操作员定义自定义资源并包含用于管理其生命周期的逻辑，通常对有关应用程序的特定于域的知识进行编码。

### 什么是 RBAC（基于角色的访问控制）？

RBAC 是 Kubernetes 中的一项安全功能，允许您根据用户角色定义和实施资源的访问策略。它使用 Role 和 ClusterRole 对象来定义权限，并使用 RoleBinding 和 ClusterRoleBinding 对象向用户或组授予这些权限。

### 什么是永久性卷 （PV） 和永久性卷声明 （PVC）？

 持久卷 （PV） 是集群中存储资源的 Kubernetes 抽象，而持久卷声明 （PVC） 是用户对存储资源的请求。PV 和 PVC 允许您独立于 Pod 及其生命周期来管理存储资源。

### 什么是 Kubernetes 网络策略？

Kubernetes 网络策略是一项安全功能，允许您定义和实施集群内 Pod 通信的规则。它使您能够根据标签和选择器控制单个 Pod 或 Pod 组的入口和出口流量。

### 什么是 Kubernetes 集群？

Kubernetes 集群是一组机器或节点，它们协同工作以管理和运行容器化应用程序。它由控制平面组件和工作器节点组成，为部署、扩展和管理容器提供了统一的平台。

### 什么是节点亲和力和反亲和力？

 节点亲和力和反亲和力是影响如何将 Pod 调度到集群中的节点上的规则。亲和力规则鼓励在具有特定特征的节点上调度 Pod，而反亲和性规则则不鼓励在具有特定特征的节点上调度 Pod。

### 什么是污点和容忍？

 污点是应用于节点的属性，表示节点不应接受某些 Pod，而容忍是应用于 Pod 的属性，允许它们在受污染的节点上调度。这种机制确保 Pod 不会调度在不适当的节点上。

### 什么是就绪情况探测？

 就绪探测是一种诊断检查，用于确定容器是否已准备好为流量提供服务。如果容器未通过就绪探测，Pod 在通过检查之前不会从服务接收流量。

### 什么是活体探针？

存活探测是一种诊断检查，用于确定容器是否正常运行。如果容器无法通过活动探测，Kubernetes 将重新启动容器，尝试解决问题。

### 什么是CronJob？

 CronJob 是一个 Kubernetes 对象，它允许您基于 cron 表达式按计划运行特定作业。它对于备份、报告生成或其他定期任务等任务非常有用。

### 什么是工作？

 Job 是一个 Kubernetes 对象，它代表一个运行一个或多个 Pod 来完成的有限任务。作业对于运行批处理或其他需要运行完成的任务（而不是持续运行服务）非常有用。

### 什么是滚动更新？

滚动更新是一种部署策略，它以增量方式更新部署或 StatefulSet 的 Pod，对应用程序可用性的影响最小。它会逐渐用新 Pod 替换旧 Pod，确保在更新过程中始终有指定数量的副本可用。

### 什么是金丝雀部署？

金丝雀部署是一种部署策略，涉及在稳定版本旁边部署应用程序的新版本，将一小部分流量定向到新版本。这允许您在逐步向所有用户推出新版本之前对其进行测试和验证。

### 什么是蓝绿部署？

蓝绿部署是一种部署策略，涉及运行两个具有相同配置的独立环境“蓝色”和“绿色”。部署新版本时，新版本将部署到非活动环境，经过测试和验证后，流量将切换到新环境。

### 什么是有状态集无外设服务？

无头服务是没有 ClusterIP 的服务，用于由 StatefulSet 管理的有状态应用程序。它允许每个 Pod 拥有自己的 DNS 主机名，从而实现直接的 Pod 到 Pod 通信，而无需依赖单个 ClusterIP。

### 什么是 Kubernetes Federation？

Kubernetes Federation 是一项功能，允许您跨多个 Kubernetes 集群同步和管理资源。它使您能够创建单个统一的控制平面，用于管理部署在多个集群中的应用程序。

### 什么是 Kubernetes StorageClass？

StorageClass 是一个 Kubernetes 对象，用于定义集群中可用的存储类型。它允许管理员定义具有不同性能和成本特征的不同存储类别，使用户能够请求满足其特定需求的存储。

### 什么是 Kubernetes Volume？

Kubernetes 卷是 Pod 中的容器可访问的目录。卷使数据能够在容器之间共享，或在容器的生存期之后保留数据。

### 什么是 Kubernetes 中的容器资源管理？

Kubernetes 中的容器资源管理是指管理 Pod 内容器分配 CPU、内存和其他资源的过程。通过设置资源请求和限制，您可以确保容器具有必要的资源以最佳方式运行，同时防止资源匮乏或过度分配。

### 什么是初始化容器？

 Init 容器是在 Pod 中的主容器之前运行的特殊用途容器。它们通常用于执行设置任务，例如下载依赖项、配置环境或在主应用程序启动之前验证配置数据。

### 什么是 Kubernetes 服务帐户？

 服务帐户是一个 Kubernetes 对象，表示在 Pod 中运行的进程的标识。服务帐户可用于为访问 Kubernetes API 或群集中的其他资源提供身份验证和授权。

### 什么是 Kubernetes 入口控制器？

Kubernetes 入口控制器是一个组件，用于根据入口规则管理将外部流量路由到集群内的服务。它监视入口资源，并相应地更新基础负载均衡器或代理配置。

### 什么是 Pod 中断预算 （PDB）？

Pod 中断预算 （PDB） 是一个 Kubernetes 对象，它限制可以从副本集、部署或 StatefulSet 中自愿逐出的 Pod 数量，确保维护操作（如节点升级或扩展事件）期间的高可用性。

### 什么是 kube-proxy？

Kube-proxy 是一个网络代理，运行在 Kubernetes 集群中的每个节点上，负责维护网络规则，促进服务的服务发现和负载均衡。

### 什么是库贝莱特？

 Kubelet 是一个在 Kubernetes 集群中的每个工作节点上运行的代理，负责确保 Pod 中的容器运行和健康，并与控制平面进行通信。

### 什么是蚀刻？

Etcd 是 Kubernetes 用来存储集群配置数据的分布式、一致的键值存储，充当控制平面组件的主数据存储。

### 什么是 Kubernetes 注释？

 Kubernetes 注释是附加到对象的键值对，可用于存储任意的非识别元数据。与标签不同，批注不用于选择对象，但可用于存储有关对象的其他信息，例如说明或时间戳。

### 什么是 Kubernetes 自我修复系统？

Kubernetes 自我修复系统是指自动检测和解决集群内问题的内置机制，例如重启失败的容器、在故障节点上重新调度 Pod，或者根据资源使用情况扩展应用程序。

### 什么是 Kubernetes 持久存储？

 Kubernetes 持久存储是指可用于存储需要在容器或 Pod 的生命周期之后持久保存的数据的各种存储解决方案。这包括永久性卷、永久性卷声明和存储类，它们允许您以一致且高效的方式管理和预配存储资源。

### 什么是 Kubernetes 准入控制器？

Kubernetes 准入控制器是一个组件，用于在对象持久性阶段之前拦截对 Kubernetes API 服务器的请求，使您能够根据自定义策略或业务逻辑验证或修改对象。

### 什么是 Kubernetes Autoscaling？

Kubernetes 自动伸缩是指根据应用需求或集群利用率自动调整 Pod、节点或资源数量的过程。这包括水平容器自动缩放程序 （HPA）、垂直容器自动缩放程序 （VPA） 和群集自动缩放程序 （CA）。

### 什么是 Kubernetes API 组？

Kubernetes API 组是一起进行版本控制的相关 API 资源的集合。API 组通过允许添加新资源或版本而不影响现有资源来帮助组织和发展 Kubernetes API。

### 什么是 Kubernetes API 资源？

 Kubernetes API 资源是表示 Kubernetes 系统一部分的对象，例如 Pod、Service 或 Deployment。这些资源可以通过 Kubernetes API 创建、更新和删除。

### 什么是 Kubernetes API 版本？

Kubernetes API 版本是一组相关资源的特定 API 版本。它表示 API 的稳定性和支持级别，alpha、beta 和稳定版本代表不同的成熟度级别。

### 什么是 Kubernetes 控制循环？

 Kubernetes 控制循环是持续确保系统所需状态得到维护的基本机制。控制器监视当前状态并根据需要进行更改以实现所需状态，例如创建、更新或删除资源。

### 什么是 kube-scheduler？

Kube-scheduler 是一个控制平面组件，负责根据资源可用性、节点亲和力、污点和容忍度等各种因素将 Pod 分配给节点。

### 什么是 Kubernetes API Server？

Kubernetes API Server 是公开 Kubernetes API 的 Kubernetes 控制平面的核心组件。它处理 RESTful API 请求，验证它们，并在 etcd 中更新相应的对象。

### 什么是 Kubernetes Controller Manager？

Kubernetes 控制器管理器是一个控制平面组件，用于管理核心控制循环，包括复制控制器、端点控制器和命名空间控制器。

### 什么是容器网络接口 （CNI）？

 容器网络接口 （CNI） 是用于在 Linux 容器中配置网络接口的规范和库集。Kubernetes 使用 CNI 兼容的插件来配置 Pod 网络。

### 什么是 Kubernetes Sidecar 模式？

 Kubernetes Sidecar 模式涉及在 Pod 中的主容器旁边部署一个额外的容器。挎斗容器通常扩展或增强主容器的功能，例如日志转发、监视或数据处理。

### 什么是 Kubernetes 大使模式？

Kubernetes 大使模式涉及部署一个容器，该容器充当 Pod 中主容器的代理或适配器。此模式简化了与外部系统的通信，或提供了用于访问不同服务的统一接口。

### 什么是 Kubernetes 适配器模式？

 Kubernetes 适配器模式涉及部署一个容器，该容器在 Pod 中转换或修改主容器的接口。此模式使主容器能够与其他系统或 API 一起使用，而无需修改其代码。

### 什么是向下 API？

 向下 API 是一种机制，允许 Pod 将有关自身的信息（例如它们的 Pod 名称、命名空间和标签）公开为环境变量或文件。

### 什么是 Kubernetes NodeSelector？

 节点选择器是 Pod 规范中的一个字段，它允许您根据节点的标签指定应该调度 Pod 的节点的所需特征。

### 什么是 Kubernetes 容器运行时接口 （CRI）？

Kubernetes 容器运行时接口 （CRI） 是一个插件接口，允许 Kubernetes 使用不同的容器运行时，例如 Docker、containerd 或 CRI-O，而无需修改 kubelet 代码。

### 什么是 Kubernetes DaemonSet？

Kubernetes DaemonSet 是一个更高级别的抽象，它确保特定的 Pod 在集群中的所有或部分节点上运行。守护程序集通常用于部署系统级服务，例如日志收集器、监视代理程序或网络代理。

### 什么是 Kubernetes ConfigMap？

Kubernetes ConfigMap 是一个对象，允许您将非敏感配置数据存储在键值对中，Pods 可以将这些数据用作环境变量、命令行参数或作为文件挂载到卷中。

### 什么是 Kubernetes Secret？

Kubernetes 密钥是一个对象，允许您存储敏感数据，例如凭据、令牌或密钥，Pods 可以将这些数据用作环境变量或作为卷中的文件挂载。

### 什么是 Kubernetes 对象管理模型？

Kubernetes 对象管理模型是一种用于管理资源的声明性方法，您可以在其中使用 YAML 或 JSON 清单定义系统的所需状态，控制平面通过协调循环实现和维护该状态。

### 什么是 Kubernetes 垃圾收集？

 Kubernetes 垃圾回收是一种自动删除未使用或孤立资源（例如终止的 Pod、未使用的 ConfigMap 或已完成的作业）的机制，以释放系统资源并保持干净的集群。

### 什么是 Kubernetes 审计日志？

 Kubernetes 审核日志是 Kubernetes API 服务器中发生的事件的记录，提供有关请求、响应和元数据的详细信息。它可用于安全监视、故障排除或合规性目的。

### 什么是 Kubernetes Cloud Controller Manager？

 Kubernetes 云控制器管理器是一个控制平面组件，它嵌入了特定于云的控制逻辑，例如管理节点生命周期、配置存储卷或配置负载均衡器。它允许 Kubernetes 以一致和可扩展的方式与各种云提供商进行交互。

### 什么是 Kubernetes ReplicaSet？

Kubernetes ReplicaSet 是一个更高级别的抽象，它确保在任何给定时间运行指定数量的 Pod 副本。它取代了较旧的复制控制器，并由部署用于管理 Pod 扩展和更新。

### 什么是 Kubernetes PodSecurityPolicy？

Kubernetes PodSecurityPolicy 是一个集群级资源，它定义了创建和更新 Pod 的安全约束。它允许您强制实施最佳实践，例如禁止特权容器、限制主机访问或限制使用特定卷类型。

## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)

***
<https://medium.com/@bubu.tripathy/top-75-kubernetes-questions-and-answers-d677a0b87d79>

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
