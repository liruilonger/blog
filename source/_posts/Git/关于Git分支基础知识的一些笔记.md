---
title: 关于Git分支基础知识的一些笔记
tags:
  - Git
categories:
  - Git
toc: true
recommend: 1
keywords: Git
uniqueId: '2022-07-26 15:46:58/关于Git分支基础知识的一些笔记.html'
mathJax: false
date: 2022-07-26 23:46:58
thumbnail:
---




**<font color="009688"> 躺平是一种积极的生活态度 -----山河已无恙**</font>

<!-- more -->
## 写在前面
+ 今天和小伙伴们分享一些Git分支的笔记
+ 学习的原因，关于Git分支之前简单看了下，就直接开始玩了，结果整不明白，乱七八糟
+ 看着很糟心，所以觉得有必要系统的学习下
+ 博文为`《Pro Git》`读书笔记整理，基本上是书里的知识
+ 感谢开源这本书的作者和把这本书翻译为中文的大佬们
+ 理解不足小伙伴帮忙指正，书很不错，感兴趣小伙伴可以去拜读下



**<font color="009688"> 躺平是一种积极的生活态度 -----山河已无恙**</font>

***





# Git 分支

关于Git分支管理的一些建议，一般可以在本地解决的问题要在本地解决，本地合并(要申请合并到的远程分支)，本地解决冲突，如果自己的分支，只顾着开发，不做合并或者变基的操作，在自己的分支上越走越远，慢慢的和远程主主分支差的太远，申请合并一堆冲突...，那是一件很糟糕的事，尤其是对代码的审核者而言，会认为申请合并者是一个不会使用的Git的开发者。
![在这里插入图片描述](https://img-blog.csdnimg.cn/bc5449730f79491eaaeaf458e6216b29.png)
在学习Git分支之前，我们来了解一些理论知识

## 分支理论

Git 保存的不是`文件`的`变化或者差异`，而是一系列不同`时刻的快照`。

在进行`提交`操作时，`Git`会保存一个`提交对象(commit object)`。包括：
+ 一个指向`暂存内容快照`的`指针`
+ 作者的`姓名`和`邮箱`
+ 提交时`输入`的信息
+ 指向它的`父对象`的`指针`

`首次提交`产生的提交对象没有`父对象`，`普通提交`操作产生的提交对象有一个`父对象`，而由`多个分支合并产生的提交对象`有`多个父对象`

假设现在有一个工作目录，里面包含了三个将要被`暂存(add)和提交(commit)的文件`。暂存操作会为`每一个文件`计算`校验和`(使用SHA-1哈希算法)，然后会把当前版本的`文件快照保存到Git仓库中(Git使用blob对象来保存它们)`，最终将`校验和`加入到`暂存区域`等待`提交`：


当使用`git commit `进行提交操作时，Git会先计算每一个`子目录(本例中只有项目根目录)的校验和`，然后在 Git 仓库中这些校验和保存为树对象。随后，Git 便会创建一个提交对象，它除了包含上面提到的那些信息外，还包含指向这个树对象(项目根目录)的指针。如此一来，Git就可以在需要的时候重现此次保存的快照。

现在，Git仓库中有五个对象：`三个blob对象(保存着文件快照)`、`一个树对象(记录着目录结构和blob对象索引)`以及`一个提交对象(包含着指向前述树对象的指针和所有提交信息)`。

![](https://img-blog.csdnimg.cn/a4490ca65e474ed197b55763e6435530.png)



做些修改后再次提交，那么这次产生的`提交对象`会包含一个指向`上次提交对象(父对象)的指针`。

![](https://img-blog.csdnimg.cn/d8b72b2d967e4528a9196d5bb6e8dc17.png)


`Git的分支，其实本质上仅仅是指向提交对象的可变指针`。Git的默认分支名字是`master`。在多次提交操作之后，你其实已经有一个指向最后那个提交对象的`master分支`。`master分支`会在每次提交时自动向前移动。

`Git的master分支`并不是一个特殊分支。它就跟其它分支完全没有区别。之所以几乎每一个仓库都有`master分支`，是因为`git init`命令默认创建它，并且大多数人都懒得去改动

```bash
PS E:\docker\git_example> git init
Initialized empty Git repository in E:/docker/git_example/.git/
PS E:\docker\git_example> git status
On branch master
.........
```

![](https://img-blog.csdnimg.cn/76afb07cf72b4bca9ca56901d9480bcf.png)



## 分支创建

**Git 是怎么创建新分支的呢？**

很简单，它只是`为你创建了一个可以移动的新的指针`。在当前所在的`提交对象`上创建一个指针。比如，创建一个testing分支，你需要使用`git branch`命令：

```bash
git branch testing
```
![](https://img-blog.csdnimg.cn/aefff249844744139e98d50e31bbc7d9.png)

**Git 又是怎么知道当前在哪一个分支上呢？** 

也很简单，它有一个名为` HEAD 的特殊指针`。在Git中，它是一个指针，`指向当前所在的本地分支`(译注：将HEAD想象为当前分支的别名)。`git branch`命令仅仅创建一个新分支，并不会自动切换到新分支中去。

![](https://img-blog.csdnimg.cn/a7fbf3493de74c78b8bb7c316b9fced4.png)

你可以简单地使用` git log `命令查看各个分支当前所指的对象。 提供这一功能的参数是` --decorate`。

![](https://img-blog.csdnimg.cn/f2437111aeeb496e83db2a91468cf2b4.png)

```bash
$ git log --oneline --decorate
f30ab (HEAD -> master, testing) add feature #32 - ability to add new
formats to the central interface
34ac2 Fixed bug #1328 - stack overflow under certain conditions
98ca9 The initial commit of my project
```
![](https://img-blog.csdnimg.cn/fb4d5d14afc7408c9934add08395cea8.png)

## 分支切换

要切换到一个已存在的分支，你需要使用` git checkout `命令。 我们现在切换到新创建的 testing 分支去：这样 HEAD 就指向 testing 分支了。

```
$ git checkout testing
```
![](https://img-blog.csdnimg.cn/0b7deece082641d89b63df9ad319dad3.png)


在切换分支之后重新提交，新的提交会提交到新的分支

```bash
$ vim test.rb
$ git commit -a -m 'made a change'
```

你的`testing分支`向前移动了，但是`master分支`却没有，它仍然指向运行`git checkout`时所指的对象。


![](https://img-blog.csdnimg.cn/2ecefa0b11ad4cf88b023f314772ee6c.png)

现在我们切换回master分支看看：

```bash
$ git checkout master
```


![](https://img-blog.csdnimg.cn/05fbd7a7183a4b67a680c59f5f443fde.png)

这条命令做了两件事。

+ 一是使`HEAD指回master分支`
+ 二是将`工作目录恢复成master分支所指向的快照内容`

也就是说，你现在做修改的话，项目将始于一个`较旧的版本`。本质上来讲，这就是忽略testing分支所做的修改，以便于向另一个方向进行开发。


需要注意的是，分支切换会改变你工作目录中的文件，

在切换分支时，一定要注意你工作目录里的文件会被改变。即便有被跟踪但是没有提交的文件会被自动覆盖掉，如果是切换到一个较旧的分支，你的工作目录会恢复到该分支最后一次提交时的样子。


**如果Git不能干净利落地完成这个任务，它将禁止切换分支。**


```
$ vim test.rb
$ git commit -a -m 'made other changes'
```
现在，这个项目的提交历史已经产生了分叉，因为刚才你创建了一个新分支，并切换过去进行了一些工作，随后又切换回master分支进行了另外一些工作。上述两次改动针对的是不同分支：

你可以在不同分支间不断地来回切换和工作，并在时机成熟时将它们合并起来。而所有这些工作，你需要的命令只有`branch、checkout和commit。`

![](https://img-blog.csdnimg.cn/86638918e0a24e93a7e35936b0ea60f8.png)

你可以简单地使用git log命令查看分叉历史。运行`git log --oneline  --decorate  --graph  --al1`，它会输出你的提交历史、各个分支的指向以及项目的分支分叉情况。

![](https://img-blog.csdnimg.cn/bc852cc915d94e919d1a294041379f4f.png)


由于Git的分支实质上仅是`包含所指对象校验和(长度为40的SHA-1值字符串)的文件`，所以它的创建和销毁都异常高效。创建一个新分支就相当于`往一个文件中写入41个字节(40个字符和1个换行符)`，如此的简单能不快吗？


创建新分支的同时切换过去

通常我们会在创建一个新分支后立即切换过去，这可以用` git checkout -b  <newbranchname>` 一条命令搞定。

来看一个Demo 

## 分支的新建与合并


1. 开发某个网站。
2. 为实现某个新的用户需求，创建一个分支。
3. 在这个分支上开展工作。

正在此时，你突然接到一个电话说有个很严重的问题需要紧急修补。 你将按照如下方式来处理：

1. 切换到你的线上分支(production branch)。
2. 为这个紧急任务新建一个分支，并在其中修复它。
3. 在测试通过之后，切换回线上分支，然后合并这个修补分支，最后将改动推送到线上分支。
4. 切换回你最初工作的分支上，继续工作


### 新建分支

首先，我们假设你正在你的项目上工作，并且在 master 分支上已经有了一些提交。

![](https://img-blog.csdnimg.cn/75d19a882078471ab6c13fc8ab5de490.png)

现在，你已经决定要解决你的公司使用的问题追踪系统中的`#53问题`。想要新建一个分支并同时切换到那个分支上，你可以运行一个带有`-b`参数的`git checkout`命令：

```bash
$ git checkout -b iss53
Switched to a new branch "iss53"
```
你继续在` #53 `问题上工作，并且做了一些提交。 在此过程中，iss53 分支在不断的向前推进，因为你已经检出到该分支 (也就是说，你的 HEAD 指针指向了 iss53 分支)
```
$ vim index.html
$ git commit -a -m 'added a new footer [issue 53]'

```

![](https://img-blog.csdnimg.cn/3cf4906a64b9415bb8456b04b685e136.png)


现在你接到那个电话，有个紧急问题等待你来解决。有了Git的帮助，你不必把这个紧急问题和iss53的修改混在一起，你也不需要花大力气来还原关于`53#问题`的修改，然后再添加关于这个紧急问题的修改，最后将这个修改提交到线上分支。你所要做的仅仅是`切换回master分支`。

但是，在你这么做`之前`，要留意你的`工作目录`和`暂存区`里那些还没有`被提交的修改`，它可能会和你即将`检出的分支产生冲突`从而`阻止Git切换到该分支`。最好的方法是，在你切换分支之前，保持好一个干净的状态。

有一些方法可以绕过这个问题(即，`暂存(stashing)和清理(clean)`)。现在，我们假设你已经把你的`修改全部提交`了，这时你可以切换回`master`分支了：

这里需要注意的是一定要切回master分支之后在新建分支，不要在iss53的分支上新建分支。
```
$ git checkout master
Switched to branch 'master'
```
这个时候，你的工作目录和你在`开始#53问题之前`一模一样，现在你可以专心修复紧急问题了。请牢记：`当你切换分支的时候，Git会重置你的工作目录，使其看起来像回到了你在那个分支上最后一次提交的样子`。Git会自动添加、删除、修改文件以确保此时你的工作目录和这个分支最后一次提交时的样子一模一样。



接下来，你要修复这个紧急问题。我们来建立一个`hotfix分支`，在该分支上工作直到问题解决：
```
$ git checkout -b hotfix
Switched to a new branch 'hotfix'
$ vim index.html
$ git commit -a -m 'fixed the broken email address'
[hotfix 1fb7853] fixed the broken email address
 1 file changed, 2 insertions(+)
```

![](https://img-blog.csdnimg.cn/760ac4dade3f4ed4b8dc40e07907bf9a.png)

你可以运行你的测试，确保你的修改是正确的，然后将`hotfix分支合并回你的master分支`来部署到线上。

这里需要注意，在分支合并的时候，当前分支是合并后的分支，被合并的分支为`git merge xxx` 指定的分支。要把B合并到A，那么当前分支应该为A，使用`git merge B`的命令合并

你可以使用git merge命令来达到上述目的：

```bash
$ git checkout master
$ git merge hotfix
Updating f42c576..3a0874c
Fast-forward
 index.html | 2 ++
 1 file changed, 2 insertions(+)
```

**Fast-forward** :在合并的时候，你应该注意到了“快进(fast-forward)”这个词。由于你想要合并的分支hotfix所指向的提交C4是你所在的提交C2的直接后继，因此Git会直接将指针向前移动。

换句话说，当你`试图合并两个分支时，如果顺着一个分支走下去能够到达另一个分支`，那么Git在合并两者的时候，只会简单的`将指针向前推进(指针右移)`，因为这种情况下的合并操作`没有需要解决的分歧`——这就叫做“`快进(fast-forward)`”。


现在，最新的修改已经在master分支所指向的提交快照中，你可以着手发布该修复了。
![](https://img-blog.csdnimg.cn/982b8fd635d540a78903ccea66aaa5e0.png)

关于这个紧急问题的解决方案发布之后，你准备回到被打断之前时的工作中。然而，你应该先`删除hotfix分支`，因为你已经不再需要它了,它master分支已经指向了同一个位置。你可以使用带`-d选项的git branch命令来删除分支`：

```bash
$ git branch -d hotfix
Deleted branch hotfix (3a0874c).
```
现在你可以切换回你正在工作的分支继续你的工作，也就是针对 #53 问题的那个分支(iss53 分支)。

```bash
$ git checkout iss53
Switched to branch "iss53"
$ vim index.html
$ git commit -a -m 'finished the new footer [issue 53]'
[iss53 ad82d7a] finished the new footer [issue 53]
1 file changed, 1 insertion(+)
```
![](https://img-blog.csdnimg.cn/4e1fd5f28c23435787f544d8be0f08e4.png)


你在`hotfix`分支上所做的工作并没有包含到`iss53分支`中。如果你需要`拉取hotfix所做的修改`，

你可以使用`git merge master`命令将`master分支合并入iss53分支`，或者你也可以等到iss53分支完成其使命，再将其合并回`master分支`。


假设你已经修正了#53问题，并且打算将你的工作合并入master分支。为此，你需要`合并iss53分支到master分支`，这和之前你合并hotfix分支所做的工作差不多。你只需要`检出到你想合并入的分支，然后运行git merge命令`：
```
$ git checkout master
Switched to branch 'master'
$ git merge iss53
Merge made by the 'recursive' strategy.
index.html | 1 +
1 file changed, 1 insertion(+)
```
这和你之前合并hotfix分支的时候看起来有一点不一样。在这种情况下，你的开发历史从一个更早的地方开始分叉开来(diverged)。因为，`master分支所在提交并不是iss53分支所在提交的直接祖先`，Git不得不做一些额外的工作。

出现这种情况的时候，Git会使用`两个分支的未端所指的快照(C4和C5)`以及这两个分支的`公共祖先(C2)`，做一个简单的`三方合并`。

![](https://img-blog.csdnimg.cn/9d0f07d99c424591a04039204e4108b4.png)

和之前将分支指针向前推进所不同的是，`Git将此次三方合并的结果做了一个新的快照并且自动创建一个新的提交指向它`。这个被称作`一次合并提交`，它的特别之处在于他有`不止一个父提交`。

![](https://img-blog.csdnimg.cn/5c50b68f2c1e419fbf7b76357c18ed71.png)

既然你的修改已经合并进来了，就不再需要iss53分支了。现在你可以在任务追踪系统中关闭此项任务，并删除这个分支。

```
$ git branch -d iss53
```

上面讲到有一些方法可以绕过这个问题(即，`暂存(stashing)和clean)`，我们来看下

### 贮藏与清理

有时，当你在项目的一部分上已经工作一段时间后，所有东西都进入了混乱的状态， 而这时你想要切换到另一个分支做一点别的事情。 问题是，你不想仅仅因为过会儿回到这一点而为做了一半的工作创建一次提交。 针对这个问题的答案是` git stash `命令。

`贮藏(stash)`会处理工作目录的`脏的状态`——即`跟踪文件的修改与暂存的改动`——然后将`未完成的修改保存到一个栈`上，而你可以在任何时候`重新应用这些改动(甚至在不同的分支上)`。

#### 贮藏工作

运行 git status，可以看到有改动的状态：
```bash
$ git status
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
  modified: index.html
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working
directory)
  modified: lib/simplegit.rb
```

**现在想要切换分支，但是还不想要提交之前的工作；所以贮藏修改。将新的贮藏推送到栈上，运行`git stash`或`git stash push`：**

```bash
$ git stash
Saved working directory and index state \
  "WIP on master: 049d078 added the index file"
HEAD is now at 049d078 added the index file
(To restore them type "git stash apply")

```
可以看到工作目录是干净的了：
```bash
$ git status
# On branch master
nothing to commit, working directory clean
```

此时，你可以切换分支并在其他地方工作；你的修改被存储在栈上。要查看贮藏的东西，可以使用`git stash list：`

```bash
$ git stash list
stash@{0}: WIP on master: 049d078 added the index file
stash@{1}: WIP on master: c264051 Revert "added file_size"
stash@{2}: WIP on master: 21d80a5 added number to log

```
在本例中，有两个之前的贮藏，所以你接触到了三个不同的贮藏工作。可以通过原来 `stash`命令的帮助提示中的命令将你刚刚贮藏的工作重新应用：`git stash apply`。

如果想要应用其中一个更旧的贮藏，可以通过名字指定它，像这样：`git stash apply stashe@{2}`。如果不指定一个贮藏，Git认为指定的是最近的贮藏：

```bash
$ git stash apply
On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working
directory)
  modified: index.html
  modified: lib/simplegit.rb
no changes added to commit (use "git add" and/or "git commit -a")
```

`通过stash存储修改和新建分支比起来`，一大优点是，它在应用时并不是必须要有一个干净的工作目录，或者要应用到同一分支才能成功应用贮藏。

`stash`可以在一个分支上保存一个贮藏，切换到另一个分支，然后尝试重新应用这些修改。当应用贮藏时工作目录中也可以有修改与未提交的文件——如果有任何东西不能干净地应用，`Git会产生合并冲突`。

文件的改动被重新应用了，但是之前`暂存的文件(add)却没有重新暂存`。想要那样的话，必须使用`--index`选项来运行`git stash apply`命令，来尝试重新应用暂存的修改。如果已经那样做了，那么你将回到原来的位置：
```bash
$ git stash apply --index
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
  modified: index.html
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working
directory)
  modified: lib/simplegit.rb
```
应用选项只会尝试应用贮藏的工作——在堆栈上还有它。可以运行`git stash drop`加上将要移除的贮藏的名字来移除它：

```bash
$ git stash list
stash@{0}: WIP on master: 049d078 added the index file
stash@{1}: WIP on master: c264051 Revert "added file_size"
stash@{2}: WIP on master: 21d80a5 added number to log
$ git stash drop stash@{0}
Dropped stash@{0} (364e91f3f268f0900bc3ee613f9f733e82aaed43)
```

可能有小伙伴会说，如过stash之后，我忘记了有过stash的操作，通过status命令想看到文件状态？

```bash
$ git status -s
M index.html
 M lib/simplegit.rb
```
`git stash`命令的`--keep-index`选项。它告诉`Git不仅要贮藏所有已暂存的内容，同时还要将它们保留在索引中。`，即我们可以通过status命令开查看状态
```bash
$ git stash --keep-index
Saved working directory and index state WIP on master: 1b65b17 added the
index file
HEAD is now at 1b65b17 added the index file
```

```bash
$ git status -s
M index.html
```
默认情况下，`git stash`只会`贮藏已修改`和`暂存的已跟踪文件`。如果指定`--include-untracked`或`-u`选项，Git也会`贮藏任何未跟踪文件`。
```bash
$ git status -s
M index.html
 M lib/simplegit.rb
?? new-file.txt
$ git stash -u
Saved working directory and index state WIP on master: 1b65b17 added the
index file
HEAD is now at 1b65b17 added the index file
$ git status -s
$
```
然而，在贮藏中包含未跟踪的文件仍然不会包含`明确忽略`的文件。要额外包含忽略的文件，请使用`--al1`或`-a`选项。

#### 从贮藏创建一个分支

如果贮藏了一些工作，将它留在那儿了一会儿，然后继续在贮藏的分支上工作，在重新应用工作时可能会有问题。如果应用尝试修改刚刚修改的文件，你会得到一个合并冲突并不得不解决它。

如果想要一个轻松的方式来再次测试贮藏的改动，可以运行`git stash branch <new branchname>`以你指定的分支名创建一个新分支，检出贮藏工作时所在的提交，重新在那应用工作，然后在应用成功后丢弃贮藏：

```bash
$ git stash branch testchanges
M index.html
M lib/simplegit.rb
Switched to a new branch 'testchanges'
On branch testchanges
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)
  modified: index.html
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working
directory)
  modified: lib/simplegit.rb
Dropped refs/stash@{0} (29d385a81d163dfd45a452a2ce816487a6b8b014)
```

这是在新分支轻松恢复贮藏工作并继续工作的一个很不错的途径

### 清理工作目录


对于工作目录中一些工作或文件，你想做的也许不是贮藏而是移除。` git clean `命令就是用来干这个的。

`需要谨慎地使用这个命令，因为它被设计为从工作目录中移除未被追踪的文件。`

如果你改变主意了，你也不一定能找回来那些文件的内容。 一个更安全的选项是运行 `git stash --all `来移除每一样东西并存放在栈中。

你可以使用` git clean `命令`去除冗余文件或者清理工作目录`。 使用` git clean -f -d `命令来移除工作目录中所有`未追踪的文件`以及`空的子目录`。 -f 意味着“强制(force)”或“确定要移除”，使用它需要 Git 配置变量 clean.requireForce 没有显式设置为 false。


如果只是想要看看它会做什么，可以使用 `--dry-run `或 `-n `选项来运行命令， 这意味着“`做一次演习然后告诉你 将要 移除什么`”。

```bash
$ git clean -d -n
Would remove test.o
Would remove tmp/
```
默认情况下，git clean 命令只会移除没有忽略的未跟踪文件。 

```bash
$ git status -s
 M lib/simplegit.rb
?? build.TMP
?? tmp/
```
任何与 .gitignore 或其他忽略文件中的模式匹配的文件都不会被移除。 如果你也想要移除那些文件，
```bash
$ git clean -n -d
Would remove build.TMP
Would remove tmp/
```
例如为了做一次完全干净的构建而移除所有由构建生成的 .o 文件， 可以给 clean 命令增加一个` -x `选项。
```bash
$ git clean -n -d -x
Would remove build.TMP
Would remove test.o
Would remove tmp/
```

## 分支管理

`git branch `命令不只是可以创建与删除分支。 如果不加任何参数运行它，会得到当前所有分支的一个列表：

![](https://img-blog.csdnimg.cn/3875aa3ef99b443cbdcbccdebe08d66e.png)

`git branch -a `命令可以查看` 本地和远程的分支`

![](https://img-blog.csdnimg.cn/2478fbce2e8544d69b93a174c6c07cd0.png)

注意 master 分支前的 * 字符：它代表现在检出的那一个分支(也就是说，当前 HEAD 指针所指向的分支)。

这意味着如果在这时候提交，master 分支将会随着新的工作向前移动。 如果需要查看每一个分支的最后一次提交，可以运行 `git branch -v `命令：

![](https://img-blog.csdnimg.cn/11fb5a47f86f44b2af37ffdfee8b7d0a.png)


`--merged与--no-merged`这两个有用的选项可以过滤这个列表中`已经合并`或`尚未合并`到`当前分支的分支`。

+ 查看哪些分支已经合并到当前分支，可以运行`git branch--merged：`可以使用`git branch-d `删除掉；你已经将它们的工作整合到了另一个分支，所以并不会失去任何东西。
+ 查看所有包含未合并工作的分支，可以运行` git branch --no-merged：`因为它包含了还未合并的工作，尝试使用`git branch-d`命令删除它时会失败：如果真的想要删除分支并丢掉那些工作，如同帮助信息里所指出的，可以使用`-D`选项强制删除它。

![](https://img-blog.csdnimg.cn/cb11e4f220ab42518da82a00b320ba2d.png)


嗯，时间关系，先整理到这里，关于分支工作流，变基等在之后的博文中和小伙伴们分享

## 博文整理参考
***

`《Pro Git》`

