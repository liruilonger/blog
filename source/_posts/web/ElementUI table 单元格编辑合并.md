﻿---
title: ElementUI table 单元格编辑合并
date:  2021-04-19 
categories:  Vue
toc: true
tags:
  - ElementUI
  - Vue

thumbnail:
---

你存在的意义，诠释了我仓促青春里的爱情。
<!-- more -->

#### 我的需求：
+ 看公司的`低代码平台`有使用这种方式，所以想`研究`研究，后期项目里使用。

#### 我需要解决的问题：
+ 如何通过数据对`ElementUI`的`表格组件`中单元格的编辑功能进行处理，输入框和表格样式如何切换
+ 表格合并是如何处理的
#### 我是这样做的：
+ `百度`寻找解决办法，向公司前端大佬`请教`。

**<font color="009688">你存在的意义，诠释了我仓促青春里的爱情。**

***
### 单行视图手动双击切换
通过双击表格实现编辑的表格的切换。
![在这里插入图片描述](https://img-blog.csdnimg.cn/2020111311181655.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20201113111827598.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70#pic_center)


```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <!-- import Vue before Element -->
    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <!-- import JavaScript -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
    <script src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body style="display: flex;justify-content: center;width: 100%;">
    <div id="app">
        <el-card>
            <el-table :data="tableData" style="width: 100%" :row-class-name="tableRowClassName"
                @cell-dblclick="editdbClick">
                <el-table-column prop="date" label="日期" width="180">
                    <template slot-scope="scope">
                        <el-select v-show="!showEdit[scope.$index]" popper-class="role-option" v-model="scope.row.date"
                            placeholder="请选择" size="small" @change="dateHandleChange">
                            <el-option v-for="item in optionData" :key="item.name" :label="item.name"
                                :value="item.value"></el-option>
                        </el-select>
                        <span style="line-height: 32px;"
                            v-show="showEdit[scope.$index]">{{ optionDataf(scope.row.date)}} </span>
                    </template>
                </el-table-column>
                <el-table-column prop="name" label="姓名" width="180">
                    <template slot-scope="scope">
                        <el-select v-show="!showEdit[scope.$index] && scope.row.date != '2' " popper-class="role-option"
                            v-model="scope.row.name" placeholder="请选择" size="small">
                            <el-option v-for="item in optionName" :key="item.name" :label="item.name"
                                :value="item.value"></el-option>
                        </el-select>
                        <span style="line-height: 32px;"
                            v-show="showEdit[scope.$index]">{{optionNamef(scope.row.name) }} </span>
                    </template>
                </el-table-column>
                <el-table-column prop="addr" width="180" label="地址">
                    <template slot-scope="scope">
                        <el-select v-show="!showEdit[scope.$index]" popper-class="role-option" v-model="scope.row.addr"
                            placeholder="请选择" size="small">
                            <el-option v-for="item in optionsAddr(scope.row)" :key="item.name" :label="item.name"
                                :value="item.value"></el-option>
                        </el-select>
                        <span style="line-height: 32px;"
                            v-show="showEdit[scope.$index]">{{optionsAddrf(scope.row.addr) }}
                        </span>
                    </template>
                </el-table-column>
                <el-table-column label="操作" width="100" header-align="right">
                    <template slot-scope="scope">
                        <div style="display: flex;justify-content: space-between;">
                            <el-link icon="el-icon-plus" size="mini" type="primary"
                                @click="handleAdd(scope.$index, scope.row)" :underline="false"
                                style="font-size: 14px;margin-left: 40px"></el-link>
                            <el-link icon="el-icon-delete" size="mini" @click="handleDelete(scope.$index, scope.row)"
                                type="danger" :underline="false"></el-link>
                        </div>
                    </template>
                </el-table-column>
            </el-table>
        </el-card>
    </div>
</body>
<script>
 /**
    * @description: 双击行视图手动切换，通过双击表格实现编辑的表格的切换
    * @author: Liruilong
    */
    new Vue({
        el: '#app',
        data() {
            return {
                optionData: [{ value: "1", name: "2016-05-03" }, { value: "2", name: "2016-05-04" }, { value: "3", name: "2016-05-05" }],
                optionName: [{ value: "1", name: " 王小虎" }, { value: "2", name: "王小龙" }, { value: "3", name: "王小狗" }],
                optionAddr: [{ value: "1", name: "北凉" }, { value: "2", name: "西北道" }, { value: "3", name: "泰安城" }],
                tableData: [{
                    date: '1',
                    name: '1',
                    addr: '1',
                }],
                optionAddrMapper: new Map(),
                date: '',
                name: '',
                address: '',
                showEdit: [false],
            }
        },
        created() {

        },
        mounted() {
            // 模拟ajax初始化数据集
            this.optionAddrMapper.set("1", [{ value: "1", name: "北凉" }, { value: "2", name: "西北道" }, { value: "3", name: "泰安城" }]);
        },
        methods: {
            optionDataf(id) {
                let name = id;
                this.optionData.forEach((element) => {

                    if (element.value == id) {
                        name = element.name;
                    }
                });
                return name;
            },
            optionNamef(id) {
                let name = id;
                this.optionName.forEach((element) => {
                    if (element.value == id) {
                        name = element.name;
                    }
                });
                return name;
            },
            optionsAddrf(id) {
                let name = id;
                if (this.optionAddrMapper) {
                    this.optionAddrMapper.forEach((value, key) => {
                        value.forEach((e) => {
                            if (e.value == id) {
                                name = e.name;
                            }
                        })
                    });
                } else {
                    this.optionAddr.forEach(e => {
                        if (e.value == id) {
                            name = e.name;
                        }
                    });
                }
                return name;
            },
            editdbClick(row, column, cell, event) {

                if ((row.date && row.name && row.addr) || (row.date && row.addr)) {
                    this.$set(this.showEdit, row.index, !this.showEdit[row.index])
                }

            },
            // openShow(index){
            //     this.showEdit.forEach( (e) =>{
            //         if(e != index){
            //             this.$set(this.showEdit, e, false)
            //         }
                   
            //     })
            // },
            saveClick(row, column, cell, event) {
            },
            optionsAddr(row) {
                return this.optionAddrMapper.get(row.date);
            },
            //联动处理//模拟ajax调用接口获取数据.日期选择会联动地址
            dateHandleChange(val) {
                // 模拟根据日期得到地址的数据集 这里写ajax ，必须是同步的请求。
                this.optionAddrMapper.set("1", [{ value: "1", name: "北凉" }, { value: "2", name: "西北道" }, { value: "3", name: "泰安城" }]);
                this.optionAddrMapper.set("2", [{ value: "9", name: "西域" }, { value: "8", name: "苗疆" }, { value: "7", name: "剑气长城" }]);
                this.optionAddrMapper.set("3", [{ value: "10", name: "圣贤林" }, { value: "11", name: "青冥天下" }, { value: "12", name: "藕花福地" }]);
            },
            // 添加操作。默认会保存当前行的数据，并添加一条新数据
            handleAdd(index, row) {
                let data = this.tableData[this.tableData.length - 1];
                if (((row.date && row.name && row.addr) || (row.date && row.addr)) && ((data.date && data.name && data.addr) || (data.date && data.addr))) {
                    this.$set(this.showEdit, row.index, true)
                    this.tableData.push({
                        date: '',
                        name: '',
                        addr: ''
                    });
                    this.$set(this.showEdit, row.index + 1, false)
                }
            },
            handleDelete(index, row) {
                debugger
                if (index > 0) {
                    this.tableData.splice(index, 1);
                }
            },
            tableRowClassName({ row, rowIndex }) {
                row.index = rowIndex;
                if ((rowIndex + 1) % 2 === 1) {
                    return 'warning-row';
                } else {
                    return 'success-row';
                }
                return '';
            }
        },
        props: {},
        computed: {},

        created() {

        },
        watch: {},
        filters: {

        },

    })
</script>
<style>
    .el-table .warning-row {
        background: oldlace;
    }

    .el-table .success-row {
        background: #f0f9eb;
    }
</style>

</html>
```
>
***
### 单机视图自动切换
即只留一条要编辑的操作。这个和之前的原理不一样，通过样式控制。
![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-FUeHuF7B-1605237228003)(imgclip_8.png "imgclip_8.png")\]](https://img-blog.csdnimg.cn/20201113112001446.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70#pic_center)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20201113112015270.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70#pic_center)


```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
    <!-- import Vue before Element -->
    <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <!-- import JavaScript -->
    <script src="https://unpkg.com/element-ui/lib/index.js"></script>
</head>


<body style="display: flex;justify-content: center;width: 100%;margin-top: 20px;">
    <div id="app">
        <el-card>
            <!-- 关键代码 highlight-current-row -->
            <el-table :data="tableData" style="width: 100%" class="tb-edit"  highlight-current-row @row-click="handleCurrentChange" :row-class-name="tableRowClassName" >
                <el-table-column prop="date" label="日期" width="180">
                    <template slot-scope="scope">
                        <el-input size="small" v-model="scope.row.date" placeholder="请输入内容"
                            @change="handleEdit(scope.$index, scope.row)"></el-input> <span>{{scope.row.date}}</span>
                    </template>
                </el-table-column>
                <el-table-column prop="name" label="姓名" width="180">
                    <template slot-scope="scope">
                        <el-input size="small" v-model="scope.row.name" placeholder="请输入内容"
                            @change="handleEdit(scope.$index, scope.row)"></el-input> <span>{{scope.row.name}}</span>
                    </template>
                </el-table-column>
                <el-table-column prop="addr" width="180" label="地址">
                    <template slot-scope="scope">
                        <el-input size="small" v-model="scope.row.addr" placeholder="请输入内容"
                            @change="handleEdit(scope.$index, scope.row)"></el-input> <span>{{scope.row.addr}}</span>
                    </template>
                </el-table-column>
                <el-table-column label="操作" width="100" header-align="right">
                    <template slot-scope="scope">
                        <div style="display: flex;justify-content: space-between;">
                            <el-link icon="el-icon-plus" size="mini" type="primary"
                                @click="handleAdd(scope.$index, scope.row)" :underline="false"
                                style="font-size: 14px;margin-left: 40px"></el-link>
                            <el-link icon="el-icon-delete" size="mini" @click="handleDelete(scope.$index, scope.row)"
                                type="danger" :underline="false"></el-link>
                        </div>
                    </template>
                </el-table-column>
            </el-table>
        </el-card>
    </div>
</body>
<script>
    /**
    * @description: 单机视图自动切换，即只留一条要编辑的操作。这个和之前的原理不一样，通过样式控制。
    * @author: Liruilong
    */
    new Vue({
        el: '#app',
        data() {
            return {
                tableData: [{
                    date: '2016-05-02',
                    name: '王小虎',
                    addr: '北凉',
                }, {
                    date: '2016-05-04',
                    name: '王小狗',
                    addr: '西北道'
                }, {
                    date: '2016-05-04',
                    name: '王小狗',
                    addr: '西北道'
                }, {
                    date: '2016-05-04',
                    name: '王小狗',
                    addr: '西北道'
                }]
            }
        },
        mounted() {

        },
        methods: {
            handleCurrentChange(row, event, column) {
                console.log(row, event, column, event.currentTarget)
                console.log("根据 Element Table @row-click=handleCurrentChange 事件，当该行点击的时候会动态添加一个 .current-row 的class属性，然后在 css 中进行 display 控制就行了。")
            },
            handleAdd(index, row) {
                this.tableData.push({
                    date: '',
                    name: '',
                    addr: ''
                });
            },
            handleDelete(index, row) {
                if (index > 0) {
                    this.tableData.splice(index, 1);
                }
            },
            tableRowClassName({ row, rowIndex }) {
                row.index = rowIndex;
                if ((rowIndex + 1) % 2 === 1) {
                    return 'warning-row';
                } else {
                    return 'success-row';
                }
                return '';
            }
        },

        props: {},
        computed: {},

        created() {

        },
        watch: {},
    })
</script>
<style> 
    .el-table .warning-row {
        background: oldlace;
    }

    .el-table .success-row {
        background: #f0f9eb;
    }
    .tb-edit .el-input {
        display: none
    }

    .tb-edit .current-row .el-input {
        display: block
    }

    .tb-edit .current-row .el-input+span {
        display: none
    }
</style>

</html>
```

### 行列单元格合并，悬浮编辑，根据填写数据动态变色

![在这里插入图片描述](https://img-blog.csdnimg.cn/20201211204953812.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20201211205558205.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

```html
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <!-- import CSS -->
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <!-- import Vue before Element -->
  <script src="https://unpkg.com/vue/dist/vue.js"></script>
  <!-- import JavaScript -->
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
</head>


<body >
  <div id="app">
    <el-card>
      <template>
        <el-table :data="tableData" size="mini" class="tb-edit" style="width: 100%;" max-height="800" height="800"
          :row-style="{height:'16px'}" :cell-style="{padding:'2px'}" :row-class-name="tableRowClassName"
           @row-click="handleCurrentChange" @cell-mouse-enter="editdbClick" @cell-mouse-leave="editdbClick"
          :span-method="arraySpanMethod" 
          >
          <el-table-column label="质量评价表" header-align="center">
            <el-table-column :label="name">
              <el-table-column prop="serialNumber" label="序号" width="40"></el-table-column>

              <el-table-column label="类别" header-align="center">
                <el-table-column prop="category1" width="100" align="center">
                  <template slot="header" slot-scope="scope">
                  </template>
                </el-table-column>
                <el-table-column prop="category2" width="100" align="center">
                  <template slot="header" slot-scope="scope">
                  </template>
                </el-table-column>
              </el-table-column>
              <el-table-column label="标准分" header-align="center">
                <el-table-column prop="standardScore" label="100" width="100" align="center">
                  <template slot="header" slot-scope="scope">100 分</template>
                  <template slot-scope="scope">
                    <span>{{ scope.row.standardScore }} </span>
                  </template>
                </el-table-column>
              </el-table-column>
              <el-table-column label="打分" header-align="center">
                <el-table-column prop="score" :label="number" width="130" align="center">
                  <template slot="header" slot-scope="scope"> {{ number+"" }} 分 </template>
                  <template slot-scope="scope">
                    <el-input-number style="width: 100px" controls-position="right" ref="input" :controls="false"
                      v-show="showEdit[scope.$index]" v-model="scope.row.score" size="mini"
                      @change="handleChange(scope.row)" :min="0" :max="scope.row.standardScore" label="打分">
                    </el-input-number>
                    <span v-show="!showEdit[scope.$index]">{{scope.row.score}}</span>
                  </template>
                </el-table-column>
              </el-table-column>
              <el-table-column prop="reviewMain" label="加/扣分原因说明" header-align="center"></el-table-column>
            </el-table-column>
          </el-table-column>
        </el-table>
      </template>
    </el-card>
  </div>
</body>
<script>
  /**
  * @description: 单机视图自动切换，即只留一条要编辑的操作。这个和之前的原理不一样，通过样式控制。
  * @author: Liruilong
  */
  var vm = new Vue({
    el: "#app",
    data: {
      name: "工程名称:",
      number: '',
      showEdit: [],
      tableData:[{
          "id": "00bf58ee70414dca8761",
          "serialNumber": "一",
          "category1": "设计资料",
          "category2": "设计资料",
          "reviewMain": "1.设计资料齐全，设计报告、图纸、概算满足设计深度及标准化要求",
          "standardScore": 10,
          "sortNumber": 1,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "score": null
        },
        {
          "id": "ed7aeaa4c40541a0bcde",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "1、柱上变压器(箱变、环网柜、配电室)选址是否合理，周围地形地貌、道路交通情况、建筑物及规划因素考虑周全，充分考虑出线条件",
          "standardScore": 2,
          "sortNumber": 2,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 40,
            "monthValue": 12,
            "nano": 0,
            "second": 37,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 40,
            "monthValue": 12,
            "nano": 0,
            "second": 37,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "6ef643f0ab30426490b9",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "2、主接线配置是否合理，无功补偿配置方案是否合理，供电区域谐波情况是否分析清楚",
          "standardScore": 1,
          "sortNumber": 3,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 40,
            "monthValue": 12,
            "nano": 0,
            "second": 51,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 40,
            "monthValue": 12,
            "nano": 0,
            "second": 51,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "ad8b094a11a54518937d",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "3.设备选型、导体选择是否合理(规格及型号等)\n",
          "standardScore": 2,
          "sortNumber": 4,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 41,
            "monthValue": 12,
            "nano": 0,
            "second": 15,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 41,
            "monthValue": 12,
            "nano": 0,
            "second": 15,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "8c5af871ebca4e52b4e6",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "4.电气总平面布置是否合理",
          "standardScore": 1,
          "sortNumber": 5,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 41,
            "monthValue": 12,
            "nano": 0,
            "second": 34,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 41,
            "monthValue": 12,
            "nano": 0,
            "second": 34,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "dd19f4c18c1c45d3aa6a",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "5.防雷接地是否描述清楚，方案是否合理",
          "standardScore": 1,
          "sortNumber": 6,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 41,
            "monthValue": 12,
            "nano": 0,
            "second": 50,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 41,
            "monthValue": 12,
            "nano": 0,
            "second": 50,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "971dd8e1890340ce93a9",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "6.配电二次设备的布置、保护装置的选用及方案是否合理性",
          "standardScore": 1,
          "sortNumber": 7,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 42,
            "monthValue": 12,
            "nano": 0,
            "second": 15,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 42,
            "monthValue": 12,
            "nano": 0,
            "second": 15,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "60d5e9a5f5af49c780fe",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "7.是否采用典型设计、通用设备，如不采用，是否有专题论述",
          "standardScore": 1,
          "sortNumber": 8,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 42,
            "monthValue": 12,
            "nano": 0,
            "second": 23,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 42,
            "monthValue": 12,
            "nano": 0,
            "second": 23,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "4341610f21ce407db3e9",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "8.10kV线路接入系统是否合理，线路路径描述是否清晰，采用电缆的必要性是否充分，改造线路存在问题分析是否详实，必要性是否充分\n",
          "standardScore": 2,
          "sortNumber": 9,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 42,
            "monthValue": 12,
            "nano": 0,
            "second": 35,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 42,
            "monthValue": 12,
            "nano": 0,
            "second": 35,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "da710c9c61cb4cfe8d0f",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "9.10kV架空线路气象条件、导线选择、柱上设备、杆塔和基础型式、绝缘配合、防雷与接地、交叉跨越、走廊清理内容是否完整，方案是否合理\n",
          "standardScore": 2,
          "sortNumber": 10,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 42,
            "monthValue": 12,
            "nano": 0,
            "second": 59,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 42,
            "monthValue": 12,
            "nano": 0,
            "second": 59,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "0234913c5e8b41668f16",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "10.10kV电缆线路的环境条件、电缆及附件选型、开关设施布置(明确开关站、环网柜、分支箱、分线箱、电表等设备的数量、(进出线)规模、设备配置、保护配置)、电缆敷设方式、电缆通道、横断面及纵断面设计、电缆工作井设计等内容是否完整，论述是否正确，方案是否合理\n",
          "standardScore": 4,
          "sortNumber": 11,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 43,
            "monthValue": 12,
            "nano": 0,
            "second": 13,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 43,
            "monthValue": 12,
            "nano": 0,
            "second": 13,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "26421ded4df440dcb189",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "11.低压线路的路径选择是否合理，改造线路的路径描述是否清晰，存在的问题描述是否提供依据\n",
          "standardScore": 2,
          "sortNumber": 12,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 43,
            "monthValue": 12,
            "nano": 0,
            "second": 25,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 43,
            "monthValue": 12,
            "nano": 0,
            "second": 25,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "fb49c80235a745ca934a",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "12.低压架空线路气象条件、导线选择、柱上设备、杆塔和基础型式、绝缘配合、防雷与接地、交叉跨越、走廊清理内容是否完整，方案是否合理\n",
          "standardScore": 2,
          "sortNumber": 13,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 43,
            "monthValue": 12,
            "nano": 0,
            "second": 39,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 43,
            "monthValue": 12,
            "nano": 0,
            "second": 39,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "72575eccfc8b4389b136",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "配电及低压部分",
          "reviewMain": "13.低压电缆线路的环境条件、电缆及附件选型、开关设施布置(明确开关站、环网柜、分支箱、分线箱、电表等设备的数量、(进出线)规模、设备配置、保护配置)、电缆敷设方式、电缆通道、横断面及纵断面设计、电缆工作井设计等内容是否完整，论述是否正确，方案是否合理\n",
          "standardScore": 4,
          "sortNumber": 14,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 43,
            "monthValue": 12,
            "nano": 0,
            "second": 53,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 43,
            "monthValue": 12,
            "nano": 0,
            "second": 53,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "7447e2e92faa4c25a815",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "1.路径描述清晰(改造或者新建)，包括线路出线间隔、走向、地质、水文、行政区、地形比例、重要交叉跨越等，对于电缆线路，还应增加采用电缆通道建设的必要性和可行性\n",
          "standardScore": 3,
          "sortNumber": 15,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 44,
            "monthValue": 12,
            "nano": 0,
            "second": 19,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 44,
            "monthValue": 12,
            "nano": 0,
            "second": 19,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "7e17e37c22cf4b618ec0",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "2.架空线路方案气象条件切合实际\n",
          "standardScore": 1,
          "sortNumber": 16,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 44,
            "monthValue": 12,
            "nano": 0,
            "second": 51,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 44,
            "monthValue": 12,
            "nano": 0,
            "second": 51,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "6038938ab4e74637a8df",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "3.导线选择充分结合线路、网架结构及负荷发展需求\n",
          "standardScore": 2,
          "sortNumber": 17,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 45,
            "monthValue": 12,
            "nano": 0,
            "second": 4,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 45,
            "monthValue": 12,
            "nano": 0,
            "second": 4,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "89e3b65ca12848409839",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "4.柱上开关设备配置和设备选型合理\n",
          "standardScore": 2,
          "sortNumber": 18,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 45,
            "monthValue": 12,
            "nano": 0,
            "second": 21,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 45,
            "monthValue": 12,
            "nano": 0,
            "second": 21,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "a973328f7751412da758",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "5.杆塔和基础选型合理\n",
          "standardScore": 2,
          "sortNumber": 19,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 45,
            "monthValue": 12,
            "nano": 0,
            "second": 30,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 45,
            "monthValue": 12,
            "nano": 0,
            "second": 30,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "47c1af18057542ff92c1",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "6.绝缘配合、防雷与接地方案合理，内容详尽\n",
          "standardScore": 1,
          "sortNumber": 20,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 15,
            "minute": 45,
            "monthValue": 12,
            "nano": 0,
            "second": 39,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": "1111",
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 3,
            "monthValue": 12,
            "nano": 0,
            "second": 56,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "4cbebbe5b82644be9cd4",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "7.交叉跨越方案合理，走廊通道清理内容详尽\n",
          "standardScore": 1,
          "sortNumber": 21,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 3,
            "monthValue": 12,
            "nano": 0,
            "second": 47,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 3,
            "monthValue": 12,
            "nano": 0,
            "second": 47,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "689df639813c4d38918c",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "8.电缆线路环境条件描述清晰、详尽\n",
          "standardScore": 3,
          "sortNumber": 22,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 4,
            "monthValue": 12,
            "nano": 0,
            "second": 17,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": "1111",
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 4,
            "monthValue": 12,
            "nano": 0,
            "second": 40,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "bfecab1664404a758d62",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "9.电缆及附件选型充分考虑线路、网架结构及负荷发展需求\n",
          "standardScore": 2,
          "sortNumber": 23,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 4,
            "monthValue": 12,
            "nano": 0,
            "second": 29,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": "1111",
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 4,
            "monthValue": 12,
            "nano": 0,
            "second": 49,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "316f5e4d0660484f9791",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "10.开关设施配置原则和设备选型合理，涉及开关站、环网柜应提供站址选择、方案对比等内容，提供主接线、平面图等满足深度要求的图纸\n",
          "standardScore": 2,
          "sortNumber": 24,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 4,
            "monthValue": 12,
            "nano": 0,
            "second": 58,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 4,
            "monthValue": 12,
            "nano": 0,
            "second": 58,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "fdd12754e0bb48bf9f00",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "11.电缆敷设方式合理，通道描述清晰\n",
          "standardScore": 2,
          "sortNumber": 25,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 6,
            "monthValue": 12,
            "nano": 0,
            "second": 21,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 6,
            "monthValue": 12,
            "nano": 0,
            "second": 21,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "baf6846640e34870b8ca",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "12.横断面及纵断面设计方案合理\n",
          "standardScore": 1,
          "sortNumber": 26,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 6,
            "monthValue": 12,
            "nano": 0,
            "second": 32,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 6,
            "monthValue": 12,
            "nano": 0,
            "second": 32,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "239af8709d2d47f8a224",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "13.电缆工作井建设必要性充分，尺寸、数量、地点描述清晰，提供电缆井盖承载能力并结合工程实际校验\n",
          "standardScore": 1,
          "sortNumber": 27,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "createTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 6,
            "monthValue": 12,
            "nano": 0,
            "second": 53,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "updateUser": null,
          "updateTime": {
            "month": "DECEMBER",
            "year": 2020,
            "dayOfMonth": 1,
            "hour": 16,
            "minute": 6,
            "monthValue": 12,
            "nano": 0,
            "second": 53,
            "dayOfWeek": "TUESDAY",
            "dayOfYear": 336,
            "chronology": {
              "id": "ISO",
              "calendarType": "iso8601"
            }
          },
          "score": null
        }, {
          "id": "07ce43d177cc44609aaf",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "14.必要时提供路径协议，且确保协议有效\n",
          "standardScore": 1,
          "sortNumber": 28,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "score": null
        }, {
          "id": "0fde1bbe53a34f1ca9a1",
          "serialNumber": "二",
          "category1": "技术方案部分",
          "category2": "线路部分",
          "reviewMain": "15.是否应用通用设计、通用设备，不应用时是否有专题论述\n",
          "standardScore": 1,
          "sortNumber": 29,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "score": null
        }, {
          "id": "1a4226cae58c46e08941",
          "serialNumber": "三",
          "category1": "投资概算",
          "category2": "投资概算",
          "reviewMain": "1.资料完整性：设计深度，内容、附表等是否满足要求\n",
          "standardScore": 3,
          "sortNumber": 30,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "score": null
        }, {
          "id": "27cf50e95ea44a6e8524",
          "serialNumber": "三",
          "category1": "投资概算",
          "category2": "投资概算",
          "reviewMain": "2.概算编制的规范性：编制原则、取费标准是否执行现行文件规定\n\n",
          "standardScore": 3,
          "sortNumber": 31,
          "extend1": null,
          "extend2": null,
          "extend3": null,
          "createUser": "1111",
          "score": null
        },],
     
      dialogTableVisible: false,
      iframeQuestionListUrl: "",
      clo1Merge: '',
    },
    methods: {
      init() {
        var _this = this;
        // $.ajax({ });
      },

      handleChange(row) {
        let num = 0;
        this.tableData.forEach((e) => {
          if (e.score) {
            num += e.score;
          }
        });
        this.number = num;
        const id = row.id;
        let score = '';
      },
      handleCurrentChange(row, event, column) {
        //     	                console.log(row, event, column, event.currentTarget)
      },
      handleEdit(index, row) {
        //     	                console.log(index, row);
      },
      tableRowClassName({ row, rowIndex }) {
        row.index = rowIndex;
        if (row.score && row.standardScore > row.score) {
          return 'warning-row';
        } else if (row.standardScore == row.score) {
          return 'success-row';
        } else {
          return 'error-row';
        }
      },
      setTable(data, columnIndex) {
        // 行合并标识
        let spanOneArr = [];
        // 列合并标识
        let spanTwoArr = [];
        let concatOne = 0;
        let concatTwo = 0;
        if (data) {
          data.forEach((item, index) => {
            if (index === 0) {
              spanOneArr.push(1);
              spanTwoArr.push(1);
            } else {
              if (columnIndex === 0 && item.serialNumber === data[index - 1].serialNumber) { // 第一列需合并相同内容的判断条件
                spanOneArr[concatOne] += 1;
                spanOneArr.push(0);
                spanTwoArr.push(0);
              } else if (columnIndex === 1 && item.category1 === data[index - 1].category1) {
                spanOneArr[concatOne] += 1;
                spanOneArr.push(0);
                spanTwoArr.push(0);
              } else if (columnIndex === 2 && item.category2 === data[index - 1].category2) {
                spanOneArr[concatOne] += 1;
                spanOneArr.push(0);
                spanTwoArr.push(0);
              } else {
                spanOneArr.push(1);
                spanTwoArr.push(1);
                concatOne = index;
                concatTwo = index;
              }
            }
          });
        }
        return {
          one: spanOneArr,
          two: spanTwoArr
        };
      },
      setCell(row, columnIndex, _row) {
        if (row.category2 === row.category1 && columnIndex === 1) {
          return 2;
        } else if (row.category2 === row.category1 && columnIndex === 2) {
          return 0;
        } else {
          return _row > 0 ? 1 : 0;
        }
      },
      arraySpanMethod({ row, column, rowIndex, columnIndex }) {
        let td = this.setTable(this.tableData, columnIndex);
        const _row = (td.one)[rowIndex];
        const _col = this.setCell(row, columnIndex, _row);

        return {
          rowspan: _row,
          colspan: _col
        };
      },
      editdbClick(row, column, cell, event) {
        if (column.property === "score") {
          this.$nextTick(() => {
            this.$refs.input.focus()
          });
          this.showEdit.forEach((e, index) => {
            this.$set(this.showEdit, index, false);
          })
          this.$set(this.showEdit, row.index, !this.showEdit[row.index]);
        }


      },
    },
    mounted() {

    },
    created() {
      this.init();
    },

  })
    ;
</script>
<style>
  .el-dialog__body {
    padding: 0px;
  }

  /*  .el-input-number__decrease{
          display: none;
      }
      .el-input-number__increase{
          display: none;
      }*/
  .el-table .warning-row {
    background: rgba(253, 245, 230, 0.63);
  }

  .el-table .success-row {
    background: rgba(240, 249, 235, 0.64);
  }

  .el-table .error-row {}

  .el-table__body tr:hover>td {
    background-color: rgba(64, 158, 255, 0.15) !important;
  }
</style>

</html>
```


