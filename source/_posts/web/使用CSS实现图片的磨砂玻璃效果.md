---
title: 使用CSS实现图片的磨砂玻璃效果
tags:
  - CSS
categories:
  - CSS
toc: true
recommend: 1
keywords: CSS
uniqueId: '2022-10-31 10:31:33/使用CSS实现图片的磨砂玻璃效果.html'
mathJax: false
date: 2022-10-31 18:31:33
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 偶然看到，这里整理笔记
+ 博文涉及内容：
  + 使用CSS实现图片的磨砂玻璃效果Demo
  + 相关属性的简单文档说明
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

这里的` 磨砂玻璃效果`主要使用 CSS的`滤镜效果`实现，滤镜效果是 CSS 的一个模块，它定义了一种HTML元素显示在文档顶层，处理其渲染的方式。主要涉及 `backdrop-filter`,`filter` 这两个 CSS属性。

### 效果图

![在这里插入图片描述](https://img-blog.csdnimg.cn/2bb9a944417b48438cd40fb7740b3395.png)

### 原理



#### 样式一

```css
        /* Frosted glass CSS */
        .frosted_1 {
            /* Frosted glass affect */
            -webkit-backdrop-filter: blur(5px); 
            backdrop-filter: blur(5px); 
        }
```

第一种单纯利用 `backdrop-filter` CSS 属性实现

`backdrop-filter CSS 属性`: 可以让你为一个元素后面区域添加图形效果(如模糊或颜色偏移)。因为它适用于元素背后的所有元素，为了看到效果，必须使元素或其背景至少部分透明。这里一个加了 `-webkit`,只是考虑兼容性问题。如果不考虑，一个就可以了。


```css
/* 关键词值 */
backdrop-filter: none;

/* 指向 SVG 滤镜的 URL */
backdrop-filter: url(commonfilters.svg#filter);

/* <filter-function> 滤镜函数值 */
backdrop-filter: blur(2px); 
/* 模糊 */
backdrop-filter: brightness(60%); 
/* 亮度 */
backdrop-filter: contrast(40%); 
/* 对比度 */
backdrop-filter: drop-shadow(4px 4px 10px blue); 
/* 投影 */
backdrop-filter: grayscale(30%); 
/* 灰度 */
backdrop-filter: hue-rotate(120deg);
/* 色调变化 */
backdrop-filter: invert(70%); 
/* 反相 */
backdrop-filter: opacity(20%); 
/* 透明度 */
backdrop-filter: sepia(90%); 
/* 褐色 */
backdrop-filter: saturate(80%); 
/* 饱和度 */

/* 多重滤镜 */
backdrop-filter: url(filters.svg#filter) blur(4px) saturate(150%);

/* 全局值 */
backdrop-filter: inherit;
backdrop-filter: initial;
backdrop-filter: revert;
backdrop-filter: unset;

```

对应 CSS 规范 ：[https://drafts.fxtf.org/filter-effects-2/#BackdropFilterProperty](https://drafts.fxtf.org/filter-effects-2/#BackdropFilterProperty)

浏览器兼容性

![在这里插入图片描述](https://img-blog.csdnimg.cn/b68c07596fdb4023aac3393058300fd0.png)

#### 样式 二

```css        
        .frosted_2 {
            /* Frosted glass affect */
            -webkit-backdrop-filter: blur(5px);  
            backdrop-filter: blur(5px); 
        
            /* Add box-shadow for more darker */
            box-shadow: 0px 10px 15px 10px rgba(0 ,0 ,0 ,0.15);
            background-color: rgba(228 ,228 ,228 ,0.10);
        }
```


第二种在第一种的基础上利用 `box-shadow` 加了外阴影，因为背景是深色的，所以不是特别明显。同时使用 `background-color`加了一个接近透明的背景色，看上去整个偏亮一点。

关于 `box-shadow` 用的还是蛮多的，这么不多说明。


`CSS box-shadow 属性`用于在元素的框架上添加阴影效果。你可以在同一个元素上设置多个阴影效果，并用逗号将他们分隔开。该属性可设置的值包括阴影的 X 轴偏移量、Y 轴偏移量、模糊半径、扩散半径和颜色。

#### 样式三

```css        
        .frosted_3 {
            filter: drop-shadow(2px 4px 6px black);
            background-color: rgba(152, 151, 151, 0.2); 
            box-shadow: inset 0 0 0 200px rgb(255, 255, 255, 0.08);
        }
        
```

第三种没有使用 `backdrop-filter`，使用了 `filter` 加了一个投影的效果 `drop-shadow` ,同时利用 `box-shadow` 生成内阴影，一个偏量的接近透明的颜色，然后使用 `background-color` 加深了背景效果。

这里的 `filter` 和最上面的 `backdrop-filter` 基本类似，包括渲染函数基本相同，`filter` CSS属性将图形效果(如模糊或移色)应用于一个元素。过滤器通常用于调整图像、背景和边框的渲染。

对应 CSS 规范 ：[https://drafts.fxtf.org/filter-effects/#FilterProperty](https://drafts.fxtf.org/filter-effects/#FilterProperty)

浏览器兼容性

![在这里插入图片描述](https://img-blog.csdnimg.cn/bc28f4d36d434745bb7eead63b944b9a.png)

### 代码
```html
<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        body {
            padding: 0;
            margin: 0;
        }
        .container {
            height: 100vh;
            width: 100vw;
            background-image: url("./2.png");
            background-size: cover;
            display: flex;
            justify-content: space-around;
            align-items: center;
        }
        
        .glass {
            width: 30%;
            height: 20%;
        
            border-radius: 10px;
            
            display: flex;
            justify-content: center;
            align-items: center;
        
            font-size: 1.5rem;
            text-shadow: 2px 2px 2px rgba(0, 27, 77, 0.521);
            padding: 1%;
            text-align: center;
        }
        @media only screen and (max-width: 786px) {
            .glass {
                font-size: 1.2rem;
            }
        }
        
        
        /* Frosted glass CSS */
        .frosted_1 {
            /* Frosted glass affect */
            -webkit-backdrop-filter: blur(5px); 
            backdrop-filter: blur(5px); 
        }
        
        .frosted_2 {
            /* Frosted glass affect */
            -webkit-backdrop-filter: blur(5px);  
            backdrop-filter: blur(5px); 
        
            /* Add box-shadow for more darker */
            box-shadow: 0px 10px 15px 10px rgba(0 ,0 ,0 ,0.15);
            background-color: rgba(228 ,228 ,228 ,0.10);
        }
        
        .frosted_3 {
            filter: drop-shadow(2px 4px 6px black);
            background-color: rgba(152, 151, 151, 0.2); 
            box-shadow: inset 0 0 0 200px rgb(255, 255, 255, 0.08);
        }
        
        </style>
    <title>磨砂玻璃效果Demo</title>
</head>

<body>
    <div class="container">
        <div class="glass frosted_1">
            <h1>這裏裝得可愛一點。</h1>
        </div>
        <div class="glass frosted_2">
            <h1>這裏裝得可愛一點。</h1>
        </div>
        <div class="glass frosted_3">
            <h1>這裏裝得可愛一點。</h1>
        </div>
    </div>
</body>

</html>
```

关于 CSS设置磨砂玻璃就和小伙伴们分享到这里，生活加油 `^_^` 

## 博文参考
***

https://silentlad.com/frosted-glass-effect-using-css


https://developer.mozilla.org/en-US/search?q=-webkit-backdrop-filter

https://developer.mozilla.org/en-US/docs/Web/CSS/filter


