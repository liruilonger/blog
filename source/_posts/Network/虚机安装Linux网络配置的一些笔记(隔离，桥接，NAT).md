---
title: 虚机安装Linux网络配置的一些笔记(隔离，桥接，NAT)
tags:
  - Linux
  - 网络
categories:
  - Linux
toc: true
recommend: 1
keywords: Linux
uniqueId: '2021-09-16 12:45:05/虚机安装Linux网络配置的一些笔记(隔离，桥接，NAT).html'
mathJax: false
date: 2021-09-16 20:45:05
thumbnail:
---
**<font color="009688">一个成熟的人没有任何职责，除了这个：寻找自己，坚定地成为自己，不论走向何方，都往前探索自己的路。——赫尔曼·黑塞《德米安》</font>**
<!-- more -->
### <font color="#F56C6C">我的需求：</font>
 记得最开始学Linux的时候，使用VM虚拟机安装，配置网络，希望可以和主机互通，同时希望可以访问外网，改配置文件，照着网上的博客，改了又改，捣鼓了好几天也弄不好。

### <font color="#F56C6C">我需要解决的问题：</font>
后来工作了慢慢会了，而且网络的管理工具也变了，对于centos来讲，`network.service`变成了`NetworkManager.service`,配置方式更多的是通过命令，或者UI界面的方式去配置，一般不去修改配置文件了，这里和小伙伴讲解一下`linux的网络配置`，希望小伙伴们学的时候不会乱糟糟的改配置文件，同时对于`虚拟网络类型`有个大概的了解。
### <font color="#F56C6C">我是这样做的：</font>
我们这里使用的linux的版本为 **<font color=chocolate>Centos 7 </font>** ，博文主要讲解：
 
+ Linu虚拟网络配置的常用方
+ Linxu常用的网络配置命令

***



**<font color="009688">一个成熟的人没有任何职责，除了这个：寻找自己，坚定地成为自己，不论走向何方，都往前探索自己的路。——赫尔曼·黑塞《德米安》</font>**
 ***

# <font color=seagreen>一、Linu虚拟网络配置的常用方式</font>

## <font color=purple>1、桥接模式</font>
### <font color=seagreen>1、为什么要叫桥接模式？</font>
**<font color=royalblue>关于桥接模式，是设计模式的一种，了解过设计模式的小伙伴应该不陌生，这里简单回忆下，没有了解的直接忽略，不重要。</font>**

|桥接模式，桥接(Bridge)是用于把`抽象化与实现化解耦`，使得二者可以独立变化。`它通过提供抽象化和实现化之间的桥接结构`，来实现二者的解耦，我们看一个java的Demo|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/20210511034528299.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|
|**<font color=chocolate>使用相同的抽象类方法但是不同的桥接实现类，来画出不同颜色的圆</font>**|

##### `创建桥接实现接口DrawAPI.java`
```java
public interface DrawAPI {
   public void drawCircle(int radius, int x, int y);
}
```
##### `创建实现了 DrawAPI 接口的实体桥接实现类。RedCircle.java,GreenCircle.java`
```java
public class RedCircle implements DrawAPI {
   @Override
   public void drawCircle(int radius, int x, int y) {
      System.out.println("Drawing Circle[ color: red, radius: "
         + radius +", x: " +x+", "+ y +"]");
   }
}
public class GreenCircle implements DrawAPI {
   @Override
   public void drawCircle(int radius, int x, int y) {
      System.out.println("Drawing Circle[ color: green, radius: "
         + radius +", x: " +x+", "+ y +"]");
   }
}
```
##### `使用 DrawAPI 接口创建抽象类 Shape。Shape.java`
```java
public abstract class Shape {
   protected DrawAPI drawAPI;
   protected Shape(DrawAPI drawAPI){
      this.drawAPI = drawAPI;
   }
   public abstract void draw();  
}
```
##### `创建实现了 Shape 抽象类的实体类。Circle.java`
```java
public class Circle extends Shape {
   private int x, y, radius;
 
   public Circle(int x, int y, int radius, DrawAPI drawAPI) {
      super(drawAPI);
      this.x = x;  
      this.y = y;  
      this.radius = radius;
   }
 
   public void draw() {
      drawAPI.drawCircle(radius,x,y);
   }
}
```

##### `使用 Shape 和 DrawAPI 类画出不同颜色的圆。BridgePatternDemo.java`
```java
public class BridgePatternDemo {
   public static void main(String[] args) {
      Shape redCircle = new Circle(100,100, 10, new RedCircle());
      Shape greenCircle = new Circle(100,100, 10, new GreenCircle());

      redCircle.draw();
      greenCircle.draw();
   }
}
```

**<font color=green>类比`虚拟机网络连接`这一块，linux系统联网是一个抽象行为，他需要网卡才可以和外部连接，那么网卡就是他的一个桥接接口，我们通过网卡的实现类来实现网络的互通，这里我们有两种网卡，真机的物理网卡， 虚机的虚拟网卡，所以有两个桥接实现类，通过桥接模式，我们解耦了联网这种抽象行为和具体网卡的联网实现。</font>**

### <font color=amber>2、桥接模式可以做什么</font>
**<font color=orange>下面回到正题，我们要通过桥接模式，实现这样一个场景，就那我们常用的笔记本来说，我要通过笔记本上的无线网卡连接公网，然后通过虚机配置桥接模式，实现我的虚机可以`ping通真机，也可以ping通公网，同时多个虚机可以ping通`.</font>**
![在这里插入图片描述](https://img-blog.csdnimg.cn/94051ec56edf417699b228aac4607668.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

**<font color=seagreen>在实际的工作中，我们需要一个`交换机`可以完成这样的需求，`VM的桥接模式帮我们虚拟了一个交换机`，配置桥接模式后，我们虚机和真机就位于一个网段内，且掩码相同，彼此可以ping通。</font>**

![在这里插入图片描述](https://img-blog.csdnimg.cn/f9c1fbb6850344948203dc15de7fe871.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

**<font color=amber>虚机网卡 与 真机网卡 通过桥接实现；桥接物理网卡，相当于直连到 真机 所在网络；配置桥接模式后：</font>**


**<font color=royalblue>真机网卡信息,使用的无线网卡，ip为192.168.1.4</font>**
![在这里插入图片描述](https://img-blog.csdnimg.cn/b6b15babf9b4487cb17f6dae21c8e344.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)


**<font color=seagreen>虚机网卡信息,linux网关查看，</font>** 、 **<font color=blue>可以看出虚机的网络ip为：192.168.1.10 </font>** 

```bash
┌──[root@master]-[~]
└─$ ifconfig ens33 | head -2
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.10  netmask 255.255.255.0  broadcast 192.168.1.255
┌──[root@master]-[~]
└─$  route -n | grep ens33
0.0.0.0         192.168.1.1     0.0.0.0         UG    100    0        0 ens33
192.168.1.0     0.0.0.0         255.255.255.0   U     100    0        0 ens33
┌──[root@master]-[~]
└─$  route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         192.168.1.1     0.0.0.0         UG    100    0        0 ens33
10.254.0.0      0.0.0.0         255.255.0.0     U     0      0        0 flannel.1
192.168.1.0     0.0.0.0         255.255.255.0   U     100    0        0 ens33
192.168.122.0   0.0.0.0         255.255.255.0   U     0      0        0 virbr0
┌──[root@master]-[~]
└─$
```
+ 网关第一行的意思就是去往所有目标地址数据包由网关192.168.1.1  通过网卡ens33来转发；0.0.0.0代表的是匹配所有目标地址
+ 网关第二行：意思就是去往10.254.0.0  地址的数据包通过flannel.1网桥设备来转发

在它显示的信息中，如果标志是 U，则说明是可达路由(活动的)；如果是 G，则说明这个网络接口连接的是网关，H则说明目标是一个主机。

### <font color=tomato>3. 桥接模式如何配置：</font>

**<font color=royalblue>说了这么多，来具体操作一下，桥接模式到底要怎么配置：</font>**


|**<font color=green>&&&&&&&&&&&&&&&&&&配置网络步骤&&&&&&&&&&&&&&&&&&</font>**|
|:--:|
|![](https://img-blog.csdnimg.cn/85d3cf2e418b498e8cb51b7fd02b6d35.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![](https://img-blog.csdnimg.cn/ebaa0696135344e196583d0ed5ab54f2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![](https://img-blog.csdnimg.cn/f55822c3c3664b5abb70c621e87d1912.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![](https://img-blog.csdnimg.cn/f346162fecae454597615b91e7143075.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=purple>桥接模式下，要自己选择桥接到哪个网卡(实际联网用的网卡)，然后确认</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/adae0950316541df9cfafec5e3e9b7f2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![](https://img-blog.csdnimg.cn/bff89aed281148a9b9b55e4a989e415b.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=plum>配置网卡为DHCP模式(自动分配IP地址):执行方式见表尾，这里值得一说的是，如果网络换了，那么所以有的节点ip也会换掉，因为是动态的，但是还是在一个网段内。DNS和SSH免密也都不能用了，需要重新配置，但是如果你只连一个网络，那就没影响。</font>**|
|`nmcli connection modify 'ens33' ipv4.method auto   connection.autoconnect yes #将网卡改为DHCP模式(动态分配IP)`，`nmcli connection up 'ens33'`|
|![](https://img-blog.csdnimg.cn/2ab459d9831345ebb48acc5f030d5e9d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![](https://img-blog.csdnimg.cn/b570744e771847f1a1b33d6f61e5c4ee.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

**<font color=purple>配置网卡为DHCP模式(自动分配IP地址)</font>**
```bash

┌──[root@localhost.localdomain]-[~] 
└─$ nmcli connection modify 'ens33' ipv4.method auto   connection.autoconnect yes
┌──[root@localhost.localdomain]-[~] 
└─$ nmcli connection up 'ens33'
连接已成功激活(D-Bus 活动路径：/org/freedesktop/NetworkManager/ActiveConnection/4)
┌──[root@localhost.localdomain]-[~] 
└─$ ifconfig | head -2 
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.7  netmask 255.255.255.0  broadcast 192.168.1.255
┌──[root@localhost.localdomain]-[~] 
└─$ 
```
```bash
┌──[root@192.168.1.7]-[~] 
└─$ ifconfig
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.7  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::8899:b0c7:4b50:73e0  prefixlen 64  scopeid 0x20<link>
        inet6 240e:319:707:b800:2929:3ab2:f378:715a  prefixlen 64  scopeid 0x0<global>
        ether 00:0c:29:b6:a6:52  txqueuelen 1000  (Ethernet)
        RX packets 535119  bytes 797946990 (760.9 MiB)
        RX errors 0  dropped 96  overruns 0  frame 0
        TX packets 59958  bytes 4119314 (3.9 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 616  bytes 53248 (52.0 KiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 616  bytes 53248 (52.0 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255
        ether 52:54:00:2e:66:6d  txqueuelen 1000  (Ethernet)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

┌──[root@192.168.1.7]-[~] 
└─$ 
```
当然，桥接模式特别方便，但是也有一个弊端，就是当更换网络时，ip都会变化，关于这一点，我们可以使用`NAT模式`,这个我们最后讲。


## <font color=blue>2、隔离模式</font>

### <font color=green>1、什么是隔离模式，隔离模式能做什么</font>
**<font color=blue>关于隔离模式，顾名思义，就是虚机可以ping通真机上的上的其他虚机，也可以ping通真机，但是不能ping通外网。</font>**

|这里如果小伙使用vm虚拟机，那么在装好系统以后会自动生成两个虚拟网卡，vmnet1和vmnet8|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/d5caa0a435f94963b84090d5ae8f45a8.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=amber>我们可以使用vmnet1或者vmnet8虚拟网卡来实现隔离模式的网络配置。</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/a50d03ed8aec4b3095b6f05c9ab2db42.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|


### <font color=blue>2、如何配置隔离模式</font>


|下面和小伙伴分享隔离模式如何配置|
|--|
|**<font color=green>这里我们使用vmnet1 配置网络,配置虚拟网卡</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/1b322a4c3b724a99b286701bca3bed06.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=tomato>设置网络为vmnet1</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/ce4f843d7e794315bcffebc2e3604ee4.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|`nmcli connection modify 'ens33' ipv4.method auto connection.autoconnect yes`,`nmcli connection up 'ens33'`,这<font color=seagreen>里使用静态的也可以，一般也是设置成静态，这里为了方便</font>.|
|**<font color=seagreen>静态配置方式，注意ip和掩码</font>** `nmcli connection modify 'ens33' ipv4.method manual ipv4.addresses 192.168.4.12/24 connection.autoconnect yes`,`nmcli connection up 'ens33'`|
|**<font color=yellowgreen>到这里我们已经基于隔离模式配置好了网网络，虚机看看网卡信息，测试一下</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/6c3fa8d6f3d64b17ba39cb4bc0c2653f.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=tomato>这里如果不通的话，需要关闭真机的防火墙试试</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/280e6a1ba1f748b9a276608d029c5491.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/f61c6c38c5014bf78239b43fdbf75841.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=brown>嗯，然后做真机测试一下</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/1088db03a6f147acace8394e8595cb95.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|



**<font color=brown>虚机网卡配置</font>**
```bash
──[root@master]-[~] 
└─$ nmcli connection modify 'ens33' ipv4.method auto connection.autoconnect yes
┌──[root@master]-[~] 
└─$ nmcli connection up 'ens33'
连接已成功激活(D-Bus 活动路径：/org/freedesktop/NetworkManager/ActiveConnection/8)
┌──[root@master]-[~] 
└─$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: ens33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:0c:29:5e:c9:7f brd ff:ff:ff:ff:ff:ff
    inet 192.168.5.128/24 brd 192.168.5.255 scope global noprefixroute dynamic ens33
       valid_lft 1798sec preferred_lft 1798sec
    inet 192.168.0.106/32 brd 192.168.0.106 scope global noprefixroute ens33
       valid_lft forever preferred_lft forever
    inet6 fe80::8899:b0c7:4b50:73e0/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
3: virbr0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default qlen 1000
    link/ether 52:54:00:2e:66:6d brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.1/24 brd 192.168.122.255 scope global virbr0
       valid_lft forever preferred_lft forever
4: virbr0-nic: <BROADCAST,MULTICAST> mtu 1500 qdisc pfifo_fast master virbr0 state DOWN group default qlen 1000
    link/ether 52:54:00:2e:66:6d brd ff:ff:ff:ff:ff:ff
┌──[root@master]-[~] 
└─$ 
┌──[root@master]-[~]
└─$ 

```

## <font color=blue>3、NAT模式</font>

### <font color=green>1、什么是NAT模式，NAT模式能做什么</font>
NAT模式借助虚拟NAT设备和虚拟DHCP服务器，使得虚拟机可以联网.

在NAT模式中，主机网卡直接与虚拟NAT设备相连，然后虚拟NAT设备与虚拟DHCP服务器一起连接在虚拟交换机VMnet8上，这样就实现了虚拟机联网。这里的`VMware Network Adapter VMnet8虚拟网卡主要是为了实现主机与虚拟机之间的通信`

使用NAT模式，可以实现上面桥接模式的功能，同时又不需要考虑IP的问题

### <font color=green>2、NAT模式的配置</font>
||
|--|
|**<font color=yellowgreen>配置真机vm8网卡，添加ip，用于和虚机通信</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/78417a0efb664f0fb124df4aa28cc1de.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=amber>配置虚机为NAT模式，添加子网IP</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/a7b11b2d6d334cc9b171b7471b10c3ff.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=red>配置虚机IP</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/e21cadec13c043e89c3bee8b37185824.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
然后启动虚机，配置ip这里我们写了一个脚本配置，直接 运行脚本 + ip最后一位。
```bash
#!/bin/bash
if [ $# -eq 0 ]; then
        echo "usage: `basename $0` num"
        exit 1
fi
[[ $1 =~ ^[0-9]+$ ]]
if [ $? -ne 0 ]; then
        echo "usage: `basename $0` 10~240"
        exit 1
fi

cat > /etc/sysconfig/network-scripts/ifcfg-ens32 <<EOF
TYPE=Ethernet
BOOTPROTO=none
NAME=ens32
DEVICE=ens32
ONBOOT=yes
IPADDR=192.168.26.${1}
NETMASK=255.255.255.0
GATEWAY=192.168.26.2
DNS1=192.168.26.2
EOF

systemctl restart network &> /dev/null
ip=$(ifconfig ens32 | awk '/inet /{print $2}')
sed -i '/192/d' /etc/issue
echo $ip
echo $ip >> /etc/issue
hostnamectl set-hostname vms${1}.liruilongs.github.io
echo "192.168.26.${1} vms${1}.rhce.cc vms${1}"  >> /etc/hosts
```
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ ifconfig ens32
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.55  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:fec9:6fae  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:c9:6f:ae  txqueuelen 1000  (Ethernet)
        RX packets 217344  bytes 305924645 (291.7 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 31035  bytes 2482366 (2.3 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

┌──[root@liruilongs.github.io]-[~]
└─$
```
# <font color=seagreen>二、linxu常用的网络配置命令</font>

## <font color=green>1、配置静态主机名</font>

**<font color=tomato>linux主机名的配置在配置文件 /etc/hostname 这里需要注意的是 一般的命里提示服配置的都是`-h`即短的主机名，只会显示`·`前面的部分，要想全部显示，需要修改为`-H`</font>**
```bash
E:\docker>ssh  root@39.97.241.18
Last failed login: Thu Sep 16 19:46:40 CST 2021 from 47.106.250.53 on ssh:notty
There were 3 failed login attempts since the last successful login.
Last login: Thu Sep 16 17:48:01 2021 from 121.57.15.165

Welcome to Alibaba Cloud Elastic Compute Service !

How would you spend your life?.I don't know, but I will cherish every minute to live.
```
```bash
[root@liruilong ~]# echo liruilongs.github.io > /etc/hostname
[root@liruilong ~]# cat /etc/hostname
liruilongs.github.io
[root@liruilong ~]#
[root@liruilong ~]# echo liruilongs.github.io > /etc/hostname #重定向将主机名写入配置文件，重启系统后才会生效
[root@liruilong ~]# cat /etc/hostname #查看配置文件
liruilongs.github.io
```
```bash
##临时设置的主机名，重新开一个终端即可生效 Ctrl + Shift + T
[root@liruilong ~]# hostname liruilongs.github.io #临时设置主机名
[root@liruilong ~]# hostname #查看当前系统的主机名
liruilongs.github.io
[root@liruilong ~]#

```
## <font color=seagreen>2. NetworkManager </font>


`NetworkManager`主要管理2个对象：**<font color=red>Connection(网卡连接配置) 和 Device(网卡设备)</font>**，他们之间是多对一的关系，但是同一时刻只能有一个Connection对于Device才生效。这里理解的话，我们要配置linux网络的话，主要就是使用的这个配置的。

在 **<font color=tomato>RHEL 8/Centos 8</font>** 有四种方法配置网络：

+ 通过`nmcli connection add`命令配置，会自动生成`ifcfg文件`。我们上面用的就是这种。
+ 手动配置`ifcfg`文件，通过`nmcli connection reload`来加载生效。
+ 手动配置`ifcfg`文件，通过传统`network.service`来加载生效。
+ 通过`nmtui `以图形化的方式配置

### <font color=orange>启动NetworkManager 服务</font>

```bash
[root@liruilong ~]# nmcli connection show
Error: NetworkManager is not running.
[root@liruilong ~]# systemctl start  NetworkManager
[root@liruilong ~]# systemctl status  NetworkManager
● NetworkManager.service - Network Manager
   Loaded: loaded (/usr/lib/systemd/system/NetworkManager.service; disabled; vendor preset: enabled)
   Active: active (running) since Thu 2021-09-16 21:30:52 CST; 7s ago
     Docs: man:NetworkManager(8)
 Main PID: 997 (NetworkManager)
    Tasks: 5
   Memory: 12.0M
   CGroup: /system.slice/NetworkManager.service
           ├─ 997 /usr/sbin/NetworkManager --no-daemon
           └─1005 /sbin/dhclient -d -q -sf /usr/libexec/nm-dhcp-helper -pf /var/run/dhclient-eth0.pid -lf /var/lib/Ne...

Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9624] dhcp4 (eth0):   nameserver '1...138'
Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9625] dhcp4 (eth0): state changed u...ound
Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9635] device (eth0): state change: ...me')
Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9646] device (eth0): state change: ...me')
Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9648] device (eth0): state change: ...me')
Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9651] manager: NetworkManager state...SITE
Sep 16 21:30:52 liruilongs.github.io dhclient[1005]: bound to 172.17.57.70 -- renewal in 154114011 seconds.
Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9767] device (eth0): Activation: su...ted.
Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9770] manager: NetworkManager state...OBAL
Sep 16 21:30:52 liruilongs.github.io NetworkManager[997]: <info>  [1631799052.9774] manager: startup complete
Hint: Some lines were ellipsized, use -l to show in full.
[root@liruilong ~]#  
```
### <font color=camel>1、查看网络连接</font>

**<font color=green>使用 show 指令</font>**

+ `nmcli connection show`
+ `nmcli connection show "连接名"`

```bash
[root@liruilong ~]# nmcli connection show #显示网卡设备
[root@liruilong ~]# ifconfig | head -3 #查看网卡信息
[root@liruilong ~]# nmcli connection show ens33 #查看网卡的详细信息
```
```bash
[root@liruilong ~]# nmcli connection show
NAME             UUID                                  TYPE      DEVICE
System eth0      5fb06bd0-0bb0-7ffb-45f1-d6edd65f3e03  ethernet  eth0
br-3e7fae020360  9fbbc0c9-8da1-482e-b443-014900ac1ec0  bridge    br-3e7fae020360
[root@liruilong ~]# ifconfig | head -3
br-3e7fae020360: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
        inet 172.19.0.1  netmask 255.255.0.0  broadcast 0.0.0.0
        ether 02:42:65:a1:d1:61  txqueuelen 0  (Ethernet)
[root@liruilong ~]# nmcli connection show "System eth0"
connection.id:                          System eth0
connection.uuid:                        5fb06bd0-0bb0-7ffb-45f1-d6edd65f3e03
connection.stable-id:                   --
connection.type:                        802-3-ethernet
connection.interface-name:              eth0
connection.autoconnect:                 yes
connection.autoconnect-priority:        0
connection.autoconnect-retries:         -1 (default)
connection.multi-connect:               0 (default)
connection.auth-retries:                -1
。。。。。。。
```

### <font color=seagreen>2、修改网络连接配置</font>

**<font color=camel>使用 modify 指令</font>**

```bash
nmcli connection modify "连接名" 参数1 值1 ....
```
>#常用参数：
ipv4.method auo|manual
ipv4.addresses "IP地址/掩码长度"
ipv4.gateway 网关地址
connection.autoconnect yes | no

```bash
#给网卡ens33配置ip地址，子网掩码，网关，并配置开机自启动
[root@liruilong ~]# nmcli connection modify 'ens33' ipv4.method manual
ipv4.addresses 192.168.4.7/24 ipv4.gateway 192.168.4.254 connection.autoconnect
yes
[root@liruilong ~]# nmcli connection up 'ens33' #激活网卡
连接已成功激活(D-Bus 活动路径：/org/freedesktop/NetworkManager/ActiveConnection/4)
[root@liruilong ~]# ifconfig | head -3 #查看网卡配置信息
ens33: flags=4163<UP,BROADCAST,RUNNING,MULTICAST> mtu 1500
inet 192.168.4.7 netmask 255.255.255.0 broadcast 192.168.4.255
inet6 fe80::deea:10bb:4b70:a959 prefixlen 64 scopeid 0x20<link>
[root@liruilong ~]# route -n #查看网关
Kernel IP routing table
Destination Gateway Genmask Flags Metric Ref Use Iface
0.0.0.0 192.168.4.254 0.0.0.0 UG 100 0 0 ens33
192.168.4.0 0.0.0.0 255.255.255.0 U 100 0 0 ens33
192.168.122.0 0.0.0.0 255.255.255.0 U 0 0 0 virbr0
```

### <font color=camel>3、激活/禁用网络连接</font>

```bash
[root@liruilong ~]# nmcli connection up "ens33" #激活网卡ens33
[root@liruilong ~]# nmcli connection down "ens33" #取消激活网卡ens33
```
## <font color=orange>3.为本机指定 DNS 服务器</font>

在配置文件配置，有时间小伙访问ip能够访问，但是访问域名却出了问题，这有可能是 DNS的问题，无法解析域名。

**<font color=purple>配置DNS服务器 在配置文件 `/etc/resolv.conf` 中，配置方式为` nameserver DNS服务器地址`</font>**

```bash
[root@liruilong ~]# vim /etc/resolv.conf
nameserver 172.25.254.254 #设置DNS服务器地址
[root@liruilong ~]# echo nameserver 192.168.4.7 > /etc/resolv.conf
[root@liruilong ~]# cat /etc/resolv.conf
nameserver 192.168.4.7
```

**<font color=yellowgreen>network.service 的配置方式</font>**

```bash
[root@localhost ~]# vim /etc/sysconfig/network-scripts/ifcfg-ens33
DEVICE="ens33" #驱动名称,与ifconfig 看到的名称一致
ONBOOT="yes" #开机启动
NM_CONTROLLED="yes" #接受 NetworkManager 控制
TYPE="Ethernet" #类型
BOOTPROTO="static" #协议(dhcp|static|none)
IPADDR="192.168.4.7" #IP地址
NETMASK="255.255.255.0" #子网掩码
GATEWAY="192.168.4.254" #默认网关
DNS1="8.8.8.8" #DNS地址为可选内容，主要用于连接外网
DNS2="8.8.4.4"
[root@room9pc01 ~]# systemctl restart network #重启网络
```
***
### <font color=chocolate><center>后记</center></font>

关于linux的网络配置就讲到这里，当然，还有其他的配置方式，我们这里没有讲到，感兴趣的小伙伴赶快尝试吧！
