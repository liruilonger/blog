---
title: 关于 Linux中systemd的一些笔记
tags:
  - systemctl
  - Linux
categories:
  - Linux
toc: true
recommend: 1
keywords: Linux
uniqueId: '2022-04-03 03:12:18/关于 Linux中systemd操作Service unit的一些笔记.html'
mathJax: false
date: 2022-04-03 11:12:18
thumbnail:
---


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>
<!-- more -->
## 写在前面
***

+ 嗯，准备`RHCA`，学习整理这部分知识
+ 博文内容涉及：
   + `systemd`简述
   + 对于`unit`的信息的介绍
   + 通过`systemctl`命令控制`Service unit`的`Demo`
   + `Service unit`配置文件内容,权值的一些介绍




**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

## 一、Systemd 简述

`systemd`是Linux操作系统的`系统和服务管理器`。在系统启动时或者正在运行的系统上的一个`守护进程`,`systemd`负责`激活系统资源、服务器守护程序和其他进程`，`systemd`是`RHEL 7`之后出现的。

`守护进程(daemon)`是在后台执行各种任务的进程。通常,守护程序在引导时自动启动并继续运行直到系统关闭或手动停止。许多守护程序的名称的约定以`字母d结尾`。

关于`RHEL`启动进程管理：

+ 在`RHEL 5`的时候，使用` Sys init`，启动速度最慢的,无论进程相互之间有无依赖关系,都是`串行启动过程`,
+ `RHEL 6`的时候，使用` Upstart init`相对启动速度快一点有所改进。`有依赖的进程之间依次启动`而其他与之没有依赖关系的则`并行同步启动`。
+ `RHEL 7 Systemd`与以上都不同。所有进程无论`有无依赖关系`则都是`并行启动`(很多时候进程没有真正启动而是只有一个信号或者说是标记而已,在真正利用的时候才会真正启动)。

`Systemd`为了解决上文的问题而诞生。它的目标是,为系统的启动和管理提供一套完整的解决方案, 


服务通常包含一个或多个守护进程。

`RHEL`启动的`第一个进程(PID 1)`就是`systemd`,提供的功能包括:

+ `并行化`功能(同时启动多个服务),可提高系统的启动速度。
+ `按需启动`后台程序而无需单独的服务。
+ `自动管理服务依赖关系`,可以防止长时间超时。例如,网络相关服务在网络可用之前不会尝试启动。
+ 使用`Linux控制组(Cgroup)`一起跟踪相关进程的方法。

![Systemd 架构图](https://img-blog.csdnimg.cn/e802f9b8a9074e0b8c0440da8bddef3f.png)

systemd Targets 启动级别

## 二、systemctl 命令管理系统

`systemctl`是` Systemd` 的主命令，`systemctl`命令用于管理不同类型的系统对象,这些对象称之为`units`。

+ `Service unit`:用于定义`系统服务`,文件扩展名为`.service`
+ `Target unit`:用于模拟实现“`运行级别`”,文件扩展名为`.target`,可以理解`Target `就是一个 `Unit` 组，包含许多相关的 `Unit `
+ `Device unit`:用于定义内核识别的`设备`,文件扩展名为`.device`
+ `Mount unit:`用于定义文件系统`挂载点`,文件扩展名为`.mount`
+ `Socket unit`:用于标识`进程间通信`用的socket文件,文件扩展名为`.socket`
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status cockpit.socket #CocKpit驾驶舱 和SSH类似，用于远程控制，类似于阿里云的控制台。为初级管理员提供基本操作，通过web端的控制台，访问地址：服务器IP:9090
● cockpit.socket - Cockpit Web Service Socket
   Loaded: loaded (/usr/lib/systemd/system/cockpit.socket; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:cockpit-ws(8)
   Listen: [::]:9090 (Stream)
```
+ `Snapshot unit`:管理`系统快照`,文件扩展名为`.snapshot`
+ `Swap unit`:用于标识`swap设备`,文件扩展名为`.swap`
+ `Automount unit`:文件系统的`自动挂载点`,文件扩展名为`.automount`
+ `Path uni`t:用于根据文件系统上特定对象的变化来启动其他服务,文件扩展名为`.path`
+ `Timer unit`:用于管理`计划任务`,文件扩展名为`.timer`
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status systemd-tmpfiles-clean.timer #用于定时清理 `/tmp`,`/var/tmp`之类的临时文件
● systemd-tmpfiles-clean.timer - Daily Cleanup of Temporary Directories
   Loaded: loaded (/usr/lib/systemd/system/systemd-tmpfiles-clean.timer; static; vendor preset: disabl>
   Active: active (waiting) since Sun 2022-04-03 11:24:00 CST; 1h 48min ago
  Trigger: Mon 2022-04-04 11:39:07 CST; 22h left
     Docs: man:tmpfiles.d(5)
           man:systemd-tmpfiles(8)

Apr 03 11:24:00 liruilongs.github.io systemd[1]: Started Daily Cleanup of Temporary Directories.
┌──[root@liruilongs.github.io]-[~]
└─$
```
+ `Slice unit`:用于`资源管理`,文件扩展名为`.slice`


### 1.查看units列表信息

列出`loaded`(配置文件以加载)的`units`
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-units
  UNIT                                               LOAD   ACTIVE SUB       DESCRIPTION
  proc-sys-fs-binfmt_misc.automount                  loaded active waiting   Arbitrary Executable File>
 .............
```
命令输出说明:
+ `UNIT`:服务单元名称。 
+ `LOAD`: `systemd`是否正确解析了单元的配置并将该单元`加载到内存中`。
+ `ACTIVE`:单元的高级别激活状态。此信息表明单元`是否已成功启动`。
+ `SUB`:单元的`低级别激活状态`。此信息指示有关该单完的更多详细信息。信息视单元类型、状态以及单元的执行方式而异。
+ `DESCRIPTION`:单元的简短`描述`。 

![在这里插入图片描述](https://img-blog.csdnimg.cn/87aff0f3cb11441dbdb7975d864c5d92.png)

#### 列出全部的units
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-unit-files
UNIT FILE                                  STATE
proc-sys-fs-binfmt_misc.automount          static
-.mount                                    generated
boot.mount                                 generated
dev-hugepages.mount                        static
dev-mqueue.mount                           static
proc-sys-fs-binfmt_misc.mount              static
sys-fs-fuse-connections.mount              static
sys-kernel-config.mount                    static
sys-kernel-debug.mount                     static
tmp.mount                                  disabled
systemd-ask-password-console.path          static
.....
```
#### 查看指定的系统对象

查看timer，系统定时任务
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-units -i timer
0 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'.
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-unit-files -t timer
UNIT FILE                    STATE
dnf-makecache.timer          enabled  #yaml仓库定时更新
fstrim.timer                 disabled  
systemd-tmpfiles-clean.timer static  # 临时文件清理
unbound-anchor.timer         enabled

4 unit files listed.
┌──[root@liruilongs.github.io]-[~]
└─$
```
查看socket套接字
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-unit-files -t socket
UNIT FILE                               STATE
cockpit-wsinstance-http-redirect.socket static
cockpit-wsinstance-http.socket          static
cockpit-wsinstance-https-factory.socket static
cockpit-wsinstance-https@.socket        static
cockpit.socket                          enabled
dbus.socket                             static
```
列出类型为`service`,状态为`loaded`和`active`的 unit
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-units -t service
  UNIT                                                 LOAD   ACTIVE SUB     DESCRIPTION
  auditd.service                                       loaded active running Security Auditing Service
```
列出类型为`service`,状态为`active`和`inactive` 的unit
```bash
┌──[root@liruilongs.github.io]-[~]
└─$  systemctl list-units --type service --all
```
#### 查看失败的服务
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl --failed -t service
  UNIT          LOAD   ACTIVE SUB    DESCRIPTION
● kdump.service loaded failed failed Crash recovery kernel arming
.....
```

## 三、systemctl操作Service unit

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status sshd 
● sshd.service - OpenSSH server daemon  #单元的描述信息
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled) #子状态,加载的配置文件,默认开机自启
   Active: active (running) since Sun 2022-04-03 11:24:03 CST; 2h 16min ago #运行状态，时间
     Docs: man:sshd(8)  #帮助文档
           man:sshd_config(5)
 Main PID: 880 (sshd) #主进程
    Tasks: 1 (limit: 6044) # 任务量 
   Memory: 7.2M   #消耗的内存
   CGroup: /system.slice/sshd.service  # 用的什么slice
           └─880 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes2>
Apr 03 11:24:02 liruilongs.github.io systemd[1]: Starting OpenSSH server daemon...  #启动日志，什么进程引导激活的
Apr 03 11:24:03 liruilongs.github.io sshd[880]: Server listening on 0.0.0.0 port 22. #监听的端口
Apr 03 11:24:03 liruilongs.github.io sshd[880]: Server listening on :: port 22.
Apr 03 11:24:03 liruilongs.github.io systemd[1]: Started OpenSSH server daemon.  #启动服务
Apr 03 11:25:30 liruilongs.github.io sshd[1112]: Accepted publickey for root from 172.25.254.254 port > #连接日志
Apr 03 11:25:30 liruilongs.github.io sshd[1112]: pam_unix(sshd:session): session opened for user root >
lines 1-17/17 (END)
```
#### 查看Service单元状态信息
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl is-active sshd
active
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl is-enabled sshd
enabled
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl is-
is-active          is-enabled         is-failed          is-system-running
```
#### 配置文件加载

**优先级从高到低**

+ 本地配置的系统单元: `/etc/systemd/system`
+ 运行时配置的系统单元: `/run/systemd/system`
+ 软件包安装的系统单元: `/usr/lib/systemd/system`

默认的服务配置文件位置：`/usr/lib/systemd/system`
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status sshd | head  -2
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
```
添加高优先级配置文件：`/run/systemd/system`
```
┌──[root@liruilongs.github.io]-[~]
└─$ cp /usr/lib/systemd/system/sshd.service /run/systemd/system/sshd.service
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl daemon-reload
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status sshd | head  -2
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/run/systemd/system/sshd.service; enabled; vendor preset: enabled)
```
添加高优先级配置文件，`/etc/systemd/system`
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cp /usr/lib/systemd/system/sshd.service /etc/systemd/system/
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl daemon-reload
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status sshd | head  -2
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/etc/systemd/system/sshd.service; enabled; vendor preset: enabled)
┌──[root@liruilongs.github.io]-[~]
└─$
```

#### 配置文件内容

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /usr/lib/systemd/system/sshd.service
[Unit]
Description=OpenSSH server daemon #描述信息
Documentation=man:sshd(8) man:sshd_config(5) #帮助文档
After=network.target sshd-keygen.target  #依赖关系,在...之后执行
Wants=sshd-keygen.target  # 强依赖

[Service] 
Type=notify  #notify类型
EnvironmentFile=-/etc/crypto-policies/back-ends/opensshserver.config  #环境变量读取
EnvironmentFile=-/etc/sysconfig/sshd   # - 号为文件可以不存在
ExecStart=/usr/sbin/sshd -D $OPTIONS $CRYPTO_POLICY  # 启动命令
ExecReload=/bin/kill -HUP $MAINPID  # reload从新初始化
KillMode=process  #设置在单元停止时，杀死进程的方法 process 表示仅杀死主进程
Restart=on-failure  #仅在服务进程异常退出时重启
RestartSec=42s # 42秒后重启

[Install]
WantedBy=multi-user.target #属于那个target
┌──[root@liruilongs.github.io]-[~]
└─$
```

`systemctl cat kubelet.service` 查看具体的Service单元相关文件

```bash
┌──[root@vms82.liruilongs.github.io]-[/usr/lib/systemd/system/kubelet.service.d]
└─$systemctl cat kubelet.service
# /usr/lib/systemd/system/kubelet.service
[Unit]
Description=kubelet: The Kubernetes Node Agent #描述信息
Documentation=https://kubernetes.io/docs/ #帮助文档
Wants=network-online.target   # 强依赖
After=network-online.target   #依赖关系,在network-online.target之后执行

[Service]
ExecStart=/usr/bin/kubelet  # 启动命令
Restart=always #重启策略：无条件的重启
StartLimitInterval=0 #设置单元的启动频率限制，没有限制
RestartSec=10  # 42秒后重启

[Install]
WantedBy=multi-user.target  #属于那个target

# /usr/lib/systemd/system/kubelet.service.d/10-kubeadm.conf
# Note: This dropin only works with kubeadm and kubelet v1.11+
[Service]
Environment="KUBELET_KUBECONFIG_ARGS=--bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf --pod-manifest-path=/etc/kubernetes/kube
Environment="KUBELET_CONFIG_ARGS=--config=/var/lib/kubelet/config.yaml"
# This is a file that "kubeadm init" and "kubeadm join" generates at runtime, populating the KUBELET_KUBEADM_ARGS variable dynamically
EnvironmentFile=-/var/lib/kubelet/kubeadm-flags.env
# This is a file that the user can use for overrides of the kubelet args as a last resort. Preferably, the user should use
# the .NodeRegistration.KubeletExtraArgs object in the configuration files instead. KUBELET_EXTRA_ARGS should be sourced from this file.
EnvironmentFile=-/etc/sysconfig/kubelet
ExecStart=
ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_CONFIG_ARGS $KUBELET_KUBEADM_ARGS $KUBELET_EXTRA_ARGS
```

手册查看,不错的翻译 [systemd.service 中文手册](http://www.jinbuguo.com/systemd/systemd.service.html)
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ man -k systemd
```

#### 控制Service unit
启动停止服务
```bash
systemctl start sshd.service 
systemctl stop sshd.service
```
重新加载配置文件
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl reload sshd #还是原来的进程，只是从新加载配置文件  
```
重启服务
```
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl restart  sshd #一个新的进程
```
查看服务状态
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Sun 2022-04-03 11:46:36 CST; 3h 5min ago
     Docs: man:httpd.service(8)
 Main PID: 1616 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 6044)
   Memory: 33.7M
   CGroup: /system.slice/httpd.service
           ├─1616 /usr/sbin/httpd -DFOREGROUND
           ├─1617 /usr/sbin/httpd -DFOREGROUND
           ├─1618 /usr/sbin/httpd -DFOREGROUND
           ├─1619 /usr/sbin/httpd -DFOREGROUND
           └─1620 /usr/sbin/httpd -DFOREGROUND

Apr 03 11:46:35 liruilongs.github.io systemd[1]: Starting The Apache HTTP Server...
Apr 03 11:46:36 liruilongs.github.io systemd[1]: Started The Apache HTTP Server.
Apr 03 11:46:36 liruilongs.github.io httpd[1616]: Server configured, listening on: port 80
```
设置开机自启，添加一个软链接
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl enable httpd --now #立刻激活
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl is-enabled httpd
enabled
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl is-active  httpd
active
┌──[root@liruilongs.github.io]-[~]
└─$
```
设置服务开机不启动
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl disable  httpd
Removed /etc/systemd/system/multi-user.target.wants/httpd.service.
```
服务禁用
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl mask httpd
Created symlink /etc/systemd/system/httpd.service → /dev/null.
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl start httpd
Failed to start httpd.service: Unit httpd.service is masked.
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status  httpd | head -2
● httpd.service
   Loaded: masked (Reason: Unit httpd.service is masked.)
```
可以删除软链接取消禁用
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ ll  /etc/systemd/system/httpd.service
lrwxrwxrwx. 1 root root 9 Apr  3 16:08 /etc/systemd/system/httpd.service -> /dev/null
┌──[root@liruilongs.github.io]-[~]
└─$ rm -r   /etc/systemd/system/httpd.service
rm: remove symbolic link '/etc/systemd/system/httpd.service'? y
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl daemon-reload
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl start httpd
```
可以通过`unmask`来取消禁用
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl mask httpd
Created symlink /etc/systemd/system/httpd.service → /dev/null.
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl unmask httpd
Removed /etc/systemd/system/httpd.service.
┌──[root@liruilongs.github.io]-[~]
└─$
```
### unit依赖
可以通过如下命令查看unit的启动依赖关系
```bash
systemctl list-dependencies cups.service 
systemctl list-dependencies cups.service --reverse
```
查看`httpd Service `的`正向依赖`，在httpd之前启动
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-dependencies httpd
httpd.service
● ├─-.mount
● ├─httpd-init.service
● ├─system.slice
● └─sysinit.target
●   ├─dev-hugepages.mount
●   ├─dev-mqueue.mount
●   ├─dracut-shutdown.service
●   ├─import-state.service
●   ├─iscsi-onboot.service
●   ├─kmod-static-nodes.service
......
......
┌──[root@liruilongs.github.io]-[~]
└─$
```
查看`httpd Service `的`反向依赖`，在httpd之后启动
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-dependencies httpd --reverse
httpd.service
● └─multi-user.target
●   └─graphical.target
```
查看`multi-user.target`的`正向依赖`是否有httpd
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl list-dependencies multi-user.target | grep httpd
● ├─httpd.service
```

***
嗯，关于`systemd`和小伙伴们分享到这里，这块涉及的内容很多，这里简单介绍，在`Linux`生态系统中，`systemd`被部署到了大多数的标准`Linux`发行版中，只有为数不多的几个发行版尚未部署。关于`systemd`启动过程、对其他unit的控制和管理、对系统运行级别等的控制，感兴趣小伙伴可以了解下。
