---
title: 关于Linux中自动化配置网络Team的一些笔记
tags:
  - Team
categories:
  - Team
toc: true
recommend: 1
keywords: Team
uniqueId: '2022-06-29 13:11:10/关于Linux中自动化配置网络Team的一些笔记.html'
mathJax: false
date: 2022-06-29 21:11:10
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***
Linux 版本
```bash
[root@servera /]# cat /etc/redhat-release
Red Hat Enterprise Linux release 8.1 (Ootpa)
[root@servera /]# uname -a
Linux servera.lab.example.com 4.18.0-147.5.1.el8_1.x86_64 #1 SMP Tue Jan 14 15:50:19 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
[root@servera /]#
```


关于Team小伙伴应该不陌生啦，

网络接口链路聚合具体的技术有很多，在红帽的发行版中，`RHEL5、RHEL6`中使用的是`Bonding`。而`RHEL7、RHEL8`使用`Teaming`用来实现网络接口链路聚合的功能，在`RHEL7、RHEL8`中，`teaming和bonding`它们是并存的，我们可以选择`Teaming`，也可以选`Bonding`。


通过`Teaming(网络组)`技术把同一台服务器上的多个物理网卡通过软件绑定成一个虚拟网卡(同理虚机中，将多个虚拟网卡绑定为一个逻辑网卡).

+ `对于外部网络而言`，这台服务器只有一个可用网络接口。对于任何应用程序和网络，这台服务器只有一个网络链接或者说只有一个可以访问的IP地址。

+ `对于服务器内部而言`，通过team将网络流量分散在多个网络接口上，从而实现故障转移或提高吞吐量。

常用命令
```bash
# 创建team虚拟接口卡
nmcli connection add con-name team0 ifname team0 type team team.runner activebackup autoconnect yes
# 修改team网络接口卡
nmcli connection modify team0 ipv4.method manual ipv4.addresses 1.1.1.1/24
# 停止网络接口卡
nmcli connection down team0
# 激活网络接口卡
nmcli connection up team0
# 为team虚拟网络接口卡配置实际的网络接口卡
nmcli connection add con-name port1 ifname eth1 slave-type team master team0 type ethernet 
nmcli connection add con-name port2 ifname eth2 slave-type team master team0 type ethernet
# 查看team负载状态 
teamdctl team0 state
```

```bash
我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》
```

`eth0`网络接口用于综合组网，练习的话会造成SSH断掉，所以我们用`eth1/eth2`这两个网络接口卡来学习
```bash
[root@servera /]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:00:fa:0a brd ff:ff:ff:ff:ff:ff
    inet 172.25.250.10/24 brd 172.25.250.255 scope global noprefixroute eth0
       valid_lft forever preferred_lft forever
    inet6 fe80::984:87d2:dba7:1007/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:01:fa:0a brd ff:ff:ff:ff:ff:ff
    inet 192.168.0.10/24 brd 192.168.0.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::d932:da41:591c:5c89/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: eth2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:02:fa:0a brd ff:ff:ff:ff:ff:ff
[root@servera /]#
```

使⽤ ansible-galaxy 命令验证系统⻆⾊是否可⽤。
```bash
[student@workstation netlink-automation]$ ansible-galaxy list
# /usr/share/ansible/roles
# /etc/ansible/roles
[WARNING]: - the configured path /home/student/.ansible/roles does not exist.
```
如果没有⻆⾊可⽤，请安装 rhel-system-roles 软件包。
```bash
[student@workstation netlink-automation]$ sudo yum -y install rhel-system-roles
[sudo] password for student:
Last metadata expiration check: 0:25:21 ago on Wed Jun 29 21:38:42 2022.
Dependencies resolved.
=====================================================================================================
 Package                 Architecture Version         Repository                                Size
=====================================================================================================
Installing:
 rhel-system-roles       noarch       1.0-9.el8       rhel-8.1-for-x86_64-appstream-rpms       174 k

Transaction Summary
。。。。。。。。。。。。。。
```

