---
title: 关于Linux中网络连接配置(NetworkManager)的一些笔记
tags:
  - NetworkManager
  - Linux
categories:
  - Linux
toc: true
recommend: 1
keywords: Linux
uniqueId: '2022-04-04 12:45:10/关于Linux中网络连接配置(NetworkManager)的一些笔记.html'
mathJax: false
date: 2022-04-04 20:45:10
thumbnail:
---
**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>)
<!-- more -->
## 写在前面
***

+ 嗯，准备`RHCA`，学习整理这部分知识
+ `NetworkManager`其实是`RHCAS`的内容
+ 博文内容为常见的配置操作回顾：
   + `NetworkManager`简述
   + 命令行的方式配置网络连接
   + 修改配置文件的方式配置网络连接
   + 谁有权限修改网络连接配置



**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>
***
红帽8(`rhel8`,`Centos 8`)使用的网络管理服务有两个：`NetworkManager`和`network`。`rhel8`默认使用`NetworkManager`服务管理网络。如果要使用`network`,需要单独启动服务处理，`network`的启动是通过之前的`init`文件来引导的。

常见的配置网络方法：

+ 通过`nmcli connection add`命令配置，会自动生成`ifcfg文件`。
+ 手动配置`ifcfg-name`文件，通过`nmcli connection reload`来加载生效。
+ 手动配置`ifcfg-name`文件，通过旧服务`network.service`来加载生效。
+ 通过`nmtui `以图形化的方式配置

不管那种方式，基本都是就围绕`NetworkManager`服务展开，除了使用旧的服务单元`network.service`
```bash
┌──[root@liruilongs.github.io]-[~]
└─$systemctl status NetworkManager
● NetworkManager.service - Network Manager
   Loaded: loaded (/usr/lib/systemd/system/NetworkManager.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2022-04-11 21:10:36 CST; 1h 24min ago
     Docs: man:NetworkManager(8)
 Main PID: 1139 (NetworkManager)
    Tasks: 3 (limit: 37720)
   Memory: 8.9M
   CGroup: /system.slice/NetworkManager.service
           └─1139 /usr/sbin/NetworkManager --no-daemon

Apr 11 22:12:28 liruilongs.github.io NetworkManager[1139]: <info>  [1649686348.7137] device (vnet1): Activation: starti>
....
```
## NetworkManager简述

基于`NetworkManager`的系统`守护进程`，主要负责管理`网络接口(device)`和`连接配置(connection)`。它`监视`和`管理`网络设置，并使用`/etc/sysconfig/networkscripts/`目录中的文件来存储它们。


在`NetworkManager`中,设备是`网络接口`。`连接`是可以为设备配置的设置的集合。任何设备在同一时间只有一个连接是活动的。可能存在多个连接，用于不同设备的使用，或者允许对同一设备的配置进行更改。

每个连接都有`一个名称或ID`来标识它。`/etc/sysconfig/network-scripts/ifcfg-name`文件存储连接的持久配置，其中`name`是连接的名称。当连接名中有空格时，文件名中的空格将被替换为下划线。如果需要，这个文件可以手工编辑。

`nmcli`程序从`shell`提示符创建和编辑连接文件。

查看组网信息，可以通过`nmcli dev status`命令用来查看所有网络设备的当前状态。

```bash
┌──[root@liruilongs.github.io]-[~]
└─$nmcli dev status
DEVICE      TYPE      STATE      CONNECTION
privbr0     bridge    connected  privbr0
virbr0      bridge    connected  virbr0
eth0        ethernet  connected  eth0
vnet0       tun       connected  vnet0
vnet1       tun       connected  vnet1
lo          loopback  unmanaged  --
virbr0-nic  tun       unmanaged  --
```

当然`nmcli`操作除dev和con之外其他一些对象，暂时不清楚是干什么的，以后有机会研究下。
```bash
┌──[root@liruilongs.github.io]-[~]
└─$nmcli
agent       connection  device      general     help        monitor     networking  radio
```
`nmcli con show`命令用来显示所有连接的列表。添加`--active`选项来只列出活动的连接。
```bash
┌──[root@liruilongs.github.io]-[~]
└─$nmcli con show --active
NAME     UUID                                  TYPE      DEVICE
privbr0  ef4e7961-d070-920c-71d7-51d86377d921  bridge    privbr0
virbr0   667e5abb-da79-4c74-b4d6-dc203a14948c  bridge    virbr0
eth0     5fb06bd0-0bb0-7ffb-45f1-d6edd65f3e03  ethernet  eth0
vnet0    d801c759-2775-4a4c-8694-85adb30af9db  tun       vnet0
vnet1    a33cf39d-c485-40d3-affd-6fed087fbbe8  tun       vnet1
┌──[root@liruilongs.github.io]-[~]
└─$
```




### device
device即网卡设备。常用的命令如下
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli device
connect     disconnect  lldp        monitor     set         status
delete      help        modify      reapply     show        wifi
```
关于device，一般不怎么操作，通过show命令 `nmcli device show eth0`可以查看网卡详细信息
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli device show eth0
GENERAL.DEVICE:                         eth0
GENERAL.TYPE:                           ethernet
GENERAL.HWADDR:                         52:54:00:00:FA:0A
GENERAL.MTU:                            1500
GENERAL.STATE:                          100 (connected)
GENERAL.CONNECTION:                     Wired connection 1
GENERAL.CON-PATH:                       /org/freedesktop/NetworkManager/ActiveConnection/1
WIRED-PROPERTIES.CARRIER:               on
IP4.ADDRESS[1]:                         172.25.250.10/24
IP4.GATEWAY:                            172.25.250.254
IP4.ROUTE[1]:                           dst = 172.25.250.0/24, nh = 0.0.0.0, mt = 100
IP4.ROUTE[2]:                           dst = 0.0.0.0/0, nh = 172.25.250.254, mt = 100
IP4.DNS[1]:                             172.25.250.254
IP6.ADDRESS[1]:                         fe80::984:87d2:dba7:1007/64
IP6.GATEWAY:                            --
IP6.ROUTE[1]:                           dst = fe80::/64, nh = ::, mt = 100
IP6.ROUTE[2]:                           dst = ff00::/8, nh = ::, mt = 256, table=255
┌──[root@servera.lab.example.com]-[~]
└─$
```

### connection
connection 即为添加动态网络连接配置，常用的命令如下
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection
add      delete   edit     help     load     monitor  show
clone    down     export   import   modify   reload   up
```
#### 查看show
通过`nmcli connection show`可以查看网络配置的详细信息
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection show
NAME                UUID                                  TYPE      DEVICE
Wired connection 1  4ae4bb9e-8f2d-3774-95f8-868d74edcc3c  ethernet  eth0
Wired connection 2  c0e6d328-fcb8-3715-8d82-f8c37cb42152  ethernet  --
Wired connection 3  9b5ac87b-572c-3632-b8a2-ca242f22733d  ethernet  --
```

#### 添加add
通过 `nmcli connection add `添加一个网络连接,默认IP为动态获取
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection add type ethernet con-name dynamic-eth1 ifname eth1
Connection 'dynamic-eth1' (91575fc2-a151-4da4-a174-757b4bf9c483) successfully added.
```
这里添加的网络配置名字为`dynamic-eth1`，使用的网卡为`eth1`,查看添加的网络连接配置
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection show
NAME                UUID                                  TYPE      DEVICE
Wired connection 2  c0e6d328-fcb8-3715-8d82-f8c37cb42152  ethernet  eth1
Wired connection 3  9b5ac87b-572c-3632-b8a2-ca242f22733d  ethernet  eth2
Wired connection 1  4ae4bb9e-8f2d-3774-95f8-868d74edcc3c  ethernet  eth0
dynamic-eth1        91575fc2-a151-4da4-a174-757b4bf9c483  ethernet  --
```
这里IP地址获取方式默认为动态获取
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection show  dynamic-eth1 | grep ipv4.m
ipv4.method:                            auto
ipv4.may-fail:                          yes
```

添加一个连接，网络类型配置为因特网(ethernet)
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection add con-name static ifname eth1 type ethernet
Connection 'static' (1833628a-ebe3-4fd0-9be1-9bed877067c9) successfully added.
```
#### 修改modify
`nmcli con mod name`命令用于修改连接设置。这些更改也保存在连接的`/etc/sysconfig/network-scripts/ifcfg-name`文件中。
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection modify static  +ipv4.dns 114.114.114.114
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection show static | grep ipv4.dn
ipv4.dns:                               172.25.25.254,114.114.114.114
ipv4.dns-search:                        --
ipv4.dns-options:                       --
ipv4.dns-priority:                      0
```

修改ipv4的IP地址获取方式为手动，并指定IP地址、网关、DNS
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection modify static ipv4.method  manual ipv4.addresses  172.25.25.10/24 ipv4.gateway 172.25.25.254 ipv4.dns 172.25.25.254
```
查看系修改后的信息，切记修改完需要从新激活
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection show static | grep ipv4
ipv4.method:                            manual
ipv4.dns:                               172.25.25.254
ipv4.dns-search:                        --
ipv4.dns-options:                       --
ipv4.dns-priority:                      0
ipv4.addresses:                         172.25.25.10/24
ipv4.gateway:                           172.25.25.254
ipv4.routes:                            --
ipv4.route-metric:                      -1
ipv4.route-table:                       0 (unspec)
ipv4.routing-rules:                     --
ipv4.ignore-auto-routes:                no
ipv4.ignore-auto-dns:                   no
ipv4.dhcp-client-id:                    --
ipv4.dhcp-timeout:                      0 (default)
ipv4.dhcp-send-hostname:                yes
ipv4.dhcp-hostname:                     --
ipv4.dhcp-fqdn:                         --
ipv4.never-default:                     no
ipv4.may-fail:                          yes
ipv4.dad-timeout:                       -1 (default)
┌──[root@servera.lab.example.com]-[~]
└─$
```
#### 激活up
`nmcli con up name`命令用来`激活`绑定了名称连接的网络接口。该命令使用连接名，而不是网络接口名,下面执行可以动态获取ip
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection up static
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/288)
```
#### 断开disconnect
`nmcli dev disconnect device`命令用来`断开`设备的网络接口，使其断开。命令缩写为`nmcli dev dis device`
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli dev dis dynamic-eth1
```
这里需要说明的是，`nmcli con down name`命令通常`不是禁用网络接口的最佳方法`，因为它会导致连接中断。然而，默认情况下，大多数有线系统连接配置为启用自动连接。这将在网络接口可用时立即激活连接。因为连接的网络接口仍然是可用的，`nmcli con down name`会将该接口关闭，但随后`NetworkManager`会立即将其打开，除非该连接与该接口完全断开。


#### 删除delete
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli connection delete dynamic-eth1
Connection 'dynamic-eth1' (91575fc2-a151-4da4-a174-757b4bf9c483) successfully deleted.
```

### 配置文件
网络连接配置中，静态连接属性保存在`/etc/sysconfig/network-scripts/ifcfg-*`配置文件中。

动态的连接数据，如设置IP自动获取，即从DHCP服务器获得的，是不持久存储的。

执行`nmcli con show name`命令查询连接的当前设置。`小写设置`是可以更改的`静态属性`。`大写设置`是连接实例`临时使用`的`动态设置`。
```bash
┌──[root@liruilongs.github.io]-[~]
└─$nmcli con show vnet0
connection.id:                          vnet0
connection.uuid:                        d801c759-2775-4a4c-8694-85adb30af9db
connection.stable-id:                   --
connection.type:                        tun
connection.interface-name:              vnet0
...................
GENERAL.NAME:                           vnet0
GENERAL.UUID:                           d801c759-2775-4a4c-8694-85adb30af9db
GENERAL.DEVICES:                        vnet0
GENERAL.IP-IFACE:                       vnet0
GENERAL.STATE:                          activated
......
```
#### 配置文件信息查看
```bash
┌──[root@servera.lab.example.com]-[~]
└─$cd /etc/sysconfig/network-scripts/
┌──[root@servera.lab.example.com]-[/etc/sysconfig/network-scripts]
└─$cat ifcfg-static
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=static
UUID=1833628a-ebe3-4fd0-9be1-9bed877067c9
DEVICE=eth1
ONBOOT=yes
IPADDR=172.25.25.10
PREFIX=24
GATEWAY=172.25.25.254
DNS1=172.25.25.254
IPV6_DISABLED=yes
DNS2=114.114.114.114
┌──[root@servera.lab.example.com]-[/etc/sysconfig/network-scripts]
└─$
```
**配置文件修改后需要从新加载激活**
```bash
┌──[root@servera.lab.example.com]-[/etc/sysconfig/network-scripts]
└─$nmcli connection reload
┌──[root@servera.lab.example.com]-[/etc/sysconfig/network-scripts]
└─$nmcli connection up static
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/290)
┌──[root@servera.lab.example.com]-[/etc/sysconfig/network-scripts]
└─$
```

## 谁可以修改网络设置
修改网络配置的权限：`root`和以及`本地登录的普通用户`可以修改，通过`SSH远程的普通用户`没有权限

+ ROOT
```bash
┌──[root@liruilongs.github.io]-[~]
└─$nmcli general permissions
PERMISSION                                                        VALUE
org.freedesktop.NetworkManager.enable-disable-network             yes
org.freedesktop.NetworkManager.enable-disable-wifi                yes
org.freedesktop.NetworkManager.enable-disable-wwan                yes
org.freedesktop.NetworkManager.enable-disable-wimax               yes
org.freedesktop.NetworkManager.sleep-wake                         yes
org.freedesktop.NetworkManager.network-control                    yes
org.freedesktop.NetworkManager.wifi.share.protected               yes
org.freedesktop.NetworkManager.wifi.share.open                    yes
org.freedesktop.NetworkManager.settings.modify.system             yes
org.freedesktop.NetworkManager.settings.modify.own                yes
org.freedesktop.NetworkManager.settings.modify.hostname           yes
org.freedesktop.NetworkManager.settings.modify.global-dns         yes
org.freedesktop.NetworkManager.reload                             yes
org.freedesktop.NetworkManager.checkpoint-rollback                yes
org.freedesktop.NetworkManager.enable-disable-statistics          yes
org.freedesktop.NetworkManager.enable-disable-connectivity-check  yes
org.freedesktop.NetworkManager.wifi.scan                          unknown
```
+ 本地登录的普通用户

![在这里插入图片描述](https://img-blog.csdnimg.cn/cf9ff1f2cd3a4db68681a08316aa4949.png)

+ 远程登录的普通用户
```bash
[liruilong@liruilongs /]$nmcli general permissions
PERMISSION                                                        VALUE
org.freedesktop.NetworkManager.enable-disable-network             no
org.freedesktop.NetworkManager.enable-disable-wifi                no
org.freedesktop.NetworkManager.enable-disable-wwan                no
org.freedesktop.NetworkManager.enable-disable-wimax               no
org.freedesktop.NetworkManager.sleep-wake                         no
org.freedesktop.NetworkManager.network-control                    auth
org.freedesktop.NetworkManager.wifi.share.protected               no
org.freedesktop.NetworkManager.wifi.share.open                    no
org.freedesktop.NetworkManager.settings.modify.system             auth
org.freedesktop.NetworkManager.settings.modify.own                auth
org.freedesktop.NetworkManager.settings.modify.hostname           auth
org.freedesktop.NetworkManager.settings.modify.global-dns         auth
org.freedesktop.NetworkManager.reload                             auth
org.freedesktop.NetworkManager.checkpoint-rollback                auth
org.freedesktop.NetworkManager.enable-disable-statistics          no
org.freedesktop.NetworkManager.enable-disable-connectivity-check  no
org.freedesktop.NetworkManager.wifi.scan                          unknown
[liruilong@liruilongs /]$
```













