---
title: Ansible最佳实践之 AWX 启用facts缓存和模板问卷调查
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: Ansible
uniqueId: '2022-09-01 17:16:29/Ansible最佳实践之 AWX 构建高级作业工作流.html'
mathJax: false
date: 2022-09-08 01:16:29
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 分享一些 AWX 启用facts缓存和模板问卷调查的笔记
+ 博文内容涉及：
  + 启动facts缓存相关配置Demo
  + 启用模板调查来设置变量demo
+ 食用方式： 需要了解 Ansible
+ 理解不足小伙伴帮忙指正



**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***


## 通过事实缓存提高性能

### 事实缓存

Ansible Facts 是 Ansible 在受管主机上自动检测到的变量。包含主机的特定信息，这些信息可以像常规变量一样使用。通常，每个剧本都会在执行第一个任务之前自动运行`setup模块`，以便从剧本中主机模式匹配的`托管主机`中收集事实。

1. 这确保了该剧本具有当前事实，但收集事实会带来明显的性能后果，尤其是在托管主机数量庞大的情况下。如果您在剧中不使用任何事实，则可以在剧本中设置关闭自动事实收集，加快执行速度。如果您在剧中使用事实，则关闭自动事实收集将导致剧本无法执行。
2. 剧本还可以引用其他主机的事实(ansible使用任务委派)。例如，在托管主机`servera`上运行的任务可以通过引用变量`hostvars[serverb]`,`['ansible_facts']['default_ipv4']['address']`访问`serverb`的变量，仅当通过此剧本或同一Playbook中较早任务已从serverb收集了事实时，此方法才有效。

一个 Playbook 可以为清单中的所有主机收集事实并缓存这些事实，以便后续 Playbook 可以在不收集事实或手动运行 setup 模块的情况下使用它们。


### 在  AWX  中启用事实缓存

Red Hat  AWX  支持`Fact Caching`功能。 AWX 启动作业时，将作业中每个托管主机的所有 ansible_facts 注入到内存中。完成工作后，从内存中检索特定主机的所有记录，然后将时间更新的事实存储到数据库中。


#### 全局设置

AWX 中有一个全局设置，用于控制每个主机的事实到期时间。在下图中的作业设置中

![在这里插入图片描述](https://img-blog.csdnimg.cn/3d322a2672e34d3bbb525b3882f837b5.png)

以秒为单位，控制收集到的Ansible事实缓存效时间。存储的 Ansible 事实自上次修改后被视为有效的最长时间(以秒为单位)。只有有效且未过时的事实才会被 playbook 访问。注

意，这不会影响从数据库中删除 ansible_facts。默认值设置为0，使用 0 值表示不应用超时。如果您未定期收集事实来更新缓存，则可能会因为托管主机动态变更而导致事实不是最新值。

![在这里插入图片描述](https://img-blog.csdnimg.cn/69878fb971b045d2bfc6c320b61832c9.png)


#### 作业模板设置

AWX 作业模板启用事实缓存`Use Fact Cache`选项,作业模板中的任务才会使用事实缓存中的事实。

![在这里插入图片描述](https://img-blog.csdnimg.cn/3f6e4a15a6a24023aaa788adbbe99b14.png)

以下步骤演示如何在  AWX  中启用事实缓存：
1. 单击左侧导航栏中的 Templates。
2. 选择适当的作业模板，再单击其名称以编辑设置。
3. 在页面的 OPTIONS 部分中，选中 Use Fact Cache 旁边的复选框。
4. 单击 SAVE 以保存修改后的作业模板配置。

当启用了` Use Fact Cache `选项的模板运行新作业时，该作业都将使用事实缓存。

如果` Ansible Playbook `的` gather_facts `变量也设置为yes，当前facts没有缓存，该作业将收集事实，检索它们，并将它们存储在事实缓存中。

这里我们测试下，手工创建一个项目，只用于收集事实

在项目文件加下面新建项目文件
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl exec -it awx-demo-65d9bf775b-hc58x -c awx-demo-task  -- bash
bash-5.1$ cd /var/lib/awx/projects/
bash-5.1$ ls
_6__demo_project  _6__demo_project.lock  _9__ansible_role_keepalived  _9__ansible_role_keepalived.lock  liruilong_manual  liruilong_manual.lock
bash-5.1$ mkdir gather_gacts
bash-5.1$ cd gather_gacts/;touch task.yaml
bash-5.1$ vim task.yaml
bash: vim: command not found
bash-5.1$ vi task.yaml
bash-5.1$ ls -l
total 4
-rw-r--r-- 1 awx root 66 Oct 22 04:27 task.yaml
bash-5.1$ cat /var/lib/awx/projects/gather_gacts/task.yaml
---
- name: 事实缓存学习
  hosts: all
  gather_facts: true

bash-5.1$
bash-5.1$ exit
exit
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
在 AWX UI 面板中创建对应的项目
![在这里插入图片描述](https://img-blog.csdnimg.cn/aae8086bc98e4128840795ebd6fc325b.png)

创建项目对应的作业模板
![在这里插入图片描述](https://img-blog.csdnimg.cn/90cdec9dc4ca455b860027cee54fcce5.png)

执行测试
![在这里插入图片描述](https://img-blog.csdnimg.cn/3ff24717980843e093a6cc47e297000d.png)

然后我们可以在主机的事实中看到缓存的数据
![在这里插入图片描述](https://img-blog.csdnimg.cn/de3fd81a8b554387bb56455d511f9cd0.png)

## 使用Job Template Surveys设置变量


### 管理变量

建议 Ansible 用户编写可在不同情况下重复使用的 Playbook，或者当部署到系统时应具有略微不同的行为、配置，或在不同环境中运行。处理此问题的⼀种简单方法是使用变量。


### 定义额外变量

在  AWX  中，可以通过两种方式使用作业模板来直接设置额外的变量：

![在这里插入图片描述](https://img-blog.csdnimg.cn/6176997420994fcf9cc806692c53c89d.png)

+ 通过作业模板中的 EXTRA VARIABLES 变量 字段。
+ 如果为 EXTRA VARIABLES 变量字段选择了启动时提示 ` PROMPT ON LAUNCH`，则系统将提示  AWX  用户在使用作业模板来启动作业时以交互方式式修改所使用的额外变量的列表。

![在这里插入图片描述](https://img-blog.csdnimg.cn/b0bb9d6908e641319d34c68d177bfb3f.png)


如果生成的作业后来重新启动，则会再次使用相同的额外变量。在重新启动作业时，不能更改其额外变量。相反，应从原始作业模板启动作业，并设置不同的额外变量。

另一种即通过作业模板调查来实现

### 作业模板调查

作业模板调查允许作业模板在用于启动作业时显示简写形式，提示用户输入用于为额外变量设置值的信息。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2dd85020fee74588bfd963fd0cf00da0.png)


提示用户输入与设置额外变量的其他方法相比具有多个优势。用户无需详细了解额外变量的运行方式或使用情况。他们也不需要了解 Playbook 所使用的额外变量的名称。

由于提示可以包含任意文本，因此可以选择对用户友好的措辞，并且易于那些可能并不详细了解 Ansible 的用户理解。


+ 用户友好的问题：调查允许使用自定义的问题对用户进行提示。与 PROMPT ON LAUNCH 方法相比，这种方式能够更加友好地提示用户输入额外变量值。
+ 回答类型：除了提供用户友好的提示之外，调查还可以定义用户输入的规则，并对其执行验证。用户对调查问题的回答可以
限制为以下七种回答类型之⼀：

![在这里插入图片描述](https://img-blog.csdnimg.cn/f784d0c91ee14b12a8fe8c23e506ab8e.png)

+ 答案长度：还可以为调查问题的用户回答定义大小规则。对于以下非列表答案类型，调查可以定义用户回答所允许的最小和最大字符长度：Text、 Textarea、Password、Integer 和 Float。

+ 默认回答：可以为问题提供默认回答。问题也可以标记为 REQUIRED，这表示必须为问题提供回答。
创建作业模板调查过程略。


![在这里插入图片描述](https://img-blog.csdnimg.cn/3d6ca80d5dae4237a0b80f36e1c66144.png)

这里我们修改一下之前的剧本，测试如何使用
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl exec -it awx-demo-65d9bf775b-hc58x -c awx-demo-task  -- bash
bash-5.1$ cat /var/lib/awx/projects/liruilong_manual/test.yaml
---
- name: awx demo
  hosts: all
  tasks:
     - name: 调查问卷变量学习
       debug:
         msg: "{{ username  }}"
bash-5.1$ exit
exit
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

这里添加完 问卷调查之后需要启动
![在这里插入图片描述](https://img-blog.csdnimg.cn/19babbdcae854c4b9db40c1d3bbd34e4.png)

在启动作业的时候会提示问卷信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/af9af8a8e98d412dabe8b803cea7ad70.png)

测试输出 我们输入的数据
![在这里插入图片描述](https://img-blog.csdnimg.cn/ae2992ccb7804f85b86b96af4c7545b4.png)


## 博文参考
***
《DO447 Advanced Automation Ansible Best Practices》
