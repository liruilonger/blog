---
title: Ansible最佳实践之Playbook运行选定的任务
tags:
  - Ansible
categories:
  - Playbook
toc: true
recommend: 1
keywords: java
uniqueId: '2022-08-12 17:22:51/Ansible最佳实践之Playbook运行选定的任务.html'
mathJax: false
date: 2022-08-13 01:22:51
thumbnail:
---

**<font color="009688"> 如果我会发光，就不必害怕黑暗。——王小波**</font>

<!-- more -->
## 写在前面
***
+ 今天和小伙伴们分享一些 Ansible 中Playbook运行选定的任务
+ 不知道小伙伴们有么有遇到这样的情况
+ 一些运维场景，Github中找了很棒的剧本或者角色，但是只需要其中的一部分
+ 一般情况下我们只能重新编辑剧本处理，这里其实我们可以通过标签的方式处理
+ 仅运行标有特定标记的任务，或者从特定的任务开始执行 Playbook
+ 食用方式
  + 了解 Ansible 基础知识
  + 可以编写 Ansible 剧本、角色
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 如果我会发光，就不必害怕黑暗。——王小波**</font>
***

## 对Ansible资源打标签


在处理大型或复杂的剧本时，只希望运行部分剧本或部分任务。我们可以将标签应用于可能要跳过或运行的特定资源。

通过标签来标记资源，在资源上使用tags关键字，然后是要应用的标记列表。

标记可用于下列资源：

+ 标记每个`任务`。这是使用标签的最常见方式之一。
+ 标记整个`剧本`。在剧本级别使用标签指令。
+ 标记`include_tasks`任务。include_tasks加载的所有任务都与此标签关联。
+ 标记`角色`。角色中的所有任务都与此标签关联。
+ 标记`任务块`。块中的所有任务都与此变量关联。

关于上面的标记我们依次来看一下。在这之前，我们先准备一个角色
```bash
$ ansible-galaxy  init tag_role --init-path=roles
- tag_role was created successfully
$ ansible-galaxy  list | grep tag
- tag_role, (unknown version)
$ cat roles/tag_role/tasks/main.yml
---
# tasks file for tag_role
- name: tags roles
  shell: echo 'tasks  for tag_role'
```
剧本的不同执行块的标签
```yaml
---
- name: tags Demo 1
  hosts: servera
  # 标记整个剧本
  tags:
    - play-tag-1

  roles:
    - role: tag_role
      # 标记角色
      tags:
        - role-tags
  tasks:
    - name: task 1 tag
      shell: echo 'tags to task 1'
      # 标记每个任务
      tags:
        - task-tags-1

    - name: include or import a  tasks file
      include_tasks:
        file: tasks_file
      # 标记include_tasks任务  
      tags:
        - include-import

    - block:
      - name: task 1 in block
        shell: echo 'task 1 in block'
      - name: task 2 in block
        shell: echo 'task 2 in block'
      #  标记任务块  
      tags:
        - block-tags

- name: tags Demo 2
  hosts: servera
  # 标记整个剧本
  tags:
    - play-tag-2

  tasks:
    - name: task 2 tag
      shell: echo 'tags to task 2'
      tags:
        - task-tag-2

```

执行剧本，默认情况下打了标签依旧正常执行
```bash
$ ansible-playbook tags.yaml
PLAY [tags Demo 1] ************************************************************************************
TASK [Gathering Facts] ********************************************************************************
ok: [servera]
TASK [tag_role : tags roles] **************************************************************************
changed: [servera]
TASK [task 1 tag] *************************************************************************************
changed: [servera]
TASK [include or import a  tasks file] ****************************************************************
included: /home/student/DO447/labs/task-execution/tasks_file for servera
TASK [task 1] *****************************************************************************************
changed: [servera]
TASK [task 2] *****************************************************************************************
changed: [servera]
TASK [task 1 in block] ********************************************************************************
changed: [servera]
TASK [task 2 in block] ********************************************************************************
changed: [servera]
PLAY [tags Demo 2] ************************************************************************************
TASK [Gathering Facts] ********************************************************************************
ok: [servera]
TASK [task 2 tag] *************************************************************************************
changed: [servera]
PLAY RECAP ********************************************************************************************
servera                    : ok=10   changed=7    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
$
```
下面我们来看下如何管理标记资源，选择执行
## 管理标记的资源


要列出 Playbook 中的所有标记，使用` --list-tags `选项
```bash
$ ansible-playbook tags.yaml  --list-tags

playbook: tags.yaml

  play #1 (servera): tags Demo 1        TAGS: [play-tag-1]
      TASK TAGS: [block-tags, include-import, play-tag-1, role-tags, task-tags-1]

  play #2 (servera): tags Demo 2        TAGS: [play-tag-2]
      TASK TAGS: [play-tag-2, task-tag-2]
$
```
使用 ansible-playbook 运行 playbook 时，可使用` --tags `选项来筛选playbook 并且仅运行带有特定标记的play 或任务。
```bash
$ ansible-playbook tags.yaml  --tags=play-tag-2
PLAY [tags Demo 1] *************************************************************************************************
PLAY [tags Demo 2] *************************************************************************************************
TASK [Gathering Facts] *********************************************************************************************
ok: [servera]
TASK [task 2 tag] **************************************************************************************************
changed: [servera]
PLAY RECAP *********************************************************************************************************
servera                    : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0  
```
多个标签之前逗号隔开
```bash
$ ansible-playbook tags.yaml  --tags=block-tags,role-tags
PLAY [tags Demo 1] *************************************************************************************************
TASK [tag_role : tags roles] ***************************************************************************************
changed: [servera]
TASK [task 1 in block] *********************************************************************************************
changed: [servera]
TASK [task 2 in block] *********************************************************************************************
changed: [servera]
PLAY [tags Demo 2] *************************************************************************************************
PLAY RECAP *********************************************************************************************************
servera                    : ok=3    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
$
```
运行 ansible-playbook 命令，使用` --skip-tags `选项跳过带有特定标记的任务。
```bash
$ ansible-playbook tags.yaml  --list-tags

playbook: tags.yaml

  play #1 (servera): tags Demo 1        TAGS: [play-tag-1]
      TASK TAGS: [block-tags, include-import, play-tag-1, role-tags, task-tags-1]

  play #2 (servera): tags Demo 2        TAGS: [play-tag-2]
      TASK TAGS: [play-tag-2, task-tag-2]      
$ ansible-playbook tags.yaml  --skip-tags play-tag-1
PLAY [tags Demo 1] *************************************************************************************************
PLAY [tags Demo 2] *************************************************************************************************
TASK [Gathering Facts] *********************************************************************************************
ok: [servera]
TASK [task 2 tag] **************************************************************************************************
changed: [servera]
PLAY RECAP *********************************************************************************************************
servera                    : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0  
$
```

## 特殊标记

Ansible 还具有可在 Playbook 中分配的特殊标记：

+ `always`：带有 always 标记的资源始终都会运行，除非明确指定` --skip-tags always `选项。
+ `never`：带有 never 特殊标记的资源不会运行，除非明确指定` --tags never `选项。

看一个Demo
```yaml
$ cat tags-all.yaml
---
- name: tags Demo 1
  hosts: servera

  tags:
    - play-tag-1
    - never
  roles:
    - role: tag_role
      tags:
        - role-tags
  tasks:
    - name: task 1 tag
      shell: echo 'tags to task 1'
      tags:
        - task-tags-1

    - name: include or import a  tasks file
      include_tasks:
        file: tasks_file
      tags:
        - include-import

    - block:
      - name: task 1 in block
        shell: echo 'task 1 in block'
      - name: task 2 in block
        shell: echo 'task 2 in block'
      tags:
        - block-tags

- name: tags Demo 2
  hosts: servera
  tags:
    - always
  tasks:
    - name: task 2 tag
      shell: echo 'tags to task 2'
```
可以看到剧本1设置`never`标签，所以不会执行，剧本2设置`always`，所以默认总会执行
```bash
$ ansible-playbook  tags-all.yaml
PLAY [tags Demo 1] *************************************************************************************************
PLAY [tags Demo 2] *************************************************************************************************
TASK [Gathering Facts] *********************************************************************************************
ok: [servera]
TASK [task 2 tag] **************************************************************************************************
changed: [servera]
PLAY RECAP *********************************************************************************************************
servera                    : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
$
```

命令行指定标签时：
+ tagged 标记将运行任何带有显式标记的资源
+ untagged 标记将运行不带有显式标记的资源
+ all 标记将包括 Play 中的所有任务，无论是否带有标记，这是默认行为。


```yaml
$ cat  tags-all.yaml
---
- name: tags Demo 1
  hosts: servera

  tags:
    - play-tag-1

  roles:
    - role: tag_role
      tags:
        - role-tags
  tasks:
    - name: task 1 tag
      shell: echo 'tags to task 1'
      tags:
        - task-tags-1

    - name: include or import a  tasks file
      include_tasks:
        file: tasks_file
      tags:
        - include-import

    - block:
      - name: task 1 in block
        shell: echo 'task 1 in block'
      - name: task 2 in block
        shell: echo 'task 2 in block'
      tags:
        - block-tags

- name: tags Demo 2
  hosts: servera

  tasks:
    - name: task 2 tag
      shell: echo 'tags to task 2'

$
```
`tagged 标记`将运行任何带有显式标记的资源
```bash
$ ansible-playbook  tags-all.yaml  --tags=tagged
PLAY [tags Demo 1] *************************************************************************************************
TASK [Gathering Facts] *********************************************************************************************
ok: [servera]
TASK [tag_role : tags roles] ***************************************************************************************
changed: [servera]
TASK [task 1 tag] **************************************************************************************************
changed: [servera]
TASK [include or import a  tasks file] *****************************************************************************
included: /home/student/DO447/labs/task-execution/tasks_file for servera
TASK [task 1] ******************************************************************************************************
changed: [servera]
TASK [task 2] ******************************************************************************************************
changed: [servera]
TASK [task 1 in block] *********************************************************************************************
changed: [servera]
TASK [task 2 in block] *********************************************************************************************
changed: [servera]
PLAY [tags Demo 2] *************************************************************************************************
TASK [Gathering Facts] *********************************************************************************************
ok: [servera]
PLAY RECAP *********************************************************************************************************
servera                    : ok=9    changed=6    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
`untagged 标记`将运行不带有显式标记的资源
```bash
$ ansible-playbook  tags-all.yaml  --tags=untagged
PLAY [tags Demo 1] *************************************************************************************************
PLAY [tags Demo 2] *************************************************************************************************
TASK [Gathering Facts] *********************************************************************************************
ok: [servera]
TASK [task 2 tag] **************************************************************************************************
changed: [servera]
PLAY RECAP *********************************************************************************************************
servera                    : ok=2    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
$
```


## 博文参考 
***
`《Red Hat Ansible Engine 2.8 DO447》`