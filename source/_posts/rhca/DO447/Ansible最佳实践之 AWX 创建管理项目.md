---
title: Ansible最佳实践之 AWX 创建管理项目的一些笔记
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: Ansible
uniqueId: '2022-09-01 17:16:03/Ansible最佳实践之 AWX 创建管理项目.html'
mathJax: false
date: 2022-09-13 01:16:03
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 分享一些 AWX 创建管理项目的笔记
+ 博文内容涉及：
  + 容器化 AWX 手工创建项目Demo
  + 通过 SCM 创建项目 Demo
  + 项目角色，更新策略介绍，SCM 凭据的创建 
+ 食用方式： 需要了解 Ansible
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***


## 为 Ansible Playbook 创建项目

### 项目

在  AWX  Web 界面中，每个 Ansible 项目都由⼀个项目资源来表示。


AWX 中 项目 代表多个剧本及其相关的资源集合，例如，剧本和角色。在AWX Web界面中，每个Ansible项目都由一个对象 资源表示。

常见的项目类型有两种：

+ `Manual`，从AWX服务器项目基础路径获取项目资料。该目录由`/etc/tower/settings.py`配置，默认情况下位于`/var/lib/awx/projects`。这`不是推荐的做法`。更新此类项目需要在 AWX 界面之外进行手动干预，而且项目管理员具有直接访问权，以在AWX上更改操作系统环境。降低了AWX服务器的安全性。如果是容器环境，比如 `K8s` 中，不是合适的创建方式，如果`pod`调度，那么是不可预测的。需要做成有状态应用。

+ `SCM(source code management)`，该项目配置为从版本控制系统(也被  AWX  称为源代码控制管理或 SCM 系统)检索这些资料。 AWX  支持使用 Git、Subversion 或 Mercurial 从 SCM 下载和自动获取项目资料更新的功能。

当前的 AWX 为 k8s 集群中部署，我们来看下对应的处理方式。

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl get pods awx-demo-65d9bf775b-hc58x -o yaml | grep -A 4 '/etc/tower/settings.py'
    - mountPath: /etc/tower/settings.py
      name: awx-demo-settings
      readOnly: true
      subPath: settings.py
    - mountPath: /etc/nginx/nginx.conf
--
    - mountPath: /etc/tower/settings.py
      name: awx-demo-settings
      readOnly: true
      subPath: settings.py
    - mountPath: /var/run/redis
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
这里可以看到，对应配置文件做了 `CM` 卷挂载上去的。我们找一些具体 的 `CM`
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl get pods awx-demo-65d9bf775b-hc58x -o yaml |grep -A 10 awx-demo-settings | grep -A 5  configMap  | grep name
      name: awx-demo-awx-configmap
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
查看配置文件的详细信息，确认项目目录
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl get cm awx-demo-awx-configmap -o jsonpath='{.data.settings}'
import os
import socket
# Import all so that extra_settings works properly
from django_auth_ldap.config import *

def get_secret():
    if os.path.exists("/etc/tower/SECRET_KEY"):
        return open('/etc/tower/SECRET_KEY', 'rb').read().strip()

ADMINS = ()
STATIC_ROOT = '/var/lib/awx/public/static'
STATIC_URL = '/static/'
PROJECTS_ROOT = '/var/lib/awx/projects'
JOBOUTPUT_ROOT = '/var/lib/awx/job_status'
.................
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

### 创建项目

#### 手动创建 

![在这里插入图片描述](https://img-blog.csdnimg.cn/3bea21af93ff41299a35e2d6946f4fa4.png)

手动的方式，所以我们需要这 pod 容器内部创建，这里我们这了一个创建好的项目上穿上去。

查看当前项目目录
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl exec -it  awx-demo-65d9bf775b-hc58x -c awx-demo-task  -- bash
bash-5.1$ cd  /var/lib/awx/projects
bash-5.1$ ls
bash-5.1$ exit
exit
```
生成一个角色作为项目，然后 cp 到容器内部目录下
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible-galaxy init liruilong_manual
- Role liruilong_manual was created successfully
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl cp liruilong_manual awx-demo-65d9bf775b-hc58x:/var/lib/awx/projects/ -c awx-demo-task
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl exec -it awx-demo-65d9bf775b-hc58x -c awx-demo-task  -- bash
bash-5.1$ cd /var/lib/awx/projects/
bash-5.1$ ls
liruilong_manual
bash-5.1$
```
刷新 AWX ，会看到刚才创建的本地项目

![在这里插入图片描述](https://img-blog.csdnimg.cn/503ea88bff8146f1ba791059a1b121e5.png)

选择创建
![在这里插入图片描述](https://img-blog.csdnimg.cn/38ef6a2dc2db4ee3b3c1c679ee91caee.png)

创建成功
![在这里插入图片描述](https://img-blog.csdnimg.cn/f69f8066a66245fcbb9ea22c1497009c.png)

#### SCM 的方式创建

SCM的方式，这里我们选择通过 github 获取一个之前写的角色  [https://github.com/LIRUILONGS/ansible_role_keepalived](https://github.com/LIRUILONGS/ansible_role_keepalived)



![在这里插入图片描述](https://img-blog.csdnimg.cn/e797138d63d84e0cab66de98e9105257.png)

在源控制中输入项目路径
![在这里插入图片描述](https://img-blog.csdnimg.cn/dfb1bc09fb82425c898264e8db3d8689.png)

创建后查看状态 
![在这里插入图片描述](https://img-blog.csdnimg.cn/f14bbfb7ecb546d0b7717d41c88e4110.png)

这里同步失败 ，因为还没有配置 github 的凭据，需要SCM 凭据，下面会介绍
![在这里插入图片描述](https://img-blog.csdnimg.cn/c8830057db8e4311a0e53394cc426cc5.png)

在容器项目路径下，可以看到我们同步的项目
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl exec -it awx-demo-65d9bf775b-hc58x -c awx-demo-task  -- bash
bash-5.1$ cd /var/lib/awx/projects/
bash-5.1$ ls -l
total 0
drwxr-xr-x  2 awx root   6 Oct 21 19:20 _6__demo_project
-rwxr-xr-x  1 awx root   0 Oct 21 19:20 _6__demo_project.lock
drwxr-xr-x  4 awx root  86 Oct 21 18:25 _9__ansible_role_keepalived
-rwxr-xr-x  1 awx root   0 Oct 21 18:25 _9__ansible_role_keepalived.lock
drwxr-xr-x 10 awx root 154 Oct 21 13:45 liruilong_manual
-rwxr-xr-x  1 awx root   0 Oct 21 19:35 liruilong_manual.lock
bash-5.1$
```

### 项目角色

创建好项目之后需要分配角色

![在这里插入图片描述](https://img-blog.csdnimg.cn/fb8bf05c41114b419c64f4a66f76c148.png)

项目可用的角色列表：
+ Admin：授予用户对项目完全的访问权限。
+ Use：授予用户在模板资源中使用项目的权限。
+ Update：授予用户从其 SCM 来源手动更新或计划更新项目资料更新的权限。
+ Read：授予用户查看与项目关联的详细信息。

### 管理项目访问权限

首次创建项目时，用户必须拥有该项目的组织的` Admin 或 Auitor `角色才能访问它。用户的其它访问权限必须经过特别配置。创建项目时无法分配角色，必须通过编辑项目进行添加。

![在这里插入图片描述](https://img-blog.csdnimg.cn/23079bc3ec4a48eba785ec62327cbea0.png)


### 创建 SCM 凭据

源代码控制凭据存储身份验证信息，供  AWX  用来访问存储在像 Git 这样的版本控制系统中的项目中的资料。SCM 凭据存储对源代码控制存储库访问权限进行身份验证所需的用户名和密码或私钥。

创建 SCM 凭据

![在这里插入图片描述](https://img-blog.csdnimg.cn/dea1fc46b6704d1d8663348e65dc6c3f.png)

添加凭据，这里我们使用帐密凭据

![在这里插入图片描述](https://img-blog.csdnimg.cn/24f761f2ae174826858a530a5e6fa407.png)

同步项目

![在这里插入图片描述](https://img-blog.csdnimg.cn/5e51de52374244d3a07e4ae564364e43.png)

### SCM 凭据角色

与计算机凭据一样，专用 SCM 凭据仅供其创建者以及 System Administrator 和 System Auditor 用户使用。分
配给某一组织的 SCM 凭据可以与其它用户共享，方法是为用户或团队分配该凭据的角色。


![在这里插入图片描述](https://img-blog.csdnimg.cn/ad3532ef0c9d48cd8ec9624f3dc0eb27.png)

可用的角色列表：
+ Admin：授予用户对 SCM 凭据完全的访问权限。
+ Use：授予用户将 SCM 凭据与项目资源关联的权限。
+ Read：授予用户查看 SCM 凭据详细信息的权限。




### 管理 SCM 凭据访问权限

SCM 凭据 添加角色

![在这里插入图片描述](https://img-blog.csdnimg.cn/6b922a9fc2534ba28d5101849ebfee9d.png)

首次创建组织凭据时，只能由特定用户进行访问，其它用户的其它权限必须经过特别配置。


### 更新项目策略

![在这里插入图片描述](https://img-blog.csdnimg.cn/ef37367bc7144f79a4e2d5845ed39280.png)

可以通过以下方式在  AWX  中更新 SCM 项目资源的策略：

+ 在进行更新前删除任何本地修改。

+ 在进行更新前删除整个本地存储库。根据存储库的大小，这可能会显著增加完成更新所需的时间

+ 子模块将跟踪其 master 分支(或在 .gitmodules 中指定的其他分支)的最新提交。如果没有，子模块将会保留在主项目指定的修订版本中。这等同于在 git submodule update 命令中指定 --remote 标志。

+ 每次使用此项目运行作业时，请在启动该作业前更新项目的修订。

+ 允许在使用此项目的作业模板中更改 Source Control 分支或修订版本。


也可以手动更新到最新版本：

### 对 Ansible 角色的支持

项目可以指定外部 Ansible 角色，它们作为依赖项存储在 Ansible Galaxy 或其他源代码管理存储库中。在项目更新结束时，如果项⽬的存储库包括⼀个包含有效 requirements.yml 文件的 roles目录，则红帽  AWX 将自动运行 ansible-galaxy 以安装角色，这个用到了在研究


## 博文参考
***
《DO447 Advanced Automation Ansible Best Practices》