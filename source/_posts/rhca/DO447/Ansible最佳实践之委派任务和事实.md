---
title: Ansible最佳实践之委派任务和事实
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: ansible
uniqueId: '2022-08-22 16:37:34/Ansible最佳实践之委派任务和事实.html'
mathJax: false
date: 2022-08-23 00:37:34
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 分享一些 Ansible 委派任务和事实委派 的笔记
+ 博文内容涉及：
  + Ploybook 任务委派 Demo
  + Ploybook 事实委派 Demo
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***


## 委派任务

Ansible 委派任务，用通俗的话讲，就是`在当前的剧本中，执行一些与当前hosts主机不想关的任务`，可以保持一定的`调度性`，Ansible默认只会在定义好的一组服务器上执行相同的操作，但如果在这过程中需要同时对另外1台机器执行操作时，就需要用到`Ansible的任务委派功能(delegate_to)`。

有的时候，可能在 Ansible 运行 Play 处理了某些任务后，可能需要`代表受管主机`在不同系统上执行一个或多个任务。这个时候我们会用到任务委派。

工作中往往有这样的运维场景：在对一组服务器server_group 执行操作过程中，同时需要在另外一台机器A上执行一些操作，比如在A服务器上添加一条hosts记录，这些操作必须要在一个playbook`内联动完成`。也就是说A服务器这个操作与server_group1组上的服务器有`依赖关系`。


比如一个`CICD`的例子：假设我们没有使用`jenkins`,`Git Web钩子`等工具时，希望在提交代码之后。根据代码里的`dockerFile`构建镜像，然后把镜像传到镜像私仓里，上传成功后在通过`kubectl`命令更新新版本的无状态应用的副本，之后修改负载均衡的选择器。选择新版本。

这里我们的docker环境，远程Git仓库，kubectl都在不同的机器上。那么我们可以编写剧本，本地提交代码，委托docker环境根据构建文件打镜像，打完之后push到镜像库，在委托k8s客户端所在机器pull镜像更新新版本deploy的副本。之后可以选择灰度或者蓝绿发布，进行流量负载调整。修改LB选择器进行升级。


在 Play 中，可以委派任务以在不同的主机上运行，而不是当前受管主机上运行。任务使用` delegate_to `指令将操作委派给主机。该指令将` Ansible `指向将执行任务的主机，以替代对应的目标。

看一个Demo

```yaml
$ cat delegate.yaml
---
- name: conn
  hosts: servera
  gather_facts: no
  tasks:
    - name: hell world
      shell: echo hell world
    - name: conn suc
      shell: echo "{{ inventory_hostname }}  --> $(hostname) "
      delegate_to: localhost
$
```
在 `hosts：servera` 的剧本中，委托任务给`当前主机`，`delegate_to: localhost`,打印主机名
```bash
$ ansible-playbook  delegate.yaml  -v
............
TASK [conn suc] ********************************************************************************************************
changed: [servera] => {"changed": true, "cmd": "echo \"servera  --> $(hostname) \"", "delta": "0:00:00.004740", "end": "2022-08-23 01:04:53.490178", "rc": 0, "start": "2022-08-23 01:04:53.485438", "stderr": "", "stderr_lines": [], "stdout": "servera  --> workstation.lab.example.com ", "stdout_lines": ["servera  --> workstation.lab.example.com "]}

PLAY RECAP *************************************************************************************************************
servera                    : ok=2    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```

这里的使用 `inventory_hostname` 这个魔法变量来展示，当前剧本实际执行的机器，而 `$(hostname)`为执行剧本委托任务的机器


在 `hosts：servera `的剧本中给 serverb 委托命令

```yaml
$ cat deleg_demo.yaml
---
- name: 任务委派学习
  hosts: servera

  tasks:
    - name: get info servera
      shell: uname -a
      register: out
    - debug: msg={{out.stdout}}

    - name: get info serverb
      shell: uname -a
      delegate_to: serverb
      register: out
    - debug: msg={{out.stdout}}
$
```

```bash
$ ansible-playbook  deleg_demo.yaml

PLAY [任务委派学习] 
TASK [Gathering Facts] 
ok: [servera]
TASK [get info servera] 
changed: [servera]
TASK [debug] ****************************************************************************************************************************************************************************************************
ok: [servera] => {
    "msg": "Linux servera.lab.example.com 4.18.0-80.el8.x86_64 #1 SMP Wed Mar 13 12:02:46 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux"
}

TASK [get info serverb] 
changed: [servera]

TASK [debug] ****************************************************************************************************************************************************************************************************
ok: [servera] => {
    "msg": "Linux serverb.lab.example.com 4.18.0-80.el8.x86_64 #1 SMP Wed Mar 13 12:02:46 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux"
}

PLAY RECAP ******************************************************************************************************************************************************************************************************
servera                    : ok=5    changed=2    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

这里需要注意的是，`连接委托机器`的方式不一定是直接在`delegate_to`上指定委托机器，`控制节点连接被委派主机`的方式也可以通过`add_host`模块更改委托主机连接方式。看一个Demo。这里通过别名的方式实现。


```yaml
$ cat delegate.yaml
---
- name: test play
  hosts: 192.168.26.82
  tasks:
    - name: add delegation host
      add_host:
        name: sb
        ansible_host: 192.168.26.156
        ansible_user: root
    - name: echo Hello
      debug:
        msg: "Hello from  {{ ansible_host }}"
      delegate_to: sb
      register: output

```
编写到的剧本我们可以看到，通过add_host 模块的方式，我们可以在定义委托机器的同时，指定相关的连接变量，并且可以使用别名
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible-playbook delegate.yaml

PLAY [test play] *******************************************************************************************************
TASK [Gathering Facts] *************************************************************************************************
ok: [192.168.26.82]
TASK [add delegation host] *********************************************************************************************
changed: [192.168.26.82]
TASK [echo Hello] ******************************************************************************************************
ok: [192.168.26.82 -> sb] => {
    "msg": "Hello from  192.168.26.156"
}
PLAY RECAP *************************************************************************************************************
192.168.26.82              : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

## 委派事实

前面我们把部分的命令通过委托的方式交给不在当前hosts里面的任意主机执行，这里有小伙伴会讲，如果我希望得到委派机器的事实收集(系统变量)，我应该如何处理。

如果我们只是想要委托的事实收集，那个可以通 `setup`模块来实现，默认的事实收集就是通过setup模块来实现，但是这里有一个问题，那就是通过setup模块收集后之后，他会`覆盖掉剧本默认hosts对应主机的事实`


对此 ansible 提供了委派事实的方式，如果希望将委派任务所收集的事实分配到该任务所委派到主机上，可以将 `delegate_facts` 指令设置为 `true `,delegate_facts 指令可将事实收集到` hostvars['delegate_to_host'] `命名空间中

```yaml
$ cat delegteto_to.yaml
---
- name: 委派实时学习
  hosts: servera
  gather_facts: yes
  tasks:
    - name: Set a fact in delegated task on servera
      set_fact:
          myfact: sy
    - name: Set a fact in delegated task on servera
      set_fact:
          myfact: liruilong
      delegate_to: serverb
      delegate_facts: True
    - debug:
        msg: "{{ hostvars['serverb']['myfact'] }}"
    - debug:
        msg: "{{ hostvars['servera']['myfact'] }}"
$
```



```bash
[student@workstation create_users]$ ansible-playbook  delegteto_fact.yaml

PLAY [委派实时学习] **********************************************************************************************************

TASK [Gathering Facts] *************************************************************************************************
ok: [servera]

TASK [Set a fact in delegated task on servera] *************************************************************************
ok: [servera]

TASK [Set a fact in delegated task on servera] *************************************************************************
ok: [servera]

TASK [debug] ***********************************************************************************************************
ok: [servera] => {
    "msg": "liruilong"
}

TASK [debug] ***********************************************************************************************************
ok: [servera] => {
    "msg": "sy"
}

PLAY RECAP *************************************************************************************************************
servera                    : ok=5    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

```

`delegate_facts` 指令可将事实收集到 hostvars['serverb'] 命名空间中。


## 博文参考 
***
`《Red Hat Ansible Engine 2.8 DO447》`

