---
title: Ansible之Ansible Tower使用User和Team管理访问权限的笔记
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: Ansible
uniqueId: '2022-09-01 17:15:34/Ansible最佳实践之Ansible Tower使用User和Team管理访问权限.html'
mathJax: false
date: 2022-09-22 01:15:34
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 这部分内容没有太大的差别，所以用旧版本的 `Ansible Tower`
+ 博文内容涉及：
  + 创建 Tower 用户即角色添加
  + 创建 Tower 团队 即角色添加
+ 食用方式： 需要了解 Ansible  
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

## 创建和管理 Ansible Tower 用户

### 基于角色的访问控制(RBAC)

使用 Ansible Tower 创建的不同人员需要具有不同的访问级别。RBAC对于开发的小伙伴应该很熟悉了，一般的系统都这涉及。

Ansible Tower 界面内置有管理用户 admin，它拥有对整个 Ansible Tower 配置的超级用户访问权限。

为每个人设置用户帐户后，可以更加轻松地管理对清单、凭据、项目和作业模板的单独访问权限。向用户分配角色，授予权限，以定义谁可以查看、更改或删除 Ansible Tower 中的对象。


可以通过将`角色赋予 Team `来集中管理`角色`， Team 是用户的集合。 `Team 中的所有用户都继承 Team 的角色`。角色决定了用户和 Team 是否可以查看、使用、更改或删除清单和项目等对象。

这里和传统的RBAC略有一点区别，传统的RBAC，角色控制资源，用户添加角色，从而实现对资源的控制，Ansible Tower中添加了组织这一层，通过角色控制资源，组织添加角色的方式。而且对于资源和组织的界定不是那么清晰，组织作为资源的一部分，同时可以作为权限控制。



### Ansible Tower 组织

Ansible Tower 组织表示` Team 、项目和清单的逻辑`集合。所有`用户都必须属于某一组织`。作为 Ansible Tower 安装的一部分，创建了⼀个 `default 的组织`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/9201e40ff72c45438d4b9ac56d1910cf.png)

以下是使用 Ansible Tower web 界面创建其它组织的步骤。

1. 以 admin 用户身份登录 Ansible Tower Web 界面。
2. 单击位于左侧导航栏中的` Organizations `链接。

![在这里插入图片描述](https://img-blog.csdnimg.cn/0bba51a5da6949e8b30a225aea4163b7.png)

3. 单击` + `按钮以创建新组织。
4. 在提供的字段中，输⼊新组织的名称，并在需要时输⼊可选的描述。

![在这里插入图片描述](https://img-blog.csdnimg.cn/d313767f23d546e69dc24517329b1ec9.png)

5. 单击` SAVE `以完成新组织的创建。

![在这里插入图片描述](https://img-blog.csdnimg.cn/3b88b8103f244f84b2302bed1d185ae5.png)

### 用户类型
Ansible Tower 中的三种用户类型为:

+ `系统管理员：System Administrator`：也称为超级用户，提供不受限制的访问权限，以在整个Ansible Tower 内执行任何操作。
+ `系统审计：System Auditor`：该角色对整个 Ansible Tower 具有只读和访问权限。
+ `标准用户:Normal User`:没有分配特殊角色，并以最少的访问权限开始。

### 创建用户
以 `Ansible Tower admin` 身份登录的用户可以通过执行以下步骤来创建新用户：

![在这里插入图片描述](https://img-blog.csdnimg.cn/3483d3d6a022406bba2a415fb57461bb.png)

1. 单击位于左侧导航栏中的 Users 链接。
2. 单击 + 按钮以创建新用户。
3. 将新用户的名字、姓氏和电子邮件地址分别输入到 FIRST NAME、LAST NAME 和 EMAIL 字段中。
4. 在 USERNAME 字段中，为新用户指定唯一的用户名。
5. `单击 ORGANIZATION 字段旁边的放⼤镜图标，以显⽰ Ansible Tower 内的组织列表`。从列表中选择⼀个组织，再单击 SAVE。
6. 将新用户的所需密码输入到 PASSWORD 和 CONFIRM PASSWORD 字段。
7. 选择用户类型。
8. 单击 SAVE 完成操作。



#### 编辑用户
使用 Edit User 界面，执行以下步骤来编辑新创建的用户属性：

![在这里插入图片描述](https://img-blog.csdnimg.cn/8871f6756eb64d20bd525af3d2e961ae.png)

+ 单击左侧导航栏中的 Users 链接。
+ 单击要编辑的用户的链接。
+ 对所需字段进行更改。
+ 单击 SAVE 完成操作。

### 组织角色

新创建的用户根据用户的角色类型从其组织继承特定的角色。也可以在用户创建后，为其分配角色。可以为用户分配组织的四种角色：

`Admin`：用户可以管理该组织的所有方面。存在多个相关的管理角色，但比 Admin 权限更低：

+ Project Admin
+ Inventory Admin
+ Credential Admin
+ Notification Admin
+ Workflow Admin
+ Job Template Admin

`Auditor`：用户获得该组织的`只读访问权限`。

`Member`：用户获得该组织的`只读访问权限`。与组织 Admin 和 Auditor 角色不同，Member 角色不为用户提供组织所包含的任何资源的权限，如 Team 、凭据、项目、清单、作业模板、工作模板和通知。

`Read`：只能让用户查看属于`组织成员的用户列表`，以及所分配的`组织角色`。

`Execute`：用户获得在组织中`执行作业模板和工作流作业模板的权限`。


重要：
+ 拥有 `System Administrator` 角色的用户将继承 Ansible Tower 内的每个组织的 Admin 角色。
+ 拥有 `System Auditor` 角色的用户将继承 Ansible Tower 内的每个组织的Auditor 角色。


### 管理用户组织角色

在组织中对用户角色的完全管理需要以下步骤：
以 admin 身份或具有被修改组织的 Admin 角色的任何用户身份登录 Ansible Tower Web 界面。

![在这里插入图片描述](https://img-blog.csdnimg.cn/c44e12b0276d4252936aec1967401264.png)

1. 单击左侧导航栏中的 Organizations 链接。
2. 单击受管理的组织下的` Permissions `链接。
3. 在 `ADD USERS/TEAMS` 屏幕中的 USERS 下，选中所需用户旁边的框。
4. 单击` SELECT ROLES `下拉列表，再选择用户所需的组织角色。此步骤可以重复多次，为⼀个用户添加多个角色。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2db41ff0a97c47079d822f0d89215576.png)

5. 单击 SAVE ，将角色分配给该组织的用户。
6. 单击角色前面的 X ，以从用户中删除现有的角色。


## 使用 Teams 高效管理用户

### Team

`team `是组。借助` Team 管理 Ansible Tower `对象(如清单、项目和作业模板)的角色，要比分别为每个用户管理它们更高效。

属于 Team 成员的用户将继承分配给该 Team 的角色。管理员可以将角色分配给代表一组用户的 Team 。

### 创建 Team


1. 以 admin 用户身份，或者对于打算在其中创建新 Team 的组织，作为被分配了其 admin 角色用户，登录Ansible Tower Web 界面。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2da48197cdfe4162ad01f81f07888dfb.png)

2. 单击左侧导航栏中的 Teams 链接。单击 + 按钮。
3. 在 New Team 屏幕上的 NAME 字段中输入新 Team 的名称,如果需要，在 DESCRIPTION 字段中输入描述。

![在这里插入图片描述](https://img-blog.csdnimg.cn/289111ce74b344cea0c477be0f482ed6.png)

4. 对于所需的 ORGANIZATION 字段，单击放⼤镜图标以获取 Ansible Tower 内的组织列表。在 SELECT ORGANIZATIO 屏幕中，选择要在其中创建 Team 的组织，然后单击 SAVE。
7. 单击 SAVE 以完成新 Team 的创建。

### Team 角色

可以为用户分配以下一个或多个 `Team 角色`：

+ `Member`：授予用户查看团队用户和相关团队角色的能力。
+ `Admin`：授予用户对团队的完全控制权
+ `Read`：授予用户查看团队用户和相关团队角色的能力。但是不会继承 Ansible Tower 资源授予给该团队的角色。

实际上，大多数组织只使用member团队角色，使用其他角色，将导致Ansible Tower角色管理更加复杂，而且团队成员还有可能通过外部身份验证源进行管理。

建议：使用组织管理员和系统管理员管理团队成员。

### 向团队添加用户

将拥有 Member 角色的用户添加到组织内的团队中，执行以下步骤：

![在这里插入图片描述](https://img-blog.csdnimg.cn/71e94d8383fd47708a1ed772c7463b12.png)

1. 以 admin 用户身份，或者作为被分配了团队所属组织的 admin 角色的用户，登录 Ansible Tower Web 界面。
2. 单击左侧导航栏中的 Organizations 链接。
3. 单击该团队所属组织下的 TEAMS 链接。
4. 在 Teams 屏幕上，单击要将用户添加到的团队的名称。
5. 在团队详细信息屏幕上，单击 USERS 按钮。
6. 单击 + 按钮。
7. 在 ADD USERS 屏幕中，选择要添加到团队的⼀个或多个用户。然后，单击 SAVE 以保存编辑，并将该用户添加到团队中。


### 设置团队角色
自红帽` Ansible Tower 3.4 `起，向组织中的团队添加具有 `admin 或 read `角色的用户需要` towercli `工具。
+ 示例，授予 Admin 角色：`tower-cli role grant --user 'joe' --target-team 'Operators' --type 'admin' `
+ 示例，授予 read 角色：`tower-cli role grant --user 'joe' --target-team 'Operators' --type 'read' `

注意：
+ 若要使用 tower-cli，需要运行 tower-cli config 命令来指定红帽 AnsibleTower 的主机，以及用于访问它的用户名和密码。
+ 可以通过 tower-cli config verify_ssl false 命令允许未验证的 SSL 连接。


## 博文参考

***
《DO447 Advanced Automation Ansible Best Practices》