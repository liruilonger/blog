---
title: Ansible最佳实践之AWK ( Anssible Tower  )界面介绍
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: Ansible
uniqueId: '2022-10-29 23:19:53/Ansible最佳实践之 AWX 界面介绍.html'
mathJax: false
date: 2022-09-01 16:19:53
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 参加考试，这里整理这部分笔记
+ 博文内容为` AWX `和 `Ansible Tower`  UI 界面的简单介绍
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***
这里` AWX `使用的是最新的版本 ` AWX 21.7.0 `

![在这里插入图片描述](https://img-blog.csdnimg.cn/ee02c95475d14e1b970e9b5abbc1634e.png)

目前 `Ansible Tower `最新版是` 3.8.6 `，博客中的版本为` 3.5.0 `，老师给的环境

![在这里插入图片描述](https://img-blog.csdnimg.cn/5acc65e267d04bc48895c943a7559d88.png)


对于 `Redhat`的 ` Ansible Tower ` ，官网上看到在 2022 年 11 月之后不在维护了，改版之后现在叫 [Ansible Automation Platform](https://www.redhat.com/en/resources/ansible-automation-platform-datasheet)，感兴趣小伙伴可以了解下

关于两者之间的区别可以看看：[https://www.rogerperkin.co.uk/network-automation/ansible/ansible-tower-vs-ansible-automation-platform/](https://www.rogerperkin.co.uk/network-automation/ansible/ansible-tower-vs-ansible-automation-platform/)

![在这里插入图片描述](https://img-blog.csdnimg.cn/be3919ff666a41768019cfae84683447.png)

虽然有` AWX `是` Tower `的上游版本，但 `Ansible Automation Platform` 严格来说是企业生产，只能通过 Red Hat 订阅获得。

` AWX `和` Tower `的区别：[https://www.ansible.com/products/awx-project/faq](https://www.ansible.com/products/awx-project/faq)





###  AWX  控制面板

控制面板含有四个报告区域:

+ `资源概况`：控制面板的顶部是关于受管主机、清单和 Ansible 项目的状态的摘要报告。
+ `作业状态`：作业是  AWX  运行 playbook 的一次尝试。这一区域中提供随时间成功和失败的作业数的图形化显示。
+ `最近的模板`：这一区域显示最近用于执行作业的作业模板列表。
+ `最近的作业`：这一区域显示最近执行的作业以及执行日期和时间的列表。


![在这里插入图片描述](https://img-blog.csdnimg.cn/a553c1203448403f839832826346a211.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/1a1df67e6a2b4349acb30aac7dd50d60.png)




### 导航栏

AWX  Web UI 左侧提供一系列导航链接，可用于访问常用的  AWX  资源。


+ `作业`：作业表示  AWX  针对某一主机清单单次运行某一 Ansible Playbook。
+ `模板`：模板定义了用于通过  AWX  启动作业(以运行 Ansible Playbook)的参数。
+ `凭据`：使用此接口管理凭据。凭据是身份验证数据，供  AWX  用于登录受管主机来运行 play，解密Ansible Vault 文件，从外部来源同步清单数据，从版本控制系统下载更新过的项目资料，以及执行类似任务。
+ `项目`：项目表示一组相关的 Ansible Playbook。
+ `Inventories 主机清单`：清单包含一组要管理的主机。
+ `清单脚本`：使⽤此界面管理从外部来源(如云提供商和配置管理数据库 (CMDB) 等)生成和更新动态清单的脚本。(只有Tower有)
+ `Organizations 机构`：使用此界面管理  AWX  内的组织实体，表示  AWX  资源的逻辑集合。
+ `用户`：使用此界面管理  AWX  用户。
+ `Teams`：使用此界面管理  AWX  团队。
+ `Notifications`：使用此界面管理通知模板。
+ `Management Jobs`：使用此界面管理系统作业，这将清理来自  AWX  操作的旧数据。


![在这里插入图片描述](https://img-blog.csdnimg.cn/f115dea8d6834ca1bc10181ebcd9a8b5.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/8e529269531145798ddd276e22bf81aa.png)



### 管理工具链接
 AWX  Web UI 的右上方包含各种  AWX  管理工具的链接。

![在这里插入图片描述](https://img-blog.csdnimg.cn/b684d6067dac4fbeae1d346d2b0ca9d5.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/179ac112ec274ccb838449b362e116c3.png)

+ `账户配置`：当前用户账户名称显示为一个链接。可以点击进入配置界面。
+ `关于`：显示  AWX  的已安装版本，以及使用的 Ansible 版本。
+ `查看文档`：在新窗口显示  AWX  文档网站。
+ `注销`：从  AWX  Web UI 注销。



###  AWX  设置

单击左侧导航栏中的 Settings，以访问  AWX  Settings 页面。

Settings 页面中提供的不同类型如下：

+ `身份验证`：身份验证类别包含的设置用于在  AWX  中使用第三方登录信息(如 LDAP、AzureActive Directory、GitHub 或 Google OAuth2)为用户帐户配置简化的身份验证。

+ `作业`：作业类别包含用于配置作业执行的高级设置。来控制用户可以设置的计划作业数量、支持由  AWX  启动 ad hoc 作业的 Ansible 模块，以及项目更新、事实缓存和作业运行的超时。



+ `系统`：系统类别包含高级设置，可以使用它们来配置日志聚合、活动流设置和其他各种  AWX  选项。
+ `用户界面`：用户界面类别允许配置分析报告，并为  AWX  服务器设置自定义徽标或自定义登录消息。
+ `许可`：Tower 比 AWX 多一个 License,此界面提供安装的许可证的详细信息，也可用于执行许可证管理任务，如安装和升级许可证等。



![在这里插入图片描述](https://img-blog.csdnimg.cn/007d5fbf019a452cbbace93435b942e1.png)

 

![在这里插入图片描述](https://img-blog.csdnimg.cn/1484043773394d68b62319c0109e8c43.png)



### 常规控件

除了前面概述的导航和管理控件外， AWX  Web UI 中也使用了⼀些其它控件。

面包屑导航链接：浏览  AWX  Web UI 时，页面的左上角会创建一个“面包屑”轨迹。此轨迹清楚地标识各个页面的路径，同时还提供了返回到上一页的快捷方式。

活动流：位于 Logout 图表下。单击此图标可显示与当前页面相关的活动的报告。

搜索栏：可用于搜索或过滤数据集合。

## 博文参考

***

《DO447 Advanced Automation Ansible Best Practices》


https://www.redhat.com/en/resources/ansible-automation-platform-datasheet

https://www.rogerperkin.co.uk/network-automation/ansible/ansible-tower-vs-ansible-automation-platform/

