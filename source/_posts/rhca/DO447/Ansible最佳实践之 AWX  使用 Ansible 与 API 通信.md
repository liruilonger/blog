---
title: Ansible最佳实践之 AWX  使用 Ansible 与 API 通信
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: Ansible
uniqueId: '2022-09-01 17:17:02/Ansible最佳实践之 AWX  使用 Ansible 与 API 通信.html'
mathJax: false
date: 2022-09-02 01:17:02
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 分享一些 AWX  使用 Ansible 与 API 通信的笔记
+ 博文内容涉及：
  + curl 方式调用 AWX API
  + 浏览器接口文档方式调用 AWX API
  + 使用 API 调用方式启动 AWX 中 作业模板
  + Ansible 模块 uri 的简单介绍
  + Ansible 剧本方式 调用 API 启动作业模板
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

## API 简单介绍

红帽 AWX 提供了一个类似 `Swagger` 的 RESTful 风格的 Web 服务框架，可以和 awx 直接交互。使管理员和开发人员能够在 web UI之外控制其 AWX  环境。

可以使用自定义脚本或外部应用使用标准HTTP消息访问API。尤其一些 devops 的联动、钩子相关的处理，REST API 的优势之一在于，任何支持 HTTP 协议的编程语言、框架或系统都可以使用API。这提供了一种简单的方式来自动化重复性任务，并将其他企业IT系统与 AWX  集成。

REST架构在客户端和服务器之间提供了无状态通信通道。每个客户端请求的行为都独立于任何其他请求，并且包含完成该请求所需的所有信息。


### 命令行curl的方式调用

请求使用HTTP获取方法检索API主入口点：

![在这里插入图片描述](https://img-blog.csdnimg.cn/8b5eaf8014f640069c346a0d1d583d2b.png)

命令行访问RESTAPI示例：使用HTTP GET方法检索API主入口点。

这里为了展示可读，我们需要安装一个 json 的 格式化插件 `jq`
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$yum -y install jq
```
查看 API 版本等相关信息，这里的 `-k`参数取消 tocker 认证， -s 取消请求的过程展示

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET http://192.168.26.82:30066/api/ -s -k | jq
{
  "description": "AWX REST API",
  "current_version": "/api/v2/",
  "available_versions": {
    "v2": "/api/v2/"
  },
  "oauth2": "/api/o/",
  "custom_logo": "",
  "custom_login_info": "",
  "login_redirect_override": ""
}
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
查看 当前版本的 所有 API 接口
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET http://192.168.26.82:30066/api/v2/ -s -k | jq
{
  "ping": "/api/v2/ping/",
  "instances": "/api/v2/instances/",
  "instance_groups": "/api/v2/instance_groups/",
  "config": "/api/v2/config/",
  "settings": "/api/v2/settings/",
  "me": "/api/v2/me/",
  "dashboard": "/api/v2/dashboard/",
  "organizations": "/api/v2/organizations/",
  "users": "/api/v2/users/",
  "execution_environments": "/api/v2/execution_environments/",
  "projects": "/api/v2/projects/",
  "project_updates": "/api/v2/project_updates/",
  "teams": "/api/v2/teams/",
  "credentials": "/api/v2/credentials/",
  "credential_types": "/api/v2/credential_types/",
  "credential_input_sources": "/api/v2/credential_input_sources/",
  "applications": "/api/v2/applications/",
  "tokens": "/api/v2/tokens/",
.........
  "mesh_visualizer": "/api/v2/mesh_visualizer/"
}
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
查看指定业务接口的相关信息
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET http://192.168.26.82:30066/api/v2/ping/ -s -k | jq
{
  "ha": false,
  "version": "21.7.0",
  "active_node": "awx-demo-65d9bf775b-hc58x",
  "install_uuid": "d969a45a-6674-4e6b-87a5-2f4184de62b0",
  "instances": [
    {
      "node": "awx-demo-65d9bf775b-hc58x",
      "node_type": "control",
      "uuid": "e01fe449-b2f6-4df4-ae55-a5639f601b7b",
      "heartbeat": "2022-10-24T08:20:04.405470Z",
      "capacity": 79,
      "version": "21.7.0"
    }
  ],
  "instance_groups": [
    {
      "name": "controlplane",
      "capacity": 79,
      "instances": [
        "awx-demo-65d9bf775b-hc58x"
      ]
    },
    {
      "name": "default",
      "capacity": 0,
      "instances": []
    }
  ]
}
```
查看主机的相关的信息，这里需要登录，所以提示么有会话信息
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET http://192.168.26.82:30066/api/v2/hosts/ -s -k | jq
{
  "detail": "Authentication credentials were not provided. To establish a login session, visit /api/login/."
}
```
传递用户名密码就可以查看主机信息
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET http://192.168.26.82:30066/api/v2/hosts/ -s -k --user devops:devops | jq
{
  "count": 2,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": 8,
      "type": "host",
      "url": "/api/v2/hosts/8/",
      "related":
      ................
```
### 浏览器页面调用

也可以通过 浏览地址的方式，通过接口文档地址调用，http://192.168.26.82:30066/api/


![在这里插入图片描述](https://img-blog.csdnimg.cn/c9c541c606ff4aaab64bc1911789594c.png)

这样的好处是可以查看相关的接口信息，参数信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/65412ffd8cca4690a6c5378749b84965.png)


## 使用 API 启动作业模板

API 的一个常见用途是启动现有的作业模板。可以通过 API 中的名称或者作业ID来引用作业模板：


从 API 启动作业模板是分两个步骤完成的：

+ 使用 GET 方法访问它，以获取有关启动该作业所需的任何参数或数据的信息。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET --user admin:tP59YoIWSS6NgCUJYQUG4cXXJIaIc7ci http://192.168.26.82:30066/api/v2/job_templates/"liruilong_job_template"// -s | jq
{
  "id": 11,
  "type": "job_template",
  "url": "/api/v2/job_templates/11/",
  .....
  
```
过滤出执行作业的相关接口

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET --user admin:tP59YoIWSS6NgCUJYQUG4cXXJIaIc7ci http://192.168.26.82:30066/api/v2/job_templates/"liruilong_job_template"// -s | jq | grep -m 1  launch
    "launch": "/api/v2/job_templates/11/launch/",
```
+ 使用 POST 方法访问它以启动该作业。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X POST --user admin:tP59YoIWSS6NgCUJYQUG4cXXJIaIc7ci http://192.168.26.82:30066/api/v2/job_templates/11/launch/ -k -s | jq
{
  "job": 72,
  "ignored_fields": {},
  "id": 72,
  "type": "job",
  "url": "/api/v2/jobs/72/",
```
查看作业的执行情况
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET  --user admin:tP59YoIWSS6NgCUJYQUG4cXXJIaIc7ci http://192.168.26.82:30066/api/v2/jobs/72/ -k -s | jq
{
  "id": 72,
  "type": "job",
  "url": "/api/v2/jobs/72/",
  "related": {
    "created_by": "/api/v2/users/1/",
    "labels": "/api/v2/jobs/72/labels/",
    "inventory": "/api/v2/inventories/2/",
```
通过 jq 获取状态字段的值，这里可以看到当前 作业 状态`failed`， 失败了。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET  --user admin:tP59YoIWSS6NgCUJYQUG4cXXJIaIc7ci http://192.168.26.82:30066/api/v2/jobs/72/ -k -s | jq .status
"failed"
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```



### 使用 Ansible Playbook 中的 API 启动作业


可以使用 Ansible Playbook 来启动作业模板，方法是使用 uri 模块来访问  AWX   API。也可以从  AWX   中的作业模板运行该 playbook，并使用它将另一作业模板作为其任务之一来启动。

在 Playbook 中必须为  AWX   提供足够的凭据，以便作为拥有启动该作业的权限的用户进行身份验证：

这里为了方便在 playbook 中嵌入了用于向  AWX   服务器进行身份验证的用户名和密码。若要保护这些数据，应该使用 Ansible Vault 加密 playbook，或者将机密移到一个变量文件中，再使用 Ansible Vault加密该文件。

#### uri模块与APl交互

Red Hat Ansible Engine可以使用uri模块与提供任意HTTPAPl类型的服务进行交互，包括RESTfulAPl。

参数，用于指定连接到服务器的HTTP方法，支持以下值：
+ GET，从URL标识的服务中获取实体信息，这是默认值。
+ POST，要求服务将实体信息存储在URL标识的资源下。
+ UT，要求服务将实体信息存储为URL标识的资源，如果存在则对其进行修改。
+ DELETE，删除服务中URL标识的实体。
+ PATCH，使用主体中的值修改请求URL所标识的实体。正文中只能有修改的值。

#### 向API发送信息
有两个相互排斥的参数来发送此信息：
+ src选项，指向一个文件，该文件包含要发出的HTTP请求的正文。
+ body选项，以YAML格式定义HTTP请求的正文。
+ body_format选项，用于控制接收服务返回的格式。此选项支持3中格式：raw，json和form-urlencoded。对于RESTAPI，请使用json；对于基于表单的传统页面，请使用form-urlencoded。
示例：
```yml
- name: Login to a form-based webpage 
  uri: 
    url: https://example.com/login.php 
    method: POST 
    body_format: form-urlencoded
    body:
      name: your_username 
      password: your_password 
      enter: Sign in
```

#### 处理API响应

任何HTTP服务都会返回响应状态码信息。使用`status_code`选项将您期望成功的状态码告诉uri模块。`如果响应中的状态码不同，则任务将失败。`
```yaml
- name: Login to a form-based webpage 
  uri:
    url: https://example.com/login.php 
    status_code: 200    
```

您必须处理响应本身：

+ 如果要将响应另存为文件，请使用dest选项指定。
+ 如果要在剧本中使用响应，请使用`return_content`选项将响应的正文添加到结果中，并将其保存在`register`变量中。


一个Demo，这里用于获取当前的用户信息

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cat api.yaml
---
- hosts: localhost
  tasks:
    - name: get job-template
      uri:
       # url: http://192.168.26.82:30066/api/v2/job_templates/11/
        url: http://192.168.26.82:30066/api/v2/users/
        method: GET
        user: admin
        password: tP59YoIWSS6NgCUJYQUG4cXXJIaIc7ci
        validate_certs: False
        force_basic_auth: true
        return_content: true
      register: awx_api_result
    - debug:
        var: awx_api_result
    - name: Print job
      debug:
        msg: "{{ item['username'] }}"
      loop: "{{ awx_api_result['json']['results'] }}"
```


## 博文引用资源

《DO447 Advanced Automation Ansible Best Practices》

