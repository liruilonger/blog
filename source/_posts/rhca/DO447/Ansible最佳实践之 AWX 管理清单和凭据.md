---
title: Ansible之 AWX 管理清单和凭据的一些笔记
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: Ansible
uniqueId: '2022-09-01 17:15:34/Ansible最佳实践之 AWX 管理清单和凭据.html'
mathJax: false
date: 2022-09-012 01:15:34
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 分享一些 AWX 创建清单和凭据的笔记
+ 博文内容涉及：
  + 创建静态清单，清单角色，变量的配置
  + 创建凭据，凭据类型，角色等配置
  + 使用创建清单和凭据运行 ad-hoc 的 Demo
+ 食用方式： 需要了解 Ansible  
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

关于清单，不多讲，这里的凭据即我们配置的 SSH 密钥，或者帐密信息，通过 SSH 来运行 Ansible 命令或者剧本，我们需要有SSH的相关认证信息。凭据就是这些认证信息

## 创建静态清单

创建要管理的清单，并设置  AWX  所需的凭据，以登录并在这些系统上运行 Ansible 剧本或者临时命令，当然，在 AWX 中，我们更多的是叫作业





### 在 AWX 中创建清单

清单在  AWX  中以单独的对象进行管理。,组织可能有多个可用的清单。创建作业模板时，可以为它们指定特定清单。` AWX  上的哪些用户可以使用清单对象取决于其在清单中的角色。`


清单对象列表

![在这里插入图片描述](https://img-blog.csdnimg.cn/8bf4111a02544861a512996e7f3d5826.png)

创建静态清单

![在这里插入图片描述](https://img-blog.csdnimg.cn/5b87c96429c4482396ada8a01d1259b9.png)

添加主机组

![在这里插入图片描述](https://img-blog.csdnimg.cn/c958d7ed24a64f3591e9c52096420aa2.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/7134c3bc1dae4ab2ace03ba002475d8b.png)

查看主机组列表

![在这里插入图片描述](https://img-blog.csdnimg.cn/87875babb60b480b90ae1a635099fcbf.png)

添加主机


![在这里插入图片描述](https://img-blog.csdnimg.cn/b4b857b493b343d083da9a75191e0114.png)

编辑主机信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/89ecfcbfe20d4f2c94e470ef9ef6ae47.png)

在添加一个

![在这里插入图片描述](https://img-blog.csdnimg.cn/0f5b80b4bd1346b693995babd73a7e3e.png)



### 清单角色


通过清单的访问控制我们来看一下清单的角色，下面为为该清单添加团队角色

![在这里插入图片描述](https://img-blog.csdnimg.cn/a4cdfd99ae6f4289bc87139088d51cd9.png)

清单的可用角色的列表：
+ 管理员/Admin：清单 Admin 角色授予用户对清单的完全权限。
+ 更新/Update：清单 Update 角色授予用户从其外部数据源更新动态清单的权限。
+ 运行临时命令/Ad Hoc：清单 Ad Hoc 角色授予用户使用清单执行 ad hoc 命令的权限。
+ 使用/Use：清单 Use 角色授予用户在作业模板资源中使用清单的权限。
+ 读取/Read：清单 Read 角色授予用户用户查看清单内容的权限。


首次创建清单时，只能由拥有清单所属组织的 `Admin、Inventory Admin 或 Auditor `角色的用户访问。

![在这里插入图片描述](https://img-blog.csdnimg.cn/bc92138fab5c45b6bbfcf5d7a3fe2a6f.png)


### 配置清单变量

在  AWX  中管理静态清单时，可以直接在清单对象中定义清单变量。而不是使用` host_vars` 和`group_vars `目录。

**注意：如果项目有 host_vars 和 group_vars 文件，不能在  AWX  中进行编辑这些文件。如果在两个变量目录文件中，以及在通过 Web UI 管理的静态清单对象中都定义了相同的主机或组变量，并且它们具有不同的值，则很难预测  AWX  将要使用的值。**

在 清单 界面中，通过 编辑 图标来设置变量：

在`清单内创建主机组`时，可以在 变量 字段中使用 YAML 或 JSON 来定义`组变量`，也可以通过 Edit Group 来修改`组变量`：

![在这里插入图片描述](https://img-blog.csdnimg.cn/b764573c38c147f3863dc8243b41cff0.png)

在`清单内创建单个主机`时，可以在 界面中使用 YAML 或 JSON 来定义`组变量`，也可以通过 Edit Host 来设置`组变量`：

## 创建用于访问清单主机的凭据

为清单创建计算机凭据，以允许`  AWX  `使用` SSH `在清单主机上运行作业

### 凭据
凭据也是`  AWX  `对象，用于进行远程系统的身份验证。凭据可以提供`密码`和` SSH 密钥`，以成功访问或使用远程资源。

 AWX  负责安全的存储这些凭据，`凭据和密钥在加密`之后保存到  AWX  数据库，`无法从  AWX  用户界面以明文检索`。虽然可以为用户或团队分配使用凭据的特权，但是这些机密不会透露给它们。

当  AWX  需要某一凭据时，它会在`内部解密数据`并直接传递给 SSH 或其它程序。

### 凭据类型

![在这里插入图片描述](https://img-blog.csdnimg.cn/9b7b81878e564076a78af1cf74a88216.png)

 AWX  可以管理许多不同类型的凭据，包括：
+ `Machine`：用于对清单主机的 Playbook 登录和特权升级进行身份验证。
+ `Network`：用于 Ansible 网络模块管理网络设备。
+ `SCM`：用于项目从远程版本控制系统克隆或更新 Ansible 项目资料。
+ `Vault`：用于解密存储在 Ansible Vault 保护中的敏感信息。
+ `自定义凭据`：管理员可以定义自定义凭据类型，不建议修改


### 创建计算机凭据

凭据通过位于左侧导航栏上的`  AWX  凭据` 链接下的页面进行管理。任何用户都可以创建凭据，并被视为该凭据的所有者。

![在这里插入图片描述](https://img-blog.csdnimg.cn/9ab0ccb390e442f7af72ebcca70c392c.png)


创建凭据

![在这里插入图片描述](https://img-blog.csdnimg.cn/b4272c5ae99d416096c2705ded279ee5.png)

可用通过`帐密，SSH密钥，签名的SSH证书`三种方式配置

需要提权，则配置提权信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/1b2740db742a415c8c286c988510ec8a.png)

編輯凭据

![在这里插入图片描述](https://img-blog.csdnimg.cn/8203227fae6c4ac2872ef5be269d8641.png)


如果没有为`组织分配凭据`，则它是`专用凭据`，只有拥有该凭据的`用户`和具有 ` AWX  系统管理员`才可以使用。

`专用凭据`与`分配给组织的凭据`的主要区别如下：
+ 任何用户都可以创建`专用凭据`，但只有拥有组织的` Admin 角色的  AWX  系统管理员`和用户才能创建`组织凭据`。
+ 如果凭据属于某个组织，则可以为用户和团队授予其角色，并且`凭据可以共享`。`未分配到组织`的`专用凭据`仅可由`所有者`和 ` AWX  角色`使用，其它用户和团队不能被授予角色。


任何用户都可以创建凭据，并视为该凭据的所有者。


### 凭据角色

![在这里插入图片描述](https://img-blog.csdnimg.cn/6efaeb8cda724f6fa8ac0389b82d1de1.png)

凭据可用的角色：
+ `Admin`：授予用户对凭据的完全权限。
+ `Use`：授予用户在作业模板中使用凭据的权限。
+ `Read`：授予用户查看凭据详细信息的权限。


管理凭据访问权限过程,将添加的凭据添加 teams 授予权限

![在这里插入图片描述](https://img-blog.csdnimg.cn/e599d5d79dff42e1a4fe07275f0c104d.png)



### 常见使用凭据的场景
以下是一些常见的使用凭据的场景。

**由  AWX  保护的凭据，不被用户所知**

使用  AWX  凭据的一种常见场景是将任务的执行从管理员委派给一级支持人员。由于凭据由支持人员的团队共享，因此应创建⼀个组织凭据资源，以存储对受管主机进行 SSH会话身份验证所需的用户名、SSH 私钥和 SSH 密钥。该凭据还存储特权升级类型、用户名和 sudo 密码信息。创建后，该凭据可供支持人员用于在受管主机上启动作业，而无需知道 `SSH 密钥` 或 `sudo 密码`。



**凭据提示输入敏感密码，而不是存储在  AWX  中**

另⼀种场景是使用凭据来存储用户名身份验证信息，同时在使用凭据时仍以交互方式提示输入敏感密码。
可以配置为在某个作业`使用凭据时提示用户输入帐户的密码`，方法是`选中 PASSWORD 的 Prompt on launch 复选框`。比如数据库密码等特殊凭证

这里的启动时提示可用于动态的输入密码信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/1b2740db742a415c8c286c988510ec8a.png)

## 实战

看一个 Demo,通过上面创建的清单和凭据，执行一个 ad-hoc 作业。

创建一个用户
![在这里插入图片描述](https://img-blog.csdnimg.cn/d249caa8529149eda2b8051b84dc6972.png)

并且加入到liruilonger 这个团队里
![在这里插入图片描述](https://img-blog.csdnimg.cn/0fae9f06874d4197a9efce2b0b9f7ddd.png)

上面我们给liruilonger 这个teams对应的添加了相关的角色，所以这里通过devops用户来执行执行巡检操作

![在这里插入图片描述](https://img-blog.csdnimg.cn/32f2ad2b8bfe46f4b49f149b893e5bf3.png)


执行一个临时命令。通过清单`ad hoc`的方式

![在这里插入图片描述](https://img-blog.csdnimg.cn/e05ba08572844940aa4e939429f41775.png)

查看下节点中kubelet服务 的状态

![在这里插入图片描述](https://img-blog.csdnimg.cn/c88818b81ed84932ac63763654cb60ee.png)

选择之前创建的凭据
![在这里插入图片描述](https://img-blog.csdnimg.cn/4b5ea8385c5a487d967a4072313350a0.png)

执行作业：通过输出我们可以查看 工作节点的 kubelet 状态
![在这里插入图片描述](https://img-blog.csdnimg.cn/9ceb2e1ce4154bcd9992b312c7ede473.png)
可以对输出日志进行查询

刚才的任务状态信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/91acb2d8fea14348b2c012c1bb9807f5.png)



## 博文引用资源

《DO447 Advanced Automation Ansible Best Practices》




