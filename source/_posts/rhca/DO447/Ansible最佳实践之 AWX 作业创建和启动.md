---
title: Ansible最佳实践之 AWX 作业创建和启动
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: Ansible
uniqueId: '2022-09-21 17:16:03/Ansible最佳实践之 AWX 作业创建和启动.html'
mathJax: false
date: 2022-09-02 01:16:03
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 分享一些 AWX 作业创建和启动的笔记
+ 博文内容涉及：
  + 创建作业模板
  + 涉及相关参数，作业模板角色配置介绍
  + 运行作业模板并测试的Demo 
+ 食用方式： 需要了解 Ansible
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***

## 创建作业模板和启动作业

### 作业模板、项目和清单

`作业模板`是一个用来启动运行 `Playbook 的作业`的模板。

我们创建了项目，这是时候，当前剧本只有项目，也就是要执行的剧本，执行剧本涉及的主机清单，变量等都没有，这里的项目类型于一个角色的 `task` 目录下的部分

作业模板将`项目`中的 `Playbook` 与`主机清单`、用于身份验证的`凭据`以及在启动 Ansible 作业以运行该 Playbook时使用的`其他参数、变量`相关联。

用户是否可以启动作业或创建具有特定项目和清单的作业模板取决于已为其分配的角色。当被授权` Use `角色时，用户可以使用作业模板将项目与清单关联。

作业模板定义了用于执行 Ansible 作业的`参数`。作业模板还必须定义将用于对受管主机进行身份验证的计算机`凭据`。

### 创建作业模板

与其它  AWX  资源不同，作业模板不直接属于某一组织，而是属于`某一组织的项目使用`。作业模板与组织的关系由它所使用的项目决定。

由于必须使用清单、项目和计算机凭据定义作业模板，因此只有当`用户被分配了这三个  AWX  资源中的⼀个或多个的 Use 角色`时，它们才可`创建作业模板`。

创建作业模板

![在这里插入图片描述](https://img-blog.csdnimg.cn/45ba9a0d9bae4b009f61ee2027a8f443.png)

可以设置超时，作业分片等
![在这里插入图片描述](https://img-blog.csdnimg.cn/3b98cca7e0da4d4195e0b360facf883a.png)

这里方便测试，使用之前手工创建的项目，下面为修改项目的执行剧本，这里我们使用 `yum` 模块来安装 `htop` 这个监控软件
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl exec -it awx-demo-65d9bf775b-hc58x -c awx-demo-task  -- bash
bash-5.1$ cat /var/lib/awx/projects/liruilong_manual/test.yaml
---
- name: awx demo
  hosts: all
  tasks:
    - name: yum htop
      yum:
        name: htop
        state: installed

bash-5.1$ exit
exit
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

### 修改作业执行
作业模板具有其它配置，来调整启动模板时，下面问参数对应的描述：
+ 描述/DESCRIPTION：用于存储作业模板的可选描述。
+ 并行数/FORKS：控制在 Playbook 执行期间允许的并行进程数量。
+ 限制/LIMIT：限制由作业模板的清单提供的受管主机的列表。
+ 设置标签/JOB TAGS：接受在 Playbook 中存在以逗号分隔的标记列表。可以选择性的仅执行 Playbook 的特定部分。
+ 跳过标签/SKIP TAGS：接受在 Playbook 中存在以逗号分隔的标记列表。可以选择性的在 Playbook 执行期间条做其中的特定部分。
+ 标签/LABELS：标记是可以附加到作业模板来帮助分组或过滤作业模板的名称。
+ 提权/Enable Privilege Escalation：启用后，Playbook 将使用特权来执行。
+ 回调设置/Allow Provisioning Callbacks：启用后，会在  AWX  上创建回调 URL，供主机使用作业模板请求配置更新。
+ 是否并发执行/Enable Concurrent Job：启用后，将允许多次同时执行此作业模板。
+ 利用事实缓存/Use Fact Cache：启用后，将使用缓存的 Facts ，并将新发现的 Facts 存储在  AWX  上的 Facts缓存中。
+ 变量/EXTRA VARIABLES：与 ansible-playbook 命令的 -e 选项等效，用于将额外的命令行变量传递到作业执行的 Playbook。


### 提示输入作业参数

 AWX  允许作业模板中的某些参数在作业执行时提示用户输⼊。这⼀ `启动时提示` 选项适用于：
+ JOB TYPE
+ INVENTORY
+ CREDENTIAL
+ LIMIT
+ VERBOSITY
+ JOB TAGS
+ SKIP TAGS
+ EXTRA VARIABLES

在作业执行时能够灵活地更改作业参数有助于重复利用 Playbook。

### 作业模板角色

有三种角色可用于控制用户对作业模板的访问权限。

+ Admin：为用户提供了删除作业模板或编辑其属性的角色。
+ Execute：授予用户使用作业模板执行作业的权限。
+ Read：授予用户查看作业模板属性的只读访问权限。

![在这里插入图片描述](https://img-blog.csdnimg.cn/9722b712b7b742299f7b747f90d0f187.png)



### 管理作业模板访问权限

首次创建作业模板时，只能由创建它的用户或特定用户进行访问。

编辑作业模板访问权限

![在这里插入图片描述](https://img-blog.csdnimg.cn/b559df97d9274c34b2ce48e62843bae3.png)



###　启动作业

创建作业模板后，需要使用具有 Excute 角色的用户来启动作业。

启动作业过程

![在这里插入图片描述](https://img-blog.csdnimg.cn/e880e9420740429ea49eacf718d4b22c.png)





### 评估作业结果
从  AWX  web 界面中的作业模板启动作业运行后，该用户会自动重定向到该作业的详细信息页面。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2025f1bfa4fb470ca9ff7905a04cc3c1.png)

可以看到剧本状态为 change ，说明运行成功

![在这里插入图片描述](https://img-blog.csdnimg.cn/7db1aecc35964768834932ebeb3d5d14.png)

测试安装结果

![在这里插入图片描述](https://img-blog.csdnimg.cn/e6ddb0b0964944f484e5edca95b040ce.png)

我们可以使用 ad-hoc 的方式测试 命令是否存在

![在这里插入图片描述](https://img-blog.csdnimg.cn/1c91c2b2c64649d180c7c32f89a27954.png)

## 博文参考

《DO447 Advanced Automation Ansible Best Practices》
