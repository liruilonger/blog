---
title: Ceph：关于Ceph 集群如何访问的一些笔记
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-05-20 08:45:40/Ceph：关于Ceph 集群如何访问的一些笔记.html'
mathJax: false
date: 2023-05-20 04:45:40
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***


+ 准备考试，整理 Ceph 相关笔记
+ 博文内容涉及,Ceph 集群四种访问方式介绍及 Demo，Ceph 客户端支持的操作介绍
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


# Ceph 访问方式

Ceph 提供四种访问 Ceph 集群的方法：

1. `Ceph 原生 API (librados)`:通过 客户端调用 `API` 接口，最快
2. `Ceph 块设备(RBD、librbd)`，也称为 `RADOS 块设备 (RBD) 镜像`
3. `Ceph 对象网关(RADOSGW,RGW,librgw)`
4. `Ceph 文件系统(CephFS、libcephfs)`

![在这里插入图片描述](https://img-blog.csdnimg.cn/9da7f56594d24033aea5bb69590a5af3.png)

上图描述了Ceph集群的四种数据访问方法，支持访问方法的库，以及管理和存储数据的底层Ceph组件


## Ceph 原生API (librados)

`librados` 是原生C 库，允许应用直接使用 `RADOS` 来访问 `Ceph` 集群中存储的对象，可以用 `C++、Java、Python、Ruby、Erlang 和 PHP`，编写软件以直接与 librados 配合使用可以提升性能，为了简化对 Ceph 存储的访问，也可以改为使用提供的更高级访问方式，如 `RADOS 块设备、Ceph 对象网关 (RADOSGW) 和 CephFS`

### Demo
```py
import rados

# 初始化 Ceph 集群连接
cluster = rados.Rados(conffile='/etc/ceph/ceph.conf')
cluster.connect()

# 获取所有 OSD 的状态信息
osd_stats = cluster.get_osdmap().dump()
for osd in osd_stats['osds']:
    print('OSD ID: {}, Up: {}, In: {}'.format(osd['osd'], osd['up'], osd['in']))

# 关闭 Ceph 集群连接
cluster.shutdown()

```
这个 Python 脚本通过 rados 模块来连接 Ceph 集群，然后使用 get_osdmap() 方法获取 OSD 的状态信息，并打印出每个 OSD 的 ID、Up 和 In 状态。最后使用 shutdown() 方法关闭连接。



## RADOS 块设备(RBD)

`Ceph 块设备(RADOS 块设备或 RBD)`通过 RBD 镜像在 Ceph 集群内提供块存储。它是一种`虚拟块设备`

Ceph 分散在集群不同的 OSD 中构成 RBD 镜像的个体对象。由于组成 RBD 的对象分布到不同的 OSD，对块设备的访问自动`并行处理`


RBD 提供下列功能：
1. Ceph 集群中虚拟磁盘的存储
2. Linux 内核中的挂载支持
3. QEMU、KVM 和 OpenStack Cinder 的启动支持

RBD 是指 Ceph 块设备（Ceph Block Device）。它是一种基于 Ceph 存储集群的虚拟块设备，可以提供高性能、可扩展和高可用性的存储服务，尤其适合于云计算和虚拟化环境中使用。

### Demo

要使用 RBD，需要完成以下几个步骤：

创建 RBD 镜像，首先需要创建一个 RBD 镜像，可以使用 `rbd create` 命令来创建，例如：
```bash
$ rbd create <pool>/<image> --size <size>
```
其中 `<pool>` 是存储池名称，`<image>` 是镜像名称，`<size>` 是镜像大小。

映射 RBD 镜像,接下来需要将 RBD 镜像映射到本地系统上，可以使用 `rbd map` 命令来实现，例如：
```bash
$ rbd map <pool>/<image>
```

格式化和挂载 RBD 设备,完成映射后，RBD 镜像会作为一个设备出现在本地系统上，可以通过格式化和挂载来使用它，例如：

```bash
$ mkfs.ext4 /dev/rbdX
$ mount /dev/rbdX /mnt/rbd
```
其中 `/dev/rbdX` 是 `RBD` 设备名，`/mnt/rbd` 是挂载点。

使用 RBD 设备,完成挂载后，就可以像使用本地磁盘一样使用 RBD 设备了，例如在 `/mnt/rbd` 下创建文件、目录等操作。要卸载 RBD 设备，可以使用 `umount /mnt/rbd` 命令，并通过 `rbd unmap` 命令来取消映射，例如：
```bash
$ rbd unmap /dev/rbdX
```

以上就是使用 Ceph 中的 RBD 的基本流程。值得注意的是，RBD 镜像的大小并不是固定不变的，也可以使用 `rbd resize` 命令来扩展或缩小镜像的大小。


## Ceph 对象网关(RADOS 网关)

Ceph 对象网关(RADOS 网关、RADOSGW 或 `RGW`)是使用`librados` 构建的对象存储接口。它使用这个库来与 Ceph 集群通信并且直接写入到 OSD 进程。

它通过 `RESTful API `为应⽤提供了网关，并且支持两种接口：`Amazon S3 和 OpenStack Swift`

Ceph 对象网关提供扩展支持，它不限制可部署的网关数量，而且支持标准的 HTTP 负载均衡器。它解决的这些案例包括：

1. 镜像存储(例如，SmugMug 和 Tumblr)
2. 备份服务
3. 文件存储和共享(例如，Dropbox)

### Demo

```bash
import boto3

# 初始化 S3 客户端连接
s3 = boto3.client('s3',
                  endpoint_url='http://<RGW_HOST>:<RGW_PORT>',
                  aws_access_key_id='<ACCESS_KEY>',
                  aws_secret_access_key='<SECRET_KEY>')

# 将文件上传到 Ceph 对象存储桶中
with open('/path/to/local/file', 'rb') as f:
    s3.upload_fileobj(f, '<BUCKET_NAME>', 'object_key')

# 从 Ceph 对象存储桶中下载文件
with open('/path/to/local/file', 'wb') as f:
    s3.download_fileobj('<BUCKET_NAME>', 'object_key', f)

# 删除 Ceph 对象存储桶中的对象
s3.delete_object(Bucket='<BUCKET_NAME>', Key='object_key')

```

## Ceph 文件系统 (CephFS)

Ceph 文件系统 (CephFS) 是一种并行文件系统，提供可扩展的、单层级结构共享磁盘，Ceph 元数据服务器 (MDS) 管理与 CephFS 中存储的文件关联的元数据 ，这包括文件的访问、更改和修改时间戳等信息

### Demo
在运行前，请确保已经安装了 ceph-fuse 工具，并正确配置了 CephFS 文件系统的访问密钥等信息
```bash
# 挂载 CephFS 文件系统
sudo mount -t ceph <MONITOR_IP>:<MONITOR_PORT>:<ROOT_DIR> <MOUNT_POINT> -o name=<USERNAME>,secret=<SECRET_KEY>

# 读取文件内容
cat <MOUNT_POINT>/path/to/file

# 卸载 CephFS 文件系统
sudo umount <MOUNT_POINT>
```
这个 shell 脚本使用 mount 和 umount 命令来挂载和卸载 CephFS 文件系统，并使用 cat 命令来读取指定文件的内容。其中` <MONITOR_IP> `和` <MONITOR_PORT> `是 Ceph 集群监视器的 IP 地址和端口号，`<ROOT_DIR> `是 CephFS 文件系统的根目录，`<USERNAME> `和 `<SECRET_KEY>` 是访问 `CephFS` 的用户名和密码，`<MOUNT_POINT> `是要挂载文件系统的本地目录路径，`<MOUNT_POINT>/path/to/file` 是要读取的文件路径。


## Ceph客户端支持的操作

支持云计算的应用程序需要一个有异步通讯能力的简单对象存储接口，Ceph 存储集群提供了这样的接口。客户端直接并行访问对象，包括:

+ 池操作
+ 快照
+ 读/写对象
  + 创建或删除
  + 整个对象或字节范围
  + 追加或截断
+ 创建/设置/获取/删除 XATTRs
+ 创建/设置/获取/删除键/值对
+ 复合操作和 dual-ack 语义

当客户端写入 RBD 映像时，对象映射跟踪后端已存在的RADOS对象，当写入发生时，它会被转换为后端RADOS对象中的偏移量，当对象映射特性启用时，将跟踪RADOS对象的存在以表示对象存在，对象映射保存在librbd客户机的内存中，以避免在osd中查询不存在的对象

对象映射对于某些操作是有益的，例如:

+ 重新调整大小
+ 导出
+ 复制
+ 平衡
+ 删除
+ 读

存储设备有吞吐量限制，这会影响性能和可伸缩性。存储系统通常支持条带化，即跨多个存储设备存储连续的信息片段，以提高吞吐量和性能。当向集群写入数据时，Ceph客户端可以使用`数据分条`来提高性能

## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)

***

<https://docs.ceph.com/en/pacific/architecture/>

<https://docs.ceph.com>

CL260 授课老师课堂笔记

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)

