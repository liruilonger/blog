---
title: Ceph：关于部署 Ceph 存储集群的一些笔记
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-04-10 05:33:20/Ceph：关于部署 Ceph 存储集群的一些笔记.html'
mathJax: false
date: 2023-04-10 13:33:20
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 这部分建议小伙伴直接看官方文档操作
+ 准备考试，整理ceph 相关笔记
+ 博文用于初学和温习
+ 博文内容涉及：集群部署，设置管理节点，集群扩容，配置额外的 OSD
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***




#  部署 Ceph 存储

## 准备集群部署

采用 `cephadm` 工具来进行部署，`cephadm` 是一个用于部署和管理 Ceph 集群的工具。它是在 Ceph 版本 14.2.0 中引入的相对较新的工具。

cephadm 工具旨在通过自动化以前手动完成的许多任务来简化 Ceph 集群的部署和管理。它使用容器来部署和管理 Ceph 服务，这使得更容易管理依赖关系并确保在不同环境中保持一致性。

要使用 cephadm，您需要对 Ceph 及其架构有基本的了解。您还需要具有 Docker 和 Kubernetes 等容器化技术的一些经验。

在使用方面，cephadm 提供了许多命令，可用于部署和管理 Ceph 集群。其中一些最常用的命令包括：

- `ceph orch apply`：此命令用于将配置应用于 Ceph 集群。它可用于部署新服务、更新现有服务或删除不再需要的服务。
- `ceph orch ls`：此命令用于列出当前在 Ceph 集群中运行的服务。
- `ceph orch ps`：此命令用于列出当前在 Ceph 集群中运行的服务的状态。
- `ceph orch rm`：此命令用于从 Ceph 集群中删除服务。
- `ceph orch upgrade`：此命令用于将 Ceph 集群升级到新版本。


cephadm 会包含两个组件
+ `cephadm shell`
+ `cephadm orchestrator`

`cephadm shell` 命令在ceph提供的管理容器中运行一个bash shell，最初使用 cephadm shell 执行集群部署任务、安装并运行集群后执行集群管理任务。

启动 `cephadm shell` 以交互方式运行单个或多个命令，要以交互方式运行它，应使用 cephadm shell 命令打开 shell，然后运行Ceph命令。

`cephadm` 提供了一个命令行来编排 `ceph-mgr` 模块，该模块与外部编排服务进行对接，编排器的作用是协调必须跨多个节点和服务协作进行配置更改

```bash
[root@clienta ~]# cephadm shell 
```

如果要执行非交互式的单个命令，可以用两个破折号连接

```bash
[root@clienta ~]# cephadm shell -- ceph osd pool ls
```

### 规划服务托管

所有集群服务现在都作为容器运行。容器化的 Ceph 服务可以运行在同一个节点上;这叫做“托管”。

Ceph服务的托管允许更好地利用资源，同时保持服务之间的安全隔离。支持与 OSD 配置的守护进程有:`RADOSGW、MOS、RBD-mirror、MON、MGR、Grafana、NFS Ganesha`

### 节点之间的安全通讯

`cephadm `命令使用 SSH 协议与存储集群节点通信。集群 `SSH Key` 是在集群引导过程中创建的。将集群公钥复制到每个主机，使用如下命令复制集群密钥到集群节点:

下面的步骤是 cephadm 自动的，不需要显示处理
```bash
[root@node -]# cephadm shell 
[ceph: root@node /]# ceph cephadm get-pub-key > ~/ceph.pub 
[ceph: root@node /]# ssh-copy-id -f -i ~/ceph.pub root@node.example.com
```

## 部署新集群

部署新集群的步骤如下：

1. 在选择作为引导节点的主机上安装cephadm-ansible包，它是集群中的第一个节点。
2. 在节点上执行cephadm的预检查playbook。该剧本验证主机是否具有所需的先决条件。
3. 使用cephadm引导集群。引导过程完成以下任务:
   + 在引导节点上安装并启动 `Ceph Monitor` 和 `Ceph Manager` 守护进程
   + 创建 `/etc/ceph` 目录
   + 拷贝一份集群SSH公钥到 ·etc/ceph/ceph`，并添加密钥到 `/root/.ssh/authorized_keys` 文件中
   + 将与新集群通信所需的最小配置文件写入 `/etc/ceph/ceph.conf` 文件
   + 写入 `client.admin` 管理密钥到 `/etc/ceph/ceph.client.admin.keyring`
   + 为 `prometheus` 和 `grafana`  服务以及其他工具部署一个基本的监控 `stack`

### 安装先决条件

在引导节点上安装 cephadm-ansible

cephadm-ansible 是 Ansible 剧本的集合，用于简化 CEPHADM 未涵盖的工作流。涵盖的工作流程 是：
+ 安装前检查：在引导集群之前对主机进行初始设置
+ 客户端：设置客户端主机
+ 清除：移除 Ceph 集群

通过下面的方式安装，没有对应 yum 源的话可以通过 github 获取。[https://github.com/ceph/cephadm-ansible/blob/devel/README.md](https://github.com/ceph/cephadm-ansible/blob/devel/README.md),也可以直接通过 `cephadm-ansible` 提供的剧本来安装 `ceph`，当前我们使用 `cephadm` 的方式， `cephadm-ansible` 这里仅仅用作为安装前的检查。
```bash
[root@node ~]# yum install cephadm-ansible
```


运行 `cephadm-preflight`，这个剧本配置 `Ceph` 存储库 并为引导准备存储集群。它还安装必要的包，例如`Podman、lvm2、chrony 和 cephadm`

`cephadm-preflight` 使用 cephadm-ansible inventory 文件来识别 admin 和 client 节点

inventory 默认位置是 `/usr/share/cephadm-ansible/hosts`，下面的例子展示了一个典型的 `inventory`文件的结构:

```ini
[admin]
node00

[clients]
client01
client02
client03
```

运行 `cephadm-preflight`

```bash
[root@node ~]# ansible-playbook \
	-i INVENTORY-FILE \
	-e "ceph_origin=rhcs" \
	cephadm-preflight.yml
```

ceph_origin：Ceph 存储库的来源。默认值为 `community`
有效值：
+ rhcs：来自红帽 Ceph 存储的存储库。
+ community：社区存储库(https://download.ceph.com)
+ custom：自定义存储库。
+ shaman：开发存储库。

### bootstrap 引导集群

`cephadm` 引导过程在单个节点上创建一个小型存储集群，包括一个`Ceph Monitor`和一个`Ceph Manager`，以及任何必需的依赖项，然后通过使用 `ceph orchestrator` 命令或 Dashboard GUI 扩展存储集群添加集群节点和服务

#### 使用cephadm bootstrap引导新集群

引导命令

```bash
[root@node -]# cephadm bootstrap \
  --mon-ip=MON_IP \
  --allow-fqdn-hostname
  --initial-dashboard-password=DASHBOARO_PASSWORO \
  --dashboard-password-noupdate \
  --registry-url=registry.redhat.io \
  --registry-username=REGISTRY_USERNAME \
  --registry-password=REGISTRY_PASSWORD
```
参数说明：
+ --mon-ip: 第一监控器的IP
+ --apply-spec： 服务规范文件
+ --registry-XXX： 镜像仓库相关命令
+ --allow-fqdn-hostname： 主机名使用权限定名
+ --initial-dashboard-password：仪表盘密码
+ --dashboard-password-noupdate： 不允许修改仪表盘密码

```bash
cephadm bootstrap \
--mon-ip=172.25.250.12 \
--apply-spec=/root/ceph/initial-config-primary-cluster.yaml \
--initial-dashboard-password=redhat \
--dashboard-password-noupdate \
--allow-fqdn-hostname \
--registry-url=registry.lab.example.com \
--registry-username=registry \
--registry-password=redhat
```

运行结束后，会输出以下内容，包括仪表盘等相关信息

```bash
Ceph Dashboard is now available at: 
URL: https://boostrapnode.example.com:8443/ 
User: admin 
Password: adminpassword 
You can access the Ceph CLI with: 
sudo /usr/sbin/cephadm shell --fsid 266ee7a8-2a05-lleb-b846-5254002d4916 -c /etc/ceph/ceph.conf -k /etc/ceph/ceph.client.admin.keyring 
Please consider enabling telemetry to help improve Ceph: 
ceph telemetry on
```

### 使用服务规范文件

`cephadm bootstrap` 命令使用 `--apply-spec `选项和服务规范文件用于引导存储集群并配置其他主机和守护进程，配置文件是一个YAML文件，其中包含服务类型、位置和要部署服务的指定节点

服务配置文件示例如下:

```yaml
# 添加三个主机节点
service_type: host
addr: node-00
hostname: node-00
---
service_type: host
addr: node-01
hostname: node-01
---
service_type: host
addr: node-02
hostname: node-02
---
# 添加三个 mon，生产要求至少3个，而且需要是奇数
service_type: mon
placement:
  hosts:
    - node-00
    - node-01
    - node-02
---
# 添加三个 mgr,至少2个
service_type: mgr
placement:
  hosts:
    - node-00
    - node-01
    - node-02
---
# 添加三个 rgw,至少2个
service_type: rgw
service_id: realm.zone
placement:
  hosts:
    - node-01
    - node-02
---
service_type: osd
placement:
  host_pattern: "*"
data_devices:
  all:true
```

```bash
[root@node ~]# cephadm bootstrap \
	--mon-ip MONITOR-IP-ADDRESS \
	--apply-spec CONFIGURATION_FILE_NAME 
```
看一个实际的 Demo,使用下面的服务配置文件来点火一个集群

```yaml
service_type: host
addr: 172.25.250.10
hostname: clienta.lab.example.com
---
service_type: host
addr: 172.25.250.12
hostname: serverc.lab.example.com
---
service_type: host
addr: 172.25.250.13
hostname: serverd.lab.example.com
---
service_type: host
addr: 172.25.250.14
hostname: servere.lab.example.com
---
service_type: mon
placement:
  hosts:
    - clienta.lab.example.com
    - serverc.lab.example.com
    - serverd.lab.example.com
    - servere.lab.example.com
---
service_type: rgw
service_id: realm.zone
placement:
  hosts:
    - serverc.lab.example.com
    - serverd.lab.example.com
---
service_type: mgr
placement:
  hosts:
    - clienta.lab.example.com
    - serverc.lab.example.com
    - serverd.lab.example.com
    - servere.lab.example.com
---
service_type: osd
service_id: default_drive_group
placement:
  host_pattern: 'server*'
data_devices:
  paths:
    - /dev/vdb
    - /dev/vdc
    - /dev/vdd
```

```bash
# cephadm bootstrap \
--mon-ip=172.25.250.12 \
--apply-spec=/root/ceph/initial-config-primary-cluster.yaml \
--initial-dashboard-password=redhat \
--dashboard-password-noupdate \
--allow-fqdn-hostname \
--registry-url=registry.lab.example.com \
--registry-username=registry \
--registry-password=redhat
```


## 为集群节点打标签

`Ceph` 编排器支持为主机分配标签，标签可以用于对集群进行分组 `hosts` ，以便可以同时将Ceph服务部署到多个主机，主机可以有多个标签


标签可以帮助识别每个主机上运行的守护进程，从而简化集群管理任务，例如，您可以使用 `ceph orch host ls`或YAML服务规范文件在特定标记的主机上部署或删除守护进程，可以使用 `ceph orch host ls`命令来列出我们可以用于编排器或YAML服务规范文件，在指定的标签节点上用于部署或删除特定的守护进程

除了_admin 标签外，标签都是自由形式，没有特定的含义，可以使用标签，如 mon, monitor, mycluster_monitor，或其他文本字符串标签和分组集群节点。例如，将 mon 标签分配给部署 mon 守护进程的节点，为部署 mgr 守护进程的节点分配 mgr 标签，并为 RADOS 分配 rgw 网关

例如，下面的命令将 `_admin` 标签应用到主机，用于以指定为 `admin`节点

```bash
[ceph: root@node /)# ceph orch \
	host label add AOMIN_NOOE _admin
```

使用标签将集群守护进程部署到特定的主机

```bash
(ceph: root@node /)# ceph orch \
	apply prometheus --placement="label:prometheus"
```



## 设置Admin节点

配置admin节点的步骤如下:

1. 将admin标签分配给节点
```bash
[ceph: root@node /)# ceph orch \
	host label add AOMIN_NOOE _admin
```
2. 复制admin密钥到管理节点

```bash
[root@node ~]# scp \
	/etc/ceph/ceph.client.admin.keyring ADMIN_NODE:/etc/ceph/
```  
3. 复制ceph.conf文件到admin节点 

```bash
[root@node ~]# scp \
	/etc/ceph/ceph.conf ADMIN_NODE:/etc/ceph/
```



# 执行Ceph存储集群扩容

有两种方法可以扩展集群中的 `存储空间`:

1. 向集群中添加额外的 `OSD` 节点，这称为 `横向扩展`
2. 向以前的 `OSD`节点添加额外的存储空间，这称为 `纵向扩展`

在开始部署额外的osd之前使用 `cephadm shell -- ceph health` 命令确保集群处于`HEALTH_OK` 状态



## 配置更多的OSD服务器

作为存储管理员，可以向Ceph存储集群添加更多主机，以维护集群健康并提供足够的负载容量。在当前存储空间已满的情况下，可以通过增加一个或多个osd来增加集群存储容量。

### 分发ssh密钥

作为root用户，将 Ceph 存储集群 SSH 公钥添加到新主机上 `root` 用户的 `authorized_keys` 文件中

```bash
[root@adm ~]# ssh-copy-id \
	-f -i /etc/ceph/ceph.pub \
	root@new-osd-1
```

### 检查并配置先决条件

作为 root 用户，将新节点添加到目录 `/usr/share/cephadm-ansible/hosts/` 的inventory文件中，使用--limit选项运行preflight剧本，以限制剧本的任务只在指定的节点上运行，Ansible Playbook会验证待添加的节点是否满足前置要求

```bash
[root@adm ~]# ansible-playbook \
	-i /usr/share/cephadm-ansible/hosts/ \
	--limit new-osd-1 \
	/usr/share/cephadm-ansible/cephadm-preflight.yml
```

### 选择添加主机的方法

#### 使用命令

以root用户，在Cephadm shell下，使用ceph orch host add命令添加一个存储集群的新主机，在本例中，该命令还分配主机标签

```bash
[ceph: root@adm /]# ceph orch \
	host add new-osd-1 --labels=mon,osd,mgr
```

#### 使用规范文件添加多个主机

要添加多个主机，创建一个包含主机描述的YAML文件，在管理容器中创建YAML文件，然后运行ceph orch

```yaml
service_type: host
addr:
hostname: new-osd-1
labels:
  - mon
  - osd
  - mgr
---
service_type: host
addr:
hostname: new-osd-2
labels:
  - mon
  - osd
```

使用ceph orch apply 添加OSD服务器

```bash
[ceph: root@adm ~]# ceph orch apply -i host.yaml
```



### 列出主机

ceph orch host ls可以列出所有主机，正常情况下STATUS是空的

```bash
[ceph: root@clienta ~]# ceph orch host ls 
HOST                     ADDR           LABELS  STATUS  
clienta.lab.example.com  172.25.250.10  _admin          
serverc.lab.example.com  172.25.250.12                  
serverd.lab.example.com  172.25.250.13                  
servere.lab.example.com  172.25.250.14                  
```



## 为OSD服务器配置额外的OSD存储

Ceph要求在考虑存储设备时需满足以下条件:
+ 设备不能有任何分区
+ 设备不能有 LVM
+ 设备不能被挂载
+ 该设备不能包含文件系统
+ 设备不能包含 `Ceph BlueStore OSD`
+ 设备大小必须大于5GB

`ceph orch device ls` 可以列出集群中可用的 `osd`，`--wide` 选项可以查看更多详情

```bash
[ceph: root@clienta ~]#  ceph orch device ls --wide
Hostname                 Path      Type  Transport  RPM      Vendor  Model  Serial  Size   Health   Ident  Fault  Available  Reject Reasons                                                 
clienta.lab.example.com  /dev/vdb  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
clienta.lab.example.com  /dev/vdc  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
clienta.lab.example.com  /dev/vdd  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
clienta.lab.example.com  /dev/vde  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
clienta.lab.example.com  /dev/vdf  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
serverc.lab.example.com  /dev/vde  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
serverc.lab.example.com  /dev/vdf  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
serverc.lab.example.com  /dev/vdb  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
serverc.lab.example.com  /dev/vdc  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
serverc.lab.example.com  /dev/vdd  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
serverd.lab.example.com  /dev/vde  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
serverd.lab.example.com  /dev/vdf  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
serverd.lab.example.com  /dev/vdb  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
serverd.lab.example.com  /dev/vdc  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
serverd.lab.example.com  /dev/vdd  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
servere.lab.example.com  /dev/vde  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
servere.lab.example.com  /dev/vdf  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    Yes                                                                       
servere.lab.example.com  /dev/vdb  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
servere.lab.example.com  /dev/vdc  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
servere.lab.example.com  /dev/vdd  hdd   Unknown    Unknown  0x1af4  N/A            10.7G  Unknown  N/A    N/A    No         Insufficient space (<10 extents) on vgs, LVM detected, locked  
[ceph: root@clienta ~]# 
```

以 root 用户执行 `ceph orch daemon add osd` 命令，在指定主机上使用指定设备创建 osd

```bash
[ceph: root@admin /]# ceph orch \
	daemon add osd osd-1:/dev/vdb 
```

执行 `ceph orch apply osd --all-available-devices` 命令，在所有可用且未使用的设备上部署 osd

```bash
[ceph: root@adm /]# ceph orch \
	apply osd --all-available-devices
```

可以仅使用特定主机上的特定设备创建 `osd`，下例中，在每台主机上由 `default_drive_group` 组中提供的后端设备 `/dev/vdc` 和 `/dev/vdd` 创建两个 `osd`

```yaml
[ceph: root@adm I]# cat lvarlliblcephlosdlosd_spec.yml 
service_type: osd
service_id: default_drive_group
placement:
  hosts:
    - osd-1
    - osd-2
data_devices:
  paths:
    - /dev/vdc
    - /dev/vdd
```

执行 `ceph orch apply` 命令实现 YAML文件中的配置

```bash
[ceph: root@adm /]# ceph orch \
	apply -i /var/lib/ceph/osd/osd_spec.yml
```

### apply 和  daemon 的区别

ceph orch apply 和 ceph orch daemon 是 Ceph 中的两个不同命令，用于不同的目的。

#### ceph orch apply

ceph orch apply 命令用于`将服务规范应用于 Ceph 集群`。这个规范在一个 YAML 文件中定义，包含有关服务的信息，例如副本数、放置规则和其他配置选项。当您运行 ceph orch apply 时，Ceph Orchestrator 会读取 YAML 文件并在集群中创建必要的服务实例。

以下是一个服务规范 YAML 文件的示例：
```yaml
service_type: rgw
service_id: my-rgw
placement:
  count: 1
  hosts:
    - node1
    - node2
```

YAML 文件指定应创建一个 rgw 服务的单个实例，其 ID 为 my-rgw。该服务应放置在两个主机 node1 和 node2 上。

要将此规范应用于集群，您需要运行以下命令：
```bash
ceph orch apply rgw my-rgw -i /path/to/spec.yaml
```
#### ceph orch daemon

ceph orch daemon 命令用于`管理集群中的单个服务守护程序`。您可以使用此命令启动、停止、重启或检查特定守护程序的状态。

例如，要启动 ID 为 my-rgw 的 rgw 守护程序，您需要运行以下命令：
```bash
ceph orch daemon start rgw.my-rgw
```

osd 扩容的一个 Demo

```bash
[root@clienta ~]# ceph orch device  ls --hostname=servere.lab.example.com
Hostname                 Path      Type  Serial  Size   Health   Ident  Fault  Available
servere.lab.example.com  /dev/vdc  hdd   11906   10.7G  Unknown  N/A    N/A    Yes
servere.lab.example.com  /dev/vdd  hdd   27887   10.7G  Unknown  N/A    N/A    Yes
servere.lab.example.com  /dev/vde  hdd   16656   10.7G  Unknown  N/A    N/A    Yes
servere.lab.example.com  /dev/vdf  hdd   6063    10.7G  Unknown  N/A    N/A    Yes
servere.lab.example.com  /dev/vdb  hdd   5089    10.7G  Unknown  N/A    N/A    No
```

```bash
[root@clienta ~]# ceph orch daemon  add osd servere.lab.example.com:/dev/vde
Created osd(s) 3 on host 'servere.lab.example.com'
[root@clienta ~]# ceph orch daemon  add osd servere.lab.example.com:/dev/vdf
Created osd(s) 4 on host 'servere.lab.example.com'
```

```bash
[root@clienta ~]# ceph orch device  ls --hostname=servere.lab.example.com
Hostname                 Path      Type  Serial  Size   Health   Ident  Fault  Available
servere.lab.example.com  /dev/vdc  hdd   11906   10.7G  Unknown  N/A    N/A    Yes
servere.lab.example.com  /dev/vdd  hdd   27887   10.7G  Unknown  N/A    N/A    Yes
servere.lab.example.com  /dev/vdb  hdd   5089    10.7G  Unknown  N/A    N/A    No
servere.lab.example.com  /dev/vde  hdd   16656   10.7G  Unknown  N/A    N/A    No
servere.lab.example.com  /dev/vdf  hdd   6063    10.7G  Unknown  N/A    N/A    No
[root@clienta ~]#
```

## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***

https://docs.ceph.com/en/latest/cephadm/install/

https://docs.ceph.com/en/latest/cephadm/

https://github.com/ceph/cephadm-ansible/blob/devel/README.md

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
