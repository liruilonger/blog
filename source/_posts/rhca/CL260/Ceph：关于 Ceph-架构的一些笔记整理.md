---
title: Ceph：关于 Ceph 存储架构理论方面的一些笔记整理
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-04-09 05:21:57/Ceph：关于Linux Ceph 存储架构的一些笔记整理.html'
mathJax: false
date: 2023-04-09 13:21:57
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***

+ 准备考试，整理ceph 相关笔记
+ 博文内容涉及，Ceph 架构/核心组件和对应的映射介绍
+ 理解不足小伙伴帮忙指正

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

# Ceph 存储架构

Ceph 集群搭建使用标准硬件和存储设备的服务器,是一个高度可扩展的分布式存储系统, 采用`模块化分布式架构`。Ceph 主要通过 `RADOS` 核心组件来提供能力。

RADOS 是 Ceph 的底层对象存储服务，由 OSD 守护进程组成，而 Ceph 集群中的其他组件如 MON、MDS 和 RGW 等也都是守护进程，各自扮演着不同的角色和功能。OSD 进程是 Ceph 存储集群中的核心组件之一，负责将数据分散存储在多个节点和磁盘上，并提供高可用性、容错性和可靠性等特性。

Ceph 中的核心组件并不一定是守护进程，`RADOS` 是核心组件，而不是守护进程本身。

**RADOS(Reliable Autonomic Distributed Object Store)** :可靠的自主分布式对象存储,RADOS 是一种 `自我修复、自我管理` 的软件型对象存储,  `Ceph` 也同时支持块存储，和文件存储，这里实际上是在对象存储的基础上封装了一层，提供块存储和文件存储的能力。

## Ceph 存储核心组件

Ceph集群的核心组件主要包括:

1. **MON (Monitor Daemon)** : `集群监控`, 守护进程,MON 负责监控 Ceph 集群的状态、配置和拓扑结构，并将这些信息保存在 Ceph Monitor 数据库(Store）中。Ceph 集群至少需要三个 MON 进程来保证高可用性。在集群中可以配置不同的选举方式。
2. **OSD (Object Storage Daemon)**: `对象存储设备`,守护进程,存储数据并处理`数据复制、恢复和重新平衡`,
3. **MGR (Managers,ceph-mgr)**: `管理器(非必须)`,守护进程,通过基于浏览器的仪表板和 REST API，跟踪运行时指标并公开集群信息
4. **MDS (Metadata Servers)**: `元数据服务器`,守护进程,存储供 CephFS 使用的元数据(而非对象存储或块存储)，让客户端能够高效执行 POSIX 命令。CephFS是基于 `RADOS` 实现的分布式文件系统，可提供与传统文件系统类似的功能。
5. **RGW(RADOS Gateway)** :`RADOS 网关`,守护进程,RGW 是一个 REST 接口，使得客户端可以通过 HTTP 或 S3 协议来访问 Ceph 存储集群中的对象。RGW 可以将 Ceph 集群中的对象作为 Web 资源来公开，从而实现与 Amazon S3 兼容的对象存储服务。
6. **RBD（RADOS Block Device）**：Ceph 块设备，将 Ceph 存储集群的对象存储能力暴露为块设备，支持虚拟机和容器等应用的块存储需求。

部分组件以守护进程的方式运行，可以以副本形式扩展，以满足部署的存储集群的要求。

### Ceph 监控器 MON

Ceph 监控器 (MON) 是`维护集群映射主要副本的守护进程`,是用于监控和管理集群状态、拓扑结构和配置信息的组件之一。MON 组件通常由多个进程组成，以提高可用性和容错性。

`集群映射（Cluster Map）`是指将 Ceph 集群中的各个组件（如 OSD、MON、MDS、RGW 等）`映射`到实际的`网络地址和端口号`的过程。Ceph 集群映射由五种不同的映射组成的集合，包括:`OSD 映射集合`/`MON 映射集合`/`MDS 映射集合`/`RGW 映射集合`/`CRUSH 映射集合`/

这五种映射集合共同构成了 `Ceph` 集群映射，用于实现各个组件之间的通信和协作，并支持数据的高可用性、可靠性和性能。

通过集群映射，`客户端和其他组件可以找到并连接正确的 OSD、MON、MDS、RGW 进程，并使用 CRUSH 算法来计算数据位置和副本策略`。

我理解，所谓集群映射，就是一个配置中心，用于维护映射K和具体配置。

`Ceph 生产至少需要三台机器`，根据官方的 Ceph 文档，建议在 Ceph 集群中`至少`使用三台机器以确保数据冗余和可用性。但是，可以使用少于三台机器运行 Ceph 集群，但`不建议`在生产环境中这样做。

在后端组件的基础上多了 `CRUSH映射`， Ceph 必须处理每个`集群事件`，更新合适的映射，并将更新后的映射复制到每个监控器守护进程，若要`应用更新`，MON 必须就集群状态 `建立共识`。

这要求配置的监控器中有多数可用且就`映射更新达成共识`，为 `Ceph 集群配置奇数个监控器`，以确保监控器能在就`集群状态投票时建立仲裁`，配置的监控器中必须有 `超过半数正常发挥作用` ，Ceph 存储集群才能`运行并可访问`，这是保护集群数据的完整性所`必需`的。

在 `Ceph` 集群运行期间，MON 进程需要相互通信并就下列事项达成`共识`：

+ `集群状态`：每个 MON 进程都要了解 Ceph 集群的当前状态，如 OSD 的在线状态、PG 的映射关系、数据副本的位置等等。
+ `健康状况`：每个 MON 进程需要收集和汇总所有 OSD 和 PG 的健康报告，并根据报告指示的问题推断出整个集群的健康状况。
+ `拓扑结构`：每个 MON 进程需要知道 Ceph 集群的拓扑结构，并为客户端请求和数据复制提供正确的路由和路径。
+ `配置信息`：每个 MON 进程需要了解集群的配置信息，如 MON 和 OSD 的数量、PG 的数量和策略、CRUSH 映射规则等等。

因此，MON 必须就集群状态建立共识，才能保证整个 `Ceph` 集群的正确运行和数据的可靠存储。如果 `MON 无法建立共识`，那么可能会导致数据丢失、访问失败、性能下降等问题。

### Ceph 对象存储设备 OSD

`Ceph 对象存储设备 (OSD)` 是 Ceph 存储集群的`构建块`，`OSD 将存储设备(如硬盘或其他块设备)连接到 Ceph 存储集群`。

一台存储服务器可以`运行多个 OSD 守护进程`，并为集群提供多个 `OSD`，Ceph 旧版本要求 `OSD` 存储设备具有底层文件系统，但 `BlueStore` 以原始模式使用本地存储设备，不在需要文件系统，这有助于提升性能

`Ceph` 客户端和 OSD 守护进程都使用 `可扩展哈希下的受控复制 (CRUSH, Controlled Replication Under Scalable Hashing)` 算法来高效地计算对象位置的信息，而不依赖中央服务器查找

在高层次上，`CRUSH` 算法通过使用`分层树结构`将数据对象映射到存储设备上。树是基于存储设备的物理拓扑结构构建的，树中的每个节点表示一组设备(`放置组PG`)。然后，算法使用确定性函数将每个数据对象映射到树中的叶节点，该叶节点对应于特定的存储设备。

CRUSH 算法的一个关键优点是它被设计为`高度容错`。由于算法是确定性的，即使某些设备失败，也可以重构数据对象到存储设备的映射。这使得即使在硬件故障的情况下，也可以保持数据可用性

CRUSH (keruasi) 将每个对象分配给单个`哈希存储桶`，称为`放置组 (PG)`，也就是上面我们讲的树的节点。

`PG`  是对象(应用层)和 OSD(物理层)之间的抽象层，CRUSH 使用伪随机放置算法在 PG 之间分布对象，并且使用规则来确定 PG 到 OSD 的映射。

出现`故障`时，Ceph 将 PG 重新映射到不同的物理设备 (OSD) ，并`同步`其内容以匹配配置的数据保护规则，一个 OSD 是对象放置组的`主要 OSD`，Ceph 客户端在读取或写入数据时始终联系操作集合中的`主要 OSD`，其他 OSD 为`次要 OSD`，在确保集群故障时的数据弹性方面发挥重要作用

**Primary OSD 的功能：**

+ 服务所有 I/O 请求
+ 复制和保护数据
+ 检查数据的一致性
+ 重新平衡数据
+ 恢复数据

**次要 OSD 的功能(备份)：**

+ 行动始终受到 Primary OSD 的控制
+ 能够变为 Primary OSD

每个 OSD 具有自己的 OSD ⽇志。OSD 日志提供了对 OSD 实施写操作的性能。

来自 `Ceph 客户端` 的写操作本质上通常是`随机的 I/O`，由 `OSD 守护进程`顺序写入到日志中。当涉及的所有 OSD 日志`记录`了写请求后，Ceph 将每个写操作`确认`到客户端，OSD 然后将`操作提交`到其`后备存储`。

每隔几秒钟，`OSD` 会停止向日志写入新的请求，以将 OSD日志的内容应用到`后备存储`，然后，它会修剪日志中的已提交请求，回收日志存储设备上的空间

当 `Ceph OSD` 或其存储服务器出现故障时，Ceph 会在 `OSD 重新启动后重演其日志`，重演序列在`最后一个已同步`的操作后开始，因为 Ceph 已将同步的日志记录提交到 OSD 的存储，OSD日志使用OSD 节点上的原始卷，若有可能，应在单独的SSD等快速设备上配置日志存储

要查看 Ceph 集群的 OSD（Object Storage Daemon）服务信息，可以使用 Ceph 自带的命令行工具 ceph 命令。以下是一些常用的命令：


+ `ceph osd tree`：显示 OSD 树，即显示每个 OSD 的编号、主机名、状态、容量等信息，以及 CRUSH 算法对存储设备的映射关系。
+ `ceph osd df`：显示 OSD 的使用情况，包括总容量、已用空间、可用空间等信息。
+ `ceph osd pool ls`：列出所有的 OSD 数据存储池，包括数据存储池名称、ID 等信息。
+ `ceph osd pool stats`：显示 OSD 数据存储池的使用情况，包括存储池大小、已用空间、剩余空间等。
+ `ceph tell osd.* bench`：对 OSD 进行性能测试，以便评估 OSD 的性能和稳定性。
+ `ceph tell osd.* query`：查询所有 OSD 进程的状态和性能数据，包括 IOPS、延迟等指标。


### RADOS 网关 RGW


RADOS Gateway（ RGW） 提供了 S3 和 Swift 接口兼容的对象存储服务，可以将 Ceph 存储集群暴露为云存储服务。RADOS Gateway 的作用如下：

+ 对象存储：RADOS Gateway 提供了基于 S3 和 Swift 接口兼容的对象存储服务，支持大规模、高并发的数据访问和管理。用户可以使用标准的 S3 或 Swift 客户端库进行对象存储。
+ 数据访问控制：RADOS Gateway 支持多种认证和授权方式，可以对不同的用户或应用程序进行权限控制，保障数据的安全性和隐私性。
+ 数据可靠性：RADOS Gateway 使用 RADOS 集群作为底层存储设备，具有高可靠性和高可用性，可以保障数据的持久性和可靠性。
+ 多租户支持：RADOS Gateway 支持多租户功能，可以将一个 RGW 集群划分为多个租户，每个租户拥有自己的存储空间和访问权限，可以避免资源的冲突和干扰。
+ 弹性伸缩：RADOS Gateway 可以根据需求进行水平扩展和缩减，支持动态调整存储容量和性能，具有高度的弹性和灵活性。

要查看 Ceph 集群的 RGW（RADOS Gateway）服务信息，可以使用 Ceph 自带的命令行工具 radosgw-admin 命令。以下是一些常用的命令：

+ `radosgw-admin user list`：列出所有的 RGW 用户，包括用户 ID、显示名称等信息。
+ `radosgw-admin bucket list`：列出所有的 RGW 存储桶，包括存储桶名称、拥有者等信息。
+ `radosgw-admin quota get --uid=<user-id>`：显示指定用户的配额信息，包括存储空间配额、对象数配额等。
+ `radosgw-admin policy list`：列出所有的 RGW 策略，包括策略名称、权限等信息。
+ `radosgw-admin usage show`：显示集群的 RGW 使用情况，包括总请求数、总流量等信息。
+ `radosgw-admin metadata list`：列出集群中所有的 RGW 元数据，包括实体 ID、实体类型、元数据键值对等信息。

### Ceph 管理器 MGR

`Ceph 管理器 (MGR)` 提供一系列集群统计数据，集群中不可用的管理器不会给 `客户端 I/O`操作带来负面影响。在这种情况下，尝试查询集群统计数据会失败，可以在`不同的故障域中部署至少两个 Ceph 管理器提升可用性`

管理器守护进程将集群中收集的所有数据的访问集中到一处，并通过 TCP 端⼝ `7000`(默认)向存储管理员提供一个简单的 Web 仪表板。

它还可以将状态信息导出到外部 `Zabbix` 服务器，将性能信息导出到 `Prometheus`。Ceph 指标是一种基于 `collectd` 和 `grafana` 的监控解决方案，可补充默认的仪表板

查看 Ceph 集群的 MGR（Manager）服务信息，可以使用 Ceph 自带的命令行工具 ceph 命令。以下是一些常用的命令：



+ `ceph mgr module ls`：列出所有加载的 MGR 模块，包括模块名称、状态等信息。
+ `ceph mgr module enable <module-name>`：启用指定的 MGR 模块，以便进行管理和监控。
+ `ceph mgr services`：显示所有的 MGR 服务，包括进程 ID、名称、状态等信息。
+ `ceph daemon <mgr-id> perf dump`：显示指定 MGR 进程的性能监控信息，包括 CPU 使用率、内存使用情况等。


### 元数据服务器 MDS

Ceph 元数据服务器(MDS)管理 `Ceph 文件系统(CephFS)元数据`。

它提供兼容 POSIX 的共享文件系统元数据管理，包括所有权、时间戳和模式。MDS 使用 RADOS 而非本地存储来存储其元数据，它无法访问文件内容

`MDS 可让 CephFS 与 Ceph 对象存储交互`，将索引节点映射到对象，并在树内映射 Ceph 存储数据的位置，访问 CephFS 文件系统的客户端首先向 `MDS` 发出请求，这会提供必要的信息以便从正确的 OSD 获取文件内容

查看 Ceph 集群的 MDS（Metadata Server）服务信息

+ `ceph mds dump`：显示所有的 MDS 进程的信息，包括 MDS 的 ID、名称、状态等。
+ `ceph fs ls`：列出所有的 CephFS 文件系统，包括文件系统 ID、名称等信息。
+ `ceph fs status`：显示 CephFS 文件系统的状态和健康状况，包括 MDS 的运行状态、文件系统的元数据池使用情况等信息。
+ `ceph fs subvolumegroup ls`：列出指定 CephFS 文件系统中的子卷组，包括子卷组 ID、名称、使用情况等信息。

### Ceph 块设备 RBD


RBD 全称为 `RADOS Block Device`，它将 Ceph 存储集群的对象存储能力暴露为块设备，支持虚拟机和容器等应用的块存储需求。RBD 的作用如下：

+ 块存储：RBD 提供了块存储服务，支持虚拟机和容器等应用使用块设备进行数据存储和访问。
+ 快照和克隆：RBD 支持块设备的快照和克隆功能，可以对块设备进行备份和恢复，以及基于现有块设备创建新的块设备。
+ 多租户支持：RBD 支持多租户功能，可以将一个 RBD 集群划分为多个租户，每个租户拥有自己的块设备空间和访问权限，可以避免资源的冲突和干扰。
+ 弹性伸缩：RBD 可以根据需求进行水平扩展和缩减，支持动态调整存储容量和性能，具有高度的弹性和灵活性。

要查看 Ceph 中 RBD 的服务信息，可以使用以下命令：

+ `rbd ls`：显示所有的 RBD 块设备，包括块设备名称、大小等信息。
+ `rbd info <image>`：显示指定 RBD 块设备的信息，包括块设备 ID、大小、快照数等信息。
+ `rbd snap ls <image>`：列出指定 RBD 块设备的所有快照，包括快照名称、创建时间等信息。
+ `rbd du`：显示 RBD 块设备的使用情况，包括总使用容量、总可用容量等信息。
+ `rbd map <image>`：将指定的 RBD 块设备映射为本地块设备，以便进行数据读写操作。




### 集群映射

Ceph 客户端和对象存储守护进程 `OSD` 需要确认`集群拓扑`。五个映射表示`集群拓扑`，统称为`集群映射`。

Ceph 监控器(Mon)守护进程维护集群映射的主副本。Ceph MON 集群在监控器守护进程出现故障时确保高可用性

#### 监控器映射 

包含集群 fsid、各个监控器的位置、名称、地址和端口，以及映射时间戳。

使用 [ceph mon dump]() 来查看监控器映射。fsid 是一种自动生成的唯⼀标识符 (UUID)，用于标识 Ceph 集群

```bash
[ceph: root@serverc /]# ceph mon dump
epoch 5
fsid 4c759c0c-d869-11ed-bfcb-52540000fa0c
last_changed 2023-04-11T13:34:50.058049+0000
created 2023-04-11T13:03:51.482001+0000
min_mon_release 16 (pacific)
election_strategy: 1
0: [v2:172.25.250.12:3300/0,v1:172.25.250.12:6789/0] mon.serverc.lab.example.com
1: [v2:172.25.250.13:3300/0,v1:172.25.250.13:6789/0] mon.serverd
2: [v2:172.25.250.14:3300/0,v1:172.25.250.14:6789/0] mon.servere
dumped monmap epoch 5
[ceph: root@serverc /]#
```

+ epoch：监视器映射的时代编号。
+ fsid：Ceph集群的唯一标识符。
+ last_changed：修改监视器映射的时间。
+ created：创建监视器映射的时间。
+ min_mon_release：与监视器映射兼容的最小Ceph版本。
+ election_strategy：监视器用于选举领导者的策略。Ceph监视器的选举策略有两种：
  + classic：这是默认的选举策略，在此策略下，监视器具有相同的投票权，并根据周期性心跳和消息传递进行通信。如果监视器检测到其他监视器离线，则它们将在一段时间后进入新的领导者选举过程。
  + quorum：该策略要求至少三个监视器在线，并从中选择领导者，而不是所有在线监视器都拥有相同的投票权。采用此策略时，如果没有足够的监视器参与选举，则集群将无法正常工作或处于只读状态，直到更多的监视器加入并恢复其大多数选举能力。  
+ 0、1、2：集群中每个监视器的排名和IP地址，以及它们的主机名。
+ dumped：表示已成功转储指定时代的监视器映射。

#### OSD 映射 

包含集群 fsid、池列表、副本大小、放置组编号、OSD 及其状态的列表，以及映射时间戳。使用 <a>ceph osd dump</a> 查看 OSD 映射

```bash
[ceph: root@serverc /]# ceph osd dump
epoch 77
fsid 4c759c0c-d869-11ed-bfcb-52540000fa0c
created 2023-04-11T13:03:54.016381+0000
modified 2023-04-25T07:46:25.481881+0000
flags sortbitwise,recovery_deletes,purged_snapdirs,pglog_hardlimit
crush_version 24
full_ratio 0.95
backfillfull_ratio 0.9
nearfull_ratio 0.85
require_min_compat_client luminous
min_compat_client jewel
require_osd_release pacific
stretch_mode_enabled false
pool 1 'device_health_metrics' replicated size 3 min_size 2 crush_rule 0 object_hash rjenkins pg_num 1 pgp_num 1 autoscale_mode on last_change 74 flags hashpspool stripe_width 0 pg_num_min 1 application mgr_devicehealth
max_osd 9
osd.0 up   in  weight 1 up_from 71 up_thru 0 down_at 70 last_clean_interval [50,65) [v2:172.25.250.12:6800/2046452011,v1:172.25.250.12:6801/2046452011] [v2:172.25.250.12:6802/2046452011,v1:172.25.250.12:6803/2046452011] exists,up 82c98821-3c02-468a-8e2f-7c5dfc2a0fbe
osd.1 up   in  weight 1 up_from 68 up_thru 23 down_at 67 last_clean_interval [50,65) [v2:172.25.250.12:6808/260025682,v1:172.25.250.12:6809/260025682] [v2:172.25.250.12:6810/260025682,v1:172.25.250.12:6811/260025682] exists,up 1ca3352b-4c2d-4a85-86cc-33d1715c1915
.......
blocklist 172.25.250.12:0/1295757381 expires 2023-04-26T07:42:28.540520+0000
blocklist 172.25.250.12:6825/3489966617 expires 2023-04-26T07:42:28.540520+0000
blocklist 172.25.250.12:6824/3489966617 expires 2023-04-26T07:42:28.540520+0000
```

+ `epoch`：OSD映射的时代编号。
+ `fsid`：Ceph集群的唯一标识符。
+ `created`：创建OSD映射的时间。
+ `modified`：修改OSD映射的时间。
+ `flags`：包含有关 `OSD` 映射状态的标志位，这些标志位都是用来控制 `Ceph` 集群中 `OSD`的行为和状态，以便最大程度地提高性能和可靠性。如 `sortbitwise`、`recovery_deletes`、`purged_snapdirs` 和 `pglog_hardlimit` 等。
  + `sortbitwise`：开启了每个对象单独排序，这可以优化存储分配和数据分布。
  + `recovery_deletes`：在恢复过程中删除陈旧的对象，以避免占用空间和影响性能。
  + `purged_snapdirs`：快照目录已从PG日志中删除，以节省空间并提高性能。
  + `pglog_hardlimit`：PG日志达到硬限制时强制转储日志，以保持一致性并避免性能问题。
+ `crush_version`：`CRUSH`映射版本号。
  + `full_ratio`：当 OSD 的使用量达到该值时，将触发警告（如果开启了警告）并阻止新数据写入 OSD。默认值为 0.95。
+ `full_ratio、backfillfull_ratio 和 nearfull_ratio`：用于控制OSD空间利用率的比率。
  + `backfillfull_ratio`：当 OSD 在后期重新填充时，其使用量达到该值时，将阻止进一步的后期重新填充操作。默认值为 0.9。
  + `nearfull_ratio`：当 OSD 的使用量达到该值时，将向 Ceph 发送慢操作警告。默认值为 0.85。
+ `require_min_compat_client、min_compat_client 和 require_osd_release`：最低客户端和OSD版本限制。
+ `stretch_mode_enabled`：是否启用了伸缩模式。该模式允许将数据复制到远程位置，以`实现跨区域灾难恢复和数据备份`。
+ `pool`：Ceph 存储池的相关信息，例如 size、min_size、pg_num、pgp_num 等等。
  + `pool 1`：这是存储池的编号，Ceph 存储集群中的每个存储池都有一个唯一的编号。
  + `device_health_metrics`：这是存储池的名称，通常用于描述存储池中存储的对象类型或数据用途。
  + `replicated`：这是存储池的副本模式，表示该存储池中的数据将被复制到多个 OSD 上以实现高可用性和冗余。
  + `size 3 和 min_size 2`：这些是存储池的副本大小设置。size 3 表示每个对象将被复制到 3 个不同的 OSD 上，而 min_size 2 表示在执行写入操作时至少要在 2 个 OSD 上成功复制数据才会视为写入成功。
  + `crush_rule 0`：这是使用的 CRUSH 规则的编号，CRUSH 是 Ceph 集群用于计算数据位置的分布式算法，该规则规定了如何将数据分散到存储设备上。
  + `object_hash rjenkins`：这是用于确定对象位置的哈希函数。在这种情况下，rjenkins 是一种快速哈希函数，能够快速生成对象位置。
  + `pg_num 1 和 pgp_num 1`：这些是该存储池中的 PG 数量和 PGP 数量。PG 是 Ceph 存储池中数据的基本组织单元，它们将对象分配到 OSD 上以实现负载均衡。
  + `autoscale_mode on`：这是存储池的自动扩展设置，表示在存储池满时是否自动扩展存储池大小。此处设置为“开启”。
  + `last_change 74`：这是最后修改存储池配置的时间戳，通常用于跟踪存储池配置更改历史记录。
  + `flags hashpspool stripe_width 0 pg_num_min 1`：这些是其他存储池标志，如使用哈希池（hashpspool）、条带宽度（stripe_width）和最小 PG 数量（pg_num_min）等。
  + `application mgr_devicehealth`：这是存储池的应用程序类型，表示该存储池用于存储设备健康度信息的管理数据。
+ `max_osd`：OSD的最大数量。
+ `osd.X`：每个OSD的详细信息，包括其状态、权重、IP地址、端口、在PG中的位置等等。
+ `blocklist`：列出被阻止或禁止使用的IP地址列表。

#### 放置组 (PG) 映射 

包含 PG 版本、全满比率、每个放置组的详细信息，例如 PG ID、就绪集合、操作集合、PG 状态、每个池的数据使用量统计、以及映射时间戳。使用[ceph pg dump]()查看包含的 PG 映射统计数据

```bash
PG_STAT  OBJECTS  MISSING_ON_PRIMARY  DEGRADED  MISPLACED  UNFOUND  BYTES  OMAP_BYTES*  OMAP_KEYS*  LOG  DISK_LOG  STATE         STATE_STAMP                      VERSION  REPORTED  UP       UP_PRIMARY  ACTING   ACTING_PRIMARY  LAST_SCRUB  SCRUB_STAMP                      LAST_DEEP_SCRUB  DEEP_SCRUB_STAMP                 SNAPTRIMQ_LEN
4.19           0                   0         0          0        0      0            0           0    0         0  active+clean  2023-04-30T05:50:02.431578+0000      0'0    108:12  [7,2,5]           7  [7,2,5]               7         0'0  2023-04-30T05:50:01.330091+0000              0'0  2023-04-30T05:50:01.330091+0000              0
2.1f           0                   0         0          0        0      0            0           0    0         0  active+clean  2023-04-30T05:49:47.320452+0000      0'0    108:17  [0,3,8]           0  [0,3,8]               0         0'0  2023-04-30T05:49:46.202749+0000              0'0  2023-04-30T05:49:46.202749+0000              0
3.1e           0                   0         0          0        0      0            0           0    0         0  active+clean  2023-04-30T05:49:49.349973+0000      0'0    108:15  [2,6,3]           2  [2,6,3]               2         0'0  2023-04-30T05:49:48.270032+0000              0'0  2023-04-30T05:49:48.270032+0000              0
4.18           0                   0         0          0        0      0            0           0    0         0  active+clean  2023-04-30T05:50:02.449439+0000      0'0    108:12  [3,1,6]           3  [3,1,6]               3         0'0  2023-04-30T05:50:01.330091+0000              0'0  2023-04-30T05:50:01.330091+0000              0
```

+ PG_STAT: PG 的状态，表示 PG 在当前时间点内的活动情况和健康状况。 PG_STAT 数字由两部分组成，第一部分是十六进制的 OSD 编号，第二部分是十六进制的 epoch 号。
+ OBJECTS: PG 中对象数量。
+ MISSING_ON_PRIMARY: 在 primary OSD 上缺少的对象数量。
+ DEGRADED: 在副本 OSD 上损坏或不可用的对象数量。
+ MISPLACED: 在非预期 OSD 上的对象数量。
+ UNFOUND: 未找到的对象数量。
+ BYTES: PG 中对象的总字节数。
+ OMAP_BYTES: PG 中对象元数据的总字节数。
+ OMAP_KEYS: PG 中对象元数据键值对的总数。
+ LOG: 最近操作日志的序列号范围。
+ DISK_LOG: 存储在磁盘上的日志序列号范围。
+ STATE: PG 的状态，例如“active+clean”。
+ STATE_STAMP: PG 最后转换到当前状态的时间戳。
+ VERSION: PG 的版本。
+ REPORTED: 汇报 PG 状态的 OSD 的编号。
+ UP: 处于活动状态的 OSD 编号列表。
+ UP_PRIMARY: 作为主 OSD 进行同步的 OSD 编号。
+ ACTING: 负责读写请求的 OSD 编号列表。
+ ACTING_PRIMARY: 正在执行同步操作的 OSD 编号。
+ LAST_SCRUB: 上次 scrub 的时间戳和结果。
+ SCRUB_STAMP: 上次 scrub 的结束时间戳。
+ LAST_DEEP_SCRUB: 上次 deep scrub 的时间戳和结果。
+ SNAPTRIMQ_LEN: PgSnapTrimq 中等待处理的数量。
+ DEEP_SCRUB_STAMP: 上次 deep scrub 的结束时间戳。


#### CRUSH 映射 


包含存储设备的列表、故障域层次结构(例如设备、主机、机架、行、机房)，以及存储数据时层次结构的规则，Crush Map 是 Ceph 存储集群的关键组件之一，用于定义数据如何分布到存储设备（如 OSD）上。 Crush Map 包含 bucket、设备和 ruleset 三个主要组件，结合使用可以定义数据的分布和副本策略。


若要查看CRUSH 映射，
+ 首先使用 `ceph osd getcrushmap -o comp-filename`，
+ 使用 `crushtool -d comp-filename -o decomp-filename` 解译该输出
+ 使用文本编辑器查看解译后的映射

```bash
[root@serverc ~]# cephadm shell
Inferring fsid 4c759c0c-d869-11ed-bfcb-52540000fa0c
Inferring config /var/lib/ceph/4c759c0c-d869-11ed-bfcb-52540000fa0c/mon.serverc.lab.example.com/config
Using recent ceph image registry.redhat.io/rhceph/rhceph-5-rhel8@sha256:6306de945a6c940439ab584aba9b622f2aa6222947d3d4cde75a4b82649a47ff
[ceph: root@serverc /]# ceph osd getcrushmap -o comp-filename
27
[ceph: root@serverc /]# crushtool -d comp-filename -o decomp-filename
[ceph: root@serverc /]# cat de
decomp-filename  dev/
[ceph: root@serverc /]# cat decomp-filename
# begin crush map
tunable choose_local_tries 0
tunable choose_local_fallback_tries 0
tunable choose_total_tries 50
tunable chooseleaf_descend_once 1
tunable chooseleaf_vary_r 1
tunable chooseleaf_stable 1
tunable straw_calc_version 1
tunable allowed_bucket_algs 54

# devices
device 0 osd.0 class hdd
device 1 osd.1 class hdd
device 2 osd.2 class hdd
device 3 osd.3 class hdd
device 4 osd.4 class hdd
device 5 osd.5 class hdd
device 6 osd.6 class hdd
device 7 osd.7 class hdd
device 8 osd.8 class hdd

# types
type 0 osd
type 1 host
type 2 chassis
type 3 rack
type 4 row
type 5 pdu
type 6 pod
type 7 room
type 8 datacenter
type 9 zone
type 10 region
type 11 root

# buckets
host serverc {
        id -3           # do not change unnecessarily
        id -4 class hdd         # do not change unnecessarily
        # weight 0.029
        alg straw2
        hash 0  # rjenkins1
        item osd.0 weight 0.010
        item osd.1 weight 0.010
        item osd.2 weight 0.010
}
host serverd {
        id -5           # do not change unnecessarily
        id -6 class hdd         # do not change unnecessarily
        # weight 0.029
        alg straw2
        hash 0  # rjenkins1
        item osd.3 weight 0.010
        item osd.4 weight 0.010
        item osd.5 weight 0.010
}
host servere {
        id -7           # do not change unnecessarily
        id -8 class hdd         # do not change unnecessarily
        # weight 0.029
        alg straw2
        hash 0  # rjenkins1
        item osd.6 weight 0.010
        item osd.7 weight 0.010
        item osd.8 weight 0.010
}
root default {
        id -1           # do not change unnecessarily
        id -2 class hdd         # do not change unnecessarily
        # weight 0.088
        alg straw2
        hash 0  # rjenkins1
        item serverc weight 0.029
        item serverd weight 0.029
        item servere weight 0.029
}

# rules
rule replicated_rule {
        id 0
        type replicated
        min_size 1
        max_size 10
        step take default
        step chooseleaf firstn 0 type host
        step emit
}

# end crush map
[ceph: root@serverc /]#
```


#### 元数据服务器 (MDS) 映射 

包含用于存储元数据的池、元数据服务器列表、元数据服务器状态和映射时间戳。查看包含 [ceph fs dump]() 的 MDS映射


```bash
[ceph: root@serverc /]# ceph fs dump
e1
enable_multiple, ever_enabled_multiple: 1,1
compat: compat={},rocompat={},incompat={1=base v0.20,2=client writeable ranges,3=default file layouts on dirs,4=dir inode in separate object,5=mds uses versioned encoding,6=dirfrag is stored in omap,8=no anchor table,9=file layout v2,10=snaprealm v2}
legacy client fscid: -1

No filesystems configured
dumped fsmap epoch 1
[ceph: root@serverc /]#
```
运行 ceph fs dump 命令后的输出结果。以下是每个字段的含义：

+ e1: 当前文件系统的 epoch 号。
+ enable_multiple, ever_enabled_multiple: 1,1: 文件系统是否启用了多 MDS 支持并且曾经启用过。
+ compat: 文件系统支持的兼容性特性，包括三个字段（compat、rocompat 和 incompat），其中 compat 表示 CephFS 要求支持的最低版本，而 rocompat 和 incompat 分别表示与只读客户端和不兼容客户端相关的特性。
+ legacy client fscid: -1: 指定将 legacy 客户端分配给文件系统的 ID，但当前没有 legacy 客户端连接到该文件系统。
+ No filesystems configured: 表示当前没有配置任何文件系统。
+ dumped fsmap epoch 1: 表示已成功输出当前 Ceph 集群的文件系统映射。


## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)

***

<https://docs.ceph.com/en/pacific/architecture/>

<https://docs.ceph.com>

CL210 授课老师课堂笔记

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)

