---
title: Ceph：关于Ceph用户认证授权管理的一些笔记
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-05-17 09:09:08/Ceph：关于Ceph用户认证授权管理的一些笔记.html'
mathJax: false
date: 2023-05-17 17:09:08
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***

+ 准备考试，整理 Ceph 相关笔记
+ 博文内容涉及, Ceph 用户管理，认证管理，权限管理 以及相关 Demo
+ 理解不足小伙伴帮忙指正



**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其 它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


# 管理Ceph用户认证授权

## 简单介绍

### 用户身份验证

`Ceph` 使用 `cephx` 协议对集群中`客户端、应用程序和守护进程`之间的通信进行授权。`cephx协议`基于`共享密钥`

安装过程默认启用`cephx`，因此集群需要所有客户端应用程序进行`用户身份验证`和`授权`，Ceph使用用户帐户有以下几个目的：

1. 用于 Ceph 守护进程之间的内部通信
2. 对于通过`librados`库访问集群的客户机应用程序
3. 为集群管理员


Ceph守护进程使用的`帐户名称`与其关联的守护进程osd.1或mgr.serverc相匹配，并且其在安装过程中创建

使用`librados`的`客户端`应用程序所使用的帐户具有`client.`名称前缀.

例如，在集成`OpenStack`和`Ceph`时，通常会创建一个专用的`client.openstack`用户帐户。

对于`Ceph`对象网关，安装会创建一个专用的`client.rgw.hostname`用户帐号，在librados之上创建定制软件的开发人员应该创建具有适当功能的专用帐户

管理员帐户名也具有`client.`前缀。

在运行`ceph、rados`等命令时使用，安装程序创建超级用户帐户`client.admin`，具有允许帐户访问所有内容和修改集群配置的功能。

`Ceph`使用 `client.admin` 帐户用于运行管理命令，除非使用 `--name` 或 `--id` 选项明确指定用户名

可以设置`CEPH_ARGS`环境变量来定义诸如`集群名称`或`用户ID`等参数

```bash
[ceph: root@node /]# export CEPH_ARGS="--id cephuser" 
```

`Ceph-aware` 应用程序的最终用户没有Ceph集群上的帐户。相反，他们访问应用程序，然后应用程序代表他们访问Ceph。从Ceph的角度来看，应用程序就是客户端。应用程序可以通过其他机制提供自己的用户身份验证

下图概述了应用程序如何提供自己的用户身份验证

![在这里插入图片描述](https://img-blog.csdnimg.cn/027be05775bf4c99b7cd1922ac1cda2a.png)


Ceph对象网关有自己的`用户数据库`来认证`Amazon S3和Swift`用户，但使用`client.rgw.hosttname` 用于访问集群的帐号


### 配置用户授权

创建新用户帐户时，授予集群权限，以授权用户的集群任务，cephx中的权限被称为`能力`，可以通过守护进程类型(mon、osd、mgr或mds)授予它们。

使用能力来根据应用程序标记限制或提供对池、池的名称空间或一组池中的数据的访问。能力还允许集群中的守护进程相互交互


# 用户管理

需要查询现有用户，使用 `ceph auth list`命令

```bash
[ceph: root@node /]# ceph auth list 
... output omitted ... 
osd.0 
key: AQBW6Tha5z6OIhAAMQ7nY/4MogYecxKqQxX1sA== 
caps : [mgr] allow profile osd
caps: [mon] allow profile osd 
caps: [osd] allow * 
client.admin 
key: AQCi6Dhajw7pIRAA/ECkwyipx2/raLWjgbklyA== 
caps: [mds] allow * 
caps: [mgr] allow * 
caps: [mon] allow * 
caps: [osd] allow * 
. . . output omitted ... 
```

要获取特定帐户的详细信息，使用`ceph auth get`命令:

```bash
[ceph: root@node /]# ceph auth get client.admin 
exported keyring for client.admin 
[client . ad min] 
key = AQCi6Dhajw7pIRAA/ECkwyipx2/raLWj gbklyA== 
caps mds = "allow *" 
caps mgr = "allow *" 
caps mon = "allow *" 
caps osd = "allow *" 
```

可以打印密钥：

```bash
[ceph: root@node /]# ceph auth print-key client.adrnin 
AQCi6Dhajw7pIRAA/ECkwyipx2/raLWjgbklyA== 
```

需要导出和导入用户帐号，使用`ceph auth export`和`ceph auth import`命令

```bash
[ceph: root@node /]# ceph auth \
	export client.operator1 > ~/operatorl.export 
[ceph: root@node /]# ceph auth \
	import -i ~/operator1.export 
```

## 创建新用户帐户

`ceph auth get-or-create` 命令创建一个新用户帐户并生成它的`密钥`，该命令默认将该`密钥`打印到stdout，因此通常会添加`-o`选项来将标准输出保存到`密钥环文件`中。

本例创建了对所有池具有读写权限的 `app1` 用户帐户，并将密钥环文件存储在 `/etc/ceph/ceph.client.app1.keyring`

```bash
[ceph: root@node /]# ceph auth \
	get-or-create client.app1 \
	mon 'allow r' \
	osd 'allow rw' \
	-o /etc/ceph/ceph.client.app1.keyring
```

身份验证需要`密匙环文件`，因此必须将该文件复制到使用此新用户帐户操作的所有客户端系统

## 用户认证

### Keyring 文件

对于身份验证，客户端配置一个 `Ceph` 用户名和一个包含用户`安全密钥`的密钥环文件，Ceph在创建每个用户帐户时为其生成`密匙环文件`，但是，必须将此文件复制到需要它的每个客户机系统或应用程序服务器

在这些客户机系统上，librados 使用来自`/etc/ceph/ceph.conf` 的密匙环参数。`Conf配置文件以定位密钥环文件`。默认值为`/etc/ceph/$cluster.$name.keyring`密匙环。

例如，对于`client.openstack`帐户，密钥环文件`/etc/ceph/ceph.client.openstack.keyring`密匙环

`密钥环文件`以纯文本的形式`存储密钥`，对文件进行相应的 Linux `文件权限保护`，仅允许 Linux 授权用户访问，只在需要 `Ceph` 用户的`密匙环文件`进行身份验证的系统上部署它

### 传输密钥

`cephx` 协议不以纯文本的形式`传输共享密钥`，相反，客户机从 `Monitor` 请求一个`会话密钥`，`Monitor` 使用客户机的共享密钥加密会话密钥，并向客户机提供会话密钥，客户机解密会话密钥并从 `Monitor` 请求票据，以对集群守护进程进行身份验证。这类似于 `Kerberos` 协议，`cephx` 密钥环文件类似于 `Kerberos keytab`文件


在 Kerberos 中，有三个主要的组件：认证服务器（AS）、票证授予服务器（TGS）和客户端。当一个用户需要访问受保护的资源时，它首先向 AS 发送请求，AS 验证用户的身份，然后生成一个票证并将其发送给 TGS。TGS 再次验证用户的身份，并为用户生成一个可用于访问特定资源的票证。最后，TGS 将票证发送给客户端，客户端使用这个票证来访问所需的服务。

### 配置用户身份验证

使用命令行工具，如`ceph、rados和rbd`，管理员可以使用 `--id` 和 `--keyring` 选项指定用户`帐户`和`密钥环文件`。如果没有指定，命令作为 `client.admin` 进行身份验证

在本例中，`ceph` 命令作为 `client.operator3`进行身份验证列出可用的池

```bash
[ceph: root@node /]# ceph \
	--id operator3 \
	osd lspools
```

在使用`--id` 的时候不适用 `client.` 的前缀，`--id` 会自动使用 `client.` 前缀，而使用`--name`的时候就需要使用 `client.` 的前缀

如果将密钥环文件存储在默认位置，则不需要`--keyring`选项。`cephadm shell`自动从`/etc/ceph/`目录挂载密钥环



## 修改用户权限

用 `ceph auth caps` 命令修改用户帐户的能力(权限)，这个例子修改了 osd 上的 `appuser account` 功能，只允许对myapp池进行读写访问:

```bash
[ceph: root@node /]# ceph auth \
	caps client.app1 \
	mon 'allow r' \
	osd 'allow rw pool=myapp' 
updated caps for client.app1 
```

`ceph auth caps` 命令`覆盖`现有能力，使用该命令时，必须为所有守护进程指定`完整的能力集`，而不仅仅是要修改的那些。


可以定义一个空字符串来删除所有功能。

```bash
[ceph: root@node /]# ceph auth caps client.app1 osd '' 
updated caps for client.app1 
```


### Cephx能力(权限)

在cephx中，对于每个守护进程类型，有几个可用的能力：这里的能力，也就是权限，也做功能

+ `R,授予读访问权限`，每个用户帐户`至少`应该对监视器具有读访问权限，以便能够`检索CRUSH map`
+ `W,授予写访问权限`，客户端需要写访问来存储和修改osd上的对象。对于manager (MGRs)， w授予启用或禁用模块的权限
+ `X,授予执行扩展对象类的授权`，这允许客户端对对象执行额外的操作，比如用`rados lock get或list`列出RBD图像.`class-read和class-write` 是`x`的子集，你通常在RBD池中使用它们
+ `* 授予完全访问权`


创建了 `formyappl` 用户帐户，并赋予了从任意池中存储和检索对象的能力：

```bash
[ceph: root@node /]# ceph auth \
	get-or-create client.formyappl \
	mon 'allow r' \
	osd 'allow rw'
```

### 使用配置文件设置能力

Cephx 提供`预定义的功能配置文件`，在创建用户帐户时，利用配置文件简化用户访问权限的配置

本例通过 `rbd 配置文件`定义新的 `forrbd` 用户帐号的访问权限，客户端应用程序可以使用该帐户使用 `RADOS块设备`对 Ceph存储进行基于块的访问

```bash
[ceph: root@node /]# ceph auth \
	get-or-create client.forrbd \
	mon 'profile rbd' \
	osd 'profile rbd'
```

`rbd-read-only` 配置文件的工作方式相同，但授予只读访问权限，Ceph利用其他现有的配置文件在守护进程之间进行内部通信，不能创建自己的配置文件，Ceph在内部定义它们

```bash
profile rbd-read-only
```

下表列出了默认安装下Ceph的权限

|         能力          | 描述                                                         |
| :-------------------: | ------------------------------------------------------------ |
|        `allow`        | 授予允许能力                                                 |
|          `r`          | 赋予用户读访问权限，需要监视器来检索CRUSH map                |
|          `w`          | 赋予用户对对象的写访问权                                     |
|          `x`          | 使用户能够调用类方法(即读取和写入)并在监视器上执行身份验证操作 |
|      class-read       | 赋予用户调用类读取方法的能力，x的子集                        |
|      class-write      | 赋予用户调用类写入方法的能力，x的子集                        |
|           *           | 为用户提供特定守护进程或池的读、写和执行权限，以及执行管理命令的能力 |
|     `profile osd`     | 允许用户作为OSD连接到其他OSD或监视器，授予osd权限，使osd能够处理复制心跳流量和状态报告。 |
| profile bootstrap-osd | 允许用户引导一个OSD，这样用户在引导一个OSD时就有了添加key的权限 |
|     `profile rbd`     | 允许用户对Ceph块设备进行读写访问                             |
| profile rbd-read-only | 为用户提供对Ceph块设备的只读访问权限                         |

## 权限限制访问

限制用户OSD的权限，使用户只能访问自己需要的池，即可以通过不同的方式来对池等相关对象做限制访问，类似 `白名单`一样。



### 池限制


创建了formyapp2用户，并限制了他们对myapp池的读写权限:

```bash
[ceph: root@node /]# ceph auth \
	get-or-create client.formyapp2 \
	mon 'allow r' \
	osd 'allow rw pool=myapp'
```

如果在配置功能时没有指定池，那么Ceph将在所有现有的池上设置它们，cephx机制可以通过其他方式限制对对象的访问:

### 对象名称前缀 限制
**通过对象名称前缀**，下面的示例限制对任何池中名称以pref开头的对象的访问

```bash
[ceph: root@node /]# ceph auth \
	get-or-create client.formyapp3 \
	mon 'allow r' \
	osd 'allow rw object_prefix pref'
```

### namespace 限制 

`通过namespace`，实现namespace来对池中的对象进行逻辑分组，然后可以将用户帐户限制为属于特定namespace的对象：

```bash
[ceph: root@node /)# ceph auth \
	get-or-create client.designer \
	mon 'allow r' \
	osd 'allow rw namespace=photos'
```

### 路径限制
`通过路径`，Ceph文件系统(cepphfs)利用这种方法来限制对特定目录的访问，下面的例子创建了一个新的用户帐户webdesigner，它只能访问/webcontent目录及其内容:

```bash
[ceph: root@node /]# ceph \
	fs authorize WEBFS \
	client.webdesigner \
  /webcontent rw 
[ceph: root@node /]# ceph auth get client.webdesigner 
exported keyring for client .webdesigner 
[client.webdesigner] 
key = AQBrVE9aNwoEGRAApYR6m71ECRzUlLpp4wEJkw== 
caps mds = "allow rw path=/webcontent" 
caps mon = "allow r" 
caps osd = "allow rw pool=cephfs_data"
```

### 命令限制

通过 `monitor` 命令，这种方法将管理员限制在特定的命令列表中，创建 `operator1`用户帐户并`限制`其访问两个命令的示例如下:

```bash
[ceph: root@node /]# ceph auth \
	get-or-create client.operator1 \
	mon 'allow r, allow command "auth get-or-create", allow command "auth list" '
```



## 删除用户帐号

`ceph auth del` 命令用于删除用户帐号

```bash
[ceph: root@node /]# ceph auth del client.app1
updated
```
然后可以删除相关的密钥环文件



# Demo

```bash
# 在 Ceph 集群中创建两个新的客户端，分别用于编辑和获取名为 replpool1 下的文档资源
cephadm shell -- ceph auth get-or-create client.docedit mon 'allow r' osd 'allow rw pool=replpool1 namespace=docs' | tee /etc/ceph/ceph.client.docedit.keyring

cephadm shell -- ceph auth get-or-create client.docget mon 'allow r' osd 'allow r pool=replpool1 namespace=docs' | tee /etc/ceph/ceph.client.docget.keyring

# 列出 Ceph 认证密钥，并查找与 client.docedit 和 client.docget 对应的密钥
cephadm shell -- ceph auth ls | egrep -A3 'docedit|docget'

# 将客户端密钥文件同步到另一个服务器上
rsync -v /etc/ceph/ceph.client.doc*.keyring serverd:/etc/ceph/

# 在另一个服务器上挂载客户端密钥文件并使用客户端进行操作
cephadm shell --mount /etc/ceph:/etc/ceph

# 使用 client.docedit 客户端在 replpool1.docs 命名空间中写入数据
rados --id docedit -p replpool1 -N docs put adoc /etc/hosts

# 使用 client.docget 客户端从 replpool1.docs 命名空间中读取数据
rados --id docget -p replpool1 -N docs get adoc /tmp/test

# 比较两份数据文件
diff /etc/hosts /tmp/test

# 使用未授权的 client.docget 客户端试图写入数据并触发错误
rados --id docget -p replpool1 -N docs put mywritetest /etc/hosts || echo ERROR

# 更新客户端授权，将 client.docget 授予对 replpool1.docs 和 docarchive 池进行读写操作的权限
ceph auth caps client.docget mon 'allow r' osd 'allow rw pool=replpool1 namespace=docs, allow rw pool=docarchive'

# 使用已授权的 client.docget 客户端在 replpool1.docs 命名空间中写入数据
rados --id docget -p replpool1 -N docs put mywritetest /etc/hosts

# 删除客户端密钥文件和认证记录
rm /etc/ceph/ceph.client.doc*.keyring
ssh serverd rm /etc/ceph/ceph.client.doc*.keyring
cephadm shell -- ceph auth del client.docedit
cephadm shell -- ceph auth del client.docget

```

## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知  :)

***

<https://docs.ceph.com/en/pacific/architecture/>

<https://docs.ceph.com>

CL260 授课老师课堂笔记

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)


