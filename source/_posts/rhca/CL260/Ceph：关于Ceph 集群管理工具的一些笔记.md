---
title: Ceph：关于Ceph 集群管理工具的一些笔记
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-04-30 08:47:05/Ceph：关于Ceph 集群管理工具的一些笔记.html'
mathJax: false
date: 2023-04-30 04:47:05
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***

+ 准备考试，整理ceph 相关笔记
+ 博文内容涉及，Ceph 管理工具 `cephadm，ceph 编排器，Ceph CLI 和 Dashboard GUI` 介绍
+ 理解不足小伙伴帮忙指正



**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

# Ceph存储管理工具

## Ceph 部署工具

以前的Ceph版本使用 `Ceph-ansible` 软件中的 `Ansible Playbooks` 进行部署并管理集群， Ceph O版 引入了 `cephadm` 作为工具来管理集群的整个生命周期(部署、管理和监控)，替换之前的 `ceph-ansible` 提供的功能

`Cephadm` 被视为 Manager 守护进程(MGR)中的一个模块，这是部署新集群时的第一个守护进程，Ceph集群核心集成了所有的管理任务

`Cephadm` 由 Cephadm 包装提供，应该在第一个集群节点上安装这个包，它充当引导节点。

Ceph  被部署在容器中，建立并运行 Ceph 集群的仅有几个安装包要求是 `cephadm、podman、python3、chrony`，容器化版本降低了部署过程中的复杂性和包依赖关系

下图说明了 Cephadm 如何与其他服务交互

![在这里插入图片描述](https://img-blog.csdnimg.cn/2fec32fe59c44f0e9ec4f883ea1fd084.png)

Cephadm 可以登录到容器注册中心来提取Ceph镜像，并使用该映像在节点上部署服务。当引导集群时，这个Ceph容器镜像是必需的，因为部署的 Ceph 容器是基于该镜像，为了与 Ceph  集群 节点交互，Cephadm 使用 SSH 连接向集群添加新主机、添加存储或监控这些主机

Cephadm 常用命令

+ `cephadm bootstrap`: 用于在 Ceph 群集上引导 cephadm 客户端。
+ `cephadm add-repo`: 添加一个自定义 yum 或 apt 存储库，以便 cephadm 客户端可以使用新的软件包。
+ `cephadm deploy`: 使用配置文件指定的配置部署 Ceph 群集。
+ `cephadm rm-daemon`: 从 Ceph 群集中删除指定的 Ceph 守护进程。
+ `cephadm ls`: 列出所有已知主机及其状态、版本和其他详细信息。
+ `cephadm set-privs`: 设置 ssh 私钥配置参数，以便 cephadm 可以通过 SSH 访问主机。
+ `cephadm shell`: 在指定容器中启动一个交互式 shell。
+ `cephadm rm-cluster`: 从主机上完全清除 Ceph 群集，包括数据和配置文件等。


### Ceph编排器

可以使用 `Ceph编排器` 轻松地向集群添加`主机和守护进程`，使用 编排器 来提供 Ceph守护进程和服务，并扩展或收缩集群。通过 `Ceph orch` 命令使用 Ceph编排器，还可以使用`Ceph  Dashboard`接口来运行编排器任务。`cephadm` 脚本与 `Ceph Manager` 业务流程模块交互

下面的图表说明了`Ceph Orchestrator`

![在这里插入图片描述](https://img-blog.csdnimg.cn/2d8bb65afd994c979d0c749e667d3e5b.png)

常用命令

+ `ceph orch apply`: 应用指定的 Service、Daemons 等配置。
+ `ceph orch ls`: 列出所有已知主机及其状态、版本和其他详细信息。
+ `ceph orch ps`: 列出正在运行的 Service、Daemons 等实例。
+ `ceph orch device ls|osd create`: 查看存储设备列表或创建 OSD。
+ `ceph orch service restart`: 重启指定的服务（如 OSD 等）。
+ `ceph orch daemon add`: 在指定的主机上添加一个新的 Daemon 实例。
+ `ceph orch daemon rm`: 从指定的主机上删除一个 Daemon 实例。
+ `ceph orch osd status`: 查看 OSD 的状态和健康状态。
+ `ceph orch upgrade status`: 查看升级进度和状态。
+ `ceph orch release upgrade`: 执行 Ceph 版本升级操作。



### Ceph 管理工具

Ceph部署在容器中，在引导节点中不需要额外的软件，可以从集群的引导节点中的命令行引导集群，引导集群设置了一个最小的集群配置，其中只有一个主机(引导节点)和两个守护进程(监视器和管理进程)，` Ceph O版 `提供两个默认部署的接口:` Ceph CLI 和 Dashboard GUI`


#### Ceph命令行

`Cephadm` 可以启动一个装有所有必需 Ceph 包的容器，使用这个容器的命令是 `cephadm shell`，只应该在引导节点中运行此命令，因为在引导集群时，只有这个节点可以访问`/etc/ceph`中的`admin`密钥

```bash
[root@clienta ~]# cephadm shell
Inferring fsid 2ae6d05a-229a-11ec-925e-52540000fa0c
Inferring config /var/lib/ceph/2ae6d05a-229a-11ec-925e-52540000fa0c/mon.clienta/config
Using recent ceph image registry.redhat.io/rhceph/rhceph-5-rhel8@sha256:6306de945a6c940439ab584aba9b622f2aa6222947d3d4cde75a4b82649a47ff
[ceph: root@clienta /]# 
```

可以通过破折号直接非交互式执行命令

```bash
[root@clienta ~]# cephadm shell -- ceph -s
Inferring fsid 2ae6d05a-229a-11ec-925e-52540000fa0c
Inferring config /var/lib/ceph/2ae6d05a-229a-11ec-925e-52540000fa0c/mon.clienta/config
Using recent ceph image registry.redhat.io/rhceph/rhceph-5-rhel8@sha256:6306de945a6c940439ab584aba9b622f2aa6222947d3d4cde75a4b82649a47ff
  cluster:
    id:     2ae6d05a-229a-11ec-925e-52540000fa0c
    health: HEALTH_OK
```

#### Ceph Dashboard 面板

Ceph O版 Dashboard GUI 通过该接口增强了对许多集群任务的支持，Ceph Dashboard GUI是一个基于web的应用程序，用于监控和管理集群，它以比Ceph CLI更直观的方式提供了集群信息。

与Ceph CLI一样，Ceph 将 Dashboard GUI web服务器作为`Ceph-mgr` 守护进程的一个模块，默认情况下，当创建集群时，Ceph在引导节点中部署`Dashboard GUI`并使用TCP端口`8443`

Ceph Dashboard GUI提供了这些特性：

+ **多用户和角色管理**：可以创建具有多种权限和角色的不同用户帐户
+ **单点登录**：Dashboard GUI允许通过外部身份提供者进行身份验证
+ **审计**：可以配置仪表板来记录所有REST API请求
+ **安全**：Dashboard默认使用SSL/TLS保护所有HTTP连接

Ceph Dashboard GUI还实现了管理和监控集群的不同功能，下面的列表虽然不是详尽的，但总结了重要的管理和监控特点:

##### 管理功能

+ 使用 CRUSH map 查看集群层次结构
+ 启用、编辑和禁用管理器模块
+ 创建、移除和管理osd
+ 管理iSCSI
+ 管理池

##### 监控功能

+ 检查整体集群健康状况
+ 查看集群中的主机及其服务
+ 查看日志
+ 查看集群警报
+ 检查集群容量

下图显示了Dashboard GUI中的状态屏幕。可以快速查看集群的一些重要参数，如集群状态、集群中的主机数量、osd数量等

![在这里插入图片描述](https://img-blog.csdnimg.cn/8c90acdc4ff24693891b90c537d40220.png)

```bash
[root@clienta student]# cephadm shell
Inferring fsid 2ae6d05a-229a-11ec-925e-52540000fa0c
Inferring config /var/lib/ceph/2ae6d05a-229a-11ec-925e-52540000fa0c/mon.clienta/config
Using recent ceph image registry.redhat.io/rhceph/rhceph-5-rhel8@sha256:6306de945a6c940439ab584aba9b622f2aa6222947d3d4cde75a4b82649a47ff
[ceph: root@clienta /]# ceph orch ps | head -n 5
NAME                                HOST                     STATUS         REFRESHED  AGE  PORTS          VERSION           IMAGE ID      CONTAINER ID
alertmanager.serverc                serverc.lab.example.com  running (10m)  9m ago     18M  *:9093 *:9094  0.20.0            4c997545e699  7358b97d835b
crash.clienta                       clienta.lab.example.com  running (10m)  9m ago     18M  -              16.2.0-117.el8cp  2142b60d7974  e9e0350d9f4c
crash.serverc                       serverc.lab.example.com  running (10m)  9m ago     18M  -              16.2.0-117.el8cp  2142b60d7974  8bf8e52b09d2
crash.serverd                       serverd.lab.example.com  running (10m)  9m ago     18M  -              16.2.0-117.el8cp  2142b60d7974  e5447c052636
[ceph: root@clienta /]# ceph health
HEALTH_OK
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/31e7799a7905421ea18156496f6438be.png)

## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)

***

<https://docs.ceph.com/en/pacific/architecture/>

<https://docs.ceph.com>

CL260 授课老师课堂笔记

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)



