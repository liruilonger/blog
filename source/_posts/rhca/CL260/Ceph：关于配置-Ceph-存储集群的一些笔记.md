---
title: Ceph：关于 Ceph 存储集群配置的一些笔记
tags:
  - Ceph
categories:
  - Ceph
toc: true
recommend: 1
keywords: Ceph
uniqueId: '2023-04-10 13:59:42/Ceph：关于 Ceph 存储集群配置的一些笔记.html'
mathJax: false
date: 2023-04-10 21:59:42
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***

+ Ceph 考试整理笔记，老师总结基础上，略有补充
+ 博文内容涉及：
  + ceph 集群的配置简单介绍
  + 永久和零时修改集群配置文件
  + 集群 Mon 的配置
  + 集群身份验证的配置
  + 集群多网络的配置
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***




#  集群管理配置

## Ceph集群配置简介

所有的 Ceph 存储集群配置包含这些必需的定义:
+ 集群网络配置
+ 集群监视器(MON)配置和引导程序选项
+ 集群身份验证配置
+ 守护进程的配置选项

Ceph 配置设置使用唯一的名称，该名称由小写字母与下划线连接，每个 Ceph 守护进程、进程和库都从以下来源访问它的配置:

1. 编译后的默认值
2. 集中式配置数据库
3. 保存在本地主机上的配置文件
4. 环境变量
5. 命令行参数
6. 运行时将覆盖

`监视器(MON)` 节点管理集中的 `配置数据库`，在启动时，Ceph 守护进程通过命令行选项解析环境变量和本地集群配置文件提供的配置选项，守护进程然后联系 `MON集群` 以检索存储在集中配置数据库中的配置设置。

Red Hat Ceph Storage 5 已降级 `/etc/ceph/ceph.conf` 配置文件，使集中`配置数据库`成为存储配置设置的`首选方式`



## 修改集群配置文件

每个 `Ceph` 节点存储一个`本地集群配置文件`，集群配置文件的默认位置为 `/etc/ceph/ceph.conf`，`cephadm` 工具使用最小的选项集创建一个初始的 Ceph 配置文件

配置文件使用一种  INI 文件格式，包含几个部分，其中包括对
+ Ceph守护进程
+ client

的配置，每个 section 都有一个名称，它是用[name]头定义的，以及定义为键值对的一个或多个参数

```ini
[name] 
parameterl = valuel
parameter2 = value2 
```

使用`井号#分号;`来禁用设置或添加注释，设置引导集群时使用集群配置文件以及自定义设置，使用 `cephadm boostrap` 带有 `--config` 选项的命令来传递配置文件

```bash
[root@node ~]# cephadm bootstrap --config ceph-config.yaml
```



### 配置 Sections

Ceph 将配置设置组织到组中，无论是存储在 `配置文件` 中还是存储在 `配置数据库`，使用 sections 调用它们应用到的守护进程或客户端

+ `[global]` 存储所有守护进程或读取配置的任何进程(包括客户端)通用的通用配置，可以通过为各个守护进程或客户端创建被调用的部分来覆盖[global]参数
+ `[mon]`: 存储了监视器(mon)的配置
+ `[osd]`: 存储osd守护进程的配置
+ `[mgr]`: 存储Managers (mgr)的配置
+ `[mds]`: 存储元数据服务器(mds)的配置
+ `[client]` 存储了应用于所有Ceph客户端的配置
 

### 实例设置

将应用于特定守护进程实例的设置分组在各自的部分中，名称为 `[daemon-type.instance-id]`

```ini
[mon]
设置全局的
[mon.serverc]
设置单个实例的
```

同样的命名也适用于[osd]， [mgr]， [mds]和[client]段。对于OSD进程，实例ID总是为数字，例如[osd.0]，对于客户端，实例ID为活动用户名，例如[client.operator3]



### 元变量

元变量是 `ceph` 定义的变量。使用它们可以简化配置

+ `$cluster`：Red Hat Ceph Storage 5集群名称，默认集群名称为ceph
+ `$type`：守护进程类型，例如monitor使用mon，OSDs使用osd、MDSes使用mds, MGRs使用mgr，client应用程序使用client
+ `$id`：守护进程实例ID，在serverc上的monitor变量的值为的serverc，osd 1的id是osd.1，client应用程序是用户名
+ `$name`：守护进程名称和实例ID，这个变量是`$type.$id`的快捷方式
+ `$host`：运行守护进程的主机名

```bash
[root@serverc ~]# ceph status
  cluster:
    id:     4c759c0c-d869-11ed-bfcb-52540000fa0c
    health: HEALTH_OK

  services:
    mon: 3 daemons, quorum serverc.lab.example.com,serverd,servere (age 2h)
    mgr: serverc.lab.example.com.cbhfyf(active, since 3h), standbys: serverd.itakzd, servere.teklwv
    osd: 9 osds: 9 up (since 2h), 9 in (since 2h)

  data:
    pools:   1 pools, 1 pgs
    objects: 0 objects, 0 B
    usage:   51 MiB used, 90 GiB / 90 GiB avail
    pgs:     1 active+clean

```

## 使用集中式配置数据库

`MON` 集群在 `MON`节点上管理和存储`集中配置数据库`，可以临时更改设置(直到守护进程重新启动)，也可以配置设置永久保存并存储在数据库中，可以在`集群运行时更改大多数配置设置`

使用 `ceph config` 命令查询数据库，查看配置信息

+ `ceph config ls` 列出所有可能的配置设置
+ `ceph config help setting` 查询特定的配置设置的帮助
+ `ceph config dump` 显示集群配置数据库设置
+ `ceph config show $type.$id`显示特定守护进程的数据库设置。使用show-with -defaults包含默认设置
+ `ceph config get $type.$id`，以获得特定的配置设置
+ `ceph config set $type.$id`，用于设置特定的配置设置

```bash
[root@serverc ~]# ceph config set mon mon_allow_pool_delete true
```
```bash
# cephadm shell
[ceph: root@clienta /]# ceph config dump
[ceph: root@clienta /]# ceph config show osd.1

[ceph: root@clienta /]# ceph config \
show osd.1 debug_ms
[ceph: root@clienta /]# ceph config \
get osd.1 debug_ms

[ceph: root@clienta /]# ceph config \
set osd.1 debug_ms 10

[ceph: root@clienta /]# ceph config \
show osd.1 debug_ms
[ceph: root@clienta /]# ceph config \
get osd.1 debug_ms

[ceph: root@clienta /]# ceph orch \
daemon restart osd.1

[ceph: root@clienta /]# ceph config \
show osd.1 debug_ms
10/10
[ceph: root@clienta /]# ceph config \
get osd.1 debug_ms
```


这里的配置为永久的，如果使用 `ceph tell` 命令则为临时修改

```bash

[ceph: root@clienta /]# ceph tell \
osd.1 config get debug_ms
[ceph: root@clienta /]# ceph tell \
osd.1 config set debug_ms 5
[ceph: root@clienta /]# ceph tell \
osd.1 config get debug_ms
5
[ceph: root@clienta /]# ceph orch \
daemon restart osd.1
[ceph: root@clienta /]# ceph tell \
osd.1 config get debug_ms

[ceph: root@clienta /]# <Ctrl-D>

[root@clienta ~]# <Ctrl-D>
```


将现有的 `ceph.conf` 配置文件导入到 `Ceph` 配置数据库中。当您有一个现有的 ceph.conf 文件并希望将其与 Ceph 集群一起使用，但不想手动输入所有配置选项时，此命令非常有用。

```bash
[ceph: root@node /]# ceph config assimilate-conf -i ceph.conf
```


`assimilate-conf` 命令读取 ceph.conf 文件并将配置选项导入到 Ceph 配置数据库中。这使您可以使用 ceph config 命令管理配置选项，该命令提供了一种更结构化和组织化的管理配置选项的方式。



## 集群引导选项

一些选项提供启动集群所需的信息。`MON` 节点通过读取 `monmap` 来找到其他的 `MON` 并建立 `quorum`。 `MON` 节点读取 `ceph.conf` 文件来确定如何与其他 `MONs` 进行通信

`mon_host` 选项列出集群监视器，此选项非常重要，不能存储在配置数据库中，为了避免使用集群配置文件，Ceph 集群支持使用 DNS 服务记录来提供 `mon_host` 列表

本地集群配置文件可以包含其他选项以满足的需求：
+ `mon_host_override`，集群要联系以开始通信的初始监视器列表
+ `mon_dns_serv_name`, 要检查的DNS SRV记录的名称，以便通过DNS识别集群监视器
+ `mon_data, osd_data, mds_data, mgr_data`，定义守护进程的本地数据存储目录
+ `Keyring，keyfile和key`，它们是要用监视器进行身份验证的身份验证凭证



## 使用服务配置文件

服务配置文件是引导存储集群和其他 `Ceph` 服务的 YAML 文件。`cephadm` 工具通过平衡集群中运行的守护进程来协调服务部署、大小和放置，通过各种参数，可以更明确地部署 `osd、mon` 等服务

下面是一个服务配置文件示例：

```yaml
service_type: mon
placement:
  host_pattern: "mon"
  count: 3 
---
service_type: osd 
service_id: default_drive_group 
placement: 
  host_pattern: "osd*" 
data_devices: 
  all: true
```

1. Service_type定义了服务的类型，如mon、mds、mgr、rgw等

2. 位置定义要部署的服务的位置和数量，可以定义主机、主机模式或标签来选择目标服务器

3. `data_devices` 是特定于 `OSD` 服务的，支持的过滤参数为大小、模型或路径等

使用 `cephadm bootstrap - -apply- spec` 命令应用指定文件中的服务配置

```bash
[root@node ~]# cephadm bootstrap \
	--apply-spec service-config.yaml 
```



## 在运行时覆盖配置设置

可以在运行时更改大多数集群配置设置，可以在`守护进程运行时临时更改配置设置`

### ceph tell 方式
`ceph tell $type.$id config` 命令临时覆盖配置设置，并且要求所配置的`MONs和守护进程都在运行`，在任何配置为`运行 ceph 命令`的`集群主机`上运行此命令，使用该命令更改的设置`在守护进程重新启动时恢复到原来的设置`
+ `ceph tell $type.$id config get` 获取守护进程的特定运行时设置
+ `ceph tell $type.$id config set` 设置守护进程的特定运行时设置，当守护进程重新启动时，这些临时设置会恢复到原来的值

`ceph tell $type.$id config` 命令还可以接受通配符来获取或设置同一类型的所有守护进程的值。例如`ceph tell osd.* config get debug_ms` 显示集群中所有OSD守护进程的该设置的值

### ceph daemon 方式

可以使用`ceph daemon $type.$id config` 临时覆盖配置设置，在`需要设置的集群节点`上运行此命令,

`ceph daemon` 不需要通过 `MONs` 进行连接，即使在 `MONs` 不运行的情况下，`ceph daemon` 命令仍然可以运行，这对于故障排除很有用

+ `ceph daemon $type.$id config get` 获取守护进程的特定运行时设置
+ `ceph daemon $type.$id config set` 设置守护进程的特定运行时设置，当守护进程重新启动时，临时设置会恢复到原来的值

### ceph tell  和 ceph daemon  有什么区别？

`ceph tell` 和 `ceph daemon` 命令都是用于与 `Ceph` 守护进程进行交互的命令，但它们的使用方式和目的略有不同。

`ceph tell` 命令用于向指定的 `Ceph` 守护进程发送命令。例如，您可以使用 ceph tell osd.0 version 命令向 osd.0 守护进程发送 version 命令，以获取该守护进程的版本信息。ceph tell 命令还可以向多个守护进程发送命令，以便同时执行相同的操作。比如 `ceph tell osd.* config get debug_ms`

ceph daemon 命令用于直接控制指定的 Ceph 守护进程。例如，您可以使用 ceph daemon osd.0 config show 命令来获取 osd.0 守护进程的配置信息。ceph daemon 命令还可以用于启动、停止、重启和重新加载指定的守护进程。

总之，`ceph tell 命令用于向守护进程发送命令，而 ceph daemon 命令用于直接控制守护进程`。


# 集群监控配置

## Ceph监控配置

Ceph监视器(MONs)存储和维护客户端用来查找MON和OSD节点的集群映射，Ceph客户端在向osd读写任何数据之前，必须连接到一个 MON 来检索集群映射，因此，正确配置集群 MONs 至关重要，MONs通过使用一种变异的 Paxos 算法组成一个quorum ，选出一个 leader，在一组分布式计算机之间达成共识，MONs有以下角色之一：

+ Leader：第一个获得最新版本的cluster map的MON
+ Provider：一个MON，它具有集群映射的最新版本，但不是leader
+ Requester：一个MON，它没有最新版本的集群映射，并且在重新加入仲裁之前必须与 Provider 同步

`同步总是在新的 MON 加入集群时发生`，每个 MON 定期检查相邻的监视器是否有最新版本的集群映射，如果一个MON没有集群映射的最新版本，那么它必须同步并获取它

要建立仲裁，集群中的大多数 MONs 必须处于运行状态，例如，如果部署了 5 个 MONs，那么必须运行3个MONs来建立仲裁，`在生产Ceph集群中部署至少三个MON节点`，以确保高可用性.

支持对运行中的集群添加或移除mon，集群配置文件定义了用于集群操作的MON主机IP地址和端口，`rnon_host` 设置可以包含IP地址或DNS名称，`cephadm` 工具无法更新集群配置文件，定义一个策略来保持集群配置文件在集群节点之间同步，例如使用rsync

```ini
[global]
mon_host = [v2:172.25.250.12:3300,v1:172.25.250.12:6789], [v2:172.25.250.13:3300,v1:172.25.250.13:6789], [v2:172.25.250.14:3300, v1:172.25.250.14:6789] 
```

不建议在集群部署并运行后修改MON节点IP地址

## 查看Monitor仲裁

使用 `ceph status`或 `ceph mon stat` 命令验证 `MON` 仲裁状态

```bash
[ceph: root@clienta /]#  ceph mon stat
e4: 4 mons at {clienta=[v2:172.25.250.10:3300/0,v1:172.25.250.10:6789/0],serverc.lab.example.com=[v2:172.25.250.12:3300/0,v1:172.25.250.12:6789/0],serverd=[v2:172.25.250.13:3300/0,v1:172.25.250.13:6789/0],servere=[v2:172.25.250.14:3300/0,v1:172.25.250.14:6789/0]}, election epoch 134, leader 0 serverc.lab.example.com, quorum 0,1,2,3 serverc.lab.example.com,clienta,serverd,servere
```

或者，使用` ceph quorum_status`命令。添加 `-f json-pretty` 选项以创建更可读的输出

```json
[ceph: root@clienta /]# ceph quorum_status -f json-pretty

{
    "election_epoch": 134,
    "quorum": [
        0,
        1,
        2,
        3
    ],
    "quorum_names": [
        "serverc.lab.example.com",
        "clienta",
        "serverd",
        "servere"
    ],
    "quorum_leader_name": "serverc.lab.example.com",
```

也可以在 `Dashboard` 中查看 `MONs` 的状态。在仪表板中，单击 `Cluster-->Monitors`，查看 `Monitor节点`和仲裁的状态



## 分析 mon map

Ceph 集群地图包括 `MON map、OSD map、PG map、MDS map 和 CRUSH map`

MON 映射包含集群 fsid(文件系统ID)，以及与每个MON节点通信的名称、IP地址和网口。fsid是一个惟一的、自动生成的标识符(UUID)，用于标识 Ceph 集群

MON 映射还保存映射版本信息，例如最后一次更改的序号(epoch )和时间，MON节点通过同步更改和对当前版本达成一致来维护映射

使用 `ceph mon dump` 命令查看当前的mon映射。

```bash
[ceph: root@clienta /]# ceph mon dump
epoch 4
fsid 2ae6d05a-229a-11ec-925e-52540000fa0c
last_changed 2021-10-01T09:33:53.880442+0000
created 2021-10-01T09:30:30.146231+0000
min_mon_release 16 (pacific)
election_strategy: 1
0: [v2:172.25.250.12:3300/0,v1:172.25.250.12:6789/0] mon.serverc.lab.example.com
1: [v2:172.25.250.10:3300/0,v1:172.25.250.10:6789/0] mon.clienta
2: [v2:172.25.250.13:3300/0,v1:172.25.250.13:6789/0] mon.serverd
3: [v2:172.25.250.14:3300/0,v1:172.25.250.14:6789/0] mon.servere
dumped monmap epoch 4
```



## 管理集中式配置数据库

MON节点存储和维护集中式配置数据库，数据库在每个MON节点上的默认位置是`/var/lib/ceph/$fsid/mon.$host/store.db`，不建议更改数据库的位置。

随着时间的推移，数据库可能会变大，运行 `ceph tell mon.$id compact` 命令用于压缩数据库以提高性能，另外，将`mon_compact_on_start` 配置设置为true，以便在每次daemon启动时压缩数据库:

```bash
[ceph: root@clienta /]# ceph \
	config set mon mon_compact_on_start true 
```

定义基于数据库大小触发健康状态更改的阈值设置：

| 描述                                                 | 设置                   | 默认值     |
| -------------------------------------------------- | -------------------- | ------- |
| 配置数据库超过此大小时，将群集健康状态更改为HEALTH_WARN                  | mon_data_size_warn   | 15 (GB) |
| 当存储配置数据库的文件系统的剩余容量小于或等于该百分比时，将集群健康状态更改为HEALTH_WARN | mon_data_avail_warn  | 30 (%)  |
| 当存储配置数据库的文件系统的剩余容量小于或等于该百分比时，将集群健康状态更改为HEALTH_ ERR | mon_ data avail_crit | 5 (%)   |



# 集群的身份验证

Ceph 默认使用 Cephx 协议在 Ceph 组件之间进行加密身份验证，并使用共享密钥进行身份验证。


Cephx 是 Ceph 用于认证客户端和服务器的安全认证协议。它旨在通过加密客户端和服务器之间传输的所有数据来提供安全通信。

Cephx 使用对称和非对称加密的组合来提供安全通信。当客户端连接到 Ceph 集群时，它首先使用共享的密钥进行身份验证。然后使用此密钥生成会话密钥，该密钥用于加密客户端和服务器之间的所有后续通信。

Cephx 还支持使用公钥加密进行身份验证。在此模式下，客户端和服务器交换公钥并使用它们来加密和解密消息。这提供了额外的安全层，因为它确保只有授权的客户端可以连接到 Ceph 集群。


使用 `cephadm` 部署集群时，默认启用 `cephx`。如果需要，可以禁用 `Cephx`，但不建议这样做，因为它会削弱集群的安全性，为启用或禁用cephx，`ceph config set`命令可以管理多个设置

```bash
[ceph: root@clienta /]# ceph \
	config get mon auth_service_required
[ceph: root@clienta /]# ceph \
	config get mon auth_cluster_required
[ceph: root@clienta /]# ceph \
	config get mon auth_client_required
```

`/etc/ceph` 目录和守护进程数据目录中包含`cepphx` 密钥环文件，对于MONs，数据目录为`/var/lib/ceph/$fsid/mon.$host/`

##  `ceph auth` 命令创建、查看和管理集群密钥。


ceph auth 命令用于管理 Ceph 集群中的 Cephx 认证。它允许您创建和管理密钥环文件，其中包含用于身份验证的共享密钥。

以下是您可以使用 ceph auth 命令执行的一些操作：

- 创建新的密钥环文件： 您可以使用 ceph auth get-or-create-key 命令为客户端或监视器创建新的密钥环文件。例如，要为名为 client.foo 的客户端创建密钥环文件，可以运行以下命令：

```bash
ceph auth get-or-create-key client.foo mon 'allow r' osd 'allow rw' -o /path/to/keyring/file
```


此命令在 /path/to/keyring/file 处创建新的密钥环文件，并授予 client.foo 客户端对监视器的读取访问权限以及对 OSD 的读写访问权限。

- 向现有密钥环文件添加新密钥： 您可以使用 ceph auth add 命令向现有密钥环文件添加新密钥。例如，要将名为 client.bar 的客户端的新密钥添加到 /path/to/keyring/file 中的现有密钥环文件中，可以运行以下命令：

```bash
ceph auth add client.bar -i /path/to/new/keyring/file
```



此命令将新密钥添加到 /path/to/keyring/file 中的现有密钥环文件中。

- 列出密钥环文件中的密钥： 您可以使用 ceph auth list 命令列出密钥环文件中的密钥。例如，要列出 /path/to/keyring/file 中的密钥环文件中的所有密钥，可以运行以下命令：

```bash
 ceph auth list -i /path/to/keyring/file
```


此命令列出密钥环文件中的所有密钥。

- 从密钥环文件中删除密钥： 您可以使用 ceph auth del 命令从密钥环文件中删除密钥。例如，要从 /path/to/keyring/file 中的密钥环文件中删除名为 client.foo 的客户端的密钥，可以运行以下命令：


```bash
  ceph auth del client.foo -i /path/to/keyring/file
```



此命令从密钥环文件中删除 client.foo 的密钥。





##  `ceph-authtool` 命令创建 key-ring 文件


ceph-authtool 命令用于在 Ceph 集群之外管理 Cephx 认证。它可以用于创建新的密钥环文件，向现有密钥环文件添加新密钥以及显示密钥环文件的内容。


下面的命令为MON节点创建一个密钥环文件

```bash
[ceph: root@clienta /]# ceph-authtool \
	--create-keyring /tmp/ceph.mon.keyring \
	--gen-key \
	-n mon \
	--cap mon 'allow *'
```

## ceph auth 命令和 ceph-authtool 命令的区别?

`ceph auth ` 命令用于管理 `Ceph` 集群中的 `Cephx` 认证，而 `ceph-authtool` 命令用于管理 `Cephx` 认证，但是是在 `Ceph` 集群之外。

`ceph auth ` 命令允许您创建和管理 `密钥环`文件，其中包含用于身份验证的共享密钥。您可以使用它来创建新的密钥环文件，向现有密钥环文件添加新密钥，列出密钥环文件中的密钥以及从密钥环文件中删除密钥。

另一方面，`ceph-authtool` 命令用于在 Ceph 集群之外创建、修改和显示 `Cephx` 认证密钥和密钥环文件。它可以用于创建新的密钥环文件，向现有密钥环文件添加新密钥以及显示密钥环文件的内容。



cephadm 工具在 `/etc/ceph` 目录下创建 `client.admin` 用户，它允许运行管理命令和创建其他ceph客户端用户帐户



# 配置集群网络

## 配置公共网络和集群网络

public 网络是所有 Ceph 集群通信的默认网络，cephadm 工具假设第一个 MON 守护进程IP地址的网络是 public 网络，新的 MON 守护进程部署在 public 网络中，除非显式地定义了不同的网络

Ceph客户端通过集群的public网络直接向osd发出请求，OSD复制和恢复流量使用public网络，除非为此配置单独的cluster网络

`配置单独的cluster网络可以通过减少public网络流量负载和将客户端流量与后端OSD操作流量分离来提高集群性能`

![在这里插入图片描述](https://img-blog.csdnimg.cn/665936cc47e34027819bc4ae244baf8a.png)


执行以下步骤，为单独的集群网络配置节点：
1. 在每个集群节点上配置一个额外的网络接口
2. 在每个节点的新网口上配置相应的cluster网络IP地址
3. 使用 `cephadm bootstrap` 命令的 `--cluster-network` 选项在集群 `bootstrap` 创建 `cluster` 网络

可以使用集群配置文件来设置public和cluster网络。每个网络可以配置多个子网，子网之间以“，”分隔。子网使用CIDR符号表示，例如:172.25.250.0/24

```ini
[global] 
public_network = 172.25.250.0/24, 172.25.251.0/24 
cluster_network = 172.25.249.0/24 
```

可以使用ceph config set命令或ceph config assimilate-conf命令更改public和cluster网络



### 配置特定的守护进程

MON守护进程绑定特定的IP地址，而MGR、OSD和MOS守护进程默认绑定任何可用的IP地址.


在Red Hat Ceph Storage 5中，cephadm通过对大多数服务使用public网络在任意主机上部署守护进程，为了处理cephadm部署新守护进程的位置，可以定义一个特定的子网供服务使用，只有IP地址在同一子网的主机才会考虑部署该服务。

设置172.25.252.0/24子网到MON守护进程

```bash
[ceph: root@node /)# ceph \
	config set mon public_network 172.25.252.0/24
```

这个示例命令等价于集群配置文件中的下面的[mon]部分

```ini
[mon) 
public_network = 172.25.252.0/24 
```

使用`ceph orch daemon add`命令手动将守护进程部署到特定的子网或IP地址

```bash
[ceph: root@node /)# ceph orch \
	daemon add mon cluster-host02:172.25.251.0/24 
[ceph: root@node /)# ceph orch \
	daemon rm mon.cluster-host01
```

不建议使用运行时ceph orch守护进程命令进行配置更改，相反，建议使用服务规范文件作为管理Ceph集群的方法



### 运行IPV6

`ms_bind_ipv4`的缺省值为true, `ms_bind_ipv6`的缺省值为false，要将Ceph守护进程绑定到IPv6地址，需要在集群配置文件中设置`ms_bind_ipv6`为true，设置`ms_bind_ipv4`为false

```ini
[global] 
public_network = <1Pv6 public-network/netmask>
cluster_network = <IPv6 cluster-network/netmask>
```



### 启用巨型帧

建议在存储网络上配置网络MTU以支持巨型帧，这可能会提高性能，在集群网口上配置 MTU 值为`9000`，以支持巨型帧

`同一通信路径下的所有节点和网络设备的MTU值必须相同`。对于 bound 网口，配置 bound 网口的MTU值后，底层接口继承相同的MTU值



## 配置网络安全

通过减少public网络上的攻击面，挫败针对集群的某些类型的拒绝服务(DoS)攻击，并防止osd之间的通信中断，配置单独的cluster网络还可以提高集群的安全性和可用性。当osd之间的通信中断时，可以防止客户端读写数据

将后端OSD流量隔离到自己的网络中可能有助于防止public网络上的数据泄露，为保证后端cluster网络安全，请确保流量不能在cluster与public之间路由



##  配置防火墙规则

Ceph OSD和MDS默认绑定的TCP端口范围为6800 ~ 7300。要配置不同的范围，请修改ms_bind_port_min和ms_bind_port_max设置

下表列出了Red Hat Ceph Storage 5的默认端口。

| 服务名称                      | 端口                               | 描述                                                                                                                               |
| ------------------------- | -------------------------------- | -------------------------------------------------------------------------------------------------------------------------------- |
| Monitor (MON)             | 6789/TCP (msgr),3300/TCP (msgr2) | Ceph集群内的通信                                                                                                                       |
| OSD                       | 6800-7300/TCP                    | 每个OSD使用3个端口:1个用于通过public与客户端和MONs通信；一个用于通过cluster网络向其他osd发送数据，如果前者不存在，则通过public网络发送数据；另一个用于在cluster网络或public网络上交换心跳数据包，如果前者不存在的话 |
| Metadata Server(MDS)      | 6800-7300/TCP                    | 与Ceph元数据服务器通信                                                                                                                    |
| Dashboard/Manager(MGR)    | 8443/TCP                         | 通过SSL与Ceph管理器仪表板通信                                                                                                               |
| Manager RESTful Module    | 8003/TCP                         | 通过SSL与Ceph Manager RESTful模块通信                                                                                                   |
| Manager Prometheus Module | 9283/TCP                         | 与Ceph Manager Prometheus插件的通信                                                                                                    |
| Prometheus Alertmanager   | 9093/TCP                         | 与Prometheus Alertmanager服务的通信                                                                                                    |
| Prometheus Node Exporter  | 9100/TCP                         | 与Prometheus Node Exporter守护进程通信                                                                                                  |
| Grafana server            | 3000/TCP                         | 与Grafana服务沟通                                                                                                                     |
| Ceph Object Gateway (RGW) | 80/TCP                           | 与Ceph RADOSGW通信。如果client.rgw配置段为空，Cephadm使用默认的80端口                                                                               |
| Ceph iSCSI Gateway        | 9287/TCP                         | 与Ceph iSCSI网关通信                                                                                                                  |

MONs总是在public网络上运行。为了保证的MON节点安全可以启用防火墙规则，需要配置带有public接口和public网络IP地址的规则。可以手动将端口添加到防火墙规则中

```bash
[root@node ~]# firewall-cmd --permanent --zone=public \
	--add-port=6789/tcp
[root@node ~]# firewall-cmd --reload 
```

还可以通过将ceph-mon服务添加到防火墙规则中来保护MON节点

```bash
[root@node ~]# firewall-cmd --permanent --zone=public \
	--add-service=ceph-mon 
[root@node ~]# firewall-cmd --reload
```

为了配置cluster网络，osd需要同时配置public网络和cluster网络的规则，客户端通过public连接osd, osd之间通过cluster网络通信

为了保护OSD不受防火墙规则的影响，需要配置相应的规则网口和IP地址

```bash
[root@node ~]# firewall-cmd --permanent \
	--zone=<public-or-cluster> --add-port=6800-7300/tcp 
[root@node ~]# firewall-cmd --reload
```

也可以通过在防火墙规则中添加ceph服务来保护OSD的安全

```bash
[root@node ~]# firewall-cmd --permanent \
	--zone=<public-or-cluster> --add-service=ceph 
[root@node ~]# firewall-cmd --reload
```

## 一些 Demo

### 管理集群配置设置
```bash
[root@clienta ~]# ceph config show osd.1 debug_ms
0/0
[root@clienta ~]# ceph config get  osd.1 debug_ms
0/0
[root@clienta ~]# ceph config set  osd.1 debug_ms 10
[root@clienta ~]# ceph config get  osd.1 debug_ms
10/10
[root@clienta ~]#
```

```bash
[root@clienta ~]# ceph tell osd.1  config get debug_ms
{
    "debug_ms": "10/10"
}
[root@clienta ~]# ceph tell osd.1  config set debug_ms 5
{
    "success": ""
}
[root@clienta ~]# ceph tell osd.1  config get debug_ms
{
    "debug_ms": "5/5"
}
[root@clienta ~]#
```

```bash
[root@clienta ~]# ceph orch daemon restart osd.1
Scheduled to restart osd.1 on host 'serverc.lab.example.com'
[root@clienta ~]# ceph tell osd.1  config get debug_ms
{
    "debug_ms": "10/10"
}
[root@clienta ~]#
```


### 配置集群 Monitors
```bash
[root@clienta ~]# ceph config set mon mon_compact_on_start true
[root@clienta ~]# ceph health
HEALTH_OK
[root@clienta ~]#
```



## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)

***

<https://docs.ceph.com/en/pacific/architecture/>

<https://docs.ceph.com>+

CL260 授课老师课堂笔记

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)


