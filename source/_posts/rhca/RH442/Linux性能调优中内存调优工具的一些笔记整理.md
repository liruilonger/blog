---
title: Linux性能调优中内存调优工具的一些笔记整理
tags:
  - Linux
categories:
  - Linux
toc: true
recommend: 1
keywords: Linux
uniqueId: '2022-07-31 11:05:53/Linux性能调优中内存调优工具的一些笔记整理.html'
mathJax: false
date: 2022-07-31 19:05:53
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 和小伙伴分享一些`Linux性能调优中内存调优工具`的笔记,内容很浅，可以用作入门
+ 博文内容结合`《Linux性能优化》`读书笔记整理
+ 涉及内容包括
  + 系统内存相关理论
  + 实际的调优工具介绍 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***






# Linux 系统内存
## 内存性能统计信息

每一种系统级Linux性能工具都提供了不同的方式来提取类似的统计结果。

### 内存子系统和性能

在现代处理器中，与CPU执行代码或处理信息相比，`向内存子系统保存信息或从中读取信息一般花费的时间更长`。通常，在`CPU执行指令`或处理数据前，它会消耗相当多的空闲时间来`等待从内存中取出指令和数据`。处理器用不同层次的`高速缓存(cache)`来弥补这种缓慢的内存性能。

### 内存子系统(虚拟存储器)

任何给定的Linux系统都有一定容量的`RAM或物理内存`。在这个物理内存中寻址时，Linux将其分成`块`或`内存“页”`。当对内存进分配或传送时，Linux`操作的单位是页`，而不是`单个字节`。

在报告一些内存统计数据时，Linux内核报告的是`每秒页面的数量`，该值根据其运行的架构可以发生变化。

对IA32架构而言(“`Intel32位体系结构`”(Intel Architecture 32-bit)，而我们常说的X86-64就是IA32的64位拓展。)，页面大小为`4KB`。极少数情况下，这些页面大小的内存块会导致极高的`跟踪开销`，所以，内核用更大的`块来操作内存`，这些块被称为`HugePage(大页面)`。


它们的容量为`2048KB`，而不是`4KB`，这大大降低了管理庞大内存的开销。某些应用，如Oracle，用这些大页面加载内存中的大量数据，同时又`最小化Linux内核的管理开销`。如果`HugePage`不能完全被填满，就会浪费相当多的内存。`一个半填充的普通页面浪费2KB内存`，而`一个半填充的HugePage就会浪费1024KB的内存`。


Linux内核可以分散收集这些物理页面，向应用程序呈现出一个精心设计的虚拟内存空间。

#### 交换(物理内存不足)

所有系统`RAM芯片的物理内存容量`都是固定的。即使应用程序需要的内存容量大于可用的物理内存，Linux内核仍然允许这些程序运行。Linux内核使用`硬盘作为临时存储器`，这个硬盘空间被称为`交换分区(swap space)`。

尽管交换是让进程运行的极好的方法，但它却慢的要命。与使用物理内存相比，应用程序使用交换的速度可以慢到一千倍。`如果系统性能不佳，确定系统使用了多少交换通常是有用的。`

#### 缓冲区(buffer)和缓存(cache)(物理内存太多)

##### 缓存(cache)
相反，如果你的`系统物理内存容量超过了应用程序的需求`，Linux就会在`物理内存中缓存近期使用过的文件`，这样，后续访问这些文件时就不用去访问硬盘了。

对要频繁访问硬盘的应用程序来说，这可以显著加速其速度，显然，对经常启动的应用程序而言，这是特别有用的。

应用程序首次启动时，它需要从硬盘读取；但是，如果应用程序留着缓存中，那它就需要从更快速的物理内存读取。

`这个硬盘缓存不同于前面章节提到的处理器高速缓存(cache)`

##### 缓冲区(buffer)

Linux还使用了额外的存储作为缓冲区。为了进一步优化应用程序，`Linux为需要被写回硬盘的数据预留了存储空间。这些预留空间被称为缓冲区`。如果应用程序要将`数据写回硬盘`，通常需要花费`较长时间`，Linux让应用程序`立刻继续执行`，但将`文件数据`保存到`内存缓冲区`。在之后的某个时刻，`缓冲区被刷新到硬盘`，而应用程序可以`立即继续`。



`高速缓存和缓冲区`的使用使得系统内`空闲的内存`很少，默认情况下，Linux试图尽可能多的使用你的内存。这是好事。

如果`Linux侦测`到有`空闲内存`，它就会`将应用程序和数据缓存到这些内存以加速未来的访问`。由于访问内存的速度比访问硬盘的速度`快了几个数量级`，因此，这就可以显著地提升`整体性能`。

如果系统需要缓存空间做更重要的事情，那么缓存空间将被擦除并交给系统。之后，对原来被缓存对象的访问就需要转向硬盘来满足。

#### 3活跃与非活跃内存

+ `活跃内存`是指当前被进程使用的内存。
+ `不活跃内存`是指已经被分配了，但暂时还未使用的内存。
  
这两种类型的内存没有本质上的区别。需要时，Linux找出进程最近最少使用的`内存页面`，并将它们从活跃列表移动到不活跃列表。当要选择把`哪个内存页交换到硬盘`时，`内核就从不活跃内存列表中进行选择`。

#### 高端与低端内存

对拥有1GB或更多物理内存的32位处理器(比如lA32)来说，Linux管理内存时必须将其分为`高端与低端内存`。

高端内存`不能`直接被`Linux内核`访问，而是必须在使用前`映射到低端`内存范围内。

64位处理器(比如AMD64/EM6T、Alpha或Itanium)没有这个问题，因为它们可以直接寻址当前系统可用的额外内存。

#### 内核的内存使用情况(分片)

除了应用程序需要分配内存外，`Linux内核`也会为了`记账`的目的消耗一定量的内存。

记账包括，比如跟踪从网络或磁盘I/O来的数据，以及跟踪哪些进程正在运行，哪些正在休眠。`为了管理记账，内核有一系列缓存`，包含`了一个或多个内存分片`。每个分片为一组对象，个数可以是一个或多个。`内核消耗的内存分片数量取决于使用的是Linux内核的哪些部分`，而且还可以随着机器负载类型的变化而变化。

## Linux内存性能调优工具

### vmstat

+ 使用了多少交换分区。
+ 物理内存是如何被使用的。
+ 有多少空闲内存。

vmstat除了提供CPU统计信息外，你还可以通过如下命令行调用vmstat来调查内存统计信息：

`vmstat[-a][-s][-m]`


|选项|说明|
|--|--|
|-a |该项改变内存统计信息的默认输出以表示活跃/非活跃内存量，而不是缓冲区和高速缓存使用情况的信息|
|-s(oroc0s 3.2或更高版本)|打印输出vm表。自系统启动开始的综合统计信息。该项不能用于采样模式，它包含了内存和CPU的统计数据|
|-m(procps3.2或更高版本)|该项输出内核分片信息。键入cat/proc/slabinfo可以获得同样的信息。信息详细展示了内核内存是如何分配的，并有助于确定哪部分内核消耗内存最多|

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 3  0      0 27270048   3104 984432    0    0    38    23  315  294  4  2 94  0  0
```

vmstat调用时没有使用任何命令行选项，它显示的是从`系统启动开始的性能统计数据的均值(si和so)`，以及其他统计信息的瞬时值(swpd、free、buff和cache)

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat  1 100
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free   buff  cache   si   so    bi    bo   in   cs us sy id wa st
 1  0      0 23365320   3104 962048    0    0   176    23  305  296  2  3 95  0  0
 1  0      0 23365164   3104 962088    0    0     0     0  768  854  0  0 100  0  0
 0  0      0 23364900   3104 962088    0    0     0     0  836  993  0  0 100  0  0
^C
```

+ **swpd** :当前`交换到硬盘的内存总量`
+ **free** :`未被操作系统或应用程序使用的物理内存总量`
+ **buff** : `系统缓冲区大小(单位为KB)，或用于存放等待保存到硬盘的数据的内存大小(单位为KB)`。该存储区允许`应用程序向Linux内核发出写调用后立即继续执行`(而不是等待直到数据被提交到硬盘)
+ **cache** :用于保存之前`从硬盘读取的数据的系统高速缓存或内存的大小(单位为KB)`。如果应用程序再次需要该数据，内核可以从内存而非硬盘抓取数据，由此可提高性能

**vmstat显示活跃与非活跃页面的数量信息。**

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat -a
procs -----------memory---------- ---swap-- -----io---- -system-- ------cpu-----
 r  b   swpd   free  inact active   si   so    bi    bo   in   cs us sy id wa st
 0  0      0 23225336 1987728 6884184    0    0    31   145  267  282  1  1 98  0  0
```
|列|说明(Kb)|
|--|--|
|active| 被使用的`活跃内存量`。活跃/不活跃的统计数据与缓冲区/高速缓存的是正交的；缓冲区和高速缓存可以是活跃的，也可以是不活跃的|
|inactive|`不活跃的内存总量`(单位为KB)，或一段时间未被使用，适合交换到硬盘的内存量|
|si|上一次采样中，从硬盘进来的内存交换速率(单位为KB/s)|
|so|上一次采样中，到硬盘去的内存交换速率(单位为KB/s)|


```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat -s
     32931532 K total memory
      8498760 K used memory
      8329368 K active memory
       497280 K inactive memory
     23470020 K free memory
         3104 K buffer memory
       959648 K swap cache
     10485756 K total swap
            0 K used swap
     10485756 K free swap
         8162 non-nice user cpu ticks
            0 nice user cpu ticks
        10277 system cpu ticks
       220249 idle cpu ticks
          653 IO-wait cpu ticks
            0 IRQ cpu ticks
          435 softirq cpu ticks
            0 stolen cpu ticks
       682275 pages paged in
        83668 pages paged out
            0 pages swapped in
            0 pages swapped out
       907353 interrupts
       828939 CPU context switches
   1658547198 boot time
         9260 forks
```
|列|说明|
|--|--|
|pages paged in |从硬盘读人系统缓冲区的内存总量(单位为页)|
|pages paged out |从系统高速缓存写到硬盘的内存总量(单位为页)|
|pages swapped in |从交换分区读入系统内存的内存总量(单位为页)|
|pages swapped out| 从系统内存写到交换分区的内存总量(单位为页)|
|used swap |Linux内核目前使用的交换分区容量|
|free swap |当前可用的交换分区容量|
|total swap|系统的交换分区总量，即used swap与free swap之和|

vmstat可以提供关于Linux内核如何分配其内存的信息。如前所述，Linux内核有一系列`“分片”`来保存其动态数据结构。

vmstat显示每一个`分片(Cache)`，展示使用了多少`元素(Num)`，分配了多少`(Total)`，每个元素的`大小(Size)`，整个分片使用了`多少内存页(Pages)`。这些信息有助于跟踪内核究竟是怎样使用其内存的。



```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vmstat -m
Cache                       Num  Total   Size  Pages
nf_conntrack_ffff8807fbe22880    153    153    320     51
nf_conntrack_ffff8807f2650000    306    306    320     51
nf_conntrack_ffffffff81ad9d40    306    306    320     51
xfs_dqtrx                     0      0    528     62
xfs_icr                       0      0    152     53
xfs_inode                 37298  37298    960     34
xfs_efd_item                760    800    408     40
.............
┌──[root@liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /proc/slabinfo
slabinfo - version: 2.1
# name            <active_objs> <num_objs> <objsize> <objperslab> <pagesperslab> : tunables <limit> <batchcount> <sharedfactor> : slabdata <active_slabs> <num_slabs> <sharedavail>
nf_conntrack_ffff8807fbe22880    153    153    320   51    4 : tunables    0    0    0 : slabdata      3      3      0
nf_conntrack_ffff8807f2650000    306    306    320   51    4 : tunables    0    0    0 : slabdata      6      6      0
nf_conntrack_ffffffff81ad9d40    306    306    320   51    4 : tunables    0    0    0 : slabdata      6      6      0
xfs_dqtrx              0      0    528   62    8 : tunables    0    0    0 : slabdata      0      0      0
xfs_icr                0      0    152   53    2 : tunables    0    0    0 : slabdata      0      0      0
xfs_inode          37298  37298    960   34    8 : tunables    0    0    0 : slabdata   1097   1097      0
xfs_efd_item         760    800    408   40    4 : tunables    0    0    0 : slabdata     20     20      0
xfs_buf_item         408    408    240   68    4 : tunables    0    0    0 : slabdata      6      6      0
xfs_btree_cur        234    234    208   39    2 : tunables    0    0    0 : slabdata      6      6      0
xfs_log_ticket       352    352    184   44    2 : tunables    0    0    0 : slabdata      8      8      0
bio-1                408    408    320   51    4 : tunables    0    0    0 : slabdata      8      8      0
ip6_dst_cache        252    252    448   36    4 : tunables    0    0    0 : slabdata      7      7      0
RAWv6                624    624   1216   26    8 : tunables    0    0    0 : slabdata     24     24      0
...........

```





### top(2.x和3.x)

默认情况下，top展示的是对进程的CPU消耗量进行降序排列的列表，但它也可以调整为按内存使用总量排序，以便你能跟踪到哪个进程使用的内存最多。



学习版本
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ top -v
  procps-ng version 3.3.10
Usage:
  top -hv | -bcHiOSs -d secs -n max -u|U user -p pid(s) -o field -w [cols]
```


top运行时切换项

| 选项|说明|
|--|--|
|m|该项切换是否将内存使用量信息显示到屏幕|
|M|按任务使用的内存量排序。由于分配给进程的内存量可能会大于其使用量，因此，该项按驻留集大小排序。驻留集大小是指进程实际使用量，而不是简单的进程请求量|

```bash
top - 11:48:07 up 14 min,  1 user,  load average: 0.07, 0.10, 0.13
Tasks: 273 total,   2 running, 271 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.0 us,  0.0 sy,  0.0 ni,100.0 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem : 32931532 total, 23319380 free,  8643596 used,   968556 buff/cache
KiB Swap: 10485756 total, 10485756 free,        0 used. 23759444 avail Mem

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
   889 etcd      20   0 10.559g  29332  11008 S   1.3  0.1   0:10.94 etcd
  8089 nginx     20   0   41636  12148   1588 S   1.3  0.0   0:03.35 redis-server
```
|选项|说明|
|--|--|
|%MEM|进程使用内存量占系统物理内存的百分比|
|VIRT(v3.x)/SIME(v2.x)|进程虚拟内存使用总量。其中包括了应用程序分配到但未使用的全部内存|
|SWAP|进程使用的交换区(单位为KB)总量|
|RSS(v2.x)/RES(v3.x)|应用程序实际使用的物理内存总量|
|SHARE(v 2.x)/SHR(V 3.x) |可与其他进程共享的内存总量(单位为KB)|
|Mem:total，used，free |对物理内存来说，该项表示的是其总量、使用量和空闲量|
|swap:total，used，free |对交换分区来说，该项表示的是其总量、使用量和空闲量|
|buff/cache |用于缓冲区写人硬盘的数值和缓存的物理内存总量(单位为KB)|

```bash
top - 11:47:42 up 14 min,  1 user,  load average: 0.11, 0.11, 0.13
Tasks: 273 total,   1 running, 272 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.1 us,  0.2 sy,  0.0 ni, 99.6 id,  0.0 wa,  0.0 hi,  0.1 si,  0.0 st
KiB Mem : 27.9/32931532 [|||||||||||||||||||||                                                      ]
KiB Swap:  0.0/10485756 [                                                                           ]

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
   889 etcd      20   0 10.559g  29332  11008 S   1.0  0.1   0:10.70 etcd
  8073 992       20   0  762040 109952  11972 S   1.0  0.3   0:06.59 prometheus
  8079 chrony    20   0  860404 575904   9968 S   1.0  1.7   0:30.24 bundle
  8082 992       20   0  542292  15620   5476 S   0.3  0.0   0:00.77 node_exporter
  8089 nginx     20   0   41636  12148   1588 S   0.3  0.0   0:03.26 redis-server
  8093 chrony    20   0  305636  30176   4964 S   0.3  0.1   0:03.33 gitlab-mon
  8099 nginx     20   0  398856  11900   3784 S   0.3  0.0   0:00.53 redis_exporter
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/6711fb4848b14535a37debc9dddf3396.png)


### free


free提供的是系统使用内存的总体情况，包括空闲内存量。

`free [-l][·t][-s delay 1[-c count]`

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free -V
free from procps-ng 3.3.10
┌──[root@liruilongs.github.io]-[~]
└─$ free --help

Usage:
 free [options]

Options:
 -b, --bytes         show output in bytes
 -k, --kilo          show output in kilobytes
 -m, --mega          show output in megabytes
 -g, --giga          show output in gigabytes
     --tera          show output in terabytes
     --peta          show output in petabytes
 -h, --human         show human-readable output
     --si            use powers of 1000 not 1024
 -l, --lohi          show detailed low and high memory statistics
 -t, --total         show total for RAM + swap
 -s N, --seconds N   repeat printing every N seconds
 -c N, --count N     repeat printing N times, then exit
 -w, --wide          wide output

     --help     display this help and exit
 -V, --version  output version information and exit

For more details see free(1).
```
常用命令
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free
              total        used        free      shared  buff/cache   available
Mem:       32931532     8820980    22388436       27388     1722116    23579356
Swap:      10485756           0    10485756
┌──[root@liruilongs.github.io]-[~]
└─$ free -h
              total        used        free      shared  buff/cache   available
Mem:            31G        8.4G         21G         26M        1.6G         22G
Swap:            9G          0B          9G
```
|统计信息|说明|
|--|--|
|Total|物理内存与交换空间的总量|
|Used|使用的物理内存和交换分区的容量|
|Free|未使用的物理内存和交换分区的容量|
|shared|进程共享内存使用量，该项已过时，应忽略|
|Buffers|用作`硬盘写缓冲区`的物理内存的容量|
|Cached|用作`硬盘读缓存`的物理内存的容量|
|High|`高端内存`或`不能被内核直接访问`的内存总量|
|Low|`低端内存`或`能被内核直接访问`的内存总量|
|Totals|对Total、Used和Free列，该项显示的是该列中物理内存和交换分区的总和|


`-l `向你展示使用了多少高端内存和多少低端内存

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free -l
              total        used        free      shared  buff/cache   available
Mem:       32931532     4835004    24441680       35596     3654848    27657976
Low:       32931532     8489852    24441680
High:             0           0           0
Swap:      10485756           0    10485756
```
`-t  `命令可以查看内存的统计信息
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free -t
              total        used        free      shared  buff/cache   available
Mem:       32931532     5005552    24808440       35588     3117540    27487560
Swap:      10485756           0    10485756
Total:     43417288     5005552    35294196
```
选项说明
+ `-s delay `:使free按每delay秒的间隔输出新的内存统计数据
+ `-c count `:使free 输出count次新的统计数据

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ free -s 2 -c 2
              total        used        free      shared  buff/cache   available
Mem:       32931532     5405500    24408372       35588     3117660    27087612
Swap:      10485756           0    10485756

              total        used        free      shared  buff/cache   available
Mem:       32931532     5405460    24408388       35588     3117684    27087652
Swap:      10485756           0    10485756
┌──[root@liruilongs.github.io]-[~]
└─$
```
### slabtop

`slabtop实时显示内核是如何分配其各种缓存的`，以及这些缓存的被占用情况。在内部，内核有一系列的缓存，它们由一个或多个`分片(slab)`构成。每个分片包括一组对象，对象个数为一个或多个。

这些对象可以是活跃的(使用的)或非活跃的(未使用的)。slabtop向你展示的是不同分片的状况。它显示了这些分片的被占用情况，以及它们使用了多少内存。

slabtop可以一窥Linux内核的数据结构。每一种分片类型都与Linux内核紧密相关。如果某个特定分片使用了大量的内核内存，那么阅读Linux内核源代码和搜索互联网是找出这些分片用在哪里的最好的两种方法。
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ slabtop --help

Usage:
 slabtop [options]

Options:
 -d, --delay <secs>  delay updates
 -o, --once          only display once, then exit
 -s, --sort <char>   specify sort criteria by character (see below)

 -h, --help     display this help and exit
 -V, --version  output version information and exit

The following are valid sort criteria:
 a: sort by number of active objects
 b: sort by objects per slab
 c: sort by cache size
 l: sort by number of slabs
 v: sort by number of active slabs
 n: sort by name
 o: sort by number of objects (the default)
 p: sort by pages per slab
 s: sort by object size
 u: sort by cache utilization

For more details see slabtop(1).
```


```bash
 Active / Total Objects (% used)    : 2172003 / 2195023 (99.0%)
 Active / Total Slabs (% used)      : 49649 / 49649 (100.0%)
 Active / Total Caches (% used)     : 69 / 95 (72.6%)
 Active / Total Size (% used)       : 466189.06K / 477588.18K (97.6%)
 Minimum / Average / Maximum Object : 0.01K / 0.22K / 8.00K

  OBJS ACTIVE  USE OBJ SIZE  SLABS OBJ/SLAB CACHE SIZE NAME
727440 727339  99%    0.19K  17320       42    138560K dentry
571038 570600  99%    0.10K  14642       39     58568K buffer_head
305088 304062  99%    0.06K   4767       64     19068K kmalloc-64
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ slabtop -d 2 -s c
```
### sar(Ⅱ)

`sar [-B][-rl[-R]`

|选项|描述|
|--|--|
|-B|报告的信息为内核与磁盘之间交换的块数。此外，对v2.5之后的内核版本，该项报告的信息为缺页数量|
|-W|报告的是系统交换的页数|
|-r|报告系统使用的内存信息。它包括总的空闲内存、正在使用的交换分区、缓存和缓冲区的信息|

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -W
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年07月23日  _x86_64_        (6 CPU)

11时33分39秒       LINUX RESTART

11时40分01秒  pswpin/s pswpout/s
11时50分01秒      0.00      0.00
12时00分01秒      0.00      0.00
12时10分01秒      0.00      0.00
12时20分01秒      0.00      0.00
平均时间:      0.00      0.00
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -B
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年07月23日  _x86_64_        (6 CPU)

11时33分39秒       LINUX RESTART

11时40分01秒  pgpgin/s pgpgout/s   fault/s  majflt/s  pgfree/s pgscank/s pgscand/s pgsteal/s    %vmeff
11时50分01秒     12.52    125.74    677.40      0.02    692.72      0.00      0.00      0.00      0.00
12时00分01秒    138.94    533.55   1253.45      0.17   1110.67      0.00      0.00      0.00      0.00
12时10分01秒    118.52   5527.63  35379.48      0.20  19923.40      0.00      0.00      0.00      0.00
12时20分01秒     15.85    122.99   3750.36      0.02   1989.95      0.00      0.00      0.00      0.00
平均时间:     71.46   1578.02  10268.63      0.10   5931.11      0.00      0.00      0.00      0.00
┌──[root@liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -r 1 3
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年07月23日  _x86_64_        (6 CPU)

12时39分27秒 kbmemfree kbmemused  %memused kbbuffers  kbcached  kbcommit   %commit  kbactive   kbinact   kbdirty
12时39分28秒  23253572   9677960     29.39      3104   2789588   5138464     11.84   6864288   1980388        96
12时39分29秒  23253572   9677960     29.39      3104   2789620   5138464     11.84   6864300   1980416        96
12时39分30秒  23253572   9677960     29.39      3104   2789620   5138464     11.84   6864300   1980416        96
平均时间:  23253572   9677960     29.39      3104   2789609   5138464     11.84   6864296   1980407        96
```

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sar -R 1 3
Linux 3.10.0-693.el7.x86_64 (liruilongs.github.io)      2022年07月23日  _x86_64_        (6 CPU)

12时39分44秒   frmpg/s   bufpg/s   campg/s
12时39分45秒    -96.00      0.00      6.00
12时39分46秒     29.00      0.00      0.00
12时39分47秒    -33.00      0.00      0.00
平均时间:    -33.33      0.00      2.00
┌──[root@liruilongs.github.io]-[~]
└─$

```
### /proc/meminfo


Linux内核提供用户可读文本文件`/proc/meminfo`来显示当前系统范围内的内存性能统计信息，

它提供了系统范围内`内存统计数据`的超集，包括了`vmstat、top、free和procinfo`的信息，但是使用起来有一定的难度。如果你想定期更新，就需要自己写一个脚本或一些代码来实现这个功能。如果你想保存内存性能信息或是将其与CPU统计信息相协调，就必须创建一个新的工具或是写一个脚本。


```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /proc/meminfo
MemTotal:       32931532 kB
MemFree:        23182352 kB
MemAvailable:   25878308 kB
Buffers:            3104 kB
Cached:          2790760 kB
SwapCached:            0 kB
Active:          6933748 kB
Inactive:        1981384 kB
Active(anon):    6124200 kB
Inactive(anon):    32624 kB
Active(file):     809548 kB
Inactive(file):  1948760 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:      10485756 kB
SwapFree:       10485756 kB
Dirty:                84 kB
Writeback:             0 kB
AnonPages:       6121244 kB
Mapped:           188304 kB
Shmem:             35556 kB
Slab:             481624 kB
SReclaimable:     340480 kB
SUnreclaim:       141144 kB
KernelStack:       12944 kB
PageTables:        31076 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    26951520 kB
Committed_AS:    5158652 kB
VmallocTotal:   34359738367 kB
VmallocUsed:      225072 kB
VmallocChunk:   34359310332 kB
HardwareCorrupted:     0 kB
AnonHugePages:   5437440 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
DirectMap4k:      161600 kB
DirectMap2M:     7178240 kB
DirectMap1G:    28311552 kB
```
部分参数描述
+ SwapTotal:可用的总交换空间
+ SwapFree:剩余可用交换空间
+ Dirty:等待写回磁盘的内存
+ Writeback:正在主动写回磁盘的内存
+ AnonPages:映射到用户空间页表的非文件支持页
+ Mapped:已映射的文件，例如库
+ Slab:内核数据结构缓存
+ PageTables:专用于最低级别页表的内存量。如果很多进程连接到同一个共享内存段，这可能会增加到一个很高的值。
+ NFS_Unstable:NFS 页面发送到服务器，但尚未提交到存储
+ Bounce:用于块设备的内存bounce buffers
+ CommitLimit:根据过量使用率 ( vm.overcommit_ratio)，这是系统上当前可分配的内存总量。仅当启用了严格的过量使用记帐(模式 2 in )时才遵守此限制vm.overcommit_memory。
+ Committed_AS:当前在系统上分配的内存量。提交的内存是进程分配的所有内存的总和，即使它还没有被它们“使用”。
+ VmallocTotal:vmalloc 内存区域的总大小
+ VmallocUsed:使用的vmalloc区域的数量
+ VmallocChunk:vmalloc 区域的最大连续块，它是空闲的
+ HugePages_Total:内核分配的大页数(用 定义vm.nr_hugepages)
+ HugePages_Free:进程未分配的大页数
+ HugePages_Rsvd:已承诺从池中分配但尚未分配的大页数。
+ Hugepagesize:a 的大小hugepage(在基于 Intel 的系统上通常为 2MB)


