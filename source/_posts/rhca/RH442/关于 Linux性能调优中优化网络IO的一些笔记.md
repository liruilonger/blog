---
title: 关于Linux性能调优中网络I/O的一些笔记
tags:
  - Linux
categories:
  - Linux
toc: true
recommend: 1
keywords: java
uniqueId: '2022-05-14 18:17:43/关于Linux性能调优中网络I/O的一些笔记.html'
mathJax: false
date: 2022-05-15 02:17:43
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 和小伙伴分享一些`Linux网络优化`的笔记,内容很浅，可以用作入门
+ 博文内容结合`《Linux性能优化》`读书笔记整理
+ 涉及内容包括
  + 常用的优化工具(`mii-tool`,`ethtool`,`ifconfig`,`ip`,`sar`,`iptraf`,`netstat`)使用Demo及对应的输出解释
  + 具体的调优策略步骤
+ 食用方式：
  + 需要了解`Linux基础`，网络分层相关知识
  + 没有完整的调优Demo，只是提供的一些方向。关于调优工具，你可以学习到
  + 确定系统内以太网设备的`带宽和双工设置`(`mii-tool、ethtool`)。
  + 确定流经每个`以太网接口的网络流量`(`ifconfig、sar、iptraf、netstat`)。
  + 确定流入和流出系统的`IP流量的类型`(`iptraf、netstat`)。
  + 确定流入和流出系统的`每种类型的IP流量`(`iptraf`)。
  + 确定是哪个`应用程序`产生了`IP流量`(`netstat -p`)。
            
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***


学习网络调优工具之前，简单温习一下网络I/O相关的知识


### 涉及名词解释


`带宽(传输速率)`：数据传输的过程中，两个设备之间数据流动的`物理速度`称为`传输速率`，单位为`bps(Bits Per Second,每秒比特数)`，传输速率不是指单位数据流动的速度，而是指单位时间内传输的数据量有多少。`传输速率`有被称之为`带宽(Bandwidth)`,带宽越大网络传输能力就越强。

`吞吐量`:`主机之间的实际的传输速率被称为吞吐量`，单位与带宽相同，吞吐量还衡量CPU处理能力，网络的拥堵程度，报文中数据字段的占有份额。

`网卡`：网络接口卡，也称网络适配器，网卡，LAN卡。联网时必须使用。

`路由器`:在OSI模型的第3层-`网络层面`上连接两个网络(路由寻址，类似送快递)、并对`分组报文进行转发的设备`。涉及`路由控制表`(静态路由，动态路由OSPF，默认路由)

`交换机`:在OSI模型的第2层-`数据链路层`上连接局域网的主要设备,交换机能够根据`以太网帧中目标地址智能的转发数据`,涉及`地址转发表`(通过MAC地址学习记录实际的MAC地址本身)

`包，数据帧，数据报，段，消息`：包为全能性术语，帧表示数据链路层中包的单位，而数据报是IP和UDP 等网络层以上的分层中包的单位，段则表示TCP数据中流的信息，消息是指应用协议中数据的单位。




## 网络I/O介绍

`Linux`和其他主流操作系统中的`网络流量`被抽象(协议分层与OSI参考模型)为一系列的`硬件和软件层次`。在每个分层上，发送端添加首部包装信息，经过路由器，接受端分离首部恢复数据。当然路由器的传递也涉及网络层和链路层的首部分离和添加。简单回顾下

|分层|描述|
|--|--|
|应用层|应用层涉及协议比较多`TELNET,SSH,HTTP,SMTP,POP,SSL/TLS,FTP,MIME,HTML,SNMP,MIB……`,主要负责声明目标地址(请求头)以及写入内容(请求报文)|
|表示层|表示层负责将机器特定的数据格式转化为网络标准的传输格式发送出去|
|会话层|会话层决定采用那种连接方式？同时标记数据包的发送顺序|
|传输层|传输层即进行建立连接或者断开连接，在两个主机之间创建逻辑上的通信连接，确保数据是否到达，没到达重发，保证数据的可靠性，涉及到的协议包括 `TCP,UDP,DCDC`|
|网络层|网络层将数据从发送端主机发送到接收端主机。涉及到的协议包括`ARP,IPv4,IPv6，ICMP(ping),IPsec`等|
|链路层| 包括`以太网，无线LAN,PPP`,数据链路层负责实现每一个区间内的通信。通信实际上是通过物理的传输介质实现的，数据链路层在传输介质互连的设备进行数据处理。|
|物理层|硬件层，物理层将数据的`01转换为电压和脉冲光`传输给物理的传输介质，相互直连的`设备`通过`MAC(Media Access Control,介质访问控制)`实现传输。物理层将`MAC地址信息的首部附加到从网络层转发过来的数据上，将其发送到网络`。|



在OSI分层中,`链路层`包含网络硬件，如`以太网设备`。在传送网路流量时，这一层并`不区分流量类型`，而仅仅以尽可能快的速度发送和接收`数据包(或帧)。`

链路层的上面是`网络层`。使用`互联网协议(IP)`和`网际控制报文协议(ICMP)`在机器间`寻址并路由`数据包。`IP/ICMP`尽其最大努力尝试在机器之间传递数据包，但是它们不能保证数据包是否能真正达到其目的地。

`网络层`的上面是`传输层`，它定义了`传输控制协议(TCP)和用户数据报协议(UDP)`。

+ `TCP`是一个可靠协议，它可以保证消息通过网络送达，如果消息无法送达它就会产生一个错误。
+ `TCP`的同级协议UDP，则是一个不可靠协议，它无法保证信息能够送达(为了获得最高的数据传输速率)。

UDP和TCP为IP增加了`服务`的概念。UDP和TCP接收有编号`端口`的消息。按照惯例，每个类型的网络服务都被分配了不同的编号即端口。

+ `超文本传输协议(HTTP)`通常为`端口80`
+ `安全外壳(SSH)`通常为`端口22`
+ `文件传输协议(FTP)`通常为`端口23`。


```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$cat  /etc/services | grep -E "^http\\s|^ssh\\s|^ftp\\s" | sort
ftp             21/sctp                 # FTP
ftp             21/tcp
ftp             21/udp          fsp fspd
http            80/sctp                         # HyperText Transfer Protocol
http            80/tcp          www www-http    # WorldWideWeb HTTP
http            80/udp          www www-http    # HyperText Transfer Protocol
ssh             22/sctp                 # SSH
ssh             22/tcp                          # The Secure Shell (SSH) Protocol
ssh             22/udp                          # The Secure Shell (SSH) Protocol
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

在`Linux`系统中，文件`/etc/services`定义了`全部的端口以及它们提供的服务类型`。


传输层层上面为应用层。这一层包含了各种应用程序，它们使用下面各层在网络上传输数据包。


`Linux内核实现或控制`的是`最低三层(链路层、网络层和传输层)`。内核可以提供每层的`性能统计信息`，包括`数据流经每一层时的带宽`使用情况信息和`错误`计数信息。


### 链路层的网络流量

`Linux`可以侦测到`流经链路层`的数据流量的速率。

链路层，通常是`以太网`，以`帧序列`的形式将信息发送到网络上。不管应用层的交互方式是什么，链路层也会将它们分割为`帧`，再发送到网络上。`数据帧的最大尺寸`被称为`最大传输单位(MTU)`。可以使用网络配置工具，如`ip或ifconfig`来设置`MTU`。

k8s集群机器所有有好多Calico虚拟网卡

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$ifconfig | grep mtu
cali12cf25006b5: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1480
cali13a4549bf1e: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1480
cali45e02b0b21e: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1480
cali5a282a7bbb0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1480
cali86e7ca9e9c2: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1480
calicb34164ec79: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1480
docker0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
tunl0: flags=193<UP,RUNNING,NOARP>  mtu 1480
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

就以太网而言，最大大小一般为`1500字节`，虽然有些硬件支持的`巨型帧`可以高达`9000字节`。`MTU`的大小对网络效率有直接影响。

链路层上的每一个帧都有一个小容量的头部，因此，使用`大尺寸的MTU`就提高了用户数据对开销(头部)的比例。但是，使用大尺寸的MTU，每个数据帧被损坏或丢弃的几率会更高。

对单一物理链路来说，大尺寸MTU通常会带来更好的性能，因为它需要的开销更小；反之，对嘈杂的链路(链路聚合)来说，更小的MTU则通常会提升性能，因为，当单个帧被损坏时，它要重传的数据更少。


在物理层，帧流经物理网络，`Linux内核`可以收集大量有关`帧数量和类型的不同统计数据`：

+ `发送/接收`：如果一个帧成功地流出或流入机器，那么它就会被计为一个已发送或已接收的帧。
+ `错误`:有错误的帧(可能是因为网络电缆坏了，或双工不匹配)。
+ `丢弃`:被丢弃帧的(很可能是因为内存或缓冲区容量小)。
+ `溢出`:由于内核或网卡有过多的帧，因此被网络丢弃的帧。通常这种情况不应该发生。
+ `帧`:由于物理级问题导致被丢弃的帧。其原因可能是循环冗余校验(CRC)错误或其他低级别的问题(这个分类有些搞不懂?)。
+ `多播`: 这些帧不直接寻址到当前系统，而是同时广播到一组节点。
+ `压缩`:一些底层接口，如`点对点协议(PPP)`或`串行线路网际协议(SLIP)设备`在把帧发送到网络上之前，会对其进行压缩。该值表示的就是被压缩帧的数量。


有些Linux网络性能工具能够显示通过每一个`网络设备`的每一种`类型的帧数`。这些工具通常需要`设备名`，因此，熟悉Linux如何对`网络设备命名`以便搞清楚哪个名字代表了哪个设备是很重要的。


`以太网设备`被命名为`ethN`，正常，`eth0`指的是第一个设备，`ethl`指的是第二个设备，以此类推。与以太网设备命名方式相同，PPP设备被命名为`pppN`。环回设备，用于与本机联网，被命名为`lo`。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ifconfig lo
lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1  (Local Loopback)
        RX packets 10250705  bytes 2227288333 (2.0 GiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 10250705  bytes 2227288333 (2.0 GiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```


### 协议层(传输层)网络流量

对TCP或UDP流量而言，Linux使用套接字/端口来抽象两台机器的连接。当与远程机器连接时，本地应用程序用一个网络套接字来打开远程机器上的一个端口。

Linux网络性能工具可以跟踪流经特定网络端口的数据量。由于每个服务的端口号具有唯一性，因此有可能确定流向特定服务的物理流量。

## 网络性能工具

### mii-tool(媒体无关接口工具)

`mii-tool`是以太网专用硬件工具，主要用于设置以太网设备，但它也可以提供有关当前设置的信息。诸如`链接速度和双工设置`，对于追踪性能不佳设备的成因是非常有用。

`mii-tool`已经过时了，推荐使用`ethtool`,一般也不会怎么使用，书里有讲，我们简单看下 
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$man mii-tool  | grep obsolete
       This program is obsolete. For replacement check ethtool.
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

我这里是CentOS 7虚机，所以网卡为`eth32` 系统上eth32的配置信息。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$mii-tool -v ens32
ens32: negotiated 1000baseT-FD flow-control, link ok
  product info: Yukon 88E1011 rev 3
  basic mode:   autonegotiation enabled
  basic status: autonegotiation complete, link ok
  capabilities: 1000baseT-FD 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD
  advertising:  1000baseT-FD 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD
  link partner: 1000baseT-HD 1000baseT-FD 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD
```
`mi-tool`提供了关于如何配置以太网设备物理层的底层信息。

### ethtool

在配置和显示以太网设备统计数据方面，`ethtool`提供了与`mii-tool`相似的功能。不过，`ethtool`更加强大，包含了更多配置选项和设备统计信息。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ethtool  ens32
Settings for ens32:
        Supported ports: [ TP ]
        Supported link modes:   10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Supported pause frame use: No
        Supports auto-negotiation: Yes
        Advertised link modes:  10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Advertised pause frame use: No
        Advertised auto-negotiation: Yes
        Speed: 1000Mb/s
        Duplex: Full
        Port: Twisted Pair
        PHYAD: 0
        Transceiver: internal
        Auto-negotiation: on
        MDI-X: off (auto)
        Supports Wake-on: d
        Wake-on: d
        Current message level: 0x00000007 (7)
                               drv probe link
        Link detected: yes
```
通过上面的配置我们可以看大，带宽为 `Speed: 1000Mb/s` 千兆,双工模式为  `Duplex: Full` 全双工 ,网卡是否连接网线：`Link detected: yes`


### ifconfig(接口配置)

ifconfig的主要工作就是在Linux机器上安装和配置网络接口。它还提供了系统中所有网络设备的基本性能统计信息。ifconfig几乎在所有联网的Linux机器上都是可用的。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ifconfig  ens32
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.81  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:fead:e393  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:ad:e3:93  txqueuelen 1000  (Ethernet)
        RX packets 507331  bytes 69923393 (66.6 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 556567  bytes 308574743 (294.2 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```
关于部分参数的说明

|列|说明|
|--|--|
|RX packets |设备已接收的数据包数|
|TX packets |设备已发送的数据包数|
|errors     |发送或接收时的错误数|
|dropped    |发送或接收时丢弃的数据包数|
|overruns   |网络设备没有足够的缓冲区来发送或接收一个数据包的次数|
|frame      |底层以太网帧错误的数量|
|carrier    |由于链路介质故障(如故障电缆)而丢弃的数据包数量|

`ifconfig`提供的统计数据显示的是自系统启动开始的累计数值。如果你将一个网络设备下线，之后又让其上线，其统计数据也不会重置。如果你按规律的间隔来运行ifconfig，就可以发现各种统计数据的变化率。这一点可以通过`watch命令或shell脚本`来自动实现，

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$watch -d -n 1 ifconfig ens32
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/cbe570fd19624a2da4b82d7fef6fe3f5.png)


### ip

一些网络工具，如ifconfig，正在被淘汰，取而代之的是新的命令：ip，ip不仅可以让你对Linux联网的多个不同方面进行配置，还可以显示每个网络设备的性能统计信息。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$ip -s -s link ls ens32
2: ens32: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT qlen 1000
    link/ether 00:0c:29:ad:e3:93 brd ff:ff:ff:ff:ff:ff
    RX: bytes  packets  errors  dropped overrun mcast
    127977457  756138   0       0       0       0
    RX errors: length   crc     frame   fifo    missed
               0        0       0       0       0
    TX: bytes  packets  errors  dropped carrier collsns
    438259866  814226   0       0       0       0
    TX errors: aborted  fifo   window heartbeat transns
               0        0       0       0       8
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
部分字段说明，RX代表接收，TX代表发送。

|列|说明|
|--|--|
|bytes     |发送或接收的字节数|
|packets   |发送或接收的数据包数|
|errors    |发送或接收时发生的错误数|
|dropped   |由于网卡缺少资源，导致未发送或接收的数据包数|
|overruns  |网络没有足够的缓冲区空间来发送或接收更多数据包的次数|
|mcast     |已接收的多播数据包的数量|
|carrier   |由于链路介质故障(如故障电缆)而丢弃的数据包数量|
|collsns   |传送时设备发生的冲突次数。当多个设备试图同时使用网络时就会发生冲突|

###  sar
sar提供了链路级的网络性能数据。但是，它同时还提供了一些关于传输层打开的套接字数量的基本信息。sar使用如下命令行来收集网络统计信息：
```
sar[-n DEV | EDEV | SOCK | FULL ] [DEVICE] [linterval][count]
```

|选项|说明|
|--|--|
|-n DEV |显示每个设备发送和接收的数据包数和字节数信息|
|-n EDEV|显示每个设备的发送和接收错误信息|
|-n SoCK|显示使用套接字(TCP、UDP和RAW)的总数信息|
|-n FULL|显示所有的网络统计信息|
|interval|采样间隔时长|
|count   |采样总数|

**显示每个设备发送和接收的数据包数和字节数信息**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$sar -n DEV 1 1
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        2022年05月14日  _x86_64_        (2 CPU)

22时46分16秒     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
22时46分17秒     ens32      1.00      1.00      0.11      0.09      0.00      0.00      0.00
22时46分17秒 cali86e7ca9e9c2      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒 cali13a4549bf1e      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒 cali5a282a7bbb0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒 cali12cf25006b5      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒 cali45e02b0b21e      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒        lo    224.00    224.00     27.57     27.57      0.00      0.00      0.00
22时46分17秒 calicb34164ec79      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒     tunl0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时46分17秒   docker0      0.00      0.00      0.00      0.00      0.00      0.00      0.00

平均时间:     IFACE   rxpck/s   txpck/s    rxkB/s    txkB/s   rxcmp/s   txcmp/s  rxmcst/s
平均时间:     ens32      1.00      1.00      0.11      0.09      0.00      0.00      0.00
平均时间: cali86e7ca9e9c2      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali13a4549bf1e      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali5a282a7bbb0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali12cf25006b5      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali45e02b0b21e      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:        lo    224.00    224.00     27.57     27.57      0.00      0.00      0.00
平均时间: calicb34164ec79      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:     tunl0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:   docker0      0.00      0.00      0.00      0.00      0.00      0.00      0.00
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
|列|说明|
|--|--|
|rxpck/s |数据包接收速率|
|txpck/s |数据包发送速率|
|rxkB/s |kb接收速率|
|txkB/s |kb发送速率|
|rxcmp/s |压缩包接收速率|
|txcmp/s |压缩包发送速率|
|rxmcst/s|多播包接收速率|

**显示每个设备的发送和接收错误信息**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$sar -n EDEV 1 1
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        2022年05月14日  _x86_64_        (2 CPU)

22时53分07秒     IFACE   rxerr/s   txerr/s    coll/s  rxdrop/s  txdrop/s  txcarr/s  rxfram/s  rxfifo/s  txfifo/s
22时53分08秒     ens32      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali86e7ca9e9c2      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali13a4549bf1e      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali5a282a7bbb0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali12cf25006b5      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 cali45e02b0b21e      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒 calicb34164ec79      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒     tunl0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
22时53分08秒   docker0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00

平均时间:     IFACE   rxerr/s   txerr/s    coll/s  rxdrop/s  txdrop/s  txcarr/s  rxfram/s  rxfifo/s  txfifo/s
平均时间:     ens32      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali86e7ca9e9c2      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali13a4549bf1e      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali5a282a7bbb0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali12cf25006b5      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: cali45e02b0b21e      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:        lo      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间: calicb34164ec79      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:     tunl0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
平均时间:   docker0      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00      0.00
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

|列|说明|
|--|--|
|rxerr/s  |接收错误率|
|txerr/s  |发送错误率|
|co11/s   |发送时的以太网冲突率|
|rxdrop/s |由于Linux内核缓冲区不足而导致的接收帧丢弃率|
|txdrop/s |由于Linux内核缓冲区不足而导致的发送帧丢弃率|
|txcarr/s |由于载波错误而导致的发送帧丢弃率|
|rxfram/s |由于帧对齐错误而导致的接收帧丢弃率|
|rxfifo/s |由于FIFO错误而导致的接收帧丢弃率|
|txfifo/s |由于FIFO错误而导致的发送帧丢弃率|

**显示使用套接字(TCP、UDP和RAW)的总数信息**

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$sar -n SOCK 1 3
Linux 3.10.0-693.el7.x86_64 (vms81.liruilongs.github.io)        2022年05月14日  _x86_64_        (2 CPU)

22时56分23秒    totsck    tcpsck    udpsck    rawsck   ip-frag    tcp-tw
22时56分24秒      3487       245         9         0         0       163
22时56分25秒      3487       245         9         0         0       165
22时56分26秒      3487       245         9         0         0       167
平均时间:      3487       245         9         0         0       165
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

|列|说明|
|--|--|
|totsck    |当前正在被使用的套接字总数|
|tcpsck    |当前正在被使用的TCP套接字总数|
|udpsck    |当前正在被使用的UDP套接字总数|
|rawsck    |当前正在被使用的RAW套接字总数|
|ip-frag   |IP分片的总数|


###  iptraf

`iptraf`是一个`实时网络监控工具`。它提供了相当多的模式来监控网络接口和流量。`iptraf`是一种控制台应用程序，但其用户界面则是基于光标的一组菜单和窗口。

`iptraf`可以提供有关`每个网络设备发送帧速率的信息`。同时，它还能够显示`TCP/IP数据包的类型和大小信息`，以及·。

需要装包
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$yum -y install iptraf
................
Running transaction
  正在安装    : iptraf-ng-1.1.4-7.el7.x86_64                                                               1/1
  验证中      : iptraf-ng-1.1.4-7.el7.x86_64                                                               1/1

已安装:
  iptraf-ng.x86_64 0:1.1.4-7.el7

完毕！
```
`iptraf`用如下命令行调用：

`iptraf[-d interface][-s interface][-t <minutes>]`

如果调用`iptraf`时不带参数，就会显示一个菜单，让你选择监控界面以及想要监控的信息类型。这些选项用于观察特定接口或网络服务上的网络流量。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/c7d4381b389449ac87e056dae9d26084.png)

观察所有接口的网络流量信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/af5ab4e9a28b4aadb702878a8ae1b1b0.png)

|选项|说明|
|--|--|
|-d interface|接口的详细统计信息，包括：接收信息、发送信息以及错误率信息|
|-s interface|关于接口上哪些IP端口正在被使用，以及有多少字节流经它们的统计信息|
|-t<minutes>|iptraf退出前运行的分钟数|

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng -d ens32 -t 1
```
这条命令指定iptraf显示以太网设备ets32 的详细信息并在运行1分钟后退出。我们可以看到，当前网络设备接收速率为6.13kbps，发送速率为42.81kbps

![在这里插入图片描述](https://img-blog.csdnimg.cn/2e936d8771f641e1b9222634fb892c5a.png)

`iptraf`显示每个`UDP和TCP`端口上的网络流量信息。通过端口我们可以看到每个端口对应的服务处理了多少流量，下图我们可以看到，有278kb的流量被22端口接收，有362kb的ssh数据从当前网卡发送出去

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng -s ens32 -t 10
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/9ce45a73f038434b9ce5668b2decc6c4.png)


### netstat

`netstat`是一种基本的网络性能工具，它几乎出现在每一个联网的Linux机器上(当然我们也可以使用`ss`命令)，可以用它抽取的信息包括：

+ 当前正在使用的网络套接字的数量和类型，
+ 有关流入和流出当前系统的UDP和TCP数据包数量的特定接口统计数据。
+ 能将一个套接字回溯到其特定进程或PID，这在试图确定哪个应用程序要对网络流量负责时是很有用的。


`netstat [-p][-c] [-interfaces=cname>][-s][-t][-u] I-w]`

如果`netstat `调用时不带任何参数，它将显示系统范围内的套接字使用情况以及`Internet域`和`UNIX域套接字`的信息。(UNIX域套接字用于本机的进程通信。)为了能检索所有其可以显示的统计信息，需要从根目录运行`netstat`。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$netstat  | sed -n  '20,30p'
tcp        0      0 localhost:2379          localhost:51396         ESTABLISHED
tcp        0      0 localhost:2379          localhost:33432         ESTABLISHED
tcp        0      0 localhost:2379          localhost:33392         ESTABLISHED
tcp        0      0 localhost:35008         localhost:9099          TIME_WAIT
tcp        0      0 localhost:51154         localhost:2379          ESTABLISHED
tcp        0      0 localhost:51390         localhost:2379          ESTABLISHED
tcp        0      0 vms81.liruilongs.:53982 10.96.0.1:https         ESTABLISHED
tcp        0      0 localhost:51266         localhost:2379          ESTABLISHED
tcp        0      0 localhost:51482         localhost:2379          ESTABLISHED
tcp        0      0 localhost:2379          localhost:52920         ESTABLISHED
tcp        0      0 localhost:2379          localhost:traceroute    ESTABLISHED
```

|选项|说明|
|--|--|
|-p     | 给出打开每个被显示套接字的PID/程序名|
|-c      | 每秒持续更新显示信息|
|--interfaces=<name> | 显示指定接口的网络统计信息|
|-statistics/ -s     | IP/UDP/ICMP/TCP统计信息|
|--tcp / -t  |仅显示TCP套接字相关信息|
|--udp / -u  |仅显示UDP套接字相关信息|
|-raw  / -w   |仅显示RAW套接字相关信息(IP和ICMP)|

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$netstat -p  | sed -n  '20,30p'
tcp        0      0 localhost:2379          localhost:51396         ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:33432         ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:33392         ESTABLISHED 2025/etcd
tcp        0      0 localhost:35008         localhost:9099          TIME_WAIT   -
tcp        0      0 localhost:51154         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:51390         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 vms81.liruilongs.:53982 10.96.0.1:https         ESTABLISHED 108260/calico-node
tcp        0      0 localhost:51266         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:51482         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:2379          localhost:52920         ESTABLISHED 2025/etcd
tcp        0      0 localhost:2379          localhost:traceroute    ESTABLISHED 2025/etcd
```

然后我们看一个日常的运维脚本

```bash
function network() {
    #获取网卡流量信息,接收|发送的数据流量,单位为字节(bytes)
    net_monitor=$(cat /proc/net/dev | tail -n +3 | awk 'BEGIN{ print "网卡名称 入站数据流量(bytes) 出站数据流量(bytes)"} {print $1,$2,$10}' | column -t)
    #获取暴露端口信息
    ip_port=$(ss -ntulpa)

     #本地IP地址列表
    localip=$(ip a s | awk '/inet /{print $2}' )


    echo -e "\n\033[32m################## 网络 相关 ############\033[0m\n"
    echo -e "|本地IP地址列表:"
    echo -e "\033[31m$localip    \033[0m"
    echo -e "\033[32m------------------------------------\033[0m"
    echo -e "|获取网卡流量信息:"
    echo -e "\033[31m$net_monitor           \033[0m"
    echo -e "\033[32m------------------------------------\033[0m"
    echo -e "|获取暴露端口信息:"
    echo -e "\033[31m$ip_port           \033[0m"
    echo -e "\033[32m------------------------------------\033[0m"

}
```

![输出截图](https://img-blog.csdnimg.cn/fffee8ae8afc4ea8b1d146132ba42760.png)





## 优化网络IO使用情况

当知道网络发生了问题时，Linux提供了一组工具来确定哪些应用程序涉及其中。但是，在与外部机器连接时，对网络问题的修复就不完全由你控制了。

![在这里插入图片描述](https://img-blog.csdnimg.cn/59ccbb544a9e4d029c0dc446810f8002.png)

### 网络设备发送/接收量接近理论极限了吗？

要做的第一件事就是用`ethtool`来确定每个`Ethernet`设备设置的硬件速度是多少。通过下面的配置文件我们可以看到，设置当前网卡带宽为`1000Mb/s`
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ethtool ens32
Settings for ens32:
        Supported ports: [ TP ]
        Supported link modes:   10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Supported pause frame use: No
        Supports auto-negotiation: Yes
        Advertised link modes:  10baseT/Half 10baseT/Full
                                100baseT/Half 100baseT/Full
                                1000baseT/Full
        Advertised pause frame use: No
        Advertised auto-negotiation: Yes
        Speed: 1000Mb/s
        Duplex: Full
        Port: Twisted Pair
        PHYAD: 0
        Transceiver: internal
        Auto-negotiation: on
        MDI-X: off (auto)
        Supports Wake-on: d
        Wake-on: d
        Current message level: 0x00000007 (7)
                               drv probe link
        Link detected: yes
```
如果有这些信息的记录，就可以调查是否有网络设备处于饱和状态。Ethernet设备和/或交换机容易`被误配置`，`ethtool显示每个设备认为其应运行的速度`。在确定了`每个Ethernet设备的理论极限后`，使用`iptraf(甚至是ifconfig)来明确流经每个接口的流量`。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ifconfig ens32
ens32: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.26.81  netmask 255.255.255.0  broadcast 192.168.26.255
        inet6 fe80::20c:29ff:fead:e393  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:ad:e3:93  txqueuelen 1000  (Ethernet)
        RX packets 628172  bytes 109448643 (104.3 MiB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 674109  bytes 362438519 (345.6 MiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```
如果有任何网络设备表现出饱和，转到下面两个节点。我们可以看到当前网卡接收的数据量为104M,发送的数据量为345M。这里我们可以通过watch监听的方式看计算每秒的流量数据。

也可以使用`iptraf`来实现，下面的命令统计流量的进出速率
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng -d ens32 -t 1
````
![在这里插入图片描述](https://img-blog.csdnimg.cn/71e2f6a1f89543aabf979cbf6481b2c8.png)


### 网络设备产生了大量错误吗？

网络流量减缓的原因也可能是大量的网络错误。用`ifconfig`来确定是否有接口产生了大量的错误。大量错误可能是不匹配的`Ethernet卡/Ethernet交换机`设置的结果。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ifconfig ens32 | grep err
        RX errors 0  dropped 0  overruns 0  frame 0
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

#### 设备上流量的类型是什么？
如果特定设备正在服务大量的数据，使用`iptraf`可以跟踪该设备发送和接收的流量类型。当知道了设备处理的流量类型后，转到下面的节点
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$iptraf-ng -s ens32 -t 10
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/87562c56ba8e4455903f1d0613704ded.png)


#### 特定进程要为流量负责吗？

接下来，我们想要确定是否有特定进程要为这个流量负责。使用`netstat的 -p 选项`来查看是否有进程在处理流经网络端口的类型流量。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$netstat -p | grep 2379
tcp        0      0 localhost:33354         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:33416         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:51498         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:53062         localhost:2379          ESTABLISHED 14196/kube-apiserve
tcp        0      0 localhost:2379          localhost:52520         ESTABLISHED 2025/etcd
```
如果有应用程序要对此负责，转到`[流量是哪个远程系统发送的]`节点。如果没有这样的程序，则转到`[哪个应用程序套接字要为流量负责]`。

#### 流量是哪个远程系统发送的？

如果没有应用程序应对这个流量负责，那么就可能是网络上的某些系统用无用的流量攻击了你的系统。要确定是哪些系统发送了这些流量，要使用`iptraf或etherape`。


如果可能的话，请与系统所有者联系，并尝试找出发生这种情况的原因。如果所有者无法联系上，可以在Linux内核中设置ipfilters，永久丢弃这个特定的流量，或者是在远程机与本地机之间建立防火墙来拦截该流量。


#### 哪个应用程序套接字要为流量负责？

确定使用了哪个套接字要分两步。这部分完全看不懂，先记录下，

+ 第一步，用`strace -e trace=file`跟踪应用程序所有的`I/0系统`调用。这能显示进程是从哪些文件描述符进行读写的。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$yum -y install strace
```
跟踪执行`kubectl get nodes`涉及到的文件读写
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get nodes
NAME                         STATUS     ROLES                  AGE    VERSION
vms81.liruilongs.github.io   Ready      control-plane,master   153d   v1.22.2
vms82.liruilongs.github.io   Ready      <none>                 153d   v1.22.2
vms83.liruilongs.github.io   NotReady   <none>                 153d   v1.22.2
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$strace -e trace=file  kubectl get nodes
execve("/usr/bin/kubectl", ["kubectl", "get", "nodes"], 0x7ffc888b4e40 /* 22 vars */) = 0
openat(AT_FDCWD, "/sys/kernel/mm/transparent_hugepage/hpage_pmd_size", O_RDONLY) = -1 ENOENT (没有那个文件或目 录)
readlinkat(AT_FDCWD, "/proc/self/exe", "/usr/bin/kubectl", 128) = 16
openat(AT_FDCWD, "/usr/bin/kubectl", O_RDONLY|O_CLOEXEC) = 6
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
--- SIGURG {si_signo=SIGURG, si_code=SI_TKILL, si_pid=22013, si_uid=0} ---
openat(AT_FDCWD, "/root/.kube/cache/discovery/192.168.26.81_6443/policy/v1beta1/serverresources.json", O_RDONLY|O_CLOEXEC) = 6
..........
```
+ 第二步，通过查看proc文件系统，将这些文件描述符映射回套接字。`/proc/<pid>/fd/`中的文件是从文件描述符到实际文件或套接字的符号链接。该目录下的`1s-1a`会显示特定进程全部的文件描述符。名字中带有socket的是网络套接字。之后就可以利用这些信息来确定程序中的哪个套接字产生了这些通信。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ps -elF | grep etcd
4 S root       2025   2004  2  80   0 - 2803899 futex_ 96656 1 5月14 ?       00:33:14 etcd --advertise-client-urls=https://192.168.26.81:2379 --cert-file=/etc/kubernetes/pki/etcd/server.crt --client-cert-auth=true --data-dir=/var/lib/etcd --initial-advertise-peer-urls=https://192.168.26.81:2380 --initial-cluster=vms81.liruilongs.github.io=https://192.168.26.81:2380 --key-file=/etc/kubernetes/pki/etcd/server.key --listen-client-urls=https://127.0.0.1:2379,https://192.168.26.81:2379 --listen-metrics-urls=http://127.0.0.1:2381 --listen-peer-urls=https://192.168.26.81:2380 --name=vms81.liruilongs.github.io --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt --peer-client-cert-auth=true --peer-key-file=/etc/kubernetes/pki/etcd/peer.key --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt --snapshot-count=10000 --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
4 S root      14196  14020 10  80   0 - 311578 futex_ 445044 0 5月14 ?       01:54:30 kube-apiserver --advertise-address=192.168.26.81 --allow-privileged=true --token-auth-file=/etc/kubernetes/pki/liruilong.csv --authorization-mode=Node,RBAC --client-ca-file=/etc/kubernetes/pki/ca.crt --enable-admission-plugins=NodeRestriction --enable-bootstrap-token-auth=true --etcd-cafile=/etc/kubernetes/pki/etcd/ca.crt --etcd-certfile=/etc/kubernetes/pki/apiserver-etcd-client.crt --etcd-keyfile=/etc/kubernetes/pki/apiserver-etcd-client.key --etcd-servers=https://127.0.0.1:2379 --kubelet-client-certificate=/etc/kubernetes/pki/apiserver-kubelet-client.crt --kubelet-client-key=/etc/kubernetes/pki/apiserver-kubelet-client.key --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname --proxy-client-cert-file=/etc/kubernetes/pki/front-proxy-client.crt --proxy-client-key-file=/etc/kubernetes/pki/front-proxy-client.key --requestheader-allowed-names=front-proxy-client --requestheader-client-ca-file=/etc/kubernetes/pki/front-proxy-ca.crt --requestheader-extra-headers-prefix=X-Remote-Extra- --requestheader-group-headers=X-Remote-Group --requestheader-username-headers=X-Remote-User --secure-port=6443 --service-account-issuer=https://kubernetes.default.svc.cluster.local --service-account-key-file=/etc/kubernetes/pki/sa.pub --service-account-signing-key-file=/etc/kubernetes/pki/sa.key --service-cluster-ip-range=10.96.0.0/12 --tls-cert-file=/etc/kubernetes/pki/apiserver.crt --tls-private-key-file=/etc/kubernetes/pki/apiserver.key
0 S root      24735  24319  0  80   0 - 28170 pipe_w   980   0 02:08 pts/1    00:00:00 grep --color=auto etcd
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cd  /proc/2025/fd/
┌──[root@vms81.liruilongs.github.io]-[/proc/2025/fd]
└─$ls
0    102  109  116  121  126  131  136  18  22  27  31  36  40  45  5   54  59  64  71  78  83  9   95
1    103  11   117  122  127  132  14   19  23  28  32  37  41  46  50  55  6   65  73  79  84  90  97
.......
```

#### 最后的手段
当你看到这里的时候，你的问题可能得到也可能没有得到解决，但是，你会获取大量描述它的信息。在搜索引擎上看看他们是如何解决问题的。尝试一个解决方案，并观察系统或应用程序的行为是否发生了变化。每次尝试新方案时，请转到流程最开始重新开始系统诊断，因为，每一个修复都可能会让应用程序的行为发生变化。

***

如果涉及到网络配置，也可用个通过`NetworkManager`服务来配置网络相关的管理，网卡方面，可以通过`nmcli dev status`命令用来查看所有网络设备的当前状态。感兴趣小伙伴可以看看我之前的博文

```bash
┌──[root@liruilongs.github.io]-[~]
└─$nmcli dev status
DEVICE      TYPE      STATE      CONNECTION
privbr0     bridge    connected  privbr0
virbr0      bridge    connected  virbr0
eth0        ethernet  connected  eth0
vnet0       tun       connected  vnet0
vnet1       tun       connected  vnet1
lo          loopback  unmanaged  --
virbr0-nic  tun       unmanaged  --
```
通过`show`命令 `nmcli device show eth0`可以查看网卡详细信息
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli device show eth0
GENERAL.DEVICE:                         eth0
GENERAL.TYPE:                           ethernet
GENERAL.HWADDR:                         52:54:00:00:FA:0A
GENERAL.MTU:                            1500
GENERAL.STATE:                          100 (connected)
GENERAL.CONNECTION:                     Wired connection 1
GENERAL.CON-PATH:                       /org/freedesktop/NetworkManager/ActiveConnection/1
WIRED-PROPERTIES.CARRIER:               on
IP4.ADDRESS[1]:                         172.25.250.10/24
IP4.GATEWAY:                            172.25.250.254
IP4.ROUTE[1]:                           dst = 172.25.250.0/24, nh = 0.0.0.0, mt = 100
IP4.ROUTE[2]:                           dst = 0.0.0.0/0, nh = 172.25.250.254, mt = 100
IP4.DNS[1]:                             172.25.250.254
IP6.ADDRESS[1]:                         fe80::984:87d2:dba7:1007/64
IP6.GATEWAY:                            --
IP6.ROUTE[1]:                           dst = fe80::/64, nh = ::, mt = 100
IP6.ROUTE[2]:                           dst = ff00::/8, nh = ::, mt = 256, table=255
┌──[root@servera.lab.example.com]-[~]
└─$

```
相关的命令
```bash
┌──[root@servera.lab.example.com]-[~]
└─$nmcli device
connect     disconnect  lldp        monitor     set         status
delete      help        modify      reapply     show        wifi
```
man 帮助文档
```bash
DEVICE MANAGEMENT COMMANDS
       nmcli device {status | show | set | connect | reapply | modify | disconnect | delete | monitor | wifi
                    | lldp} [ARGUMENTS...]

       Show and manage network interfaces.

       status
           Print status of devices.

           This is the default action if no command is specified to nmcli device.

       show [ifname]
           Show detailed information about devices. Without an argument, all devices are examined. To get
           information for a specific device, the interface name has to be provided.

       set [ifname] ifname [autoconnect {yes | no}] [managed {yes | no}]
           Set device properties.

       connect ifname
           Connect the device. NetworkManager will try to find a suitable connection that will be activated.
           It will also consider connections that are not set to auto connect.

           If no compatible connection exists, a new profile with default settings will be created and
           activated. This differentiates nmcli connection up ifname "$DEVICE" from nmcli device connect
           "$DEVICE"

           If --wait option is not specified, the default timeout will be 90 seconds.

       reapply ifname
           Attempt to update device with changes to the currently active connection made since it was last
           applied.

       modify ifname {option value | [+|-]setting.property value}...
           Modify the settings currently active on the device.

           This command lets you do temporary changes to a configuration active on a particular device. The
           changes are not preserved in the connection profile.

           See nm-settings(5) for the list of available properties. Please note that some properties can t
           be changed on an already connected device.

           You can also use the aliases described in PROPERTY ALIASES section. The syntax is the same as of
           the nmcli connection modify command.

       disconnect ifname...
           Disconnect a device and prevent the device from automatically activating further connections
           without user/manual intervention. Note that disconnecting software devices may mean that the
           devices will disappear.

           If --wait option is not specified, the default timeout will be 10 seconds.

       delete ifname...
           Delete a device. The command removes the interface from the system. Note that this only works for
           software devices like bonds, bridges, teams, etc. Hardware devices (like Ethernet) cannot be
```


