---
title: 关于Linux中前端负载均衡之VIP(LVS+Keepalived)自动化部署的一些笔记
tags:
  - LVS
  - Keepalived
categories:
  - LVS
  - Keepalived
toc: true
recommend: 1
keywords: 负载均衡
uniqueId: '2023-03-23 13:56:14/关于Linux中前端负载均衡之LVS+Keepalived自动化部署的一些笔记.html'
mathJax: false
date: 2023-03-23 21:56:14
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 整理一些 `LVS` 相关的笔记
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***


### LVS & Keepaliveed 简单介绍


关于 `LVS` 是什么，即 Linux 虚拟服务器，是由章文嵩博士主导的开源负载均衡项目，目前 `LVS` 已经被集成到 `Linux` 内核模块中。使用只需要安装一个操作工具就可以。


使用 `ansible` 进行相关的操作 ,下面为清单文件，这里 `master` 作为代理节点,`node ` 作为负载节点。

```ini 
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat inventory
[master]
192.168.26.152
[node]
192.168.26.153
192.168.26.154
```

在使用之前我们需要安装工具包 `ipvsadm`

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible master -m yum -a 'name=ipvsadm state=installed'
```


LVS 负载均衡环境 由一个或多个 `VIP` 加上多个真实服务器构成，多个 `VIP` 的情况需要配合 `Keepalived` 使用。

LVS 工作原理即用户请求 LVS 的 VIP 地址，LVS 根据 `转发方式` 和 `负载均衡算法`，将请求转发给后端服务器。

实现 `LVS` 负载均衡转发方式有三种，分别为 `NAT、DR、TUN` 模式，负载均衡算法相对较多。
+ `NAT`: 即网络地址转换，简单理解，即可以实现两个不通网段的通信。通过 LVS 服务器实现用户私网和公网的通信。
+ `TUN` ：TUN的思路就是将请求与响应数据分离，请求还走 LVS 机器，响应走单独的通道，所以 TUN 模式要求真实服务器可以直接与外部网络连接，真实服务器在收到请求数据包后直接给客户端主机响应数据。
+ `DR`： DR模式也叫直接路由模式，直接路由模式(DR模式)要求LVS与后端服务器必须在同一个局域网内。


LVS负载均衡算法：

+ RR算法：轮叫调度(Round-RobinScheduling) 
+ WRR算法：加权轮叫调度(WeightedRound-RobinScheduling)
+ LC算法：最小连接调度(Least-ConnectionScheduling)
+ WLC算法: 加权最小连接调度(WeightedLeast-ConnectionScheduling)
+ LBLC算法：基于局部性的最少链接(Locality-BasedLeastConnectionsScheduling)
+ LBLCR算法：带复制的基于局部性最少链接(Locality-BasedLeastConnectionswithReplicationScheduling)
+ DH算法：目标地址散列调度(DestinationHashingScheduling)
+ SH算法：源地址散列调度(SourceHashingScheduling)


ipvsadm 常用命令

+ `-A` 增加一台虚拟服务器VIP地址；
+ `-t` 虚拟服务器提供的是tcp服务；
+ `-s` 使用的调度算法；
+ `-a` 在虚拟服务器中增加一台后端真实服务器；
+ `-r` 指定真实服务器地址；
+ `-w` 后端真实服务器的权重；
+ `-m` 设置当前转发方式为NAT模式；
+ `-g` 为直接路由模式；
+ `-i` 模式为隧道模式。


#### 通过命令行方式简单使用

这里利用 ipvsadm 工具搭建简单的 LVS 负载均衡 Demo

"ipvsadm -C" 命令用于清除所有 IPVS 虚拟服务器条目。这意味着所有虚拟服务器及其关联的连接都将被删除。
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ipvsadm -C
```
下面的命令用于将新的虚拟服务添加到 `IP Virtual Server (IPVS) 表` 中。其中:
+ -A 选项指定添加新的虚拟服务，
+ -t 选项指定虚拟服务的 IP 地址和端口号，本例中为 192.168.26.200:80。
+ -s 选项指定虚拟服务使用的调度算法，本例中指定为 rr，即“轮询”。这意味着连接将在 IPVS 表中的真实服务器之间均匀分布。

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ipvsadm -A -t  192.168.26.200:80 -s rr
```

向刚刚创建的虚拟服务添加新的真实的服务器
- -a：此选项指定我们要向虚拟服务添加新的真实服务器。
- -t 192.168.26.200:80：此选项指定要向其添加新的真实服务器的虚拟服务。在本例中，虚拟服务位于 IP 地址 192.168.26.200 和端口 80 上。
- -r 192.168.26.153:80：此选项指定我们要添加到虚拟服务的新真实服务器的 IP 地址和端口。在本例中，新真实服务器的 IP 地址为 192.168.26.153，端口为 80。
- -m：此选项指定我们要为虚拟服务使用 NAT 模式。这意味着当流量转发到真实服务器时，真实服务器的 IP 地址将被转换为负载均衡器的 IP 地址。
- -w 2：此选项指定新真实服务器的权重。权重用于在虚拟服务中分配流量。在本例中，新真实服务器的权重设置为 2。

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ipvsadm -a -t 192.168.26.200:80 -r  192.168.26.153:80 -m -w 2
```

相同的方式在添加一台真实的机器

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ipvsadm -a -t 192.168.26.200:80 -r  192.168.26.154:80 -m -w 2
```
负载节点上启动 httpd 服务
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible node -m service -a 'name=httpd state=started'
```
简单测试，轮询模式返回

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$curl 192.168.26.200:80
vms154.liruilongs.github.io
┌──[root@vms152.liruilongs.github.io]-[~]
└─$curl 192.168.26.200:80
vms153.liruilongs.github.io
┌──[root@vms152.liruilongs.github.io]-[~]
└─$curl 192.168.26.200:80
vms154.liruilongs.github.io
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

通过 `ipvsadm -Ln` 命令查看，将以数字格式列出 `IPVS` 中的所有虚拟服务和真实服务器：

- -L：此选项指定我们要列出 IPVS 中的所有虚拟服务和真实服务器。
- -n：此选项指定我们要以数字格式显示 IP 地址和端口号。


```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ipvsadm -Ln
IP Virtual Server version 1.2.1 (size=4096)
Prot LocalAddress:Port Scheduler Flags
  -> RemoteAddress:Port           Forward Weight ActiveConn InActConn
TCP  192.168.26.200:80 rr
  -> 192.168.26.153:80            Masq    2      0          0
  -> 192.168.26.154:80            Masq    2      0          0
```

`ipvsadm -C` 清理 ipvs 表。
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ipvsadm -C
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ipvsadm -Ln
IP Virtual Server version 1.2.1 (size=4096)
Prot LocalAddress:Port Scheduler Flags
  -> RemoteAddress:Port           Forward Weight ActiveConn InActConn
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```



`LVS` 无法对真实负载机器进行监控检测，即无法判断负载节点是否健康，能够提供能力，由此引入 Keepalived：


Keepalived 可以按照配置的规则自动检测服务器的运行状态，并进行剔除和添加的动作，使用户完全感受不到后台服务器是否存在宕机的状态，当后端一台 WEB 服务器工作正常后 Keepalived 自动将 WEB 服务器加入到服务器群中，这些工作全部自动完成，不需要人工干涉，需要人工做的只是修复故障的WEB服务器。


另外使用 keepalived 可进行 HA 故障切换，也就是有一台备用的 LVS，主 LVS 宕机，LVS VIP 自动切换到从，可以基于 `LVS+Keepalived` 实现负载均衡及高可用功能，满足网站7x24小时稳定高效的运行。


Keepalived 基于三层检测(IP层，TCP传输层，及应用层)，需要注意，如果使用了 `keepalived.conf` 配置，就不需要再执行 `ipvsadm -A` 命令去添加均衡的 `realserver` 命令了，所有的配置都在 `keepalived.conf`里面设置即可。它可以自动地将真实服务器添加到 IPVS 中。当然，您需要在 keepalived.conf 文件中配置虚拟服务和真实服务器。

通过 ansible 剧本的方式实现 `keepalived+LVS` 自动化部署

### 剧本编写

```yaml
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat keepalived_lvs.yaml
---
  # 初始化工作，安装 keepalived 和 ipvsadm ,关闭防火墙
- name: ipvsadm  keepalived init
  hosts: node
  tasks:
    - name: install
      yum:
        name:
          - keepalived
          - ipvsadm
        state: installed
    - name: firewall clons
      shell: firewall-cmd --set-default-zone=trusted
    - name: ipvsadm clean
      shell: ipvsadm -C

  # 在主节点安装 keepalived
- name: vms153.liruilongs.github.io config
  hosts: 192.168.26.153
  tags:
    - master
  vars:
    role: MASTER
    priority: 100
  tasks:
    - name: copy keeplived config
      template:
        src: keepalived.conf.j2
        dest: /etc/keepalived/keepalived.conf

    - name: restart keeplived
      service:
        name: keepalived
        state: restarted

  # 在从节点安装 keepalived
- name: vms154.liruilongs.github.io config
  hosts: 192.168.26.154
  tags:
    - backup
  vars:
    role: BACKUP
    priority: 50
  tasks:
    - name: copy keepalived config
      template:
        src: keepalived.conf.j2
        dest: /etc/keepalived/keepalived.conf

    - name: restart keepalived
      service:
        name: keepalived
        state: restarted

┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

对应的 `keepalived.conf.j2` 模板配置文件,部分变量在剧本中指定了，这里配置文件格式需要注意一下，不同版本配置略有区别。

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat keepalived.conf.j2
! Configuration File for keepalived

global_defs {
   router_id LVS_DEVEL
   vrrp_iptables
}


vrrp_instance VI_1 {
    state {{ role }}
    interface ens32
    virtual_router_id 51
    priority {{ priority }}
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    virtual_ipaddress {
        192.168.26.200
    }

}

virtual_server 192.168.26.200 80 {
   delay_loop 1
   lb_algo rr
   lb_kind DR
   protocol TCP

   real_server 192.168.26.155 80 {
     weight 3
     TCP_CHECK {
      connect_timeout 3
      retry 3
      delay_before_retry 3
      connect_port 80
      }
   }
   real_server 192.168.26.156 80 {
     weight 3
     TCP_CHECK {
      connect_timeout 3
      retry 3
      delay_before_retry 3
      connect_port 80
      }
  }
}
```

web服务配置，内核参数修改，本地回环网卡修改

`ifcfg-lo:0` 接口是一个虚拟环回接口，用于为系统分配虚拟IP地址。在 keepalived 和 LVS 的上下文中，此虚拟 IP 地址用作在集群中活动节点和备用节点之间共享的 VIP(虚拟IP)。此 VIP 用于将流量路由到运行 `httpd` 服务的活动节点。

这里最开始有些不理解，当 LVS 和 keepalived 结合之后，原来的 VIP 变成虚拟 VIP，我们上面的Demo中使用的 VIP 是一个实际的 IP 地址，变成虚拟VIP之后，需要修改本地回环地址。


```yaml
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat deploy_web.yaml
---
- name: web init
  hosts: web
  tasks:
    - name: 网卡配置
      copy:
        dest: /etc/sysconfig/network-scripts/ifcfg-lo:0
        src: ifcfg-lo
        force: yes
    - name: 内核参数修改
      copy:
        dest: /etc/sysctl.conf
        src: sysctl.conf
        force: yes
    - name: sysctl
      shell: sysctl -p

    - name : install httpd
      yum:
        name: httpd
        state: installed

    - name: restart network
      service:
         name: network
         state: restarted

    - name: httpd content
      shell: "echo `hostname` > /var/www/html/index.html"

    - name: Restart service httpd, in all cases
      service:
        name: httpd
        state: restarted

    - name: firewall clons
      shell: firewall-cmd --set-default-zone=trusted
    - name: iptables
      shell: "iptables -F"
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```
涉及到的配置文件

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat ifcfg-lo
DEVICE=lo:0
IPADDR=192.168.26.200
NETMASK=255.255.255.255
NETWORK=192.168.26.200
BROADCAST=192.168.26.200
ONBOOT=yes
NAME=lo:0
```

关于为什么要修改本地回环地址为 VIP 地址？

VIP地址是客户端用来访问服务的地址。负载均衡器然后将传入的流量分配到真实服务器上。

为了确保真实服务器能够处理来自负载均衡器的流量，我们需要将它们的环回地址修改为VIP地址。这是因为真实服务器将从VIP地址作为源地址接收来自负载均衡器的流量。如果真实服务器使用自己的环回地址作为源地址响应此流量，则响应将无法路由回负载均衡器。

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat sysctl.conf
# sysctl settings are defined through files in
# /usr/lib/sysctl.d/, /run/sysctl.d/, and /etc/sysctl.d/.
#
# Vendors settings live in /usr/lib/sysctl.d/.
# To override a whole file, create a new file with the same in
# /etc/sysctl.d/ and put new settings there. To override
# only specific settings, add a file with a lexically later
# name in /etc/sysctl.d/ and put new settings there.
#
# For more information, see sysctl.conf(5) and sysctl.d(5).

net.ipv4.ip_forward = 1
net.ipv4.conf.all.arp_ignore = 1
net.ipv4.conf.lo.arp_ignore = 1
net.ipv4.conf.lo.arp_announce = 2
net.ipv4.conf.all.arp_announce = 2
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

### 部署测试


剧本运行完成之后，通过下面的命令检查

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible node -a 'ipvsadm -Ln'
192.168.26.153 | CHANGED | rc=0 >>
IP Virtual Server version 1.2.1 (size=4096)
Prot LocalAddress:Port Scheduler Flags
  -> RemoteAddress:Port           Forward Weight ActiveConn InActConn
TCP  192.168.26.200:80 rr persistent 2
  -> 192.168.26.155:80            Route   3      0          0
  -> 192.168.26.156:80            Route   3      0          0
192.168.26.154 | CHANGED | rc=0 >>
IP Virtual Server version 1.2.1 (size=4096)
Prot LocalAddress:Port Scheduler Flags
  -> RemoteAddress:Port           Forward Weight ActiveConn InActConn
TCP  192.168.26.200:80 rr persistent 2
  -> 192.168.26.155:80            Route   3      0          0
  -> 192.168.26.156:80            Route   3      0          0
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```
查看本地回环地址
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ansible web -m shell -a 'ip a | grep lo:'
192.168.26.156 | CHANGED | rc=0 >>
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    inet 192.168.26.200/32 brd 192.168.26.200 scope global lo:0
192.168.26.155 | CHANGED | rc=0 >>
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1
    inet 192.168.26.200/32 brd 192.168.26.200 scope global lo:0
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$curl 192.168.26.200
vms156.liruilongs.github.io
┌──[root@vms152.liruilongs.github.io]-[~]
└─$curl 192.168.26.200
vms155.liruilongs.github.io
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

### 遇到问题处理

1. 遇到问题要看官方文档,网上的好的有些旧了
2. 版本不同，配置文件略有差别，以官方文档为主，可以参考帮助文档中的一些配置模板

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$rpm -ql keepalived  | grep doc | grep conf
/usr/share/doc/keepalived-1.3.5/keepalived.conf.SYNOPSIS
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.HTTP_GET.port
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.IPv6
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.SMTP_CHECK
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.SSL_GET
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.fwmark
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.inhibit
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.misc_check
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.misc_check_arg
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.quorum
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.sample
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.status_code
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.track_interface
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.virtual_server_group
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.virtualhost
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.vrrp
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.vrrp.localchec
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.vrrp.lvs_syncd
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.vrrp.routes
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.vrrp.rules
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.vrrp.scripts
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.vrrp.static_ipaddress
/usr/share/doc/keepalived-1.3.5/samples/keepalived.conf.vrrp.sync
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```
3. 如果配置没有生效但是，服务正常启动，可能配置文件中的空格影响的，部分配置走了默认配置，需要通过 vim + `set list` 修改配置


## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***
https://blog.csdn.net/weixin_42808782/article/details/115671278


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)







