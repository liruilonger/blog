---
title: K8s：K8s 20个常用命令汇总
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-06-19 23:34:31/K8s：K8s 20个常用命令汇总.html'
mathJax: false
date: 2023-06-20 07:34:31
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 博文内容为节译整理，用于温习
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

Kubernetes 是一个容器编排平台，允许用户部署、管理和扩展容器化应用程序。

由于其灵活性、可扩展性和易用性，它已成为管理容器的最流行的平台之一。

Kubernetes 的主要功能之一是其命令行界面 （CLI），它允许用户使用命令与平台进行交互。

使用 k8s cli 提高效率将使您对工作负载的操作及其调试速度更快。

在本文中，我们将讨论可以帮助用户更有效地管理其 Kubernetes 集群的前 20 个命令式命令。

### kubectl create

kubectl create 命令用于在 Kubernetes 中创建资源。它可用于创建各种资源，包括 Pod、服务、部署等。例如，若要创建新部署，可以：

```bash
kubectl create deployment my-deployment --image=my-image
```

`kubectl create deployment <deployment-name> --image=<image-name>`：创建一个 Deployment 对象，指定容器镜像名称和部署名称。

`kubectl create service <service-name> --tcp=<port>:<target-port>`：创建一个 Service 对象，将容器的端口暴露到集群内部，并将其映射到指定的端口。

`kubectl create configmap <configmap-name> --from-file=<path-to-file>`：创建一个 ConfigMap 对象，用于存储应用程序的配置信息。

`kubectl create secret generic <secret-name> --from-literal=<key>=<value>`：创建一个 Secret 对象，用于存储敏感信息，如密码等。

`kubectl create namespace <namespace-name>`：创建一个 Namespace 对象，用于隔离和管理 Kubernetes 资源。

`kubectl create job <job-name> --image=<image-name>`：创建一个 Job 对象，用于在 Kubernetes 集群中运行一个或多个任务。

`kubectl create cronjob <cronjob-name> --image=<image-name> --schedule=<cron-expression>`：创建一个 CronJob 对象，用于定期运行一个或多个任务。

### kubectl get

它用于检索有关 Kubernetes 资源的信息。它可用于检索有关各种资源（包括 Pod、服务、部署等）的信息。

例如，要检索有关 Kubernetes 集群中所有 Pod 的信息，您可以：

```bash
kubectl get pods/deployments/svc/configmaps/secrets
```

### kubectl describe

kubectl describe 命令用于检索有关特定 Kubernetes 资源的详细信息。它可用于检索有关各种资源（包括 Pod、服务、部署等）的信息。

例如，要检索有关特定容器的详细信息，可以：

```bash
kubectl describe pod <pod-name>
```

### kubectl delete

kubectl delete 命令用于删除 Kubernetes 资源。它可用于删除各种资源，包括 Pod、服务、部署等。例如，要删除特定 Pod，可以：

```bash
kubectl delete pod my-pod
```

### kubectl exec

kubectl exec 命令用于在正在运行的容器中执行命令。它可用于在各种容器中执行命令，包括 Pod、服务、部署等。

例如，要在正在运行的 Pod 中执行命令，可以：

```bash
kubectl exec my-pod -- ls 
kubectl exec -it <pod-name> /bin/bash/
```

### kubectl logs

kubectl logs 命令用于从容器中检索日志。它可用于从各种容器（包括 Pod、服务、部署等）中检索日志。

```bash
kubectl logs my-pod
```

### kubectl port-forward

kubectl port-forward 命令用于将本地端口转发到 Kubernetes pod 上的端口。

它可用于转发来自各种 Pod 的端口，包括 Pod、服务、部署等。例如，要将本地计算机上的端口 8080 转发到 Pod 上的端口 80，您可以：

```bash
kubectl port-forward my-pod 8080:80
```

### kubectl label

kubectl label 命令用于在 Kubernetes 资源中添加或删除标签。它可用于在各种资源（包括容器、服务、部署等）中添加或删除标签。

例如，要向容器添加标签，可以：

```bash
kubectl label pod my-pod app=backend
```

### kubectl scale

kubectl scale 命令用于扩展或缩减 Kubernetes 资源。它可用于缩放各种资源，包括部署、副本集等。例如，若要将部署扩展到 5 个副本，可以：

```bash
kubectl scale deployment my-deployment --replicas=5
```

### kubectl rollout

kubectl rollout 命令用于管理 Kubernetes 资源的推出。它可用于管理各种资源的推出，包括部署、副本集等。

```bash
kubectl rollout status deployment/my-deployment
```

### kubectl expose

kubectl expose 命令用于将 Kubernetes 资源公开为服务。它可用于公开各种资源，包括 Pod、部署等。

例如，若要将部署公开为服务，可以：

```bash
kubectl expose deployment my-deployment --port=80 --target-port=8080
```

### kubectl run

kubectl run 命令用于创建新的 Kubernetes 资源。它可用于创建各种资源，包括 Pod、deployment等。

```bash
kubectl run my-pod --image=my-image
```


### kubectl config

kubectl config 命令用于管理 Kubernetes 配置。它可用于管理各种配置，包括上下文、群集等。

例如，要查看当前上下文配置，可以：
```bash
kubectl config current-context
```

### kubectl cluster-info

`kubectl cluster-info` 命令用于检索有关 Kubernetes 集群的信息。
它可用于检索各种信息，包括 API 服务器 URL、Kubernetes 版本等。

例如，要检索有关 Kubernetes 集群的信息，您可以：

```bash
kubectl cluster-info
```

### kubectl apply -dry-run

`kubectl apply --dry-run` 命令用于模拟对 Kubernetes 资源更改的应用。它可用于模拟对各种资源（包括 Pod、服务、部署等）的更改。

```bash
kubectl apply -f deployment.yaml — dry-run
```

### kubectl rollout undo

kubectl rollout undo 命令用于撤消 Kubernetes 资源的推出。它可用于撤消各种资源的推出，包括部署、副本集等。


```bash
kubectl rollout undo deployment/my-deployment
```


### kubectl auth

kubectl auth 命令用于管理 Kubernetes 身份验证。它可用于管理各种身份验证设置，包括角色、角色绑定等。


```bash
kubectl auth can-i get pods —-as my-user

```

### kubectl top


kubectl top 命令用于从 Kubernetes 资源中检索资源使用指标。它可用于从各种资源（包括节点、Pod 等）中检索指标。

```bash
kubectl top pod my-pod
```

### kubectl set

此命令用于更新或修改 Kubernetes 资源的状态。这是一个命令式命令，这意味着它直接指示 Kubernetes 执行操作，而不是声明所需的状态。


+ `kubectl set image`：此子命令用于更新部署或 Pod 使用的容器映像。
+ `kubectl set env`：此子命令用于更新 Pod 或部署的环境变量。
+ `kubectl set resources`：此子命令用于更新 Pod 或部署的资源请求和限制。
+ `kubectl set replicas`：此子命令用于更新部署的副本数。

```bash
kubectl set image deployment/my-deployment my-container=new-image:latest
```





## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


<https://shahneil.medium.com/top-20-must-know-kubernetes-commands-for-effective-container-orchestration-8ec6ef77c3c7>


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
---------------------
