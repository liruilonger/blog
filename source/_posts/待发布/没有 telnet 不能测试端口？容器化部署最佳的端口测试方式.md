---
title: '没有 telnet 不能测试端口？容器化部署最佳的端口测试方式'
tags:
  - 网络测试
categories:
  - 网络测试
toc: true
recommend: 1
keywords: 网络测试
uniqueId: '2023-04-11 08:15:06/没有 telnet 不能测试端口？容器化部署最佳的端口测试方式.html'
mathJax: false
date: 2023-04-11 16:15:06
thumbnail:
---

**<font color="009688"> 他的一生告诉我们，不能自爱就不能爱人，憎恨自己也必憎恨他人，最后也会像可恶的自私一样，使人变得极度孤独和悲观绝望。 —— 赫尔曼·黑塞《荒原狼》**</font>

<!-- more -->
## 写在前面

***
+ 生产中遇到，整理笔记
+ 在容器中没有 `telnet` ，如何测试远程端口
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 他的一生告诉我们，不能自爱就不能爱人，憎恨自己也必憎恨他人，最后也会像可恶的自私一样，使人变得极度孤独和悲观绝望。 —— 赫尔曼·黑塞《荒原狼》**</font>

***

没有 `telnet` ,就不能测试端口了？和小伙伴们分享一些容器下端口测试的方法

Demo 端口环境准备，相关网段 `192.168.26.55`机器，远程机器，监听 55555 端口

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ coproc python3 -m http.server 55555
[1] 11694
```
测试环境，相同网段 `192.168.26.100` 机器
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$wget 192.168.26.55:55555 -v
--2023-04-11 16:59:35--  http://192.168.26.55:55555/
正在连接 192.168.26.55:55555... 已连接。
已发出 HTTP 请求，正在等待回应... 200 OK
```
下文中的 `timeout $number` 非必须，用于约束命令超时时间


### `/dev/tcp`
 
利用 `/dev/tcp 虚拟文件系统` 检查端口是打开还是关闭，使用下面这样的命名就可以测试

```bash
 </dev/tcp/172.30.127.22/3306
```


看一个Demo，使用 Linux 中的 `/dev/tcp` 虚拟文件系统连接到远程主机的 55555 端口。用于测试远程主机是否在该端口上侦听或建立到该端口的连接。 `/dev/proto/host/port/` 对应测试数据   `/dev/tcp/$host/$port`

这里为了好看，我们做一些简单修饰 

通的情况
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$(timeout 1 bash -c '</dev/tcp/192.168.26.55/55555' && echo PORT OPEN || echo PORT CLOSED) 2>/dev/null
PORT OPEN
```
不通的情况
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$(timeout 1 bash -c '</dev/tcp/192.168.26.55/443' && echo PORT OPEN || echo PORT CLOSED) 2>/dev/null
PORT CLOSED
```

原理，Linux 的真值和编程语言代码中的真值是不同的。根据执行的返回值确认是否通
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$bash -c '</dev/tcp/192.168.26.55/55555' && echo $?
0
┌──[root@vms100.liruilongs.github.io]-[~]
└─$bash -c '</dev/tcp/192.168.26.55/443' && echo $?
bash: connect: 拒绝连接
bash: /dev/tcp/192.168.26.55/443: 拒绝连接
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```
换成分号的情况我们就可以正常显示。
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$bash -c '</dev/tcp/192.168.26.55/443' ;echo $?
bash: connect: 拒绝连接
bash: /dev/tcp/192.168.26.55/443: 拒绝连接
1
```
需要注意的是，这个命令可能不适用于所有系统，因为它依赖于 `/dev/tcp` 虚拟文件系统是否启用。此外，这个命令应该只用于测试或诊断目的，而不用于任何恶意活动。

如果三层都不通，对于不同的情况也可以具体分析

端口通的情况
```bash
[root@master ~]# </dev/tcp/172.30.127.22/3306
```
IP 都不通的情况
```bash
[root@master ~]# </dev/tcp/172.30.127.23/3306
bash: connect: No route to host
bash: /dev/tcp/172.30.127.23/3306: No route to host
[root@master ~]# </dev/tcp/192.168.26.55/55555
bash: connect: Network is unreachable
bash: /dev/tcp/192.168.26.55/55555: Network is unreachable
[root@master ~]#
```
- `Network is unreachable` ：当源设备无法找到到达目标网络的路由时，会出现此错误。这意味着源设备的路由表中没有目标设备所在的网络的条目。这可能是因为网络已经关闭，或者路由表中存在配置错误。
- `No route to host` ：当源设备可以找到到达目标网络的路由，但无法找到该网络上特定主机的路由时，会出现此错误。这意味着源设备的路由表中有目标设备所在的网络的条目，但没有目标设备的特定IP地址的条目。这可能是因为目标设备已关闭，或者路由表中存在配置错误。


### curl


curl是用于从服务器传输数据或将数据传输到服务器的工具。它支持以下协议：`DICT，FILE，FTP，FTPS，GOPHER，GOPHERS，HTTP，HTTPS，IMAP，IMAPS，LDAP，LDAPS，MQTT，POP3，POP3S，RTMP，RTMPS，RTSP，SCP，SFTP，SMB，SMBS，SMTP，SMTPS，TELNET，TFTP，WS和WSS`。它由 `libcurl` 提供支持，适用于所有与传输相关的功能

通的情况
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$timeout 3 curl -vvv telnet://192.168.26.55:55555
* About to connect() to 192.168.26.55 port 55555 (#0)
*   Trying 192.168.26.55...
* Connected to 192.168.26.55 (192.168.26.55) port 55555 (#0)
```
不通的情况
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$timeout 3 curl -vvv telnet://192.168.26.55:443
* About to connect() to 192.168.26.55 port 443 (#0)
*   Trying 192.168.26.55...
* 拒绝连接
* Failed connect to 192.168.26.55:443; 拒绝连接
* Closing connection 0
curl: (7) Failed connect to 192.168.26.55:443; 拒绝连接
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```




### nc OR nact

`nc` 或者 `ncat` 命令也可以用于测试远程主机是否在指定端口上侦听或建立到该端口的连接。
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$which nc
/usr/bin/nc
```
例如，要测试远程主机的 55555 端口是否打开，可以使用以下命令：

通的情况
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$nc -vz 192.168.26.55 55555
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.26.55:55555.
Ncat: 0 bytes sent, 0 bytes received in 0.01 seconds.
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```
不通的情况
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$nc -vz 192.168.26.55 443
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connection refused.
```

或者可以使用 `ncat`,ncat 是 nmap 网络工具套件的一部分，是一个更现代的工具。它被设计为 nc 的更丰富的替代品。它支持 SSL/TLS 加密、IPv6、SOCKS 代理等。
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$ncat -zv 192.168.26.55 55555
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connected to 192.168.26.55:55555.
Ncat: 0 bytes sent, 0 bytes received in 0.01 seconds.
┌──[root@vms100.liruilongs.github.io]-[~]
└─$ncat -zv 192.168.26.55 443
Ncat: Version 7.50 ( https://nmap.org/ncat )
Ncat: Connection refused.
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```

### nmap

端口扫描工具，会扫描所有的端口,一般不建议使用
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$rpm -ql nmap  || yum -y install nmap
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$nmap -sT 192.168.26.55 443

Starting Nmap 6.40 ( http://nmap.org ) at 2023-04-11 16:44 CST
Nmap scan report for 192.168.26.55 (192.168.26.55)
Host is up (0.0025s latency).
Not shown: 994 closed ports
PORT      STATE SERVICE
22/tcp    open  ssh
80/tcp    open  http
222/tcp   open  rsh-spx
8080/tcp  open  http-proxy
50000/tcp open  ibm-db2
55555/tcp open  unknown
MAC Address: 00:0C:29:9F:48:81 (VMware)

Nmap done: 2 IP addresses (1 host up) scanned in 1.74 seconds
```

### python


Linux 环境一般都 `python` 环境，要使用 Python 检查远程端口是否打开，可以使用 socket 模块

端口通的 Demo
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$python2
Python 2.7.5 (default, Aug  4 2017, 00:39:18)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-16)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import socket
>>> sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
>>> sock.connect(('192.168.26.55', 55555))
>>>
```
端口不通的 Demo 
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$python2
Python 2.7.5 (default, Aug  4 2017, 00:39:18)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-16)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import socket
>>> sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
>>> sock.connect(('192.168.26.55', 443))
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/usr/lib64/python2.7/socket.py", line 224, in meth
    return getattr(self._sock,name)(*args)
socket.error: [Errno 111] Connection refused
>>>
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```

关于容器下中端口测试就可小伙伴分享这里，感觉没什么差别，Demo 都没有做容器里做。


## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知。

***
https://medium.com/geekculture/linux-useful-tricks-telnet-alternatives-ed9f342149a1


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
