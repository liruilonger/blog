---
title: 关于 OpenCV 图像处理工具包 imutils 简单认知
tags:
  - imutils
categories:
  - imutils
toc: true
recommend: 1
keywords: imutils
uniqueId: '2023-05-17 15:45:22/关于OpenCv图像处理工具包 imutils 简单认知.html'
mathJax: false
date: 2023-05-17 23:45:22
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 博文内容涉及 基本的图像处理工具包 imutils 的简单介绍以及使用Demo
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***




`imutils` 是一个基于 OpenCV 的 Python 图像处理库。它包含了许多函数来简化常见的操作，如调整大小、旋转和显示图像等。一些关键特性包括：

+ 调整图像大小并保持纵横比
+ 以指定角度旋转图像
+ 在任何方向上平移(即移动)图像
+ 图像 骨架化,检测边缘
+ 点透视变换
+ 显示 Matplotlib 图像
+ 对轮廓进行排序

该项目的 demos 目录下 `https://github.com/PyImageSearch/imutils/tree/master/demos`，提供了一些常用的图片操作demo 


## 常见方法以及参数解释

- `translate(image, x, y)`：将图像`向右移动 x 个像素，向下移动 y 个像素`。image 是输入图像，x 是要将图像向右移动的像素数，y 是要将图像向下移动的像素数。
- `rotate(image, angle, center=None, scale=1.0)`：围绕中心点旋转 angle 度的图像。image 是输入图像，angle 是旋转角度（以度为单位），center 是旋转中心（默认为图像中心），scale 是缩放因子（默认为 1.0）。
- `rotate_bound(image, angle)`：旋转图像 angle 度而不裁剪图像。image 是输入图像，angle 是旋转角度（以度为单位）。
- `resize(image, width=None, height=None, inter=cv2.INTER_AREA)`：将图像调整为指定的 width 和 height。image 是输入图像，width 是输出图像的期望宽度，height 是输出图像的期望高度，inter 是插值方法（默认为 cv2.INTER_AREA）。
- `skeletonize(image, size=(3, 3))`：对图像应用形态学骨架化操作。image 是输入图像，size 是结构元素的大小（默认为 (3, 3)）。
- `opencv2matplotlib(image)`：将 OpenCV 图像转换为 Matplotlib 兼容格式。image 是输入图像。
- `url_to_image(url)`：从 URL 下载图像并将其作为 NumPy 数组返回。url 是图像的 URL。
- `auto_canny(image, sigma=0.33)`：使用自动确定的阈值将 Canny 边缘检测算法应用于图像。image 是输入图像，sigma 是高斯滤波器的标准差（默认为 0.33）。
- `grab_contours(cnts)`：从 cv2.findContours 函数的输出中提取轮廓。cnts 是 cv2.findContours 函数的输出。
- `is_cv2()`：如果安装了 OpenCV 2，则返回 True，否则返回 False。
- `is_cv3()`：如果安装了 OpenCV 3，则返回 True，否则返回 False。
- `is_cv4()`：如果安装了 OpenCV 4，则返回 True，否则返回 False。
- `check_opencv_version(major, minor, patch=None)`：检查安装的 OpenCV 版本是否至少为指定版本。major、minor 和 patch 分别是主要、次要和补丁版本号。
- `build_montages(images, image_shape, montage_shape)`：构建图像的拼贴画。images 是输入图像的列表，image_shape 是每个输入图像的形状，montage_shape 是拼贴画的形状。
- `adjust_brightness_contrast(image, alpha=1.0, beta=0.0)`：调整图像的亮度和对比度。image 是输入图像，alpha 是对比度控制（默认为 1.0），beta 是亮度控制（默认为 0.0）。
- `find_function(name)`：按名称查找 imutils 包中的函数。name 是要查找的函数的名称。
- `paths.list_images`: 获取指定目录下的所有图片路径
- `put_text和put_centered_text`。这两个函数都是用来在图像上绘制文本的。put_text函数可以在指定的位置绘制一段文本，而put_centered_text函数可以在图像中央绘制一段文本。

## 个别 Demo

### 依赖环境

```bash
# pip install matplotlib==3.7.1
# pip install numpy==1.23.5
# pip install opencv-contrib-python==4.7.0.72
# pip install opencv-python==4.7.0.72
# pip install imutils==0.5.4


import imutils
import cv2
```

### Demo

图片上添加文字

```py
def  demo1():
    import cv2
    from imutils.text import put_text

    # Load image
    img = cv2.imread('W:\python_code\deepface\database\yz_W.jpg')

    # Define text parameters
    text = 'Hello, world!\nThis is a test.'
    org = (50, 50)
    font_face = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 1
    color = (255, 255, 255)
    thickness = 2
    line_type = 8
    bottom_left_origin = False

    # Draw text on image
    put_text(img, text, org, font_face, font_scale, color, thickness, line_type, bottom_left_origin)

    # Display image
    cv2.imshow('Image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

# demo1()

def  demo2():
    import cv2
    from imutils.text import put_centered_text

    # Load image
    img = cv2.imread('W:\python_code\deepface\database\yz_W.jpg')

    # Define text parameters
    text = 'Hello, world!\nThis is a test.'
    font_face = cv2.FONT_HERSHEY_SIMPLEX
    font_scale = 1
    color = (255, 255, 255)
    thickness = 2
    line_type = 8

    # Draw centered text on image
    put_centered_text(img, text, font_face, font_scale, color, thickness, line_type)

    # Display image
    cv2.imshow('Image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

demo2()
```
|添加文字|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/e907398b9e974ef9a6cfbc7ad69cb798.png)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/823aa754f0c041ad8c8f8931341b64bf.png)|


平移
```py
img = cv2.imread(img_path)
translated = imutils.translate(img, 100, 30)
cv2.imwrite(out_path + 'translate.jpg', translated)
```

|原图|平移后的图|
|--|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/cb30e8846d35425bb9a4e4544f52f70f.jpeg)|![在这里插入图片描述](https://img-blog.csdnimg.cn/d81019f01b05464bafabdcd079402f76.png)|


获取当前目录下的所有图片文件路径
```py
def paths():
    from imutils import paths
    for imagePath in paths.list_images("W:\python_code\deepface\database"):
        print(imagePath)
```

等比例缩放
```py
        resized = imutils.resize(img, width=width)
        cv2.imwrite(out_path + 'resize' + str(width) + '.jpg', resized)
```

|100的等比例缩放|400 的等比例缩放|
|--|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/d56f9f94ef724bca94db69bb4c7a837a.jpeg)|![在这里插入图片描述](https://img-blog.csdnimg.cn/2ef0f39295704f8a9d6685072abcc5fe.jpeg)|





## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知，这是一个开源项目，如果你认可它，不要吝啬星星哦 :)


***
 
https://github.com/PyImageSearch/imutils/

https://pypi.org/project/imutils/

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
