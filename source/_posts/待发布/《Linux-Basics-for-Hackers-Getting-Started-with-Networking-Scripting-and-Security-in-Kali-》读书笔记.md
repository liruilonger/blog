---
title: 《Linux Basics for Hackers：Getting Started with Networking, Scripting, and Security in Kali 》读书笔记
tags:
  - Linux
categories:
  - Linux
toc: true
recommend: 1
keywords: Linux
uniqueId: 2022-10-03 08:59:46/《Linux Basics for Hackers： Getting Started with Networking, Scripting, and Security in Kali 》读书笔记.html
mathJax: false
date: 2022-10-03 16:59:46
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 中英文版下载地址：[https://github.com/OpenCyberTranslationProject/Linux-Basics-for-Hackers](https://github.com/OpenCyberTranslationProject/Linux-Basics-for-Hackers)
+ vmware Kali虚机下载：[https://www.kali.org/get-kali/](https://www.kali.org/get-kali/)
+ 谷歌上一个pdf网站看到，豆瓣了下，评分还好，搜了下有国内大佬们翻译了中文版的 `^_^`，所以写这篇博客
+ 书籍适合入门安全领域相关，但是对Linux所知甚少的小伙伴
+ 博文内容涉及：
  + 进程优先级设置修改
  + 创建存储设备块的副本
  + 系统日志管理
  + 几种不同的匿名访问方式
  + 理解和检查无线设备



**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***
![在这里插入图片描述](https://img-blog.csdnimg.cn/b9e212c75a4049f89da3686b5e881554.png)

下载好虚机之后，vmware 的窗口不太方便使用，我们需要配置ssh登录

学习的内核版本
```bash
┌──(kali㉿kali)-[~]
└─$ uname -a
Linux kali 5.18.0-kali5-amd64 #1 SMP PREEMPT_DYNAMIC Debian 5.18.5-1kali6 (2022-07-07) x86_64 GNU/Linux
┌──(kali㉿kali)-[~]
└─$
```

同样重要的是，在开始之前，你不应该以root身份登录执行日常任务，因为当你以root身份登录时，任何入侵你的系统的人都会立即获得root权限，从而“拥有”你的系统。需要以普通用户身份登录

下载的虚机帐密：`kali/kali`,配置的ssh登录
```bash
┌──(kali㉿kali)-[~]
└─$ sudo sed 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config -i
[sudo] password for kali: 
                                                                             
┌──(kali㉿kali)-[~]
└─$ sed 's/#PasswordAuthentication yes/PasswordAuthentication yes/g' /etc/ssh/sshd_config | grep -i pass
#PermitRootLogin prohibit-password
# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication yes
#PermitEmptyPasswords no
# Change to yes to enable challenge-response passwords (beware issues with
#KerberosOrLocalPasswd yes
# PasswordAuthentication.  Depending on your PAM configuration,
# PAM authentication via KbdInteractiveAuthentication may bypass
# the setting of "PermitRootLogin without-password".
# PAM authentication, then enable this but set PasswordAuthentication
# Allow client to pass locale environment variables
```
如果需要配置root免密登录，需要配置这个
```bash 
┌──(kali㉿kali)-[~]
└─$ cat /etc/ssh/sshd_config | grep -i root 
#PermitRootLogin prohibit-password
# the setting of "PermitRootLogin without-password".
#ChrootDirectory none
```
修改完配置需要重启服务
```bash 
┌──(kali㉿kali)-[~]
└─$ systemctl status sshd
Unit sshd.service could not be found.

┌──(kali㉿kali)-[~]
└─$ service start sshd
start: unrecognized service
                                                                             
┌──(kali㉿kali)-[~]
└─$ /etc/init.d/ssh restart 
Restarting ssh (via systemctl): ssh.service.
                                                                             
┌──(kali㉿kali)-[~]
└─$ 
```
系统命名搜索
```bash 
┌──(kali㉿kali)-[~]
└─$ whereis vmstat
vmstat: /usr/bin/vmstat /usr/share/man/man8/vmstat.8.gz

┌──(kali㉿kali)-[~]
└─$ which vmstat
/usr/bin/vmstat

```

### 进程优先级设置修改

#### 使用nice命令改变进程优先级

`nice`的值范围从`-20到+19`，默认值为0(见下图)。`高nice值为低优先级，低nice值为高优先级`。当进程启动时，它继承其父进程的nice值。`进程的所有者可以降低进程的优先级，但不能增加其优先级。`当然，超级用户或根用户可以`随意将nice值设置为他们喜欢的任何值`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/6a347d21fef44a709ff6e5722b939c97.png)


`启动进程`时，可以使用`nice`命令`设置优先级`，然后在使用`renice`命令开始运行进程后`更改优先级`。nice命令要求增加nice值，而renice命令要求niceness的绝对值。

```bash 
┌──(kali㉿kali)-[~]
└─$nice -n -10 /bin/slowprocess
```

##### 在运行进程时设置优先级

出于演示目的，我们假设我们有一个名为`slowprocess的进程位于/bin/slowprocess`。

如果我们`希望它加速执行完成`，我们可以使用nice命令启动该过程：此命令会将`nice值递增-10`，从而增加其优先级并为其分配更多资源。
```bash 
┌──(kali㉿kali)-[~]
└─$nice -n -10 /bin/slowprocess
```


给予slowprocess一个较低的优先级，我们可以将其好的值正增加10：
```bash 
┌──(kali㉿kali)-[~]
└─$nice -n 10 /bin/slowprocess
```

#### 使用renice命令改变正在运行的进程优先级

`renice`命令采用介于`-20和19之间的绝对值`，并将优先级设置为该特定级别，而不是从其开始的级别增加或减少。此外，renice所针对的进程的PID而不是名字。因此，`如果slowprocess在您的系统上使用过多的资源，并且你想给它一个较低的优先级，从而允许其他进程具有更高的优先级和更多的资源，你可以重新设置slowprocess(PID为6996)并给出一个更高的值`，如下：

```bash
┌──(kali㉿kali)-[~]
└─$renice 20 6996
```

与`nice`一样，只有`root`用户可以将进程重新设置为负值以赋予其更高的优先级，但任何用户都可以使用`renice`来降低优先级。

还可以使用top工具更改nice值。随着top运行，只需按R键，然后提供PID和nice值。如清单6-4中top正在运行，当我按下R键并提供PID和nice值时，我得到以下输出：

![在这里插入图片描述](https://img-blog.csdnimg.cn/6092c8f0028741e9b9e3779eb99d0cca.png)





### 创建存储设备块的副本

在信息安全和黑客的世界里，一个Linux归档命令在实用性方面胜过其他命令。**dd**命令`逐个比特位复制文件、文件系统`，甚至整个硬盘驱动器。这意味着即`使删除的文件也会被复制(是的，重要的是要知道您删除的文件可能是可恢复的)`，以便于发现和恢复。大多数逻辑复制实用程序(如**cp**)不会复制已删除的文件。

一旦黑客拥有目标系统，**dd**命令将允许他们将整个硬盘驱动器或存储设备复制到他们的系统。此外，那些想要抓住黑客的人
- 即调查调查人员 
- 可能会使用此命令来制作硬盘驱动器的物理副本，其中包含已删除的文件和其他可能有助于查找针对黑客证据的构件。

需要注意的是，**dd**命令`不应该用于典型的文件和存储设备的日常复制`，因为它非常慢，其他命令执行起来更快、更有效。但是，当您需要一个没有文件系统或其他逻辑结构的存储设备的副本时(例如在取证调查中)，它是非常好的。

**dd**命令的基本语法如下:
```bash 
dd if=inputfile of=outputfile
```
因此，如果您想为您的闪存驱动器创建一个物理副本，假设闪存驱动器是sdb，您将输入以下内容:
```bash 
┌──(kali㉿kali)-[~]
└─$dd if=/dev/sdb of=/root/flashcopy

1257441=0 records in

1257440+0 records out

7643809280 bytes (7.6 GB) copied, 1220.729 s, 5.2 MB/s
```
让我们分解这个命令：**dd** 是指定输入文件的物理“复制”命令，**/dev/sdb** 表示指定输出文件的/dev目录下的闪存驱动器，**/root/flashcopy** 是要将物理副本复制到的文件的名称。


**dd命**令可以使用许多参数选项，您可以对这些选项进行一些研究，但其中最有用的是 **noerror** 选项和 **bs** (块大小)选项。顾名思义，即使遇到错误，**noerror** 选项也会继续复制。**bs** 选项允许您确定要复制的数据的块大小(每个块的读/写字节数)。默认情况下，它被设置为512字节，但是可以更改它以加快进度。通常，这将设置为设备的扇区大小，通常为4KB(4096字节)。有了这些选项，您的命令将如下所示:
```bash 
┌──(kali㉿kali)-[~]
└─$dd if=/dev/media of=/root/flashcopy bs=4096 conv:noerror
```

### rsyslog 日志记录守护进程

Linux使用一个名为**syslogd** 的守护进程来自动记录计算机上的事件。**syslog** 的几个变体，包括rsyslog和syslog-ng，在Linux的不同发行版上使用，尽管它们的操作非常相似，但仍然存在一些细微的差异。由于Kali Linux是在Debian上构建的，并且Debian默认情况下带有rsyslog，所以我们将在本章重点介绍这个实用程序。如果您想使用其他发行版，那么有必要对它们的日志系统做一些研究。

```bash
┌──(kali㉿kali)-[~]
└─$ cat /etc/rsyslog.conf
# /etc/rsyslog.conf configuration file for rsyslog
#
# For more information install rsyslog-doc and see
# /usr/share/doc/rsyslog-doc/html/configuration/index.html


#################
#### MODULES ####
#################

module(load="imuxsock") # provides support for local system logging
module(load="imklog")   # provides kernel logging support
#module(load="immark")  # provides --MARK-- message capability

# provides UDP syslog reception
#module(load="imudp")
#input(type="imudp" port="514")

# provides TCP syslog reception
#module(load="imtcp")
#input(type="imtcp" port="514")


###########################
#### GLOBAL DIRECTIVES ####
###########################

#
# Use traditional timestamp format.
# To enable high precision timestamps, comment out the following line.
#
$ActionFileDefaultTemplate RSYSLOG_TraditionalFileFormat

#
# Set the default permissions for all log files.
#
$FileOwner root
$FileGroup adm
$FileCreateMode 0640
$DirCreateMode 0755
$Umask 0022

#
# Where to place spool and state files
#
$WorkDirectory /var/spool/rsyslog

#
# Include all config files in /etc/rsyslog.d/
#
$IncludeConfig /etc/rsyslog.d/*.conf


###############
#### RULES ####
###############

#
# First some standard log files.  Log by facility.
#
auth,authpriv.*                 /var/log/auth.log
*.*;auth,authpriv.none          -/var/log/syslog
#cron.*                         /var/log/cron.log
daemon.*                        -/var/log/daemon.log
kern.*                          -/var/log/kern.log
lpr.*                           -/var/log/lpr.log
mail.*                          -/var/log/mail.log
user.*                          -/var/log/user.log

#
# Logging for the mail system.  Split it up so that
# it is easy to write scripts to parse these files.
#
mail.info                       -/var/log/mail.info
mail.warn                       -/var/log/mail.warn
mail.err                        /var/log/mail.err

#
# Some "catch-all" log files.
#
*.=debug;\
        auth,authpriv.none;\
        mail.none               -/var/log/debug
*.=info;*.=notice;*.=warn;\
        auth,authpriv.none;\
        cron,daemon.none;\
        mail.none               -/var/log/messages

#
# Emergencies are sent to everybody logged in.
#
*.emerg                         :omusrmsg:*

┌──(kali㉿kali)-[~]
└─$
```
每一行都是一个单独的日志记录规则，说明记录了哪些消息以及记录到哪里。这些规则的基本格式如下:`facility.priority action`

**facility** 关键字引用正在记录其消息的程序，例如邮件、内核或打印系统。
+ **auth/authpriv**安全/授权消息
+ **cron**时钟守护进程
+ **daemon**其他守护进程
+ **kern**内核消息
+ **lpr**打印系统
+ **mail**邮件系统
+ **user**常规用户级别消息

**priority** 关键字决定为该程序记录哪种类型的消息。warning、warn、error、err、emerg、panic，这些都已被弃用，不应该使用。
-   debug
-   info
-   notice
-   warning
-   warn
-   error
-   err
-   crit
-   alert
-   emerg
-   panic

**action** 关键字引用将发送日志的位置。


### 使用 logrotate 自动清理日志
日志文件会占用空间，所以如果不定期删除它们，它们最终会填满整个硬盘驱动器。另一方面，如果太频繁地删除日志文件，那么在将来的某个时间点就没有日志供研究了。您可以使用 logrotate 通过切割日志来确定这些符合相反需求之间的平衡。

logrotate 是通过将日志文件移动到其他位置来定期归档日志文件的过程，从而为您留下一个新的日志文件。然后，在指定的一段时间之后，归档的位置将被清理。

您的系统已经在使用 logrotate 实用程序的 cron 作业切割转储日志文件。您可以配置 logrotate 实用程序，以使用`/etc/logrotate.conf `文本文件选择日志转储备份的规律性

```bash
┌──(kali㉿kali)-[~]
└─$ cat /etc/logrotate.conf
# see "man logrotate" for details

# global options do not affect preceding include directives

# rotate log files weekly
weekly

# keep 4 weeks worth of backlogs
rotate 4

# create new (empty) log files after rotating old ones
create

# use date as a suffix of the rotated file
#dateext

# uncomment this if you want your log files compressed
#compress

# packages drop log rotation information into this directory
include /etc/logrotate.d

# system-specific logs may also be configured here.

┌──(kali㉿kali)-[~]
└─$

```
+ 设置切割转储日志默认设置的频率是每四个星期转储日志:如果您的日志有足够的存储空间，并且希望保留半永久记录以便以后进行取证分析，那么,您可以将此设置更改为 rotate 26 以保存六个月的日志，或者将 rotate 52 更改为保存一年的日志

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cat /etc/logrotate.conf
# see "man logrotate" for details
# rotate log files weekly
weekly

# keep 4 weeks worth of backlogs
rotate 4

# create new (empty) log files after rotating old ones
create

# use date as a suffix of the rotated file
dateext

# uncomment this if you want your log files compressed
#compress

# RPM packages drop log rotation information into this directory
include /etc/logrotate.d

# no packages own wtmp and btmp -- we'll rotate them here
/var/log/wtmp {
    monthly
    create 0664 root utmp
        minsize 1M
    rotate 1
}

/var/log/btmp {
    missingok
    monthly
    create 0600 root utmp
    rotate 1
}

# system-specific logs may be also be configured here.
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```
####　保持隐身

一旦您破坏了 Linux 系统，禁用日志记录并删除日志文件中您入侵的任何证据，这都是有用的，以减少被检测到的机会。有很多方法可以做到这一点，每种方法都有自己的风险和可靠性。

##### 删除证据


首先，您需要删除活动的任何日志。您可以简单地打开日志文件，并使用逐行准确地删除详细描述您的活动的任何日志。然而，这可能会很耗时，并在日志文件中留下时间间隔，这看起来很可疑。此外，删除的文件通常可以由一个熟练的取证调查员恢复。

更好、更安全的解决方案是`分解日志文件`。对于其他文件删除系统，熟练的调查员仍然能够恢复已删除的文件，Linux有一个内置命令，名为**shred**，可以删除文件并多次擦写覆盖它，这使得恢复变得更加困难。

```bash
┌──(kali㉿kali)-[~]
└─$ shred --help
Usage: shred [OPTION]... FILE...
Overwrite the specified FILE(s) repeatedly, in order to make it harder
for even very expensive hardware probing to recover the data.

If FILE is -, shred standard output.

Mandatory arguments to long options are mandatory for short options too.
  -f, --force    change permissions to allow writing if necessary
  -n, --iterations=N  overwrite N times instead of the default (3)
      --random-source=FILE  get random bytes from FILE
  -s, --size=N   shred this many bytes (suffixes like K, M, G accepted)
  -u             deallocate and remove file after overwriting
      --remove[=HOW]  like -u but give control on HOW to delete;  See below
  -v, --verbose  show progress
  -x, --exact    do not round file sizes up to the next full block;
                   this is the default for non-regular files
  -z, --zero     add a final overwrite with zeros to hide shredding
      --help     display this help and exit
      --version  output version information and exit
```
碎片化`auth.log`文件
```bash
┌──(kali㉿kali)-[/var/log]
└─$ cat -n auth.log | head -n 10
     1  Oct  2 22:34:01 kali systemd-logind[585]: New seat seat0.
     2  Oct  2 22:34:01 kali systemd-logind[585]: Watching system buttons on /dev/input/event1 (Power Button)
     3  Oct  2 22:34:01 kali systemd-logind[585]: Watching system buttons on /dev/input/event0 (AT Translated Set 2 keyboard)
     4  Oct  2 22:34:02 kali lightdm: pam_unix(lightdm-greeter:session): session opened for user lightdm(uid=116) by (uid=0)
     5  Oct  2 22:34:02 kali systemd-logind[585]: New session c1 of user lightdm.
     6  Oct  2 22:34:02 kali systemd: pam_unix(systemd-user:session): session opened for user lightdm(uid=116) by (uid=0)
     7  Oct  2 22:34:30 kali lightdm: pam_unix(lightdm:auth): check pass; user unknown
     8  Oct  2 22:34:30 kali lightdm: pam_unix(lightdm:auth): authentication failure; logname= uid=0 euid=0 tty=:0 ruser= rhost=
     9  Oct  2 22:35:01 kali CRON[1017]: pam_unix(cron:session): session opened for user root(uid=0) by (uid=0)
    10  Oct  2 22:35:01 kali CRON[1017]: pam_unix(cron:session): session closed for user root

┌──(kali㉿kali)-[/var/log]
└─$ shred -f -n 10 auth.log
shred: auth.log: failed to open for writing: Operation not permitted

┌──(kali㉿kali)-[/var/log]
└─$ sudo shred -f -n 10 auth.log
[sudo] password for kali:

┌──(kali㉿kali)-[/var/log]
└─$ cat -n auth.log | head -n 5
     1  �ƭǢqC
              > �Yu6��ulh��y�@��d�V
�����A�!7~��u�ϢZ���$8l��q��j�)��V�O��=��^i�28ԽaP;�|?���g0���}nD*j�q�U�Zgk��!L�'HJ���UW��~������9x?ek]mo.�[&j�V����t��L0�I       +a��e�P�zWύuMl+�*�S��0O���w��
�^yVC[aU.tKe2�Z��
....
┌──(kali㉿kali)-[/var/log]
└─$
```
碎片化历史命令文件
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tail -n 5 .bash_history
ansible 192.168.26.153 -m  shell -a 'mysql -uroot -pliruilong -e"select user,host from mysql.user;"'
ansible db_node -m  shell -a 'mysql -uroot -pliruilong -e"select user,host from mysql.user;"'
ansible db_node -m  shell -a 'mysql -uroot -pliruilong -e"select user,host,Repl_slave_priv from mysql.user;"'
ansible db_node -m shell -a 'wget https://github.com/lzimd/mha-rpms/blob/master/mha4mysql-node-0.56-0.el6.noarch.rpm'
ansible db_node -m shell -a 'rpm -ivh mha4mysql-node-0.56-0.el6.noarch.rpm'
┌──[root@vms152.liruilongs.github.io]-[~]
└─$shred -f -n 20 .bash_history
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tail -n 5 .bash_history
�/��R�j��a��.���Ew��=���E��U#Ibٽ��[�#�gA��|��� �KUv��,
:Bр���v�:�,�Փrh:y�A�����V��R�@U��O�I�����՚L��1h���G��ڦ`�E.U�.Ϲ��7Vն���)�}�����i��),&��^e���рsQC���ϩ�ҋ� &Uu�X:�n��}����`�4�UnuInx���T�>�n�<1�<���:�yr�-W����?z
                      %�T1�İN[�A��68�,*N�'�cΨ���Z�8�G���.6�s�
&�7���RCO:�##֤�~�z~�Z_Vs�7�r�D�
                                 #���k89���]H� G�!�<���g�L�B?�#�W�S����\��q�F@����`E�>Ϛ�P�H-3�`Kx�e����l�1��i�q�3��|ؐX]���`�t*g�S|�C�:s���b����N����Iت�6B�޸4&
�ͭ��a��䮧���;Ca�p<sˉ{�<�����fR��Q �)Y�tl��.��o
                                                 nO#g>
                                                      P'��B���� ��
                                                                  ��" ���7�����1�90��ބ�D�z���4���T��D�n�ќ.��Ux�i�������hj֚�kH��&����4���Z|칳�"��E�G��"4���`Ǜ��d��Fk:S��M�~ �����2�A9s���7��ҩ�
�siSd�M���T��`�bciO��ܱ��HX�&/�/���[[�6/ވ9w6�6���w�;��
`R�p��R�y}pibt64-�7���h���/��S ����V�`�))5�1AP}Ր,��s�� ��_���xe�q �2'y��֦^2a�v���=�~�33_
....................
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```
##### 禁用日志记录


另一个覆盖跟踪的选项是禁用日志记录。当黑客控制了一个系统，他们可以立即禁用日志，以防止系统跟踪他们的活动。当然，这需要**root**特权。要禁用所有日志记录，黑客只需停止 **rsyslog** 守护进程。停止Linux中的任何服务使用相同的语法

这里还有一点，即删除日志的同时选，需要把历史记录删除


### 安全和匿名

今天，几乎我们在互联网上做的一切都被跟踪。我们的每一次在线活动都会被记录、编入索引，然后被挖掘出来，以造福他人。普通个人，尤其是黑客，需要了解如何限制这种跟踪，并在网上保持相对匿名，以限制这种无处不在的监视。

在本章中，我们将介绍如何使用以下四种方法匿名(或尽可能接近)浏览互联网：
- The Onion Network:洋葱网络
- 代理服务器
- 虚拟专用网络
- 私有加密的电子邮件

`没有一种方法能保证你的活动不被窥探，如果有足够的时间和资源，任何事情都能被追踪。然而，这些方法可能会使跟踪器的工作更加困难。`


当数据包在互联网上传播时，任何截获数据包的人都可以看到是谁发送了数据包，它到过哪里，它要去哪里。这是网站可以告诉您到达目的地并自动登录的一种方式，也是有人可以跟踪您在互联网上的位置的方式。

理论的HTTPS是安全的，可以做到防窃听

要查看数据包在您和目的地之间的跳转情况，可以使用 **traceroute** 命令

```bash
┌──(kali㉿kali)-[~]
└─$ traceroute googlle.com
traceroute to googlle.com (95.216.161.60), 30 hops max, 60 byte packets
 1  192.168.26.2 (192.168.26.2)  0.641 ms  0.592 ms  0.550 ms
 2  * * *
 3  * * *
 4  * * *
 5  * * *
 6  * * *
 7  * * *
 8  * * *
 9  * * *
10  * * *
11  * * *
12  * * *
13  * * *
14  * * *
15  * * *
16  * * *
17  * * *
18  * * *
19  * * *
20  * * *
21  * * *
22  * * *
23  * * *
24  * * *
25  * * *
26  * * *
27  * * *
28  * * *
29  * * *
30  * * *

┌──(kali㉿kali)-[~]
└─$ traceroute www.baidu.com
traceroute to www.baidu.com (220.181.38.149), 30 hops max, 60 byte packets
 1  192.168.26.2 (192.168.26.2)  0.179 ms  0.158 ms  0.133 ms
 2  * * *
 3  * * *
 4  * * *
 5  * * *
 6  * * *
 7  * * *
 8  * * *
 9  * * *
10  * * *
11  * * *
12  * * *
13  * * *
14  * * *
15  * * *
16  * * *
17  * * *
18  * * *
19  * * *
20  * * *
21  * * *
22  * * *
23  * * *
24  * * *
25  * * *
26  * * *
27  * * *
28  * * *
29  * * *
30  * * *

┌──(kali㉿kali)-[~]
└─$
```

#### 洋葱路由系统

上世纪90年代，美国海军研究办公室(ONR)着手开发一种匿名浏览互联网进行间谍活动的方法。该计划是建立一个路由器网络，它与互联网的路由器是分开的，可以加密流量，并且只存储前一个路由器的未加密IP地址-这意味着一路上所有其他路由器地址都加密了。他们的想法是，任何观察流量的人都无法确定数据的来源或目的地。

这项研究在2002年被称为`洋葱路由器(Tor)项目`，现在任何人都可以使用它在web上进行相对安全且匿名的导航。

##### Tor是如何工作的

通过Tor发送的数据包不是通过`常规路由器发送的`，而是通过全世界`7000多台路由器组成的网络发送`的，这要感谢志愿者们允许Tor使用他们的计算机。在使用完全独立的路由器网络的基础上，Tor加密每个数据包的数据、目的地和发送方IP地址。

![在这里插入图片描述](https://img-blog.csdnimg.cn/565362f58d154421a571ec732dc52f1f.png)


在每一跳，接收到的信息被加密，然后在下一跳被解密。这样，`每个包只包含关于路径上的前一跳的信息，而不包含源的IP地址`。如果有人拦截流量，他们只能看到上一跳的IP地址，网站所有者只能看到发送流量的最后一个路由器的IP地址(参见图13-1)，这确保在互联网上的相对匿名性。

要启用 Tor，只需从 [`https://www.torproject.org/`](https://www.torproject.org/) 安装 Tor 浏览器。

![在这里插入图片描述](https://img-blog.csdnimg.cn/a4fa7977a50042de8304f239168e9e7d.png)

不幸的是，权衡的结果是通过 Tor 浏览器浏览会慢得多，因为没有那么多路由器，网络带宽有限。

除了能够访问传统互联网上几乎任何网站外，`Tor浏览器还能够访问暗网`。构成暗网的网站需要匿名，因此它们只允许通过Tor浏览器访问，并且它们的地址以`.onion`结尾用于其`顶级域名(TLD)`。暗网因非法活动而臭名昭着，但也有一些合法的服务。但请注意：在访问暗网时，您可能会遇到很多人会觉得冒犯的内容。

##### 安全问题

美国和其他国家的情报和间谍机构认为Tor网络是对国家安全的威胁，认为这样一个匿名网络可以让外国政府和恐怖分子在不被监视的情况下进行交流。因此，许多强大的、雄心勃勃的研究项目正在努力打破Tor的匿名性。

Tor的匿名曾经被这些当局破坏过，而且很可能会再次被破坏。例如，NSA运行自己的Tor路由器，这意味着当您使用Tor时，您的流量可能正在通过NSA的路由器。如果您的流量正在退出NSA的路由器，那就更糟了，因为退出路由器总是知道您的目的地。NSA还有一种被称为“流量关联”(traffic correlation)的方法，该方法涉及寻找进出流量的模式，能够打破Tor的匿名性。虽然这些破解Tor的尝试不会影响Tor在商业服务(如Google)中隐藏您身份的有效性，但它们可能会限制浏览器在间谍机构中保持匿名的有效性。

#### 代理服务器

在互联网上实现匿名的另一种策略是使用代理，代理是充当流量中间商的中间系统：用户连接到代理，并在传递代理之前向其提供流量的IP地址。当流量从目标返回时，代理将流量发送回源IP。通过这种方式，流量似乎来自代理，而不是原始IP地址。

![在这里插入图片描述](https://img-blog.csdnimg.cn/07167d1112bf4c56a36916e5262e3b13.png)

Kali
Linux有一个优秀的代理工具，称为proxychain，您可以设置它来隐藏您的流量。proxychain命令的语法很简单

`kali \>proxychains \<the command you want proxied\> \<arguments\>`

您提供的参数可能包括一个IP地址。例如，如果您想使用命令nmap以通过代理链匿名方式扫描一个站点，则应输入以下内容：

kali \>proxychains nmap -sT - Pn \<IP address\>

这将通过代理将nmap -sS扫描命令隐身发送到给定的IP地址。然后，该工具自己构建代理链，因此您不必担心它。


##### 在配置文件中设置代理

需要注意的是，如果您没有输入自己的代理，那么proxychain默认使用Tor。
```bash
┌──(kali㉿kali)-[~]
└─$ cat  /etc/proxychains4.conf | grep -v ^# | grep -v ^$
strict_chain
proxy_dns
remote_dns_subnet 224
tcp_read_time_out 15000
tcp_connect_time_out 8000
[ProxyList]
socks4  127.0.0.1 9050

┌──(kali㉿kali)-[~]
└─$
┌──(kali㉿kali)-[~]
└─$
```

我们可以通过输入要在此列表中使用的代理的IP地址和端口来添加代理。现在，我们将使用一些免费代理。你可以通过谷歌搜索“免费代理”或使用网站 [http://www.hidemy.name](http://www.hidemy.name) 找到免费代理，

[https://hidemy.name/cn/](https://hidemy.name/cn/)

##### 添加更多的代理

首先，让我们向列表中添加更多代理。回到http://www.hidemy.name并找到更多的代理IP地址。然后在proxychain
.conf文件中添加更多的代理，如下所示:

[ProxyList]

\# add proxy here...

socks4 114.134.186.12 22020

socks4 188.187.190.59 8888

socks4 181.113.121.158 335551

现在保存这个配置文件，并尝试运行以下命令:

kali \>proxychains firefox www.hackers-arise.com


#### 虚拟专用网络

使用虚拟专用网(VPN)是保持您的web流量相对匿名和安全的有效方法。VPN用于连接到一个中间的互联网设备，例如路由器，它将您的流量发送到其最终目的地，并使用该路由器的IP地址进行标记。

使用VPN当然可以增强您的安全性和隐私，但它不能保证匿名。您所连接的互联网设备必须记录或记录您的IP地址，以便能够正确地将数据发送回您，这样任何能够访问这些记录的人都可以发现关于您的信息。

VPN的优点是简单易用。您可以在VPN提供商那里开户，然后在每次登录计算机时无缝连接VPN。您可以像往常一样使用浏览器来浏览web，但是任何人看到您的流量来自互联网
VPN设备的IP地址和位置，而不是您自己的。另外，您与VPN设备之间的所有通信都是加密的，所以即使您的互联网服务提供商也看不到您的通信。

除此之外，VPN可以有效地规避政府控制的内容和信息审查。例如，如果您的国家政府限制您访问具有特定政治信息的网站，您可以使用您所在国家/地区以外的VPN来访问该内容。

一些媒体公司，如Netflix，Hulu和HBO，将其内容的访问权限限制为源自其本国的IP地址。在这些服务允许的国家/地区使用VPN通常可以帮助您解决这些访问限制。

根据CNET，一些最好和最受欢迎的商业VPN服务如下：

-   IPVanish
-   NordVPN
-   ExpressVPN
-   CyberGhost
-   Golden Frog VPN
-   Hide My Ass (HMA)
-   Private Internet Access
-   PureVPN
-   TorGuard
-   Buffered VPN

#### 加密的电子邮件

免费的商业电子邮件服务，如Gmail，雅虎!，Outlook Web Mail(前身为Hotmail)的免费是有原因的：`它们是追踪你的兴趣和提供广告的工具。如前所述，如果服务是免费的，那么您就是产品，而不是客户`。此外，电子邮件提供者的服务器(例如谷歌)可以访问电子邮件的未加密内容，即使您使用HTTPS。

防止窃听你的电子邮件的一个方法是使用加密的电子邮件。`ProtonMail`，从一端加密到另一端或从一个浏览器加密到另一个浏览器。这意味着您的电子邮件是在ProtonMail服务器上加密的，即使是ProtonMail管理员也无法读取您的电子邮件。

ProtonMail是由一群年轻的科学家在瑞士欧洲核子研究中心(CERN)的超级对撞机设施工厂建立的。瑞士在保护机密方面有着悠久而传奇的历史(还记得你经常听说的那些瑞士银行账户吗?)

ProtonMail的服务器位于欧盟，该网络在分享个人数据方面有着比美国法律更为严格的法律规定。ProtonMail不收取基本帐户，但提供保费帐户象征性的费用。需要注意的是，在与非protonmail用户交换电子邮件时，可能有部分或全部电子邮件没有加密。有关详细信息，请参见ProtonMail支持知识库。


### 理解和检查无线网络

这里需要无线网卡，所以Demo做简单记录

#### WI-FI 网络

我们先从Wi-Fi开始。在本节中，我将向您展示如何查找、检查和连接Wi-Fi接入点。了解一些基本的Wi-Fi术语和技术

+ **AP (access point)** 这是无线用户连接到互联网的设备。
+ **ESSID (extended service set identifier)** 这和SSID相同，不过它可以用于无线局域网中的多个AP中。
+ **BSSID (basic service set identifier)** 这是每个AP的唯一标识符，与设备的MAC地址相同。
+ **SSID (service set identifier)** 这是网络的名称。
+ **Channels** Wi­Fi可以在14个信道(1-14)中的任何一个信道上工作。在美国，Wi-Fi被限制在信道1到11。
+ **Power** 你距离 WI-Fi AP越近,功率越大，连接越容易破解。
+ **Security** 这是正在读取的WiFi AP上使用的安全协议。 WiFi有三种主要的安全协议。最初的有线等效保密`(WEP)`存在严重缺陷，容易破解。它的替代品，WiFi保护访问`(WPA)`，更安全一点。最后，`WPA2-PSK`更加安全并且使用所有用户共享的预共享密钥`(PSK)`，现在几乎所有WiFi AP(企业WiFi除外)都使用它。
+ **Modes** Wi­Fi可以在三种模式下切换：managed, master,或者monitor。
+ **Wireless range** 在美国，Wi-Fi AP必须合法地以0.5瓦的上限广播其信号。在此功率下，它具有约300英尺(100米)的正常范围。`Highgain天线可以将此范围扩展到20英里`。
+ **Frequency** Wi­Fi被设计用于2.4GHz和5GHz。 现代Wi-Fi AP和无线网卡经常使用这两者。

##### *基本无线网络命令*
运行ifconfig的结果，这次重点关注无线连接。
```bash 
kali \>ifconfig
eth0Linkencap:EthernetHWaddr 00:0c:29:ba:82:0f
inet addr:192:168.181.131 Bcast:192.168.181.255 Mask:255.255.255.0
­­snip­­
lo Linkencap:Local Loopback
inet addr:127.0.0.1 Mask:255.0.0.0
­­snip­­
➊ wlan0 Link encap:EthernetHWaddr 00:c0:ca:3f:ee:02
```
这里的Wi-Fi接口显示为 wlan0 ➊。在Kali Linux中，Wi-Fi接口通常被指定为wlanX，X代表该接口的编号。换句话说，系统上的第一个Wi-Fi适配器将标记为wlan0，第二个是wlan1，依此类推。

如果您只想查看Wi-Fi接口及其统计数据，Linux有一个与 ifconfig 类似但专用于无线的特定命令。这个命令是iwconfig。输入时，仅显示无线接口及其关键数据：
```bash 
kali \>iwconfig
lo no wireless extensions
wlan0 IEEE 802.11bg ESSID:off/any
Mode:Managed Access Point:Not­Associated Tx­Power=20 dBm
Retry short limit:7 RTS thr:off Fragment thr:off
Encryption key:off
Power Management:off
eth0 no wireless extensions
```

在这里，我们只看到无线接口(也称为网卡)及其关键数据，包括使用的无线标准、ESSID是否关闭以及模式。该模式有三个设置：*managed*，这意味着它已准备好加入或已加入AP；*master*，这意味着它已准备好充当或已经加入AP；以及*monitor*，我们将在章节稍后讨论。我们还可以看到是否有任何客户端与之相关联，以及它的传输功率是什么，等等。从这个例子可以看出，wlan0处于连接Wi-Fi网络所需的模式，但尚未连接到任何网络。一旦无线接口连接到Wi-Fi网络，我们将再次访问此命令。

如果您不确定要连接哪一个Wi-Fi AP，可以使用iwlist命令查看您的网卡可以访问的所有无线接入点。iwlist的语法如下：`iwlist interface action`

可以使用iwlist执行多个操作。出于我们的目的，我们将使用扫描操作来查看您所在地区的所有Wi-Fi AP。(请注意，使用标准天线，您的范围将为300-500英尺，但这可以通过廉价的高增益天线来扩展。)
```bash 
kali \>iwlist wlan0 scan
wlan0 Scan completed:
Cell 01 ­ Address:88:AD:43:75:B3:82
Channel:1
Frequency:2.412GHz (Channel 1)
Quality=70/70 Signal level =­38 dBm
Encryption key:off
ESSID:"Hackers­Arise"
­­snip­­
```

此命令的输出应包括无线接口范围内的所有WiFi AP，以及每个AP的关键数据，比如这个AP的MAC地址，其运行的信道和频率，其质量，信号电平，是否启用了加密密钥及其ESSID。

为了进行各种不同的入侵攻击，你将需要目标AP的MAC地址(BSSID)，客户端的MAC地址(另一个无线网卡)以及AP操作的通道来执行任何类型的黑客攻击，因此这是有价值的信息。

另一个在管理WiFi连接时非常有用的命令是nmcli(或网络管理器命令行界面)。为网络接口(包括无线接口)提供高级接口的Linux守护程序称为网络管理器。通常，Linux用户从其图形用户界面(GUI)熟悉此守护程序，但也可以从命令行使用它。

nmcli 命令可以使用来查看你附近的Wi-Fi AP和它们的一些关键信息，像我们使用iwlist一样，但此命令为我们提供了更多信息。我们以nmcli
dev networktype格式使用它，其中dev是设备(device)的简称以及类型(在本例中)是wifi，如下所示：
```bash 
kali \>nmcli dev wifi
\* SSID MODE CHAN RATE SIGNAL BARS SECURITY
Hackers­Arise Infra 1 54 Mbits/s 100 WPA1 WPA2
Xfinitywifi Infra 1 54 Mbits/s 75 WPA2
TPTV1 Infra 11 54 Mbits/s 44 WPA1 WPA2
­­snip­­
```

除了在范围内显示Wi-Fi AP以及关于它们的关键数据(包括SSID，模式，信道，传输速率，信号强度和设备上启用的安全协议)之外，还可以使用nmcli连接到AP。 

连接到AP的语法如下：`nmcli dev wifi connect AP-SSID password APpassword`

因此，根据我们的第一个命令的结果，我们知道有一个SSID为Hackers-Arise的AP。我们也知道它具有WPA1 WPA2安全性(这意味着AP能够同时使用旧的WPA1和新的WPA2)，这意味着我们必须提供连接到网络的密码。幸运的是，因为它是我们的AP，我们知道密码是12345678，所以我们可以输入以下内容：
```bash 
kali \>nmcli dev wifi connect Hackers-Arise password 12345678
Device 'wlan0' successfully activated with '394a5bf4­8af4­36f8­49beda6cb530'.
```
在一个你知晓的网络尝试此操作，然后当你成功连接这个无线AP时，再一次运行iwconfig来观察一下有什么变化。下面是我连接Hackers­-Arise的输出结果：
```bash  
kali \>iwconfig
lo no wireless extensions
wlan0 IEEE 802.11bg ESSID:"Hackers­Arise"
Mode:Managed Frequency:2.452GHz Access Point:00:25:9C:97:4F:48
Bit Rate=12 Mbs Tx­Power=20 dBm
Retry short limit:7 RTS thr:off Fragment thr:off
Encryption key:off
Power Management:off
Link Quality=64/70 Signal level=­46 dBm
Rx invalid nwid:0 Rx invalid crypt:0 Rx invalid frag:0
Tx excessive reties:0 Invalid misc:13 Missed beacon:0
eth0 no wireless extensions
```

请注意，现在iwconfig已经指出ESSID是“Hackers-Arise”，并且AP的运行频率为2.452GHz。在Wi-Fi网络中，多个接入点可能都是同一网络的一部分，因此可能会有许多接入点组成Hackers-Arise的网络。如您所料，MAC地址00:25:9C:97:4F:48是我所连接的AP的MAC。Wi-Fi网络使用的安全协议类型，无论是2.4GHz还是5GHz的运行频率，其ESSID是什么，以及AP的MAC地址是什么，都是Wi-Fi入侵攻击所必需的关键信息。既然你现在了解了基本的命令，那么让我们来进行一些黑客入侵攻击。

#### *使用aircrack-ng进行Wi-Fi侦查*

对于入门黑客最受欢迎的攻击之一就是破解Wi-Fi接入点。如前所述，在考虑攻击Wi-Fi AP之前，你需要目标AP(BSSID)的MAC地址、客户端的MAC地址以及AP正在运行的信道。

我们可以使用aircrack－­ng套件的工具获取尽可能多的信息。我之前提到过这套Wi-Fi黑客工具，现在是时候实际使用了。这套工具包含在Kali的每个版本中，所以你不需要下载或安装任何东西。

要有效地使用这些工具，首先需要将无线网卡置于监视器模式，以便卡能够看到所有经过它的流量。通常情况下，网卡只捕获特定于该卡的流量。监控模式类似于有线网卡上的混杂模式。

要将无线网卡置于监视器模式，请使用Aircrack-ng套件中的airmon-ng命令。此命令的语法很简单：
```bash 
airmon­ng 　start\|stop\|restart interface
```
因此，如果要将无线网卡(指定为WLAN)设置为监视器模式，请输入以下内容：
```bash 
kali \>airmon-ng start wlan0
```
发现了三个可能导致故障的过程，如果airodump-ng、aireplay-ng或airtun-ng在短时间运行后停止工作，你可能需要运行“airmon-ng check kill”。
```bash 
­­snip­­
PHY INTERFACE DRIVER Chipset
phy0 wlan0 rt18187 Realtek Semiconductor Corop RTL8187 (mac8311 monitor mode vif
enabled for [phy0]wlan0 on [phy0]wlan0mon)
­­snip­­
```
如果遇到故障，分别使用stop和restart命令来恢复，比如stop monitor模式和restart monitor模式。

当无线网卡处于监视器模式时，你可以在无线网络适配器和天线的范围内访问经过你网卡的所有无线通信(标准约300-500英尺)。请注意，airmon－ng将重命名你的无线接口：我的无线网卡已重命名为“wlan0mon”，尽管你的可能有所不同。一定要记下无线的新的指定名称，因为在下一步中您需要这些信息。

现在，我们将使用Aircrack-ng套件中的另一个工具从无线通信中查找关键数据。airodump-ng命令捕获并显示来自广播AP和连接到这些AP或附近的任何客户端的关键数据。这里的语法很简单：只需插入airdump-ng，后跟刚才运行airmon-ng得到的接口名。当你执行此命令时，你的无线卡将从附近AP的所有无线通信中获取关键信息(如下所示)：

+ **BSSID** AP或客户端的MAC地址
+ **PWR** 信号强度
+ **ENC** 用于保护传输安全的加密类型
+ **\#Data** 数据吞吐量
+ **CH AP**运行的信道
+ **ESSID** AP的名字

```bash 
kali \>airodump-ng wlan0mon
CH 9][ Elapsed: 28 s ][ 2018­02­08 10:27
BSSID PWR Beacons \#Data \#/s CH MB ENC CIPHER AUTH ESSID
01:01:AA:BB:CC:22 ­1 4 26 0 10 54e WPA2 CCMP PSK Hackers­Arise
­­snip­­
BSSID Station PWR Rate Lost Frames Probe
(not associated) 01:01:AA:BB:CC:22
01:02:CC:DD:03:CF A0:A3:E2:44:7C:E5
```

注意airodump-ng将输出屏幕分成上下两部分。上面部分包含广播AP的信息，包括BSSID、AP的功率、检测到的信标帧数、数据吞吐量、通过无线网卡的数据包数、信道(1-14)、理论吞吐量限制、加密协议、用于加密的密码类型、身份验证离子类型和ESSID(通常称为SSID)。在客户机部分，输出告诉我们一个客户机没有关联，这意味着它已经被检测到但没有连接到任何AP，另一个客户机与一个工作站关联，这意味着它在该地址连接到AP。

现在你已经掌握了破解AP所需的所有信息！尽管这超出了本书的范围，但要破解无线AP，你需要客户端MAC地址、APMAC地址、目标操作的信道和密码列表。

为了破解Wi-Fi密码，你需要打开三个终端。在第一个终端中，您将输入类似以下的命令，填写客户机和AP
MAC地址以及通道：
```bash 
airodump­ng ­c 10 ­­bssid 01:01:AA:BB:CC:22 ­w Hackers­ArisePSK wlan0mon
```
此命令使用**-c**选项捕获通过信道10上的AP的所有数据包。

在另一个终端中，你可以使用 **aireplayng** 命令取消(取消身份验证)与AP连接的任何人，并强制他们重新身份验证到AP，如下所示。当他们重新验证时，你可以捕获在WPA2-PSK四次握手中交换的密码散列。密码散列将出现在airodump-ng终端的右上角。
```bash 
aireplay­ng ­­deauth 100 ­a 01:01:AA:BB:CC:22­c A0:A3:E2:44:7C:E5 wlan0mon
```
最后，在最后一个终端中，你可以使用密码列表(wordlist.dic)在捕获的散列(hacker-arisesk.cap)中查找密码，如下所示：
```bash 
aircrack­ng ­w wordlist.dic ­b 01:01:AA:BB:CC:22 Hacker­ArisePSK.cap
```

### 检测并连接到蓝牙

如今，几乎所有的小器具、移动设备和系统都内置了蓝牙，包括我们的电脑、智能手机、iPod、平板电脑、扬声器、游戏控制器、键盘和许多其他设备。能够入侵蓝牙会导致设备上任何信息的泄露，设备的控制，以及向设备发送不需要的信息的能力等等威胁。

为了利用这项技术，我们需要了解它是如何工作的。对蓝牙的深入了解已经超出了本书的范围，但我将向您提供一些基本知识，帮助你扫描和连接蓝牙设备，为黑客的入侵攻击做好准备。

#### *蓝牙的工作原理*

蓝牙是一种用于`低功耗近场通信的通用协议`，使用扩频在`2.4-2.485GHz下工作，跳频速度为每秒1600跳(这种跳频是一种安全措施)`。它由瑞典爱立信公司于1994年开发，以10世纪丹麦国王哈拉尔德蓝牙命名(请注意，瑞典和丹麦在10世纪是一个单一的国家)。

`蓝牙规范的最小范围为10米，但没有上限`，制造商可以在其设备中实施。许多装置的射程可达100米。有了特殊的天线，这一范围可以扩大得更远。

连接两个蓝牙设备被称为`配对`。几乎任何两个蓝牙设备都可以相互连接，但只有在处于`可发现模式时才能配对`。处于可发现模式的蓝牙设备传输以下信息：
- 名称
- 类别
- 服务清单
- 技术信息

当两个设备配对时，它们交换一个`密钥或链接密钥`。每个存储这个链接键，以便在将来的配对中识别另一个。

每个设备都有一个唯一的`48位标识符(类似于MAC的地址)`，通常还有一个制造商指定的名称。当我们想要识别和访问一个设备时，这些将是有用的数据片段。

#### *蓝牙扫描与侦察*

Linux有一个称为`bluez的蓝牙协议栈`的实现，我们将使用它来扫描蓝牙信号。大多数Linux发行版，包括Kali Linux，都默认安装了它。如果没有，通常可以使用以下命令在存储库中找到它：
```bash 
kali \>apt-get install bluez
```
BlueZ有许多简单的工具，我们可以用来管理和扫描蓝牙设备，包括：

+ **hciconfig** 这个工具的操作与Linux中的ifconfig非常相似，但是对于蓝牙设备。如清单14-1所示，我使用它来打开蓝牙接口并查询设备的规格。

+ **hcitool** 此查询工具可以为我们提供设备名称、设备ID、设备类和设备时钟信息，使设备能够同步工作。

+ **hcidump** 这个工具使我们能够嗅探蓝牙通信，这意味着我们可以捕获通过蓝牙信号发送的数据。

蓝牙的第一个扫描和侦察步骤是检查我们使用的系统上的蓝牙适配器是否被识别和启用，以便我们可以使用它扫描其他设备。我们可以使用内置的Bluez工具hciconfig来实现这一点，如命令清单14-1所示。
```bash 
kali \>hciconfig
hci0: Type: BR/EDR Bus: USB
BD Address: 10:AE:60:58:F1:37 ACL MTU: 310:10 SCO MTU: 64:8 UP RUNNING PSCAN INQUIRY
RX bytes:131433 acl:45 sco:0 events:10519 errors:0 TX bytes:42881 acl:45 sco:0
commands:5081 errors:0
```

如你所见，我的蓝牙适配器的MAC地址是10:ae:60:58:f1:37。此适配器已命名为HCI0。下一步是检查连接是否已启用，我们也可以通过提供名称和up命令来使用hciconfig:
```bash 
kali \>hciconfig hci0 up
```

如果命令成功运行，我们将看不到输出，只看到一个新的提示。很好，hci0准备好了！让我们开始工作吧。

##### 使用hcitool扫描蓝牙设备

现在我们知道适配器已经启动，我们可以使用Bluez套件中的另一个工具hcitool来扫描范围内的其他蓝牙设备。

让我们首先使用这个工具的扫描功能来查找发送发现信标的蓝牙设备，这意味着它们处于发现模式，简单的扫描命令如清单14-2所示。
```bash 
kali \>hcitool
Scanning...
72:6E:46:65:72:66 ANDROID BT
22:C5:96:08:5D:32 SCH­I535
```

如你所见，在我的系统中，hcitool发现了两个设备，ANDROIDBT和SCH­I535。你的可能会根据您周围的设备提供不同的输出。出于测试目的，请尝试将你的手机或其他蓝牙设备置于发现模式，并查看扫描时是否接收到该设备。

现在，让我们收集有关具有查询功能的已检测设备的更多信息：
```bash 
kali \>hcitool inq
Inquiring...
24:C6:96:08:5D:33 clock offset:0x4e8b class:0x5a020c
76:6F:46:65:72:67 clock offset:0x21c0 class:0x5a020c
```

这给了这给了我们设备的MAC地址、时钟偏移量和设备类别。该类指示您找到的蓝牙设备的类型，您可以通过访问蓝牙SIG站点(https://www.bluetooth.org/en-us/specification/assigned-numbers/service-discovery/)来查找代码并查看它是什么类型的设备。

工具hcitool是一个强大的命令行接口蓝牙栈，可以做很多很多事情。清单14-3显示了帮助页面，其中包含一些可以使用的命令。亲自查看帮助页面以查看完整列表。
```bash 
kali \>hcitool --help
hcitool ­ HCI Tool ver 4.99
Usage:
hcitool [options] \<command\> [command parameters]
Options:
­­help Display help
­i dev HCI device
Commands
dev Display local devices
inq Inquire remote devices
scan Scan for remote devices
name Get name from remote devices
­­snip­­
```

许多蓝牙黑客工具，你将看到周围简单地使用这些命令在脚本，你可以很容易地创建你自己的工具使用这些命令在你自己的bash或python脚本，我们将在第17章研究这些脚本。

##### 使用sdptool扫描服务

服务发现协议(SDP)是一种用于搜索蓝牙服务的蓝牙协议(蓝牙是一套服务)，而且，很有帮助的是，bluez提供了sdp
tool工具，用于浏览设备上提供的服务。还需要注意的是，设备不必处于要扫描的发现模式。语法如下：

```bash 
sdptool browse MACaddress
```

显示使用sdptool在前面检测到的某个设备上搜索服务。
```bash 
kali \>sdptool browse 76:6E:46:63:72:66
Browsing 76:6E:46:63:72:66...
Service RecHandle: 0x10002
Service Class ID List:
""(0x1800)
Protocol Descriptor List:
"L2CAP" (0x0100)
PSM: 31
"ATT" (0x0007)
uint16: 0x1
uint16: 0x5
­­snip­­
```

在这里，我们可以看到sdptool工具能够获取关于这个设备能够使用的所有服务的信息。特别地，我们看到这个设备支持ATT协议，即低能量属性协议。这可以为我们提供更多关于设备是什么的线索，以及可能的进一步与之交互的途径。

##### 通过l2ping查看设备是否可以访问

一旦我们收集了所有附近设备的MAC地址，我们就可以向这些设备发送ping，无论它们是否处于发现模式，以查看它们是否在可访问范围内。这让我们知道它们是否在活动范围内。要发送ping，我们使用L2ping命令，语法如下：`l2ping MACaddress`

发现的Android设备执行ping操作。
```bash 
kali \>l2ping 76:6E:46:63:72:66 -c 4
Ping: 76:6E:46:63:72:66 from 10:AE:60:58:F1:37 (data size 44)...
44 bytes 76:6E:46:63:72:66 id 0 time 37.57ms
44 bytes 76:6E:46:63:72:66 id 1 time 27.23ms
44 bytes 76:6E:46:63:72:66 id 2 time 27.59ms
­­snip­­
```
此输出表示MAC地址为76:6e:46:63:72:66的设备在范围内且可访问。这是有用的知识，因为在我们考虑黑客攻击之前，我们必须知道设备是否可以访问。

### 总结

无线设备代表未来的网络连通和黑客入侵攻击的核心。Linux已经开发了专门的命令来扫描和连接Wi-Fi接入点，这是黑客攻击这些系统的第一步。aircrack-ng无线黑客工具套件包括airmon-ng和airodump-ng，使我们能够从远程无线设备。Bluez套件包括hciconfig、hcitool和其他能够扫描和收集信息的工具，这些工具是在一定范围内对蓝牙设备进行黑客攻击所必需的。它还包括许多其他值得探索的工具。

## 博文整理来自


《Linux Basics for Hackers_ Getting Started with Networking, Scripting, and Security in Kali 》