---
title: 关于 Linux 中 iptables 升级的一些笔记
tags:
  - iptables
categories:
  - iptables
toc: true
recommend: 1
keywords: iptables
uniqueId: '2023-04-01 11:23:39/关于 Linux 中 iptables 升级的一些笔记.html'
mathJax: false
date: 2023-04-01 19:23:39
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 学习 K8s 中 kube-proxy 组件调用相关，iptables 模式 发现调用链有些特殊
+ 怀疑是 iptables 版本太旧的问题，所以升级一下试试
+ 关于升级是否有必要，`nftables` 都出来好久了，这里不做考虑哈
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

这里需要一提的是 ，`k8s` 集群使用 `Calico` 在旧版本的 `iptables` 中有些链的规则不能正常显示，但是并不影响规则。

低版本不能正常显示
```bash
-A KUBE-SEP-OLNIRCQFCXAN5USW -p tcp -m comment --comment "velero/minio:console" -m tcp -j DNAT --to-destination :0 --persistent --to-destination :0 --persistent --to-destination 0.0.0.0:0
```

高版本可以正常显示
```bash
-A KUBE-SEP-OLNIRCQFCXAN5USW -p tcp -m comment --comment "velero/minio:console" -m tcp -j DNAT --to-destination 10.244.169.89:9090
```





当前系统环境版本

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$hostnamectl
   Static hostname: vms100.liruilongs.github.io
         Icon name: computer-vm
           Chassis: vm
        Machine ID: e93ae3f6cb354f3ba509eeb73568087e
           Boot ID: 3276184063244f00bc6efff60ef31879
    Virtualization: vmware
  Operating System: CentOS Linux 7 (Core)
       CPE OS Name: cpe:/o:centos:centos:7
            Kernel: Linux 5.4.230-1.el7.elrepo.x86_64
      Architecture: x86-64
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$
```

下载二进制包位置

https://www.netfilter.org/projects/iptables


```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$wget --no-check-certificate  https://www.netfilter.org/projects/iptables/files/iptables-1.8.7.tar.bz2
```
安装解压工具
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$yum -y install bzip2
```
解压二进制安装包
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$bunzip2 iptables-1.8.7.tar.bz2
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$tar -xvf iptables-1.8.7.tar
```

`nftables` 一起升级有些其他的包不支持，好像需要同步升级，所以这里我直接把 `nftables` 排除调

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/iptables-1.8.7]
└─$./configure --disable-nftables
┌──[root@vms100.liruilongs.github.io]-[~/ansible/iptables-1.8.7]
└─$ls
aclocal.m4  build-aux     config.h     config.log     configure     COPYING  extensions  INCOMPATIBILITIES  iptables          libipq   libtool     m4        Makefile.am  stamp-h1  xlate-test.py
autogen.sh  COMMIT_NOTES  config.h.in  config.status  configure.ac  etc      include     INSTALL            iptables-test.py  libiptc  libxtables  Makefile  Makefile.in  utils
┌──[root@vms100.liruilongs.github.io]-[~/ansible/iptables-1.8.7]
└─$make
```

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/iptables-1.8.7]
└─$make install
```

这里发现升级完并没有，生效，还是原来的版本
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/iptables-1.8.7]
└─$iptables -V
iptables v1.4.21
```

重启机器后发现变成了升级后的版本,这里应该不需要重启，可以考虑重新登录或者 `source`  一下

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible]
└─$iptables -V
iptables v1.8.7 (legacy)
```




## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***

https://www.netfilter.org/projects/iptables

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
