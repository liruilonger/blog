---
title: K8s：Kubernetes 故障排除方法论
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-06-18 06:22:57/K8s：Kubernetes 故障排除方法论.html'
mathJax: false
date: 2023-06-18 14:22:57
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 博文内容为节译整理
+ 文中提到的工具大部分是商业软件，不是开源的，作为了解
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


## Kubernetes 故障排除方法论？

Kubernetes 故障排除是`识别、诊断和解决` Kubernetes 集群、节点、Pod 或容器中问题的过程。

更广泛地说，Kubernetes 故障排除还包括`有效的持续故障管理`，并采取措施防止 Kubernetes 组件出现问题。

Kubernetes 故障排除的三大支柱，在 Kubernetes 集群中进行有效的故障排除有三个方面：

+ 了解问题
+ 管理和修复问题
+ 防止问题再次发生

### 了解问题

在 Kubernetes 环境中，很难理解发生了什么并确定问题的根本原因。这通常涉及：

+ 查看最近对受影响的集群、Pod 或节点所做的更改，以查看导致故障的原因。
+ 分析运行故障组件的虚拟机或裸机的 YAML 配置、GitHub 存储库和日志。
+ 查看 Kubernetes 事件和指标，如磁盘压力、内存压力和利用率。在成熟的环境中，您应该有权访问仪表板，这些仪表板显示一段时间内集群、节点、Pod 和容器的重要指标。
+ 比较行为相同的类似组件，并分析组件之间的依赖关系，以查看它们是否与故障相关。

为了实现上述目标，团队通常使用以下技术：

+ 监控工具：`Datadog，Dynatrace，Grafana，New Relic`
+ 可观测性工具：`Lightstep, Honeycomb`
+ 实时调试工具：`OzCode，Rookout`
+ 日志记录工具：`Splunk，LogDNA，Logz.io`


### 管理和修复问题

在微服务体系结构中，通常每个组件都由单独的团队开发和管理。由于生产事件通常涉及多个组件，因此协作对于快速修复问题至关重要。

了解问题后，有三种方法可以修复它：

+ `临时解决方案` : 基于处理受影响组件的团队的部落知识。通常，构建组件的工程师对如何调试和解决它有不成文的知识。
+ `手动运行手册` : 一个清晰的、记录在案的过程，显示如何解决每种类型的事件。拥有运行手册意味着团队的每个成员都可以快速解决问题。
+ `自动化运行手册`:  一种自动化过程，可以作为脚本、基础结构即代码 （IaC） 模板或 Kubernetes 运算符实现，并在检测到问题时自动触发。自动响应所有常见事件可能具有挑战性，但它可能非常有益，可以减少停机时间并消除人为错误。

为了实现上述目标，团队通常使用以下技术：

+ 事件管理：`PagerDuty, Kintaba`
+ 项目管理：`Jira, Monday, Trello`
+ 基础设施即代码：`Amazon CloudFormation、Terraform`

### 预防

成功的团队将预防作为重中之重。随着时间的推移，这将减少用于识别和解决新问题的时间。防止 Kubernetes 中的生产问题涉及：

+ 在每次事件发生后`创建策略、规则和行动手册`，以确保有效补救
+ 调查是否可以`自动响应问题`，以及如何自动执行
+ 定义如何在`下次快速识别问题并提供相关数据` - 例如通过检测相关组件
+ 确保将问题上报给适当的团队，并且这些团队可以有效地沟通以解决问题

为了实现上述目标，团队通常使用以下技术：

+ 混沌工程：`Gremlin, Chaos Monkey, ChaosIQ.`

`Gremlin` 是一个混沌工程平台，它提供了多种故障注入工具，包括网络故障、主机故障、应用程序故障等，可以帮助用户测试系统的弹性和可靠性。

`Chaos Monkey` 是 Netflix 公司开发的一个混沌工程工具，它可以在生产环境中随机关闭实例来测试系统的可靠性和弹性。

`ChaosIQ` 是一个混沌工程平台，它提供了多种故障注入工具，包括网络故障、主机故障、应用程序故障等，可以帮助用户测试系统的弹性和可靠性。它还提供了可视化仪表板和报告，帮助用户分析测试结果和改进系统。

+ 自动修复：`Shoreline, OpsGenie.`

### 集群故障排除

#### 获取集群信息

要在集群中进行调试，请确保所有节点都已正确注册。

```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get nodes
NAME                          STATUS   ROLES           AGE    VERSION
vms100.liruilongs.github.io   Ready    control-plane   141d   v1.25.1
vms101.liruilongs.github.io   Ready    control-plane   141d   v1.25.1
vms102.liruilongs.github.io   Ready    control-plane   141d   v1.25.1
vms103.liruilongs.github.io   Ready    <none>          141d   v1.25.1
vms105.liruilongs.github.io   Ready    <none>          141d   v1.25.1
vms106.liruilongs.github.io   Ready    <none>          141d   v1.25.1
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```

#### 使用dump进行故障排除

获取集群信息：

```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl cluster-info
Kubernetes control plane is running at https://192.168.26.99:30033
CoreDNS is running at https://192.168.26.99:30033/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

使用 `grep` 对关键字进行过滤，排查问题
```
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl cluster-info dump
```

#### 获取群集组件的运行状况

`v1.19+`之后可能要被废弃
```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl get componentstatuses
Warning: v1 ComponentStatus is deprecated in v1.19+
NAME                 STATUS    MESSAGE                         ERROR
scheduler            Healthy   ok
etcd-0               Healthy   {"health":"true","reason":""}
controller-manager   Healthy   ok
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```

### 集群日志记录查找

获取日志的最后五行：

```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl logs argocd-application-controller-0 --tail=5
time="2023-06-16T20:26:58Z" level=warning msg="Failed to save clusters info: dial tcp 10.96.153.48:6379: connect: connection refused"
time="2023-06-16T20:27:08Z" level=warning msg="Failed to save clusters info: dial tcp 10.96.153.48:6379: connect: connection refused"
time="2023-06-16T20:27:18Z" level=warning msg="Failed to save clusters info: dial tcp 10.96.153.48:6379: connect: connection refused"
time="2023-06-16T20:27:28Z" level=warning msg="Failed to save clusters info: dial tcp 10.96.153.48:6379: connect: connection refused"
time="2023-06-16T20:27:38Z" level=warning msg="Failed to save clusters info: dial tcp 10.96.153.48:6379: connect: connection refused"
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```

获取特定于时间的信息

```bash
┌──[root@vms100.liruilongs.github.io]-[~]
└─$kubectl logs  argocd-application-controller-0 --since=20s
time="2023-06-16T20:29:16Z" level=error msg="Failed to cache app resources: error setting app resource tree: dial tcp 10.96.153.48:6379: connect: connection refused" application=argocd/guestbook dedup_ms=0 diff_ms=1 git_ms=15114 health_ms=0 live_ms=0 settings_ms=0 sync_ms=0
time="2023-06-16T20:29:16Z" level=info msg="No status changes. Skipping patch" application=argocd/guestbook
time="2023-06-16T20:29:16Z" level=info msg="Reconciliation completed" application=argocd/guestbook dedup_ms=0 dest-name= dest-namespace=default dest-server="https://kubernetes.default.svc" diff_ms=1 fields.level=0 git_ms=15114 health_ms=0 live_ms=0 settings_ms=0 sync_ms=0 time_ms=15289
time="2023-06-16T20:29:18Z" level=warning msg="Failed to save clusters info: dial tcp 10.96.153.48:6379: connect: connection refused"
time="2023-06-16T20:29:28Z" level=warning msg="Failed to save clusters info: dial tcp 10.96.153.48:6379: connect: connection refused"
┌──[root@vms100.liruilongs.github.io]-[~]
└─$
```







## 博文部分内容参考

© 文中涉及参考链接内容版权归原作者所有，如有侵权请告知


***


<https://medium.com/@sudheer.barakers/kubernetes-troubleshooting-3bc4c3a3cb9a>


***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
