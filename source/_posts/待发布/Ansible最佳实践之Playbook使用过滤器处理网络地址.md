---
title: Ansible最佳实践之Playbook使用过滤器处理网络地址
tags:
  - Ansible
categories:
  - Ansible
toc: true
recommend: 1
keywords: Ansible
uniqueId: '2022-08-12 13:44:23/Ansible最佳实践之Playbook使用过滤器处理网络地址.html'
mathJax: false
date: 2022-08-12 21:44:23
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 使用过滤器检查、验证和操作包含网络信息的变量
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***


### 收集和处理网络信息

标准 `setup` 模块可在多个 `play` 开头自动收集事实，从每个受管主机上收集大量与网络相关的信息。

常用的网络事实：

+ ansible_facts['dns']['nameservers']
+ ansible_facts['domain']
+ ansible_facts['all_ipv4_addresses']
+ ansible_facts['all_ipv6_addresses']
+ ansible_facts['fqdn']
+ ansible_facts['hostname']


查看所有清单主机的完全限定名 
```yaml
---
- name: net_work
  hosts: all
  tasks:
    - name: print
      debug:
        msg:  "{{ ansible_facts['fqdn'] }}"
$
```
执行测试

```bash
$ ansible-playbook network.yaml
.........
TASK [print] ***************************************************************************************************************
ok: [servera] => {
    "msg": "servera.lab.example.com"
}
ok: [serverb] => {
    "msg": "serverb.lab.example.com"
}
ok: [serverc] => {
    "msg": "serverc.lab.example.com"
}
ok: [serverd] => {
    "msg": "serverd.lab.example.com"
}
ok: [servere] => {
    "msg": "servere.lab.example.com"
}
ok: [serverf] => {
    "msg": "serverf.lab.example.com"
}
```

### 网络信息过滤器

`ipaddr` 过滤器可用于处理和验证网络相关的事实：
+ 检查IP地址的语法
+ 转换VLSN子网掩码为CIDR子网
+ 执行子网数学运算
+ 在网络范围内找到下一个可用地址

使用要求：RHEL8系统使用ipaddr过滤器需要安装 `python3-netaddr` 软件包，该包提供Python模块netaddr。

```bash
[student@workstation laomalS sudo yum install -y python3-netaddr
```
ipaddr 过滤器提供了操作和验证与网络相关的事实功能。

可以用于检查 IP 地址的语法，从 VLSN 子网掩码转换为 CIDR 子网前缀表示法，执行子网计算，查找网络范围内的下一个可用地址等。

在最简单的形式中，不带参数的 ipaddr 过滤器接受单个值。如果值是 IP 地址，则过滤器返回 IP 地址，如果不是IP 地址，则过滤器将返回 False。

+ 如果该值为有效的P地址，则过滤器将返回地址。
+ 如果该值不是有效的IP地址，则过滤器返回False。


```bash
$ ansible servera -m debug -a 'msg={{ "175.25.250.50" | ipaddr}}'
servera | SUCCESS => {
    "msg": "175.25.250.50"
}
$ ansible servera -m debug -a 'msg={{ "175.25.250.50/24" | ipaddr}}'
servera | SUCCESS => {
    "msg": "175.25.250.50/24"
}
$ ansible servera -m debug -a 'msg={{ "175.25.250.500/24" | ipaddr}}'
servera | SUCCESS => {
    "msg": false
}
```
ipaddr过滤器接受参数值：
+ 如果该值包涵有效的IP地址，则返回有效的IP地址。
+ 如果所有项目均无效，则返回一个空列表。

```bash
$ ansible servera -m debug -a 'msg={{ "175.25.250.50/24" | ipaddr("netmask")}}'
servera | SUCCESS => {
    "msg": "255.255.255.0"
}
$
```

+ ipaddr 过滤器接受以下选项：
+ address：验证输入值是否为有效的 IP 地址，如果输入中包含网络前缀，其会被剥离。
+ net：验证输入值是否为网络范围，并以 CIDR 格式返回。
+ host：确保 IP 地址符合等效的 CIDR 前缀格式。
+ prefix：验证输入主机是否满足主机/前缀或 CIDR 格式，并返回前缀。
+ host/prefix：验证输入是否为网络/前缀格式。
+ public 或 private：验证输入 IP 地址或网络范围是否由 IANA 分别预留为公共或私有的范围内。
+ size：将输入网络范围转换为该范围内的 IP 地址数。
+ n：任何整数。将网络范围转换为该范围内的第 N 个元素。负数返回从最后一个数的第 n 个元素。
+ network、netmask、broadcast：验证输入主机是否满足主机/前缀或CIDR格式，并将其分别转换为网络地址、子网掩码或广播地址。
+ subnet：验证输入主机是否满足主机/前缀或 CIDR 格式，并返回包含该主机的子网。
+ ipv4 ipv6：验证输入是否有效的网络范围，并将它们分别转换为 ipv4 和 ipv6 格式。

```bash
$ ansible servera -m debug -a 'msg={{ "175.25.250.50/24" | ipaddr("ipv6")}}'
servera | SUCCESS => {
    "msg": "::ffff:175.25.250.50/120"
}
$ ansible servera -m debug -a 'msg={{ "175.25.250.50/24" | ipaddr("subnet")}}'
servera | SUCCESS => {
    "msg": "175.25.250.0/24"
}
$ ansible servera -m debug -a 'msg={{ "175.25.250.50/24" | ipaddr("size")}}'
servera | SUCCESS => {
    "msg": "256"
}

```

### 使用插件收集网络信息


### 查找 DNS 信息
dig 命令针对 DNS 服务进行查询，并返回生成的记录。dig 需要在控制节点上安装 python3-dns 软件包。


```bash
$ ansible servera -m debug -a 'msg={{  lookup("dig","servera.lab.example.com")}}'
servera | SUCCESS => {
    "msg": "172.25.250.10"
}
$ ansible servera -m debug -a 'msg={{  lookup("dig","example.com")}}'
servera | SUCCESS => {
    "msg": "172.25.254.254"
}
$ ansible servera -m debug -a 'msg={{  lookup("dig","com")}}'
servera | SUCCESS => {
    "msg": "NXDOMAIN"
}
```

dig 查找 DNS 服务器中是否存在提供 FQDN 的 A 记录：

```bash
$ ansible servera -m debug -a 'msg={{  lookup("dig","example.com", "qtype=A")}}'
servera | SUCCESS => {
    "msg": "10 classroom.example.com."
}
$ ansible servera -m debug -a 'msg={{  lookup("dig","example.com", "@")}}'
servera | SUCCESS => {
    "msg": "172.25.254.254"
}
```

### 一个 Demo
```yaml
[student@workstation netfilters]$ cat ./tasks/main.yml
# Complete each task by setting the fact as the expected value.
# Replace ellipsis by the appropriate filter usage.
# All task but the last one should be using the 'ipaddr' filter.
# Use the lookup filter with the `dig` command for the last task

# Tasks make use of th gathered fact 'default_ipv4', and its keys 'address', 'network' and 'netmask'

- name: Task 1- Verify the 'ansible_default_ipv4.addresss' provided address is correctly formatted.
  set_fact:
    server_address: "{{ ansible_facts.default_ipv4.address | ipaddr }}"
- name: Task 2- Check 'server_address' value
  assert:
    that: "server_address == ansible_facts.default_ipv4.address"
    fail_msg: "'server_address' must be {{ ansible_facts.default_ipv4.address }}, but is {{ server_address }}"


- name: Task 3- Obtain the DNS name associated to the server IP address (reverse DNS)
  set_fact:
    address_dns: "{{ server_address | ipaddr('revdns') }}"
- name: Task 4- Check 'address_dns' value
  assert:
    that: "address_dns == '10.250.25.172.in-addr.arpa.'"
    fail_msg: "'address_dns' must be '10.250.25.172.in-addr.arpa.', but is {{ address_dns }}"


- name: Task 5- Obtain server's network/netmask
  set_fact:
    net_mask: "{{ ansible_facts.default_ipv4.network }}/{{ ansible_facts.default_ipv4.netmask }}"
- name: Task 6- Check 'net_mask' value
  assert:
    that: "net_mask == '172.25.250.0/255.255.255.0'"
    fail_msg: "'net_mask' must be '172.25.250.0/255.255.255.0', but is {{ net_mask }}"


- name: Task 7- Transform the network/netmask to the CIDR format
  set_fact:
    cidr: "{{ net_mask | ipaddr('net') }}"
- name: Task 8- Check 'cidr' value
  assert:
    that: "cidr == '172.25.250.0/24'"
    fail_msg: "'cidr' must be '172.25.250.0/24', but is {{ cidr }}"


- name: Task 9- Verify the server address actualy belong to the network/mask
  set_fact:
    address_in_range: "{{ server_address | ipaddr(net_mask) }}"
- name: Task 10- Check 'address_in_range' value
  assert:
    that: "address_in_range == server_address"
    fail_msg: "'address_in_range' must be {{ server_address }}, but is {{ address_in_range }}"


- name: Task 11- Obtain the broadcast address associated to the CIDR
  set_fact:
    broadcast: "{{ cidr | ipaddr('broadcast') }}"
- name: Task 12- Check 'broadcast' value
  assert:
    that: "broadcast == '172.25.250.255'"
    fail_msg: "'broadcast' must be '172.25.250.255', but is {{ broadcast }}"


- name: Task 13- DIG for the MX record of the domain 'example.com'
  set_fact:
    dig_record: "{{ lookup( 'dig', 'example.com.', 'qtype=MX') }}"
- name: Task 14- Check 'dig_record' value
  assert:
    that: "dig_record == '10 classroom.example.com.'"
    fail_msg: "'dig_record' must be '10 classroom.example.com.', but is '{{ dig_record }}'"

```
```bash
[student@workstation data-netfilters]$ ansible-playbook  site.yml

PLAY [Tasks for netfilter guided exercise] *****************************************************************************

TASK [Gathering Facts] *************************************************************************************************
ok: [servera.lab.example.com]

TASK [netfilters : Task 1- Verify the 'ansible_default_ipv4.addresss' provided address is correctly formatted.] ********
ok: [servera.lab.example.com]

TASK [netfilters : Task 2- Check 'server_address' value] ***************************************************************
ok: [servera.lab.example.com] => {
    "changed": false,
    "msg": "All assertions passed"
}

TASK [netfilters : Task 3- Obtain the DNS name associated to the server IP address (reverse DNS)] **********************
ok: [servera.lab.example.com]

TASK [netfilters : Task 4- Check 'address_dns' value] ******************************************************************
ok: [servera.lab.example.com] => {
    "changed": false,
    "msg": "All assertions passed"
}

TASK [netfilters : Task 5- Obtain server's network/netmask] ************************************************************
ok: [servera.lab.example.com]

TASK [netfilters : Task 6- Check 'net_mask' value] *********************************************************************
ok: [servera.lab.example.com] => {
    "changed": false,
    "msg": "All assertions passed"
}

TASK [netfilters : Task 7- Transform the network/netmask to the CIDR format] *******************************************
ok: [servera.lab.example.com]

TASK [netfilters : Task 8- Check 'cidr' value] *************************************************************************
ok: [servera.lab.example.com] => {
    "changed": false,
    "msg": "All assertions passed"
}

TASK [netfilters : Task 9- Verify the server address actualy belong to the network/mask] *******************************
ok: [servera.lab.example.com]

TASK [netfilters : Task 10- Check 'address_in_range' value] ************************************************************
ok: [servera.lab.example.com] => {
    "changed": false,
    "msg": "All assertions passed"
}

TASK [netfilters : Task 11- Obtain the broadcast address associated to the CIDR] ***************************************
ok: [servera.lab.example.com]

TASK [netfilters : Task 12- Check 'broadcast' value] *******************************************************************
ok: [servera.lab.example.com] => {
    "changed": false,
    "msg": "All assertions passed"
}

TASK [netfilters : Task 13- DIG for the MX record of the domain 'example.com'] *****************************************
ok: [servera.lab.example.com]

TASK [netfilters : Task 14- Check 'dig_record' value] ******************************************************************
ok: [servera.lab.example.com] => {
    "changed": false,
    "msg": "All assertions passed"
}

PLAY RECAP *************************************************************************************************************
servera.lab.example.com    : ok=15   changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

[student@workstation data-netfilters]$ cat site.yml
- name: Tasks for netfilter guided exercise
  hosts: servera.lab.example.com
  roles:
    - role: netfilters

[student@workstation data-netfilters]$
```

## 博文参考

《DO447》

