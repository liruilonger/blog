---
title: 关于 K8s 中 Kubectl && bash 命名空间批量操作命令的一些笔记
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2023-02-04 04:16:03/关于K8s中Kubectl&&bash 常用命令的一些笔记整理.html'
mathJax: false
date: 2023-02-04 12:16:03
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 分享几个命名空间批量操作的 `bash` 命令
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***


命名空间切换
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/argocd]
└─$kubectl config set-context --current --namespace=argocd
Context "kubernetes-admin@kubernetes" modified.
```

`查看`所有命名空间 pod 状态为 `Terminating` 的 pod

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubescape]
└─$kubectl get pods -A | grep Terminating \
| awk '{print "pod: " $2 " -n " $1  }' \
| xargs  -n1 -I{} bash -c "echo {}"
pod botkube-64cc8669b8-hhq29 -n botkube 
pod linruilong-rancher-5fc57954cb-289pf -n cattle-system 
pod rancher-c94449448-slv9t -n cattle-system 
pod host-scanner-mtlr5 -n kubescape-host-scanner
pod virt-operator-6dddf97b4f-mdpwr -n kubevirt 
pod virt-operator-6dddf97b4f-z2rg2 -n kubevirt 
pod 5a2b7c5a8b6eea11c36f1f2decbe1c82a528d49ca9d526b8356b5e5b13rksvd -n px-operator 
pod pixie-operator-index-vskv2 -n px-operator 
```

`删除`所有命名空间 pod 状态为 `Terminating` 的 pod。
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubescape]
└─$kubectl get pods -A | grep Terminating \
| awk '{print "kubectl delete pod " $2 " -n " $1 " --force" }' \
| xargs  -n1 -I{} bash -c "{}"
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubescape]
└─$
```
获取当前集群,所有命名空间的所有资源，通过下面的命令获取

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$kubectl api-resources --verbs=list --namespaced -o name  \ 
| xargs -n1 -I{} bash -c "echo @@@  {}   @@@  && kubectl get {} -A  && echo ---"
```

获取指定命名空间 `kubevirt`的全部资源 

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/k8s_shell_secript]
└─$kubectl api-resources --verbs=list --namespaced -o name  \
| xargs -n1 -I{} bash -c "echo @@@  {}   @@@  && kubectl get  {} -n kubevirt  && echo ---"
```

删除某个命名空间 `kubevirt` 的所有资源

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/kubevirt]
└─$kubectl get all -A  -o wide | grep kubevirt \
| awk '{print $2}' \
| awk -F'/' '{ print "kubectl delete "$1" "$2 " -n kubevirt --force" }'\
| xargs  -n1 -I{} bash -c "{}"
```


## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***


***

© 2018-2023 liruilonger@gmail.com，All rights reserved. 保持署名-非商用-自由转载-相同方式共享(创意共享3.0许可证)

