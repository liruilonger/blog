---
title: SSH远程连接命令执行没反应不报错问题解决(-bash:fork:retry:Resource temporarily unavailable.[资源暂时不可用])
tags:
  - SSH
  - Linux
categories:
  - Linux
toc: true
recommend: 1
keywords: 调优
uniqueId: '2021-12-10 10:39:22/SSH远程连接命令执行没反应不报错问题解决(-bash:fork:retry:Resource temporarily unavailable.[资源暂时不可用]).html'
mathJax: false
date: 2021-12-10 18:39:22
thumbnail:
---
**<font color="009688"> 人生当苦无妨,良人当归即好.——烽火戏诸侯《雪中悍刀行》**</font>
<!-- more -->
## 写在前面
***
## <font color=seagreen>我遇到了什么问题：</font>
+ 很老的一个系统`bug`原因升级，大概五六年没有重启机器了，4A平台上面通过`ssh`远程连接`Linux`机器，偶尔连接不上
+ 即使连接之后命令无法正常执行，执行了没有反应。
+ 即使可以执行，执行命名报`-bash: fork: retry: Resource temporarily unavailable.`(资源暂时不可用)

## <font color=yellowgreen>出现问题的原因是什么：</font>

**<font color=purple>Linux进程数超过了设置的最大进程数。会对系统进行资源限制，所以分配给ssh进程的资源时有时无，一些命令的bash进程会被杀调，以保证系统进程不超过设置的最大进程数，无法正常执行。即下面的第一个输出要远远小与第二个和第三个输出。如果有些接近就会出现这种问题</font>**
```bash
┌──[root@liruilongs.github.io]-[/]
└─$ ps -eLf | wc -l  # 当前进程数
221
┌──[root@liruilongs.github.io]-[/]
└─$ ulimit  -u  # 用户的最大进程数
15665
┌──[root@liruilongs.github.io]-[/]
└─$ sysctl kernel.pid_max # 内核设置的最大进程数
kernel.pid_max = 150000
┌──[root@liruilongs.github.io]-[/]
└─$
```

## <font color=orange>我是怎样解决的</font>

**<font color=red>修改内核参数，调整最大进程数限制。这里修改的话需要root权限，同时需要修改两个地方。</font>**

**<font color=chocolate>`其一:`用户登录会加载`pam_limit`模块，`pam_limit`模块读取配置文件 `/etc/security/limits.conf`来限制用户资源的占用。可以使用` ulimit `的命令来查看和临时设置资源信息，也可以通过 写入`/etc/security/limits.conf `来永久配置，配置文件在每次登录时会加载。可以用来设置`ssh`连接数，最大进程数等。</font>**

**<font color=chocolate>`其二:`Linux系统中内核kernel模块，有个全局的设置最大进程数的内核参数，需要修改这个参数，内核参数的设置方式有临时设置和永久设置两种方式，临时设置完就会刷新，重启失效。可以先临时设置后查看效果，然后永久设置。</font>**



**<font color="009688"> 人生当苦无妨,良人当归即好.——烽火戏诸侯《雪中悍刀行》**</font>

***

### <font color=royalblue>查看当前用户的活跃进程数</font>
```bash
┌──[root@liruilongs.github.io]-[/]
└─$ ps -eLf | wc -l  # 当前进程数
221
```
### <font color=purple>查看用户允许运行的最大进程数</font>
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ ulimit  -a
core file size          (blocks, -c) 0
data seg size           (kbytes, -d) unlimited
scheduling priority             (-e) 0
file size               (blocks, -f) unlimited
pending signals                 (-i) 15665
max locked memory       (kbytes, -l) 64
max memory size         (kbytes, -m) unlimited
open files                      (-n) 1024
pipe size            (512 bytes, -p) 8
POSIX message queues     (bytes, -q) 819200
real-time priority              (-r) 0
stack size              (kbytes, -s) 8192
cpu time               (seconds, -t) unlimited
max user processes              (-u) 15665
virtual memory          (kbytes, -v) unlimited
file locks                      (-x) unlimited  # 无限大
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=orange>ulimit为shell内建指令，可用来控制shell执行程序的资源。</font>**
##### <font color=plum>语法：</font>
>`ulimit [-aHS][-c <core文件上限>][-d <数据节区大小>][-f <文件大小>][-m <内存大小>][-n <文件数目>][-p <缓冲区大小>][-s <堆栈大小>][-t <CPU时间>][-u <程序数目>][-v <虚拟内存大小>]`

|参数：|--|
|--|--|
|-a| 　显示目前资源限制的设定。 |
|-c| <core文件上限> 　设定core文件的最大值，单位为区块。 |
|-d| <数据节区大小> 　程序数据节区的最大值，单位为KB。 |
|-f| <文件大小> 　shell所能建立的最大文件，单位为区块。 |
|-H| 　设定资源的硬性限制，也就是管理员所设下的限制。 |
|-m| <内存大小> 　指定可使用内存的上限，单位为KB。 |
|-n| <文件数目> 　指定同一时间最多可开启的文件数。 |
|-p| <缓冲区大小> 　指定管道缓冲区的大小，单位512字节。 |
|-s| <堆栈大小> 　指定堆栈的上限，单位为KB。 |
|-S| 　设定资源的弹性限制。 |
|-t| <CPU时间> 　指定CPU使用时间的上限，单位为秒。 |
|-u| <进程数目> 　用户最多可开启的进程数目。 |
|-v| <虚拟内存大小> 　指定可使用的虚拟内存上限，单位为KB。|

```bash
┌──[root@liruilongs.github.io]-[~]
└─$ ulimit  -u
15665
```
##### <font color=seagreen>默认值</font>
|用户|描述|
|--|--|
|`root 账号`| `ulimit -u `的值 默认是`/proc/sys/kernel/threads-max `的值` /2`，即`系统线程数的一半`|
|`普通账号` |`ulimit -u `的值 默认是 `/etc/security/limits.d/20-nproc.conf `|

### <font color=camel>修改用户允许运行的最大进程数</font>

#### <font color=amber>临时修改</font>
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ ulimit  -u 75535
┌──[root@liruilongs.github.io]-[~]
└─$ ulimit  -u
75535
┌──[root@liruilongs.github.io]-[~]
└─$
```
#### <font color=tomato>永久修改</font>
**<font color=camel>在`/etc/security/limits.conf` 文件里添加如下内容</font>**

```bash
* soft nproc 65535      

* hard nproc 65535    
```
|关键字|描述|
|--|--|
|`nproc`      | 是操作系统级别对每个用户创建的进程数的限制|
|`nofile`     | 是每个进程可以打开的文件数的限制|
|`soft`  xxx  | 代表警告的设定，可以超过这个设定值，但是超过后会有警告。|
|`hard`  xxx  | 代表严格的设定，不允许超过这个设定的值。|

```bash
┌──[root@liruilongs.github.io]-[~]
└─$  echo "* soft nproc 65535"  >> /etc/security/limits.conf
┌──[root@liruilongs.github.io]-[~]
└─$ echo "* hard nproc 65535"  >> /etc/security/limits.conf
┌──[root@liruilongs.github.io]-[~]
└─$ cat /etc/security/limits.conf  | grep nproc
#        - nproc - max number of processes
#@student        hard    nproc           20
#@faculty        soft    nproc           20
#@faculty        hard    nproc           50
#ftp             hard    nproc           0
* soft nproc 65535
* hard nproc 65535
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=red>从新通过ssh的方式登录，就会刷新 ulimit -u的值</font>**

### <font color=amber>查看Linux内核模块kernel允许的最大进程数</font>

**<font color=yellowgreen>查看`kernel.pid_max`的内核参数</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ sysctl -a | grep pid_max  #查看pid的内核参数
kernel.pid_max = 131072
sysctl: reading key "net.ipv6.conf.all.stable_secret"
sysctl: reading key "net.ipv6.conf.br-0e0cdf9c70b0.stable_secret"
sysctl: reading key "net.ipv6.conf.br-4b3da203747c.stable_secret"
sysctl: reading key "net.ipv6.conf.default.stable_secret"
sysctl: reading key "net.ipv6.conf.docker0.stable_secret"
sysctl: reading key "net.ipv6.conf.ens32.stable_secret"
sysctl: reading key "net.ipv6.conf.lo.stable_secret"
┌──[root@liruilongs.github.io]-[/]
└─$ sysctl kernel.pid_max
kernel.pid_max = 131072
```
**<font color=brown>根据变量找到对应的内核参数文件位置</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cd /proc/sys/kernel/;cat pid_max
131072
```
### <font color=brown>调整`kernel.pid_max`内核参数</font>

#### <font color=brown>临时调整内核参数</font>
```bash
┌──[root@liruilongs.github.io]-[/proc/sys/kernel]
└─$ echo 150000 > pid_max
┌──[root@liruilongs.github.io]-[/proc/sys/kernel]
└─$ cat pid_max  # 临时调整内核参数
150000
```
#### <font color=chocolate>永久调整`kernel.pid_max`内核参数</font>

```bash
┌──[root@liruilongs.github.io]-[/]
└─$ echo "kernel.pid_max = 150000" >> /etc/sysctl.conf # 永久调整
┌──[root@liruilongs.github.io]-[/]
└─$ cat /etc/sysctl.conf | grep kernel
kernel.pid_max = 150000
┌──[root@liruilongs.github.io]-[/]
└─$ sysctl -p
net.ipv4.ip_forward = 1
vm.swappiness = 20
kernel.pid_max = 150000
┌──[root@liruilongs.github.io]-[/]
└─$
```


