---
title: 'CPU占用率爆满,服务器遭遇挖矿如何排查'
tags:
  - 安全
categories:
  - 挖矿
toc: true
recommend: 1
keywords: 安全
uniqueId: '2022-03-03 04:28:05/CPU占用率爆满,服务器遭遇挖矿如何排查.html'
mathJax: false
date: 2022-03-03 12:28:05
thumbnail:
---
**<font color="009688">太用力的人跑不远, 真正坚持到最后的人靠的不是激情，而是恰到好处的喜欢和投入。**</font>

<!-- more -->
## 写在前面
***
+ 之前上学在阿里云租了台学生服务器，装了一个`Docker`玩，后来不知道怎么回事，通过`Docker`连客户端暴露的端口
+ 被植入了国外的`挖矿木马`，只部署了一个毕设项目，`CPU`天天爆满，机器被拉去挖矿了 :(
+ 自己折腾好久，进程杀不死，删文件直接把机器搞挂了，后来提交工单重置了
+ 整理了这篇博文,对这方面问题做个简单记录
+ 博文内容涉及
  + `遭遇挖矿基本的排查措施`
  + `两种简单防护手段`

+ 博文不涉及具体的实例，只是提供一些找问题的方向

**<font color="009688">太用力的人跑不远, 真正坚持到最后的人靠的不是激情，而是恰到好处的喜欢和投入。**</font>


***


# 挖矿木马

`挖矿木马`是攻击者利用各种方法入侵计算机,在拥有者不知情的情况下`利用被入侵计算机的算力挖掘加密数字货币以谋取利益的木马`,其既可以是一段`自动化扫描`、`攻击的脚本`,也可以集成在`单个可执行文件中`。

`挖矿木马`为了能够长期在`服务器中驻留`,会采用多种安全对抗技术,如`修改任务计划`,`修改防火墙配置`,`修改系统动态链接库`等,使用这些技术手段严重时可能会造成服务器业务中断。


# 是否正在遭遇挖矿

+ 计算机CPU使用率是否飘升,系统是否卡顿,部分服务是否存在无法正常运行等现象
+ 通过服务器性能检测设备查看服务器性能判断异常
+ 通过查看安全检测类设备告警判断挖矿木马会与矿池地址建立连接 


# 常规处理步骤

+ 当出现可疑进程时,找当前主机的使用用户确认该进程;
+ 当确定为挖矿进程后,进行溯源排查,因为一般攻击者会进行相关的设置,来保证当挖矿进程被`kill` 之后,还能再次启动,通过溯源排查能够查找相关配置,追潮攻击者的攻击途径,进行有针对性的防御和补丁等,阻止攻击者再次利用;
+ 当溯源排查完成后,进行封堵矿池地址,清理任务计划、禁用可疑用户,结束异常进程,清除挖矿木马,全盘杀毒、加固。 
+ 判断挖矿木马的挖矿时间挖矿木马文件创建时间,任务计划创建时间,查看矿池地址

## Windows系统操作

### 系统排查
**检查用户信息,使用指令`net users`,或`计算机管理`--->`本地用户和组`,通过注册表查找克隆用户**
```bash
D:\>net users

\\LIRUILONG 的用户帐户

-------------------------------------------------------------------------------
__HSKDDNS_USER__         Administrator            DefaultAccount
Guest                    lenovo                   WDAGUtilityAccount
命令成功完成。
```
**网络连接排查, `netstat -ano`,检查可疑网络连接**
```bash
D:\>netstat -ano

活动连接

  协议  本地地址          外部地址        状态           PID
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1116
  TCP    0.0.0.0:443            0.0.0.0:0              LISTENING       8036
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  .......
  TCP    192.168.1.5:2384       112.86.128.91:443      ESTABLISHED     14184
  TCP    192.168.1.5:2386       112.86.128.91:443      ESTABLISHED     14184
  TCP    192.168.1.5:2413       112.86.128.91:443      ESTABLISHED     14184
  TCP    192.168.1.5:2415       112.86.128.91:443      ESTABLISHED     14184
  TCP    192.168.1.5:2511       124.238.121.41:80      CLOSE_WAIT      5144
  TCP    192.168.1.5:2513       113.96.237.36:443      CLOSE_WAIT      11696
  TCP    192.168.1.5:2523       113.96.237.36:443      CLOSE_WAIT      11696
  TCP    192.168.1.5:2524       113.96.237.36:443      CLOSE_WAIT      11696
  .......
```
**进程排查,`任务管理器`查看,检查可疑进程**

**任务计划排查,`任务计划程序`查看,检查可疑任务计划**

**服务排查,`任务管理器`---> `服务`**

### 日志排查

系统日志:默认情况下, `Windows`系统日志放在`%SystemRoot%\System32\Winevt\Logs`中,相关日志有:
+ `Application.evtx` (应用程序日志) , 
+ `Security.evtx` (安全性日志)
+ `System.evtx `(系统日志)

可以使用系统自带的事件查看器查看安全性日志
```bash
PS C:\Windows\System32\winevt\Logs> ls


    目录: C:\Windows\System32\winevt\Logs


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----          2022/3/3     15:46       20975616 Application.evtx
-a----         2021/2/17     16:02          69632 HardwareEvents.evtx
-a----         2021/2/17     16:02          69632 Internet Explorer.evtx
-a----         2021/2/17     16:02          69632 Key Management Service.evtx
-a----         2022/2/25     21:42        1118208 Lenovo-Power-BaseModule%4Operational.evtx
-a----          2021/4/9      8:23          69632 Microsoft-AppV-Client%4Admin.evtx
-a----          2021/4/9      8:23          69632 Microsoft-AppV-Client%4Operational.evtx
-a----          2021/4/9      8:23          69632 Microsoft-AppV-Client%4Virtual Applications.evtx
............................
```

## Linux 系统操作

### 系统排查
#### 检查用户信息
**所有用户信息`cat /etc/passwd`**

```bash
┌──[root@liruilongs.github.io]-[/usr/bin]
└─$cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
........
oprofile:x:16:16:Special user account to be used by OProfile:/var/lib/oprofile:/sbin/nologin
┌──[root@liruilongs.github.io]-[/usr/bin]
└─$
```
**用户最后登录信息`lastlog`**

>显示系统中所有用户最近一次登录信息,`lastlog`文件在每次有用户登录时被查询。可以使用`lastlog`命令检查某特定用户上次登录的时间，并格式化输出上次登录日志`/var/log/lastlog`的内容。它根据`UID排序显示登录名、端口号(tty)和上次登录时间`。如果一个用户从未登录过，`lastlog`显示`**Never logged**`。注意需要以`root`身份运行该命令。

```bash
┌──[root@liruilongs.github.io]-[~]
└─$lastlog
Username         Port     From          Latest
root             pts/0    192.168.26.1  Thu Mar  3 16:47:46 +0800 2022
bin                                     **Never logged in**
daemon                                  **Never logged in**
...........
postfix                                 **Never logged in**
sshd                                    **Never logged in**
chrony                                  **Never logged in**
tom                                     **Never logged in**
nginx                                   **Never logged in**
etcd                                    **Never logged in**
apache                                  **Never logged in**
pcp                                     **Never logged in**
mysql                                   **Never logged in**
oprofile                                **Never logged in**
┌──[root@liruilongs.github.io]-[~]
└─$
```
**用户错误登录列表`lastb`**

>`Linux lastb`命令用于列出`登入系统失败的用户相关信息`。单独执行`lastb`指令，它会读取位于`/var/log`目录下，名称为`btmp`的文件，并把该文件内容记录的登入失败的用户名单，全部显示出来。
```bash
┌──[root@liruilongs.github.io]-[~]
└─$lastb
root     ssh:notty    192.168.26.1     Thu Mar  3 16:50 - 16:50  (00:00)
root     ssh:notty    192.168.26.1     Thu Mar  3 16:50 - 16:50  (00:00)

btmp begins Thu Mar  3 16:50:54 2022
┌──[root@liruilongs.github.io]-[~]
└─$
```
**用户最近登录信息`last`;**

>`Linux last `命令用于显示用户最近登录信息。

```bash
┌──[root@liruilongs.github.io]-[~]
└─$last -5 root
root     pts/0        192.168.26.1     Thu Mar  3 16:47   still logged in
root     pts/0        192.168.26.1     Thu Mar  3 09:43 - 16:45  (07:02)
root     pts/0        192.168.26.1     Wed Mar  2 16:07 - 23:06  (06:58)
root     pts/0        192.168.26.1     Tue Mar  1 16:40 - 20:04  (03:23)
root     pts/1        192.168.26.1     Mon Feb 28 12:46 - 11:18  (22:31)

wtmp begins Thu Oct 18 23:13:06 2018
┌──[root@liruilongs.github.io]-[~]
└─$
```

**空口令账户`awk -F ':' 'length($2)==0 {print $1}' /etc/shadow`**

```bash
┌──[root@liruilongs.github.io]-[~]
└─$awk -F ':' 'length($2)==0 {print $1}' /etc/shadow
┌──[root@liruilongs.github.io]-[~]
└─$
```
```bash
┌──[root@liruilongs.github.io]-[~]
└─$useradd test
┌──[root@liruilongs.github.io]-[~]
└─$passwd -d test
Removing password for user test.
passwd: Success
┌──[root@liruilongs.github.io]-[~]
└─$awk -F ':' 'length($2)==0 {print $1}' /etc/shadow
test
┌──[root@liruilongs.github.io]-[~]
└─$
```
**空口令登录限制`cat /etc/ssh/sshd_config | grep  PermitEmptyPasswords`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$cat /etc/ssh/sshd_config | grep  PermitEmptyPasswords
#PermitEmptyPasswords no    #设置 PermitEmptyPasswords yes 即允许空密码登入
┌──[root@liruilongs.github.io]-[~]
└─$sed -i 's/#PermitEmptyPasswords no/PermitEmptyPasswords yes/g' /etc/ssh/sshd_config
┌──[root@liruilongs.github.io]-[~]
└─$systemctl restart sshd
┌──[root@liruilongs.github.io]-[~]
└─$
```
```bash
PS E:\docker> ssh test@192.168.26.55
Last login: Thu Mar  3 17:30:48 2022 from 192.168.26.1
[test@liruilongs ~]$uptime
 17:34:27 up 3 days, 23:33,  2 users,  load average: 0.17, 0.09, 0.07
[test@liruilongs ~]$
```


#### 检查进程

**这里我们找一个压测工具模拟一下`CPU 100%`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$yum -y install stress
┌──[root@liruilongs.github.io]-[~]
└─$nohup stress  -c 1 -t 100 & 
[1] 132748
┌──[root@liruilongs.github.io]-[~]
└─$nohup: ignoring input and appending output to ‘nohup.out’
```
**同时需要`lsof`工具**
```bash
┌──[root@liruilongs.github.io]-[/usr/bin]
└─$yum -y install lsof
```
**系统进程`ps aux`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ps --no-headers -eo %cpu,pid,args | sort -k1 -n -r | head -10
99.6 134004 stress -c 1 -t 500
 1.7   8079 sidekiq 5.0.5 gitlab-rails [0 of 25 busy]
 1.4    843 /usr/bin/etcd --name=default --data-dir=/var/lib/etcd/default.etcd --listen-client-urls=http://192.168.26.55:2379,http://localhost:2379
 1.2   8156 /opt/gitlab/embedded/bin/prometheus -web.listen-address=localhost:9090 -storage.local.path=/var/opt/gitlab/prometheus/data -storage.local.chunk-encoding-version=2 -storage.local.target-heap-size=67827957 -config.file=/var/opt/gitlab/prometheus/prometheus.yml
 1.2   8060 /opt/gitlab/embedded/bin/redis-server 127.0.0.1:0
 1.1   8091 /opt/gitlab/embedded/bin/ruby /opt/gitlab/embedded/bin/gitlab-mon web -c /var/opt/gitlab/gitlab-monitor/gitlab-monitor.yml
 0.6   8198 ruby /opt/gitlab/embedded/service/gitaly-ruby/bin/gitaly-ruby 490 /tmp/gitaly-ruby360216715/socket.1
 0.6   8196 ruby /opt/gitlab/embedded/service/gitaly-ruby/bin/gitaly-ruby 490 /tmp/gitaly-ruby360216715/socket.0
 0.5   7467 java -Duser.home=/var/jenkins_home -Dhudson.security.csrf.GlobalCrumbIssuerConfiguration.DISABLE_CSRF_PROTECTION=true -Djenkins.model.Jenkins.slaveAgentPort=50000 -jar /usr/share/jenkins/jenkins.war
 0.3      9 [rcu_sched]
┌──[root@liruilongs.github.io]-[~]
└─$
```
**进程、端口及对应的PID `netstat -antp或则ss`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$netstat -antp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 127.0.0.1:2379          0.0.0.0:*               LISTEN      843/etcd
tcp        0      0 192.168.26.55:2379      0.0.0.0:*               LISTEN      843/etcd
tcp        0      0 127.0.0.1:2380          0.0.0.0:*               LISTEN      843/etcd
tcp        0      0 192.168.26.55:2380      0.0.0.0:*               LISTEN      843/etcd
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      7347/docker-proxy
...........
tcp6       0      0 :::2376                 :::*                    LISTEN      1521/dockerd
tcp6       0      0 ::1:4330                :::*                    LISTEN      36013/pmlogger
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=seagreen>CPU占用率`top或者ps`</font>**
```bash
top - 18:18:25 up 4 days, 17 min,  2 users,  load average: 0.51, 0.62, 0.77
Tasks: 249 total,   2 running, 247 sleeping,   0 stopped,   0 zombie
%Cpu(s): 52.5 us,  0.2 sy,  0.0 ni, 47.0 id,  0.3 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  2031912 total,    77056 free,  1712644 used,   242212 buff/cache
KiB Swap: 10485756 total,  8991960 free,  1493796 used.    88756 avail Mem

   PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND
134004 root      20   0    7264    100      0 R 100.0  0.0   0:37.87 stress
  8079 chrony    20   0  889220 349276   1624 S   1.3 17.2  98:16.80 bundle
   843 etcd      20   0 10.308g   5488    824 S   1.0  0.3  83:53.19 etcd
  8156 992       20   0  348680  56736   3920 S   1.0  2.8  71:08.16 prometheus
  7467 tom       20   0 3109092 213956    764 S   0.7 10.5  30:15.35 java
  8060 nginx     20   0   41636   3676    888 S   0.7  0.2  70:50.13 redis-server
  8099 chrony    20   0  401332  16700   1964 S   0.7  0.8  15:00.93 gitaly
     9 root      20   0       0      0      0 S   0.3  0.0  20:37.39 rcu_sched
  8196 chrony    20   0 1314608     36      0 S   0.3  0.0  34:57.02 ruby
121037 root      20   0  151984    428    268 S   0.3  0.0   0:01.30 sshd
132973 root      20   0       0      0      0 S   0.3  0.0   0:00.21 kworker/1:1
134044 root      20   0  162032   2360   1556 R   0.3  0.1   0:00.05 top
........
    15 root       0 -20       0      0      0 S   0.0  0.0   0:00.00 kworker/1:0H

```
**详细查看可疑进程  `lsof -p PID`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$lsof -p 134004
COMMAND    PID USER   FD   TYPE DEVICE SIZE/OFF      NODE NAME
stress  134004 root  cwd    DIR    8,1     4096 134217793 /root
stress  134004 root  rtd    DIR    8,1     4096        64 /
stress  134004 root  txt    REG    8,1    27704 277828820 /usr/bin/stress
stress  134004 root  mem    REG    8,1  2127336 402654082 /usr/lib64/libc-2.17.so
stress  134004 root  mem    REG    8,1  1139680 402654090 /usr/lib64/libm-2.17.so
stress  134004 root  mem    REG    8,1   164264 402654075 /usr/lib64/ld-2.17.so
stress  134004 root    0w   CHR    1,3      0t0        18 /dev/null
stress  134004 root    1w   REG    8,1       66 145012428 /root/nohup.out
stress  134004 root    2w   REG    8,1       66 145012428 /root/nohup.out
┌──[root@liruilongs.github.io]-[~]
└─$
```
**详细查看可疑进程信息  `ll /proc/PID`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ll /proc/134004
ls: cannot access /proc/134004: No such file or directory
[1]+  Done                    nohup stress -c 1 -t 500
┌──[root@liruilongs.github.io]-[~]
└─$
```
**详细查看可疑端口 `lsof -i :port`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$lsof -i
COMMAND      PID      USER   FD   TYPE  DEVICE SIZE/OFF NODE NAME
chronyd      642    chrony    1u  IPv4   21585      0t0  UDP localhost:323
chronyd      642    chrony    2u  IPv6   21586      0t0  UDP localhost:323
etcd         843      etcd    5u  IPv4   24201      0t0  TCP vms55.rhce.cc:2380 (LISTEN)
etcd         843      etcd    6u  IPv4   24209      0t0  TCP localhost:2380 (LISTEN)
........
pmcd        1011       pcp    3u  IPv6   23985      0t0  TCP localhost:pmcd (LISTEN)
master      1036      root   13u  IPv4   24056      0t0  TCP localhost:smtp (LISTEN)
........
sshd      121037      root    3u  IPv4 3821579      0t0  TCP vms55.rhce.cc:ssh->192.168.26.1:13570 (ESTABLISHED)
sshd      127459      root    3u  IPv4 3850818      0t0  TCP *:ssh (LISTEN)
sshd      127459      root    4u  IPv6 3850820      0t0  TCP *:ssh (LISTEN)
sshd      128629      root    3u  IPv4 3856418      0t0  TCP vms55.rhce.cc:ssh->192.168.26.1:5928 (ESTABLISHED)
sshd      128631 liruilong    3u  IPv4 3856418      0t0  TCP vms55.rhce.cc:ssh->192.168.26.1:5928 (ESTABLISHED)
┌──[root@liruilongs.github.io]-[~]
└─$lsof -i :7269
```

### 日志排查

#### 查看任务计划日志:

**当前任务计划 `crontab -l`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$crontab -l
no crontab for root
┌──[root@liruilongs.github.io]-[~]
└─$
```
**`etc`目录任务计划相关文件 `ls /etc/cron*`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ls /etc/cron*
/etc/cron.deny  /etc/crontab

/etc/cron.d:
0hourly  sysstat

/etc/cron.daily:
logrotate  man-db.cron

/etc/cron.hourly:
0anacron

/etc/cron.monthly:

/etc/cron.weekly:
┌──[root@liruilongs.github.io]-[~]
└─$
```
**查看任务计划日志`cat /var/log/cron`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$cat /var/log/cron | head -3
Feb 27 03:43:13 liruilongs run-parts(/etc/cron.daily)[65785]: finished man-db.cron
Feb 27 03:43:13 liruilongs anacron[59178]: Job cron.daily'' terminated
Feb 27 03:43:13 liruilongs anacron[59178]: Normal exit (1 job run)
┌──[root@liruilongs.github.io]-[~]
└─$
```
**查看相关日志记录文件`ls /var/spool/mail`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ls /var/spool/mail/
liruilong  test  tom
┌──[root@liruilongs.github.io]-[~]
└─$
```

#### 查看自启动日志:
**查看整体系统信息, `cat /var/log/message`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$cat /var/log/messages | head -3
Feb 27 03:50:01 liruilongs systemd: Started Session 157 of user root.
Feb 27 03:55:18 liruilongs systemd: Starting Check pmlogger instances are running...
Feb 27 03:55:21 liruilongs systemd: Started Check pmlogger instances are running.
┌──[root@liruilongs.github.io]-[~]
└─$
```
**查看验证和授权方面的信息, `cat /var/log/secure`**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$cat /var/log/secure | head -5
Feb 27 11:17:41 liruilongs sshd[110566]: pam_unix(sshd:session): session closed for user root
Feb 27 17:38:04 liruilongs sshd[148418]: Accepted password for root from 192.168.26.1 port 11561 ssh2
Feb 27 17:38:04 liruilongs sshd[148418]: pam_unix(sshd:session): session opened for user root by (uid=0)
Feb 28 10:05:52 liruilongs sshd[148418]: pam_unix(sshd:session): session closed for user root
Feb 28 12:12:37 liruilongs sshd[94739]: Accepted password for root from 192.168.26.1 port 13575 ssh2
┌──[root@liruilongs.github.io]-[~]
└─$
```
**查看所有用户最近一次进入系统的信息: `cat /var/log/lastlog`**

**查看所有失败登录信息:`cat /var/log/btmp`**

**查看系统运行电子邮件服务器的日志信息: `cat /var/log/maillog`**

**查看之前使用过的shell命令: `cat ~/.bash_history`**

>以上是常规的排查方法,但是黑客一般都会抹去自己的操作痕迹,可能排查到最后发现什么都没发现,`要记得及时的把挖矿进程kill掉,他的下一次攻击再进行探查`。

# 简单预防手段


## AIDE入侵检测

`AIDE(Advanced intrusion detection environment)`软件为一套入侵检测系统

aide原理

+ 在没有被人篡改之前对我们的计算机做一个全面的检查
+ 记录每个文件的状态(包括数据的时间，大小，权限，哈希值等)
+ 当计算机被入侵以后，aide对计算机做同样的校验，同样的检查
+ 最后，通过对比入侵之前检查的结果和入侵之后的检查结果，找到哪些数据发生了哪些变化

```bash
┌──[root@liruilongs.github.io]-[~]
└─$yum -y install aide
```
**配置文件信息**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$vim /etc/aide.conf
1 # Example configuration file for AIDE.
2
3 @@define DBDIR /var/lib/aide #aide对数据校验后，结果文件的存放位置
4 @@define LOGDIR /var/log/aide #aide日志文件的目录
......
###当系统被入侵后，需要将入侵前的数据aide.db.new.gz，更名为aide.db.gz
##系统被入侵后，aide重新校验时会将新生成的校验结果aide.db.new.gz和以前校验结果aide.db.gz进行对比
7 database=file:@@{DBDIR}/aide.db.gz
......
#####定义校验完数据后，将校验结果存放到哪个目录下的哪个文件中
##默认存放到/var/lib/aide目录下；保存的文件名问aide.db.new.gz
12 database_out=file:@@{DBDIR}/aide.db.new.gz
......
28 #p: permissions #校验文件的权限是否被修改
29 #i: inode: #校验文件的inode节点是否被修改
30 #n: number of links #校验文件的链接数量是否增多或减少
31 #u: user #校验文件的所有者是否被修改
32 #g: group #校验文件的所属组否是被修改
33 #s: size #校验文件的大小是否变化
......
42 #md5: md5 checksum #校验文件md5生成的哈希值
43 #sha1: sha1 checksum #校验文件sha1生成的哈希值
44 #sha256: sha256 checksum #校验文件sha256生成的哈希值
......
######定义变量，变量中包含以上的所有校验
54 FIPSR = p+i+n+u+g+s+m+c+acl+selinux+xattrs+sha256
......
71 NORMAL = sha256 #定义变量，校验哈希值，会调用

```
**<font color=red>校验的目录设置</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$vim /etc/aide.conf
.........
# Extended content + file type + access.
CONTENT_EX = sha256+ftype+p+u+g+n+acl+selinux+xattrs

# Some files get updated automatically, so the inode/ctime/mtime change
# but we want to know when the data inside them changes.
DATAONLY =  p+n+u+g+s+acl+selinux+xattrs+sha256

# Next decide what directories/files you want in the database. Aide
# uses a first match system. Put file specific instructions before generic
# matches. e.g. Put file matches before directories.

/boot/   CONTENT_EX  #对/boot目录，进行sha256+ftype+p+u+g+n+acl+selinux+xattrs校验,CONTENT_EX为变量
/bin/    CONTENT_EX
/sbin/   CONTENT_EX
/lib/    CONTENT_EX
/lib64/  CONTENT_EX
/opt/    CONTENT

# Admin's dot files constantly change, just check perms.
/root/\..* PERMS
# Otherwise get all of /root.
/root/   CONTENT_EX

# These are too volatile.
!/usr/src/ #! 取反，设置不需要校验的目录
!/usr/tmp/
# Otherwise get all of /usr.
/usr/    CONTENT_EX

```
这里我们简单测试下，修改配置文件，只校验`tmp`目录下的文件
```bash
DATAONLY =  p+n+u+g+s+acl+selinux+xattrs+sha256
........
/tmp/ DATAONLY
#/boot/   CONTENT_EX
#/bin/    CONTENT_EX
#/sbin/   CONTENT_EX
#/lib/    CONTENT_EX
#/lib64/  CONTENT_EX
#/opt/    CONTENT
.........
```
### 初始化检查
**初始化检查:在没有被攻击入侵前,根据配置文件，对数据进行校验操作**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$aide --init

AIDE, version 0.15.1
### AIDE database at /var/lib/aide/aide.db.new.gz initialized.
┌──[root@liruilongs.github.io]-[~]
└─$
```
**查看生成的校验结果数据,#aide每次校验,生成的校验结果文件名都相同**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ll /var/lib/aide/aide.db.new.gz
-rw------- 1 root root 39758 Mar  3 19:04 /var/lib/aide/aide.db.new.gz
┌──[root@liruilongs.github.io]-[~]
└─$
```
**备份数据库,在被入侵前，将校验的数据库文件备份到安全的地方 如，优盘、光盘、移动硬盘、网络存储**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$mv /var/lib/aide/aide.db.new.gz /mnt/
┌──[root@liruilongs.github.io]-[~]
└─$
```

### 入侵检查
将之前备份的校验数据库文件拷贝到 `/var/lib/aide`


```bash
┌──[root@liruilongs.github.io]-[~]
└─$cp /mnt/aide.db.new.gz /var/lib/aide/aide.db.gz
```
**配置文件中已经定义了新生成的数据库文件会和`aide.db.gz`进行比对，所以需要改名。**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$aide --check

AIDE, version 0.15.1

### All files match AIDE database. Looks okay!

┌──[root@liruilongs.github.io]-[~]
└─$
```
**在没有修改文件前，检测没有任何变化**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$echo aide test >> /tmp/test.txt
```
**对`/tmp`目录下的文件进行修改，重新使用`aide`进行校验比对**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$aide --check
AIDE 0.15.1 found differences between database and filesystem!!
Start timestamp: 2022-03-03 19:10:51

Summary:
  Total number of files:        2712
  Added files:                  1
  Removed files:                0
  Changed files:                0


---------------------------------------------------
Added files:
---------------------------------------------------

added: /tmp/test.txt
┌──[root@liruilongs.github.io]-[~]
└─$
```

## <font color=brown>NMAP扫描</font>

`NMAP`一款强大的网络探测利器工具，支持多种探测技术:`ping `扫描,多端口扫描, `TCP/IP`指纹检验

基本用法 :` nmap [扫描类型] [选项] <扫描目标 ...>`

**常用的扫描类型**

+ `-sS，TCP SYN扫描(半开)`
+ `-sT，TCP 连接扫描(全开)`
+ `-sU，UDP扫描`
+ `-sP，ICMP扫描`
+ `-A，目标系统全面分析`


![在这里插入图片描述](https://img-blog.csdnimg.cn/8a69411ce37c4b98bb4df1873885c552.png)


>**<font color=amber>TCP SYN扫描(全开):</font>**
左边图：要检查目标主机的80端口是否打开，扫描时，A主机向目标主机的80端口发送请求建立连接的请求syn,目标主机回应syn和ack,A主机也回应一个ack，连接建立，目标主机的80端口是打开的；

>**<font color=purple>TCP SYN扫描(半开):</font>**
右边图：要检查目标主机的80端口是否打开，扫描时，A主机向目标主机的80端口发送请求建立连接的请求syn,目标主机回应syn和ack, 目标主机回应了，代表目标主机的80端口是打开的，A主机不再回应ack，节省一个ack回应的资源；因为并不需要建立连接，只要目标主机回应即可，当扫描更多主机时会节省更多的系统资源；

### NMAP应用示例

**这里 扫描机器为本机windows里的一个Linux子系统，被扫描机器为windows上一个Linux虚机**

**检查目标主机的存活状态(是否可`ping`通)**
```bash
┌──(root💀Liruilong)-[/mnt/e/docker]
└─# nmap -n -sP 192.168.26.0/24
Starting Nmap 7.91 ( https://nmap.org ) at 2022-03-03 19:28 CST
Nmap scan report for 192.168.26.55
Host is up (0.0012s latency). #192.168.26.55存活
Nmap done: 256 IP addresses (1 host up) scanned in 10.61 seconds
```
**扫描http端口**
```bash
┌──(root💀Liruilong)-[/mnt/e/docker]
└─#  nmap -n 192.168.26.55 -p 80
Starting Nmap 7.91 ( https://nmap.org ) at 2022-03-03 19:31 CST
Nmap scan report for 192.168.26.55
Host is up (0.00089s latency).

PORT   STATE SERVICE
80/tcp open  http  #打开，服务http

Nmap done: 1 IP address (1 host up) scanned in 0.40 seconds

```
**扫描查看网络中所有26.0网段上的80端口的状态**
```bash
┌──(root💀Liruilong)-[/mnt/e/docker]
└─# nmap -n 192.168.26.0/24 -p 80
Starting Nmap 7.91 ( https://nmap.org ) at 2022-03-03 19:32 CST
Nmap scan report for 192.168.26.1
Host is up (0.00052s latency).

PORT   STATE    SERVICE
80/tcp filtered http

Nmap scan report for 192.168.26.55
Host is up (0.0011s latency).

PORT   STATE SERVICE
80/tcp open  http

Nmap done: 256 IP addresses (2 hosts up) scanned in 18.83 seconds
```
**扫描查看TCP协议53-55和25的端口状态**
```bash
──(root💀Liruilong)-[/mnt/e/docker]
└─# nmap -n 192.168.26.55 -p 53-55,25
Starting Nmap 7.91 ( https://nmap.org ) at 2022-03-03 19:37 CST
Nmap scan report for 192.168.26.55
Host is up (0.00098s latency).

PORT   STATE  SERVICE
25/tcp closed smtp
53/tcp closed domain
54/tcp closed xns-ch
55/tcp closed isi-gl

Nmap done: 1 IP address (1 host up) scanned in 0.40 seconds

```
**复合扫描：检查操作系统指纹、软件版本等信息**
```bash
┌──(root💀Liruilong)-[/mnt/e/docker]
└─#  nmap -n -A 192.168.26.55
Starting Nmap 7.91 ( https://nmap.org ) at 2022-03-03 19:27 CST
Nmap scan report for 192.168.26.55
Host is up (0.00087s latency).
Not shown: 995 closed ports
PORT      STATE SERVICE VERSION
#目标主机开启了22端口，使用的是ssh服务，使用软件为OpenSSH 7.4
22/tcp    open  ssh     OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey:
|   2048 5b:40:0e:e6:1d:70:7f:f1:05:34:8d:2b:72:a1:c5:b3 (RSA)
|_  256 f1:27:ee:82:cc:94:b2:7c:68:c9:ea:a0:88:64:20:b3 (ECDSA)
#目标主机开启了80端口，使用的是http服务，使用软件为 nginx
80/tcp    open  http    nginx
| http-robots.txt: 53 disallowed entries (15 shown)
| / /autocomplete/users /search /api /admin /profile
| /dashboard /projects/new /groups/new /groups/*/edit /users /help
|_/s/ /snippets/new /snippets/*/edit
222/tcp   open  ssh     OpenSSH 7.2p2 Ubuntu 4ubuntu2.4 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|_  256 03:b8:32:59:ad:e7:9c:33:63:5c:04:7a:45:68:93:cb (ED25519)
8080/tcp  open  http    Jetty 9.4.43.v20210629
|_http-title: Site doesn''t have a title (text/html;charset=utf-8).
#目标主机开启了50000端口，使用的是http服务，使用软件为  Jenkins
50000/tcp open  http    Jenkins httpd 2.319
|_http-title: Site doesn't have a title (text/plain;charset=UTF-8).
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.91%E=4%D=3/3%OT=22%CT=1%CU=36908%PV=Y%DS=2%DC=T%G=Y%TM=6220A6BE
OS:%P=x86_64-pc-linux-gnu)SEQ(SP=102%GCD=1%ISR=10C%TI=Z%CI=I%II=I%TS=A)SEQ(
OS:SP=102%GCD=1%ISR=10C%TI=Z%TS=A)SEQ(SP=102%GCD=1%ISR=10C%TI=Z%CI=I%TS=A)O
OS:PS(O1=M5B4ST11NW7%O2=M5B4ST11NW7%O3=M5B4NNT11NW7%O4=M5B4ST11NW7%O5=M5B4S
OS:T11NW7%O6=M5B4ST11)WIN(W1=7120%W2=7120%W3=7120%W4=7120%W5=7120%W6=7120)E
OS:CN(R=Y%DF=Y%T=40%W=7210%O=M5B4NNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S=O%A=S+%F
OS:=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%RD=0%Q=)T5
OS:(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=0%S=A%A=Z
OS:%F=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF=
OS:N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=8E44%RUD=G)IE(R=Y%DFI=N%T=
OS:40%CD=S)

Network Distance: 2 hops
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

TRACEROUTE (using port 6002/tcp)
HOP RTT     ADDRESS
1   0.32 ms 172.24.208.1
2   0.77 ms 192.168.26.55
#整个检测一共花费了171.45秒
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 171.45 seconds

```