---
title: 【python小脚本】连接redis集群编辑指定键值
tags:
  - Python
  - Redis
categories:
  - Python
toc: true
recommend: 1
keywords: Python
uniqueId: '2021-12-14 19:36:37/【python小脚本】连接redis集群编辑指定键值.html'
mathJax: false
date: 2021-12-15 03:36:37
thumbnail:
---
**<font color="009688"> 想念想念，一经想起便念念不忘了。——烽火戏诸侯 《雪中悍刀行》**</font>
<!-- more -->
## 写在前面
***
+ 有一个系统中不知道什么原因，Redis有个K要定期清理，但是没有权限在服务器上搞，而且服务器也没有redis的客户端
+ 所以只能本地写一个python脚本，然后配置一个定时任务.
+ 逻辑很简单,但是要考虑集群的问题


**<font color="009688"> 想念想念，一经想起便念念不忘了。——烽火戏诸侯 《雪中悍刀行》**</font>

***

**<font color="#C0C4CC">嗯，实现很简单，连接Redis之后，直接处理就可以了，这里要注意的是集群的问题，redis连接成功，报错</font>**
```bash
D:\haojingkeji\flask-vue-crud\redisclear>python redisclear.py
redis连接成功 Redis<ConnectionPool<Connection<host=192.168.50.162,port=8003,db=0>>>

MOVED 15160 192.168.50.177:8003
```
**<font color=blue>redis可以连接成功，但是你要处理的键不在你连接的节点上，那么这时候redis 会告诉你键所在的节点即：MOVED 15160 192.168.50.177:8003</font>**

**<font color=red>我们用Node ID来标识集群中的节点， 但是为了让客户端的转向操作尽可能地简单， 节点在 MOVED 错误中直接返回目标节点的 IP 和端口号， 。我们客户端记录槽15495由节点 192.168.50.177:8003负责处理，然后通过这一信息处理键数据</font>**

**<font color=green>可以用返回的MOVED 错误里的IP和端口连接就可以了，即客户端可以直接向正确的节点发送命令请求，或者也可以捕获异常的方式处理。我们这里用了第二种</font>**
```python
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
# pip install redis
# Python 3.9.0
'''
@File    :   redisclear.py
@Time    :   2021/12/1 10:32:56
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   redis key-value --> clear
'''

# here put the import lib
import redis
import time

ip = '192.168.26.26'
password = '************'
port = 8333



conn=redis.Redis(host=ip,password=password,port=port,decode_responses=True)

print("redis conntion succeed ",conn,"\n")

def ex(conn):
     time.sleep(3)
     print("old key=",conn.get('GROUP_CIRCUIT-3'),"\n")
     conn.set('GROUP_CIRCUIT-3',600)
     time.sleep(3)
     print("new key=",conn.get('GROUP_CIRCUIT-3'),"\n")


if __name__ == '__main__':

    try:
        rs = conn.get('GROUP_CIRCUIT-3')
    except Exception as  e:
        print(e)
        c= str(e).split()
        ip = str(c[2]).split(':')[0]
        port = str(c[2]).split(':')[1]
        print(ip,port,"\n")
        conn=redis.Redis(host=ip,password=password,port=int(port),decode_responses=True)
        rs = conn.get('GROUP_CIRCUIT-3')
        ex(conn)
    else:
        ex(conn)
```

