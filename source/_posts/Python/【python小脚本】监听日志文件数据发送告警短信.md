---
title: 【python小脚本】监听日志文件异常数据发送告警短信
tags:
  - Python
categories:
  - Python
toc: true
recommend: 1
keywords: Python
uniqueId: '2022-11-08 12:23:02/【python小脚本】监听日志文件数据发送告警短信.html'
mathJax: false
date: 2022-11-08 20:23:02
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>


<!-- more -->
## 我的需求：
+ 老项目中有个用脚本启动的服务，很重要，用来做业务留痕的，涉及业务客户经常性投诉，是找第三方做的，时间长了维护需要花钱，老出各种未知bug，没办法处理所以机器上配了定时任务，定期的几天重启一次来解决。
+ 但是有时候不知道什么原因，重启脚本运行，服务没起来，crond 也没有什么调度机制。
+ 希望写个脚本做监控，当发现服务没起来，发送告警信息，或者重启服务。

## 我需要解决的问题：

+ 这里需要考虑的问题，如何在服务死掉后触发这个告警或者重启服务的动作,即健康监测的手段是什么？
+ 常见的手段比如`心跳`、`探针` 之类，心跳即服务定时向外发送存活信息，探针即外部监听者，定时向服务发送存活询问。

## 我是这样做的：

+ 目前的解决办法是通过检索 日志来 触发，类似一种探针的手段，定时读取日志文件来确认存在当天的日志来确认服务正常，通过执行命名的返回值确认。类似下面这样 
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$grep Pod demo.yaml
kind: Pod
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$echo $?
0
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$grep pod demo.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$echo $?
1
```
+ 也可以使用常见的判断方式，比如考虑重启时指定 `PID`，然后通过 PID 进程存在来判断，或者直接通过 `启动命名` 来匹配进程是否存在
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$pgrep etcd
2507
```
+ 如果希望 `systemd` 管理，可以把 启动脚本写成一个 `Service unit`，通过 `systemctl` 相关命令来判断服务存在，类似`systemctl show httpd -p ActiveState` 这样
+ 实现方式考虑　`shell` 、`python` 脚本。需要配置为定时任务

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>
***

下面为一个python 写的 Demo，通过监听日志文件中是否存在指定日期的日志来触发动作，这里插表发送短信。 

主要利用 `python` 的 `subprocess` 模块来执行命令，这是一个自带的模块，当命名返回值不为`0` 的时候，会报异常，然后通过 命令来发送执行 `SQL install` 操作。

前提是机器需要有 `python` 环境，同时需要有 `mysql` 客户端，当然连接 `mysql` 也可以通过`python`实现，但是需要装对应的模块。

当然这里 `grep` 的触发方式很简单，如果使用下面的脚本，只能判断当天日志存在，如果精确到时间需要修改正则匹配。

**需要注意的是，这并不是一个完全可信任的方式，当sql连接异常就无法做到监听的目的。同时需要考虑正则匹配的偶然性。**

```python
# -*- encoding: utf-8 -*-
"""
@File    :   ipcc_log_mis.py
@Time    :   2022/10/13 17:27:15
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   IPCC日志监控
             如果日志文件不存在，或者当天的日志没有，会发送告警短信
"""

# here put the import lib


import subprocess

parser = argparse.ArgumentParser(description='简单的日志监控：如果命令失败或者不是成功状态码，执行对应的操作')



try:
    com = 'tail -n 10 /home/****/RecordUser.log | grep -i $(date +%m-%d)'
    out_bytes = subprocess.check_output(com, shell=True)
    out_bytes = out_bytes.decode('utf-8')
    # print out_bytes
except subprocess.CalledProcessError as e:
    out_bytes = e.output  # Output generated before error
    line = 'mysql -h 192.168.50.187 -P 3306 -u PT90  *************   --database cloud -A  -e"{sql}" > /dev/null 2>&1'
    sql = "insert into nm_sms_send (accNbr,smsContent,spId) values ('181****5370','【XX平台】IPCC 日志异常，监控不到当天日志。请排查，$(date)',1);"
    sql += "insert into nm_sms_send (accNbr,smsContent,spId) values ('153****7834','【XX平台】IPCC 日志异常，监控不到当天日志。请排查，$(date)',1);"
    line = line.format(sql=sql)
    subprocess.check_output(line, shell=True)
```

+ 理解不足之处小伙伴帮忙指正，生活加油 `^_^`








