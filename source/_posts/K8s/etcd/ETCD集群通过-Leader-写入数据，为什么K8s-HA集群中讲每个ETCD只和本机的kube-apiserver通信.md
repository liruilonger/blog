---
title: ETCD集群通过 Leader 写入数据，为什么K8s HA集群中讲每个 kube-apiserver 只和本机的 ETCD 通信
tags:
  - etcd
categories:
  - etcd
toc: true
recommend: 1
keywords: etcd
uniqueId: >-
  2023-02-12 06:12:33/ETCD集群通过 Leader 写入数据，为什么K8s
  HA集群中讲每个ETCD只和本机的kube-apiserver通信.html
mathJax: false
date: 2023-02-12 14:12:33
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 对这个我不太明白，所有在 `stackOverflow` 的请教了大佬
+ 这里分享给小伙伴
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

### 我的问题

![在这里插入图片描述](https://img-blog.csdnimg.cn/1d17303138b54413a05c605a79a15a79.png)


搭建高可用的 k8s 集群，需要 搭建 etcd 集群，在 k8s 官方文档里面，我看到这样讲。

>每个控制平面节点创建一个本地 etcd 成员(member)，这个 etcd 成员只与该节点的 kube-apiserver 通信。 这同样适用于本地 kube-controller-manager 和 kube-scheduler 实例。


即 `kube-apiservice`  只和自己节点的 `etcd` 通信，是否可以理解为 读写都是发生在 同一节点的 `etcd` , 

但是我在学习 `etcd` 的时候， 告诉我 `etcd ` 集群中 客户端 通过 `Follower` 读取数据，通过 `Leader` 写入数据. 

实际上  `Leader`  在集群中只存在一个。 `etcd` 集群的这种读写分离适用于 `k8s` ?  

我理解这和上面讲的 有些矛盾。我想知道`k8s` 集群中 `etcd` 的读写方式是怎么回事，我对 etcd 了解甚少，感谢为我解惑

---

### @解答

![在这里插入图片描述](https://img-blog.csdnimg.cn/36be5fc8fd5e4d0db0299b54c3253978.png)


>In fact, there is only one Leader in the cluster. Does this read/write separation of etcd clusters apply to Kubernetes?

Yes. In an etc cluster, there is only one leader that does the writes. But etcd internally forwards all requests that needs _consensus_ (e.g. writes) to the _leader_, so the client application (Kubernetes in our case) does not need to know what etcd node is the leader.

From [etcd FAQ][1]:

>**Do clients have to send requests to the etcd leader?**

>Raft is leader-based; the leader handles all client requests which need cluster consensus. However, the client does not need to know which node is the leader. Any request that requires consensus sent to a follower is automatically forwarded to the leader. Requests that do not require consensus (e.g., serialized reads) can be processed by any cluster member.


  [1]: https://etcd.io/docs/v3.5/faq/

---

大概意思说，在 etcd 集群中，只有一个负责写入的领导者。但是 etcd 在内部将所有 `需要共识(例如写入)的请求转发给领导者`，因此客户端应用程序(在我们的例子中是 Kubernetes)`不需要知道哪个 etcd 节点是领导者`。


#### 来自etcd 常见问题解答：

客户是否必须向 etcd 领导者发送请求？

`Raft` 是基于领导者的；领导者处理所有需要集群共识的客户端请求。但是，客户端不需要知道哪个节点是领导者。`发送给跟随者的任何需要达成共识的请求都会自动转发给领导者。不需要共识的请求(例如，序列化读取)可以由任何集群成员处理。`

#### 名词解释

`Raft` ：etcd 使用`Raft协议`来维护集群内各个节点状态的一致性。实现强一致性。

`Leader`, `Follower`, `Candidate`：集群角色，当集群初始化时候，每个节点都是`Follower`角色，`通过心跳与其他节点同步数据`
，通过选举产生 `Leader`,之后通过`Follower`读取数据，通过`Leader`写入数据，当`Follower`在一定时间内没有收到来自`Leader`的心跳，会将自己角色改变为`Candidate`，并发起`一次选主投票`。


## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***

https://etcd.io/docs/v3.5/faq/

https://stackoverflow.com/questions/75424632/how-etcd-cluster-data-is-read-and-synchronized-under-k8s-clusters

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
