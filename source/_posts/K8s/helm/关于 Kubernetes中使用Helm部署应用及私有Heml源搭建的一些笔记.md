---
title: 关于Kubernetes中使用Helm部署应用及私有Helm源搭建的一些笔记
tags:
  - Kubernetes
  - Helm
categories:
  - Helm
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2022-01-18 17:11:10/关于Kubernetes中使用Helm部署应用及私有Helm源搭建的一些笔记.html'
mathJax: false
date: 2022-01-19 01:11:10
thumbnail:
---

**<font color="009688"> “不爱也不恨”包含了全部世俗智慧的一半；“不要说话也不要相信”则包含了另一半的人生智慧。——叔本华《人生的智慧》**</font>
<!-- more -->
## 写在前面
***
+ 学习`K8s`涉及，整理笔记记忆
+ 博文偏实战，内容涉及：
  + `Helm`的基本概念及安装，`Helm`源配置
  + `chart`包的安装部署
  + 私有`Helm`源的搭建及`chart`包的`push`和`pull`
  


**<font color="009688"> “不爱也不恨”包含了全部世俗智慧的一半；“不要说话也不要相信”则包含了另一半的人生智慧。——叔本华《人生的智慧》**</font>
 ***


# <font color=seagreen>Helm</font>

>`Helm`是一个由`CNCF`孵化和管理的项目,用于对需要在`Kubernetes`上部署的复杂应用进行定义、安装和更新。`Helm以Chart`的方式对应用软件进行描述,可以方便地创建、版本化、共享和发布复杂的应用软件。

**<font color=seagreen>`helm`的作用就是把许多的资源定义 比如`svc`，`deployment`，一次性通过全部定义好，放在源里统一管理，这样很容易在其他机器上部署，个人理解这个类似于`自动化运维`中`ansible`中的`角色`概念，`前端项目`中的`npm`包管理工具,`后端项目`中的`maven`等构建工具一样，类比`Ansible`使用角色来整合`playbook.yaml`达到复用性。同样的，使用helm用于整合k8s中的资源对象yaml文件，实现复用性,同时讲资源文件的参数，和参数值通过temple和value进行了分离</font>**



## <font color=royalblue>Helm主要概念</font>

**<font color=brown>Chart</font>**:一个Helm包,其中包含运行一个应用所需要的`工具和资源定义`,还可能包含`Kubernetes集群`中的`服务定义`,类似`Ansible`中的`rhel-system-roles`软件包或者`ansible-galaxy`工具：

**<font color=royalblue>Release</font>**: 在`Kubernetes`集群上运行的一个`Chart`实例。在同一个集群上,一个`Chart`可以被安装多次。

**<font color=red>Repository</font>**:用于`存放和共享Chart仓库`。**<font color=blue>简单来说, Helm整个系统的主要任务就是,在仓库中查找需要的Chart,然后将Chart以Release的形式安装到Kubernetes集群中。</font>**

>**<font color=royalblue>使用helm我们首先需要安装，可以通过Github下载安装包</font>**

## <font color=tomato>Helm 安装</font>
**<font color=blue>安装包下载:https://github.com/helm/helm/releases</font>**:

**<font color=purple>解压安装</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$tar zxf helm-v3.2.1-linux-amd64.tar.gz
┌──[root@vms81.liruilongs.github.io]-[~]
└─$cd linux-amd64/
┌──[root@vms81.liruilongs.github.io]-[~/linux-amd64]
└─$ls
helm  LICENSE  README.md
```
**<font color=tomato>之后直接将helm复制到`/usr/local/bin/`，配置完之后，即可以使用helm命令</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/linux-amd64]
└─$cp helm  /usr/local/bin/
┌──[root@vms81.liruilongs.github.io]-[~/linux-amd64]
└─$ls /usr/local/bin/
helm
```
**<font color=tomato>配置命令自动补全，通过写入`/etc/profile ` 文件 `souece < (helm completion bash)`的方式配置命令自动补全。配置完记得使用`source /etc/profile`去刷新配置</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm --help | grep bash
  completion  generate autocompletions script for the specified shell (bash or zsh)
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$vim /etc/profile
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$source /etc/profile
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$cat /etc/profile | grep -v ^# | grep   source
source <(kubectl completion bash)
source <(helm completion bash)
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$
```
**<font color=tomato>验证安装,查看Helm版本</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm version
version.BuildInfo{Version:"v3.2.1", GitCommit:"fe51cd1e31e6a202cba7dead9552a6d418ded79a", GitTreeState:"clean", GoVersion:"go1.13.10"}
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$
```
## <font color=chocolate>配置helm源</font>

**<font color=blue>使用helm需要配置yaml源,常见的有阿里。微软，和Githup上的源</font>**

+ 阿里云的源 https://apphub.aliyuncs.com
+ 微软azure的源 http://mirror.azure.cn/kubernetes/charts/

**<font color=chocolate>查看所以的源</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm repo list  #查看所以的源
Error: no repositories to show
```
**<font color=chocolate>添加指定是源</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm repo add azure http://mirror.azure.cn/kubernetes/charts/

"azure" has been added to your repositories
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm repo add ali https://apphub.aliyuncs.com
"ali" has been added to your repositories
```
**<font color=yellowgreen>查看刚才添加的yum源</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm repo list
NAME    URL
azure   http://mirror.azure.cn/kubernetes/charts/
ali     https://apphub.aliyuncs.com
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$
```
## <font color=seagreen>Helm的常见用法</font>

>Helm的常见用法，包括搜索Chart、安装Chart、自定义Chart配置、更新或回滚Release、删除Release、创建自定义Chart、搭建私有仓库等

### <font color=seagreen>helm search：搜索可用的Chart</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm search  repo mysql
NAME                            CHART VERSION   APP VERSION     DESCRIPTION

ali/mysql                       6.8.0           8.0.19          Chart to create a Highly available MySQL cluster
ali/mysqldump                   2.6.0           2.4.1           A Helm chart to help backup MySQL databases usi...
ali/mysqlha                     1.0.0           5.7.13          MySQL cluster with a single master and zero or ...
ali/prometheus-mysql-exporter   0.5.2           v0.11.0         A Helm chart for prometheus mysql exporter with...
azure/mysql                     1.6.9           5.7.30          DEPRECATED - Fast, reliable, scalable, and easy...
azure/mysqldump                 2.6.2           2.4.1           DEPRECATED! - A Helm chart to help backup MySQL...
azure/prometheus-mysql-exporter 0.7.1           v0.11.0         DEPRECATED A Helm chart for prometheus mysql 
。。。。。。。。。。

```
### <font color=blue>chart包拉取</font>

**<font color=brown>安装chart可以直接使用命令安装，也可以拉取到本地之后安装，也可以直接通过命名行安装</font>**

+ **<font color=camel>本地的Chart压缩包(helm install mysql-1.6.4.tgz)</font>**
+ **<font color=blue>一个Chart目录(helm install mysql/)</font>**
+ **<font color=yellowgreen>一个完整的URL(helm install https://example.com/charts/mysql-1.6.4.tgz)</font>**

**<font color=camel>chart包拉取</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm pull azure/mysql --version=1.6.4
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$ls
mysql-1.6.4.tgz
```

### <font color=red>helm install：安装Chart</font>
**<font color=yellowgreen>chart包直接安装</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$#helm install db azure/mysql --version=1.6.4
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$
```
**<font color=yellowgreen>拉取的chart包详细信息，通过解压之后查看</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$ls
mysql-1.6.4.tgz
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$tar zxf mysql-1.6.4.tgz
.......
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$ls
mysql  mysql-1.6.4.tgz
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$cd mysql/
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$ls
Chart.yaml  README.md  templates  values.yaml
```

|yaml文件|描述|
|--|--|
|Chart.yaml  |用于描述Chart信息的YAML文件,版本定义等|
|README.md   |可选: README文件|
|values.yaml |默认的配置值，即把可变的值抽离出来。|
|templates   |可选:即模板，结合values.yaml,能够生成Kubernetes的manifest文件|

**<font color=royalblue>对于下载好的`yaml`文件，我们可以修改后使用`helm package `重新打包</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$rm -rf mysql-1.6.4.tgz ; helm package  mysql/
Successfully packaged chart and saved it to: /root/ansible/k8s-helm-create/mysql-1.6.4.tgz
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$ls
mysql  mysql-1.6.4.tgz
```
**<font color=amber>下面我们修改chart中的对应镜像为已经下载好的mysql和busybox镜像</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.82 -m shell -a "docker images | grep mysql"
192.168.26.82 | CHANGED | rc=0 >>
mysql                                                     latest    ecac195d15af   2 months ago    516MB
mysql                                                     <none>    9da615fced53   3 months ago    514MB
hub.c.163.com/library/mysql                               latest    9e64176cd8a2   4 years ago     407MB
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.82 -m shell -a "docker images | grep busybox"
192.168.26.82 | CHANGED | rc=0 >>
busybox                                                   latest    ffe9d497c324   5 weeks ago     1.24MB
busybox                                                   <none>    7138284460ff   2 months ago    1.24MB
busybox                                                   <none>    cabb9f684f8b   2 months ago    1.24MB
busybox                                                   1.27      6ad733544a63   4 years ago     1.13MB
yauritux/busybox-curl                                     latest    69894251bd3c   5 years ago     21.3MB
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$fg
vim ./k8s-helm-create/mysql/values.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
**<font color=amber>通过修好的yaml文件创建chart</font>**， **<font color=amber>使用`helm ls`查看当前运行的chart</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm ls
NAME    NAMESPACE       REVISION        UPDATED STATUS  CHART   APP VERSION
```
**<font color=orange>使用`helm install`运行Chart</font>**
**<font color=tomato>这里我们使用之前的那个mysq chart来安装一个mysql</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$cd mysql/
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$helm install mydb .
NAME: mydb
LAST DEPLOYED: Thu Jan 13 01:51:42 2022
NAMESPACE: liruilong-network-create
STATUS: deployed
REVISION: 1
NOTES:
MySQL can be accessed via port 3306 on the following DNS name from within your cluster:
mydb-mysql.liruilong-network-create.svc.cluster.local

To get your root password run:

    MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace liruilong-network-create mydb-mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo)

To connect to your database:

1. Run an Ubuntu pod that you can use as a client:

    kubectl run -i --tty ubuntu --image=ubuntu:16.04 --restart=Never -- bash -il

2. Install the mysql client:

    $ apt-get update && apt-get install mysql-client -y

3. Connect using the mysql cli, then provide your password:
    $ mysql -h mydb-mysql -p

To connect to your database directly from outside the K8s cluster:
    MYSQL_HOST=127.0.0.1
    MYSQL_PORT=3306

    # Execute the following command to route the connection:
    kubectl port-forward svc/mydb-mysql 3306

    mysql -h ${MYSQL_HOST} -P${MYSQL_PORT} -u root -p${MYSQL_ROOT_PASSWORD}
```
**<font color=brown>查看是否是否运行成功mydb的pod和SVC</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$kubectl  get pods
NAME                          READY   STATUS    RESTARTS        AGE
mydb-mysql-7f8c5c47bd-82cts   1/1     Running   0               55s
pod1                          1/1     Running   2 (7d17h ago)   9d
pod2                          1/1     Running   3 (3d3h ago)    9d
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$kubectl  get svc
NAME         TYPE           CLUSTER-IP       EXTERNAL-IP      PORT(S)        AGE
mydb-mysql   ClusterIP      10.107.17.103    <none>           3306/TCP       62s
svc1         LoadBalancer   10.106.61.84     192.168.26.240   80:30735/TCP   9d
svc2         LoadBalancer   10.111.123.194   192.168.26.241   80:31034/TCP   9d
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$
```
**<font color=yellowgreen>安装一个mysql客户端测试OK</font>**
```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$yum install  mariadb -y
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$mysql -h10.107.17.103 -uroot -ptesting
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MySQL connection id is 7
Server version: 5.7.18 MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MySQL [(none)]> use mysql
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
```
**<font color=yellowgreen>删除Release</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$helm del mydb
release "mydb" uninstalled
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$helm ls
NAME    NAMESPACE       REVISION        UPDATED STATUS  CHART   APP VERSION
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create/mysql]
└─$
```
## **<font color=green>搭建私有Repository</font>**
**<font color=tomato>自建的Chart之后自然需要搭建私有仓库。下面使用Nginx搭建一个简单的Chart私有仓库</font>**

### <font color=seagreen>仓库搭建</font>
**<font color=orange>仓库搭建,找一台机器运行一个Nginx服务做仓库，需要注意要对主页数据做映射</font>**
```bash
┌──[root@vms83.liruilongs.github.io]-[~]
└─$netstat  -ntulp | grep 80
┌──[root@vms83.liruilongs.github.io]-[~]
└─$docker run -dit --name=helmrepo -p 8080:80 -v /data:/usr/share/nginx/html/charts docker.io/nginx
7201e001b02602f087105ca6096b0816acb03db02296c35c098a3dfddcb9c8d0
┌──[root@vms83.liruilongs.github.io]-[~]
└─$docker ps | grep helmrepo
7201e001b026   nginx                                               "/docker-entrypoint.…"   16 seconds ago      Up 15 seconds      0.0.0.0:8080->80/tcp, :::8080->80/tcp   helmrepo
```
**<font color=blue>访问测试</font>**
```bash
┌──[root@vms83.liruilongs.github.io]-[~]
└─$curl 127.0.0.1:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
。。。。。。。。
```
### <font color=red>chart包上传</font>
**<font color=brown>打包之前的mysql包，上传helm私有仓库。这里需要本读生成索引文件`index.yaml`</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm  package mysql/
Successfully packaged chart and saved it to: /root/ansible/k8s-helm-create/mysql-1.6.4.tgz
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm repo index . --url http://192.168.26.83:8080/charts
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$ls
index.yaml  mysql  mysql-1.6.4.tgz
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$cd ..
```
**<font color=seagreen>将索引文件和chart包一同上传到私有仓库</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.83 -m copy -a "src=./k8s-helm-create/index.yaml dest=/data/"
192.168.26.83 | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": true,
    "checksum": "233a0f3837d46af8a50098f1b29aa524b751cb29",
    "dest": "/data/index.yaml",
    "gid": 0,
    "group": "root",
    "md5sum": "66953d9558e44ab2f049dc602600ffda",
    "mode": "0644",
    "owner": "root",
    "size": 843,
    "src": "/root/.ansible/tmp/ansible-tmp-1642011407.72-76313-71345316897038/source",
    "state": "file",
    "uid": 0
}
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.83 -m copy -a "src=./k8s-helm-create/mysql-1.6.4.tgz dest=/data/"
192.168.26.83 | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": true,
    "checksum": "4fddb1c13c71673577570e61f68f926af7255bad",
    "dest": "/data/mysql-1.6.4.tgz",
    "gid": 0,
    "group": "root",
    "md5sum": "929267de36f9be04e0adfb2f9c9f5812",
    "mode": "0644",
    "owner": "root",
    "size": 11121,
    "src": "/root/.ansible/tmp/ansible-tmp-1642011437.58-76780-127185287864942/source",
    "state": "file",
    "uid": 0
}
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
### <font color=purple>仓库索引文件更新</font>
**<font color=blue>如果添加新的chart包到私有仓库，需要对于索引文件进行更新</font>**

**<font color=amber>helm create 创建一个自定义的chart包</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm create  liruilonghelm
Creating liruilonghelm
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$ls
index.yaml  liruilonghelm  mysql  mysql-1.6.4.tgz
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm package liruilonghelm/
Successfully packaged chart and saved it to: /root/ansible/k8s-helm-create/liruilonghelm-0.1.0.tgz
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$ls
index.yaml  liruilonghelm  liruilonghelm-0.1.0.tgz  mysql  mysql-1.6.4.tgz
```
**<font color=yellowgreen>使用同样的命令跟新索引文件</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm repo index . --url http://192.168.26.83:8080/charts
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$cat index.yaml
```
**<font color=orange>查看新的索引文件</font>**
```yaml
apiVersion: v1
entries:
  liruilonghelm:
  - apiVersion: v2
    appVersion: 1.16.0
    created: "2022-01-13T02:22:19.442365047+08:00"
    description: A Helm chart for Kubernetes
    digest: abb491f061cccc8879659149d96c99cbc261af59d5fcf8855c5e86251fbd53c1
    name: liruilonghelm
    type: application
    urls:
    - http://192.168.26.83:8080/charts/liruilonghelm-0.1.0.tgz
    version: 0.1.0
  mysql:
  - apiVersion: v1
    appVersion: 5.7.30
    created: "2022-01-13T02:22:19.444985984+08:00"
    description: Fast, reliable, scalable, and easy to use open-source relational
      database system.
    digest: 29153332e509765010c7e5e240a059550d52b01b31b69f25dd27c136dffec40f
    home: https://www.mysql.com/
    icon: https://www.mysql.com/common/logos/logo-mysql-170x115.png
    keywords:
    - mysql
    - database
    - sql
    maintainers:
    - email: o.with@sportradar.com
      name: olemarkus
    - email: viglesias@google.com
      name: viglesiasce
    name: mysql
    sources:
    - https://github.com/kubernetes/charts
    - https://github.com/docker-library/mysql
    urls:
    - http://192.168.26.83:8080/charts/mysql-1.6.4.tgz
    version: 1.6.4
generated: "2022-01-13T02:22:19.440764685+08:00"
```
**<font color=camel>会发现索引文件已经被更新，entries里有两个对象，上传相关的数据</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.83 -m copy -a "src=./k8s-helm-create/index.yaml dest=/data/"
192.168.26.83 | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": true,
    "checksum": "dbdc550a24159764022ede9428b9f11a09ccf291",
    "dest": "/data/index.yaml",
    "gid": 0,
    "group": "root",
    "md5sum": "b771d8e50dd49228594f8a566117f8bf",
    "mode": "0644",
    "owner": "root",
    "size": 1213,
    "src": "/root/.ansible/tmp/ansible-tmp-1642012325.1-89511-190591844764611/source",
    "state": "file",
    "uid": 0
}
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.83 -m copy -a "src=./k8s-helm-create/liruilonghelm-0.1.0.tgz dest=/data/"
192.168.26.83 | CHANGED => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": true,
    "checksum": "f7fe8a0a7585adf23e3e23f8378e3e5a0dc13f92",
    "dest": "/data/liruilonghelm-0.1.0.tgz",
    "gid": 0,
    "group": "root",
    "md5sum": "04670f9b7e614d3bc6ba3e133bddae59",
    "mode": "0644",
    "owner": "root",
    "size": 3591,
    "src": "/root/.ansible/tmp/ansible-tmp-1642012352.54-89959-104738456182106/source",
    "state": "file",
    "uid": 0
}
```
### <font color=seagreen>用私有仓库chart部署应用程序</font>
#### <font color=purple>添加私有源</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$helm repo add liruilong_repo http://192.168.26.83:8080/charts
"liruilong_repo" has been added to your repositories
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$helm repo list
NAME            URL
azure           http://mirror.azure.cn/kubernetes/charts/
ali             https://apphub.aliyuncs.com
liruilong_repo  http://192.168.26.83:8080/charts
```
**<font color=orange>私有源查找安装的chart</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$helm search repo mysql | grep liruilong
liruilong_repo/mysql            1.6.4           5.7.30          Fast, reliable, scalable, and easy to use open-...
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
#### <font color=camel>安装私有源chart</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$helm list
NAME    NAMESPACE       REVISION        UPDATED STATUS  CHART   APP VERSION
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm install liruilongdb  liruilong_repo/mysql
NAME: liruilongdb
LAST DEPLOYED: Thu Jan 13 02:42:41 2022
NAMESPACE: liruilong-network-create
STATUS: deployed
REVISION: 1
NOTES:
MySQL can be accessed via port 3306 on the following DNS name from within your cluster:
liruilongdb-mysql.liruilong-network-create.svc.cluster.local

To get your root password run:

    MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace liruilong-network-create liruilongdb-mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo)

To connect to your database:

1. Run an Ubuntu pod that you can use as a client:

    kubectl run -i --tty ubuntu --image=ubuntu:16.04 --restart=Never -- bash -il

2. Install the mysql client:

    $ apt-get update && apt-get install mysql-client -y

3. Connect using the mysql cli, then provide your password:
    $ mysql -h liruilongdb-mysql -p

To connect to your database directly from outside the K8s cluster:
    MYSQL_HOST=127.0.0.1
    MYSQL_PORT=3306

    # Execute the following command to route the connection:
    kubectl port-forward svc/liruilongdb-mysql 3306

    mysql -h ${MYSQL_HOST} -P${MYSQL_PORT} -u root -p${MYSQL_ROOT_PASSWORD}
```
**<font color=camel>验证安装，查看chart列表</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm list
NAME            NAMESPACE                       REVISION        UPDATED                                 STATUS         CHART            APP VERSION
liruilongdb     liruilong-network-create        1               2022-01-13 02:42:41.537928447 +0800 CST deployed       mysql-1.6.4      5.7.30
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$
```
**<font color=red>在`helm install`命令的执行过程中，可以使用`helm status`命令跟踪 `Release`的状态：`Helm`不会等待所有创建过程的完成，这是因为有些Chart的`Docker`镜像较大，会消耗很长的时间进行下载和创建</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm status  liruilongdb
NAME: liruilongdb
LAST DEPLOYED: Thu Jan 13 02:42:41 2022
NAMESPACE: liruilong-network-create
STATUS: deployed
REVISION: 1
NOTES:
MySQL can be accessed via port 3306 on the following DNS name from within your cluster:
liruilongdb-mysql.liruilong-network-create.svc.cluster.local

To get your root password run:

    MYSQL_ROOT_PASSWORD=$(kubectl get secret --namespace liruilong-network-create liruilongdb-mysql -o jsonpath="{.data.mysql-root-password}" | base64 --decode; echo)

To connect to your database:

1. Run an Ubuntu pod that you can use as a client:

    kubectl run -i --tty ubuntu --image=ubuntu:16.04 --restart=Never -- bash -il

2. Install the mysql client:

    $ apt-get update && apt-get install mysql-client -y

3. Connect using the mysql cli, then provide your password:
    $ mysql -h liruilongdb-mysql -p

To connect to your database directly from outside the K8s cluster:
    MYSQL_HOST=127.0.0.1
    MYSQL_PORT=3306

    # Execute the following command to route the connection:
    kubectl port-forward svc/liruilongdb-mysql 3306

    mysql -h ${MYSQL_HOST} -P${MYSQL_PORT} -u root -p${MYSQL_ROOT_PASSWORD}
```

**<font color=red>在成功安装Chart后，系统会在当前命名空间内创建一个ConfigMap用于保存Release对象的数据</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$kubectl  get configmaps
NAME                     DATA   AGE
kube-root-ca.crt         1      12d
liruilongdb-mysql-test   1      2d19h
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$kubectl  describe configmaps liruilongdb-mysql-test
Name:         liruilongdb-mysql-test
Namespace:    liruilong-network-create
Labels:       app=liruilongdb-mysql
              app.kubernetes.io/managed-by=Helm
              chart=mysql-1.6.4
              heritage=Helm
              release=liruilongdb
Annotations:  meta.helm.sh/release-name: liruilongdb
              meta.helm.sh/release-namespace: liruilong-network-create

Data
====
run.sh:
----
@test "Testing MySQL Connection" {
  mysql --host=liruilongdb-mysql --port=3306 -u root -ptesting
}

BinaryData
====

Events:  <none>
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$
```
**<font color=brown>可以通过 `helm delete `命令删除运行的`Release`</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$kubectl  get pods
NAME                                 READY   STATUS    RESTARTS      AGE
liruilongdb-mysql-5cbf489f65-6ff4q   1/1     Running   1 (56m ago)   26h
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$helm delete liruilongdb
release "liruilongdb" uninstalled
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$kubectl  get pods
NAME                                 READY   STATUS        RESTARTS      AGE
liruilongdb-mysql-5cbf489f65-6ff4q   1/1     Terminating   1 (57m ago)   26h
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$
```
>关于Helm和小伙伴就分享到这里，其实还有很大，比如chart更新回滚，模板导出等。有遇到的做补充。生活加油
