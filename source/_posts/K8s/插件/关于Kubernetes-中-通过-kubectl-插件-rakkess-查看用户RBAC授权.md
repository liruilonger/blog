---
title: K8s:通过 kubectl 插件 rakkess 查看集群 RBAC授权信息
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: rakkess
uniqueId: '2023-01-18 16:05:30/关于Kubernetes 中 通过 kubectl 插件 rakkess 查看用户RBAC授权.html'
mathJax: false
date: 2023-01-18 00:05:30
thumbnail:
---

**<font color="009688"> 出其东门，有女如云。虽则如云，匪我思存。缟衣綦巾，聊乐我员。——《郑风·出其东门》**</font>
<!-- more -->
## 写在前面

***
+ 分享一个 查看  `RBAC` 权限的工具
+ 通过 `rakkess` 可以查看当前命名空间 rbac 的授权
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 出其东门，有女如云。虽则如云，匪我思存。缟衣綦巾，聊乐我员。——《郑风·出其东门》**</font>

***

在 `K8s` 中集群权限管理中，常常使用 `SA+token 、ca证书` 的认证方式，使用 `RBAC` 的鉴权方式，往往通过不同命名空间实施最小权限原则来保证他们的集群安全并在不同的集群租户之间创建隔离。 sa 和 ca证书都涉及 赋权，k8s 提供了，角色，集群角色，角色绑定，集群角色绑定等 API 资源来查看集群信息。

### 安装

如果安装了 `krew` 并且可以科学上网，可以通过下面的方式安装

```bash
kubectl krew install access-matrix
```

如果没有，可以通过二进制的方式安装

```bash
curl -LO https://github.com/corneliusweig/rakkess/releases/download/v0.5.0/rakkess-amd64-linux.tar.gz 
```
解压编译配置为 kubectl 插件。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$tar -zxvf rakkess-amd64-linux.tar.gz
LICENSE
rakkess-amd64-linux
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv rakkess-amd64-linux kubectl-rakkess
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv kubectl-rakkess /usr/local/bin/
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl rakkess version
v0.5.0
```

查看当前命名空间的  `rbac` 权限。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl rakkess --namespace default
NAME                                            LIST  CREATE  UPDATE  DELETE
                                                ✖     ✖       ✖       ✖
............
alertmanagerconfigs.monitoring.coreos.com       ✔     ✔       ✔       ✔
alertmanagers.monitoring.coreos.com             ✔     ✔       ✔       ✔
awxbackups.awx.ansible.com                      ✔     ✔       ✔       ✔
awxrestores.awx.ansible.com                     ✔     ✔       ✔       ✔
awxs.awx.ansible.com                            ✔     ✔       ✔       ✔
bindings                                              ✔
configmaps                                      ✔     ✔       ✔       ✔
controllerrevisions.apps                        ✔     ✔       ✔       ✔
cronjobs.batch                                  ✔     ✔       ✔       ✔
csistoragecapacities.storage.k8s.io             ✔     ✔       ✔       ✔
daemonsets.apps                                 ✔     ✔       ✔       ✔
deployments.apps                                ✔     ✔       ✔       ✔
endpoints                                       ✔     ✔       ✔       ✔
endpointslices.discovery.k8s.io                 ✔     ✔       ✔       ✔
events                                          ✔     ✔       ✔       ✔
events.events.k8s.io                            ✔     ✔       ✔       ✔
horizontalpodautoscalers.autoscaling            ✔     ✔       ✔       ✔
ingresses.networking.k8s.io                     ✔     ✔       ✔       ✔
jobs.batch                                      ✔     ✔       ✔       ✔
leases.coordination.k8s.io                      ✔     ✔       ✔       ✔
limitranges                                     ✔     ✔       ✔       ✔
localsubjectaccessreviews.authorization.k8s.io        ✔
networkpolicies.crd.projectcalico.org           ✔     ✔       ✔       ✔
networkpolicies.networking.k8s.io               ✔     ✔       ✔       ✔
networksets.crd.projectcalico.org               ✔     ✔       ✔       ✔
persistentvolumeclaims                          ✔     ✔       ✔       ✔
poddisruptionbudgets.policy                     ✔     ✔       ✔       ✔
podmonitors.monitoring.coreos.com               ✔     ✔       ✔       ✔
pods                                            ✔     ✔       ✔       ✔
podtemplates                                    ✔     ✔       ✔       ✔
probes.monitoring.coreos.com                    ✔     ✔       ✔       ✔
prometheuses.monitoring.coreos.com              ✔     ✔       ✔       ✔
prometheusrules.monitoring.coreos.com           ✔     ✔       ✔       ✔
replicasets.apps                                ✔     ✔       ✔       ✔
replicationcontrollers                          ✔     ✔       ✔       ✔
resourcequotas                                  ✔     ✔       ✔       ✔
rolebindings.rbac.authorization.k8s.io          ✔     ✔       ✔       ✔
roles.rbac.authorization.k8s.io                 ✔     ✔       ✔       ✔
secrets                                         ✔     ✔       ✔       ✔
serviceaccounts                                 ✔     ✔       ✔       ✔
servicemonitors.monitoring.coreos.com           ✔     ✔       ✔       ✔
services                                        ✔     ✔       ✔       ✔
statefulsets.apps                               ✔     ✔       ✔       ✔
thanosrulers.monitoring.coreos.com              ✔     ✔       ✔       ✔
```


查看给定 API 资源的 RBAC  权限

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl rakkess resource cm
NAME                                    KIND            SA-NAMESPACE          LIST  CREATE  UPDATE  DELETE
admin-user                              ServiceAccount  kubernetes-dashboard  ✔     ✔       ✔       ✔
generic-garbage-collector               ServiceAccount  kube-system           ✔     ✖       ✔       ✔
horizontal-pod-autoscaler               ServiceAccount  kube-system           ✔     ✖       ✖       ✖
ingress-nginx                           ServiceAccount  ingress-nginx         ✔     ✖       ✖       ✖
kubernetes-dashboard                    ServiceAccount  kubernetes-dashboard  ✔     ✔       ✔       ✔
kuboard-user                            ServiceAccount  kube-system           ✔     ✔       ✔       ✔
kuboard-viewer                          ServiceAccount  kube-system           ✔     ✖       ✖       ✖
liruilong                               User                                  ✔     ✔       ✔       ✔
local-path-provisioner-service-account  ServiceAccount  local-path-storage    ✔     ✖       ✖       ✖
namespace-controller                    ServiceAccount  kube-system           ✔     ✖       ✖       ✔
resourcequota-controller                ServiceAccount  kube-system           ✔     ✖       ✖       ✖
root-ca-cert-publisher                  ServiceAccount  kube-system           ✖     ✔       ✔       ✖
system:kube-controller-manager          User                                  ✔     ✖       ✖       ✖
system:masters                          Group                                 ✔     ✔       ✔       ✔
Only ClusterRoleBindings are considered, because no namespace is given.
```

查询在的时候可以指定查询的权限

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl rakkess r cm --verbs get,delete,watch,patch
NAME                                    KIND            SA-NAMESPACE          GET  DELETE  WATCH  PATCH
admin-user                              ServiceAccount  kubernetes-dashboard  ✔    ✔       ✔      ✔
calico-node                             ServiceAccount  kube-system           ✔    ✖       ✖      ✖
generic-garbage-collector               ServiceAccount  kube-system           ✔    ✔       ✔      ✔
horizontal-pod-autoscaler               ServiceAccount  kube-system           ✔    ✖       ✖      ✖
ingress-nginx                           ServiceAccount  ingress-nginx         ✖    ✖       ✔      ✖
kubernetes-dashboard                    ServiceAccount  kubernetes-dashboard  ✔    ✔       ✔      ✔
kuboard-user                            ServiceAccount  kube-system           ✔    ✔       ✔      ✔
kuboard-viewer                          ServiceAccount  kube-system           ✔    ✖       ✔      ✖
liruilong                               User                                  ✔    ✔       ✔      ✔
local-path-provisioner-service-account  ServiceAccount  local-path-storage    ✔    ✖       ✔      ✖
namespace-controller                    ServiceAccount  kube-system           ✔    ✔       ✖      ✖
resourcequota-controller                ServiceAccount  kube-system           ✖    ✖       ✔      ✖
system:kube-controller-manager          User                                  ✔    ✖       ✔      ✖
system:masters                          Group                                 ✔    ✔       ✔      ✔
Only ClusterRoleBindings are considered, because no namespace is given.
```


```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl rakkess --as liruilong
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl rakkess --as kube-system:namespace-controller

```

## 博文参考

***

https://github.com/corneliusweig/rakkess