---
title: K8s:kubectl 插件管理器 Krew 离线安装
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Krew
uniqueId: '2023-01-08 03:12:10/关于Kubernetes 中 kubectl 插件管理器 Krew 的一些笔记.html'
mathJax: false
date: 2023-01-08 11:12:10
thumbnail:

---

**<font color="009688"> 知我者，谓我心忧；不知我者，谓我何求。——《王风·黍离》**</font>

<!-- more -->
## 写在前面

***
+ 分享一些 `kubectl` 插件管理器 `Krew` 的笔记
+ 博文内容涉及 `Krew` 离线安装，配置为 kubectl 插件 的 Demo
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 知我者，谓我心忧；不知我者，谓我何求。——《王风·黍离》**</font>


***

`Krew`  是一个由 Kubernetes SIG CLI 社区维护的 kubectl 命令行工具的插件管理器。类似 红帽的` YUM `, 开发角度理解，类似 `Nodejs` 的 `npm`。

Krew 可以用于管理 kubelet 插件，发现 kubectl 插件，并在机器上安装它们。保持安装的插件是最新的。

Krew 适用于所有主要平台，例如 macOS、Linux 和 Windows。

需要说明的是，Krew 插件索引 所维护的 kubectl 插件并未经过安全性审查。 你要了解安装和运行第三方插件的安全风险,因为它们本质上时是一些在你的机器上 运行的程序。

国内因为墙的问题，无法正常使用，所以需要科学上网。所以如果是内网，或者是没办法科学上网，装了也没啥用。

### 下载安装

如果机器可以科学上网，可以直接运行下面的脚本。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$cat install-krew.sh
#!/bin/bash

(
  set -x; cd "$(mktemp -d)" &&
  OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
  ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
  KREW="krew-${OS}_${ARCH}" &&
  curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
  tar zxvf "${KREW}.tar.gz" &&
  ./"${KREW}" install krew
)
```
然后添加环境变量
```bash
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
```

如果没有科学上网，可以尝试浏览器直接下载。前提是你需要知道你要安装的版本。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$wget  https://github.com/kubernetes-sigs/krew/releases/latest/download/krew-linux_amd64.tar.gz
```
解压后运行下面的命令安装
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$tar -zxvf krew-linux_amd64.tar.gz
./LICENSE
./krew-linux_amd64
```
添加下面的环境变量
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$vim  ~/.bashrc
....
export PATH="${PATH}:${HOME}/.krew/bin"
.....
```
查看版本测试
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$./krew-linux_amd64 version
OPTION            VALUE
GitTag            v0.4.3
GitCommit         dbfefa5
IndexURI          https://github.com/kubernetes-sigs/krew-index.git
BasePath          /root/.krew
IndexPath         /root/.krew/index/default
InstallPath       /root/.krew/store
BinPath           /root/.krew/bin
DetectedPlatform  linux/amd64
```

### 配置为 kubectl 插件

配置为 kubectl 插件，测试
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv ./krew-linux_amd64 ./kubectl-krew
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv ./kubectl-krew /usr/local/bin/
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl krew version
OPTION            VALUE
GitTag            v0.4.3
GitCommit         dbfefa5
IndexURI          https://github.com/kubernetes-sigs/krew-index.git
BasePath          /root/.krew
IndexPath         /root/.krew/index/default
InstallPath       /root/.krew/store
BinPath           /root/.krew/bin
DetectedPlatform  linux/amd64
```

下载插件列表，查看插件列表
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl krew update
Updated the local copy of plugin index.
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl krew info
accepts 1 arg(s), received 0
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl krew search
NAME                            DESCRIPTION                                         INSTALLED
access-matrix                   Show an RBAC access matrix for server resources     no
accurate                        Manage Accurate, a multi-tenancy controller         no
advise-policy                   Suggests PodSecurityPolicies and OPA Policies f...  no
advise-psp                      Suggests PodSecurityPolicies for cluster.           no
allctx                          Run commands on contexts in your kubeconfig         no
apparmor-manager                Manage AppArmor profiles for cluster.               no
。。。。。。。。。。。。。。。。。
```

关于 krew 和小伙伴分享到这里，如果没有科学上网，或者是内网环境，这其实是一个很鸡肋的工具，安装了没啥用，还不如使用什么插件直接下载二进制包，如果有科学上网，或则使用公有云，提供了相关的科学上网环境，那还是很方便的。





## 博文参考

***
https://krew.sigs.k8s.io/

https://krew.sigs.k8s.io/docs/user-guide/setup/install/