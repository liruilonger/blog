---
title: 关于 K8s 中 Open Kruise 的一些笔记整理
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: OpenKruise
uniqueId: "2023-02-28 06:52:19/ 关于 K8s 中 Open Kruise 的一些笔记整理.html"
mathJax: false
date: 2023-02-28 14:52:19
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->

## 写在前面

---

- 一次公开课看到，这里简单认识下
- 博文内容涉及：
  - `Open Kruise ` 特性简单认知
  - `Open Kruise` 系统架构认知
  - 原地升级概念，内置控制器 `CloneSet` 简单认知
- 理解不足小伙伴帮忙指正

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

---

### OpenKruise 是什么？

Open Kruise 是一个国内的开源项目， CNCF 沙盒项目，一个基于 `Kubernetes` 的扩展套件，聚焦于云原生应用的自动化，比如部署、发布、运维以及可用性防护。

OpenKruise 提供的绝大部分能力都是基于 `CRD` 扩展来定义，它们不存在于任何 `外部依赖`，可以运行在任意纯净的 `Kubernetes` 集群中。

所以可以简单的认知，OpenKruise 是一些 `CRD` 集合，自定义控制器，用于完善 k8s 集群中标准的 API 对象提供的能力。

### 核心能力

#### 增强版本的 Workloads

工作负载用于 `管理和运行集群中的容器`。`容器` 是由 `控制器` 通过 `Pod` 创建的，在原有的 k8s API 资源中 ，提供了 Deploy、DS、StatefulSet、RS/RC Jobs 等一系列的控制器对象资源， OpenKruise 在 原生 K8s API 资源的基础上提供了一系列增强版本的 Workloads(工作负载)或者叫控制器，比如 CloneSet、Advanced StatefulSet、Advanced DaemonSet、BroadcastJob 等。

它们不仅支持类似于 Kubernetes 原生 Workloads 的基础功能，还提供了如原地升级、可配置的扩缩容/发布策略、并发操作等。

#### 应用的旁路管理(Sidecar)

OpenKruise 提供了多种通过旁路管理应用 sidecar 容器、多区域部署的方式，“旁路” 意味着你可以不需要修改应用的 Workloads 来实现它们。通俗的话讲，旁路，即可以自由的管理注入 pod 中的非业务应用容器。或者叫代理容器，而不会对 原有 Pod 产生影响。

比如，`SidecarSet` 能帮助你在所有匹配的 Pod 创建的时候都注入特定的 `sidecar` 容器，甚至可以原地升级已经注入的 sidecar 容器镜像、并且对 Pod 中其他容器不造成影响。

#### 高可用性防护

OpenKruise 在为应用的高可用性防护方面也做出了很多努力。

目前它可以保护你的 Kubernetes 资源`不受级联删除机制的干扰`，包括 CRD、Namespace、以及几乎全部的 Workloads 类型资源。

#### 高级的应用运维能力

OpenKruise 也提供了很多高级的运维能力来帮助你更好地管理应用。

你可以通过 ImagePullJob 来在任意范围的节点上预先拉取某些镜像，或者指定某个 Pod 中的一个或多个容器被原地重启。

更多特性小伙伴可以到官网了解

### 下载安装

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$helm repo add openkruise https://openkruise.github.io/charts/
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$helm repo update
Hang tight while we grab the latest from your chart repositories...
...Successfully got an update from the "botkube" chart repository
...Successfully got an update from the "openkruise" chart repository
...Successfully got an update from the "rancher-stable" chart repository
...Successfully got an update from the "awx-operator" chart repository
...Unable to get an update from the "kubescape" chart repository (https://kubescape.github.io/helm-charts/):
        read tcp 192.168.26.100:35526->185.199.109.153:443: read: connection reset by peer
Update Complete. ⎈Happy Helming!⎈
```

也可以通过下面的路径下载 helm 的静态文件的方式来安装，如果需要替换私有仓库镜像可以 通过 `helm template`导出完整 Yaml 文件来替换对应的镜像。

https://github.com/openkruise/charts/releases

如果网络没有问题，不需要替换镜像，可以直接安装，下面使用国内阿里的镜像

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$helm install kruise openkruise/kruise  --set  manager.image.repository=openkruise-registry.cn-shanghai.cr.aliyuncs.com/openkruise/kruise-manager
NAME: kruise
LAST DEPLOYED: Tue Feb 28 15:25:34 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

查看部署情况

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$helm ls
NAME    NAMESPACE       REVISION        UPDATED                                 STATUS          CHART           APP VERSION
kruise  default         1               2023-02-28 15:25:34.865181878 +0800 CST deployed        kruise-1.3.0    1.3.0
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$
```

在第一次安装的时候，DS 资源一直在重启

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl get all -n kruise-system
NAME                                             READY   STATUS             RESTARTS      AGE
pod/kruise-controller-manager-7dc584559b-j8j78   1/1     Running            0             2m31s
pod/kruise-controller-manager-7dc584559b-r954q   1/1     Running            0             2m32s
pod/kruise-daemon-24fgt                          0/1     CrashLoopBackOff   4 (9s ago)    2m33s
pod/kruise-daemon-7t5q6                          0/1     CrashLoopBackOff   4 (11s ago)   2m32s
pod/kruise-daemon-fbt8m                          0/1     CrashLoopBackOff   4 (16s ago)   2m33s
pod/kruise-daemon-fc8xr                          0/1     CrashLoopBackOff   4 (11s ago)   2m32s
pod/kruise-daemon-kjjfd                          0/1     CrashLoopBackOff   4 (15s ago)   2m32s
pod/kruise-daemon-krs9s                          0/1     CrashLoopBackOff   4 (17s ago)   2m33s
pod/kruise-daemon-lb5nq                          0/1     CrashLoopBackOff   4 (15s ago)   2m32s
pod/kruise-daemon-zpfzg                          0/1     CrashLoopBackOff   3 (32s ago)   2m32s
```

通过下面的日志查看发现,它通过默认的 CRI 接口实现找不到对应的 runtime，

```
W0228 07:29:31.671667       1 mutation_detector.go:53] Mutation detector is enabled, this will result in memory leakage.
E0228 07:29:31.671746       1 factory.go:224] /hostvarrun/docker.sock exists, but not found /hostvarrun/dockershim.sock
W0228 07:29:31.767342       1 factory.go:113] Failed to new image service for containerd (, unix:///hostvarrun/containerd/containerd.sock): failed to fetch cri-containerd status: rpc error: code = Unimplemented desc = unknown service runtime.v1alpha2.RuntimeService
W0228 07:29:31.767721       1 mutation_detector.go:53] Mutation detector is enabled, this will result in memory leakage.
panic: runtime error: invalid memory address or nil pointer dereference
```

无法识别 CRI 接口的实现，所以这里需要显示的指定 CRI 的接口实现。当前环境使用 `docker/cri-docker` 作为 CRI,所以部署时需要指定 `,daemon.socketLocation=/var/run/,daemon.socketFile=cri-dockerd.sock`，需要注意的是，如果使用 `daemon.socketFile` 那么 `daemon.socketLocation` 需要显示指定。

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$helm uninstall kruise
release "kruise" uninstalled
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$helm install  kruise openkruise/kruise  --set  manager.image.repository=openkruise-registry.cn-shanghai.cr.aliyuncs.com/openkruise/kruise-manager,daemon.socketLocation=/var/run/,daemon.socketFile=cri-dockerd.sock
NAME: kruise
LAST DEPLOYED: Tue Feb 28 17:31:27 2023
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

部署完的资源信息，可以看到除了一些配置资源，涉及到容器，pod 管理的只有下面两个控制器

- daemonset.apps/kruise-daemon
- deployment.apps/kruise-controller-manager

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl-ketall --namespace kruise-system
W0228 18:39:41.576938   63520 warnings.go:70] metallb.io v1beta1 AddressPool is deprecated, consider using IPAddressPool
NAME                                                                      NAMESPACE      AGE
configmap/istio-ca-root-cert                                              kruise-system  68m
configmap/kruise-manager                                                  kruise-system  67m
configmap/kube-root-ca.crt                                                kruise-system  68m
endpoints/kruise-webhook-service                                          kruise-system  68m
pod/kruise-controller-manager-7dc584559b-4c95v                            kruise-system  68m
pod/kruise-controller-manager-7dc584559b-bfhm2                            kruise-system  68m
pod/kruise-daemon-2hpdc                                                   kruise-system  68m
pod/kruise-daemon-69rtg                                                   kruise-system  68m
pod/kruise-daemon-89lbr                                                   kruise-system  68m
pod/kruise-daemon-fzwnh                                                   kruise-system  68m
pod/kruise-daemon-hxmdv                                                   kruise-system  68m
pod/kruise-daemon-qjckj                                                   kruise-system  68m
pod/kruise-daemon-qkkfh                                                   kruise-system  68m
pod/kruise-daemon-rhf2h                                                   kruise-system  68m
secret/kruise-webhook-certs                                               kruise-system  68m
serviceaccount/default                                                    kruise-system  68m
serviceaccount/kruise-daemon                                              kruise-system  68m
serviceaccount/kruise-manager                                             kruise-system  68m
service/kruise-webhook-service                                            kruise-system  68m
controllerrevision.apps/kruise-daemon-7955868c86                          kruise-system  68m
daemonset.apps/kruise-daemon                                              kruise-system  68m
deployment.apps/kruise-controller-manager                                 kruise-system  68m
replicaset.apps/kruise-controller-manager-7dc584559b                      kruise-system  68m
endpointslice.discovery.k8s.io/kruise-webhook-service-vw58k               kruise-system  68m
rolebinding.rbac.authorization.k8s.io/kruise-leader-election-rolebinding  kruise-system  68m
role.rbac.authorization.k8s.io/kruise-leader-election-role                kruise-system  68m
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$
```

添加的 CRD 信息

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl get crd | grep kruise.io
advancedcronjobs.apps.kruise.io                                 2023-02-28T09:31:30Z
broadcastjobs.apps.kruise.io                                    2023-02-28T09:31:30Z
clonesets.apps.kruise.io                                        2023-02-28T09:31:30Z
containerrecreaterequests.apps.kruise.io                        2023-02-28T09:31:30Z
daemonsets.apps.kruise.io                                       2023-02-28T09:31:30Z
imagepulljobs.apps.kruise.io                                    2023-02-28T09:31:30Z
nodeimages.apps.kruise.io                                       2023-02-28T09:31:30Z
nodepodprobes.apps.kruise.io                                    2023-02-28T09:31:30Z
persistentpodstates.apps.kruise.io                              2023-02-28T09:31:30Z
podprobemarkers.apps.kruise.io                                  2023-02-28T09:31:30Z
podunavailablebudgets.policy.kruise.io                          2023-02-28T09:31:30Z
resourcedistributions.apps.kruise.io                            2023-02-28T09:31:30Z
sidecarsets.apps.kruise.io                                      2023-02-28T09:31:30Z
statefulsets.apps.kruise.io                                     2023-02-28T09:31:30Z
uniteddeployments.apps.kruise.io                                2023-02-28T09:31:30Z
workloadspreads.apps.kruise.io                                  2023-02-28T09:31:30Z
```

###　系统架构

![在这里插入图片描述](https://img-blog.csdnimg.cn/f289b3e177394a7ab4a3009232a41fc9.png)

通过上面的架构图我们可以看到，kruise 主要涉及两部分，DS `kruise-daemon` 和 deploy `kruise-controller` 控制器管理器 `kruise-controller-manager`

#### manager

Kruise-manager 是一个运行 controller 和 webhook 中心组件，它通过 Deployment 部署在 kruise-system 命名空间中。

逻辑上来说，如 `cloneset-controller/sidecarset-controller` 这些的 `controller` 都是独立运行的。不过为了减少复杂度，它们都被打包在一个独立的二进制文件、并运行在 `kruise-controller-manager-xxx` 这个 Pod 中。

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl get deployments.apps -n kruise-system
NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
kruise-controller-manager   2/2     2            2           6h42m
```

除了 controller 之外，k`ruise-controller-manager-xxx` 中还包含了针对 `Kruise CRD` 以及 Pod 资源的 `admission webhook`。Kruise-manager 会创建一些 webhook configurations 来配置哪些资源需要感知处理、以及提供一个 Service 来给 kube-apiserver 调用。

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl get svc -n kruise-system
NAME                     TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
kruise-webhook-service   ClusterIP   10.109.117.201   <none>        443/TCP   6h41m
```

#### daemon

这是从 Kruise v0.8.0 版本开始提供的一个新的 daemon 组件。

它通过 DaemonSet 部署到每个 Node 节点上，提供镜像预热、容器重启等功能。

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl get pod -n kruise-system -l control-plane=daemon
NAME                  READY   STATUS    RESTARTS   AGE
kruise-daemon-2hpdc   1/1     Running   0          6h42m
kruise-daemon-69rtg   1/1     Running   0          6h42m
kruise-daemon-89lbr   1/1     Running   0          6h42m
kruise-daemon-fzwnh   1/1     Running   0          6h42m
kruise-daemon-hxmdv   1/1     Running   0          6h42m
kruise-daemon-qjckj   1/1     Running   0          6h42m
kruise-daemon-qkkfh   1/1     Running   0          6h42m
kruise-daemon-rhf2h   1/1     Running   0          6h42m
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$
```

### 原地升级

所谓原地升级，可以简单理解为，pod 在升级的时候，类似通过 `edit` 或者 `patch` 等子命令行为对 Yaml 资源文件进行更改更新后应用，而不是销毁重建。

重建升级时我们要删除旧 Pod、创建新 Pod：

- Pod 名字和 uid 发生变化，因为它们是完全不同的两个 Pod 对象(比如 Deployment 升级)
- Pod 名字可能不变、但 uid 变化，因为它们是不同的 Pod 对象，只是复用了同一个名字(比如 StatefulSet 升级)
- Pod 所在 Node 名字发生变化，因为新 Pod 很大可能性是不会调度到之前所在的 Node 节点的
- Pod IP 发生变化，因为新 Pod 很大可能性是不会被分配到之前的 IP 地址的

但是对于原地升级，我们仍然复用同一个 Pod 对象，只是修改它里面的字段。因此：

- 可以避免如 调度、分配 IP、分配、挂载盘 等额外的操作和代价
- 更快的镜像拉取，因为开源复用已有旧镜像的大部分 layer 层，只需要拉取新镜像变化的一些 layer
- 当一个容器在原地升级时，Pod 中的其他容器不会受到影响，仍然维持运行

这种 Kruise workload 的升级类型名为 InPlaceIfPossible，它意味着 `Kruise 会尽量对 Pod 采取原地升级，如果不能则退化到重建升级。`

具体那些行为会原地升级，那些行为会销毁重建， 小伙伴可以看下官网文档，随着版本在变动。

### 内置控制器认识

Kruise 提供了很多自定义控制器来增强 工作负载的能力，这里我们简单来看一个 `CloneSet`

#### CloneSet

CloneSet 控制器提供了高效管理无状态应用的能力，它对标原生的 `Deployment`，但 `CloneSet` 提供了很多增强功能。按照 `Kruise` 的命名规范，`CloneSet` 是一个直接管理 Pod 的 Set 类型 workload,

官网 Demo

```yaml
apiVersion: apps.kruise.io/v1alpha1
kind: CloneSet
metadata:
  labels:
    app: sample
  name: sample
spec:
  replicas: 3
  selector:
    matchLabels:
      app: sample
  template:
    metadata:
      labels:
        app: sample
    spec:
      containers:
        - name: nginx
          image: nginx:alpine
```

应用资源文件

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl apply  -f cloneset.yaml
cloneset.apps.kruise.io/sample created
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl get cloneset
NAME     DESIRED   UPDATED   UPDATED_READY   READY   TOTAL   AGE
sample   3         3         3               3       3       70s
```

通过 `kubectl-tree` 插件我们可以简单的看一下 CloneSet 和 Deployment / statefulsets / replicaset 的对比

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl-tree cloneset sample
W0301 11:45:33.198222   30678 warnings.go:70] metallb.io v1beta1 AddressPool is deprecated, consider using IPAddressPool
NAMESPACE  NAME                                     READY  REASON  AGE
default    CloneSet/sample                          -              114m
default    ├─ControllerRevision/sample-d4d4fb5bd  -              114m
default    ├─Pod/sample-5w4p4                     True           114m
default    ├─Pod/sample-dqxrt                     True           114m
default    └─Pod/sample-tf92b                     True           114m
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$
```

Deployment

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl-tree deploy release-name-grafana
W0301 11:45:16.867140   30063 warnings.go:70] metallb.io v1beta1 AddressPool is deprecated, consider using IPAddressPool
NAMESPACE  NAME                                             READY  REASON  AGE
default    Deployment/release-name-grafana                  -              17d
default    └─ReplicaSet/release-name-grafana-76f4b7b77d   -              17d
default      └─Pod/release-name-grafana-76f4b7b77d-djc6x  True           17d
```

statefulsets

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl-tree statefulsets.v1.apps prometheus-release-name-kube-promethe-prometheus
W0301 11:48:10.276274   39529 warnings.go:70] metallb.io v1beta1 AddressPool is deprecated, consider using IPAddressPool
NAMESPACE  NAME                                                                                READY  REASON  AGE
default    StatefulSet/prometheus-release-name-kube-promethe-prometheus                        -              17d
default    ├─ControllerRevision/prometheus-release-name-kube-promethe-prometheus-7845648c85  -              17d
default    └─Pod/prometheus-release-name-kube-promethe-prometheus-0                          True           17d

```

replicaset

```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$kubectl-tree rs release-name-kube-state-metrics-6859bcd6d7
W0301 11:51:33.429710   50553 warnings.go:70] metallb.io v1beta1 AddressPool is deprecated, consider using IPAddressPool
NAMESPACE  NAME                                                      READY  REASON  AGE
default    ReplicaSet/release-name-kube-state-metrics-6859bcd6d7     -              17d
default    └─Pod/release-name-kube-state-metrics-6859bcd6d7-876md  True           10d
┌──[root@vms100.liruilongs.github.io]-[~/ansible/openkruise]
└─$
```

CloneSet 允许用户配置 PVC 模板 volumeClaimTemplates，用来给每个 Pod 生成独享的 PVC，这是 Deployment 所不支持的。 如果用户没有指定这个模板，CloneSet 会创建不带 PVC 的 Pod。

CloneSet 允许用户在缩小 replicas 数量的同时，指定想要删除的 Pod 名字

CloneSet 提供了 3 种升级方式，默认为 ReCreate：

- ReCreate: 控制器会删除旧 Pod 和它的 PVC，然后用新版本重新创建出来。
- InPlaceIfPossible: 控制器会优先尝试原地升级 Pod，如果不行再采用重建升级。具体参考下方阅读文档。
- InPlaceOnly: 控制器只允许采用原地升级。因此，用户只能修改上一条中的限制字段，如果尝试修改其他字段会被 Kruise 拒绝。

## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知,这是一个开源项目，如果你认可它，不要吝啬星星哦 :)

---

https://openkruise.io/zh/docs/

https://github.com/openkruise/kruise

---

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
