---
title: Kubernetes:通过轻量化工具 kubespy 实时观察YAML资源变更
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: kubespy
uniqueId: "2022-12-27 19:49:55/关于Kubernetes 中通过轻量化工具kubespy实时观察资源的一些笔记.html"
mathJax: false
date: 2022-12-28 03:49:55
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->

## 写在前面

---

- 分享一个小工具 `kubespy` 给小伙伴
- 博文内容涉及：
  + 工具的简单介绍
  + 下载安装
  + 以 `kubectl` 插件方式使用 Demo
- 理解不足小伙伴帮忙指正

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

---

### 简单介绍

偶然的机会看个这样一个小工具，分享给小伙伴，`kubespy` 可以实时观察 `Kubernetes` 资源如何变化，运行 kubespy，它会持续观察和报告有关 `Kubernetes` 资源的信息。简单来讲，是一个可以观察 资源对象 YAML 文件变化，和一个可以监控 `deploy，svc ` 中 pod 调度变化的工具。它会把监控信息实时打印在当前终端标准输出的。

`kubespy` 可以单独使用。也可以整合为 `kubectl` 插件来使用。

kubespy 有 三个命令：

- status : 它实时发出对任意 Kubernetes `.status` 资源字段所做的所有更改.
- changes : 它实时发出对 Kubernetes 资源中`任何字段` 的所有更改
- trace : 它 `跟踪` 一个 Kubernetes 资源在 `整个集群中所做的更改`，并将它们 `聚合成一个实时更新的高级摘要`。

### 下载安装

具体可以参考文档：https://github.com/pulumi/kubespy

二进制版本： https://github.com/pulumi/kubespy/releases

这里网络问题，我们直接本地下载

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$wget https://github.com/pulumi/kubespy/releases/download/v0.6.1/kubespy-v0.6.1-linux-amd64.tar.gz
```

解压

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$tar -zxvf kubespy-v0.6.1-linux-amd64.tar.gz
LICENSE
README.md
kubespy
```

配置作为 `kubectl` 插件使用，需要修改原来的插件名字，拷贝到 可执行文件目录处，然后就可以通过 `kubectl` 命名来调用，查看版本测试

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$mv kubespy kubectl-spy
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$mv kubectl-spy /usr/local/bin/
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl spy  version
v0.6.1
```

### 命令 Demo

`status <apiVersion> <kind> [<namespace>/]<name>`，它实时发出对 `.status` 任意 Kubernetes 资源字段所做的所有更改，作为 `JSON diff`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/6a05202eec9745ca941c86bb8e2e597b.png)

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl delete -n kube-system pods   kube-apiserver-vms81.liruilongs.github.io
pod "kube-apiserver-vms81.liruilongs.github.io" deleted
```

这里我们删除一个静态 pod 来观察下 对 `.status` 对象的状态监听

可以看下，整个过程 pod 状态为

```json
-  "phase": "Running",
+  "phase": "Pending",
```

到

```json
-  "phase": "Pending",
+  "phase": "Running",
```

先是创建状态，然后我删除 静态 pod 之后，对应的 `.status` 对象属性发现变动，属性全部消失，这个时候说明 静态 pod 已经被干掉了，然后受 k8s 机制影响，会周期扫描 静态 pod 目录，发现 yaml 文件还在，又会重新拉起 静态 pod， 在所以对应的 `.status` 字段在一次发生变动，属性添加。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl spy status v1 Pod kube-system/kube-apiserver-vms81.liruilongs.github.io
Watching status of v1 Pod kube-system/kube-apiserver-vms81.liruilongs.github.io
CREATED
{
  .......
  "containerStatuses": [
    {
      "containerID": "docker://3db7e6619f6b2bf4a764bb681394c1bd76369d04cb604c8a26574ba9a860bcf1",
      "image": "registry.aliyuncs.com/google_containers/kube-apiserver:v1.22.2",
      "imageID": "docker-pullable://registry.aliyuncs.com/google_containers/kube-apiserver@sha256:eb4fae890583e8d4449c1e18b097aec5574c25c8f0323369a2df871ffa146f41",
      ...........
      },
      "name": "kube-apiserver",
      "ready": true,
      "restartCount": 2,
      "started": true,
      "state": {
        "running": {
          "startedAt": "2022-12-24T13:56:43Z"
        }
      }
    }
  ],
........
  "startTime": "2022-11-26T05:40:23Z"
}
MODIFIED
MODIFIED
DELETED
ADDED
 {
...............
-  ],
-  "hostIP": "192.168.26.81",
-  "phase": "Running",
+  "phase": "Pending",
-  "podIP": "192.168.26.81",
-  "podIPs": [
-    {
-      "ip": "192.168.26.81"
-    }
-  ],
   "qosClass": "Burstable",
-  "startTime": "2022-11-26T05:40:23Z"
 }

MODIFIED
 {
-  "phase": "Pending",
+  "phase": "Running",
   "qosClass": "Burstable"
+  "conditions": [
.........
+  "startTime": "2022-11-26T05:40:23Z"
 }
```

`changes <apiVersion> <kind> [<namespace>/]<name>`，它实时发出对 Kubernetes 资源中 `任何字段` 的所有更改，作为 `JSON diff`。 即他可以监控 yaml 资源的所有字段。

这里我们摘了一段出来， `-` 代表字段删除， `+` 代表字段添加。没有代表，为创建状态

```bash
.............
-        "lastTransitionTime": "2022-12-27T20:45:18Z",
+        "lastTransitionTime": "2022-12-27T21:51:59Z",
-        "status": "True",
+        "status": "False",
         "type": "ContainersReady"
+        "message": "containers with unready status: [pause]"
+        "reason": "ContainersNotReady"
       },
       {
         "lastProbeTime": null,
         "lastTransitionTime": "2022-12-27T20:45:17Z",
         "status": "True",
         "type": "PodScheduled"
       }
     ],
     "containerStatuses": [
       {
         "containerID": "docker://23c398096e1e625977d8dab9c267ac49aff457d68930aa6cdacb26644919f763",
         "image": "registry.aliyuncs.com/google_containers/pause:3.5",
         "imageID": "docker-pullable://registry.aliyuncs.com/google_containers/pause@sha256:1ff6c18fbef2045af6b9c16bf034cc421a29027b800e4f9b68ae9b1cb3e9ae07",
         "lastState": {
         },
         "name": "pause",
-        "ready": true,
+        "ready": false,
         "restartCount": 0,
-        "started": true,
+        "started": false,
         "state": {
-          "running": {
-            "startedAt": "2022-12-27T20:45:18Z"
-          }
+          "terminated": {
+            "containerID": "docker://23c398096e1e625977d8dab9c267ac49aff457d68930aa6cdacb26644919f763",
+            "exitCode": 0,
+            "finishedAt": "2022-12-27T21:51:58Z",
+            "reason": "Completed",
+            "startedAt": "2022-12-27T20:45:18Z"
+          }
...................
```

`trace <kind> [<namespace>/]<name>`，它“跟踪”一个复杂的 Kubernetes 资源在整个集群中所做的更改，并将它们聚合成一个实时更新的高级摘要。简单讲，即会实时的把当前资源的 pod 的信息展示出来。可以清楚的看到 pod 调度。

下面为 当前的 `deploy` 的 状态，他会显示当前的 资源是否可用，对应的 pod 调度情况

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl spy trace  deployment liruilong
[ADDED apps/v1/Deployment]  liruilong-topo-namespace/liruilong
    Rolling out Deployment revision 1
    ✅ Deployment is currently available
    ✅ Rollout successful: new ReplicaSet marked 'available'

ROLLOUT STATUS:
- [Current rollout | Revision 1] [ADDED]  liruilong-topo-namespace/liruilong-744498fcbd
    ✅ ReplicaSet is available [2 Pods available of a 2 minimum]
       - [Ready] liruilong-744498fcbd-2nbq2
       - [Ready] liruilong-744498fcbd-ffk4n
```

当修改副本数后

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/podtopolog]
└─$kubectl scale deployment liruilong  --replicas=3
deployment.apps/liruilong scaled
```

可以发现 `pod` 的实时变化。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kubectl-plu]
└─$kubectl spy trace deployment liruilong
[MODIFIED apps/v1/Deployment]  liruilong-topo-namespace/liruilong
    Rolling out Deployment revision 1
    ✅ Deployment is currently available
    ✅ Rollout successful: new ReplicaSet marked 'available'

ROLLOUT STATUS:
- [Current rollout | Revision 1] [MODIFIED]  liruilong-topo-namespace/liruilong-744498fcbd
    ✅ ReplicaSet is available [3 Pods available of a 3 minimum]
       - [Ready] liruilong-744498fcbd-k6b5c
       - [Ready] liruilong-744498fcbd-kf9gq
       - [Ready] liruilong-744498fcbd-2nbq2
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/4896b87f841a4cc28f064040b87fc41b.png)

## 博文参考

---

https://github.com/pulumi/kubespy

https://kubernetes.io/docs/tasks/extend-kubectl/kubectl-plugins/