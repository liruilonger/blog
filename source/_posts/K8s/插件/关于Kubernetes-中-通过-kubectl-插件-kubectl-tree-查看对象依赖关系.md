---
title: K8s:通过 kubectl 插件 kubectl-tree 查看API对象层级关系
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: kubectl-tree
uniqueId: "2023-01-09 03:15:24/关于Kubernetes 中 通过 kubectl 插件 kubectl-tree 查看对象依赖关系.html"
mathJax: false
date: 2023-01-09 11:15:24
thumbnail:
---

**<font color="009688"> 岂其食鱼，必河之鲤？岂其取妻，必宋之子？——《陈风·衡门》**</font>

<!-- more -->

## 写在前面

---

- 分享一个小工具 `kubectl-tree`，用于查看 k8s API 对象层级关系
- 比如对于有状态应用来讲，可以看到`Deployment --> ReplicaSet --> Pod` 的构成关系
- 博文内容涉及：`tree` 插件的安装以及使用。
- 理解不足小伙伴帮忙指正

**<font color="009688"> 岂其食鱼，必河之鲤？岂其取妻，必宋之子？——《陈风·衡门》**</font>


---

有时候我们希望可以看到，k8s 中 有状态应用、无状态应用和 pod 的所有关系，但是往往需要多条命名才可以实现，其实通过 `kubectl-tree` 插件，可以很轻松的做到。

`kubectl-tree` 一个 kubectl 插件，它用于探索 Kubernetes 对象之间的所有权利关系。

![在这里插入图片描述](https://img-blog.csdnimg.cn/edca8dc62d4047f594f2718ce558ddf7.png)

### 下载安装

如果安装了 `krew`插件管理器,可以通过下面的方式，前提是你有科学上网

```bash
kubectl krew install tree
kubectl tree --help
```

如果没有，那可以通过 浏览器下载下面的二进制安装包

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$wget https://github.com/ahmetb/kubectl-tree/releases/download/v0.4.3/kubectl-tree_v0.4.3_linux_amd64.tar.gz
```

然后配置为 `kubectl` 插件

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$tar -zxvf kubectl-tree_v0.4.3_linux_amd64.tar.gz
LICENSE
kubectl-tree
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv kubectl-tree /usr/local/bin
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl tree  --version
kubectl version v0.4.3
```

### 查看依赖关系

![在这里插入图片描述](https://img-blog.csdnimg.cn/d3299a058a764820a25c481dbfd167b8.png)

**无状态应用关系查看**

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl get deployments.apps  -n awx
NAME                              READY   UP-TO-DATE   AVAILABLE   AGE
awx-demo                          1/1     1            1           85d
awx-operator-controller-manager   1/1     1            1           85d
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl tree deployments  awx-demo -n awx
NAMESPACE  NAME                                 READY  REASON  AGE
awx        Deployment/awx-demo                  -              85d
awx        └─ReplicaSet/awx-demo-65d9bf775b   -              85d
awx          └─Pod/awx-demo-65d9bf775b-hc58x  True           85d
```

可以看到，无状态应用由 `Deployment、 ReplicaSet，Pod` 三个资源对象构成

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl tree deployments  awx-operator-controller-manager  -n awx
NAMESPACE  NAME                                                        READY  REASON  AGE
awx        Deployment/awx-operator-controller-manager                  -              85d
awx        └─ReplicaSet/awx-operator-controller-manager-79ff9599d8   -              85d
awx          └─Pod/awx-operator-controller-manager-79ff9599d8-m7t8k  True           85d
```

多副本的情况

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl tree deployments liruilong
NAMESPACE                 NAME                                  READY  REASON  AGE
liruilong-topo-namespace  Deployment/liruilong                  -              12d
liruilong-topo-namespace  └─ReplicaSet/liruilong-744498fcbd   -              12d
liruilong-topo-namespace    ├─Pod/liruilong-744498fcbd-48t6z  False          7d10h
liruilong-topo-namespace    └─Pod/liruilong-744498fcbd-4bmf4  True           6d1h
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$
```

**Service 关系查看**

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl get services -n awx
NAME                                              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
awx-demo-postgres-13                              ClusterIP   None             <none>        5432/TCP       85d
awx-demo-service                                  NodePort    10.104.176.210   <none>        80:30066/TCP   85d
awx-operator-controller-manager-metrics-service   ClusterIP   10.108.71.67     <none>        8443/TCP       85d
```

Service 由 `Service、EndpointSlice` 构成，这里如果是 1.21 之前的版本，看到的应该是 `Endpoints`,而不是 `EndpointSlice`

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl tree svc awx-demo-service  -n awx
NAMESPACE  NAME                                      READY  REASON  AGE
awx        Service/awx-demo-service                  -              85d
awx        └─EndpointSlice/awx-demo-service-6gs4d  -              85d
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl tree svc awx-demo-postgres-13 -n awx
NAMESPACE  NAME                                          READY  REASON  AGE
awx        Service/awx-demo-postgres-13                  -              85d
awx        └─EndpointSlice/awx-demo-postgres-13-4tc87  -              85d
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl tree svc awx-operator-controller-manager-metrics-service -n awx
NAMESPACE  NAME                                                                     READY  REASON  AGE
awx        Service/awx-operator-controller-manager-metrics-service                  -              85d
awx        └─EndpointSlice/awx-operator-controller-manager-metrics-service-7wtml  -              85d

```

**有状态应用关系查看**

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl get -n awx statefulsets.apps
NAME                   READY   AGE
awx-demo-postgres-13   1/1     85d
web                    0/3     30d
```

有状态应用由 `statefulsets、ControllerRevision、Pod` 构成

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl tree statefulsets awx-demo-postgres-13  -n awx
NAMESPACE  NAME                                                    READY  REASON  AGE
awx        StatefulSet/awx-demo-postgres-13                        -              85d
awx        ├─ControllerRevision/awx-demo-postgres-13-85958bcbcd  -              85d
awx        └─Pod/awx-demo-postgres-13-0                          True           85d
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$

```

## 博文参考

---

https://github.com/ahmetb/kubectl-tree

