---
title: K8s:通过 kubectl 插件 Kubepug 实现升级检查(废弃API资源检查)
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubepug
uniqueId: "2023-01-10 21:04:43/关于Kubernetes 中 通过 kubectl 插件 Kubepug 实现升级检查.html"
mathJax: false
date: 2023-01-10 23:04:43
thumbnail:
---

**<font color="009688"> 昔我往矣，杨柳依依。今我来思，雨雪霏霏。 ——《小雅·采薇》**</font>


<!-- more -->

## 写在前面

---

- 分享一个小工具，可用于 版本升级的 废弃 API 对象检查
- 博文内容涉及：
  - `kubepug` 离线安装，配置 kubectl 插件
  - `kubepug` 两种方式使用 Demo
- 理解不足小伙伴帮忙指正

**<font color="009688"> 昔我往矣，杨柳依依。今我来思，雨雪霏霏。 ——《小雅·采薇》**</font>

---

k8s 的版本迭代很快，虽然主要版本一直没有变化，但是次要版本一直在迭代，2022年一年就发布了三个次要版本，同时不同的次要版本之间 API 资源一直在变化，有新加入的，也有废弃删除的。不同版本的 api 资源版本也有不同，往往不是向下兼容的，比如在低版本中 API 资源版本为 `v1beta1`，而高版本可能升级为 `v1`。但是在高版本中不能运行低版本的API资源。

所以在 k8s 版本升级的时候，需要对之前废弃的和删除的 API 资源 做出清理，需要升级的做升级，或者替换为其他的 API 资源。 在这之前，需要一个工具来检查 API 资源对象，那些事已经废弃的，那些将要废弃。

`Kubepug` 即是这样一个工具，一个升级前检查器，可帮助在迁移到新的主要版本之前在 Kubernetes 资源中找到已弃用和已删除的 API

![在这里插入图片描述](https://img-blog.csdnimg.cn/2b8f0a744e1c400d980a0961d6577dae.png)

`KubePug/Deprecations` 作为一个 kubectl 插件，他可以实现下面的功能：

- 从特定的 `Kubernetes` 版本下载 `swagger.json`
- 解析此 Json 发现弃用通知
- 验证当前的 kubernetes 集群或输入文件，检查此已弃用的 API 版本中是否存在对象，允许用户在迁移前进行检查

### Kubepug 安装

如果可以科学上网，并且安装了 `krew`,可以使用下面的方式。

```bash
kubectl krew install deprecations
```

如果是内网环境，可以浏览器下载二进制文件

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$wget  https://github.com/rikatz/kubepug/releases/download/v1.4.0/kubepug_linux_amd64.tar.gz
```

然后配置为 kubectl 插件

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$tar -zxvf kubepug_linux_amd64.tar.gz
LICENSE
README.md
kubepug
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv kubepug kubectl-kubepug
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv ./kubectl-kubepug  /usr/local/bin/
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl kubepug
```

查看版本测试

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl kubepug version
 __  ___  __    __  .______    _______   ______ .___________. __               __  ___  __    __  .______    _______ .______    __    __    _______
|  |/  / |  |  |  | |   _  \  |   ____| /      ||           ||  |             |  |/  / |  |  |  | |   _  \  |   ____||   _  \  |  |  |  |  /  _____|
|  '  /  |  |  |  | |  |_)  | |  |__   |  ,----'`---|  |----`|  |      ______ |  '  /  |  |  |  | |  |_)  | |  |__   |  |_)  | |  |  |  | |  |  __
|    <   |  |  |  | |   _  <  |   __|  |  |         |  |     |  |     |______||    <   |  |  |  | |   _  <  |   __|  |   ___/  |  |  |  | |  | |_ |
|  .  \  |  `--'  | |  |_)  | |  |____ |  `----.    |  |     |  `----.        |  .  \  |  `--'  | |  |_)  | |  |____ |  |      |  `--'  | |  |__| |
|__|\__\  \______/  |______/  |_______| \______|    |__|     |_______|        |__|\__\  \______/  |______/  |_______|| _|       \______/   \______|
kubectl-kubepug: Shows all the deprecated objects in a Kubernetes cluster allowing the operator to verify them before upgrading the cluster.
It uses the swagger.json version available in master branch of Kubernetes repository (github.com/kubernetes/kubernetes) as a reference.

GitVersion:    v1.4.0
GitCommit:     4de32d695b27c52c16d4a801b613b78e45e28ca9
GitTreeState:  clean
BuildDate:     2022-08-21T18:25:40
GoVersion:     go1.18.5
Compiler:      gc
Platform:      linux/amd64
```

### 获取集群当前 API 状态

可以使用以下命令检查正在运行的集群的状态

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl kubepug --k8s-version=v1.22.2
Error: Get "https://raw.githubusercontent.com/kubernetes/kubernetes/v1.22.2/api/openapi-spec/swagger.json": dial tcp 0.0.0.0:443: connect: connection refused
time="2023-01-08T23:23:59+08:00" level=error msg="An error has occurred: Get \"https://raw.githubusercontent.com/kubernetes/kubernetes/v1.22.2/api/openapi-spec/swagger.json\": dial tcp 0.0.0.0:443: connect: connection refused"
```

如果没有科学上网，会报上面的错误，可以把 对应的 swagger 文件下载下来上传。然后在检查的时候指定 swagger 文件目录

```bash
PS C:\Users\山河已无恙\Downloads> curl -o swagger-v1.22.2.json https://raw.githubusercontent.com/kubernetes/kubernetes/v1.22.2/api/openapi-spec/swagger.json
PS C:\Users\山河已无恙\Downloads> scp .\swagger-v1.22.2.json  root@192.168.26.81:/root/ansible/krew/
root@192.168.26.81''s password:
swagger-v1.22.2.json                                           100% 4400KB  86.4MB/s   00:00
PS C:\Users\山河已无恙\Downloads>
```

通过 `--swagger-dir=` 指定 对应的 swagger 文件位置，`--k8s-version=v1.22.2` 指定要检测的版本

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mkdir -p  swagger/folder
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv swagger-v1.22.2.json  swagger/folder/
```

通过输出我们可以看到当前集群的一些 api 变化

- `v1` 版本的 `ComponentStatus(ComponentStatusList)`在 `v1.19+` 中已弃用
- `policy/v1beta1a` 版本 `PodSecurityPolicy`在 `v1.21+` 版本已弃用，`v1.25+` 版本不可用

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl kubepug --k8s-version=v1.22.2 --swagger-dir=./swagger/folder
W0108 23:32:02.991528   30270 warnings.go:70] v1 ComponentStatus is deprecated in v1.19+
W0108 23:32:02.995308   30270 warnings.go:70] policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
RESULTS:
Deprecated APIs:

ComponentStatus found in /v1
         ├─ ComponentStatus (and ComponentStatusList) holds the cluster validation info. Deprecated: This API is deprecated in v1.19+
                -> GLOBAL: scheduler
                -> GLOBAL: controller-manager
                -> GLOBAL: etcd-0

PodSecurityPolicy found in policy/v1beta1
         ├─ PodSecurityPolicy governs the ability to make requests that affect the Security Context that will be applied to a pod and container. Deprecated in 1.21.
                -> GLOBAL: controller
                -> GLOBAL: speaker


Deleted APIs:

┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$
```

通过 `kubectl` 也可以看到当前集群存在的对应 API 资源。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl get psp
Warning: policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
NAME         PRIV    CAPS      SELINUX    RUNASUSER   FSGROUP     SUPGROUP    READONLYROOTFS   VOLUMES
controller   false             RunAsAny   MustRunAs   MustRunAs   MustRunAs   true             configMap,secret,emptyDir
speaker      true    NET_RAW   RunAsAny   RunAsAny    RunAsAny    RunAsAny    true             configMap,secret,emptyDir
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl get componentstatuses
Warning: v1 ComponentStatus is deprecated in v1.19+
NAME                 STATUS      MESSAGE                                                                                       ERROR
scheduler            Unhealthy   Get "http://127.0.0.1:10251/healthz": dial tcp 127.0.0.1:10251: connect: connection refused
controller-manager   Healthy     ok
etcd-0               Healthy     {"health":"true","reason":""}
```

假如我们希望升级到 `v1.25.2` ，可以下载 1.25.2 版本的 `swagger` json 文件

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl kubepug --k8s-version=v1.25.2 --swagger-dir=./swagger/folder
W0108 23:43:37.187999   41352 warnings.go:70] v1 ComponentStatus is deprecated in v1.19+
W0108 23:43:37.339503   41352 warnings.go:70] policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
RESULTS:
Deprecated APIs:

ComponentStatus found in /v1
         ├─ ComponentStatus (and ComponentStatusList) holds the cluster validation info. Deprecated: This API is deprecated in v1.19+
                -> GLOBAL: scheduler
                -> GLOBAL: controller-manager
                -> GLOBAL: etcd-0


Deleted APIs:

PodSecurityPolicy found in policy/v1beta1
         ├─ API REMOVED FROM THE CURRENT VERSION AND SHOULD BE MIGRATED IMMEDIATELY!!
                -> GLOBAL: controller
                -> GLOBAL: speaker
```

通过检查可以看到 ，1.25 之后的废弃和已经删除的 API 资源。如果要升级到 1.25 需要把删除的 API 处理掉。

![在这里插入图片描述](https://img-blog.csdnimg.cn/ff8fe7f895ec473a96d35d31d9a470e4.png)

### 查看指定文件资源 API 状态

Kubepug 可以放入 CI / 检查输入文件：

- 来自 master 分支的 swagger.json 将被使用
- 将验证所有 YAML 文件(不包括子目录)
- 如果发现已弃用或已删除的对象，程序将退出并出错。

这里指定为当前的版本。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl kubepug --input-file=/root/ansible/ --error-on-deleted --error-on-deprecated  --k8s-version=v1.22.2 --swagger-dir=./swagger/folder
........
RESULTS:
Deprecated APIs:


Deleted APIs:

calicoApiConfig found in /v1
         ├─ API REMOVED FROM THE CURRENT VERSION AND SHOULD BE MIGRATED IMMEDIATELY!!
                -> OBJECT:  namespace: default location: /root/ansible//calicoctl.j2

Error: found 1 Deleted APIs and 0 Deprecated APIs
time="2023-01-08T23:50:39+08:00" level=error msg="An error has occurred: found 1 Deleted APIs and 0 Deprecated APIs"
```

直接指出删除的 API 资源和对应的文件。上面两个参数的意思：

- `--error-on-deleted` 如果发现一个被删除的对象，程序将以返回代码 1 而不是 0 退出，默认为 false
- `--error-on-deprecated` 如果发现一个被废弃的对象，程序将以返回代码 1 而不是 0 退出。

当然也可以检查要升级的版本。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl kubepug --input-file=/root/ansible/ --error-on-deleted --error-on-deprecated  --k8s-version=v1.25.2 --swagger-dir=./swagger/folder
........
RESULTS:
Deprecated APIs:


Deleted APIs:

PodDisruptionBudget found in policy/v1beta1
         ├─ API REMOVED FROM THE CURRENT VERSION AND SHOULD BE MIGRATED IMMEDIATELY!!
                -> OBJECT: calico-kube-controllers namespace: kube-system location: /root/ansible//calico.yaml

calicoApiConfig found in /v1
         ├─ API REMOVED FROM THE CURRENT VERSION AND SHOULD BE MIGRATED IMMEDIATELY!!
                -> OBJECT:  namespace: default location: /root/ansible//calicoctl.j2

Error: found 2 Deleted APIs and 0 Deprecated APIs
time="2023-01-08T23:52:37+08:00" level=error msg="An error has occurred: found 2 Deleted APIs and 0 Deprecated APIs"
```

## 博文参考

---

https://github.com/rikatz/kubepug
