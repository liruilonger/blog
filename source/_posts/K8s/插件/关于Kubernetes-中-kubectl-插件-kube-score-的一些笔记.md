---
title: K8s:使用 kubectl 插件 kube-score 对YAML资源文件进行分析改进
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: kube-score
uniqueId: "2023-01-10 16:43:48/K8s:使用 kubectl 插件 kube-score 对YAML资源文件进行分析改进.html"
mathJax: false
date: 2023-01-11 00:43:48
thumbnail:
---

**<font color="009688"> 靡不有初，鲜克有终。 ——《大雅·荡》**</font>

<!-- more -->

## 写在前面

---

- `kube-score` 用于 k8s 中 YAML 资源文件分析
- 它会输出是一个建议列表，其中列出了可以改进的内容
- 博文内容涉及：
  - 下载安装、使用 kube-score
  - 静态 YAML 文件分析
  - 生成 YAML 文件分析
  - 现有集群导出YAML文件的分析
- 理解不足小伙伴帮忙指正

**<font color="009688"> 靡不有初，鲜克有终。 ——《大雅·荡》**</font>

---

尤其对于初学者来讲 ，当在 集群中定义了一些 API 资源对象，虽然 对象可以正常创建并且使用，但是我们应该如何确认定义的 API 对象是最优的，最合适。 除非你对每个对象有深入的了解，且长期使用 `k8s`，否则这是一件不容易做到的事。

这里和小伙伴们分享的 kubectl 插件 `kube-socre` 即可以替我们完成这样一件事。kube-score 是一个对 Kubernetes 对象定义执行静态代码分析的工具。它会输出 `可以改进的建议列表`，以使应用程序`更安全、更有弹性`。

![在这里插入图片描述](https://img-blog.csdnimg.cn/5492655c4ac24227a3efd55e598bb86d.png)

### 下载安装

如果你可以科学上网，并且安装了 `krew` , 可以通过下面的方式

```bash
kubectl krew install score
```

否则，可以通过下面的链接下载 二进制包，离线安装

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$wget https://github.com/zegl/kube-score/releases/download/v1.16.1/kube-score_1.16.1_linux_amd64.tar.gz
```

解压之后，配置为 `kubectl` 插件。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$tar -zxvf kube-score_1.16.1_linux_amd64.tar.gz
LICENSE
kube-score
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv kube-score kubectl-score
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv kubectl-score /usr/local/bin/
```

做简单测试，查看版本信息

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl score version
kube-score version: 1.16.1, commit: 74e551f58e9009e70012773a68dcd6424cf6379d, built: 2022-12-16T12:54:40Z
```

### 使用 kube-score

### 静态 YAML 文件分析

分析指定目录的 YAML 资源文件，并给出建议

```bash
kube-score score my-app/*.yaml
kube-score score my-app/deployment.yaml my-app/service.yaml
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl score score k8s-daemonset-create/*.yaml
apps/v1/DaemonSet myds1                                                       💥
    [CRITICAL] Container Security Context ReadOnlyRootFilesystem
        · nginx -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
    [CRITICAL] Container Resources
        · nginx -> CPU limit is not set
            Resource limits are recommended to avoid resource DDOS. Set resources.limits.cpu
        · nginx -> Memory limit is not set
            Resource limits are recommended to avoid resource DDOS. Set resources.limits.memory
        · nginx -> CPU request is not set
            Resource requests are recommended to make sure that the application can start and run
            without crashing. Set resources.requests.cpu
        · nginx -> Memory request is not set
            Resource requests are recommended to make sure that the application can start and run
            without crashing. Set resources.requests.memory
    [CRITICAL] Container Security Context User Group ID
        · nginx -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
..........
```

这里为了了直观一点，我们翻译一下看看。

```
  [关键] 容器安全上下文ReadOnlyRootFilesystem
        - nginx -> 容器没有配置安全上下文
            设置securityContext以在一个更安全的环境中运行容器。
    [危急] 容器资源
        - nginx -> 没有设置CPU限制
            推荐使用资源限制来避免资源DDOS。设置resources.limit.cpu
        - nginx -> 没有设置内存限制
            建议进行资源限制，以避免资源DDOS。设置resources.limit.memory
        - nginx -> 未设置CPU请求
            推荐使用资源请求，以确保应用程序能够启动和运行而不崩溃。
            而不崩溃。设置resources.request.cpu
        - nginx -> 未设置内存请求
            推荐使用资源请求，以确保应用程序能够启动和运行而不崩溃。
            而不崩溃。设置resources.request.memory
..............
```

### 生成 YAML 文件分析

kube-score 也可以在您的 `CI/CD` 环境中运行，如果发现严重错误，将以退出代码 1 退出。可以使用 `--exit-one-on-warning` 参数将触发级别更改为警告。

`kube-score` 可以用于分析不同方式生成的 YAML 资源文件

可以是 Helm 对应的 charts 包 导出的 YAML 文件

```bash
helm template my-app | kube-score score -
```

也可以是 Kustomize 生成的合并的 YANL 资源文件

```bash
kustomize build . | kube-score score -
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$kubectl kustomize  ./ | kubectl score score -
apps/v1/Deployment web                                                        💥
    [CRITICAL] Container Resources
        · nginx-web -> CPU limit is not set
            Resource limits are recommended to avoid resource DDOS. Set
            resources.limits.cpu
        · nginx-web -> Memory limit is not set
            Resource limits are recommended to avoid resource DDOS. Set
            resources.limits.memory
        · nginx-web -> CPU request is not set
            Resource requests are recommended to make sure that the application
            can start and run without crashing. Set resources.requests.cpu
        · nginx-web -> Memory request is not set
            Resource requests are recommended to make sure that the application
            can start and run without crashing. Set resources.requests.memory
    [CRITICAL] Pod NetworkPolicy
        · The pod does not have a matching NetworkPolicy
            Create a NetworkPolicy that targets this pod to control who/what
            can communicate with this pod. Note, this feature needs to be
            supported by the CNI implementation used in the Kubernetes cluster
            to have an effect.
    [CRITICAL] Container Ephemeral Storage Request and Limit
        · nginx-web -> Ephemeral Storage limit is not set
            Resource limits are recommended to avoid resource DDOS. Set
            resources.limits.ephemeral-storage
    [CRITICAL] Container Security Context User Group ID
        · nginx-web -> Container has no configured security context
            Set securityContext to run the container in a more secure context.
。。。。。。。。
```

### 现有集群的分析

可以通过下面的命令对 现有的集群的 API 对应 的 YAML 文件进行分析

```bash
┌──[root@vms81.liruilongs.github.io]-[~/awx-operator]
└─$kubectl api-resources --verbs=list --namespaced -o name   | xargs -n1 -I{} bash -c "kubectl get {} --all-namespaces -oyaml && echo ---"   | kubectl score score -
apps/v1/DaemonSet calico-node in kube-system                                  💥
    [CRITICAL] Pod NetworkPolicy
        · The pod does not have a matching NetworkPolicy
            Create a NetworkPolicy that targets this pod to control who/what
            can communicate with this pod. Note, this feature needs to be
            supported by the CNI implementation used in the Kubernetes cluster
            to have an effect.
.............
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/943fe6fcd8434adf92cb6d5430ac1e70.png)

## 博文参考

---

https://github.com/zegl/kube-score
