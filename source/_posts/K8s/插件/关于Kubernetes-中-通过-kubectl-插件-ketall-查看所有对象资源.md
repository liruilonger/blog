---
title: 关于 Kubernetes 中 通过 kubectl 插件 ketall  查看所有对象资源
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: ketall
uniqueId: '2023-01-19 02:12:16/关于Kubernetes 中 通过 kubectl 插件 ketall  查看所有对象资源.html'
mathJax: false
date: 2023-01-19 10:12:16
thumbnail:
---

**<font color="009688"> **<font color="009688"> 出其东门，有女如云。虽则如云，匪我思存。缟衣綦巾，聊乐我员。——《郑风·出其东门》**</font>**</font>

<!-- more -->
## 写在前面

***
+ 分享一个查看集群所有资源的小工具
+ 博文内容涉及：
  + 下载安装
  + 常用命令 Demo 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> **<font color="009688"> 出其东门，有女如云。虽则如云，匪我思存。缟衣綦巾，聊乐我员。——《郑风·出其东门》**</font>**</font>

***

分享一个查看集群所有资源的小工具，正常如果我们要删除集群，需要删除集群的所有 API 资源，需要获取当前命名空间所有的资源，通过命令行的方式可以实现，但是往往不是很简单的命令就可以实现。今天分享的 kubectl 插件可以很方便的获取到当前所有的资源。

如果看一些常见的 API 资源，可以通过下面的命令获取

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$kubectl get all -A
```
如果获取当前集群。命名空间所有的资源，可以通过下面的命令获取

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-helm-create]
└─$kubectl api-resources --verbs=list --namespaced -o name   | xargs -n1 -I{} bash -c "echo @@@  {}   @@@  && kubectl get {} -A  && echo ---"
```

但是上面的命令相关还是很繁琐的，通过 `ketall` 我们可以很方便

### 下载安装

如果集群安装了 `krew` ,并且可以科学上网，可以通过下面的方式

```bash
┌──[root@vms81.liruilongs.github.io]-[/etc/kubernetes]
└─$kubectl krew install get-all
```

如果没有，可以找一台可以科学上网的机器，通过下面的方式获取二进制文件
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$wget https://github.com/corneliusweig/ketall/releases/download/v1.3.8/ketall-amd64-linux.tar.gz
```

解压配置为 kubectl 插件
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$tar -zxvf ketall-amd64-linux.tar.gz
LICENSE
ketall-amd64-linux
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv ketall-amd64-linux kubectl-ketall
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$mv kubectl-ketall  /usr/local/bin/
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl ketall version
v1.3.8
```

使用的话，直接通过 `kubectl ketall` 的方式使用，也可以单独使用。当前命令为查看集群所有资源

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl ketall | head -n 10
W0109 10:40:25.597346   21774 warnings.go:70] v1 ComponentStatus is deprecated in v1.19+
W0109 10:40:26.020194   21774 warnings.go:70] policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
NAME                                                                                                               NAMESPACE                   AGE
componentstatus/scheduler                                                                                                                      <unknown>
componentstatus/controller-manager                                                                                                             <unknown>
componentstatus/etcd-0                                                                                                                         <unknown>
configmap/awx-demo-awx-configmap                                                                                   awx                         85d
configmap/awx-operator                                                                                             awx                         85d
configmap/awx-operator-awx-manager-config                                                                          awx                         85d
configmap/kube-root-ca.crt                                                                                         awx                         85d
configmap/kube-root-ca.crt                                                                                         constraints-cpu-example     61d
configmap/kube-root-ca.crt                                                                                         default                     392d
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$
```

可以根据 资源的年龄进行过滤，下面为过滤出 age 为小于等于 5天的 所有API 资源。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl ketall --since 5d
W0109 10:43:15.496160   24555 warnings.go:70] v1 ComponentStatus is deprecated in v1.19+
W0109 10:43:15.917773   24555 warnings.go:70] policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
NAME                                                                                                               NAMESPACE                 AGE
pod/kube-apiserver-vms81.liruilongs.github.io                                                                      kube-system               2d16h
pod/debug-pod                                                                                                      liruilong-topo-namespace  2d17h
pod/k8s-pod-restart-info-collector-5db898f9dc-6x9z5                                                                liruilong-topo-namespace  4d19h
resourcequota/object-quota-count-demo                                                                              liruilong-topo-namespace  32h
resourcequota/object-quota-count-scop-demo                                                                         liruilong-topo-namespace  31h
resourcequota/object-quota-demo                                                                                    liruilong-topo-namespace  40h
resourcequota/object-quota-sc-demo                                                                                 liruilong-topo-namespace  32h
apiservice.apiregistration.k8s.io/v1.crd.projectcalico.org                                                                                   31h
apiservice.apiregistration.k8s.io/v1.monitoring.coreos.com                                                                                   31h
apiservice.apiregistration.k8s.io/v1alpha1.monitoring.coreos.com                                                                             31h
apiservice.apiregistration.k8s.io/v1beta1.awx.ansible.com                                                                                    31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.0ba44ab08120c00b98611822088ab5e72674daaa3b56500037ee8ce6ce939975                            3d17h
ipamhandle.crd.projectcalico.org/k8s-pod-network.12e50c1eb802af492f25d489d3696165750f0edccf7d13159316b9da5e29874e                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.17c7fa0d87a33a8636c08a1b01a0ab168fa5fd5d517444b216e9ea82c6041904                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.2b7e404e213d4cb34947adc9e9521ddb50472ffdb5dddb3f7197bcde33041e51                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.2b885252580cbc6cab440c2714bc2f32770fc72850557673d8aedfb68f44e8f9                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.4acbfc63be210af515b8d9a170d4efc4ae22f2c5ff3e8b7ee6790e1915eb6de8                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.5e26fe6c8d007fd876ba187da472cd2ee2548b814b3377fce47f368fc66c3e91                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.5f1b024ae940d84a4fb506d2a5a4b19f5c7ca4b922f8b32247eea357861122e4                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.8e6c7abaf22ae3f392420974b5cfd619d03ae033b30da18897d6b1fecf5256b2                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.a2027123276da3072f0e3a656e906ed205467c9724e798d7e0a9feecf48265c9                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.a9a44e3c0213868b00d3faf45e5d1d81aed4b3e84c23b86c480ddb50376f2965                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.ac8cda4c20d46a0cf4096155abdb9defc8e92708361b930c89fe33586b975ef1                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.b38bbc9025a9009df14a9c14acf95757cb346008314a37b85358c4765ddbe07a                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.cbb90edf58e008989dd1f6085f4bb329e20fb85a0667263741589dacb6f54c12                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.ced8d70ca681592a3b2468c6a44b411136097bede53e8fb34c0146608e3ff82c                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.e1dd4fa5dd4f4d722e1775823420d5b3d0d0c23e3fdf9d536e360a15320e8a0f                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.e2a3896a4b8ed54596b6482eb789b83477c51e272a0c7af843fdbd4bedeb8af9                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.e3602c57582cc64d2ad21f86d6f00d54ee6dabcd55d7abc4537353ab8a667452                            31h
ipamhandle.crd.projectcalico.org/k8s-pod-network.e6cec51b90d9228e8e07c67df2db29e073e57c6fba48c5ccd7683c15d2fd9500                            4d19h
endpointslice.discovery.k8s.io/metrics-server-n7c7l                                                                kube-system               31h
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$
```
可以指定命名空间过滤
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl ketall --namespace=default
NAME                                       NAMESPACE  AGE
configmap/kube-root-ca.crt                 default    392d
endpoints/kubernetes                       default    392d
secret/default-token-f2kn6                 default    392d
serviceaccount/default                     default    392d
service/kubernetes                         default    392d
endpointslice.discovery.k8s.io/kubernetes  default    392d
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$
```

多集群可以指定集群过滤

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/krew]
└─$kubectl ketall --only-scope=cluster
W0109 10:45:48.207990   27024 warnings.go:70] v1 ComponentStatus is deprecated in v1.19+
W0109 10:45:48.427431   27024 warnings.go:70] policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
NAME                                                                                                               NAMESPACE  AGE
componentstatus/scheduler                                                                                                     <unknown>
componentstatus/controller-manager                                                                                            <unknown>
componentstatus/etcd-0                                                                                                        <unknown>
namespace/awx                                                                                                                 85d
namespace/constraints-cpu-example                                                                                             61d
namespace/default                                                                                                             392d
namespace/ingress-nginx                                                                                                       382d
namespace/kube-node-lease                                                                                                     392d
namespace/kube-public                                                                                                         392d
namespace/kube-system                                                                                                         392d
.................
```

## 博文参考

***

https://github.com/corneliusweig/ketall