---
title: 【翻译】如何编写Kubernetes的YAML文件
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: K8s
uniqueId: '2022-04-01 12:24:26/【翻译】如何编写Kubernetes的YAML文件.html'
mathJax: false
date: 2022-04-01 20:24:26
thumbnail:
---
**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>
<!-- more -->
## 写在前面
***
+ `CNCF`官网的文章，感兴趣小伙伴可以看看原文
+ 不足之处请小伙伴批评指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>


***

原文连接： http://webtrans.yodao.com/server/webtrans/tranUrl?url=https%3A%2F%2Fwww.armosec.io%2Fblog%2Fyaml-file-for-kubernetes%2F&from=auto&to=auto&type=1&product=mdictweb&salt=1648861041808&sign=2ff277b53b8439482956d05229afc7ea


帖子最初发表于[ARMO](http://webtrans.yodao.com/server/webtrans/tranUrl?url=https%3A%2F%2Fwww.armosec.io%2Fblog%2Fyaml-file-for-kubernetes%2F&from=auto&to=auto&type=1&product=mdictweb&salt=1648861041808&sign=2ff277b53b8439482956d05229afc7ea), 由研发副总裁兼联合创始人`Ben Hirschberg`撰写的博客

尽管`Kubernetes`越来越受欢迎，但它仍然有一定的学习难度，这使得很难采用这项技术。那些不能克服最初障碍的人通常会在快节奏的软件开发领域落后。

本文将介绍`Kubernetes`对象配置的`YAML`文件。`YAML`提供了一种以声明方式配置`Kubernetes API`的方法，这些声明性文件允许您有效地扩展和管理应用程序。如果不了解`Kubernetes`对象的基本配置，很容易陷入运行应用程序的陷阱，并且不了解问题的原因。

需要注意的是，YAML和JSON都可以用于`Kubernetes`的配置，尽管在某些情况下YAML被认为更难编写，但它要紧凑得多。当考虑到必须为大型应用程序管理的所有YAML文件时，这是一个很大的优势。

总的来说，YAML更易于用户使用，紧凑的特性允许您对相关的`Kubernetes`对象进行分组，从而减少所需文件的数量。这是因为YAML的设计目标是增加可读性并提供更完整的信息模型。它可以简单地看作是JSON的演变。

## `Kubernetes`的YAML

为了开始使用`Kubernetes`的YAML，我们将回顾YAML的基本结构类型，如下表所示:

+ Key/Field-Value Pair
```yaml
app: redis
role: replica
tier: backend
```
+ List
```yaml
containers:   
  – name: webserver1
  – name: database-server
  – name: rss-reader  
```
+ Map
```yaml
containers:
   – name: webserver1
      image: nginx:1.6
      ports:
        – containerPort:80
   – name: database-server
      image: mysql-3.2
      ports:
        – containerPort:3306
   – name: rss-reader
      image: rss-php-nginx:2.8
      ports:
        – containerPort:86
```

在为`Kubernetes`编写YAML文件时，必须提供四个必需的字段: `apiVersion, Kind, Metadata, spec.`

### apiVersion

该字段引用API，用于创建正在定义的`Kubernetes`对象。`Kubernetes`提供了各种api，使您能够创建不同的`Kubernetes`对象。例如，`apiVersion: v1 `包含许多核心对象。

`apiVersion: v1`通常被认为是`Kubernetes`发布的第一个稳定版本。另一个流行的`APIVersion`是`apps/v1`，它采用了`v1`中的对象，并提供了部署和复制集等关键功能。+

因此，在我们的YAML文件中，定义APIVersion可以是:
```yaml
apiVersion: v1
```
### Kind

kind允许你指定你要定义的`Kubernetes对象的类型`。你将在这个字段中指定的对象将被链接到你之前指定的`apiVersion`，因为它是apiVersion字段，使你能够访问不同类型的对象及其特定的定义。可以定义的对象类型有`pod、service`和`daemonset`。

因此，为了定义一个pod对象，在指定apiVersion之后，我们将指定如下所示的kind字段:
```yaml
apiVersion: v1
kind: pod
```
### metadata

在指定要定义的对象类型之后，`metadata`字段为该特定对象提供`惟一的属性`。这可能包括name、uuid和namespace 字段。为这些字段指定的值为我们提供了对象的上下文，它们可以被其他对象引用。这个字段允许我们指定对象的标识符属性。

例如，如果我们正在构建一个spring应用程序，我们的pod可以有如下所示的name值:
```yaml
apiVersion: v1
kind: Pod
metadata:
    name: spring-pod
```
###　spec

`spec`字段允许我们定义我们所构建的对象`所期望的内容`。它由特定于定义对象操作的所有键值对组成。就像对象本身一样，对象的规格依赖于之前指定的`apiVersions`。因此，不同的apiversion可能包含相同的对象，但是可以定义的对象的规格可能会不同。

如果我们继续我们构建spring应用程序的pod对象的例子，我们的spec字段可以像下面你看到的那样:
```yaml
apiVersion: v1
kind: Pod
metadata:
    name: spring-pod
    containers:
    – image: armo/springapp:example
spec:
      name: spring-app
      ports:
      – containerPort: 80
        protocol: TCP
```
在上面的最后一个示例文件中，我们使用`API v1`创建一个`Pod`对象，我们将其命名为`spring-pod`。根据规范，我们将使用的端口为80，使用的镜像为`armo/springapp:example`。

下图比较了`JSON`和`YAML`中的配置。正如您所看到的，YAML更加简洁和可读。
[](https://www.armosec.io/wp-content/uploads/2022/02/Picture1-3.png)

## 管理YAML文件的策略

现在我们已经构建了`Kubernetes` YAML文件，让我们看看一些进一步加速构建`Kubernetes`应用程序的策略。

### YAML版本控制

如上所述，使用YAML字段允许您以声明方式管理`Kubernetes`应用程序。这些YAML文件可以存储在一个公共目录中，并且可以使用`kubectl apply -f` 来应用它们。

这相当简单。但是，随着应用程序的发展和`Kubernetes`对象定义的更改，您必须考虑当前部署的是哪个版本和哪个更改。这是因为，如果没有版本控制，您可能无法回滚到以前的映像，而以前的映像可能比新的活动映像更稳定。

有几种方法可以实现`YAML版本控制`，它们的复杂性和管理的方便性各不相同。这可能需要在`YAML`中使用简单的标记以及手动`kubectl`命令，也可能需要使用特殊的工具，如[`Helm`](http://webtrans.yodao.com/server/webtrans/tranUrl?url=https%3A%2F%2Fhelm.sh%2F&from=en&to=zh-CHS&type=0&product=mdictweb&salt=1648894764918&sign=6cb16c4994ea8a8f0cae4ed0d7a81864)。详细介绍这些方法超出了本文的范围，但是值得一提的是，版本控制是一个重要的实践。

### 管理Secrets

在应用程序的整个生命周期中，您可能需要利用机密数据来支持其功能。这可能包括密码、用户信息，甚至信用卡详细信息。

当您定义`Kubernetes`对象时，您可以将这些数据直接作为变量放入YAML文件中。然而，这可能会导致重大的安全漏洞，并且增加了这些数据落入坏人之手的可能性。因此，建议使用`Kubernetes`提供的`Secrets`对象以及秘密管理工具。

当利用`Secrets`对象时，pod必须引用Secret。这就是我们使用`metadata` 的地方。pod将使用`Secret name`字段，其中`Secret`对象的名称将是一个有效的`DNS子域名`。

下面是一个已定义的Secrets对象的例子:
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
data:
  username: YWRtaW4=
  password: MWYyZDFlMmU2N2Rm
```
一定要注意`Secret`类型。在上面的例子中，我们将`Secrets`类型指定为`Opaque`。这是`Secret`对象的默认类型。另一种Secrets类型是`kubernetes.io/service-account-token`，用于存储服务帐户token。

更多关于如何在``Kubernetes``中管理秘密的详细信息可以在`ARMO`的资源中找到。

### YAML模板

YAML模板的目的是通过在`Kubernetes`应用程序中重用YAML文件来减少我们必须编写和管理的YAML文件的数量。例如，让我们考虑下面的pod定义:
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: spring-pod
spec:
  containers:
  – image: armo/springapp:example
    name: spring-app
    env:
    – name: eu-west-1
      value: postgres://db_url:5432
```
这个pod 将在欧盟西一地区。但是，如果我们想为美国的客户部署us-west-1，我们将不得不编写一个新的YAML文件，因为我们使用的是硬编码的值。

这个问题的解决方案是YAML模板，它允许我们使用不同的值重用YAML文件。YAML模板有几种方法，但最基本的方法是搜索和替换我们为YAML文件中的值使用占位符的地方。

因此，使用最初的例子，我们将有一个占位符值，就像在env字段下看到的那样:
```yaml
apiVersion: v1
kind: Pod
metadata:
  name: spring-pod
spec:
  containers:
  – image: armo/springapp:example
    name: spring-app
    env:
    – name: ENV
      value: %DEPLOYMENT_ENV%
```
通过利用bash，我们可以在处理YAML文件之前搜索和替换这些值。然而，这是最基本和最不方便的方法，因为您必须手动执行占位符的硬编码。

更方便的方法是利用像Helm这样的工具来构建和管理模板。

## 结论

由于YAML的结构方式及其设计目标，它是`Kubernetes`配置的最佳解决方案。增强的可读性和简洁的结构允许您扩展`Kubernetes`配置，而不会迷失在成堆的配置文件中。

此外，通过理解如何以声明的方式构建`Kubernetes`应用程序，我们还可以理解`Kubernetes`应用程序如何以及为什么运行的机制。考虑YAML模板等高级功能可以帮助简化定义和构建`Kubernetes`应用程序的过程。