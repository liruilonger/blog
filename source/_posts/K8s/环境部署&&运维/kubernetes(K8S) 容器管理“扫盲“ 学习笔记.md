﻿﻿---
title: kubernetes(K8S) 容器管理“扫盲“ 学习笔记
date: 2021/6/13 20:46:25
categories: Kubernetes
toc: true
tags:
  - Kubernetes
  - 容器管理
  - Kubernetes
---
**<font color="#409EFF">太多人活得不像自己。思想是别人的意见,生活是别人的模仿,情感是别人的引述。----王尔德</font>**

<!-- more -->

## 写在前面
***
+ 看着`Activiti`官网的博客教程学`Activiti7`，需要用这个，之前看有些运维岗需要学，所以了解一下。
+ 笔记是看视频学的时候做的
+ 笔记内容包含对`K8S`的一个基本认识。
+ 建议学习的时候可以先了解一些基本的编排工具，类似`docker-compose`

**<font color="#409EFF">太多人活得不像自己。思想是别人的意见,生活是别人的模仿,情感是别人的引述。----王尔德</font>**
***
## 一、kubernetes 概述
### 1、kubernetes 基本介绍
#### kubernetes 是什么
`kubernetes`，简称 `K8s`，是用 8 代替 8 个字符“ubernete”而成的缩写。是一个开源的，用于管理`云平台`中多个`主机`上的`容器化的应用`，`Kubernetes` 的`目标`是让`部署容器化`的应用简单并且高效(powerful),`Kubernetes` 提供了应用部署，规划，更新，维护的一种机制。说白了，K8S，就是基于容器(Docker单机版)的集群管理平台,用于管理多个`Docker`的。Docker 非常适合在一台主机上运行容器，并为此提供所有必需的功能。但在当今的分布式服务环境中，真正的挑战是管理跨服务器和复杂基础架构的资源和工作负载。

传统的`应用部署`方式是通过`插件`或`脚本`来安装应用。这样做的缺点是应用的运行、配置、管理、所有`生存周期`将与当前`操作系统`绑定，这样做并不利于应用的`升级更新/回滚`等操作，当然也可以通过`创建虚拟机`的方式来实现某些功能，但是虚拟机非常重，并不利于可移植性。

新的方式是通过`部署容器`方式实现，每个`容器`之间互相隔离，每个`容器`有自己的文件系统 ，`容器`之间进程不会相互影响，能区分计算资源。相对于`虚拟机`，`容器`能快速部署(虚拟机可以理解为`硬件抽象`,容器可以理解为`系统抽象,共用Linux内核`)，由于`容器`与底层设施、机器文件系统解耦的，所以它能在不同云、不同版本操作系统间进行`迁移`。容器占用`资源少`、`部署快`，每个应用可以被打包成一个`容器镜像`，每个应用与容器间成`一对一`关系也使容器有更大优势，使用容器可以在 `build` 或 `release` 的阶段，为应用创建容器镜像，因为每个应用不需要与其余的`应用堆栈组合`，也不依赖于`生产环境`基础结构，这使得从研发到测试、生产能提供一致环境。类似地，容器比虚拟机轻量、更“透明”，这更便于监控和管理。
#### `Kubernetes` 是 `Google` 开源的一个`容器编排引擎`
`Kubernetes` 是 `Google` 开源的一个`容器编排引擎`，它支持`自动化部署`、`大规模可伸缩`、`应用容器化管理`。在生产环境中部署一个应用程序时，通常要部署该应用的多个实例以便对应用请求进行`负载均衡`。在 `Kubernetes` 中，我们可以创建`多个容器`，每个容器里面运行一个应用实例，然后通过`内置`的`负载均衡策略`，实现对这一组`应用实例`的管理、发现、访问，而这些细节都不需要运维人员去进行复杂的手工配置和处理。

### 2、kubernetes 功能和架构
#### 2.1 概述
`Kubernetes` 是一个轻便的和可扩展的开源平台，用于`管理容器化应用和服务`。通过Kubernetes 能够进行应用的`自动化部署`和`扩缩容`。在 Kubernetes 中，会将组成应用的`容器组合`成一个`逻辑单元`以更易管理和发现。Kubernetes 积累了作为 Google 生产环境运行工作负载 15 年的经验，并吸收了来自于社区的最佳想法和实践。
#### 2.2 K8s 功能(Kubernetes 适用场景):
+ (1)`自动装箱`:基于容器对应用运行环境的资源配置要求`自动部署应用容器`
+ (2)`自我修复(自愈能力)`:当容器失败时，会对容器进行`重启`,当所部署的 Node 节点有问题时，会对容器进行`重新部署`和`重新调度`,当容器`未通过`监控检查时，会`关闭`此容器直到容器正常运行时，才会对外提供服务
+ (3)`水平扩展`:通过简单的`命令`、用户 UI 界面或基于 CPU 等资源使用情况，对应用容器进行`规模扩大`或`规模剪裁`
+ (3)`服务发现`:用户不需使用额外的服务发现机制，就能够基于 `Kubernetes` 自身能力实现`服务发现`和`负载均衡`
+ (4)`滚动更新`:可以根据应用的变化，对应用容器运行的应用，进行一次性或`批量式更新`
+ (5)`版本回退`:可以根据应用部署情况，对应用容器运行的应用，进行历史版本即时回退
+ (6)`密钥和配置管理`:在不需要重新构建镜像的情况下，可以`部署`和`更新密钥`和`应用配置`，类似`热部署`。
+ (7)`存储编排`:自动实现存储系统挂载及应用，特别对有状态应用实现数据持久化非常重要存储系统可以来自于本地目录、网络存储(NFS、Gluster、Ceph 等)、公共云存储服务
+ (8)`批处理`:提供一次性任务，定时任务；满足批量数据处理和分析的场景

#### 2.3 应用部署架构分类
(1) 无中心节点架构： GlusterFS
(2) 有中心节点架构： HDFS、K8S

#### 2.4 k8s 集群架构
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210614223114358.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
```js
Pod
#1》一个服务，是k8s管理的`最小单元`，k8s从 Pod中启动和管理容器；
#2》由Pod来管理一组相同功能的容器；
#3》一个Pod可以管理一个容器,也可以管理多个容器；

```
#### 2.5 k8s 集群架构节点角色功能
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615164817794.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615164859297.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)


`Master Node`:==集群主控制节点==，对集群进行调度管理，接受集群外用户去集群操作请求；`Master Node` 由
+ <font color=yellowgreen>API server (管理接口)</font>:是整个系统的对外接口，供客户端和其他组件调用，相当于`“营业厅”`
+ <font color=green>scheduler(调度器)</font>:负责对集群内部的资源进行调度，相当于` “调度室”`
+ <font color=red>controller (控制器)</font>:负责管理控制器，相当于`“大总管”`。
+ <font color=green>etcd (键值对数据库)</font>:是一个键值存储仓库，`存储集群的状态`
`Worker Node`:==集群工作节点==，运行用户业务应用容器；`Worker Node` 包含 `
+ <font color=brown>docker</font>：容器管理
+ <font color=amber>kubelet</font>：主要负责监视指派到它所在的 Pod，包括创建、修改、监控、删除等。
+ <font color=seagreen>kube-proxy</font>：主要负责为Pod对象提供代理
+ 其他附加服务
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210614225128282.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
## 分布式键值存储服务
### Etcd 服务
`etcd`是什么：
+ etcd 是 CoreOS 团队于 2013 年 6 月发起的开源项目，它的目标是构建一个高可用的分布式键值(key-value)数据库，基于 Go 语言实现。在分布式系统中，各种服务的配置信息的管理分享，服务的发现是一个很基本同时也是很重要的问题。CoreOS 项目就希望基于 etcd 来解决这一问题。
+ 我们使用 etcd 来存储网络配置，解决容器互联互通的问题。
#### 2.6 K8S核心概念：
通过`Service`统一入口进行访问，`Controller`用于创建`Pot`，`Pot`是一组容器的集合。
1. `Pod`:**<font color="#009688">最小部署单元,一组容器的集合,共享网络,生命周期是短暂的**
2. `controller`：**<font color="#009688">确保预期的`pod副本数量`、无状态应用部署(无约定)、有状态应用部署(有特定条件)、确保所有的node运行同一个pod、一次性任务和定时任务**
3. `Service`：**<font color="#009688">定义一组pod的访问规则**

##### 搭建k8环境平台规划
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615133825772.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615134319601.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
##### 服务器硬件配置要求搭建
测试环境：
+ master：2核+4G+20G
+ node：4核+8G+40G
生产环境：

####  Kubernetes 架构

![在这里插入图片描述](https://img-blog.csdnimg.cn/885fbce5f5e64472aa531fba8e7cbbb5.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

+ <font color=tomato>核心角色</font>
  + master (管理节点)
  + node(计算节点)
  + image (镜像仓库)

##### master 节点
![在这里插入图片描述](https://img-blog.csdnimg.cn/56c5ff1c9e524bb484997c38f439a431.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

+ <font color=green>master 节点服务</font>
  + API server (管理接口)
  + scheduler(调度器)
  + controller (控制器)
  + etcd (键值对数据库)

#### Node 节点
+ <font color=orange>node节点服务</font>
  + docer
  + kubelet
  + kube-proxy
  + 其他附加服务
## 二、kubernetes 集群搭建(kubeadm 方式)
### 搭建方式
目前生产部署 Kubernetes 集群主要有两种方式：
+ (1)kubeadm
`Kubeadm` 是一个` K8s 部署工具`，提供` kubeadm init `和 `kubeadm join`，用于`快速部署` Kubernetes `集群`。[官方地址](https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm/)
+ (2)二进制包
从 github 下载发行版的二进制包，手动部署每个组件，组成 Kubernetes 集群。Kubeadm 降低部署门槛，但屏蔽了很多细节，遇到问题很难排查。如果想更容易可控，推荐使用二进制包部署 Kubernetes 集群，虽然手动部署麻烦点，期间可以学习很多工作原理，也利于后期维护。

嗯，感兴趣的小伙伴也可以看看我的 博客 [从零搭建Linux+Docker+Ansible+kubernetes 学习环境(1*Master+3*Node)](https://blog.csdn.net/sanhewuyang/article/details/120300517) 类似二进制的安装方式，但是有些落后。用的k8s1.5 的包

### kubeadm 部署方式介绍
kubeadm 是官方社区推出的一个用于快速部署 `kubernetes` 集群的工具，这个工具能通过两条指令完成一个 `kubernetes` 集群的部署：
+ 第一、创建一个 `Master` 节点 `kubeadm init`
+ 第二， 将 `Node `节点加入到当前集群中 `$ kubeadm join <Master 节点的 IP 和端口 >`

### 安装要求
在开始之前，部署 Kubernetes 集群机器需要满足以下几个条件：
- 一台或多台机器，操作系统 CentOS7.x-86_x64
- 硬件配置：2GB 或更多 RAM，2 个 CPU 或更多 CPU，硬盘 30GB 或更多
- 集群中所有机器之间网络互通
- 可以访问外网，需要拉取镜像
- 禁止 swap 分区
### 最终目标

|角色| IP|
|--|--|
|k8s-master| 192.168.31.61|
|k8s-node1 |192.168.31.62|
|k8s-node2 |192.168.31.63|
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615135448298.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
### 6、系统初始化
```bash

#关闭防火墙：
$ systemctl stop firewalld
$ systemctl disable firewalld

#关闭 selinux：
$ sed -i 's/enforcing/disabled/' /etc/selinux/config # 永久
$ setenforce 0 # 临时

# swap：
$ swapoff -a # 临时
$ vim /etc/fstab # 永久

# 主机名：
$ hostnamectl set-hostname <hostname>

#  在 master 添加 hosts：
$ cat >> /etc/hosts << EOF
192.168.31.61 k8s-master
192.168.31.62 k8s-node1
192.168.31.63 k8s-node2
EOF

#将桥接的 IPv4 流量传递到 iptables 的链：
$ cat > /etc/sysctl.d/k8s.conf << EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
$ sysctl --system # 生效

#时间同步：
$ yum install ntpdate -y
$ ntpdate time.windows.com
```
### 7、所有节点安装 Docker/kubeadm/kubelet
#### Kubernetes 默认 CRI(容器运行时)为 `Docker`，因此先安装 Docker。
```bash
#(1)安装 Docker
$ wget https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo -O /etc/yum.repos.d/docker-ce.repo
$ yum -y install docker-ce-18.06.1.ce-3.el7
$ systemctl enable docker && systemctl start docker
$ docker --version

#(2)添加阿里云 YUM 软件源
#设置仓库地址
$ cat > /etc/docker/daemon.json << EOF
{
"registry-mirrors": ["https://b9pmyelo.mirror.aliyuncs.com"]
}
EOF
#添加 yum 源
$ cat > /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg
https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
EOF

#(3)安装 kubeadm，kubelet 和 kubectl
$ yum install -y kubelet kubeadm kubectl
$ systemctl enable kubelet
```
### 8、部署 Kubernetes Master
```bash
#(1)在 192.168.31.61(Master)执行
$ kubeadm init \
--apiserver-advertise-address=192.168.31.61 \
--image-repository registry.aliyuncs.com/google_containers \
--kubernetes-version v1.17.0 \
--service-cidr=10.96.0.0/12 \
--pod-network-cidr=10.244.0.0/16
#由于默认拉取镜像地址 k8s.gcr.io 国内无法访问，这里指定阿里云镜像仓库地址。

#(2)使用 kubectl 工具：
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
$ kubectl get nodes
```
### 9、安装 Pod 网络插件(CNI)
```bash
$ kubectl apply –f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kubeflannel.yml
#检查一下
kubect1 get pods -n kube-system
```
确保能够访问到 quay.io 这个 registery。如果 Pod 镜像下载失败，可以改这个镜像地址

### 10、加入 Kubernetes Node
```bash
#(1)在 192.168.31.62/63(Node)执行
向集群添加新节点，执行在 kubeadm init 输出的 kubeadm join 命令：
$ kubeadm join 192.168.31.61:6443 --token esce21.q6hetwm8si29qxwn \
--discovery-token-ca-cert-hash
sha256:00603a05805807501d7181c3d60b478788408cfe6cedefedb1f97569708be9c5
```
### 11、测试 kubernetes 集群
```bash
#在 Kubernetes 集群中创建一个 pod，验证是否正常运行：
$ kubectl create deployment nginx --image=nginx
$ kubectl expose deployment nginx --port=80 --type=NodePort
$ kubectl get pod,svc
#访问地址：http://NodeIP:Port
```

## 三、<font color=tomato>kubernetes 集群搭建(二进制方式)</font>
这个先不看

## 四，<font color=tomato>kubetcl 命令</font>
### kubectl命令

`Kubectl` 是用于控制` Kubernetes` 集群的命令行工具

>`kubectl [command] [type] [name] [flags] 语法格式`

|kubectl|
|--|
|`command`: 子命令，如：`create，get，describe，delete`|
|`type`: 资源类型，可以表示为单数，复数或缩写形式|
|`name`: 资源的名称，如果省略，则显示所有资源的详细信息|
|`flags`: 指定可选标志，或附加的参数|

>`kubectl get`

|kubectl get|
|--|
|kubectl get 查询资源|
|get node 查询节点状态|
|get pod 查看 pod 容器资源|
|get deployment 查看资源名称|

k8s中的资源是分层级结构的，可以查看顶层资源deploy，也可以查看底层资源pod
,查看master管理主机上的服务状态

```bash
[liruilong@kube-master ~]# kubectl get cs
NAME STATUS MESSAGE ERROR
controller-manager Healthy ok
scheduler Healthy ok
etcd-0 Healthy {"health":"true"}
#查看node节点的状态信息
[liruilong@kube-master ~]# kubectl get node
NAME STATUS ROLES AGE VERSION
kube-node1 Ready <none> 1d v1.10.3
kube-node2 Ready <none> 1d v1.10.3
kube-node3 Ready <none> 1d v1.10.3
#查看deploy顶层资源的信息
[liruilong@kube-master ~]# kubectl get deployment
No resources found.
#查看deploy顶层资源下pod的信息
[liruilong@kube-master ~]# kubectl get pod
No resources found.
#查看deploy顶层资源下pod的信息
#-o wide 会显示此pod是在哪一个节点上
[liruilong@kube-master ~]# kubectl get pod -o wide
```
>`kubectl run`

kubectl run 创建容器：在前台创建容器：

`kubectl run 资源(deploy)名称 -i -t --image=镜像名称: 标签`

```bash
############### 创建容器，在kube-master上操作 ################
#根据私有仓库中的镜像创建容器，haha 为deploy资源名称
[liruilong@kube-master ~]# kubectl run haha -i -t --image=镜像信息
```

>`kubectl exec`

<font color=blue>退出容器使用(Ctrl + p + q),退出以后容器仍在运行，进入容器使用 exec</font>

`kubectl exec -it 容器id 执行的命令`
```bash
#退出容器，使用【Ctrl + p + q】不会关闭容器
#使用exit，会关闭容器，但k8s中，pod会自动再把容器开启
[liruilong@haha-5b58c4687-lg78n /]#
#查看deploy顶层资源下pod的信息
[liruilong@kube-master ~]# kubectl get pod
NAME READY STATUS RESTARTS AGE
haha-5b58c4687-lg78n 1/1 Running 0 2m
##使用exec进入容器，/bin/bash为容器内命令
##attach 也可以，但是会因为有些容器并没有进入终端，无法进入容器，常用于排错
[liruilong@kube-master ~]# kubectl exec -it haha-5b58c4687-lg78n /bin/bash
[liruilong@haha-5b58c4687-lg78n /]#
```
>`kubectl describe`

**<font color=brown>查看资源详细信息(经常用于排错),`kubectl describe` 资源类型 资源名称</font>**

```bash
#查看deploy顶层资源的信息
[liruilong@kube-master ~]# kubectl get deployment
NAME DESIRED CURRENT UP-TO-DATE AVAILABLE AGE
haha 1 1 1 1 25m
#describe 查看deploy资源haha的详细信息，常用于排错
[liruilong@kube-master ~]# kubectl describe deployment haha
#查看deploy顶层资源下pod的信息
[root@kube-master ~]# kubectl get pod
NAME READY STATUS RESTARTS AGE
haha-5b58c4687-lg78n 1/1 Running 1 27
#查看pod【haha-5b58c4687-lg78n】的详细信息，常用于排错
[root@kube-master ~]# kubectl describe pod haha-5b58c4687-lg78
```

>` kubectl console 管理`

**<font color=amber>查看 `console` 终端的输出信息:`logs `和` attach `命令.</font>**
```bash
########### attach 查看控制台的输出信息，在kube-master上操作 ###########
#查看deploy顶层资源下pod的信息
[root@kube-master ~]# kubectl get pod
NAME READY STATUS RESTARTS AGE
haha-5b58c4687-lg78n 1/1 Running 1 31m
#使用attach 以上帝进程的身份进入容器，不建议使用(建议使用exec)，常用于排错
[root@kube-master ~]# kubectl attach haha-5b58c4687-lg78n -c haha -i -t
If you don't see a command prompt, try pressing enter.
#输出信息
[root@haha-5b58c4687-lg78n /]# echo hello world
hello world
#输出信息
[root@haha-5b58c4687-lg78n /]# echo hello world
hello world
#退出容器，使用【Ctrl + p + q】
[root@haha-5b58c4687-lg78n /]#
```

