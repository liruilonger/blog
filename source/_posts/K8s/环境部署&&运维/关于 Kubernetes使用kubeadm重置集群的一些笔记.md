---
title: 关于 Kubernetes使用kubeadm重置集群的一些笔记
tags:
  - Kubernetes
  - kubeadm
categories:
  - kubeadm
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2021-12-12 13:14:44/关于 Kubernetes使用kubeadm重置集群的一些笔记.html'
mathJax: false
date: 2021-12-12 21:14:44
thumbnail:
---
**<font color="009688"> 有些女子，明知很不好，可就是放不下的。  ——烽火戏诸侯《雪中悍刀行》**</font>
<!-- more -->
## 写在前面
***
+ 电脑系统坏了，windows10蓝屏，所以我强制重启了、之前运行k8s集群这么搞也起不来，所以只能重置了
+ 博文主要是`重置k8s集群`的笔记，其实和安装是一样的，多了一个命令。
+ 这里`默认之前的镜像和相关的服务包任然存在`的。
+ 关于详细的安装步骤小伙伴可以看我的`Kubernetes 管理员认证(CKA)考试笔记(一)`这篇博文。


**<font color="009688"> 有些女子，明知很不好，可就是放不下的。  ——烽火戏诸侯《雪中悍刀行》**</font>
 ***



**<font color=brown>高版本需要修改资源管理为systemd</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible master,node -m shell -a "sed  -i '3i ,\"exec-opts\": [\"native.cgroupdriver=systemd\"]' /etc/docker/daemon.json"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible master,node -m shell -a "systemctl restart docker"
```
### <font color=red>重置节点</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible master,node -m shell -a "kubeadm reset -f"
```
### <font color=brown>初始化话mater节点</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible master -m shell -a "kubeadm init --image-repository registry.aliyuncs.com/google_containers --kubernetes-version=v1.22.2 --pod-network-cidr=10.244.0.0/16"
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$mkdir -p $HOME/.kube
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$export KUBECONFIG=/etc/kubernetes/admin.conf
```
### <font color=camel>添加node节点</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get nodes
NAME                         STATUS     ROLES                  AGE   VERSION
vms81.liruilongs.github.io   NotReady   control-plane,master   89s   v1.22.2
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubeadm token create --print-join-command
kubeadm join 192.168.26.81:6443 --token e5uvm8.06sov2egw6j8570k --discovery-token-ca-cert-hash sha256:2e17952177d9c633254e6941849885fc8e0e16dde805425effa22ed04415e7d4
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m shell -a "kubeadm join 192.168.26.81:6443 --token e5uvm8.06sov2egw6j8570k --discovery-token-ca-cert-hash sha256:2e17952177d9c633254e6941849885fc8e0e16dde805425effa22ed04415e7d4"

```
### <font color=orange>配置网络</font>
**<font color=plum>这里的资源文件，镜像是之前导入的</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl apply -f calico.yaml
```
### <font color=green>重置成功</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get nodes
NAME                         STATUS   ROLES                  AGE     VERSION
vms81.liruilongs.github.io   Ready    control-plane,master   9m40s   v1.22.2
vms82.liruilongs.github.io   Ready    <none>                 3m39s   v1.22.2
vms83.liruilongs.github.io   Ready    <none>                 3m37s   v1.22.2
```
