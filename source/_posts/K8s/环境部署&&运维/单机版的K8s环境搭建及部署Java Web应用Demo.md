---
title: 单机版的K8s环境搭建及部署Java Web应用Demo
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2021-10-16 03:09:01/单机版的K8s环境搭建及部署Java Web应用Demo.html'
mathJax: false
date: 2021-10-16 11:09:01
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 书里看到，这里单独拿出整理一下
+ 博文内容包括：
  + `K8s环境单机版搭建`
  + `Tomcat+mysql` 一个简单的Java Web APP 应用实践

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>
 ***

# <font color=green>K8s环境单机版搭建</font>
## <font color=amber>1. 环境准备</font>

|环境准备|
|--|
|关闭CentoS自带的防火墙服务(交换分区)|
|安装etcd和Kubernetes软件(会自动安装Docker软件):|
|按顺序启动所有的服务:|
|查看服务状态|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/9a1f1744bcfa481db4826d312b51bb8d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

```bash
# 关闭CentoS自带的防火墙服务:
systemctl disable firewalld --now
sed -i '/swap/d' /etc/fstab
# 安装etcd和Kubernetes软件(会自动安装Docker软件): 
yum install -y etcd kubernetes
#按顺序启动所有的服务:
systemctl start etcd 
systemctl start docker 
systemctl start kube-apiserver 
systemctl start kube-controller-manager 
systemctl start kube-scheduler 
systemctl start kubelet 
systemctl start kube-proxy
# 查看服务状态
systemctl status etcd docker kube-apiserver kube-controller-manager kube-scheduler kubelet kube-proxy

```

**<font color=brown>至此,一个单机版的Kubernetes集群环境就安装启动完成了。接下来,我们可以在这个单机版的Kubernetes集群中上手练习了。</font>** 

镜像相关地址： https://hub.docker.com/u/kubeguide/.

# <font color=orange>一个简单的Java Web APP 应用实践</font>
## <font color=chocolate>1. 启动MySQL服务</font>
>首先为MySQL服务创建一个`RC`定义文件:` mysql-rc.yaml`,文件的完整内容和解释;
```yml
apiVersion: v1
kind: ReplicationController #副本控制器RC
metadata:                   
  name: mysql               # RC的名称,全局唯一
spec:
  replicas: 1                # Pod副本期待数量
  selector:                  # 符合目标的Pod拥有此标签
    app: mysql               # 根据此模板创建Pod的副本(实例).
  template:
    metadata:                 #Pod副本拥有的标签,对应RC的Selector
      labels:
        app: mysql
    spec:
      containers:                    # Pod内容器的定义部分
        - name: mysql                # 容器的名称,容器对应的Docker Image
          image: mysql
          ports:                     #容器应用监听的端口号
            - containerPort: 3306
          env:                       #注入容器内的环境变量
            - name: MYSQL_ROOT_PASSWORD
              value: "123456"
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/72e6190dbb3c44f5b0346e25645734a2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

>`yaml`定义文件中

|`yaml`定义文件|
|--|
|`kind属性`,用来表明此资源对象的类型,比如这里的值为"ReplicationController",表示这是一个RC: |
|`spec一节`中是RC的相关属性定义,比如`spec.selector是RC的Pod标签(Label)选择器`,即监控和管理拥有这些标签的Pod实例,确保当前集群上始终`有且仅有replicas个Pod实例在运行`,这里我们设置`replicas=1`表示只能运行一个MySQL Pod实例。|
|当集群中运行的`Pod数量`小于`replicas`时, RC会根据`spec.template`一节中定义的`Pod`模板来生成一个新的`Pod`实例, `spec.template.metadata.labels`指定了该`Pod`的标签.|
|需要特别注意的是:这里的`labels必须匹配之前的spec.selector`,否则此`RC每次创建了一个无法匹配Label的Pod`,就会不停地`尝试创建新的Pod`。


```bash
[root@liruilong k8s]# kubectl create -f mysql-rc.yaml
replicationcontroller "mysql" created
E:\docker>ssh  root@39.97.241.18
Last login: Sun Aug 29 13:00:58 2021 from 121.56.4.34

Welcome to Alibaba Cloud Elastic Compute Service !

^[[AHow would you spend your life?.I don t know, but I will cherish every minute to live.
[root@liruilong ~]# kubectl  get rc
NAME      DESIRED   CURRENT   READY     AGE
mysql     1         1         1         1d
[root@liruilong ~]# kubectl  get pods
NAME          READY     STATUS    RESTARTS   AGE
mysql-q7802   1/1       Running   0          1d
[root@liruilong ~]#
```
><font color=purple>嗯，这里刚开始搞得的时候是有问题的，`pod`一直没办法创建成功，第一次启动容器时，STATUS一直显示CONTAINERCREATING,我用的是阿里云ESC单核2G+40G云盘，我最开始以为系统核数的问题,因为看其他的教程写的需要双核，但是后来发现不是，网上找了解决办法，一顿操作猛如虎，后来不知道怎么就好了。</font>
+ 有说基础镜像外网拉不了，只能用 docker Hub的，有说 ，权限的问题，还有说少包的问题，反正都试了，这里列出几个靠谱的解决方案
  + https://blog.csdn.net/gezilan/article/details/80011905
  + https://www.freesion.com/article/8438814614/

K8s 根据mysqlde RC的定义自动创建的Pod。由于Pod的调度和创建需要花费一定的时间,比如需要一定的时间来确定调度到哪个节点上,以及下载Pod里容器的镜像需要一段时间,所以一开始我们看到Pod的状态将显示为`Pending`。当Pod成功创建完成以后,状态最终会被更新为`Running`我们通过`docker ps`指令查看正在运行的容器,发现提供MySQL服务的Pod容器已经创建并正常运行了,此外,你会发现MySQL Pod对应的容器还多创建了一个来自谷歌的`pause`容器,这就是`Pod的“根容器"`.

**<font color=yellowgreen>我们创建一个与之关联的`Kubernetes Service` 的定义文件 `mysql-sve.yaml`</font>**

```yml
apiVersion: v1
kind: Service  # 表明是Kubernetes Service
metadata:
  name: mysql  # Service的全局唯一名称
spec:
  ports:
    - port: 3306 #service提供服务的端口号
  selector:      #Service对应的Pod拥有这里定义的标签
    app: mysql
```

**<font color=green>我们通过`kubectl create`命令创建`Service`对象。运行kubectl命令:</font>**

```bash
[root@liruilong k8s]# kubectl create -f mysql-svc.yaml
service "mysql" created
[root@liruilong k8s]# kubectl  get svc
NAME         CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
mysql        10.254.155.86   <none>        3306/TCP   1m
[root@liruilong k8s]#
```

>注意到`MySQL`服务被分配了一个值为10.254.155.86的`Cluster IP`地址,这是一个虚地址,随后, `Kubernetes集群`中其他新创建的`Pod`就可以通过`Service`的`Cluster IP`+端口号`3306`来连接和访问它了。

在通常情况下, `Cluster IP`是在Service创建后由`Kubernetes`系统自动分配的,其他`Pod`无法预先知道某个`Service的Cluster IP地址`,因此需要一个`服务发现机制`来找到`这个服`务。

为此,最初时, `Kubernetes`巧妙地使用了`Linux`环境变量(Environment Variable)来解决这个问题,后面会详细说明其机制。现在我们只需知道,根据`Service`的唯一名字,容器可以从环境变量中获取到`Service对应的Cluster IP地址和端口,从而发起TCP/IP连接请求了`。

## <font color=chocolate>2.启动Tomcat应用</font>
**<font color=green>创建对应的 `RC `文件 `myweb-rc.yaml `</font>**
```yml
apiVersion: v1
kind: ReplicationController
metadata: 
  name: myweb
spec:
  replicas: 2
  selector:
    app: myweb
  template:
    metadata:
      labels:
        app: myweb
    spec:
      containers: 
        - name: myweb
          image: kubeguide/tomcat-app:v1
          ports:
            - containerPort: 8080   

```
**<font color=green>`  replicas: 2`: 这里我们用两个tomcat(Pod)提供服务</font>**

![在这里插入图片描述](https://img-blog.csdnimg.cn/26ba99682f974214a2f5ce37b515e2d3.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_19,color_FFFFFF,t_70,g_se,x_16)
```bash
[root@liruilong k8s]# vim myweb-rc.yaml
[root@liruilong k8s]# kubectl create -f myweb-rc.yaml
replicationcontroller "myweb" created
[root@liruilong k8s]# kubectl get rc
NAME      DESIRED   CURRENT   READY     AGE
mysql     1         1         1         1d
myweb     2         2         0         20s
[root@liruilong k8s]# kubectl get pods
NAME          READY     STATUS              RESTARTS   AGE
mysql-q7802   1/1       Running             0          1d
myweb-53r32   0/1       ContainerCreating   0          28s
myweb-609w4   0/1       ContainerCreating   0          28s
[root@liruilong k8s]# kubectl get pods
NAME          READY     STATUS    RESTARTS   AGE
mysql-q7802   1/1       Running   0          1d
myweb-53r32   1/1       Running   0          1m
myweb-609w4   1/1       Running   0          1m
[root@liruilong k8s]#
```
**<font color=orange>最后，创建对应的 `Service` 。以下是完整`yaml`定义文件 `myweb-svc.yaml`:</font>**

**<font color=red>指定端口映射:30001:8080</font>**
```yml
apiVersion: v1
kind: Service
metadata: 
  name: myweb
spec:
  type: NodePort
  ports: 
    - port: 8080
      nodePort: 30001
  selector:
    app: myweb    
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/933adef70d3149b7ae61c42a43dc2105.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

```bash
[root@liruilong k8s]# vim myweb-svc.yaml
[root@liruilong k8s]# kubectl create -f  myweb-svc.yaml
service "myweb" created
[root@liruilong k8s]# kubectl get services
NAME         CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
kubernetes   10.254.0.1      <none>        443/TCP          2d
mysql        10.254.155.86   <none>        3306/TCP         5h
myweb        10.254.122.63   <nodes>       8080:30001/TCP   54s
[root@liruilong k8s]#
```

## <font color=purple>3.通过浏览器访问网页 </font>
```html
[root@liruilong k8s]# curl http://127.0.0.1:30001/demo/

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>HPE University Docker&Kubernetes Learning</title>
</head>
<body  align="center">



  <h3> Error:com.mysql.jdbc.exceptions.jdbc4.MySQLNonTransientConnectionException: Could not create connection to database server.</h3>

</body>
</html>
```
**<font color=orange>数据库连接有问题，这里百度发现是`mysql驱动版本`问题</font>**

```bash
[root@liruilong k8s]# docker logs a05d16ec69ff
[root@liruilong k8s]# vim mysql-rc.yaml
```
```yml
apiVersion: v1
kind: ReplicationController #副本控制器RC
metadata:                   # RC的名称,全局唯一
  name: mysql               # Pod副本期待数量
spec:
  replicas: 1
  selector:                  # 符合目标的Pod拥有此标签
    app: mysql               # 根据此模板创建Pod的副本(实例).
  template:
    metadata:                 #Pod副本拥有的标签,对应RC的Selector
      labels:
        app: mysql
    spec:
      containers:                    # Pod内容器的定义部分
        - name: mysql                # 容器的名称,容器对应的Docker Image
          image: mysql:5.7
          ports:                     #容器应用监听的端口号
            - containerPort: 3306
          env:                       #注入容器内的环境变量
            - name: MYSQL_ROOT_PASSWORD
              value: "123456"
```
```bash
[root@liruilong k8s]# kubectl delete -f mysql-rc.yaml
replicationcontroller "mysql" deleted
[root@liruilong k8s]# kubectl create -f mysql-rc.yaml
replicationcontroller "mysql" created
[root@liruilong k8s]# kubectl get rc
NAME      DESIRED   CURRENT   READY     AGE
mysql     1         1         0         10s
myweb     2         2         2         4h
[root@liruilong k8s]# kubectl get pods
NAME          READY     STATUS              RESTARTS   AGE
mysql-2cpt9   0/1       ContainerCreating   0          15s
myweb-53r32   1/1       Running             0          4h
myweb-609w4   1/1       Running             1          4h
[root@liruilong k8s]# kubectl get pods
NAME          READY     STATUS              RESTARTS   AGE
mysql-2cpt9   0/1       ContainerCreating   0          32s
myweb-53r32   1/1       Running             0          4h
myweb-609w4   1/1       Running             1          4h
[root@liruilong k8s]#
```
**<font color=purple>我们在上面的SVC中定义了Tomcat的两个pod。所以这里显示两个</font>**
```bash
Digest: sha256:7cf2e7d7ff876f93c8601406a5aa17484e6623875e64e7acc71432ad8e0a3d7e
Status: Downloaded newer image for docker.io/mysql:5.7
[root@liruilong k8s]# kubectl get pods
NAME          READY     STATUS    RESTARTS   AGE
mysql-2cpt9   1/1       Running   0          31m
myweb-53r32   1/1       Running   0          5h
myweb-609w4   1/1       Running   1          5h
[root@liruilong k8s]# curl http://127.0.0.1:30001/demo/
```
**<font color=brown>测试一下</font>**
```html
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>HPE University Docker&Kubernetes Learning</title>
</head>
<body  align="center">


      <h2>Congratulations!!</h2>
     <br></br>
         <input type="button" value="Add..." onclick="location.href='input.html'" >
             <br></br>
      <TABLE align="center"  border="1" width="600px">
   <TR>
      <TD>Name</TD>
      <TD>Level(Score)</TD>
   </TR>


 <TR>
      <TD>google</TD>
      <TD>100</TD>
   </TR>

 <TR>
      <TD>docker</TD>
      <TD>100</TD>
   </TR>

 <TR>
      <TD>teacher</TD>
      <TD>100</TD>
   </TR>

 <TR>
      <TD>HPE</TD>
      <TD>100</TD>
   </TR>

 <TR>
      <TD>our team</TD>
      <TD>100</TD>
   </TR>

 <TR>
      <TD>me</TD>
      <TD>100</TD>
   </TR>

  </TABLE>

</body>
</html>

```

![在这里插入图片描述](https://img-blog.csdnimg.cn/e73a62d87d824f51a2ac553cdbb986b6.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)


```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl scale rc mysql --replicas=0
replicationcontroller/mysql scaled
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl scale rc myweb --replicas=0
replicationcontroller/myweb scaled
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl delete -f rc  mysql
error: the path "rc" does not exist
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl delete -f  mysql-rc.yaml
replicationcontroller "mysql" deleted
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl delete -f   myweb-rc.yaml
replicationcontroller "myweb" deleted
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl get pod
NAME          READY   STATUS        RESTARTS   AGE
mysql-hhjnk   1/1     Terminating   0          4d19h
myweb-bn5h4   1/1     Terminating   0          4d19h
myweb-h8jkc   1/1     Terminating   0          4d19h
```
