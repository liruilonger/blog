---
title: Kubernetes:认识 K8s开源 Web/桌面 客户端工具 Headlamp  
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Headlamp
uniqueId: '2023-01-23 00:29:04/Kubernetes:认识K8s开源 Web/桌面 客户端工具 Headlamp.html'
mathJax: false
date: 2023-01-23 08:29:04
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 分享一个 k8s 客户端开源项目 `Headlamp` 给小伙伴
+ 博文内容涉及： 
  + Headlamp 桌面/集群 Web 端安装
  + 启动导入集群简单查看集群信息
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

Headlamp 是一个2022年开源一个 k8s 客户端项目，可以将它部署为具有` Web UI 界面`的集群内服务，或者是作为 `桌面端` 安装到有桌面的本地。Headlamp 有很好的扩展性，可以通过插件扩展。

在使用 `Headlamp` 桌面端的时候，需要提供 kubeconfig 文件位于本地机器上。如果在集群内部署不需要，会通过 sa 的方式访问。

Headlamp 的特性：

+ 供应商独立/通用 Kubernetes UI
+ 在集群中工作，或在本地作为桌面应用程序工作
+ 多集群
+ 可通过插件扩展
+ 反映用户角色的 UI 控件(不允许删除/更新)
+ 干净现代的用户界面
+ 可取消的创建/更新/删除操作
+ 带有文档的日志、执行和资源编辑器
+ 读写/交互(基于权限的操作)


### 桌面客户端部署

Headlamp 可以作为桌面应用程序运行，适用于不想将其部署在集群中的用户，或者希望在本地管理不相关集群的用户。

目前有适用于 Linux 、 Mac 和 Windows的桌面应用程序 。我们尝试在 window 上安装

#### 下载安装
```bash
curl -o  Headlamp-0.14.1-win-x64.exe https://github.com/kinvolk/headlamp/releases/download/v0.14.1/Headlamp-0.14.1-win-x64.exe
```

#### 启动


直接打开是这个样子，这里因为我们没有配置 `kubeconfig` 配置文件。
![在这里插入图片描述](https://img-blog.csdnimg.cn/0653050bb9ba4ac2912c0ed95ef0cdbd.png)

配置 kubeconfig 文件，这里我们直接重集群上拷贝一下。

```bash
PS C:\Program Files\Headlamp> scp root@192.168.26.81:/root/.kube/config ./
root@192.168.26.81's password:
config                                                                                100% 5677     3.9MB/s   00:00
```

通过命令行启动，指定配置文件

```bash
PS C:\Program Files\Headlamp> .\Headlamp.exe --kubeconfig config
PS C:\Program Files\Headlamp>
01:07:19.550 > App starting...
Check for updates:  true
arguments passed to backend server [ '--kubeconfig', 'config' ]
01:07:22.299 > server process stderr: 2023/01/14 01:07:22 plugins-dir: C:\Users\山河已无恙\AppData\Roaming\Headlamp\Config\plugins

01:07:22.301 > server process stdout: *** Headlamp Server ***
  API Routers:
        localhost:4466/clusters/kubernetes-admin@kubernetes/{api...} -> https://192.168.26.81:6443

Plugins are loaded. Loading full menu.
01:07:22.923 > server process stderr: 2023/01/14 01:07:22 Requesting  https://192.168.26.81:6443/api/v1/events

01:07:22.924 > server process stderr: 2023/01/14 01:07:22 Requesting  https://192.168.26.81:6443/version
..........
```
如果不希望使用命令行启动，或者你也可以把配置文件放到默认的位置读取位置
```bash
PS C:\Users\山河已无恙\.kube> ls

    目录: C:\Users\山河已无恙\.kube

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2023/1/14      7:48                cache
-a----         2023/1/13     22:05           5579 config
PS C:\Users\山河已无恙\.kube>
```
从桌面直接启动，这里又可以能需要管理员权限。
![在这里插入图片描述](https://img-blog.csdnimg.cn/e075306832b647cd9091d787ca49eacf.png)



#### 查看集群节点信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/88e344cf09a84b2ab5b261f2b406b6e5.png)


#### 查看集群资源

![在这里插入图片描述](https://img-blog.csdnimg.cn/723ab016507b4953bb0d22b47ad133ea.png)


### 集群部署 Web 客户端


#### 下载安装
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$mkdir kubernetes-headlamp
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cd kubernetes-headlamp/
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubernetes-headlamp]
└─$curl -o kubernetes-headlamp.yaml https://raw.githubusercontent.com/kinvolk/headlamp/main/kubernetes-headlamp.yaml
```
如果你没有科学上网,可以尝试浏览器访问然后下载


在应用之前，你需要做一些准备工作，这个镜像需要科学上网，所以你只能 在 `hub.docker` 找类似的，或者找可以科学上网的机器下载。这里我们替换了镜像
```bash
Pulling image "ghcr.io/kinvolk/headlamp:latest"
```
替换为 
```bash
docker pull epamedp/headlamp:0.14.0.6
```

同时为了能在集群外访问，这里设置 svc 类型为 `NodePort`
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubernetes-headlamp]
└─$cat kubernetes-headlamp.yaml
kind: Service
apiVersion: v1
metadata:
  name: headlamp
  namespace: kube-system
spec:
  ports:
    - port: 30025
      targetPort: 4466
  type: NodePort
  selector:
    k8s-app: headlamp
---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: headlamp
  namespace: kube-system
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: headlamp
  template:
    metadata:
      labels:
        k8s-app: headlamp
    spec:
      containers:
      - name: headlamp
        image: epamedp/headlamp:0.14.0.6
        args:
          - "-in-cluster"
          - "-plugins-dir=/headlamp/plugins"
        ports:
        - containerPort: 4466
        livenessProbe:
          httpGet:
            scheme: HTTP
            path: /
            port: 4466
          initialDelaySeconds: 30
          timeoutSeconds: 30
      nodeSelector:
        'kubernetes.io/os': linux
---
kind: Secret
apiVersion: v1
metadata:
  name: headlamp-admin
  namespace: kube-system
  annotations:
    kubernetes.io/service-account.name: "headlamp-admin"
type: kubernetes.io/service-account-token
```
应用 yaml 文件
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubernetes-headlamp]
└─$kubectl apply  -f kubernetes-headlamp.yaml
service/headlamp created
deployment.apps/headlamp created
secret/headlamp-admin created
```

嗯，这里会发现创建不了 Secret ，这是因为没有 sa 的原因，所以需要创建一个 sa。创建之后会默认生成一个 token 的 Secret。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubernetes-headlamp]
└─$kubectl -n kube-system create serviceaccount headlamp-admin
```
创建好 sa 之后需要给它赋权，这里是管理工具，所以给它内置的 `cluster-admin` 角色
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubernetes-headlamp]
└─$kubectl create clusterrolebinding headlamp-admin --serviceaccount=kube-system:headlamp-admin --clusterrole=cluster-admin
clusterrolebinding.rbac.authorization.k8s.io/headlamp-admin created
```

查看 sa 对应的 tocker
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubernetes-headlamp]
└─$kubectl get secrets | grep headlamp-admin-
headlamp-admin-token-8d5pr                       kubernetes.io/service-account-token   3      19m
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/14c4d6a146614427b7b5fe8c527dc8e0.png)

获取 toker 登录 Web 端
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubernetes-headlamp]
└─$kubectl get secrets   headlamp-admin-token-8d5pr  -o  jsonpath='{.data.token}' | base64 -d
eyJhbGciOiJSUzI1NiIsImtpZCI6ImF2MmJVZ3d6M21JRC1BZUwwaHlDdzZHSGNyaVJON1BkUHF6MlhPV2NfX00ifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJoZWFkbGFtcC1hZG1pbi10b2tlbi04ZDVwciIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJoZWFkbGFtcC1hZG1pbiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjI0NDMxNTA4LWU4ZjEtNGYxOC1hMWIxLTlmMmUzZmFhNDU3ZSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlLXN5c3RlbTpoZWFkbGFtcC1hZG1pbiJ9.oSA_i8gZOXYNRQMyoUKCK_wivtSiEjJ78EDUzZ1R7_HFxiLBKWLtxYN81wyf19bp9y9BFc2YYAW9lBy9QfVxg6LzBhW1sb4tcJJ0SOldxQX8z9kWK9m1MPMMs3aqtt1S9n8ShMBeobyY5AXSkBMDvVh6_E1P22dnPyOH7r_m0DEM0pgOP7B347sDKHiKx60hHBTfayvF7WDgfVlqsItBrc-MupC7NieRe8pztCllQ8awPksZXPRAJdcKwlSPvskoYxaqOBGbfZvFAFeLJaiGHdwkb6jUKyVfcB_hX_Pm5aEHGU8LZq7twrup859zxLxwn3nAgQpM6-NySZt8ax24kg
```

访问 http://192.168.26.81:30023/c/main/token  ，输入 token

进去可以看到资源信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/4f0dccc3bf92416488442bda93bf678d.png)

节点信息，查看 集群相关信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/5425b130af104dda9ad002c16cacdd17.png)


关于 Headlamp k8s 客户端就可小伙伴们分享到这里，如果条件允许，建议使用 通过桌面端，通过命名行的方式启动，只需要拷贝一个 kubeconfig 文件，Web 端需要在集群部署服务，需要暴露端口，。考虑侵入性的问题，建议优先使用 桌面端。

## 博文参考

***

https://github.com/headlamp-k8s/headlamp

https://headlamp.dev/

https://headlamp.dev/docs/latest/installation/