---
title: K8s：KUI一款炫酷的命令行交互式k8s管理工具
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: "2023-02-18 01:27:12/K8s:KUI一款炫酷的命令行交互式k8s管理工具.html"
mathJax: false
date: 2023-02-18 09:27:12
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->

## 写在前面

---

- 对于钟爱命令行操作的小伙伴
- 个人觉得` Kui `是很不错的工具
- 博文内容涉及： `Kui` 下载安装，使用 Demo
- 理解不足小伙伴帮忙指正

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

---

`Kui` 是一个开源的桌面端工具，用于提供通过命名行管理 k8s 集群的一个交互式环境。在 Kui 中，不需要复制和粘贴自动生成的长资源名称来进行资源详细信息查看，可以直接处理 命令行生成的图形数据。

![](https://img-blog.csdnimg.cn/d40fddfd13bb483d9dc8af149c5f6768.png)

通过 Kui ，可以极大提高 kubectl 的管理效率，并且增强了命令行体验，而且速度也很快。它在 1-2 秒内启动，`处理标准 kubectl 命令的 速度比自身快 2-3 倍`

同时， Kui 很轻量，对资源消耗很小，在可以接受的范围内。下载安装

https://github.com/kubernetes-sigs/kui/releases/tag/v13.1.0

### Window 环境安装

下载解压，需要双击之后点更多信息

![](https://img-blog.csdnimg.cn/9bbf5820d23b439497b59ee296df4fef.png)

点击仍要运行

![](https://img-blog.csdnimg.cn/83fd67f2f44a41cc901f48d35651cb7f.png)

在当前用户的家目录下 需要提前准备好 kubeconfig 文件

```bash
PS C:\Users\山河已无恙\.kube> ls

    目录: C:\Users\山河已无恙\.kube

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2023/1/14      7:48                cache
d-----         2023/2/13      4:48                http-cache
-a----         2023/1/27     20:27           5641 config

PS C:\Users\山河已无恙\.kube> cat .\config
apiVersion: v1
clusters:
- cluster:
.........................
```

如果可以看到这个，说明安装成功，并且在最下面可以看到集群，上下文，命名空间信息，说明连接成功

![](https://img-blog.csdnimg.cn/64e0a0533e2049fab7a62d8a147f5512.png)

### 常用功能 Demo

查看所有命名空间，下钻查看 某个命名空间详细信息

![](https://img-blog.csdnimg.cn/1eaac84dae564c48adf5e604c6ae859c.png)

对 命名空间的 YAML 文件做 CRUD 操作，通过 `Kui` 可以对集群 任何资源文件做 CRUD 操作。

![](https://img-blog.csdnimg.cn/b1d5e452057645b4bc4df81458bef32d.png)

查看所有 Pod 信息

![](https://img-blog.csdnimg.cn/2a230f13930a4529ab3374cdb7c05bca.png)

更换展示方式，当资源较多时，可以改变展现的形式。通过窗格展示，方便操作

![](https://img-blog.csdnimg.cn/15425095c5ea424ea0840e1bd510dc98.png)

下钻查看 pod 详细信息

![](https://img-blog.csdnimg.cn/54578be2885047d5a67a555f22fb6532.png)

也可以直接查看 Pod 详细信息

![](https://img-blog.csdnimg.cn/882791451bad4d8ebb2b5f4d62d799f7.png)

job 信息查看下钻

![](https://img-blog.csdnimg.cn/9d865ab943c445e3bed5f995a783e55a.png)

deploy 信息查看

![](https://img-blog.csdnimg.cn/c7dbe04d2544466c9ac15f825972f96c.png)

支持多个 tab 页，以及 当前 tab 页分屏操作操作

![](https://img-blog.csdnimg.cn/e0ac276b19ae4129b3bda5d4fcd289a4.png)

支持集群命名空间切换

![](https://img-blog.csdnimg.cn/3a0ea7824a8c4d5896a405eb11bd1af6.png)

关于 KUI 就和小伙伴们分享到这个，感兴趣的小伙伴赶快去尝试吧。

## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

---

https://kui.tools/

https://github.com/kubernetes-sigs/kui

---

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)

