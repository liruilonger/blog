---
title: 关于Kubernetes 桌面客户端 Aptakube  的一些笔记整理
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Aptakube
uniqueId: '2023-01-14 00:30:59/关于Kubernetes 桌面客户端IDE Aptakube  的一些笔记整理.html'
mathJax: false
date: 2023-01-14 08:30:59
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 分享一个 k8s 桌面客户端 `Aptakube`
+ 当前这不是一个开源的产品，现在需要付费，最初是开源的
+ 这里简单了解下
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

`Aptakube` 是一个轻量级的多集群 `Kubernetes` 桌面客户端。通过 Aptakube，可以查看一个或多个 Kubernetes 集群的资源，查看 pod 和节点健康状态、日志等

`Aptakube` 可以做什么：
+ 同时连接到一个或多个集群
+ 聚合日志查看器
+ 人性化的资源视图
+ 查看和修改对象
+ 零配置设置
+ 不是另一个 Electron 应用程序
+ 适用于任何 Kubernetes 集群：本地、GKE、EKS、AKS 等。
+ 适用于 Windows、macOS 和 Linux

![在这里插入图片描述](https://img-blog.csdnimg.cn/5c6bf845d5094069bd0e6325ff8c93db.png)


`Aptakube` 的官方地址： https://aptakube.com/

如果你需要安装它，在它的官网地址就可以直接下载安装包，安装完的打开的初始状态是这样的

![在这里插入图片描述](https://img-blog.csdnimg.cn/1ba424ab4da4413cb1be9d26f0ded2cf.png)

连接集群需要拷贝 kubeconfig 文件到 当前 window 机器的位置。
```bash
PS C:\Users\山河已无恙\.kube> ls

    目录: C:\Users\山河已无恙\.kube

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2023/1/14      7:48                cache
-a----         2023/1/13     22:05           5579 config
PS C:\Users\山河已无恙\.kube>
```
配置完 kubeconfig 文件，会自动识别，直接导入即可

![在这里插入图片描述](https://img-blog.csdnimg.cn/b32119cb8f1440a79476507f3edaa066.png)

可以看到常用的 API 资源信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/ae5a95e54ed6463a839a7a570dd11737.png)

有些内容看不了，应该是需要付费的问题。

![在这里插入图片描述](https://img-blog.csdnimg.cn/9eb7d9dca7be467fb4b015251fb096fc.png)

嗯，关于 `Aptakube` ,如果希望选择轻量一点，和 Aptakube 功能类似的 k8s 面板工具，开源的工具有很多，可以选择 openLens , k9s ,kubebox, Headlamp 等等，当然，大多数是开源的。`Aptakube`这里仅作为一种了解。
## 博文参考

***

https://aptakube.com/

https://github.com/aptakube/aptakube