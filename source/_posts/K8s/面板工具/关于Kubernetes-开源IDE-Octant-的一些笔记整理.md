---
title: Kubernetes:分享一个可以展示资源视图的 K8s开源 Web/桌面 客户端工具 Octant
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Octant
uniqueId: '2023-01-19 00:28:43/Kubernetes:可以展示资源视图的 K8s开源 Web/桌面 客户端工具 Octant.html'
mathJax: false
date: 2023-01-19 08:28:43
thumbnail:
---


**<font color="009688">其实当你什么都经历了，会发现，人生无论你怎么精心策划都抵不过一场命运的安排。**</font>

<!-- more -->
## 写在前面

***
+ 博文内容主要为 `Octant` 介绍以及 `桌面/Web` 端的安装教程
+ 涉及 `Linux/Windows` 的安装。
+ 理解不足小伙伴帮忙指正


**<font color="009688">其实当你什么都经历了，会发现，人生无论你怎么精心策划都抵不过一场命运的安排。**</font>

***

Octant 不是仪表板，Octant 是一个带有仪表板视图的平台， 需要说明的时，这个项目有近 10 个月没有维护，不过对于 k8s 工具来讲，我认为它是不次于 Lens 的项目，并且它开源，Lens 要收费了，如果考虑费用问题，我认为这是一个不错的选择。 Octant 可以展示 当前资源的 依赖视图，这对于可以直观的看到 API 资源的依赖关系是很有帮助的。

Octant 提供了一种理解复杂 Kubernetes 环境的新方法。它可以部署为 Web ，通过浏览器客户端访问，也可以以桌面的方法来安装，你需要的仅仅是一个做了授权的 `kubeconfig` 文件.

对于类似的 k8s 工具，在生产中， `Octant` 结合 `Rancher` 可以满足大部分需求。  如果你的访问权限受限，只有一个终端，推荐使用 `kubebox` 或者 `k8s` .


### 特征:

+ `资源查看器` ： 以图形方式可视化 Kubernetes 集群中对象之间的关系。各个对象的状态用颜色表示以显示工作负载性能。
+ `摘要视图` ： 从通常使用多个 kubectl 命令找到的输出中聚合的单个页面中的合并状态和配置信息。
+ `端口转发`: 使用用于调试应用程序的单个按钮将本地端口转发到正在运行的 pod，甚至跨命名空间端口转发多个 pod。
+ `日志流`: 查看 Pod 和容器活动的日志流，以便在不打开多个终端的情况下进行故障排除或监控。
+ `标签过滤器`: 使用标签过滤组织工作负载，以检查命名空间中包含大量对象的集群。
+ `集群导航`:在不同集群的命名空间或上下文之间轻松更改。还支持多个 kubeconfig 文件。
+ `插件系统`: 高度可扩展的插件系统，供用户通过 gRPC 提供额外的功能。插件作者可以在现有视图之上添加组件。


### 桌面端安装

#### window安装

```bash
PS C:\Users\山河已无恙\Downloads> curl -o Octant.Setup.0.25.1.exe  https://github.com/vmware-tanzu/octant/releases/download/v0.25.1/Octant.Setup.0.25.1.exe

```
下载好之后安装，之后桌面上会有一个图标

![在这里插入图片描述](https://img-blog.csdnimg.cn/560626217ebc4f63a1599853969c854f.png)

点击启动它，第一次打开会提示输入 kubeconfig 文件。

![在这里插入图片描述](https://img-blog.csdnimg.cn/0dbf43b31c8e4d1ebb26e08eca4f87f9.png)

```bash
┌──[root@vms81.liruilongs.github.io]-[/var/run]
└─$cat ~/.kube/config
```
然后点击加载，就可以查看集群信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/ee4a2ec5b9f949a0aba0fe027fd33b97.png)

哈，可能我机器 CPU 不行，感觉桌面端很耗资源。

![在这里插入图片描述](https://img-blog.csdnimg.cn/49907d5395a04a47805ae0ee7255a6f5.png)


## web 端安装

### window安装


```bash
PS C:\Users\山河已无恙> choco install octant --confirm --force
Chocolatey v1.1.0
Installing the following packages:
octant
By installing, you accept licenses for the packages.
octant v0.25.1 already installed. Forcing reinstall of version '0.25.1'.
 Please use upgrade if you meant to upgrade to a new version.

octant v0.25.1 (forced) [Approved]
octant package files install completed. Performing other installation steps.
Using system proxy server '127.0.0.1:49739'.
Downloading octant 64 bit
  from 'https://github.com/vmware-tanzu/octant/releases/download/v0.25.1/octant_0.25.1_Windows-64bit.zip'
Using system proxy server '127.0.0.1:49739'.
Progress: 100% - Completed download of C:\Users\山河已无恙\AppData\Local\Temp\chocolatey\octant\0.25.1\octant_0.25.1_Windows-64bit.zip (54.37 MB).
Download of octant_0.25.1_Windows-64bit.zip (54.37 MB) completed.
Hashes match.
Extracting C:\Users\山河已无恙\AppData\Local\Temp\chocolatey\octant\0.25.1\octant_0.25.1_Windows-64bit.zip to C:\ProgramData\chocolatey\lib\octant\tools...
C:\ProgramData\chocolatey\lib\octant\tools
 ShimGen has successfully created a shim for octant.exe
 The install of octant was successful.
  Software installed to 'C:\ProgramData\chocolatey\lib\octant\tools'

Chocolatey installed 1/1 packages.
 See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log).

Enjoy using Chocolatey? Explore more amazing features to take your
experience to the next level at
 https://chocolatey.org/compare
PS C:\Users\山河已无恙>
```

启动运行

```bash
PS C:\ProgramData\chocolatey\lib\octant\tools\octant_0.25.1_Windows-64bit> .\octant.exe
2023-01-14T06:12:55.534-0500    INFO    dash/dash.go:637        cannot find kube config: C:\Users\山河
已无恙\.kube\config
2023-01-14T06:12:55.534-0500    INFO    dash/dash.go:134        no valid kube config found, initializing loading API
2023-01-14T06:12:55.536-0500    INFO    dash/dash.go:154        waiting for kube config ...
2023-01-14T06:12:55.577-0500    INFO    dash/dash.go:546        Dashboard is available at http://127.0.0.1:7777
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/cb9455f6023d442e9f688e97cfe57e0b.png)

输入 kubeconfig 文件信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/5956297933564f49b541ac10c3f4bd2c.png)

可以更换主题，查看集群信息。
![在这里插入图片描述](https://img-blog.csdnimg.cn/59fbee88c3ba42119beee22a97be716d.png)



### Linux 下安装

下载安装包
```bash
curl -o octant_0.25.1_Linux-64bit.tar.gz  https://github.com/vmware-tanzu/octant/releases/download/v0.25.1/octant_0.25.1_Linux-64bit.tar.gz
```
解压查看版本信息。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/octant]
└─$tar -zxvf octant_0.25.1_Linux-64bit.tar.gz
octant_0.25.1_Linux-64bit/README.md
octant_0.25.1_Linux-64bit/octant
┌──[root@vms81.liruilongs.github.io]-[~/ansible/octant]
└─$octant_0.25.1_Linux-64bit/octant version
Version:  0.25.1
Git commit:  f16cbb951905f1f8549469dfc116ca16cf679d46
Built:  2022-02-24T23:02:15Z
```

查看帮助文档了解启动信息

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/octant]
└─$octant_0.25.1_Linux-64bit/octant --help
octant is a dashboard for high bandwidth cluster analysis operations

Usage:
  octant [flags]
  octant [command]

Available Commands:
  completion  generate the autocompletion script for the specified shell
  help        Help about any command
  version     Show version

Flags:
      --context string                 initial context
      --disable-cluster-overview       disable cluster overview
      --enable-feature-applications    enable applications feature
      --kubeconfig string              absolute path to kubeConfig file
  -n, --namespace string               initial namespace
      --namespace-list strings         a list of namespaces to use on start
      --plugin-path string             plugin path
  -v, --verbose                        turn on debug logging
      --client-max-recv-msg-size int   client max receiver message size (default 16777216)
      --accepted-hosts string          accepted hosts list [DEV]
      --client-qps float32             maximum QPS for client [DEV] (default 200)
      --client-burst int               maximum burst for client throttle [DEV] (default 400)
      --disable-open-browser           disable automatic launching of the browser [DEV]
      --disable-origin-check           disable cross origin resource check
  -c, --enable-opencensus              enable open census [DEV]
      --klog-verbosity int             klog verbosity level [DEV]
      --listener-addr string           listener address for the octant frontend [DEV]
      --local-content string           local content path [DEV]
      --proxy-frontend string          url to send frontend request to [DEV]
      --ui-url string                  dashboard url [DEV]
      --browser-path string            the browser path to open the browser on
      --memstats string                log memory usage to this file
      --meminterval string             interval to poll memory usage (requires --memstats), valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h". (default "100ms")
  -h, --help                           help for octant

Use "octant [command] --help" for more information about a command.
```
这里在启动的时候需要注意一下，如果终端没有浏览器，需要指定 `--disable-open-browser`,同时如果是 其他机器通过IP访问，需要添加监听的任意IP对端口的访问 `--listener-addr 0.0.0.0:7777`， 默认情况下， Octant 会在启动时候自动打开默认浏览器，并且只监听本地对 指定端口的访问。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/octant/octant_0.25.1_Linux-64bit]
└─$./octant --kubeconfig ~/.kube/config --disable-open-browser --disable-origin-check --listener-addr 0.0.0.0:7777
2023-01-14T20:05:20.673+0800    INFO    dash/watcher.go:117     watching config file    {"component": "config-watcher", "config": "/root/.kube/config"}
.............
2023-01-14T20:05:20.677+0800    INFO    module/manager.go:87    registering action      {"component": "module-manager", "actionPath": "action.octant.dev/deleteObject", "module-name": "configuration"}
2023-01-14T20:05:20.677+0800    WARN    plugin/manager.go:405   Unable to add /root/.config/octant/plugins to the plugin watcher. Error: no such file or directory

github.com/vmware-tanzu/octant/pkg/plugin.(*Manager).watchPluginFiles
        /__w/octant/octant/pkg/plugin/manager.go:405
2023-01-14T20:05:20.691+0800    INFO    dash/dash.go:546        Dashboard is available at http://[::]:7777
```

其他机器测试。可以直接进入当前 pod  的内部环境

![在这里插入图片描述](https://img-blog.csdnimg.cn/caf179b01d9c43d7be26a1ac2afcf31e.png)

查看日志信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/729af7d5f1fb4d92b940bd73ee329e3d.png)

查看资源依赖关系

![在这里插入图片描述](https://img-blog.csdnimg.cn/1c668242238e434aaaa153f49c1719a2.png)

不对不说，octant 很消耗资源，尤其是对CPU，所以不太建议部署到集群相关的节点。

![在这里插入图片描述](https://img-blog.csdnimg.cn/2886c36d5fdf47b88172c58b1baa0442.png)

关于 octant 和小伙伴分享到这里，安装很简单，赶快去尝试吧。

## 博文参考

***

https://octant.dev/

https://github.com/vmware-tanzu/octant

https://reference.octant.dev/?path=/story/docs-intro--page