---
title: Kubernetes:基于命名行终端/ Web 控制台的管理工具 kubebox  
tags:
  - Kubernetes 
categories:
  - Kubernetes 
toc: true
recommend: 1
keywords: kubebox
uniqueId: '2023-01-21 01:23:12/Kubernetes:基于命名行终端&& Web 控制台的管理工具 kubebox.html'
mathJax: false
date: 2023-01-21 09:23:12
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ `kubebox` 是一个轻量的 k8s 管理工具，可以基于命令行终端或 Web 端
+ 博文内容涉及：`kubebox` 不同方式的安装下载，简单使用。
+ 如果希望轻量一点，个人很推荐这个工具，轻量，而且使用简单。
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***
## 简单介绍

`Kubebox` 是一个类似 `k9s` 的基于命令行终端的 `k8s` 管理工具，可以直接下载可执行文件到 `windows` 或者 `Linux` 上通过命令行运行，也可以部署为 Web 端，通过 `Kubernetes` 集群中托管的服务提供服务。通过浏览器访问，终端仿真由 `Xterm.js` 提供，与 `Kubernetes` 主 API 的通信由服务器代理。



kubebox 的特性：
+ 交互式切换上下文
+ 身份验证支持(承载令牌、基本身份验证、私钥/证书、OAuth、OpenID Connect、Amazon EKS、Google Kubernetes Engine、Digital Ocean)
+ 命名空间选择和 pod 列表观察
+ 容器日志滚动/观看
+ 容器资源使用情况(内存、CPU、网络、文件系统图表)[ 1 ]
+ 容器远程执行终端
+ 集群、命名空间、pod 事件
+ 对象配置编辑器和 CRUD 操作
+ 集群和节点视图/监控


客户端部署需要提供 `kubeconfig` 文件的配置(KUBECONFIG环境变量或$HOME/.kube)



## 下载安装

### 桌面终端客户端安装

这里在 Windows 下安装

```bash
PS C:\Program Files>  curl -o kubebox.exe https://github.com/astefanutti/kubebox/releases/download/v0.10.0/kubebox-windows.exe
PS C:\Program Files> .\kubebox.exe
```

需要准备好 kubeconfig 文佳

```bash
PS C:\Users\山河已无恙\.kube> ls


    目录: C:\Users\山河已无恙\.kube


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2023/1/14      7:48                cache
-a----         2023/1/14     10:19           5682 config
PS C:\Users\山河已无恙\.kube> cat .\config
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data:
    .......................
```
下载好可以通过命令行，或者双击启动，选择 Pod 可以查看 日志信息。

![在这里插入图片描述](https://img-blog.csdnimg.cn/4157435fafaf462eb3a0d5a320e55f31.png)

资源没有展示，需要安装 `cadvisor`  以 `DS` 的方式

```bash
$ kubectl apply -f https://raw.githubusercontent.com/astefanutti/kubebox/master/cadvisor.yaml
```

如果没有科学上网，需要替换镜像
```bash
- image: docker.io/google/cadvisor:v0.33.0     
```

安装 `cadvisor` , `kubebox` 的资源数据依赖于它
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/skooner]
└─$kubectl apply  -f cadvisor.yaml
namespace/cadvisor created
serviceaccount/cadvisor created
Warning: policy/v1beta1 PodSecurityPolicy is deprecated in v1.21+, unavailable in v1.25+
podsecuritypolicy.policy/cadvisor created
clusterrole.rbac.authorization.k8s.io/cadvisor created
clusterrolebinding.rbac.authorization.k8s.io/cadvisor created
daemonset.apps/cadvisor created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/skooner]
└─$kubectl get pods -n cadvisor
NAME             READY   STATUS    RESTARTS   AGE
cadvisor-256sp   1/1     Running   0          29s
cadvisor-5d42t   1/1     Running   0          29s
cadvisor-6sx5r   1/1     Running   0          29s
cadvisor-k2tv4   1/1     Running   0          29s
cadvisor-szxlj   1/1     Running   0          29s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/skooner]
└─$kubectl get all -n cadvisor
NAME                 READY   STATUS    RESTARTS   AGE
pod/cadvisor-256sp   1/1     Running   0          38s
pod/cadvisor-5d42t   1/1     Running   0          38s
pod/cadvisor-6sx5r   1/1     Running   0          38s
pod/cadvisor-k2tv4   1/1     Running   0          38s
pod/cadvisor-szxlj   1/1     Running   0          38s

NAME                      DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/cadvisor   5         5         5       5            5           <none>          38s
```

安装之后可以正常展示计算资源信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/761005abe20b40afbb3a0431757bae39.png)

可以查看CPU，网络，内存，文件系统相关的数据统计。

![在这里插入图片描述](https://img-blog.csdnimg.cn/d63fdeba973146d386e36459642bbcd3.png)


查看集群事件

![在这里插入图片描述](https://img-blog.csdnimg.cn/8f896f2e92f145ab827ff4f68a9eb821.png)



### 集群 web 端安装

在当前的 k8s 集群安装
```bash
kubectl apply -f https://raw.github.com/astefanutti/kubebox/master/kubernetes.yaml
```

yaml 文件还需要处理下，这里我们把 Ingress 的部分删掉， svc 修改为 `type： NodePort` ,如果小伙伴的 Ingress  控制器可以用可以忽略

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubebox]
└─$kubectl apply  -f kubernetes-kubebox.yaml
namespace/kubebox created
service/kubebox created
deployment.apps/kubebox created
serviceaccount/kubebox created
clusterrolebinding.rbac.authorization.k8s.io/kubebox created
clusterrole.rbac.authorization.k8s.io/kubebox created
```

创建的资源信息

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubebox]
└─$kubectl get all -n kubebox
NAME                           READY   STATUS    RESTARTS   AGE
pod/kubebox-6bc5fbdb49-tpn5d   1/1     Running   0          3m28s

NAME              TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
service/kubebox   NodePort   10.102.154.145   <none>        8080:32138/TCP   3m29s

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/kubebox   1/1     1            1           3m28s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/kubebox-6bc5fbdb49   1         1         1       3m28s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/kubebox]
└─$
```

浏览器范围 http://192.168.26.81:32138/

命令空间选择

![在这里插入图片描述](https://img-blog.csdnimg.cn/014a35f695cf4c17a135f8b6a239c7b0.png)

可以查看日志信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/25649a8f25aa4609864e4caf6c57fc6a.png)

可以直接到 pod 内部执行远程 shell

![在这里插入图片描述](https://img-blog.csdnimg.cn/ecb8b6c2732b4c50bf2c221c357dac5c.png)


### 快捷按键

<div dir="auto">
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">按键绑定</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">描述</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="2"><p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一般的</font></font></em></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>l</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, </font></font><span><kbd>Ctrl</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+</font></font><kbd>l</kbd></span></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">登录</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>n</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更改当前命名空间</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[ </font></font><kbd>Shift</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+] </font></font><kbd>←</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, </font></font><kbd>→</kbd><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
[ </font></font><kbd>Alt</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+] </font></font><kbd>1</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, … ,</font></font><kbd>9</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">导航屏幕</font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
(使用</font></font><kbd>Shift</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><kbd>Alt</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 exec 终端内)</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>Tab</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, </font></font><span><kbd>Shift</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+</font></font><kbd>Tab</kbd></span></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在活动屏幕内更改焦点</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>↑</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">,</font></font><kbd>↓</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">导航列表/表单/日志</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>PgUp</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">,</font></font><kbd>PgDn</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向上/向下移动一页</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>Enter</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">选择项目/提交表格</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>Esc</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关闭模态窗口/取消表单</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><span><kbd>Ctrl</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+</font></font><kbd>z</kbd></span></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">关闭当前屏幕</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>q</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">, </font></font><span><kbd>Ctrl</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+</font></font><kbd>q</kbd></span></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">退出</font></font><sup><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[ </font></font><a id="user-content-_footnoteref_3" href="#_footnotedef_3" title="查看脚注。"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ]</font></font></sup></p></td>
</tr>
<tr>
<td colspan="2"><p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">登录</font></font></em></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>←</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">,</font></font><kbd>→</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">导航 Kube 配置</font></font></p></td>
</tr>
<tr>
<td colspan="2"><p dir="auto"><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">豆荚</font></font></em></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>Enter</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">选择吊舱/循环容器</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>r</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">远程shell进入容器</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>m</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">内存使用情况</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>c</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CPU使用率</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>t</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网络使用</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>f</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文件系统使用</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><kbd>e</kbd></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pod 事件</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><span><kbd>Shift</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+</font></font><kbd>e</kbd></span></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">命名空间事件</font></font></p></td>
</tr>
<tr>
<td><p dir="auto"><span><kbd>Ctrl</kbd><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">+</font></font><kbd>e</kbd></span></p></td>
<td><p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">集群事件</font></font></p></td>
</tr>
</tbody>
</table>
</div>



## 博文参考

***

https://github.com/astefanutti/kubebox
