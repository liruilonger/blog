---
title: K8s:Windows 下安装 K8s 桌面端面板工具 OpenLens 并添加集群
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: OpenLens
uniqueId: '2023-01-12 18:33:21/K8s:Windows 下安装 Kubernetes 开源IDE OpenLens 并添加集群.html'
mathJax: false
date: 2023-01-13 02:33:21
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 分享一个桌面端的 k8s 面板工具 `OpenLens`
+ 博文内容为 `OpenLens` 简单介绍和 下载安装教程。
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

### 简单介绍
Lens 是一个基于桌面端的 k8s IDE，可在 Linux、macOS 和 Windows 上使用，类似于`dashboard、Kuboard`,`Lens` 可以提高集群资源的可见性、实时统计数据、日志流和动手故障排除功能。能够快速轻松地使用您的集群，从根本上提高生产力和业务速度。

某种角度上考虑，Lens  是非侵入的，不需要在集群上做任何操作，类似 `kubectl ` 一样，不像 `dashboard、Kuboard` 等面板工具，基于 Web 端，需要在集群部署相关的服务。 

从 2023 年 1 月开始，Lens 需要选择付费版还是免费版，如果从官网下载，需要激活。但是我们可以使用 `OpenLens `

`OpenLens `是一个开源项目，支持 Lens 主要功能。该代码由 Lens 团队开发人员与社区一起开发，截至目前，它保持免费。

`Lens` 建立在 `OpenLens` 项目之上，类似 `Ansible Tower` 和 `AWX` 的关系，包括一些具有不同许可证的附加软件和库, Lens 的核心功能将在 Openlens 中可用。

关于 `OpenLens`, 可以到 github 上了解更多 

[https://github.com/MuhammedKalkan/OpenLens](https://github.com/MuhammedKalkan/OpenLens)


### 下载安装 OpenLens

windows 机器上通过命令行下载安装。

![在这里插入图片描述](https://img-blog.csdnimg.cn/5c996538d1054d91866d909cefd18e13.png)

```bash
PS C:\Users\山河已无恙\Downloads> winget install openlens --source winget
已找到 OpenLens [MuhammedKalkan.OpenLens] 版本 6.3.0
此应用程序由其所有者授权给你。
Microsoft 对第三方程序包概不负责，也不向第三方程序包授予任何许可证。
Downloading https://github.com/MuhammedKalkan/OpenLens/releases/download/v6.3.0/OpenLens-6.3.0.exe
  ██████████████████████████████   117 MB /  117 MB
已成功验证安装程序哈希
正在启动程序包安装...
已成功安装
PS C:\Users\山河已无恙\Downloads>
```

安装好之后双击启动它

![在这里插入图片描述](https://img-blog.csdnimg.cn/181c03c8254d4f9f83e2c39a43f95790.png)

### 添加集群

添加集群，在这之前，你需要确保 你的客户机和集群的网络是通的。
![在这里插入图片描述](https://img-blog.csdnimg.cn/a6b77345222e4dd0a96620c16b84f0bd.png)

在打开的页面中，拷贝 kubeconfig 文件内容到指定位置

![在这里插入图片描述](https://img-blog.csdnimg.cn/e4fdb77a488441a488b47e8739078a16.png)

集群机器上查看 kubeconfig 文件

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cat ~/.kube/config
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: LS0tLS1CRUdJTiBDRVJUSUZJ.....S0tLS0tCg==
    server: https://192.168.26.81:6443
  name: kubernetes
contexts:
- context:
    cluster: kubernetes
    namespace: liruilong-topo-namespace
    user: kubernetes-admin
  name: kubernetes-admin@kubernetes
current-context: kubernetes-admin@kubernetes
kind: Config
preferences: {}
users:
- name: kubernetes-admin
  user:
    client-certificate-data: LS0tLS1CRUdJTiBDRVJUSUZJQ0......JUSUZJQ0FURS0tLS0tCg==
    client-key-data: LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS.......FIEtFWS0tLS0tCg==
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
### 简单测试查看

**查看集群节点信息**

![在这里插入图片描述](https://img-blog.csdnimg.cn/165a9fcb7a514844be9dc40ee2df8bb9.png)

**查看 pod 信息**

![在这里插入图片描述](https://img-blog.csdnimg.cn/2b829c324af34592a37514b3d1e26453.png)

**查看运行的 helm releases**

![在这里插入图片描述](https://img-blog.csdnimg.cn/ca14852ed1934e2e9b86864b26cdaf97.png)

**可以直接连接 集群终端进行命令操作**

![在这里插入图片描述](https://img-blog.csdnimg.cn/8cf003ee3a884d86a6470a602e71126d.png)

## 博文参考

***
https://docs.k8slens.dev/

https://github.com/MuhammedKalkan/OpenLens