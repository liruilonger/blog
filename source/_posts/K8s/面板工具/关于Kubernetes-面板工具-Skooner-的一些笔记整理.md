---
title: Kubernetes:分享一个很简洁的 k8s 管理工具 Skooner 
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Skooner
uniqueId: '2023-01-17 01:50:15/关于Kubernetes 面板工具 Skooner  的一些笔记整理.html'
mathJax: false
date: 2023-01-17 09:50:15
thumbnail:

---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 博文内容为 `Skooner` 简单介绍
+ 包括下载安装导入集群基本功能使用
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

### 简单介绍

`Skooner` 之前的名字叫做 `k8dash` ，现在修改为 Skooner， Skooner 是用来管理 Kubernetes 集群的一个工具。现在是Cloud Native Computing Foundation的一个沙盒项目！在功能是使用上有些类似 于 `Headlamp` .

Skooner 的特性：
+ 完整的集群管理：命名空间、节点、Pod、副本集、部署、存储、RBAC 等
+ 速度极快且永远在线：无需刷新页面即可查看最新的集群状态
+ 一目了然地快速可视化集群健康状况：实时图表有助于快速追踪性能不佳的资源
+ 简单的 CRUD 和缩放：加上内联 API 文档以轻松理解每个字段的作用
+ 100% 响应(在您的手机/平板电脑上运行)
+ 简单的 OpenID 集成：不需要特殊的代理
+ 简单安装：使用提供的 yaml 资源在 1 分钟内启动并运行 skooner(不，说真的)


### 下载安装

```bash
┌──[root@liruilongs.github.io]-[~/.kube]
└─$curl -o kubernetes-skooner.yaml  https://raw.githubusercontent.com/skooner-k8s/skooner/master/kubernetes-skooner.yaml
```
YAML 文件拉取下来需要做简单修改。这里不使用 Ingress ，svc 设置为 type: NodePort, 谷歌镜像国内拉不下来，所以换成 docker 的。
```bash
        #image: ghcr.io/skooner-k8s/skooner:stable
        image: docker.io/ymuski/skooner:latest
```
然后直接部署

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/skooner]
└─$kubectl apply  -f kubernetes-skooner.yaml
deployment.apps/skooner created
service/skooner created
```

skooner 由一个 deploy 和一个 svc 构成
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/skooner]
└─$kubectl get svc skooner
NAME      TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)        AGE
skooner   NodePort   10.108.72.51   <none>        80:32334/TCP   9m
```

浏览器访问： http://192.168.26.81:32334/#!。 登录需要 tocker。


![在这里插入图片描述](https://img-blog.csdnimg.cn/76d4ee8e0ab54e919857a814db9fd0b4.png)


对应的 pod 默认使用的 default  的 SA，所以我们直接获取  defaule 的sa 对应的 token 来登录。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/skooner]
└─$kubectl get pods skooner-5b65f884f8-9cs4k -o json |  jq  .spec.serviceAccount
"default"
```
这里默认情况下 么有授权。我们需要给 default 的 sa 授权。
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/skooner]
└─$kubectl create clusterrolebinding default --clusterrole=cluster-admin --serviceaccount=default:default
clusterrolebinding.rbac.authorization.k8s.io/default created
```
当然也可以创建新的 sa
```bash
# Create the service account in the current namespace (we assume default)
kubectl create serviceaccount skooner-sa

# Give that service account root on the cluster
kubectl create clusterrolebinding skooner-sa --clusterrole=cluster-admin --serviceaccount=default:skooner-sa

# Find the secret that was created to hold the token for the SA
kubectl get secrets

# Show the contents of the secret to extract the token
kubectl describe secret skooner-sa-token-xxxxx

```

获取 tocker 
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/skooner]
└─$kubectl describe secrets  default-token-xg77h
Name:         default-token-xg77h
Namespace:    kube-system
Labels:       <none>
Annotations:  field.cattle.io/projectId: local:p-f4zjg
              kubernetes.io/service-account.name: default
              kubernetes.io/service-account.uid: ea334cb4-6824-4186-a69f-3272c5d232e3

Type:  kubernetes.io/service-account-token

Data
====
namespace:  11 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6ImF2MmJVZ3d6M21JRC1BZUwwaHlDdzZHSGNyaVJON1BkUHF6MlhPV2NfX00ifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLXhnNzdoIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJlYTMzNGNiNC02ODI0LTQxODYtYTY5Zi0zMjcyYzVkMjMyZTMiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06ZGVmYXVsdCJ9.L_3xXrmistiAJ9G-BKETpH3wHejd8bnc5uPskHgF-oXefNNJb6OXLisX0gdrPJe33dVl0w84N6nXB_EOtbc0yvGwN9my60PS08smoGiz2lxJ-x0R8EtvI8lNKMWnc7R8Kmt4I_3aFUQs3XIL2M4Kfnb3wCFu-OaqRwpyi2qkxTBMAhdWckUUt1OgzYklf96W2wfpcpYEXoIK1qVyfq8l9zJyjZ8HCX4lHbEmHh1h-Mxng7lRPnGbgi9tFdZzsm5mB4xPIC6RWnBmkXBGL2gZcCdhUE2eidkWBlN5DfbF_qKzgyC-qrY4IB1WzkyUgigKczzh56QiUrlUbTUGMIGl6Q
ca.crt:     1099 bytes
```
登录 skooner ，查看某一命名空间资源信息



![在这里插入图片描述](https://img-blog.csdnimg.cn/e3e9539198894a15b5c79c9760a9a58f.png)

查看集群信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/07593b3312814b3a96b799b04e800bd0.png)


关于  Skooner 就和小伙伴们分享到这个，感兴趣的小伙伴可以去试试。 

## 博文参考

***


https://github.com/presidenten/skooner