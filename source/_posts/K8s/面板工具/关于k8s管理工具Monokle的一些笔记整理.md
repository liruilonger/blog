---
title: K8s:Monokle Desktop 一个集Yaml资源编写、项目管理、集群管理的 K8s IDE
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Monokle
uniqueId: "2023-02-21 16:22:39/关于k8s管理工具Monokle的一些笔记整理.html"
mathJax: false
date: 2023-02-22 00:22:39
thumbnail:

---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->

## 写在前面

---

- `Monokle Desktop` 是  `kubeshop` 推出的一个开源的 `K8s IDE`
- 相关项目还有 `Monokle CLI` 和 `Monokle Cloud`
- 相比其他的工具，`Monokle Desktop` 功能较全面，涉及 `k8s` 管理的整个生命周期
- 博文内容：`Monokle Desktop` 下载安装，项目管理，集群管理的简单介绍
- 理解不足小伙伴帮忙指正

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

---

`Monokle Desktop` 是一个 `100%开源` 的 `K8s` 可视化工具，可用于 编写、分析和部署 Kubernetes 配置，包括从创建到验证再到部署的整个配置生命周期。

![在这里插入图片描述](https://img-blog.csdnimg.cn/305220bb7a474830b9a123c06420c3ff.png)


在官方文档中, `Monokle Desktop` 这样介绍自己：

- 快速了解你的清单、其包含的资源和关系的高层视图。
- 利用 Git 来管理你的配置的生命周期
- 使用 OPA 策略规则验证资源
- 轻松地编辑资源，而无需学习或查找 yam 语法，并查看更改应用情况
- 重构资源，保持名称和引用的完整性
- 预览和调试用 `kustomize`或 `Helm`生成的资源
- 可视化 CRD 中定义的扩展资源
- 比较资源版本与你的集群，并立即或通过拉动请求应用修改。通过拉动请求应用修改
- 使用 Monokle 的模板系统创建多步骤表单，以快速生成 清单

### 下载安装

通过下面的路径下载,访问以获得最新版。

https://github.com/kubeshop/monokle/releases/

然后运行安装程序即可完成的在 windows 上的安装

安装之后需要提前配置好 `kubeconfig` 文件，位置为默认位置。

```bash
PS C:\Users\山河已无恙\.kube> ls

    目录: C:\Users\山河已无恙\.kube

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2023/1/14      7:48                cache
d-----         2023/2/13      4:48                http-cache
-a----         2023/1/27     20:27           5641 config

PS C:\Users\山河已无恙\.kube> cat .\config
apiVersion: v1
clusters:
- cluster:
.........................
```

之后便可以双击启动, 通过 `Monokle Desktop` 我们可以像使用 编码 IDE 一样，开发编写 YAML 资源文件，然后动态应用集群，开发测试，持续集成部署，同时可以对 YAML 文件做版本管理，也可以实时查看集群的状态信息。对于常用的资源可以通过 表单的方式生成。

所以在下面的介绍中，我们分两部分功能来介绍。

### 项目版本管理

Monokle 有四种不同的方式创建一个新的项目：

- 通过本地的静态文件目录构建一个空的项目
- 从头开始构建一个空的项目
- 通过模板构建一个项目
- 从 远程 git 仓库获取一个 项目

![在这里插入图片描述](https://img-blog.csdnimg.cn/8c91a0ab9d2c41fbb7f4af32a4bb9d6c.png)

这里选择第四个，导入了之前搭建 k8s HA 的相关资源的 一个远程 git 仓库

![在这里插入图片描述](https://img-blog.csdnimg.cn/b11a104bb38e432cb118c8bd0faa01a7.png)

导入之后，点击进入项目

![在这里插入图片描述](https://img-blog.csdnimg.cn/fbff2fb81c46436297624f8c0f641a9a.png)

Monokle 会解析当前项目，扫描整个项目目录，整理分析资源情况，整个项目的资源构成，按照 API 类型整理

![在这里插入图片描述](https://img-blog.csdnimg.cn/1c6e71f58f8149a49e77edf85f27330d.png)

同时会 对 Helm Chart ,Kustomize 管理的 YAML 资源文件进行扫描分析

![在这里插入图片描述](https://img-blog.csdnimg.cn/afee8ee9a13b4869a7fea373b751b12e.png)

可以通过 对应的 API 直接定位到对应的资源文件，也可以直接修改后应用资源文件

![在这里插入图片描述](https://img-blog.csdnimg.cn/545760462b624a26b136548c7413a2f4.png)

可以通过 diff 看观察集群当前应用资源和本地的静态 YANL 文件的差异

![在这里插入图片描述](https://img-blog.csdnimg.cn/0aed6beecfaf4fae89612e20783b7a57.png)

默认情况下，会对资源进行合规验证扫描，有异常的情况会做特殊标记，并提示相关信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/89dbfdc1795441d1a3c263919c72ce0a.png)

可以通过 `View validation errors` 页面查看详细信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/8682a84610be450b9a5dd706b3fefe98.png)

通过设置可以配置相关的验证规则

![在这里插入图片描述](https://img-blog.csdnimg.cn/0ce95deaf281495987f58aa8e7e641e1.png)

可用通过页面工具实现简单的项目 Git 版本管理

![在这里插入图片描述](https://img-blog.csdnimg.cn/f17629cb071b40f9b4b3a90627299c0c.png)

### 资源文件创建

`Monokle` 中，`YAML` 资源文件的创建，可以通过不同的方式，可以直接通过表单创建，通过关键的数据生成的一个 YAML 模板文件，也可以通过模板插件来实现。

#### 表单的方式生成

创建一个新的 pod 资源模板，通过表单填写一些值生成，并且放到 当前 项目 htlm 目录下，创建好之后我们可以对当前 YAML 资源模板进行修改。

![在这里插入图片描述](https://img-blog.csdnimg.cn/47af6444a8ac464883f37844f40bd0e2.png)

#### 模板插件的方式生成

默认情况下没有任何模板插件，可以编写好通过 git 仓库获取，或者通过 本地指定目录配置

![在这里插入图片描述](https://img-blog.csdnimg.cn/f492593663d5437db3307b2e2e5e6c06.png)

模板方式需要提前配置模板插件，需要下面四个文件:

- Monokle 模板配置
- 定义表单模式
- 定义表单 UI
- 带有占位符的 Kubernetes YAML 清单

通过仓库获取，需要编写好上传，这里我原本想用 官方的 Demo 来体验一下。但是不知道什么问题，一直导入不进去，时间关系没有研究

![在这里插入图片描述](https://img-blog.csdnimg.cn/4c3524134a1c41ae8794aa6f145e1cb9.png)

地址在这里，感兴趣小伙伴可以尝试

https://github.com/kubeshop/monokle-default-templates-plugin.git


### 集群管理

点击连接集群，可以直接进入集群管理页面

![在这里插入图片描述](https://img-blog.csdnimg.cn/fd126543ac144003ab044e601d7a4e46.png)

在集群管理页面可以查看集群的当中状态，包括对象资源，计算资源，以及对象资源的简要信息，状态，部分资源的监听信息、集群相关信息等。

![在这里插入图片描述](https://img-blog.csdnimg.cn/98e11c74a8c04b778be4542c28d6cf51.png)

可以直接查看相关的资源信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/dfa3740beb5347a7b8e241595f7b84a7.png)

可以查看其前置依赖和后置依赖链接

![在这里插入图片描述](https://img-blog.csdnimg.cn/e1f684933b99440e92adb034f3750701.png)

后置链接依赖
![在这里插入图片描述](https://img-blog.csdnimg.cn/5c619c89b4df4e5ba60cf9cdce987f0c.png)

对应 资源的详细信息，日志等

![在这里插入图片描述](https://img-blog.csdnimg.cn/8cf0b67b57d74932adc34bd2f4a3fcd1.png)

嗯，关于 `Monokle` 桌面端和小伙伴分享到这里，博文只是简单介绍，好多功能还待探索，Monokle 结合和了 `K8s API 对象资源的编写、版本管理`和 `K8s 实际的集群管理`，同时提供了常用资源通过表单生成，是一个功能较全面的 `K8s IDE`

官方帮助文档地址

https://kubeshop.github.io/monokle/

## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

---

https://monokle.io/

https://github.com/kubeshop/monokle

---

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
