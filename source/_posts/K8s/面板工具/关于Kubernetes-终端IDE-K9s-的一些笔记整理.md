---
title: Kubernetes:基于终端UI的管理工具 K9s 
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: K9s
uniqueId: '2023-01-14 00:29:30/Kubernetes:基于终端UI的管理工具 K9s .html'
mathJax: false
date: 2023-01-14 08:29:30
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ `K9s` 是一个基于终端UI的 K8S 管理工具
+ 博文内容为 `k9s`  在  windows、Linux 以及docker 安装Demo
+ 简单的 热键使用。
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

`K9s` 是一个基于终端的管理 `Kubernetes` 集群的工具，旨在简化 `Kubernetes` 集群管理。和 `kubebox` 有些类似， K9s 同样可以持续监控集群的变化，并且可以提供与集群资源交互的快捷命令。如果说管理集群受限，只有一个 终端你可以使用，那么 `k9s` 是一个不错的选择.


![在这里插入图片描述](https://img-blog.csdnimg.cn/f40faf1e6d084537a08f39ad32a52722.png)

`K9s` 有一些特殊功能，可以显示 Kubernetes 资源之间依赖关系的 `xray`、显示集群高级状态的 `pulse`，以及扫描和报告已部署资源和配置问题的`popeye` 。

![在这里插入图片描述](https://img-blog.csdnimg.cn/ccd490c6dffe4beb85fd1928b0bb2931.png)

## 安装下载


### window  客户端安装

```bash
PS C:\Program Files> choco install k9s
Chocolatey v1.1.0
Installing the following packages:
k9s
By installing, you accept licenses for the packages.
Progress: Downloading kubernetes-cli 1.26.0... 100%
Progress: Downloading k9s 0.26.7... 100%

kubernetes-cli v1.26.0 [Approved]
kubernetes-cli package files install completed. Performing other installation steps.
The package kubernetes-cli wants to run 'chocolateyInstall.ps1'.
Note: If you don't run this script, the installation will fail.
Note: To confirm automatically next time, use '-y' or consider:
choco feature enable -n allowGlobalConfirmation
Do you want to run the script?([Y]es/[A]ll - yes to all/[N]o/[P]rint): all

Extracting 64-bit C:\ProgramData\chocolatey\lib\kubernetes-cli\tools\kubernetes-client-windows-amd64.tar.gz to C:\ProgramData\chocolatey\lib\kubernetes-cli\tools...
C:\ProgramData\chocolatey\lib\kubernetes-cli\tools
Extracting 64-bit C:\ProgramData\chocolatey\lib\kubernetes-cli\tools\kubernetes-client-windows-amd64.tar to C:\ProgramData\chocolatey\lib\kubernetes-cli\tools...
C:\ProgramData\chocolatey\lib\kubernetes-cli\tools
 ShimGen has successfully created a shim for kubectl-convert.exe
 ShimGen has successfully created a shim for kubectl.exe
 The install of kubernetes-cli was successful.
  Software installed to 'C:\ProgramData\chocolatey\lib\kubernetes-cli\tools'

k9s v0.26.7 [Approved]
k9s package files install completed. Performing other installation steps.
Extracting 64-bit C:\ProgramData\chocolatey\lib\k9s\tools\k9s_Windows_x86_64.tar.gz to C:\ProgramData\chocolatey\lib\k9s\tools...
C:\ProgramData\chocolatey\lib\k9s\tools
Extracting 64-bit C:\ProgramData\chocolatey\lib\k9s\tools\k9s_Windows_x86_64.tar to C:\ProgramData\chocolatey\lib\k9s\tools...
C:\ProgramData\chocolatey\lib\k9s\tools
 ShimGen has successfully created a shim for k9s.exe
 The install of k9s was successful.
  Software installed to 'C:\ProgramData\chocolatey\lib\k9s\tools'

Chocolatey installed 2/2 packages.
 See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log).
PS C:\Program Files>
```

下载之后，需要准备好 `kubeconfig` 文件，放到默认加载的位置
```bash
PS C:\Users\山河已无恙\.kube> ls


    目录: C:\Users\山河已无恙\.kube


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2023/1/14      7:48                cache
-a----         2023/1/14     10:19           5682 config
PS C:\Users\山河已无恙\.kube> cat .\config
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data:
    .......................
```

通过命令行启动

```bash
PS C:\ProgramData\chocolatey\lib\k9s\tools> .\k9s.exe
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/937b767f60dd4083be198aca7ac82ff0.png)



### Linux 客户端安装

下载安装包
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k9s]
└─$curl -sS https://webinstall.dev/k9s | bash

Thanks for using webi to install 'k9s@stable' on 'Linux/x86_64'.
Have a problem? Experience a bug? Please let us know:
        https://github.com/webinstall/webi-installers/issues

Lovin' it? Say thanks with a Star on GitHub:
        https://github.com/webinstall/webi-installers

Downloading k9s from
https://github.com/derailed/k9s/releases/download/v0.26.7/k9s_Linux_x86_64.tar.gz
failed to download from https://github.com/derailed/k9s/releases/download/v0.26.7/k9s_Linux_x86_64.tar.gz
```

解压直接运行
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k9s]
└─$tar -zxvf k9s_Linux_x86_64.tar.gz
LICENSE
README.md
k9s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k9s]
└─$ls
k9s  k9s_Linux_x86_64.tar.gz  LICENSE  README.md
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k9s]
└─$./k9s
```
查看当前集群资源的统计信息。

![在这里插入图片描述](https://img-blog.csdnimg.cn/fe272b637e69448181f399f2f04cbf94.png)


### docker 安装

也可以通过docker 直接运行
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k9s]
└─$docker run --rm -it -v ~/.kube/config:/root/.kube/config docker.io/derailed/k9s
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/12a604e390104e27948b15c1e327202e.png)

查看资源状态信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/b35401543d784d2ba0bd772576c1a9c2.png)

查看资源依赖关系

![在这里插入图片描述](https://img-blog.csdnimg.cn/973d4acaa1684cc09153abd2a050c85c.png)


### 热键

<table>
<thead>
<tr>
<th>Action</th>
<th>Command</th>
<th>Comment</th>
</tr>
</thead>
<tbody>
<tr>
<td>Show active keyboard mnemonics and help</td>
<td><code>?</code></td>
<td></td>
</tr>
<tr>
<td>Show all available resource alias</td>
<td><code>ctrl-a</code></td>
<td></td>
</tr>
<tr>
<td>To bail out of K9s</td>
<td><code>:q</code>, <code>ctrl-c</code></td>
<td></td>
</tr>
<tr>
<td>View a Kubernetes resource using singular/plural or short-name</td>
<td><code>:</code>po⏎</td>
<td>accepts singular, plural, short-name or alias ie pod or pods</td>
</tr>
<tr>
<td>View a Kubernetes resource in a given namespace</td>
<td><code>:</code>alias namespace⏎</td>
<td></td>
</tr>
<tr>
<td>Filter out a resource view given a filter</td>
<td><code>/</code>filter⏎</td>
<td>Regex2 supported ie `fred</td>
</tr>
<tr>
<td>Inverse regex filter</td>
<td><code>/</code>! filter⏎</td>
<td>Keep everything that <em>doesn't</em> match.</td>
</tr>
<tr>
<td>Filter resource view by labels</td>
<td><code>/</code>-l label-selector⏎</td>
<td></td>
</tr>
<tr>
<td>Fuzzy find a resource given a filter</td>
<td><code>/</code>-f filter⏎</td>
<td></td>
</tr>
<tr>
<td>Bails out of view/command/filter mode</td>
<td><code>&lt;esc&gt;</code></td>
<td></td>
</tr>
<tr>
<td>Key mapping to describe, view, edit, view logs,...</td>
<td><code>d</code>,<code>v</code>, <code>e</code>, <code>l</code>,...</td>
<td></td>
</tr>
<tr>
<td>To view and switch to another Kubernetes context</td>
<td><code>:</code>ctx⏎</td>
<td></td>
</tr>
<tr>
<td>To view and switch to another Kubernetes context</td>
<td><code>:</code>ctx context-name⏎</td>
<td></td>
</tr>
<tr>
<td>To view and switch to another Kubernetes namespace</td>
<td><code>:</code>ns⏎</td>
<td></td>
</tr>
<tr>
<td>To view all saved resources</td>
<td><code>:</code>screendump or sd⏎</td>
<td></td>
</tr>
<tr>
<td>To delete a resource (TAB and ENTER to confirm)</td>
<td><code>ctrl-d</code></td>
<td></td>
</tr>
<tr>
<td>To kill a resource (no confirmation dialog!)</td>
<td><code>ctrl-k</code></td>
<td></td>
</tr>
<tr>
<td>Launch pulses view</td>
<td><code>:</code>pulses or pu⏎</td>
<td></td>
</tr>
<tr>
<td>Launch XRay view</td>
<td><code>:</code>xray RESOURCE [NAMESPACE]⏎</td>
<td>RESOURCE can be one of po, svc, dp, rs, sts, ds, NAMESPACE is optional</td>
</tr>
<tr>
<td>Launch Popeye view</td>
<td><code>:</code>popeye or pop⏎</td>
<td>See <a href="#popeye">popeye</a></td>
</tr>
</tbody>
</table>


## 博文参考

***

https://github.com/derailed/k9s

https://k9scli.io/



