---
title: Kubernetes:开源 K8s 管理工具 Rancher 认知
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Rancher
uniqueId: '2023-01-18 01:48:46/Kubernetes:开源 K8s 管理工具 Rancher 认知.html'
mathJax: false
date: 2023-01-18 09:48:46
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 博文内容涉及
 + `Rancher` 的介绍，集群内安装
 + 查看 `Rancher` 的基本功能
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

### Rancher  介绍

`Rancher` 是一个 Kubernetes 管理工具，让你能在任何地方和任何提供商上部署和运行集群。

Rancher 可以创建来自 Kubernetes `托管服务提供商的集群`，`创建节点并安装 Kubernetes`，或者导入在任何地方运行的现有 Kubernetes 集群。

Rancher 基于 Kubernetes 添加了新的功能，包括统一所有集群的`身份验证和 RBAC`，让系统管理员从一个位置控制全部集群的访问。

此外，Rancher 可以为集群和资源提供更精细的`监控和告警`，将日志发送到外部提供商，并通过应用商店(Application Catalog)直接集成 Helm。如果你拥有外部 CI/CD 系统，你可以将其与 Rancher 对接。没有的话，你也可以使用 Rancher 提供的 Fleet 自动部署和升级工作负载。

Rancher 是一个 全栈式 的 Kubernetes 容器管理平台，为你提供在任何地方都能成功运行 Kubernetes 的工具。

Rancher 是一种流行的 Kubernetes 多集群管理解决方案，其中包括 `Rancher Dashboard`，它是使用 Vue.js 和 Nuxt 构建的 Rancher API 的无状态客户端。它被构建并打包为与 Rancher 版本捆绑在一起的静态 HTML/CSS/JS 文件的文件夹。


![在这里插入图片描述](https://img-blog.csdnimg.cn/3476548cb8aa4dd3837939343a0bdc61.png)


`Rancher Dashboard` 显示了登录用户有权访问的所有 Kubernetes 对象类型、命名空间和操作。所有默认视图都是来自 Kubernetes API 的原始 YAML，也可以将其组织为列表页面的表格形式。它允许用户通过自定义表列及其格式以图形方式编辑资源，而不是编辑 YAML。


`Rancher` 不仅仅是一个仪表盘，还可以实现资源的简单操作。 总体来讲， `Rancher` 更多的是一个 k8s 托管平台，可以用来安装 `k8s`,实现告警，管理容器，做资源分配，同时提供了控制面板。所以说如果你只需要一个 仪表盘，或者说是 基本 的 `k8s` 管理工具，可能有点大材小用。而且安装的话，相对来说比较重。涉及东西比较多。

`Rancher` 虽然有点重，但是对中文开发者很友好，有完整的 中文文档，相关地址的文末，有兴趣的小伙伴可以尝试下。


### Rancher 安装

这里通过 `HELM` 的方式来安装，官方有很完整的安装部署文档，建议小伙伴有需要可以直接 访问官网。


添加 `HEML` 源，创建命名空间

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/Rancher]
└─$helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
"rancher-stable" has been added to your repositories
┌──[root@vms81.liruilongs.github.io]-[~/ansible/Rancher]
└─$kubectl create namespace cattle-system
namespace/cattle-system created
```

安装 `charts` 

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/Rancher]
└─$helm install rancher ./rancher-2.7.0.tgz --namespace cattle-system  --set hostname=kubernetes --set ingress.tls.source=secret  --set useBundledSystemChart=true
NAME: rancher
LAST DEPLOYED: Sat Jan 14 21:21:10 2023
NAMESPACE: cattle-system
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
Rancher Server has been installed.

NOTE: Rancher may take several minutes to fully initialize. Please standby while Certificates are being issued, Containers are started and the Ingress rule comes up.

Check out our docs at https://rancher.com/docs/

。。。。。。

Happy Containering!
┌──[root@vms81.liruilongs.github.io]-[~/ansible/Rancher]
└─$
```
```bash
┌──[root@vms100.liruilongs.github.io]-[~/ansible/botkube]
└─$openssl x509 -noout -subject -in /etc/kubernetes/pki/ca.crt
subject= /CN=kubernetes
┌──[root@vms100.liruilongs.github.io]-[~/ansible/botkube]
└─$openssl x509 -noout  -in /etc/kubernetes/pki/ca.crt -text | grep DNS
                DNS:kubernetes
┌──[root@vms100.liruilongs.github.io]-[~/ansible/botkube]
└─$
```

这里需要等一会,然后修改 `rancher`  的 SVC 类型为  `NodePort`
```bash
┌──[root@vms81.liruilongs.github.io]-[/var/run]
└─$kubectl get svc -n cattle-system
NAME              TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
rancher           NodePort    10.107.183.194   <none>        80:31160/TCP,443:31790/TCP   82m
rancher-webhook   ClusterIP   10.106.177.144   <none>        443/TCP                      78m
webhook-service   ClusterIP   10.110.246.67    <none>        443/TCP                      78m
```
这里为什么要修改，集群的 `Ingress`  控制器有问题，一直没处理，所以创建的 `Ingress` 有问题，直接通过 `NodePort` 的方式访问

![在这里插入图片描述](https://img-blog.csdnimg.cn/9ba02433679c4c909287d7e61f5972e2.png)

### 访问查看 Rancher 基本功能

浏览器访问 : https://192.168.26.81:31790/

![在这里插入图片描述](https://img-blog.csdnimg.cn/168a1b3d6a6d4cfd85415c2c6f97d318.png)


获取密码

```bash
┌──[root@vms81.liruilongs.github.io]-[/var/run]
└─$kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{"\n"}}'
2rnz77hq47s6gqvx4bn7vjbsqzsdb5gsttztsbd27hj6ng2mzjptqc
```

重新生成密码

![在这里插入图片描述](https://img-blog.csdnimg.cn/e0efb7599a354a67b5aff7eed30c5eaf.png)

OH95qexjUirrSgO2

重新登录
![在这里插入图片描述](https://img-blog.csdnimg.cn/d0e8930742744fd1adc6feb8b8d801af.png)

帐密信息： admin  / OH95qexjUirrSgO2

![在这里插入图片描述](https://img-blog.csdnimg.cn/e865a7aeb4b144eebb28594b82186b46.png)

查看集群信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/57e432b3d87142c89971e561528d02ca.png)

查看所有节点信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/c5f352def41d479092d5d2c004eba117.png)

查看具体节点信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/b236d6a8ace2481cbf093d98450199ae.png)


查看集群事件

![在这里插入图片描述](https://img-blog.csdnimg.cn/2c4c779e32e54467932a8eb28e533c20.png)

查看 deploy 

![在这里插入图片描述](https://img-blog.csdnimg.cn/12b8f6d6be4540f28f5aa75faa2ff93d.png)


具体安装步骤建议小伙伴看看官网的，这总体来讲，如果有需要可以安装试试，开源，很 nice 的一个 管理工具。
## 博文参考

***

https://rancher.com/docs/

https://ranchermanager.docs.rancher.com/

https://github.com/rancher/rancher