---
title: 关于 Kubernetes 中通过 Kustomize 定制 Yaml资源文件的一些笔记
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kustomize
uniqueId: "2022-12-22 08:14:48/关于Kubernetes 中通过 Kustomize 生成Yaml资源文件的一些笔记.html"
mathJax: false
date: 2022-12-22 16:14:48
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->

## 写在前面

---

- 分享一些 `Kustomize` 的笔记
- 博文内容涉及：
  - `Kustomize` 的简单介绍
  - `Kustomize` 安装
  - `Kustomize` 生成资源yaml 文件 Demo
- 理解不足小伙伴帮忙指正

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

---

### Kustomize 介绍与安装

在 `k8s` 中我们可以通过一些工具来简化 `kubectl 命令` 或者 `yaml 资源文件`的编写.

`Kustomize` 是一个独立于 `k8s` 的工具，通过定义一个 `kustomization` 文件来定制 `Kubernetes` 对象。即可以通过简单的方式生成一些 资源对象， `Kustomize` 新旧版本略有差异，使用时需要注意。

从 `1.14` 版本开始，`kubectl` 也开始支持使用 `kustomization` 文件来管理 `Kubernetes` 对象。

安装 `Kustomize` 需要考虑 `kubelet` 的版本。具体以 官方给出的为准：[https://github.com/kubernetes-sigs/kustomize](https://github.com/kubernetes-sigs/kustomize)

这里我们安装 `kubectl` 客户端为 1.23 的版本

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl version
Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.0", GitCommit:"ab69524f795c42094a6630298ff53f3c3ebab7f4", GitTreeState:"clean", BuildDate:"2021-12-07T18:16:20Z", GoVersion:"go1.17.3", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.2", GitCommit:"8b5a19147530eaac9476b0ab82980b4088bbc1b2", GitTreeState:"clean", BuildDate:"2021-09-15T21:32:41Z", GoVersion:"go1.16.8", Compiler:"gc", Platform:"linux/amd64"}
```

安装的 `kustomize` 版本为 `4.5.7`. 具体的安装可以参考：[https://kubectl.docs.kubernetes.io/zh/installation/](https://kubectl.docs.kubernetes.io/zh/installation/)

```bash
┌──[root@vms81.liruilongs.github.io]-[~/awx]
└─$https://github.com/kubernetes-sigs/kustomize/releases/download/kustomize/v4.5.7/kustomize_v4.5.7_linux_amd64.tar.gz
```

这里网络问题，官方给出的没办法正常安装，所以我们下载安装包解压

拷贝到可执行位置 `/usr/local/bin/`

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$tar -zxf kustomize_v4.5.7_linux_amd64.tar.gz -C ./kustomize/
┌──[root@vms81.liruilongs.github.io]-[~]
└─$cd kustomize/;ls
kustomize
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$mv kustomize /usr/local/bin/
```

查看版本测试

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$kustomize version
{Version:kustomize/v4.5.7 GitCommit:56d82a8378dfc8dc3b3b1085e5a6e67b82966bd7 BuildDate:2022-08-02T16:35:54Z GoOs:linux GoArch:amd64}
```

Kustomize 可以提供以下功能特性来管理应用配置文件：

- 从其他来源生成资源
- 为资源设置通用(Cross-Cutting)字段
- 组织和定制资源集合

今天和分享 `从其他来源生成资源` ，剩下的内容之后和小伙伴分享。

### 生成资源

在 `k8s` 中 通过 `ConfigMap` 和 `Secret` 包含其他 `Kubernetes` 对象(如 Pod)所需要的`配置或敏感数据`。 ConfigMap 或 Secret 中数据的来源往往是集群外部，例如某个 .properties 文件或者 SSH 密钥文件。

`Kustomize` 提供 `secretGenerator` 和 `configMapGenerator` ，可以基于文件或字面值来生成 `Secret 和 ConfigMap`。

#### configMapGenerator

要基于文件来生成 ConfigMap，如果使用 `kubectl` 命令来实现。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create  configmap  myconfig --from-file=./application.properties
```

`Kustomize` 可以在 configMapGenerator 的 files 列表中添加表项。 下面是一个根据 .properties 文件中的数据条目来生成 ConfigMap 的示例：

创建一个 `application.properties` 文件

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$cat application.properties
# 最大鉴权码发送次数
sendSMS.maxAuthSendNumber=3
# 最大鉴权次数
sendSMS.maxAuthNumber=3
# 鉴权码过期时间(毫秒)
sendSMS.validationCodeExpirationTime=300000
# 鉴权码长度
sendSMS.VerificationCodeLength=4
```

定义 `kustomization`

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$cat kustomization.yaml
configMapGenerator:
- name: example-configmap-liruilong
  files:
  - application.properties
```

查看生成的 资源文件。

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$kubectl kustomize ./
apiVersion: v1
data:
  application.properties: |
    # 最大鉴权码发送次数
    sendSMS.maxAuthSendNumber=3
    # 最大鉴权次数
    sendSMS.maxAuthNumber=3
    # 鉴权码过期时间(毫秒)
    sendSMS.validationCodeExpirationTime=300000
    # 鉴权码长度
    sendSMS.VerificationCodeLength=4
kind: ConfigMap
metadata:
  name: example-configmap-liruilong-4b89cmtdm5
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$
```

通过 `Kustomize` 可以直接对创建做一个持久化的保存

要从 env 文件生成 ConfigMap，如果使用 `kubectl` 命令来实现。

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create configmap  myconfig --from-env-file=./env.txt
```

`Kustomize` 需要在 configMapGenerator 中的 envs 列表中添加一个条目。 下面是一个用来自 .env 文件的数据生成 ConfigMap 的例子：

编写 `.mysql.env` 文件

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/env]
└─$cat .mysql.env
userName=root
password=!QAZ2wsx#EDC
```

编写 `kustomization.yaml`

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/env]
└─$cat kustomization.yaml
configMapGenerator:
- name: example-configmap-liruilong-env
  envs:
  - .mysql.env
```

查看生成的 资源文件。

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/env]
└─$kubectl kustomize ./
apiVersion: v1
data:
  password: '!QAZ2wsx#EDC'
  userName: root
kind: ConfigMap
metadata:
  name: example-configmap-liruilong-env-7tm6425f4d
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/env]
└─$
```

ConfigMap 也可基于字面的键值偶对来生成。要基于键值偶对来生成 ConfigMap，kubelet 中

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create configmap  myconfig --from-literal=user=liruilong --from-literal=password=liruilong
```

在 configMapGenerator 的 literals 列表中添加表项。下面是一个例子， 展示如何使用键值偶对中的数据条目来生成 ConfigMap 对象：

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/env]
└─$cat kustomization.yaml
configMapGenerator:
- name: example-configmap-liruilong-env
  literals:
  - FOO=Bar
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/env]
└─$kubectl kustomize ./
apiVersion: v1
data:
  FOO: Bar
kind: ConfigMap
metadata:
  name: example-configmap-liruilong-env-42cfbf598f
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/env]
└─$
```

#### secretGenerator

也可以基于文件或者键值偶对来生成 Secret。要使用文件内容来生成 Secret， 在 secretGenerator 下面的 files 列表中添加表项。 下面是一个根据文件中数据来生成 `Secret` 对象的示例：

```bash
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/pass]
└─$cat password.ini
username=admin
password=secret
```

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/pass]
└─$cat kusromization.yaml
secretGenerator:
- name: example-secret-liruilong
  files:
  - password.ini
```

查看生成的 资源文件。

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/pass]
└─$kubectl kustomize ./
apiVersion: v1
data:
  password.ini: dXNlcm5hbWU9YWRtaW4KcGFzc3dvcmQ9c2VjcmV0Cg==
kind: Secret
metadata:
  name: example-secret-liruilong-k55bt7h89d
type: Opaque
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/pass]
└─$
```

kubectl 的方式

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create secret generic mysecret2 --from-file=/etc/hosts
```

要基于键值偶对字面值生成 Secret，先要在 secretGenerator 的 literals 列表中添加表项。下面是基于键值偶对中数据条目来生成 Secret 的示例：

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/kvs]
└─$cat kustomization.yaml
secretGenerator:
- name: example-secret-liruilong
  literals:
  - username=admin
  - password=secret
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/kvs]
└─$kubectl kustomize  ./
apiVersion: v1
data:
  password: c2VjcmV0
  username: YWRtaW4=
kind: Secret
metadata:
  name: example-secret-liruilong-8c5228dkb9
type: Opaque
```

kubectl 的方式

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  create secret  generic mysec  --from-literal=mysqlpassword=liruilong --from-literal=rqpassword=rq
```

### 使用资源

要在 Deployment 中使用生成的 ConfigMap，使用 configMapGenerator 的名称对其进行引用。 `Kustomize 将自动使用生成的名称替换该名称。`

使用生成的 ConfigMap 的 deployment 示例：


`kustomization.yaml` 的定义，这里需要注意的是 添加了 `resources` 字段用于声明使用的资源。
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$cat kustomization.yaml
resources:
- deploy.yaml
configMapGenerator:
- name: example-configmap-liruilong
  files:
  - application.properties
```
定义 `deployment`，这里我们通过卷的方式使用， 需要 `configMap` 的 `name`字段和上面 `configMapGenerator`  定义的相同。
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$cat deploy.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 3
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx
        name: nginx-web
        ports:
        - containerPort: 80
          name: nginx-web
      volumes:
      - name: config
        configMap:
           name: example-configmap-liruilong
        resources: {}
status: {}
```
检查生成的 资源文件，这里在生成的同时，还做了合并。并且自动按照创建顺序生成

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$kubectl kustomize ./
apiVersion: v1
data:
  application.properties: |
    # 最大鉴权码发送次数
    sendSMS.maxAuthSendNumber=3
    # 最大鉴权次数
    sendSMS.maxAuthNumber=3
    # 鉴权码过期时间(毫秒)
    sendSMS.validationCodeExpirationTime=300000
    # 鉴权码长度
    sendSMS.VerificationCodeLength=4
kind: ConfigMap
metadata:
  name: example-configmap-liruilong-4b89cmtdm5
---
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 3
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx
        name: nginx-web
        ports:
        - containerPort: 80
          name: nginx-web
      volumes:
      - configMap:
          name: example-configmap-liruilong-4b89cmtdm5
        name: config
        resources: {}
status: {}
┌──[root@vms81.liruilongs.github.io]-[~/kustomize]
└─$
```

与 ConfigMap 一样，生成的 Secret 可以通过引用 secretGenerator 的名称在 Deployment 中使用：

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/pass]
└─$cat kustomization.yaml
resources:
- deploy.yaml
secretGenerator:
- name: example-secret-liruilong
  files:
  - password.ini
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/pass]
└─$cat deploy.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 3
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx
        name: nginx-web
        ports:
        - containerPort: 80
          name: nginx-web
      volumes:
      - name: secretPass
        secert:
           secertName: example-secret-liruilong
        resourc: {}
status: {}
```
方式基本一样

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/pass]
└─$kubectl kustomize ./
apiVersion: v1
data:
  password.ini: dXNlcm5hbWU9YWRtaW4KcGFzc3dvcmQ9c2VjcmV0Cg==
kind: Secret
metadata:
  name: example-secret-liruilong-k55bt7h89d
type: Opaque
---
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 3
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx
        name: nginx-web
        ports:
        - containerPort: 80
          name: nginx-web
      volumes:
      - name: secretPass
        resourc: {}
        secert:
          secertName: example-secret-liruilong
status: {}
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/pass]
└─$
```
### 生成属性的配置

通 `kustomize` 不仅可以生成 `ConfigMap 和 Secret ` ,同时可以和 对应的使用资源整合，还可以对生成的资源文件中的一些资源命名，通用字段做配置。


#### generatorOptions

所生成的 ConfigMap 和 Secret 都会包含内容哈希值后缀。 这是为了确保内容发生变化时，所生成的是新的 ConfigMap 或 Secret。 要禁止自动添加后缀的行为，用户可以使用 generatorOptions。 除此以外，为生成的 ConfigMap 和 Secret 指定通用字段也是可以的。

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/options]
└─$cat kustomization.yaml
configMapGenerator:
- name: example-configmap-liruilong
  literals:
  - FOO=Bar
generatorOptions:
  disableNameSuffixHash: true
  labels:
    type: generated
  annotations:
    note: generated
```
上面定义的生成模板中 `disableNameSuffixHash: true`,隐藏了默认的前后缀添加，剩下的为 通用字段的定义。检查生成的 `ConfigMap`
```yaml   
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/options]
└─$kubectl kustomize ./
apiVersion: v1
data:
  FOO: Bar
kind: ConfigMap
metadata:
  annotations:
    note: generated
  labels:
    type: generated
  name: example-configmap-liruilong
```

#### 设置通用字段

为单一的 资源对象设置通用字段可以使用上面的方式，`Kustomize` 同时提供了在项目中为所有 Kubernetes 对象设置通用字段。 `Kustomize` 可以配置的 通用字段的一些使用场景如下：

- 为所有资源设置相同的名字空间
- 为所有对象添加相同的前缀或后缀
- 为对象添加相同的标签集合
- 为对象添加相同的注解集合

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/failed]
└─$cat kustomization.yaml
namespace: my-namespace
namePrefix: dev-
nameSuffix: "-001"
commonLabels:
  app: liruilong-app
commonAnnotations:
  isDemo: "true"
configMapGenerator:
- name: example-configmap-liruilong-env
  literals:
  - FOO=Bar
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/failed]
└─$
```
检查生成的 yaml 文件

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/kustomize/failed]
└─$kubectl kustomize ./
apiVersion: v1
data:
  FOO: Bar
kind: ConfigMap
metadata:
  annotations:
    isDemo: "true"
  labels:
    app: liruilong-app
  name: dev-example-configmap-liruilong-env-001-42cfbf598f
  namespace: my-namespace
```

## 博文参考

---

https://kubectl.docs.kubernetes.io/zh/installation/

https://github.com/kubernetes-sigs/kustomize

https://kubernetes.io/zh-cn/docs/tasks/manage-kubernetes-objects/kustomization/
