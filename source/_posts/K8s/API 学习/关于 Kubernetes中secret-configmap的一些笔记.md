---
title: 关于 Kubernetes中secret、configmap的一些笔记
tags:
  - Kubernetes
  - configmap
  - secret
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2021-12-12 13:34:35/关于Kubernetes中secret、configmap的一些笔记.html'
mathJax: false
date: 2021-12-12 21:34:35
thumbnail:
---
**<font color="009688"> 我不再装模作样地拥有很多朋友，而是回到了孤单之中，以真正的我开始了独自的生活。有时我也会因为寂寞而难以忍受空虚的折磨，但我宁愿以这样的方式来维护自己的自尊，也不愿以耻辱为代价去换取那种表面的朋友。  ——余华《在细雨中呼喊》**</font>
<!-- more -->
## 写在前面
***
+ 学习K8s涉及到这些，整理笔记加以记忆
+ 博客内容涉及`pod`中的配置文件/密码的管理，包括：
+ `secret`,`configmap` 资源对象创建方式
+ `secret`,`configmap` 使用方式(变量/卷)
+ 博文为一些Demo相关理论小伙伴可以到官网看
+ [https://kubernetes.io/zh/docs/concepts/configuration/secret/](https://kubernetes.io/zh/docs/concepts/configuration/secret/)
+ [https://kubernetes.io/zh/docs/concepts/configuration/configmap/](https://kubernetes.io/zh/docs/concepts/configuration/configmap/)


**<font color="009688"> 我不再装模作样地拥有很多朋友，而是回到了孤单之中，以真正的我开始了独自的生活。有时我也会因为寂寞而难以忍受空虚的折磨，但我宁愿以这样的方式来维护自己的自尊，也不愿以耻辱为代价去换取那种表面的朋友。  ——余华《在细雨中呼喊》**</font>

***



**<font color=royalblue>应用部署的一个最佳实践是 **<font color=purple>将应用所需的配置信息与程序进行分离</font>**,这样可以使得应用程序被更好地复用,通过不同的配置也能实现更灵活的功能。</font>**

**<font color=purple>将应用打包为容器镜像后,可以通过`环境变量`或者`外挂文件`的方式在`创建容器时进行配置注入`,但在`大规模容器集群`的环境中,对多个容器进行不同的配置将变得非常复杂。从Kubernetes v1.2开始提供了一种统一的应用配置管理方案`ConfgMap`，`ConfigMap `是一种` API 对象`，用来将非机密性的数据保存到键值对中。使用时， `Pods `可以将其用作环境变量、命令行参数或者存储卷中的配置文件。</font>**

**<font color=plum>`Secret` 是一种包含少量敏感信息例如密码、令牌或密钥的对象。 这样的信息可能会被放在 `Pod` 规约中或者镜像中。 使用 `Secret` 意味着你不需要在应用程序代码中包含机密数据。</font>**

**<font color=amber>由于创建 `Secret` 可以独立于使用它们的 `Pod，` 因此在创建、查看和编辑 `Pod` 的工作流程中暴露 `Secret(及其数据)的风险较小。` `Kubernetes` 和在集群中运行的应用程序也可以对 `Secret` 采取额外的预防措施， 例如避免将机密数据写入非易失性存储。</font>**

|`secret`和`configmap`供容器使用的典型用法如下。|
|--|
|**<font color=purple>生成为容器内的环境变量。</font>**|
|**<font color=orange>设置容器启动命令的启动参数</font>**(需设置为环境变量)。|
|**<font color=brown>以Volume的形式挂载为容器内部的文件或目录。</font>**|

**<font color=yellowgreen>config和secret的区别主要是secret加密了，而config没有加密</font>**

### <font color=brown>环境准备</font>
**<font color=purple>相关镜像拉取</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m ping
192.168.26.83 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
192.168.26.82 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m shell -a "docker pull hub.c.163.com/library/mysql:latest"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m shell -a "docker pull hub.c.163.com/library/wordpress:latest"
```
**<font color=seagreen>学习环境准备,新建一个命名空间</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$dir=k8s-secret-create;mkdir $dir;cd $dir
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get ns
NAME                      STATUS   AGE
default                   Active   66d
kube-node-lease           Active   66d
kube-public               Active   66d
kube-system               Active   66d
liruilong                 Active   65d
liruilong-pod-create      Active   58d
liruilong-volume-create   Active   16d
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  create ns liruilong-secret-create
namespace/liruilong-secret-create created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl config  set-context $(kubectl config current-context) --namespace=liruilong-secret-create
Context "context1" modified.
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl config view | grep namespace
    namespace: default
    namespace: liruilong-secret-create
    namespace: kube-system
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl config get-contexts
CURRENT   NAME       CLUSTER    AUTHINFO            NAMESPACE
          cluster1                                  default
*         context1   cluster1   kubernetes-admin1   liruilong-secret-create
          context2                                  kube-system
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```

# <font color=seagreen>`secret`密码配置管理</font>
**<font color=green>`secret`用于多个镜像的密码管理</font>**

**<font color=brown>mysqlpod创建一个mysql镜像</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl run mysqlpod --image=hub.c.163.com/library/mysql:latest --image-pull-policy=IfNotPresent --dry-run=client -o yaml >mysqlpod.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$vim mysqlpod.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$cat mysqlpod.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: mysqlpod
  name: mysqlpod
spec:
  containers:
  - image: hub.c.163.com/library/mysql:latest
    imagePullPolicy: IfNotPresent
    name: mysqlpod
    resources: {}
    env:
    - name: MYSQL_ROOT_PASSWORD
      value: liruilong
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  apply -f mysqlpod.yaml
pod/mysqlpod created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  get pods -o wide
NAME       READY   STATUS    RESTARTS   AGE   IP               NODE                         NOMINATED NODE   READINESS GATES
mysqlpod   1/1     Running   0          19s   10.244.171.190   vms82.liruilongs.github.io   <none>           <none>
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
**<font color=plum>客户端测试</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$yum -y install mariadb
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$mysql -uroot -pliruilong -h10.244.171.190
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.7.18 MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MySQL [(none)]> quit
Bye
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```

## <font color=tomato>创建 secret</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl describe pod mysqlpod  | grep -A 2 Env
    Environment:
      MYSQL_ROOT_PASSWORD:  liruilong
    Mounts:
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
**<font color=chocolate>上面的密码我们使用的是明文，但是在实际的生产环境使用明文是很危险的一件事,所以我们需要加密处理</font>**

**<font color=seagreen>secret主要用于密码的保存</font>**
**<font color=tomato>通过键值对的方式创建。直接指定键值对，或者存放中secret中</font>**
### <font color=camel>命令行创建secret</font>
**<font color=purple>查看secret</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  get sa
NAME      SECRETS   AGE
default   1         46m
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  get secrets
NAME                  TYPE                                  DATA   AGE
default-token-7q2qj   kubernetes.io/service-account-token   3      46m
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
**<font color=brown>创建secret</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  create secret  generic mysecl  --from-literal=mysqlpassword=liruilong --from-literal=rqpassword=rq
secret/mysecl created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get secrets
NAME                  TYPE                                  DATA   AGE
default-token-7q2qj   kubernetes.io/service-account-token   3      49m
mysecl                Opaque                                2      9s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
**<font color=brown>Secret有三种类型：</font>**
|Secret有三种类型|
|--|
|**<font color=orange>Opaque</font>**                             |`base64编码`格式的`Secret`，用来存储密码、密钥等；但数据也通过`base64 –decode`解码得到原始数据，所以`加密性很弱`。|
|**<font color=red>kubernetes.io/dockerconfigjson</font>**   |用来存储私有`docker registry`的认证信息。|
|**<font color=green>kubernetes.io/service-account-token</font>**|用于被`serviceaccount`引用。`serviceaccout`创建时`Kubernetes会默认创建对应的secret`。Pod如果使用了serviceaccount，对应的secret会自动挂载到Pod目录`/run/secrets/ kubernetes.io/serviceaccount`中。|

**<font color=orange>查看详细信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl describe   secrets  mysecl
Name:         mysecl
Namespace:    liruilong-secret-create
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
mysqlpassword:  9 bytes
rqpassword:     2 bytes
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get secrets  mysecl  -o yaml
apiVersion: v1
data:
  mysqlpassword: bGlydWlsb25n
  rqpassword: cnE=
kind: Secret
metadata:
  creationTimestamp: "2021-12-12T02:45:20Z"
  name: mysecl
  namespace: liruilong-secret-create
  resourceVersion: "1594980"
  selfLink: /api/v1/namespaces/liruilong-secret-create/secrets/mysecl
  uid: 05a99a7c-c7f0-48ac-9f67-32eb52ed1558
type: Opaque
```
**<font color=amber>也可以通过解密得到想要的密码</font>**
```
┌──[root@vms81.liruilongs.github.io]-[~]
└─$echo bGlydWlsb25n | base64 -d
liruilong┌──[root@vms81.liruilongs.github.io]-[~]
┌──[root@vms81.liruilongs.github.io]-[~]
└─$echo cnE= | base64 -d
rq┌──[root@vms81.liruilongs.github.io]-[~]
```
**<font color=royalblue>直接解密</font>**
```
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl get secrets  mysecl  -o  jsonpath='{.data.mysqlpassword}' | base64 -d
liruilong┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
### <font color=camel>文件方式创建secret</font>
**<font color=orange>一般使用命令行的方式创建，很少使用文件的方式创建</font>**
**<font color=orange>帐密信息文件</font>**
```bash
liruilong┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$tee cat env.txt <<-'EOF'
> user=liruilong
> password1=redhat
> password2=redhat
> EOF
user=liruilong
password1=redhat
password2=redhat
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$ls
env.txt  mysqlpod.yaml
```
**<font color=green>通过`--from-env-file`文件创建</font>**
**<font color=red>文件中的键值对</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create secret generic mysecret1 --from-env-file=env.txt
secret/mysecret1 created
```
**<font color=blue>查看创建信息</font>**
```
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get secrets
NAME                  TYPE                                  DATA   AGE
default-token-7q2qj   kubernetes.io/service-account-token   3      6h34m
mysecl                Opaque                                2      5h45m
mysecret1             Opaque                                3      32s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  describe  secrets  mysecret1
Name:         mysecret1
Namespace:    liruilong-secret-create
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
password1:  6 bytes
password2:  6 bytes
user:       9 bytes
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
**<font color=brown>也可以通过`--from-file`来创建，文件名是键，文件内容为值</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create secret generic mysecret2 --from-file=/etc/hosts
secret/mysecret2 created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get secrets  mysecret2  -o jsonpath='{.data.hosts}'| base64 -d
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.26.81 vms81.liruilongs.github.io vms81
192.168.26.82 vms82.liruilongs.github.io vms82
192.168.26.83 vms83.liruilongs.github.io vms83
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
## <font color=tomato>使用 secret</font>
**<font color=green>secret可以通过卷的方式使用，也可以通过变量的方式使用</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  create secret  generic mysecl  --from-literal=mysqlpassword=liruilong --from-literal=rqpassword=rq
secret/mysecl created
```
**<font color=seagreen>这里我们使用前面的创建的这个secret</font>**
### <font color=red>变量的方式使用secret</font>
**<font color=camel>yaml文件中变量设置密码通过secret的方式:mysqlpodargs.yaml</font>**
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: mysqlpod
  name: mysqlpod
spec:
  containers:
  - image: hub.c.163.com/library/mysql:latest
    imagePullPolicy: IfNotPresent
    name: mysqlpod
    resources: {}
    env:
    - name: MYSQL_ROOT_PASSWORD
      valueFrom:
        secretKeyRef:
          name: mysecl
          key: mysqlpassword
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
**<font color=orange>创建pod</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl apply  -f mysqlpodargs.yaml
pod/mysqlpod created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get pods -o wide
NAME       READY   STATUS              RESTARTS   AGE   IP       NODE                         NOMINATED NODE   READINESS GATES
mysqlpod   0/1     ContainerCreating   0          15s   <none>   vms83.liruilongs.github.io   <none>           <none>
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get pods -o wide
NAME       READY   STATUS    RESTARTS   AGE   IP             NODE                         NOMINATED NODE   READINESS GATES
mysqlpod   1/1     Running   0          21s   10.244.70.19   vms83.liruilongs.github.io   <none>           <none>
```
**<font color=seagreen>测试登录</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$mysql -uroot -h10.244.70.19 -pliruilong
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.7.18 MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MySQL [(none)]>
```
### <font color=blue>以卷的方式使用secret</font>
**<font color=royalblue>pod文件nginxsecret.yaml，一般不这样使用</font>**
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginxsecret
  name: nginxsecret
spec:
  volumes:
  - name: v1
    secret:
      secretName: mysecl
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: nginxsecret
    resources: {}
    volumeMounts:
    - name: v1
      mountPath: /data
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
**<font color=camel>创建pod会把加密的文件信息写在pod里的`/data`目录下</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  apply  -f nginxsecret.yaml
pod/nginxsecret created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  get pods
NAME          READY   STATUS    RESTARTS   AGE
nginxsecret   1/1     Running   0          41s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl exec -it nginxsecret -- bash
root@nginxsecret:/# ls
bin   data  docker-entrypoint.d   etc   lib    media  opt   root  sbin  sys  usr
boot  dev   docker-entrypoint.sh  home  lib64  mnt    proc  run   srv   tmp  var
root@nginxsecret:/# cd data/;ls
mysqlpassword  rqpassword
root@nginxsecret:/data# exit
exit
```
**<font color=red>如过添加了`subPath`,会把指定的信息写入文件:nginxsecretsubPth.yaml</font>**
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginxsecret
  name: nginxsecret
spec:
  volumes:
  - name: v1
    secret:
      secretName: mysecl
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: nginxsecret
    resources: {}
    volumeMounts:
    - name: v1
      mountPath: /data/mysql
      subPath: mysqlpassword
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
**<font color=orange>创建pod测试</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  apply  -f nginxsecretsubPth.yaml
pod/nginxsecret created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get pods
NAME          READY   STATUS    RESTARTS   AGE
nginxsecret   1/1     Running   0          16s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  exec -it nginxsecret -- bash
root@nginxsecret:/# cat data/mysql
liruilongroot@nginxsecret:/# exit
exit
```

# <font color=brown>configmap(cm)服务配置管理</font>
**<font color=seagreen>也是以键值对的方式使用，一般通过命名行的方式创建，也可以通过卷和变量的方式使用</font>**
**<font color=green>config和secret的区别主要是secret加密了，而config没有加密</font>**
## <font color=brown>configmap(cm)的创建</font>
### <font color=purple>通过命令行的方式创建</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create configmap  myconfig1 --from-literal=user=liruilong --from-literal=password=liruilong
configmap/myconfig1 created
```
**<font color=blue>查看创建信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get cm
NAME               DATA   AGE
kube-root-ca.crt   1      7h32m
myconfig1          2      81s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl describe configmaps myconfig1
Name:         myconfig1
Namespace:    liruilong-secret-create
Labels:       <none>
Annotations:  <none>

Data
====
password:
----
liruilong
user:
----
liruilong

BinaryData
====

Events:  <none>
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get cm myconfig1 -o jsonpath='{.data.password}'
liruilong┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get cm myconfig1 -o jsonpath='{.data.user}'
liruilong┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
### <font color=amber>通过文件的方式创建</font>
**<font color=brown>微服务中常用的配置文件信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$cat application.properties
##配置了Web容器的端口号。
server.port=8081
##配置了当项目出错时跳转去的页面。
#server.error.path=/error
##配置了session失效时间, 30m表示30分钟,如果不写单位,默认单位是秒。由于Tomcat中配置session过期时间以分 钟为单位,因此这里单位如果是秒的话,该时间会被转换为一个不超过所配置秒数的最大分钟数,例如这里配置了119, 默认单位为秒,则实际session过期时间为1分钟。
server.servlet.session.timeout=30m
##表示项目名称,不配置时默认为/,如果配置了,就要在访问路径中加上配置的路径。
server.servlet.context-path=/
##表示配置Tomcat请求编码。
server.tomcat.uri-encoding=utf-8
##表示Tomcat最大线程数。
server.tomcat.threads.max=500
##是一个存放Tomcat运行日志和临时文件的目录,若不配置,则默认使用系统的临时目录。
server.tomcat.basedir=/home/sang/tmp


#HttpServletRequest的属性是否可以覆盖controller中model的同名项
spring.freemarker.allow-request-override=false
#HttpSession的属性是否可以覆盖controller中model的同名项
spring.freemarker.allow-session-override=true
#是否开启缓存
spring.freemarker.cache=false
#模板文件编码
spring.freemarker.charset=UTF-8
#是否检查模板位置
spring.freemarker.check-template-location=true
#Content-Type的值
spring.freemarker.content-type=text/html
#是否将HttpServletRequest中的属性添加到Model中
spring.freemarker.expose-request-attributes=false
#是否将HttpSession中的属性添加到Model中
spring.freemarker.expose-session-attributes=true
#模板文件后缀
spring.freemarker.suffix=.ftl
#模板文件位置
spring.freemarker.template-loader-path=classpath:/templates/


#是否开启缓存，开发时可设置为false，默认为true
spring.thymeleaf.cache=true
#是否检查模板是否存在，默认为true
spring.thymeleaf.check-template=true
#是否检查模板位置是否存在，默认为true
spring.thymeleaf.check-template-location=true
#模板文件编码
spring.thymeleaf.encoding=UTF-8
#模板文件位置
spring.thymeleaf.prefix=classpath:/templates/
#Content-Type配置
spring.thymeleaf.servlet.content-type=text/html
#模板文件后缀
spring.thymeleaf.suffix=.html


#spring.mvc.view.prefix=/WEB-INF/jsp/
##spring.mvc.view.suffix=.jsp

spring.redis.database=0
spring.redis.host=192.168.66.130
spring.redis.port=6379
spring.redis.password=123@456
spring.redis.lettuce.pool.max-active=
spring.redis.lettuce.pool.max-idle=
spring.redis.lettuce.pool.max-wait=
spring.redis.lettuce.pool.min-idle=
spring.redis.lettuce.shutdown-timeout=
#连接池最大连接数
spring.redis.jedis.pool.max-active=8
#连接池中的最大空闲连接
spring.redis.jedis.pool.max-idle=8
#连接池最大阻塞等待时间(使用负值表示没有限制)
spring.redis.jedis.pool.max-wait=-1ms
#连接池中的最小空闲连接
spring.redis.jedis.pool.min-idle=0
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create  configmap  myconfig2 --from-file=./application.properties
configmap/myconfig2 created
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$cat env.txt
user=liruilong
password1=redhat
password2=redhat
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl create configmap  myconfig3 --from-env-file=./env.txt
configmap/myconfig3 created
```
查看创建的全部configMap
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get cm
NAME               DATA   AGE
kube-root-ca.crt   1      8h
myconfig1          2      37m
myconfig2          1      9m16s
myconfig3          3      18s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
## <font color=brown>configmap(cm)的使用</font>
### <font color=chocolate>用卷的方式使用configmap</font>
**<font color=red>configmap通常使用卷的方式使用，一般可以在微服务中抽离配置文件： ngingconfig.yaml</font>**
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginxsecret
  name: nginxsecret
spec:
  volumes:
  - name: config
    configMap:
      name: myconfig2
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: nginxsecret
    resources: {}
    volumeMounts:
    - name: config
      mountPath: /app/java
      readOnly: true
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
**<font color=purple>测试，查看配置文件</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl apply  -f ngingconfig.yaml
pod/nginxsecret created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl  get pods
NAME          READY   STATUS    RESTARTS   AGE
nginxsecret   1/1     Running   0          40s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl exec -it nginxsecret -- bash
root@nginxsecret:/# cd /app/java/;ls
application.properties
root@nginxsecret:/app/java# cat application.properties
##配置了Web容器的端口号。
server.port=8081
##配置了当项目出错时跳转去的页面。
#server.error.path=/error
##配置了session失效时间, 30m表示30分钟,如果不写单位,默认单位是秒。由于Tomcat中配置session过期时间以分 钟为单位,因此这里单位如果是秒的话,该时间会被转换为一个不超过所配置秒数的最大分钟数,例如这里配置了119, 默认单位为秒,则实际session过期时间为1分钟。
server.servlet.session.timeout=30m
##表示项目名称,不配置时默认为/,如果配置了,就要在访问路径中加上配置的路径。
server.servlet.context-path=/
##表示配置Tomcat请求编码。
server.tomcat.uri-encoding=utf-8
.........
```
**<font color=tomato>修改kube-prosy的负载策略,修改其中的` mode: " iptables/ipvs"`,修改之后需要重启对应的pod</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get cm -n kube-system
NAME                                 DATA   AGE
calico-config                        4      66d
coredns                              1      66d
extension-apiserver-authentication   6      66d
kube-proxy                           2      66d
kube-root-ca.crt                     1      66d
kubeadm-config                       2      66d
kubelet-config-1.21                  1      66d
kubelet-config-1.22                  1      54d
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl edit  cm kube-proxy -n kube-system
```
### <font color=green>变量的方式使用configMap</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get configmaps myconfig3 -o yaml
apiVersion: v1
data:
  password1: redhat
  password2: redhat
  user: liruilong
kind: ConfigMap
metadata:
  creationTimestamp: "2021-12-12T10:04:42Z"
  name: myconfig3
  namespace: liruilong-secret-create
  resourceVersion: "1645816"
  selfLink: /api/v1/namespaces/liruilong-secret-create/configmaps/myconfig3
  uid: b75bef31-05a8-4d67-8d5c-dea42aedea67
```
**<font color=green>编写pod资源文件</font>**
```yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: mysqlpod
  name: mysqlpod
spec:
  containers:
  - image: hub.c.163.com/library/mysql:latest
    imagePullPolicy: IfNotPresent
    name: mysqlpod
    resources: {}
    env:
    - name: MYSQL_ROOT_PASSWORD
      valueFrom:
        configMapKeyRef:
          name: myconfig3
          key: user
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
**<font color=tomato>创建pod</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl apply  -f mysqlpodconfig.yaml
pod/mysqlpod created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$kubectl get pods -o wide
NAME       READY   STATUS    RESTARTS   AGE     IP               NODE                         NOMINATED NODE   READINESS GATES
mysqlpod   1/1     Running   0          3m19s   10.244.171.130   vms82.liruilongs.github.io   <none>           <none>
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$
```
**<font color=chocolate>测试使用</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-secret-create]
└─$mysql -uroot -h10.244.171.130 -pliruilong
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MySQL connection id is 3
Server version: 5.7.18 MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MySQL [(none)]>
```
