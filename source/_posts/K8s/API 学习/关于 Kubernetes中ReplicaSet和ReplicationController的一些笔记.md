---
title: 关于 Kubernetes中ReplicaSet和ReplicationController的一些笔记
tags:
  - Kubernetes
  - ReplicaSet
  - ReplicationController
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2021-12-18 15:51:58/关于 Kubernetes中ReplicaSet和ReplicationController的一些笔记.html'
mathJax: false
date: 2021-12-18 23:51:58
thumbnail:
---
**<font color="009688"> 请不要把陌生人的些许善意，视为珍稀的瑰宝，却把身边亲近人的全部付出，当做天经地义的事情，对其视而不见   ——烽火戏诸侯《剑来》**</font>
<!-- more -->
## 写在前面
***
+ 学习`k8s`这里整理记忆
+ 其实`RC`和`RS`用的不是特别多，`RC`更是早期的资源对象
+ `RC` -> `RS `-> `Deploy` 的发展顺序，所以我们一般使用`Deploy`较多
+ 博文内容涉及`RC/RS`的一些基本操作


**<font color="009688"> 请不要把陌生人的些许善意，视为珍稀的瑰宝，却把身边亲近人的全部付出，当做天经地义的事情，对其视而不见   ——烽火戏诸侯《剑来》**</font>
 ***


# <font color=yellowgreen>ReplicationController(RC)</font>

**<font color=blue>ReplicationController 确保在任何时候都有特定数量的 Pod 副本处于运行状态。 换句话说，ReplicationController 确保一个 Pod 或一组同类的 Pod 总是可用的。</font>**

**<font color=yellowgreen>推荐使用配置 `ReplicaSet` 的 `Deployment` 来建立副本管理机制。RC是一个很古老的资源控制器，现在一般不怎么使用，作为了解，和deploy很的相似。</font>**

### <font color=purple>ReplicationController 如何工作</font>
**<font color=blue>当 Pod 数量过多时，ReplicationController 会终止多余的 Pod。当 Pod 数量太少时，ReplicationController 将会启动新的 Pod。 与手动创建的 Pod 不同，由 ReplicationController 创建的 Pod 在失败、被删除或被终止时会被自动替换。 例如，在中断性维护(如内核升级)之后，你的 Pod 会在节点上重新创建。 因此，即使你的应用程序只需要一个 Pod，你也应该使用 ReplicationController 创建 Pod。 ReplicationController 类似于进程管理器，但是 ReplicationController 不是监控单个节点上的单个进程，而是监控跨多个节点的多个 Pod。</font>**

### <font color=blue>ReplicationController 的替代方案 ReplicaSet</font>

**<font color=green>`ReplicaSet` 是下一代 `ReplicationController`， 支持新的基于集合的标签选择算符。 它主要被 `Deployment` 用来作为一种编排 `Pod` 创建、删除及更新的机制。 请注意，我们推荐使用 `Deployment` 而不是直接使用 `ReplicaSet`，除非 你需要自定义更新编排或根本不需要更新。</font>**


**<font color=red>`Deployment` 是一种更高级别的 API 对象， 它以类似于 `kubectl rolling-update` 的方式更新其底层 `ReplicaSet` 及其 `Pod。` 如果你想要这种滚动更新功能，那么推荐使用 `Deployment`，因为与` kubectl rolling-update` 不同， 它们是声明式的、服务端的，并且具有其它特性。</font>**


### <font color=brown>创建一个RC</font>
```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: nginxrc
spec:
  replicas: 2
  selector:
    app: nginx
  template:
    metadata:
      name: nginx
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx
        name: web
        resources:
          requests:
            cpu: 100m
      restartPolicy: Always
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  apply  -f rc.yaml
replicationcontroller/nginxrc created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  get pods
NAME            READY   STATUS              RESTARTS   AGE
nginxrc-5szqd   0/1     ContainerCreating   0          15s
nginxrc-tstxl   1/1     Running             0          15s
```
**<font color=amber>修改RC副本数</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  scale  rc nginxrc --replicas=5
replicationcontroller/nginxrc scaled
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  get pods
NAME            READY   STATUS              RESTARTS   AGE
nginxrc-5szqd   1/1     Running             0          84s
nginxrc-6ptpt   0/1     ContainerCreating   0          3s
nginxrc-pd6qw   0/1     ContainerCreating   0          3s
nginxrc-tntbd   0/1     ContainerCreating   0          3s
nginxrc-tstxl   1/1     Running             0          84s
```
**<font color=purple>删除RC</font>**
```
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  delete  -f rc.yaml
replicationcontroller "nginxrc" deleted
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  get pods
NAME            READY   STATUS        RESTARTS   AGE
nginxrc-5szqd   1/1     Terminating   0          110s
nginxrc-6ptpt   1/1     Terminating   0          29s
nginxrc-pd6qw   1/1     Terminating   0          29s
nginxrc-tntbd   1/1     Terminating   0          29s
nginxrc-tstxl   1/1     Terminating   0          110s
```

# <font color=yellowgreen>ReplicaSet(RS)</font>

**<font color=blue>`ReplicaSet` 的目的是维护一组在任何时候都处于`运行状态的 Pod 副本的稳定集合`。 因此，它通常用来保证给定数量的、完全相同的 Pod 的可用性。</font>**



**<font color=red>ReplicaSet 的工作原理</font>**
**<font color=brown>`RepicaSet` 是通过一组字段来定义的，包括:</font>**
+ 一个用来识别可获得的 Pod 的集合的选择算符(选择器)、
+ 一个用来标明应该维护的副本个数的数值、
+ 一个用来指定应该创建新 Pod 以满足副本个数条件时要使用的 Pod 模板等等。

**<font color=orange>每个 `ReplicaSet` 都通过根据需要创建和 删除 `Pod` 以使得副本个数达到期望值， 进而实现其存在价值。当 `ReplicaSet` 需要创建新的 `Pod` 时，会使用所提供的 `Pod` 模板。</font>**

**<font color=brown>`ReplicaSet` 通过 `Pod` 上的 `metadata.ownerReferences` 字段连接到附属 `Pod`，该字段给出当前对象的属主资源。 ReplicaSet 所获得的 Pod 都在其 `ownerReferences` 字段中包含了属主 `ReplicaSet` 的标识信息。正是通过这一连接，`ReplicaSet `知道它所维护的 `Pod` 集合的状态， 并据此计划其操作行为。</font>**

**<font color=red>ReplicaSet 使用其选择算符来辨识要获得的 Pod 集合。如果某个 Pod 没有 OwnerReference 或者其 OwnerReference 不是一个 控制器，且其匹配到 某 ReplicaSet 的选择算符，则该 Pod 立即被此 ReplicaSet 获得。</font>**
### <font color=green>何时使用 ReplicaSet</font>

**<font color=tomato>ReplicaSet 确保任何时间都有指定数量的 Pod 副本在运行。 然而，Deployment 是一个更高级的概念，它管理 ReplicaSet，并向 Pod 提供声明式的更新以及许多其他有用的功能。 因此，我们建议使用 Deployment 而不是直接使用 ReplicaSet，除非 你需要自定义更新业务流程或根本不需要更新。</font>**


### <font color=amber>创建一个RS</font>
```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: frontend
  labels:
    app: guestbook
    tier: frontend
spec:
  replicas: 3
  selector:
    matchLabels:
      tier: frontend
  template:
    metadata:
      labels:
        tier: frontend
    spec:
      containers:
      - name: nginx
        image: nginx
```
```bash

┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  apply  -f rs.yaml
replicaset.apps/frontend created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  get pods
NAME             READY   STATUS              RESTARTS   AGE
frontend-8r27p   1/1     Running             0          33s
frontend-lk46p   0/1     ContainerCreating   0          33s
frontend-njjt2   0/1     ContainerCreating   0          33s
```
**<font color=camel>修改RS副本数</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  scale rs frontend  --replicas=1
replicaset.apps/frontend scaled
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$kubectl  get pods
NAME             READY   STATUS        RESTARTS   AGE
frontend-8r27p   1/1     Running       0          60s
frontend-lk46p   1/1     Terminating   0          60s
frontend-njjt2   1/1     Terminating   0          60s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-ReplicationController]
└─$
```

|三者在胚子文件的区别|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/d80d2164169a4dafb19dc6b122463ac4.png)
|

**<font color=chocolate>副本数的修改</font>**
```bash
kubectl scale deployment nginx --replicas=20
kubectl scale rs rs1 --replicas=4
kubectl scale dc nginx --replicas=20
```