---
title: 关于 Kubernetes中Job&Cronjob的一些笔记
tags:
  - Kubernetes
  - job
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2021-12-19 03:20:23/关于 Kubernetes中Job&Cronjob的一些笔记.html'
mathJax: false
date: 2021-12-19 11:20:23
thumbnail:
---
**<font color="009688"> 很多道理我憋在心里，想要跟这个世界好好的说上一说!   ——烽火戏诸侯《剑来》**</font>

<!-- more -->
## 写在前面
***
+ 学习K8s涉及到这些，整理笔记加以记忆
+ 博文内容涉及：
+ `job`的创建,参数解析,`并行多work`的`job`创建
+ `cron job`的创建


**<font color="009688"> 很多道理我憋在心里，想要跟这个世界好好的说上一说!   ——烽火戏诸侯《剑来》**</font>


 ***

# <font color=royalblue>Job&CronJob</font>
## <font color=brown>Job：批处理工作计划</font>
**<font color=purple>`Kubernetes从1.2版本`开始支持批处理类型的应用,我们可以通过`Kubernetes Job`资源对象来定义并启动一个批处理任务。</font>**

**<font color=yellowgreen>批处理任务通常`并行(或者串行)`启动多个计算进程去处理一批`工作项(work item)`处理完成后,整个批处理任务结束。</font>**

>**<font color=purple>K8s官网中这样描述</font>**：**<font color=yellowgreen>Job 会创建一个或者多个 Pods，并将继续重试 Pods 的执行，直到指定数量的 Pods 成功终止。 随着 Pods 成功结束，Job 跟踪记录成功完成的 Pods 个数。 当数量达到指定的成功个数阈值时，任务(即 Job)结束。 删除 Job 的操作会清除所创建的全部 Pods。 挂起 Job 的操作会删除 Job 的所有活跃 Pod，直到 Job 被再次恢复执行。</font>**

**<font color=orange>一种简单的使用场景下，你会创建一个 Job 对象以便以一种可靠的方式运行某 Pod 直到完成。 当第一个 Pod 失败或者被删除(比如因为节点硬件失效或者重启)时，Job 对象会启动一个新的 Pod。也可以使用 Job 以并行的方式运行多个 Pod。</font>**


**<font color=seagreen>考虑到批处理的并行问题, Kubernetes将Job分以下三种类型。</font>**

|类型|描述|
|--|--|
|**<font color=tomato>Non-parallel Jobs</font>**|通常`一个Job只启动一个Pod`,除非`Pod异常,才会重启该Pod`,一旦此`Pod正常结束, Job将结束`。|
|**<font color=blue>Parallel Jobs with a fixed completion count</font>**|`并行Job会启动多个Pod`,此时需要设定`Job的.spec.completions`参数为一个正数,当正常结束的Pod数量达至此参数设定的值后, `Job结束`。此外, `Job的.spec.parallelism参数用来控制并行度`,即`同时启动几个Job来处理Work Item`.|
|**<font color=purple>Parallel Jobs with a work queue</font>**|`任务队列方式的并行Job`需要一个独立的`Queue`, `Work item都在一个Queue中存放`,不能设置`Job的.spec.completions参数`,此时Job有以下特性。<br>每个Pod都能独立判断和决定是否还有任务项需要处理。<br>如果某个Pod正常结束,则Job不会再启动新的Pod. <br> 如果一个Pod成功结束,则此时应该不存在其他Pod还在工作的情况,它们应该都处于即将结束、退出的状态。 <br>如果所有Pod都结束了,且至少有一个Pod成功结束,则整个Job成功结束。|

**<font color=royalblue>嗯，我们就第一个，第二搞一个Demo，第三中之后有时间搞，其实就是资源配置参数的问题</font>**
**<font color=red>环境准备</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl config set-context  $(kubectl config current-context) --namespace=liruiling-job-create
Context "kubernetes-admin@kubernetes" modified.
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  create  ns liruiling-job-create
namespace/liruiling-job-create created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$vim myjob.yaml
```
### <font color=purple>创建一个job</font>
**<font color=purple>创建一个Job，执行`echo "hello jobs"`</font>**
**<font color=blue> myjob.yaml</font>**
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: my-job
spec:
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
      - command:
        - sh
        - -c
        - echo "hello jobs"
        - sleep 15
        image: busybox
        name: my-job
        resources: {}
      restartPolicy: Never
status: {}
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  apply  -f myjob.yaml
job.batch/my-job created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME              READY   STATUS              RESTARTS   AGE
my-job--1-jdzqd   0/1     ContainerCreating   0          7s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get jobs
NAME     COMPLETIONS   DURATION   AGE
my-job   0/1           17s        17s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME              READY   STATUS      RESTARTS   AGE
my-job--1-jdzqd   0/1     Completed   0          24s
```
**<font color=tomato>`STATUS` 状态变成 `Completed`意味着执行成功，查看日志</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get jobs
NAME     COMPLETIONS   DURATION   AGE
my-job   1/1           19s        46s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl logs my-job--1-jdzqd
hello jobs
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$
```

### <font color=royalblue>job的配置参数解析</font>
**<font color=blue>job的restart策略</font>**
```yaml
restartPolicy: Never
```
+ **<font color=red>Nerver </font>** : `只要任务没有完成，则是新创建pod运行，直到job完成 会产生多个pod`
+ **<font color=brown>OnFailure</font>** : `只要pod没有完成，则会重启pod，直到job完成`

**<font color=red>activeDeadlineSeconds：最大可以运行时间</font>**

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl explain jobs.spec | grep act
   activeDeadlineSeconds        <integer>
     may be continuously active before the system tries to terminate it; value
     given time. The actual number of pods running in steady state will be less
     false to true), the Job controller will delete all active Pods associated
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$vim myjobact.yaml
```
**<font color=green>使用`activeDeadlineSeconds：最大可以运行时间`创建一个job</font>**
**<font color=amber> myjobact.yaml</font>**
```yaml
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: my-job
spec:
  template:
    metadata:
      creationTimestamp: null
    spec:
      activeDeadlineSeconds: 5 #最大可以运行时间
      containers:
      - command:
        - sh
        - -c
        - echo "hello jobs"
        - sleep 15
        image: busybox
        name: my-job
        resources: {}
      restartPolicy: Never
status: {}
```
**<font color=camel>超过5秒任务没有完成，所以从新创建一个pod运行</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl delete  -f myjob.yaml
job.batch "my-job" deleted
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  apply  -f myjobact.yaml
job.batch/my-job created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME              READY   STATUS              RESTARTS   AGE
my-job--1-ddhbj   0/1     ContainerCreating   0          7s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl get jobs
NAME     COMPLETIONS   DURATION   AGE
my-job   0/1           16s        16s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME              READY   STATUS              RESTARTS   AGE
my-job--1-ddhbj   0/1     Completed           0          23s
my-job--1-mzw2p   0/1     ContainerCreating   0          3s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME              READY   STATUS      RESTARTS   AGE
my-job--1-ddhbj   0/1     Completed   0          48s
my-job--1-mzw2p   0/1     Completed   0          28s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl get jobs
NAME     COMPLETIONS   DURATION   AGE
my-job   0/1           55s        55s
```
>**<font color=royalblue>其他的一些参数</font>**

**<font color=red>parallelism: N 一次性运行N个pod</font>**
**<font color=green>completions: M job结束需要成功运行的Pod个数，即状态为Completed的pod数</font>**
**<font color=yellowgreen>backoffLimit: N 如果job失败，则重试几次</font>**
**<font color=orange>parallelism：一次性运行几个pod，这个值不会超过completions的值。</font>**

### <font color=amber>创建一个并行多任务的Job</font>

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: my-job
spec:
  backoffLimit: 6  #重试次数
  completions: 6 # 运行几次
  parallelism: 2 # 一次运行几个
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
      - command:
        - sh
        - -c
        - echo "hello jobs"
        - sleep 15
        image: busybox
        name: my-job
        resources: {}
      restartPolicy: Never
status: {}
```
**<font color=chocolate>创建一个有参数的job</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  apply  -f myjob-parma.yaml
job.batch/my-job created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods jobs
Error from server (NotFound): pods "jobs" not found
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods job
Error from server (NotFound): pods "job" not found
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get jobs
NAME     COMPLETIONS   DURATION   AGE
my-job   0/6           19s        19s
```
**<font color=blue>查看参数设置的变化，运行6个job</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME              READY   STATUS              RESTARTS   AGE
my-job--1-9vvst   0/1     Completed           0          25s
my-job--1-h24cw   0/1     ContainerCreating   0          5s
my-job--1-jgq2j   0/1     Completed           0          24s
my-job--1-mbmg6   0/1     ContainerCreating   0          1s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get jobs
NAME     COMPLETIONS   DURATION   AGE
my-job   2/6           35s        35s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get jobs
NAME     COMPLETIONS   DURATION   AGE
my-job   3/6           48s        48s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$
└─$kubectl  get pods
NAME              READY   STATUS      RESTARTS   AGE
my-job--1-9vvst   0/1     Completed   0          91s
my-job--1-b95qv   0/1     Completed   0          35s
my-job--1-h24cw   0/1     Completed   0          71s
my-job--1-jgq2j   0/1     Completed   0          90s
my-job--1-mbmg6   0/1     Completed   0          67s
my-job--1-njbfj   0/1     Completed   0          49s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get jobs
NAME     COMPLETIONS   DURATION   AGE
my-job   6/6           76s        93s
```
### **<font color=chocolate>实战:计算圆周率2000位</font>**
**<font color=seagreen>命令行的方式创建一个job</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl create job job3 --image=perl  --dry-run=client -o yaml -- perl -Mbignum=bpi -wle 'print bpi(500)'
apiVersion: batch/v1
kind: Job
metadata:
  creationTimestamp: null
  name: job3
spec:
  template:
    metadata:
      creationTimestamp: null
    spec:
      containers:
      - command:
        - perl
        - -Mbignum=bpi
        - -wle
        - print bpi(500)
        image: perl
        name: job3
        resources: {}
      restartPolicy: Never
status: {}
```
**<font color=camel>拉取相关镜像，命令行创建job</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m shell -a "docker pull perl"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl create job job2 --image=perl -- perl -Mbignum=bpi -wle 'print bpi(500)'
job.batch/job2 created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME            READY   STATUS      RESTARTS   AGE
job2--1-5jlbl   0/1     Completed   0          2m4s
```
**<font color=purple>查看运行的job输出</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl logs job2--1-5jlbl
3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456485669234603486104543266482133936072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491
```



## <font color=tomato>Cronjob(定时任务)</font>

**<font color=orange>在 cronjob 的 yaml 文件里的 `.spec.jobTemplate.spec` 字段里，可以写 `activeDeadlineSeconds` 参数，指定 `cronjob` 所生成的 pod 只能运行多久</font>**

**<font color=tomato>`Kubernetes从1.5`版本开始增加了一种新类型的Job,即类似LinuxCron的定时任务`Cron Job`,下面看看如何定义和使用这种类型的Job首先,确保`Kubernetes的版本为1.8及以上`。 </font>**

**<font color=blue>在`Kubernetes 1.9`版本后，`kubectl`命令增加了别名`cj`来表示`cronjob`，同时`kubectl set image/env`命令也可以作用在`CronJob`对象上了。</font>**

### <font color=camel>创建一个 Cronjob</font>
**<font color=yellowgreen>每分钟创建一个pod执行一个date命令</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl create cronjob test-job --image=busybox --schedule="*/1 * * * *"  --dry-run=client   -o yaml -- /bin/sh -c "date"
apiVersion: batch/v1
kind: CronJob
metadata:
  creationTimestamp: null
  name: test-job
spec:
  jobTemplate:
    metadata:
      creationTimestamp: null
      name: test-job
    spec:
      template:
        metadata:
          creationTimestamp: null
        spec:
          containers:
          - command:
            - /bin/sh
            - -c
            - date
            image: busybox
            name: test-job
            resources: {}
          restartPolicy: OnFailure
  schedule: '*/1 * * * *'
status: {}
```
**<font color=brown>可是使用yaml文件或者命令行的方式创建</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
No resources found in liruiling-job-create namespace.
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  apply  -f jobcron.yaml
cronjob.batch/test-job configured
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get job
NAME                COMPLETIONS   DURATION   AGE
test-job-27330246   0/1           0s         0s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME                         READY   STATUS    RESTARTS   AGE
test-job-27330246--1-xn5r6   1/1     Running   0          4s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get pods
NAME                         READY   STATUS      RESTARTS   AGE
test-job-27330246--1-xn5r6   0/1     Completed   0          100s
test-job-27330247--1-9blnp   0/1     Completed   0          40s

```
**<font color=purple>运行`--watch`命令,可以更直观地了解Cron Job定期触发任务执行的历史和现状:</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  apply  -f jobcron.yaml
cronjob.batch/test-job created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get cronjobs
NAME       SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
test-job   */1 * * * *   False     0        <none>          12s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get jobs --watch
NAME                COMPLETIONS   DURATION   AGE
test-job-27336917   0/1                      0s
test-job-27336917   0/1           0s         0s
test-job-27336917   1/1           25s        25s
test-job-27336918   0/1                      0s
test-job-27336918   0/1           0s         0s
test-job-27336918   1/1           26s        26s
^C┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$kubectl  get jobs -o wide
NAME                COMPLETIONS   DURATION   AGE    CONTAINERS   IMAGES    SELECTOR
test-job-27336917   1/1           25s        105s   test-job     busybox   controller-uid=35e43bbc-5869-4bda-97db-c027e9a36b97
test-job-27336918   1/1           26s        45s    test-job     busybox   controller-uid=82d2e4a5-716c-42bf-bc7d-3137dd0e50e8
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-jobs-create]
└─$
```
