---
title: 关于 Kubernetes中Pod健康检测和服务可用性检查的一些笔记(LivenessProbe+ReadinessProbe)
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2021-12-18 19:20:30/关于 Kubernetes中Pod健康检测和服务可用性检查的一些笔记.html'
mathJax: false
date: 2021-12-19 03:20:30
thumbnail:
---
**<font color="009688"> 中秋明月，豪门有，贫家也有。极慰人心。  ——烽火戏诸侯《剑来》**</font>
<!-- more -->
## 写在前面
***
+ 学习`k8s`这里整理记忆
+ 博文内容涉及：
+ `LivenessProbe`,`ReadinessProbe`两种`探针`的一些基本理论
+ `ExecAction`,`TCPSocketAction`,`HTTPGetAction`三种`检测方式`的Demo


**<font color="009688"> 中秋明月，豪门有，贫家也有。极慰人心。  ——烽火戏诸侯《剑来》**</font>
 ***

# <font color=seagreen>Pod健康检查和服务可用性检查</font>

## <font color=tomato>健康检查的目的</font>
**<font color=camel>`探测的目的`: 用来维持 pod的健壮性,当pod挂掉之后，deployment会生成新的pod,但如果pod是正常运行的，但pod里面出了问题，此时deployment是监测不到的。故此需要探测(probe)-pod是不是正常提供服务的</font>**

## <font color=yellowgreen>探针类似</font>
**<font color=green>`Kubernetes` 对 `Pod` 的健康状态可以通过两类探针来检查:`LivenessProbe `和`ReadinessProbe`, kubelet定期执行这两类探针来诊断容器的健康状况。都是通过deployment实现的</font>**

|探针类型|描述|
|--|--|
|**<font color=amber>LivenessProbe探针</font>** | **<font color=seagreen> 用于判断容器是否存活(Running状态) ,如果LivenessProbe探针探测到容器不健康,则kubelet将杀掉该容器,并根据容器的重启策略做相应的处理。如果一个容器不包含LivenesspProbe探针,那么kubelet认为该容器的LivenessProbe探针返回的值永远是Success。</font>**|
|**<font color=amber>ReadinessProbe探针</font>** | **<font color=seagreen>用于判断容器服务是否可用(Ready状态) ,达到Ready状态的Pod才可以接收请求。对于被Service管理的Pod, Service与Pod Endpoint的关联关系也将基于Pod是否Ready进行设置。如果在运行过程中Ready状态变为False,则系统自动将其从Service的后端Endpoint列表中隔离出去,后续再把恢复到Ready状态的Pod加回后端Endpoint列表。这样就能保证客户端在访问Service时不会被转发到服务不可用的Pod实例上。 </font>**|

## <font color=tomato>检测方式及参数配置</font>
**<font color=chocolate>`LivenessProbe`和`ReadinessProbe`均可配置以下三种实现方式。</font>**

|方式|描述|
|--|--|
|**<font color=plum>ExecAction</font>**|在容器内部执行一个命令,如果该命令的返回码为0,则表明容器健康。|
|**<font color=purple> TCPSocketAction</font>**|通过容器的IP地址和端口号执行TC检查,如果能够建立TCP连接,则表明容器健康。|
|**<font color=amber>HTTPGetAction</font>**|通过容器的IP地址、端口号及路径调用HTTP Get方法,如果响应的状态码大于等于200且小于400,则认为容器健康。 |

**<font color=purple>对于每种探测方式，需要设置`initialDelaySeconds`和`timeoutSeconds`等参数，它们的含义分别如下。</font>**

|参数|描述|
|--|--|
|**<font color=amber>initialDelaySeconds：</font>** |启动容器后进行首次健康检查的等待时间，单位为s。|
|**<font color=plum>timeoutSeconds:</font>** |健康检查发送请求后等待响应的超时时间,单位为s。当超时发生时, kubelet会认为容器已经无法提供服务,将会重启该容器。|
|**<font color=orange>periodSeconds</font>**|执行探测的频率，默认是10秒，最小1秒。|
|**<font color=orange>successThreshold</font>**|探测失败后，最少连续探测成功多少次才被认定为成功，默认是1，对于liveness必须是1，最小值是1。|
|**<font color=camel>failureThreshold</font>**|当 Pod 启动了并且探测到失败，Kubernetes 的重试次数。存活探测情况下的放弃就意味着重新启动容器。就绪探测情况下的放弃 Pod 会被打上未就绪的标签。默认值是 3。最小值是 1|


**<font color=yellowgreen>Kubernetes的ReadinessProbe机制可能无法满足某些复杂应用对容器内服务可用状态的判断</font>**

>所以Kubernetes从1.11版本开始,引入PodReady++特性对Readiness探测机制进行扩展,在1.14版本时达到GA稳定版,称其为Pod Readiness Gates。

>通过Pod Readiness Gates机制,用户可以将自定义的ReadinessProbe探测方式设置在Pod上,辅助Kubernetes设置Pod何时达到服务可用状态(Ready) 。为了使自定义的ReadinessProbe生效,用户需要提供一个外部的控制器(Controller)来设置相应的Condition状态。

**<font color=yellowgreen>Pod的Readiness Gates在Pod定义中的ReadinessGate字段进行设置。下面的例子设置了一个类型为www.example.com/feature-1的新ReadinessGate:</font>**

|--|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/92bcd18c1e1d45b894c798a1ee66856d.png)|
|**<font color=green>新增的自定义Condition的状态(status)将由用户自定义的外部控·制器设置,默认值为False. Kubernetes将在判断全部readinessGates条件都为True时,才设置Pod为服务可用状态(Ready为True) 。</font>**|
|**<font color=camel>这个不是太懂，需要以后再研究下</font>**|

### <font color=orange>学习环境准备</font>
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$mkdir liveness-probe
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cd liveness-probe/
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  create ns liveness-probe
namespace/liveness-probe created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl config current-context
kubernetes-admin@kubernetes
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl config set-context $(kubectl config current-context) --namespace=liveness-probe
Context "kubernetes-admin@kubernetes" modified.
```

## <font color=purple>LivenessProbe探针</font>
**<font color=orange>用于判断容器是否存活(Running状态) ,如果LivenessProbe探针探测到容器不健康,则kubelet将杀掉该容器,并根据容器的重启策略做相应的处理</font>**
### <font color=seagreen>ExecAction方式：command </font> 
**<font color=green>在容器内部执行一个命令,如果该命令的返回码为0,则表明容器健康。</font>**

**<font color=tomato>资源文件定义</font>**
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$cat liveness-probe.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-liveness
  name: pod-liveness
spec:
  containers:
  - args:
    - /bin/sh
    - -c
    - touch /tmp/healthy; sleep 30; rm -rf /tmp/healthy; slee 10
    livenessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 5 #容器启动的5s内不监测
      periodSeconds: 5 #每5s钟检测一次
    image: busybox
    imagePullPolicy: IfNotPresent
    name: pod-liveness
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always 
status: {}
```
**<font color=purple>运行这个deploy。当pod创建成功后，新建文件，并睡眠30s，删掉文件在睡眠。使用liveness检测文件的存在</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  apply  -f liveness-probe.yaml
pod/pod-liveness created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  get pods
NAME           READY   STATUS    RESTARTS     AGE
pod-liveness   1/1     Running   1 (8s ago)   41s   # 30文件没有重启
```
**<font color=yellowgreen>运行超过30s后。文件被删除，所以被健康检测命中，pod根据重启策略重启</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  get pods
NAME           READY   STATUS    RESTARTS      AGE
pod-liveness   1/1     Running   2 (34s ago)   99s
```
**<font color=yellowgreen>99s后已经从起了第二次</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.83 -m shell -a "docker ps | grep pod-liveness"
192.168.26.83 | CHANGED | rc=0 >>
00f4182c014e   7138284460ff                                        "/bin/sh -c 'touch /…"   6 seconds ago   Up 5 seconds             k8s_pod-liveness_pod-liveness_liveness-probe_81b4b086-fb28-4657-93d0-bd23e67f980a_0
01c5cfa02d8c   registry.aliyuncs.com/google_containers/pause:3.5   "/pause"                 7 seconds ago   Up 6 seconds             k8s_POD_pod-liveness_liveness-probe_81b4b086-fb28-4657-93d0-bd23e67f980a_0
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get pods
NAME           READY   STATUS    RESTARTS   AGE
pod-liveness   1/1     Running   0          25s
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get pods
NAME           READY   STATUS    RESTARTS      AGE
pod-liveness   1/1     Running   1 (12s ago)   44s
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.83 -m shell -a "docker ps | grep pod-liveness"
192.168.26.83 | CHANGED | rc=0 >>
1eafd7e8a12a   7138284460ff                                        "/bin/sh -c 'touch /…"   15 seconds ago   Up 14 seconds             k8s_pod-liveness_pod-liveness_liveness-probe_81b4b086-fb28-4657-93d0-bd23e67f980a_1
01c5cfa02d8c   registry.aliyuncs.com/google_containers/pause:3.5   "/pause"                 47 seconds ago   Up 47 seconds             k8s_POD_pod-liveness_liveness-probe_81b4b086-fb28-4657-93d0-bd23e67f980a_0
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
**<font color=amber>查看节点机docker中的容器ID，前后不一样，确定是POD被杀掉后重启。</font>**

### <font color=tomato>HTTPGetAction的方式</font>
**<font color=seagreen>通过容器的IP地址、端口号及路径调用HTTP Get方法,如果响应的状态码大于等于200且小于400,则认为容器健康。 </font>**
**<font color=plum>创建资源文件，即相关参数使用</font>**
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$cat liveness-probe-http.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-livenss-probe
  name: pod-livenss-probe
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-livenss-probe
    livenessProbe:
      failureThreshold: 3 #当 Pod 启动了并且探测到失败，Kubernetes 的重试次数
      httpGet:
        path: /index.html
        port: 80
        scheme: HTTP
      initialDelaySeconds: 10  #容器启动后第一次执行探测是需要等待多少秒
      periodSeconds: 10   #执行探测的频率，默认是10秒，最小1秒
      successThreshold: 1 #探测失败后，最少连续探测成功多少次才被认定为成功
      timeoutSeconds: 10 #探测超时时间，默认1秒，最小1秒
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
**<font color=amber>运行deploy，这个的探测机制访问Ngixn的默认欢迎页</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$vim liveness-probe-http.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl apply  -f liveness-probe-http.yaml
pod/pod-livenss-probe created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  get pods
NAME                READY   STATUS    RESTARTS   AGE
pod-livenss-probe   1/1     Running   0          15s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl exec -it pod-livenss-probe -- rm /usr/share/nginx/html/index.html
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  get pods
NAME                READY   STATUS    RESTARTS     AGE
pod-livenss-probe   1/1     Running   1 (1s ago)   2m31s

```
**<font color=chocolate>当欢迎页被删除时，访问报错，被检测命中,pod重启</font>**

### <font color=tomato>TCPSocketAction方式</font>
**<font color=purple>通过容器的IP地址和端口号执行TCP检查,如果能够建立TCP连接,则表明容器健康。</font>**
**<font color=chocolate>资源文件定义</font>**
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$cat liveness-probe-tcp.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-livenss-probe
  name: pod-livenss-probe
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-livenss-probe
    livenessProbe:
      failureThreshold: 3
      tcpSocket:
        port: 8080
      initialDelaySeconds: 10
      periodSeconds: 10
      successThreshold: 1
      timeoutSeconds: 10
    resources: {}
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
**<font color=camel>访问8080端口，但是8080端口未开放，所以访问会超时，不能建立连接，命中检测，重启Pod</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  apply  -f liveness-probe-tcp.yaml
pod/pod-livenss-probe created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  get pods
NAME                READY   STATUS    RESTARTS   AGE
pod-livenss-probe   1/1     Running   0          8s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  get pods
NAME                READY   STATUS    RESTARTS     AGE
pod-livenss-probe   1/1     Running   1 (4s ago)   44s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$
```

## <font color=yellowgreen>ReadinessProbe探针</font>  
**<font color=brown>用于判断容器服务是否可用(Ready状态) ,达到Ready状态的Pod才可以接收请求。负责不能进行访问</font>**
### <font color=purple>ExecAction方式：command</font> 
**<font color=royalblue>资源文件定义，使用钩子建好需要检查的文件</font>**
```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$cat readiness-probe.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-liveness
  name: pod-liveness
spec:
  containers:
  - readinessProbe:
      exec:
        command:
        - cat
        - /tmp/healthy
      initialDelaySeconds: 5 #容器启动的5s内不监测
      periodSeconds: 5 #每5s钟检测一次
    image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-liveness
    resources: {}
    lifecycle:
      postStart:
        exec:
          command: ["/bin/sh", "-c","touch /tmp/healthy"]
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```
**<font color=amber>创建3个有Ngixn的pod,通过POD创建一个SVC做测试用</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$sed 's/pod-liveness/pod-liveness-1/' readiness-probe.yaml | kubectl apply  -f -
pod/pod-liveness-1 created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$sed 's/pod-liveness/pod-liveness-2/' readiness-probe.yaml | kubectl apply  -f -
pod/pod-liveness-2 created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  get pods -o wide
NAME             READY   STATUS    RESTARTS   AGE    IP             NODE                         NOMINATED NODE   READINESS GATES
pod-liveness     1/1     Running   0          3m1s   10.244.70.50   vms83.liruilongs.github.io   <none>           <none>
pod-liveness-1   1/1     Running   0          2m     10.244.70.51   vms83.liruilongs.github.io   <none>           <none>
pod-liveness-2   1/1     Running   0          111s   10.244.70.52   vms83.liruilongs.github.io   <none>           <none>
```
**<font color=amber>修改主页文字</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$serve=pod-liveness
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl exec  -it $serve -- sh -c "echo $serve > /usr/share/nginx/html/index.html"
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl exec  -it $serve -- sh -c "cat /usr/share/nginx/html/index.html"
pod-liveness
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$serve=pod-liveness-1
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl exec  -it $serve -- sh -c "echo $serve > /usr/share/nginx/html/index.html"
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$serve=pod-liveness-2
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl exec  -it $serve -- sh -c "echo $serve > /usr/share/nginx/html/index.html"
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$
```
**<font color=purple>修改标签</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl get pods --show-labels
NAME             READY   STATUS    RESTARTS   AGE   LABELS
pod-liveness     1/1     Running   0          15m   run=pod-liveness
pod-liveness-1   1/1     Running   0          14m   run=pod-liveness-1
pod-liveness-2   1/1     Running   0          14m   run=pod-liveness-2
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl edit pods pod-liveness-1
pod/pod-liveness-1 edited
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl edit pods pod-liveness-2
pod/pod-liveness-2 edited
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl get pods --show-labels
NAME             READY   STATUS    RESTARTS   AGE   LABELS
pod-liveness     1/1     Running   0          17m   run=pod-liveness
pod-liveness-1   1/1     Running   0          16m   run=pod-liveness
pod-liveness-2   1/1     Running   0          16m   run=pod-liveness
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$
```
**<font color=royalblue>要删除文件检测</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  exec -it pod-liveness -- ls /tmp/
healthy
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  exec -it pod-liveness-1 -- ls /tmp/
healthy
```
**<font color=seagreen>使用POD创建SVC</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl expose  --name=svc pod pod-liveness --port=80
service/svc exposed
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl  get ep
NAME   ENDPOINTS                                         AGE
svc    10.244.70.50:80,10.244.70.51:80,10.244.70.52:80   16s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl get svc
NAME   TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
svc    ClusterIP   10.104.246.121   <none>        80/TCP    36s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/liveness-probe]
└─$kubectl get pods -o wide
NAME             READY   STATUS    RESTARTS   AGE   IP             NODE                         NOMINATED NODE   READINESS GATES
pod-liveness     1/1     Running   0          24m   10.244.70.50   vms83.liruilongs.github.io   <none>           <none>
pod-liveness-1   1/1     Running   0          23m   10.244.70.51   vms83.liruilongs.github.io   <none>           <none>
pod-liveness-2   1/1     Running   0          23m   10.244.70.52   vms83.liruilongs.github.io   <none>           <none>
```
**<font color=green>测试SVC正常，三个POD会正常 负载</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$while true; do curl 10.104.246.121 ; sleep 1
> done
pod-liveness
pod-liveness-2
pod-liveness
pod-liveness-1
pod-liveness-2
^C
```
**<font color=red>删除文件测试</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl exec -it pod-liveness -- rm  -rf /tmp/
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl exec -it pod-liveness -- ls /tmp/
ls: cannot access '/tmp/': No such file or directory
command terminated with exit code 2
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$while true; do curl 10.104.246.121 ; sleep 1; done
pod-liveness-2
pod-liveness-2
pod-liveness-2
pod-liveness-1
pod-liveness-2
pod-liveness-2
pod-liveness-1
^C

```
**<font color=camel>会发现pod-liveness的pod已经不提供服务了</font>**

## <font color=camel>kubeadm 中的一些健康检测</font>
**<font color=blue>kube-apiserver.yaml中的使用，两种探针同时使用</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cat /etc/kubernetes/manifests/kube-apiserver.yaml | grep -A 8 readi
    readinessProbe:
      failureThreshold: 3
      httpGet:
        host: 192.168.26.81
        path: /readyz
        port: 6443
        scheme: HTTPS
      periodSeconds: 1
      timeoutSeconds: 15
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cat /etc/kubernetes/manifests/kube-apiserver.yaml | grep -A 9 liveness
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 192.168.26.81
        path: /livez
        port: 6443
        scheme: HTTPS
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```