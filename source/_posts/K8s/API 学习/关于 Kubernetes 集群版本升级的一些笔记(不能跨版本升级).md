---
title: 关于k8s 集群版本升级的一些笔记(不能跨次要版本升级)
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: "2022-12-14 02:37:25/关于k8s 集群升级的一些笔记.html"
mathJax: false
date: 2022-12-14 10:37:25
thumbnail:
---

**<font color="009688"> 那认识一切而不为任何事物所认识的，就是主体  -----《作为意志和表象的世界》(第一篇 世界作为表象初论)**</font>

<!-- more -->

## 写在前面

---

- 分享一些 K8s 集群版本升级的笔记
- 博文为根据官方文档的版本升级记录
- 以及不同组件的版本偏差要求
- 理解不足小伙伴帮忙指正



**<font color="009688"> 那认识一切而不为任何事物所认识的，就是主体  -----《作为意志和表象的世界》(第一篇 世界作为表象初论)**</font>


---

## 升级 K8S集群

分享一些 基本 kubeadm 升级 K8s 集群版本的笔记， 下面为实际的升级记录，这里一定要注意，不能跨次要版本升级，可以跨补丁版本，即可以 1.26.x 升级到 1.26.y(其中 y > x+1) 或者 1.22.x 升级到 1.23.x ,不能 1.22.x 升级到 1.26.x。

Kubernetes 版本以 `x.y.z` 表示，其中 x 是`主要版本`， y 是`次要版本`，z 是`补丁版本`，遵循语义版本控制术语。

### 支持的版本偏差

`kube-apiserver`: 在高可用性(HA)集群中(可以单纯理解为多 master 节点的情况)， 最新版和最老版的 kube-apiserver 实例版本偏差最多为一个次要版本，即多个 master 节点 的 `kube-apiservice` 版本要求。

`kubelet`: kubelet 版本不能比 kube-apiserver 版本新，并且最多只可落后两个次要版本。如果 HA 集群中的 kube-apiserver 实例之间存在版本偏差，这会缩小允许的 kubelet 版本范围。

`kube-controller-manager、kube-scheduler 和 cloud-controller-manager`: kube-controller-manager、kube-scheduler 和 cloud-controller-manager 不能比与它们通信的 kube-apiserver 实例新。 它们应该与 kube-apiserver 次要版本`相匹配`，但可能最多`旧一个次要版本`。

`kubectl`:kubectl 在 kube-apiserver 的一个次要版本(较旧或较新)中支持。

`kube-proxy`: kube-proxy 和节点上的 kubelet 必须是相同的次要版本。kube-proxy 版本不能比 kube-apiserver 版本新。kube-proxy 最多只能比 kube-apiserver 落后两个次要版本。




升级工作的基本流程如下

- 升级 master 所有节点
- 升级 node 所有节点

这里升级版本为 1.21.1 升级 1.22.2 ，下面为具体的升级步骤，实际操作一般以官方文档为准，尤其涉及一些重大版本变更，比如 1.23 之后不支持 `docker` ，只能使用 `containerd`，还用 `docker` 的话只能添加垫片之类的东西。如果升级版本太低不被支持，只能找其他相关资料。 版本迭代很快，虽然旧版本也一直在维护。还是建议有稳定版本就升级。

#### 确定要升级到哪个版本

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$yum list --showduplicates kubeadm --disableexcludes=kubernetes
# 在列表中查找最新的 1.22 版本
# 它看起来应该是 1.22.x-0，其中 x 是最新的补丁版本
```

现有环境

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get nodes
NAME                         STATUS     ROLES                  AGE   VERSION
vms81.liruilongs.github.io   NotReady   control-plane,master   11m   v1.21.1
vms82.liruilongs.github.io   NotReady   <none>                 12s   v1.21.1
vms83.liruilongs.github.io   NotReady   <none>                 11s   v1.21.1
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

## 升级 master

控制节点上的升级过程应该每次处理一个节点。 首先选择一个要先行升级的控制面节点。该节点上必须拥有 `/etc/kubernetes/admin.conf` 文件。 即管理员使用的 kubeconfig 证书文件

### 执行 "kubeadm upgrade"

#### 升级 kubeadm：

```bash
# 用最新的补丁版本号替换 1.22.x-0 中的 x
┌──[root@vms81.liruilongs.github.io]-[~]
└─$yum install -y kubeadm-1.22.2-0 -`·`-disableexcludes=kubernetes
```

#### 验证下载操作正常，并且 kubeadm 版本正确：

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubeadm version
kubeadm version: &version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.2", GitCommit:"8b5a19147530eaac9476b0ab82980b4088bbc1b2", GitTreeState:"clean", BuildDate:"2021-09-15T21:37:34Z", GoVersion:"go1.16.8", Compiler:"gc", Platform:"linux/amd64"}
```

#### 验证升级计划：

此命令检查你的集群是否可被升级，并取回你要升级的目标版本。 命令也会显示一个包含组件配置版本状态的表格。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubeadm upgrade plan
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -o yaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade] Fetching available versions to upgrade to
[upgrade/versions] Cluster version: v1.21.1
[upgrade/versions] kubeadm version: v1.22.2
[upgrade/versions] Target version: v1.22.2
[upgrade/versions] Latest version in the v1.21 series: v1.21.5
................
```

#### 选择要升级到的目标版本，运行合适的命令

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$sudo kubeadm upgrade apply v1.22.2
............
upgrade/successful] SUCCESS! Your cluster was upgraded to "v1.22.2". Enjoy!

[upgrade/kubelet] Now that your control plane is upgraded, please proceed with upgrading your kubelets if you haven't already done so.
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

k8s 官网提到这里需要升级 CNI 组件，这里要根据实际情况具体分析， 当前不需要。这里如果有其他的 master 节点 ，则需要运行 `sudo kubeadm upgrade node`

#### 设置进入维护模式

通过将节点标记为不可调度并腾空节点为节点作升级准备：

```bash
# 将 <node-to-drain> 替换为你要腾空的控制面节点名称
#kubectl drain <node-to-drain> --ignore-daemonsets
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl drain vms81.liruilongs.github.io --ignore-daemonsets
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

#### 升级 kubelet 和 kubectl

```bash
# 用最新的补丁版本号替换 1.22.x-00 中的 x
#yum install -y kubelet-1.22.x-0 kubectl-1.22.x-0 --disableexcludes=kubernetes
┌──[root@vms81.liruilongs.github.io]-[~]
└─$yum install -y kubelet-1.22.2-0 kubectl-1.22.2-0 --disableexcludes=kubernetes
```

重启 kubelet

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$sudo systemctl daemon-reload
┌──[root@vms81.liruilongs.github.io]-[~]
└─$sudo systemctl restart kubelet
```

解除节点的保护

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl uncordon vms81.liruilongs.github.io
node/vms81.liruilongs.github.io uncordoned
```
master 节点版本以已经替换

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl get nodes
NAME                         STATUS     ROLES                  AGE   VERSION
vms81.liruilongs.github.io   Ready      control-plane,master   11d   v1.22.2
vms82.liruilongs.github.io   NotReady   <none>                 11d   v1.21.1
vms83.liruilongs.github.io   Ready      <none>                 11d   v1.21.1
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

## 升级工作节点 Node

工作节点上的升级过程应该一次执行一个节点，或者一次执行几个节点， 以不影响运行工作负载所需的最小容量。

### 升级 kubeadm

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -a "yum install -y kubeadm-1.22.2-0 --disableexcludes=kubernetes"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -a "sudo kubeadm upgrade node" # 执行 "kubeadm upgrade"  对于工作节点，下面的命令会升级本地的 kubelet 配置：
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get nodes
NAME                         STATUS                     ROLES                  AGE   VERSION
vms81.liruilongs.github.io   Ready                      control-plane,master   12d   v1.22.2
vms82.liruilongs.github.io   Ready                      <none>                 12d   v1.21.1
vms83.liruilongs.github.io   Ready,SchedulingDisabled   <none>                 12d   v1.22.2
```

腾空节点,设置维护状态

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl drain vms82.liruilongs.github.io --ignore-daemonsets
node/vms82.liruilongs.github.io cordoned
WARNING: ignoring DaemonSet-managed Pods: kube-system/calico-node-ntm7v, kube-system/kube-proxy-nzm24
node/vms82.liruilongs.github.io drained
```

### 升级 kubelet 和 kubectl 
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.82 -a "yum install -y kubelet-1.22.2-0 kubectl-1.22.2-0 --disableexcludes=kubernetes"

```

重启 kubelet

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.82 -a "systemctl daemon-reload"
192.168.26.82 | CHANGED | rc=0 >>

┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.82 -a "systemctl restart kubelet"
192.168.26.82 | CHANGED | rc=0 >>
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl get nodes
NAME                         STATUS                     ROLES                  AGE   VERSION
vms81.liruilongs.github.io   Ready                      control-plane,master   13d   v1.22.2
vms82.liruilongs.github.io   Ready,SchedulingDisabled   <none>                 13d   v1.22.2
vms83.liruilongs.github.io   Ready,SchedulingDisabled   <none>                 13d   v1.22.2
```

取消对节点的保护

```
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl uncordon vms82.liruilongs.github.io
node/vms82.liruilongs.github.io uncordoned
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl uncordon vms83.liruilongs.github.io
node/vms83.liruilongs.github.io uncordoned
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl get nodes
NAME                         STATUS   ROLES                  AGE   VERSION
vms81.liruilongs.github.io   Ready    control-plane,master   13d   v1.22.2
vms82.liruilongs.github.io   Ready    <none>                 13d   v1.22.2
vms83.liruilongs.github.io   Ready    <none>                 13d   v1.22.2
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

> kubeadm upgrade apply 做了以下工作：

- 检查你的集群是否处于可升级状态:
  - API 服务器是可访问的
  - 所有节点处于 Ready 状态
  - 控制面是健康的
- 强制执行版本偏差策略。
- 确保控制面的镜像是可用的或可拉取到服务器上。
- 如果组件配置要求版本升级，则生成替代配置与/或使用用户提供的覆盖版本配置。
- 升级控制面组件或回滚(如果其中任何一个组件无法启动)。
- 应用新的 CoreDNS 和 kube-proxy 清单，并强制创建所有必需的 RBAC 规则。
- 如果旧文件在 180 天后过期，将创建 API 服务器的新证书和密钥文件并备份旧文件。

> kubeadm upgrade node 在其他控制平节点上执行以下操作：

- 从集群中获取 kubeadm ClusterConfiguration。
- (可选操作)备份 kube-apiserver 证书。
- 升级控制平面组件的静态 Pod 清单。
- 为本节点升级 kubelet 配置

> kubeadm upgrade node 在工作节点上完成以下工作：

- 从集群取回 kubeadm ClusterConfiguration。
- 为本节点升级 kubelet 配置。

## 博文参考

---

https://kubernetes.io/zh-cn/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/

https://kubernetes.io/zh-cn/releases/version-skew-policy/#kube-controller-manager-kube-scheduler-and-cloud-controller-manager


