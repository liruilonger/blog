---
title: 关于 Kubernetes中kube-apiserver的一些笔记
tags:
  - Kubernetes
  - kube-apiserver
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: kube-apiserver
uniqueId: '2021-12-16 15:53:26/关于 Kubernetes中kube-apiserver的一些笔记.html'
mathJax: false
date: 2021-12-16 23:53:26
thumbnail:
---
**<font color=009688>年轻游侠儿泪眼模糊，凄然一笑，站起身，拿木剑对准墙壁，狠狠折断。</font>**

**<font color=009688>此后江湖再无温华的消息，这名才出江湖便已名动天下的木剑游侠儿，一夜之间，以最决然的苍凉姿态，离开了江湖。</font>**

**<font color=009688>刺骨大雪中，他最后对自己说了一句。</font>**

**<font color=009688>“不练剑了。”                    ——烽火戏诸侯《雪中悍刀行》</font>**
<!-- more -->

## 写在前面
***
+ 学习K8s，简单整理下，都是书里的东西。
+ 官网很详细，小伙伴系统学习可以到官网
+ [https://kubernetes.io/zh/docs/tasks/administer-cluster/access-cluster-api/](https://kubernetes.io/zh/docs/tasks/administer-cluster/access-cluster-api/)

**<font color=009688>年轻游侠儿泪眼模糊，凄然一笑，站起身，拿木剑对准墙壁，狠狠折断。</font>**

**<font color=009688>此后江湖再无温华的消息，这名才出江湖便已名动天下的木剑游侠儿，一夜之间，以最决然的苍凉姿态，离开了江湖。</font>**

**<font color=009688>刺骨大雪中，他最后对自己说了一句。</font>**

**<font color=009688>“不练剑了。”                    ——烽火戏诸侯《雪中悍刀行》</font>**
 ***

## <font color=seagreen>Kubernetes API Server原理分析</font>

官网很详细，小伙伴系统学习可以到官网：[https://kubernetes.io/zh/docs/tasks/administer-cluster/access-cluster-api/](https://kubernetes.io/zh/docs/tasks/administer-cluster/access-cluster-api/)

**<font color=purple> `Kubernetes API Server`的核心功能是提供了Kubernetes各类资源对象(如Pod,RC, Service等)的增、删、改、查及Watch等HTTP Rest接口,接受外部请求，并将信息写到ETCD中,成为集群内各个功能模块之间数据交互和通信的中心枢纽,是整个系统的数据总线和数据中心。除此之外,它还有以下一些功能特性。</font>**

**<font color=orange>(1)是集群管理的API入口。</font>**

**<font color=tomato>(2)是资源配额控制的入口。</font>**

**<font color=royalblue>(3)提供了完备的集群安全机制。</font>**

### <font color=green>理论概述</font>

**<font color=purple>`Kubernetes API Server`通过一个名为`kube-apiserver`的进程提供服务,该进程运行在`Master节点`上,如果小伙伴使用二进制方式安装k8s，会发现，kube-apiserver是docker之后第一个要启动的服务</font>**

**<font color=chocolate>旧版本中`kube-apiserver`进程在本机的`8080`端口(对应参数`-insecure-port`)提供REST服务。</font>**

**<font color=orange>新版本中启动HTTPS安全端口(`--secure-port=6443`)来启动安全机制,加强REST API访问的安全性。这里需要说明的是，好像是从1.20开始就不支持了，在apiserver配置文件里添加 --insecure-port=8080会导致启动不了，所以不在支持直接http的方式访问(可以用代理)</font>**

**<font color=amber>在高版本的环境中，有时候环境起不来，会报错说6443端口没有开放,我们需要确认`kube-apiserver`服务是否启动成功</font>**

**<font color=tomato>通常我们可以通过命令行工具`kubectl`来与`Kubernetes API Server`交互,它们之间的接口是`REST`调用。</font>**



#### <font color=brown>使用 kubectl 代理</font>
**<font color=chocolate>如果我们只想对外暴露部分`REST`服务,则可以在`Master`或其他任何节点上通过运行`kubectl proxy`进程启动一个内部代理来实现。</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl proxy --port=8080 &
[1] 43454
┌──[root@vms81.liruilongs.github.io]-[~]
└─$Starting to serve on 127.0.0.1:8080

┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl http://localhost:8080/api/ > kubeapi
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   185  100   185    0     0   2863      0 --:--:-- --:--:-- --:--:--  2936
┌──[root@vms81.liruilongs.github.io]-[~]
└─$head -20 kubeapi
{
  "kind": "APIVersions",
  "versions": [
    "v1"
  ],
  "serverAddressByClientCIDRs": [
    {
      "clientCIDR": "0.0.0.0/0",
      "serverAddress": "192.168.26.81:6443"
    }
  ]
}┌──[root@vms81.liruilongs.github.io]-[~]
└─$jobs
[1]+  Running                 kubectl proxy --port=8080 &
┌──[root@vms81.liruilongs.github.io]-[~]
└─$fg
kubectl proxy --port=8080
^C
```
**<font color=red>当然,我们也可以拒绝访问某一资源，比如pod</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl proxy  --reject-paths="^/api/v1/pod" --port=8080 --v=1 &
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl http://localhost:8080/api/v1/pods
Forbidden
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl http://localhost:8080/api/v1/configmaps > $(mktemp kube.XXXXXX)
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 25687    0 25687    0     0   644k      0 --:--:-- --:--:-- --:--:--  660k
┌──[root@vms81.liruilongs.github.io]-[~]
└─$head  -5 kube.zYxKiH
{
  "kind": "ConfigMapList",
  "apiVersion": "v1",
  "metadata": {
    "resourceVersion": "81890"
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl http://localhost:8080/api/v1/secrets | head -5
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0{
  "kind": "SecretList",
  "apiVersion": "v1",
  "metadata": {
    "resourceVersion": "82039"
100 65536    0 65536    0     0  1977k      0 --:--:-- --:--:-- --:--:-- 2064k
curl: (23) Failed writing body (0 != 16378)
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
**<font color=green>kubectl proxy具有很多特性,最实用的一个特性是提供简单有效的安全机制,比如采用白名单来限制非法客户端访问时,只要增加下面这个参数即可:</font>**
```bash
--accept-hosts="^localhost$, ^127\\.0\\.0\\.1$,^\\[::1\\]$"
```


#### <font color=seagreen>不使用 kubectl 代理</font>
**<font color=tomato>通过将身份认证令牌直接传给 API 服务器，可以避免使用 kubectl 代理，像这样：</font>** 使用 grep/cut 方式：

```bash
# 查看所有的集群，因为你的 .kubeconfig 文件中可能包含多个上下文
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl config view -o jsonpath='{"Cluster name\tServer\n"}{range .clusters[*]}{.name}{"\t"}{.cluster.server}{"\n"}{end}'
Cluster name    Server
kubernetes      https://192.168.26.81:6443

# 从上述命令输出中选择你要与之交互的集群的名称
┌──[root@vms81.liruilongs.github.io]-[~]
└─$export CLUSTER_NAME=kubernetes
# 指向引用该集群名称的 API 服务器
┌──[root@vms81.liruilongs.github.io]-[~]
└─$APISERVER=$(kubectl config view -o jsonpath="{.clusters[?(@.name==\"$CLUSTER_NAME\")].cluster.server}")
┌──[root@vms81.liruilongs.github.io]-[~]
└─$echo $APISERVER
https://192.168.26.81:6443

# 获得令牌
┌──[root@vms81.liruilongs.github.io]-[~]
└─$TOKEN=$(kubectl  get secret  default-token-xg77h -o jsonpath='{.data.token}' -n kube-system | base64 -d)

# 使用令牌玩转 API
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl -X GET $APISERVER/api --header "Authorization: Bearer $TOKEN" --insecure
{
  "kind": "APIVersions",
  "versions": [
    "v1"
  ],
  "serverAddressByClientCIDRs": [
    {
      "clientCIDR": "0.0.0.0/0",
      "serverAddress": "192.168.26.81:6443"
    }
  ]
}
```
### <font color=seagreen>编程方式访问 API</font>
#### <font color=chocolate>python</font>
**<font color=plum>要使用 Python 客户端，运行下列命令： pip install kubernete</font>**
```bash
PS E:\docker> pip install kubernetes
Collecting kubernetes
  Using cached kubernetes-21.7.0-py2.py3-none-any.whl (1.8 MB)
  ............
```
**<font color=green>将 ~/.kube 的config文件的内容复制到本地目录，保存为文件kubeconfig.yaml</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$cp .kube/config    kubeconfig.yaml
```

|python|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/8aaf37331ff4433a9fd59bab6085fd1c.png)|

```py
#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   k8s_api.py
@Time    :   2021/12/16 23:05:02
@Author  :   Li Ruilong
@Version :   1.0
@Contact :   1224965096@qq.com
@Desc    :   K8s Demo
'''

# here put the import lib
from kubernetes import client, config

config.kube_config.load_kube_config(config_file="kubeconfig.yaml")

v1=client.CoreV1Api()
print("Listing pods with their IPs:")
ret = v1.list_pod_for_all_namespaces(watch=False)
for i in ret.items:
    print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))
```
**<font color=royalblue>输出所有的pod和对应的node IP</font>**
```bash
PS D:\code\blogger\blogger\资源> python .\k8s_api.py
Listing pods with their IPs:
10.244.88.67    kube-system     calico-kube-controllers-78d6f96c7b-85rv9
192.168.26.81   kube-system     calico-node-6nfqv
192.168.26.83   kube-system     calico-node-fv458
192.168.26.82   kube-system     calico-node-h5lsq
.................
```
#### Java

```bash
# 克隆 Java 库
git clone --recursive https://github.com/kubernetes-client/java
```
|java的客户端|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/a75a5340152e491b98e429b336a38fec.png)|

```java
package io.kubernetes.client.examples;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.openapi.apis.CoreV1Api;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1PodList;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.KubeConfig;
import java.io.FileReader;
import java.io.IOException;

/**
 * A simple example of how to use the Java API from an application outside a kubernetes cluster
 *
 * <p>Easiest way to run this: mvn exec:java
 * -Dexec.mainClass="io.kubernetes.client.examples.KubeConfigFileClientExample"
 *
 * <p>From inside $REPO_DIR/examples
 */
public class KubeConfigFileClientExample {
  public static void main(String[] args) throws IOException, ApiException {

    // file path to your KubeConfig
    String kubeConfigPath = "D:\\code\\k8s\\java\\examples\\examples-release-10\\src\\main\\java\\io\\kubernetes\\client\\examples\\config";

    // loading the out-of-cluster config, a kubeconfig from file-system
    ApiClient client =
        ClientBuilder.kubeconfig(KubeConfig.loadKubeConfig(new FileReader(kubeConfigPath))).build();

    // set the global default api-client to the in-cluster one from above
    Configuration.setDefaultApiClient(client);

    // the CoreV1Api loads default api-client from global configuration.
    CoreV1Api api = new CoreV1Api();

    // invokes the CoreV1Api client
    V1PodList list =
        api.listPodForAllNamespaces(null, null, null, null, null, null, null, null, null);
    for (V1Pod item : list.getItems()) {
      System.out.println(item.getMetadata().getName());
    }
  }
}
```
**<font color=purple>输出所有pod</font>**
```bash
D:\Java\jdk1.8.0_251\bin\java.exe 。。。
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
Bad level value for property: .level
Bad level value for property: java.util.logging.ConsoleHandler.level
calico-kube-controllers-78d6f96c7b-85rv9
calico-node-6nfqv
calico-node-fv458
calico-node-h5lsq
coredns-7f6cbbb7b8-ncd2s
coredns-7f6cbbb7b8-pjnct
etcd-vms81.liruilongs.github.io
。。。。。。。。。
```


### <font color=chocolate>独特的Kubernetes Proxy API接口</font>
**<font color=camel>`Kubernetes Proxy API`接口,作用是代理REST请求,即`Kubernetes API Server`把收到的`REST`请求转发到某个`Node`上的`kubelet·`守护进程的`REST`端口上,由该`kubelet`进程负责响应。</font>**



```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl  proxy  8080 &
[1] 76543
┌──[root@vms81.liruilongs.github.io]-[~]
└─$Starting to serve on 127.0.0.1:8001
┌──[root@vms81.liruilongs.github.io]-[~]
└─$kubectl  get nodes
NAME                         STATUS     ROLES                  AGE   VERSION
vms81.liruilongs.github.io   Ready      control-plane,master   4d    v1.22.2
vms82.liruilongs.github.io   Ready      <none>                 4d    v1.22.2
vms83.liruilongs.github.io   NotReady   <none>                 4d    v1.22.2

```
REST接口方式调用
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl http://localhost:8001/api/
{
  "kind": "APIVersions",
  "versions": [
    "v1"
  ],
  "serverAddressByClientCIDRs": [
    {
      "clientCIDR": "0.0.0.0/0",
      "serverAddress": "192.168.26.81:6443"
    }
  ]
}┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl http://localhost:8001/api/v1/namespaces/default/pods
{
  "kind": "PodList",
  "apiVersion": "v1",
  "metadata": {
    "resourceVersion": "92086"
  },
  "items": []
}
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl http://localhost:8080/api/v1/nodes/vms82.liruilongs.github.io/
```

**<font color=green>需要说明的是:这里获取的Pod的信息数据来自Node而非etcd数据库,所以两者可能在·某些时间点会有偏差。此外,如果kubelet进程在启动时包含`-enable-debugging-handlers=true`参数,那么`Kubernetes Proxy API`还会增加其他的接口信息 </font>**

### <font color=tomato>集群功能模块之间的通信</font>

**<font color=red>Kubernetes API Server</font>** 作为集群的核心,负责集群各功能模块之间的通信。集群内的各个功能模块通过API Server将信息存入etcd,当需要获取和操作这些数据时,则通过`API Server`提供的`REST接口`(用GET, LIST或WATCH方法)来实现,从而实现各模块之间的信息交互。

#### <font color=orange>交互场景</font>:

**<font color=amber>`kubelet`进程与`API Server`的交互</font>** `每个Node节点上的kubelet每隔一个时间周期,就会调用一次API Server的REST接口报告自身状态`, **<font color=yellowgreen>API Server接收到这些信息后,将节点状态信息更新到etcd中。</font>** , **<font color=tomato>kubelet也通过API Server的`Watch接口监听Pod信息`,如果监听到新的Pod副本被调度绑定到本节点,则执行Pod对应的容器的创建和启动逻辑;如果监听到Pod对象被删除,则删除本节点上的相应的Pod容器;如果监听到修改Pod信息,则kubelet监听到变化后,会相应地修改本节点的Pod容器。</font>**

**<font color=amber>`kube-controller-manager`进程与`API Server`的交互。</font>** **<font color=tomato>`kube-controller-manager`中的`Node Controller`模块通过`API Sever`提供的`Watch`接口,实时监控`Node`的信息,并做相应处理</font>**

**<font color=amber>`kube-scheduler`与`API Server`的交互</font>**。**<font color=tomato>当Scheduler通过API Server的Watch接口监听到新建Pod副本的信息后,它会检索所有符合该Pod要求的Node列表,开始执行Pod调度逻辑,调度成功后将Pod绑定到目标节点上。</font>**

**<font color=orange>为了缓解集群各模块对API Server的访问压力,各功能模块都采用缓存机制来缓存数据。各功能模块定时从API Server获取指定的资源对象信息(通过`LIST`及`WATCH`方法),然后将这些信息保存到本地缓存,功能模块在某些情况下不直接访问API Server,而是通过访问缓存数据来间接访问API Server.</font>**
