---
title: 关于Kubernetes中kube-scheduler的一些笔记
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2022-01-09 03:29:04/关于Kubernetes中kube-scheduler的一些笔记.html'
mathJax: false
date: 2022-01-09 11:29:04
thumbnail:
---
+ 学习K8s，整理这部分知识
+ 博文内容主要涉及
  + K8s中调度组件kube Scheduler的一些总结
  + 主要参考《K8s权威指南》一书
+ 食用方式：
  + 需要k8s基础知识 
  + 需要了解pod基本调度行为
<!-- more -->
## 写在前面
***
+ 学习K8s，整理这部分知识
+ 博文内容主要涉及
  + K8s中调度组件kube Scheduler的一些总结
  + 主要参考《K8s权威指南》一书
+ 食用方式：
  + 需要k8s基础知识 
  + 需要了解pod基本调度行为

**<font color="009688"> 张牙舞爪的人，往往是脆弱的。因为真正强大的人，是自信的，自信就会温和，温和就会坚定--------《明朝那些事》**</font>


 ***



## Scheduler原理解析

众多周知，`Kubernetes Scheduler `是 `Kubernetes`中负责`Pod调度`的重要功能模块，运行在k8s 集群中的master节点。


**作用** : `Kubernetes Scheduler`的作用是将待调度的Pod (API`新创建`的Pod, Controller Manager为`补足副本而创建`的Pod等)按照特定的`调度算法`和`调度策略`绑定(Binding)到集群中某个合适的Node上,并将绑定信息写入etcd中。

在整个调度过程中涉及三个对象,分别是

+ `待调度Pod列表`
+ `可用Node列表`
+ `以及调度算法和策略`

**整体流程** :通过调度算法调度,为`待调度Pod列表`中的每个Pod从`Node列表`中选择一个`最适合的Node`随后, 目标node节点上的`kubelet`通过`APIServer监听到Kubernetes Scheduler`产生的`Pod绑定事件`,然后获取对应的`Pod清单`,`下载Image镜像并启动容器`。


![在这里插入图片描述](https://img-blog.csdnimg.cn/e023ee25f9064559bd0834f2b10efb99.png)

`kubelet`进程通过与`API Server`的交互，每隔一个时间周期,就会调用一次API Server的REST接口报告自身状态, API Server接收到这些信息后,将节点状态信息更新到etcd中。 同时kubelet也通过API Server的`Watch接口监听Pod信息`,如果监听到新的Pod副本被调度绑定到本节点,则执行Pod对应的容器的创建和启动逻辑;如果监听到Pod对象被删除,则删除本节点上的相应的Pod容器;如果监听到修改Pod信息,则kubelet监听到变化后,会相应地修改本节点的Pod容器。

所以说，`kubernetes Schedule `在整个系统中承担了`承上启下的`重要功能，对上负责接收声明式API或者控制器创建新pod的消息，并且为其安排一个合适的Node，对下，选择好node之后，把工作交接给node上的kubelet，由kubectl负责pod的剩余生命周期。




`Kubernetes Scheduler`当前提供的默认调度流程分为以下两步。

|流程|描述|
|--|--|
|**<font color=royalblue>预选调度过程</font>**|即遍历所有目标Node,筛选出符合要求的候选节点。为此, Kubernetes内置了多种预选策略(xxx Predicates)供用户选择|
|**<font color=brown>确定最优节点</font>**|在第1步的基础上,采用优选策略(xxxPriority)计算出每个候选节点的积分,积分最高者胜出|

Kubernetes Scheduler的调度流程是通过插件方式加载的`"调度算法提供者” (AlgorithmProvider)`具体实现的。一个`AlgorithmProvider`其实就是包括了一组`预选策略`与一组`优先选择策略`的结构体.

Scheduler中可用的预选策略包含：`NoDiskConflict、PodFitsResources、PodSelectorMatches、PodFitsHost、CheckNodeLabelPresence、CheckServiceAffinity 和PodFitsPorts`策略等。

其默认的`AlgorithmProvider`加载的预选策略返回布尔值包括：

+ PodFitsPorts(PodFitsPorts)：判断端口是否冲突
+ PodFitsResources(PodFitsResources)：判断备选节点的资源是否满足备选Pod的需求
+ NoDiskConflict(NoDiskConflict)：判断备选节点和已有节点是否磁盘冲突
+ MatchNodeSelector(PodSelectorMatches)：判断备选节点是否包含备选Pod的标签选择器指定的标签。
+ HostName(PodFitsHost):判断备选Pod的spec.nodeName域所指定的节点名称和备选节点的名称是否一致

即每个节点只有`通过前面提及的5个默认预选策略后`，才能初步被选中，进入到`确认最优节点(优选策略)流程。`


Scheduler中的优选策略包含(不限于下面3个)：

+ `LeastRequestedPriority`:从备选节点列表中选出资源消耗最小的节点,对各个节点公式打分
+ `CalculateNodeLabelPriority`：判断策略列出的标签在备选节点中存在时，是否选择该备选节,这不太懂，打分
+ `BalancedResourceAllocation`：从备选节点列表中选出各项资源使用率最均衡的节点。对各个节点公式打分

每个节点通过`优先选择策略时都会算出一个得分，计算各项得分，最终选出得分值最大的节点作为优选的结果`(也是调度算法的结果)。








