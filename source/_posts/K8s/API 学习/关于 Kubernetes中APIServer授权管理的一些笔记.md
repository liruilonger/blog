---
title: 关于 Kubernetes中API Server授权(RBAC)管理的一些笔记
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: Kubernetes
uniqueId: '2022-01-27 13:37:25/关于Kubernetes中APIServer授权管理的一些笔记.html'
mathJax: false
date: 2022-01-27 21:37:25
thumbnail:
---
**<font color="009688"> 我也突然懂得，原来痛苦、失望和悲愁不是为了惹恼我们，使我们气馁或者无地自容；它们的存在，是为了使我们心智成熟，臻于完善。      ——赫尔曼·黑塞《彼得·卡门青》**</font>

<!-- more -->
## 写在前面
***
+ 学习`K8s`涉及，整理笔记记忆
+ 博文偏实战，内容涉及：
  + `k8s`中`API Server`的授权策略简述
  + `RBAC`授权策略中涉及到的资源对象创建删除
    + `Role`
    + `ClusterRole`
    + `RoleBinding`
    + `ClusterRoleBinding`
  + 集群外客户机访问基于`RBAC`授权用户的场景`Demo`

**<font color="009688"> 我也突然懂得，原来痛苦、失望和悲愁不是为了惹恼我们，使我们气馁或者无地自容；它们的存在，是为了使我们心智成熟，臻于完善。      ——赫尔曼·黑塞《彼得·卡门青》**</font>


 ***

## <font color=tomato>API Server授权管理</font>

### **<font color=blue>环境版本</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl  get nodes
NAME                         STATUS     ROLES                  AGE   VERSION
vms81.liruilongs.github.io   Ready      control-plane,master   41d   v1.22.2
vms82.liruilongs.github.io   Ready      <none>                 41d   v1.22.2
vms83.liruilongs.github.io   Ready      <none>                 41d   v1.22.2
```
**<font color=blue>除了k8s集群，我们还用到了集群外的机器`liruilongs.github.io`,这个机器作为客户器，安装了kubectl客户端，以上传了liruilong用户的kubeconfig证书文件,但是没有绑定角色</font>**

**<font color=seagreen>命名空间环境准备</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl create ns liruilong-rbac-create
namespace/liruilong-rbac-create created
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$mkdir k8s-rbac-create;cd k8s-rbac-create
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl config set-context $(kubectl config current-context) --namespace=liruilong-rbac-create
Context "kubernetes-admin@kubernetes" modified.
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```
### <font color=seagreen>授权策略简述</font>

**<font color=purple>当客户端发起API Server调用时, API Server内部要先进行用户认证,然后执行用户鉴权流程,即通过鉴权策略来决定一个API调用是否合法。想来对于开发的小伙伴并不陌生，常用的`Spring Security`等安全框架，都会涉及认证和鉴权的过程。</font>**

>既然鉴权，那必有授权的过程，简单地说,授权就是授予不同的用户不同的访问权限。`API Server`目前支持以下几种授权策略


|策略|描述|
|:--|:--|
|<font color=chocolate>AlwaysDeny</font>|表示拒绝所有请求,一般用于测试。|
|<font color=amber>AlwaysAllow</font>|允许接收所有请求,如果集群不需要授权流程,则可以采用该策略,这也是Kubernetes的默认配置。|
|<font color=purple>ABAC</font>  |(Attribute-Based Access Control)基于属性的访问控制,表示使用用户配置的授权规则对用户请求进行匹配和控制。|
|<font color=yellowgreen>Webhook</font>|通过调用外部REST服务对用户进行授权。|
|<font color=seagreen>RBAC</font>|(Role-Based Access Control)基于角色的访问控制。|
|<font color=yellowgreen>Node</font>|是一种专用模式,用于对kubelet发出的请求进行访问控制。 |

策略的设置通过通过API Server的启动参数"`--authorization-mode`"设置。 


```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$cat /etc/kubernetes/manifests/kube-apiserver.yaml | grep authorization-mode
    - --authorization-mode=Node,RBAC
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```

关于授权策略，`AlwaysAllow`和`AlwaysDeny`不多讲，`ABAC`不够灵活，`Node`授权器主要用于各个`node`上的`kubelet`访问`apiserver`时使用的，其他一般均由`RBAC`授权器来授权,下面我们看看`RBAC`的权限策略

### <font color=orange> RBAC授权模式</font>

这一块，官方文档写的很详细，详细学习，小伙伴还是去官网看看，这里讲一些常用的完整Demo，官网地址：[https://kubernetes.io/zh/docs/reference/access-authn-authz/rbac/](https://kubernetes.io/zh/docs/reference/access-authn-authz/rbac/)

**<font color=orange>RBAC(Role-Based Access Control，基于角色的访问控制)即权限是和角色相关的,而用户则被分配相应的角色作为其成员。</font>**

>在`Kubernetes的1.5`版本中引入，在`1.6`版本时升级为`Beta版本`，在`1.8`版本时升级为`GA`。作为`kubeadm`安装方式的默认选项，相对于其他访问控制方式，新的RBAC具有如下优势。

+ **<font color=amber>对集群中的资源和非资源权限均有完整的覆盖。</font>**
+ **<font color=purple>整个RBAC完全由几个API对象完成，同其他API对象一样，可以用`kubectl`或`API`进行操作。</font>**
+ **<font color=tomato>可以在运行时进行调整，无须重新启动`API Server`。</font>**

**<font color=orange>要使用RBAC授权模式，需要在API Server的启动参数中加上--authorization-mode=RBAC,如果小伙伴默认使用kubeadm安装，那么默认使用`Node`,`RBAC`两种策略</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$cat /etc/kubernetes/manifests/kube-apiserver.yaml | grep authorization-mode
    - --authorization-mode=Node,RBAC
```


**<font color=brown>RBAC的API资源对象引入了4个新的顶级资源对象:</font>**
+ `Role`
+ `ClusterRole`
+ `RoleBinding`
+ `ClusterRoleBinding`

**<font color=camel>同其他API资源对象一样,用户可以使用`kubectl`或者`API`调用等方式操作这些资源对象。 </font>**

#### <font color=yellowgreen>角色</font>
**<font color=tomato>角色(`Role`)`一个角色就是一组权限的集合`。在`同一个命名空间中,可以用Role来定义一个角色`,如果是集群级别的,就需要使用`ClusterRole了`。角色只能对命名空间内的资源进行授权</font>**


**<font color=royalblue>集群角色(`ClusterRole`)集群角色除了具有和角色一致的</font>**, **<font color=tomato>命名空间内资源的管理能力,因其集群级别的范围,还可以用于以下特殊元素的授权。</font>**
  + **<font color=royalblue>集群范围的资源,例如Node</font>**.
  + **<font color=amber>非资源型的路径,例如“/api"</font>**
  + **<font color=seagreen>包含全部命名空间的资源,例如pods (用于`kubectl get pods -all-namespaces`这样的操作授权)。</font>**

>下面我们找一个集群中内置的集群角色管理员看看详细信息
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl describe clusterrole admin
Name:         admin
Labels:       kubernetes.io/bootstrapping=rbac-defaults
Annotations:  rbac.authorization.kubernetes.io/autoupdate: true
PolicyRule:
  Resources                                       Non-Resource URLs  Resource Names  Verbs
  ---------                                       -----------------  --------------  -----
  rolebindings.rbac.authorization.k8s.io          []                 []              [create delete deletecollection get list patch update watch]
  roles.rbac.authorization.k8s.io                 []                 []              [create delete deletecollection get list patch update watch]
  configmaps                                      []                 []              [create delete deletecollection patch update get list watch]
  events                                          []                 []              [create delete deletecollection patch update get list watch]
  persistentvolumeclaims                          []                 []              [create delete deletecollection patch update get list watch]
  pods                                            []                 []              [create delete deletecollection patch update get list watch]
  replicationcontrollers/scale                    []                 []              [create delete deletecollection patch update get list watch]
.........
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```
>**<font color=yellowgreen>在上面的管理员集群角色中，`Resources`列为允许访问的资源，`Verbs`列为允许操作的行为或者动作</font>**

#### <font color=brown>角色绑定</font>

有个角色，那么角色和用户或者用户组是如何绑定的，这里就需要角色绑定这个资源对象了，不知道小伙记不记得，我们在做认证的时候，有一个授权的动作
**<font color=amber>给用户授权，这里给 liruilong 一个集群角色 cluster-role(cluster-admin类似于root一样的角色)，这样 liruilong 具有管理员权限</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl create clusterrolebinding test  --clusterrole=cluster-admin --user=liruilong
clusterrolebinding.rbac.authorization.k8s.io/test created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```
在这里，我们把`cluster-admin`这个集群角色绑定到了用户`liruilong`，`cluster-admin`这个角色相当于是一个root用户的角色

**<font color=amber>角色绑定(`RoleBinding`)和集群角色绑定(`ClusterRoleBinding`)</font>** :用来把一个角色绑定到一个目标上,绑定目标可以是U`ser (用户) 、Group (组)或者Service Account`。

>**<font color=chocolate>在一般情况下，使用RoleBinding为某个命名空间授权,使用ClusterRoleBinding为集群范围内授权。</font>**

##### <font color=blue>角色绑定的区别：</font>

`RoleBinding(角色绑定)`可以引用`Role`进行授权。也可以引用`ClusterRole`，对属于同一命名空间内`ClusterRole`定义的资源主体进行授权

`ClusterRoleBinding(集群角色绑定)`中的角色只能是集群角色(ClusterRole)，用于进行集群级别或者对所有命名空间都生效的授权



### <font color=orange>实战</font>

#### <font color=red>角色创建</font>

**<font color=yellowgreen>查看系统中角色</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl  get role
No resources found in liruilong-rbac-create namespace.
```
**<font color=tomato>创建一个role角色资源对象</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl create role role-liruilong --verb=get,list,watch  --resource=pod --dry-run=client -o yaml
```
**<font color=blue>这里我们创建一个角色，名字叫`role-liruilong`,定义这个角色拥有pod资源的查看详细信息，列表查看，监听，创建的权限</font>**

**<font color=amber>查看资源文件</font>**
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-liruilong
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - get
  - list
  - watch
  - create
```
**<font color=royalblue>资源文件属性</font>**
+ **<font color=amber>apiGroups</font>**：支持的`API组列表`，例如`“apiVersion:batch/v1”,“apiVersion: extensions:v1beta1”,“apiVersion: apps/v1beta1”`等
+ **<font color=seagreen>resources</font>**：支持的`资源对象`列表，例如`pods、deployments、jobs`等。
+ **<font color=camel>verbs</font>**：对`资源对象`的`操作方法`列表，例如`get、watch、list、delete、replace、patch`等


**<font color=chocolate>通过yaml文件创建一个role资源对象</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl create role role-liruilong --verb=get,list,watch,create  --resource=pod --dry-run=client -o yaml >role-liruilong.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl apply -f role-liruilong.yaml
role.rbac.authorization.k8s.io/role-liruilong created
```
**<font color=tomato>查看角色信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl  get role
NAME             CREATED AT
role-liruilong   2022-01-23T13:17:15Z
```
**<font color=tomato>查看角色详细信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$kubectl describe role role-liruilong
Name:         role-liruilong
Labels:       <none>
Annotations:  <none>
PolicyRule:
  Resources  Non-Resource URLs  Resource Names  Verbs
  ---------  -----------------  --------------  -----
  pods       []                 []              [get list watch create]
```
#### <font color=royalblue>创建角色绑定</font>
**<font color=seagreen>绑定角色到liruilong用户</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl create  rolebinding  rbind-liruilong --role=role-liruilong --user=liruilong
rolebinding.rbac.authorization.k8s.io/rbind-liruilong created
```
**<font color=seagreen>查看角色绑定</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl  get rolebindings
NAME              ROLE                  AGE
rbind-liruilong   Role/role-liruilong   23s
```
**<font color=seagreen>查看绑定角色的详细信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl describe rolebindings rbind-liruilong
Name:         rbind-liruilong
Labels:       <none>
Annotations:  <none>
Role:
  Kind:  Role
  Name:  role-liruilong
Subjects:
  Kind  Name       Namespace
  ----  ----       ---------
  User  liruilong
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```

**<font color=purple>然后我们在客户机访问测试</font>**
**<font color=blue>这里使用我们之前创建的`kubeonfig`证书，我们之前创建证书也是基于用户`liruilong`的</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl config view | grep namesp
    namespace: liruilong-rbac-create
    
```
**<font color=tomato>修改一下`kubeonfig`证书的命名空间</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat kc1 | grep namesp
    namespace: default
┌──[root@liruilongs.github.io]-[~]
└─$ sed 's#namespace: default#namespace: liruilong-rbac-create#g' kc1 | grep namesp
    namespace: liruilong-rbac-create
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i 's#namespace: default#namespace: liruilong-rbac-create#g' kc1 | grep namesp
```
**<font color=tomato>在客户机指定`kubeconfig`文件测试，认证和鉴权没有问题，当前命名空间没有资源对象</font>**
```basg
┌──[root@liruilongs.github.io]-[~]
└─$kubectl --kubeconfig=kc1 get pods
No resources found in liruilong-rbac-create namespace. 
```
**<font color=seagreen>然后我们在客户机创建一个pod</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl --kubeconfig=kc1  run pod-demo --image=nginx --image-pull-policy=IfNotPresent --dry-run=client -o yaml >pod-demo.yaml
```
**<font color=orange>查看pod资源，创建成功</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get pods
NAME       READY   STATUS    RESTARTS   AGE
pod-demo   1/1     Running   0          28m
```
**<font color=purple>删除一个pod，因为没有授权删除的权限，所以无法删除</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 delete pod pod-demo
Error from server (Forbidden): pods "pod-demo" is forbidden: User "liruilong" cannot delete resource "pods" in API group "" in the namespace "liruilong-rbac-create"
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=tomato>回到集群，添加删除的角色</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$vim role-liruilong.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl apply -f role-liruilong.yaml
role.rbac.authorization.k8s.io/role-liruilong configured
```
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-liruilong
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - get
  - list
  - watch
  - create
  - delete
```
**<font color=chocolate>重新删除，pod删除成功</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 delete pod pod-demo
Error from server (Forbidden): pods "pod-demo" is forbidden: User "liruilong" cannot delete resource "pods" in API group "" in the namespace "liruilong-rbac-create"
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 delete pod pod-demo
pod "pod-demo" deleted
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=orange>这里我们查看SVC信息，发现没有权限</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get svc
Error from server (Forbidden): services is forbidden: User "liruilong" cannot list resource "services" in API group "" in the namespace "liruilong-rbac-create"
```
**<font color=purple>提示我们用户liruilong需要添加services资源权限</font>**

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$vim role-liruilong.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl apply  -f role-liruilong.yaml
role.rbac.authorization.k8s.io/role-liruilong configured
```
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-liruilong
rules:
- apiGroups:
  - ""
  resources:
  - pods
  - services
  - deployments
  verbs:
  - get
  - list
  - watch
  - create
  - delete
```
**<font color=brown>这里我们在集群中添加`svc和deploy`。然后在客户机访问测试，之前访问没有权限，现在提示命名空间中没有资源，说明鉴权成功</font>**

```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get svc
No resources found in liruilong-rbac-create namespace.
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=blue>但是我们访问deploy的时候，提示报错，没有权限，什么原因呢，这就涉及到一个`- apiGroups:`的原因</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get deployments
Error from server (Forbidden): deployments.apps is forbidden: User "liruilong" cannot list resource "deployments" in API group "apps" in the namespace "liruilong-rbac-create"
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=blue>我们查看api资源对象，发现deployments和service的版本定义不同，一个是`apps/v1 `，一个是`v1`,资源对象定义的yaml文件中`apiGroups`字段用于定义版本组别。</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl api-resources | grep pods
pods                              po           v1                                     true         Pod
pods                                           metrics.k8s.io/v1beta1                 true         PodMetrics
podsecuritypolicies               psp          policy/v1beta1                         false        PodSecurityPolicy
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl api-resources | grep deploy
deployments                       deploy       apps/v1                                true         Deployment
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl api-resources | grep service
serviceaccounts                   sa           v1                                     true         ServiceAccount
services                          svc          v1                                     true         Service
apiservices                                    apiregistration.k8s.io/v1              false        APIService
servicemonitors                                monitoring.coreos.com/v1               true         ServiceMonitor
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```
**<font color=tomato>修改资源配置文件，添加v1的的上一级apps</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$vim role-liruilong.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl apply  -f role-liruilong.yaml
role.rbac.authorization.k8s.io/role-liruilong configured
```
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-liruilong
rules:
- apiGroups:
  - ""
  - "apps"
  resources:
  - pods
  - services
  - deployments
  verbs:
  - get
  - list
  - watch
  - create
  - delete
```
**<font color=royalblue>客户机访问测试，鉴权成功，但是没有相关的deploy资源</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get deployments
Error from server (Forbidden): deployments.apps is forbidden: User "liruilong" cannot list resource "deployments" in API group "apps" in the namespace "liruilong-rbac-create"
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get deployments
No resources found in liruilong-rbac-create namespace.
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=red>这里我们在客户机创建一个deploy</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 create  deployment  web-liruilong --image=nginx --replicas=2 --dry-run=client -o yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web-liruilong
  name: web-liruilong
spec:
  replicas: 2
  selector:
    matchLabels:
      app: web-liruilong
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web-liruilong
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
status: {}
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 create  deployment  web-liruilong --image=nginx --replicas=2 --dry-run=client -o yaml >web-liruilong.yaml
```
**<font color=blue>创建成功，查看deploy的资源，可以正常查看</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1  apply -f web-liruilong.yaml
deployment.apps/web-liruilong created
┌──[root@liruilongs.github.io]-[~]
└─$kubectl   --kubeconfig=kc1 get deployments
NAME            READY   UP-TO-DATE   AVAILABLE   AGE
web-liruilong   0/2     2            0           31s
┌──[root@liruilongs.github.io]-[~]
└─$kubectl   --kubeconfig=kc1 get pods
NAME                            READY   STATUS              RESTARTS   AGE
web-liruilong-c546cb69b-g2pvm   0/1     ContainerCreating   0          59s
web-liruilong-c546cb69b-w6xfh   0/1     ContainerCreating   0          59s
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=orange>但是我们修改deplo的副本数量时，会报错，这是什么原因呢，提示我们少了`patch`权限，没办法修改deploy的副本数量</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl   --kubeconfig=kc1  scale deployment  web-liruilong --replicas=3
Error from server (Forbidden): deployments.apps "web-liruilong" is forbidden: User "liruilong" cannot patch resource "deployments/scale" in API group "apps" in the namespace "liruilong-rbac-create"
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=camel>同样，添加`patch`权限</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$vim role-liruilong.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl apply  -f role-liruilong.yaml
role.rbac.authorization.k8s.io/role-liruilong configured
```
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-liruilong
rules:
- apiGroups:
  - ""
  - "apps"
  resources:
  - pods
  - services
  - deployments
  - deployments/scale
  verbs:
  - get
  - list
  - watch
  - create
  - delete
  - patch
```
**<font color=camel>在次执行命令，可以正常修改deploy的副本</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl   --kubeconfig=kc1  scale deployment  web-liruilong --replicas=3
deployment.apps/web-liruilong scaled
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get deployments
NAME            READY   UP-TO-DATE   AVAILABLE   AGE
web-liruilong   2/3     3            2           7m43s
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get pods
NAME                            READY   STATUS              RESTARTS   AGE
web-liruilong-c546cb69b-52nqm   0/1     ContainerCreating   0          16s
web-liruilong-c546cb69b-g2pvm   1/1     Running             0          7m50s
web-liruilong-c546cb69b-w6xfh   1/1     Running             0          7m50s
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=tomato>当然，资源文件也可以分开写</font>**
```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: null
  name: role-liruilong
rules:
- apiGroups:
  - ""
  resources:
  - pods
  - services
  verbs:
  - get
  - list
  - watch
  - create
  - delete
- apiGroups:
  - "apps"
  resources:
  - deployments
  - deployments/scale
  verbs:
  - get
  - list
  - watch
  - create
  - delete
  - patch
```
#### <font color=chocolate>创建集群角色</font>

**<font color=camel>角色是基于命名空间的，即不同的命名空间基于角色的权限不会受影响，这里我们需要把角色定义为全局生效，就需要集群角色</font>**

**<font color=royalblue>我们用之前的角色修改一下资源文件，将资源类型修改为集群角色`ClusterRole`</font>**

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$sed 's#kind: Role#kind: ClusterRole#g' role-liruilong.yaml | grep kind
kind: ClusterRole
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$sed -i 's#kind: Role#kind: ClusterRole#g' role-liruilong.yaml | grep kind
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl apply -f role-liruilong.yaml
clusterrole.rbac.authorization.k8s.io/role-liruilong created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```
**<font color=orange>同时需要把集群角色和用户liruilong绑定</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl  get clusterrolebindings | grep liruilong
cbind-liruilong                                        ClusterRole/role-liruilong                                                         2m29s
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```
**<font color=tomato>查看pod资源list，不但可以查看`当前命名空间`，也可以查看`default、kube-system`命名空间的</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get pods
No resources found in liruilong-rbac-create namespace.
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get pods -n default
No resources found in default namespace.
┌──[root@liruilongs.github.io]-[~]
└─$kubectl  --kubeconfig=kc1 get pods -n kube-system
NAME                                                 READY   STATUS    RESTARTS          AGE
calico-kube-controllers-78d6f96c7b-85rv9             1/1     Running   272 (5m49s ago)   39d
calico-node-6nfqv                                    1/1     Running   290 (41m ago)     41d
calico-node-fv458                                    0/1     Running   50                41d
calico-node-h5lsq                                    1/1     Running   98 (17m ago)      41d
coredns-7f6cbbb7b8-ncd2s                             1/1     Running   21 (6d14h ago)    39d
coredns-7f6cbbb7b8-pjnct                             1/1     Running   20 (6d14h ago)    39d
etcd-vms81.liruilongs.github.io                      1/1     Running   126 (8h ago)      41d
kube-apiserver-vms81.liruilongs.github.io            1/1     Running   12 (8h ago)       7d10h
kube-controller-manager-vms81.liruilongs.github.io   1/1     Running   88 (8h ago)       40d
kube-proxy-scs6x                                     1/1     Running   10 (7d22h ago)    41d
kube-proxy-tbwz5                                     1/1     Running   22 (6d14h ago)    41d
kube-proxy-xccmp                                     1/1     Running   6 (8d ago)        41d
kube-scheduler-vms81.liruilongs.github.io            1/1     Running   242 (8h ago)      41d
metrics-server-bcfb98c76-ktgnj                       1/1     Running   0                 6d
┌──[root@liruilongs.github.io]-[~]
└─$
```

#### <font color=camel>删除角色、角色绑定</font>
**<font color=purple>删除角色、角色绑定</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl delete  role role-liruilong
role.rbac.authorization.k8s.io "role-liruilong" deleted
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl delete  rolebindings rbind-liruilong
rolebinding.rbac.authorization.k8s.io "rbind-liruilong" deleted
```
**<font color=purple>删除集群角色、集群角色绑定</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl delete  ClusterRole  role-liruilong
clusterrole.rbac.authorization.k8s.io "role-liruilong" deleted
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$kubectl delete  clusterrolebinding cbind-liruilong
clusterrolebinding.rbac.authorization.k8s.io "cbind-liruilong" deleted
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-rbac-create]
└─$
```

>关于Api Service的授权管理，还有很多内容，时间关系，先和小伙伴分享到这里，生活加油
