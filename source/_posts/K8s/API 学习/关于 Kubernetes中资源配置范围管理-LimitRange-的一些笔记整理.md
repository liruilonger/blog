---
title: 关于K8s中资源配置范围管理(LimitRange)的一些笔记整理
tags:
  - Kubernetes
categories:
  - Kubernetes
toc: true
recommend: 1
keywords: LimitRange
uniqueId: "2022-11-25 05:28:16/关于K8s中资源配置范围管理(LimitRange)的一些笔记整理.html"
mathJax: false
date: 2022-11-25 13:28:16
thumbnail:
---

**<font color="009688"> 跳出童年时代吧，朋友，觉醒呵！ --- J.J. 卢梭**</font>

<!-- more -->

## 写在前面

---

- 分享一些 `K8s` 中 `LimitRange` 的笔记
- 博文内容涉及：
  - `LimitRange` 简单介绍
  - `LimitRange` 资源对象创建使用
  - `准入检查`和`资源约束`的一些 Demo
- 理解不足小伙伴帮忙指正

**<font color="009688"> 跳出童年时代吧，朋友，觉醒呵！ --- J.J. 卢梭**</font>

---

## LimitRange 介绍

在默认情况下，`Kubernetes` 不会对 `Pod` 做 CPU 和内存等资源限制，即 Kubernetes 系统中任何 Pod 都可以`使用其所在节点的所有可用的CPU和内存`。

通过配置 Pod 的计算资源 `Requests`和 `Limits`，我们可以限制 `Pod` 的资源使用,配置`最高要求和最低要求`。

但对于 `Kubernetes` 集群管理员而言，为每一个 Pod 配置 `Requests和Limits` 是麻烦的，同时维护特别的不方便。需要考虑如何确保一个 Pod 不会垄断命名空间内所有可用的资源。

更多时候，我们需要`对集群内Requests和Limits的配置做一个全局限制`。这里就要用到 `Limitrange` ,`LimitRange` 用来限制命名空间内 适用的对象类别 (例如 `Pod` 或 `PersistentVolumeClaim`) 指定的资源分配量(限制和请求)的一个策略对象。对 Pod 和容器的 Requests 和 Limits 配置做进一步做出限制。

#### `LimitRange` 可以做什么：

- 在一个`命名空间`中实施对每个 `Pod 或 Container` `最小和最大`的资源使用量的限制。
- 在一个`命名空间`中实施对每个 `PersistentVolumeClaim` 能申请的`最小和最大`的存储空间大小的限制。
- 在一个`命名空间`中实施对一种资源的 `申请值和限制值` 的 `比值的控制` 。
- 设置一个`命名空间`中对计算资源的`默认申请/限制值`，并且自动的在运行时注入到多个`Container`中。
- 当某命名空间中有一个`LimitRange`对象时，会在该命名空间中实施 `LimitRange` 限制。

`LimitRange 的名称必须是合法的 DNS 子域名。`

## 资源限制和请求的约束

管理员在一个命名空间内创建一个 `LimitRange` 对象。 用户在此命名空间内创建(或尝试创建) `Pod` 和 `PersistentVolumeClaim` 等对象。

首先，LimitRanger 准入控制器对所有没有设置计算资源需求的所有 Pod(及其容器)`设置默认请求值与限制值`。也就是(`limits` 和 `Request`)

其次，LimitRange `跟踪其使用量` 以保证没有超出命名空间中存在的任意 `LimitRange` 所定义的`最小、最大资源`使用量以及`使用量比值`。

若尝试创建或更新的对象(Pod 和 PersistentVolumeClaim)`违反了 LimitRange 的约束`， 向 API 服务器的请求会失败，并返回 `HTTP 状态码 403 Forbidden`以及描述哪一项约束被违反的消息。

若你在命名空间中添加 `LimitRange` 启用了对 cpu 和 memory 等计算相关资源的限制(Max,Min)， 你必须`指定这些值`的`请求使用量 requests 与限制使用量 limits `,否则，系统将会拒绝创建 Pod,除非在 `LimitRange` 定义 默认的 ( limits 和 requests )。

`LimitRange` 的 `验证仅在 Pod 准入阶段进行`，不对正在运行的 Pod 进行验证。 如果你添加或修改 LimitRange，命名空间中`已存在的 Pod 将继续不变`。

如果命名空间中存在`两个或更多 LimitRange` 对象，应用哪个默认值是`不确定的`。

### 创建 LimitsRange 对象

将 LimitsRange 应用到一个 Kubernetes 的命名空间中，需要先定义一个 `LimitRange` ，定义最大及最小范围、Requests 和 Limits 的 默认值、Limits 与 Requests 的最大比例上限等。

创建一个命名空间

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl create ns limitrange-example
namespace/limitrange-example created
┌──[root@vms81.liruilongs.github.io]-[/]
└─$vim limit.yaml
```

编写一个 LimitRange ，并运行

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl apply -f limit.yaml -n limitrange-example
limitrange/mylimits created
```

查看状态

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl get limitRange -n limitrange-example
NAME       CREATED AT
mylimits   2022-11-06T17:56:20Z

```

资源文件信息

```yaml
┌──[root@vms81.liruilongs.github.io]-[/]
└─$cat limit.yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: mylimits
spec:
  limits:
  - max:
      cpu: "4"
      memory: 2Gi
    min:
      cpu: 200m
      memory: 6Mi
    maxLimitRequestRatio:
      cpu: 3
      memory: 2
    type: Pod
  - default:
      cpu: 300m
      memory: 200Mi
    defaultRequest:
      cpu: 200m
      memory: 100Mi
    max:
      cpu: "2"
      memory: 1Gi
    min:
      cpu: 100m #0.1C
      memory: 3Mi
    maxLimitRequestRatio:
      cpu: 5
      memory: 4
    type: Container
```

上面的 资源文件是相对全的一个资源文件的定义,查看定义的详细信息

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl  describe  limitRange  mylimits -n limitrange-example
Name:       mylimits
Namespace:  limitrange-example
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
Pod         cpu       200m  4    -                -              3
Pod         memory    6Mi   2Gi  -                -              2
Container   cpu       100m  2    200m             300m           5
Container   memory    3Mi   1Gi  100Mi            200Mi          4
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

在定义 LimitRange 时，当 LimitRange 中 container 类型限制没有指定 Limits 和 Requests 的默认值时，会使用 Man 作为默认值

```yaml
┌──[root@vms81.liruilongs.github.io]-[/]
└─$cat limit-Max.yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: mylimits
spec:
  limits:
  - max:
      cpu: "2"
      memory: 1Gi
    min:
      cpu: 100m
      memory: 3Mi
    type: Container
```

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl apply -f limit-Max.yaml  -n limitrange-example
limitrange/mylimits created
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl describe  limitranges  mylimits -n limitrange-example
Name:       mylimits
Namespace:  limitrange-example
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
Container   cpu       100m  2    2                2              -
Container   memory    3Mi   1Gi  1Gi              1Gi            -
```

### 具体的属性解释

在 LimitRange 中整体分为两部分，pod 部分和 Container(容器) 部分，`Pod` 和 `Container` 都可以设置 `Min、Max`和 `Max Limit/Requests Ratio`参数。分别来看下，

#### 对于容器部分：

不同的是 `Container` 还可以设置 `Default Request 和 Default Limit` 参数，而 Pod 不能设置 `Default Request和 Default Limit` 参数。

- Container 的 Min (100m 和 3Mi)是 Pod 中所有容器的 Requests 值下限,即创建 pod 等资源时，申请值 Requests 的值不能低于 Min 的值，否则提示违反约束
- Container 的 Max (2C 和 1Gi)是 Pod 中所有容器的 Limits 值上限，同理， 限制 资源使用 Limits 不能超过 2000m、 1Gi( 二进制的字节表示) ；
- Container 的 Default Request (200m 和 100Mi)是 Pod 中所有未指定 Requests 值的容器的默认 Requests 值；
- Container 的 Default Limit (300m 和 200Mi)是 Pod 中所有未指定 Limits 值的容器的默认 Limits 值。
- Container 的 Max Limit/Requests Ratio (5 和 4)限制了 Pod 中所有容器的 Limits 值与 Requests 值的比例上限；

```bash
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
..............
Container   cpu       100m  2    200m             300m           5
Container   memory    3Mi   1Gi  100Mi            200Mi          4
```

当创建的容器未指定 Requests 值或者 Limits 值时，将使用 Container 的 `Default Request` 值或者 `Default Limit` 值。

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl describe  limitranges  mylimits -n limitrange-example
Name:       mylimits
Namespace:  limitrange-example
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
Container   cpu       100m  2    200m             300m           5
Container   memory    3Mi   1Gi  100Mi            200Mi          4
```

```yaml
┌──[root@vms81.liruilongs.github.io]-[/]
└─$cat pod1.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-demo
  name: pod-demo
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-demo
    resources:
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

没有指定时，会使用默认值

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl get pods pod-demo -n  limitrange-example -o json | jq  .spec.containers[0].resources
{
  "limits": {
    "cpu": "300m",
    "memory": "200Mi"
  },
  "requests": {
    "cpu": "200m",
    "memory": "100Mi"
  }
}
```

**对于同一资源类型，这 4 个参数必须满足以下关系：`Min ≤ Default Request ≤ Default Limit ≤ Max。`**

#### 对于 Pod 部分：

这里需要注意的但是容器的总和:

- Pod 的 Min(200m 和 6Mi)是 Pod 中所有容器的 Requests 值的 `总和` 下限；
- Pod 的 Max(4 和 2Gi)是 Pod 中所有容器的 Limits 值的 `总和` 上限。
- Pod 的 Max Limit/Requests Ratio (3 和 2)限制了 Pod 中所有容器的 Limits 值总和与 Requests 值总和的比例上限。

```bash
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
Pod         cpu       200m  4    -                -              3
Pod         memory    6Mi   2Gi  -                -              2
........
```

对于任意一个 Pod 而言，该 Pod 中所有容器的 Requests 总和必须大于或等于 6MiB，而且所有容器的 Limits 总和必须小于或等于 1GiB；同样，所有容器的 CPU Requests 总和必须大于或等于 200m，而且所有容器的 CPU Limits 总和必须小于或等于 2。

- Pod 里任何容器的 Limits 与 Requests 的比例都不能超过 `Container 的 Max Limit/Requests Ratio`
- Pod 里所有容器的 Limits 总和与 Requests 的总和的比例不能超过 `Pod 的 Max Limit/Requests Ratio`

### 准入检查 Demo

当前的 LimitRange 定义为：

```bash
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
Pod         cpu       200m  4    -                -              3
Pod         memory    6Mi   2Gi  -                -              2
Container   cpu       100m  2    200m             300m           5
Container   memory    3Mi   1Gi  100Mi            200Mi          4
```

当容器中定义的申请的最小 request 值 大于 LimitRange 默认定义的限制资源 limits 的值时，是无法调度的。在上面定义了 Limitrange 的命名空间内创建一个 pod, `没有指定 limits 所以使用默认的 limits 值，但是默认的 limits 值 300m 小于当前 容器的值`

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$cat  pod-demo-limitrange.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-demo
  name: pod-demo
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-demo
    resources:
      requests:
        cpu:  500m
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

`申请的最小值大于默认的最大值，显然不符合逻辑，所以 pod 无法调度创建,`

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl apply -f  pod-demo-limitrange.yaml -n limitrange-example
The Pod "pod-demo" is invalid: spec.containers[0].resources.requests: Invalid value: "500m": must be less than or equal to cpu limit
```

如果同时设置了 `requests` 和 `limits`，那么 `limits` 不使用默认值

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$cat pod-demo-limitrange.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-demo
  name: pod-demo
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-demo
    resources:
      requests:
        cpu:  500m
      limits:
        cpu: 800m
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

新 Pod 会被成功调度，但是这里需要注意的是， `requests 和 limits` 要符合 Limitrange 中 容器的`MAX,MIN`配置

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl apply -f  pod-demo-limitrange.yaml -n limitrange-example
pod/pod-demo created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl get pods -n limitrange-example
NAME       READY   STATUS    RESTARTS   AGE
pod-demo   1/1     Running   0          11s
```

**`Max `限制** ，当 Limits 大于 Max 值时，Pod 不会创建成功， 我的定义如下一个 LimiRanga 的资源对象

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl describe  limitranges  -n limitrange-example mylimits
Name:       mylimits
Namespace:  limitrange-example
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
Container   cpu       100m  2    2                2              -
Container   memory    3Mi   1Gi  1Gi              1Gi            -
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

当前的命名空间限制 容器 Limits CUP 大小为 2C ，这里创建一个 3C 的 容器来看下

```yaml
┌──[root@vms81.liruilongs.github.io]-[/]
└─$cat pod1.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-demo
  name: pod-demo
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-demo
    resources:
      limits:
        cpu: '3'
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

创建失败，提示不满足准入规则

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl apply  -f pod1.yaml -n limitrange-example
Error from server (Forbidden): error when creating "pod1.yaml": pods "pod-demo" is forbidden: maximum cpu usage per Container is 2, but limit is 3
```

** `Max Limit/Request Ratio` 限制** ，当 `Limit/Request` 的比值 超过 `Max Limit/Request Ratio` 时调度失败

定义如下一个 LimitRange ：

```yaml
┌──[root@vms81.liruilongs.github.io]-[/]
└─$cat limit-Ratio.yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: mylimits
spec:
  limits:
  - max:
      cpu: "4"
      memory: 2Gi
    min:
      cpu: 200m
      memory: 6Mi
    maxLimitRequestRatio:
      cpu: 3
      memory: 2
    type: Pod
```

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$vim limit-Ratio.yaml
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl apply  -f  limit-Ratio.yaml -n limitrange-example
limitrange/mylimits configured
```

生成的 规则如下：

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl describe  limitranges  -n limitrange-example mylimits
Name:       mylimits
Namespace:  limitrange-example
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
Pod         cpu       200m  4    -                -              3
Pod         memory    6Mi   2Gi  -                -              2
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

可以看到 限制 pod 中 cpu 的 `Max Limit/Request Ratio` 比值 为 3，即 Requests 是 Limits 的 3 倍，最大和小之间最多可以相差 2 个单位

```yaml
┌──[root@vms81.liruilongs.github.io]-[/]
└─$cat pod1.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-demo
  name: pod-demo
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-demo
    resources:
      limits:
        cpu: '4'
      requests:
        cpu: '1'
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

我们创建这样一个 pod ，cpu 总和 最大和最小相差 3 个单位，Requests 是 Limits 的 4 倍

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl apply  -f pod1.yaml  -n limitrange-example
Error from server (Forbidden): error when creating "pod1.yaml": pods "pod-demo" is forbidden: [minimum memory usage per Pod is 6Mi.  No request is specified, maximum memory usage per Pod is 2Gi.  No limit is specified, cpu max limit to request ratio per Pod is 3, but provided ratio is 4.000000, memory max limit to request ratio per Pod is 2, but no request is specified or request is 0]
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

可以看到提示准入规则校验失败。相差了 4 倍不符合要求

`Pod 的 Limits 和 Requests 限制`，当设置了 Pod 的` Max 和 Min 、Max Limit/Request Ratio` 时，如果没有定义对应的 `Limits 和 Requests` ，则创建失败.

还使用之前的 Limitrange,

```bash
Namespace:  limitrange-example
Type        Resource  Min   Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---  ---------------  -------------  -----------------------
Pod         cpu       200m  4    -                -              3
Pod         memory    6Mi   2Gi  -                -              2
```

创建一个 pod ，只定义 `cpu` 的相关属性，没有定义 `memory` 相关属性

```yaml
┌──[root@vms81.liruilongs.github.io]-[/]
└─$cat pod1.yaml
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: pod-demo
  name: pod-demo
spec:
  containers:
  - image: nginx
    imagePullPolicy: IfNotPresent
    name: pod-demo
    resources:
      limits:
        cpu: '4'
      requests:
        cpu: '1'
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

Limitrange 中定义了 Max 所以必须要设置 Limit ，设置了 Min ，需要设置 Request。

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl apply  -f pod1.yaml  -n limitrange-example
Error from server (Forbidden): error when creating "pod1.yaml": pods "pod-demo" is forbidden: [minimum memory usage per Pod is 6Mi.  No request is specified, maximum memory usage per Pod is 2Gi.  No limit is specified, memory max limit to request ratio per Pod is 2, but no request is specified or request is 0]
```

创建失败 ，提示，没有内存相关的资源限制定义

### 资源约束 Demo

为命名空间配置 CPU 最小和最大约束

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl create namespace constraints-cpu-example
namespace/constraints-cpu-example created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$vim cpu-constraints.yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl apply -f cpu-constraints.yaml -n  constraints-cpu-example
limitrange/cpu-min-max-demo-lr created
```

创建 LimitRange yaml 文件

```yaml
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$ cat cpu-constraints.yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: cpu-min-max-demo-lr
spec:
  limits:
  - max:
      cpu: "800m"
    min:
      cpu: "200m"
    type: Container
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$
```

输出结果显示 CPU 的最小和最大限制符合预期。但需要注意的是，`尽管你在 LimitRange 的配置文件中你没有声明默认值，默认值也会被自动创建。`默人资源限制为 limits 和 requests 都是 max 的值

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl describe limitrange cpu-min-max-demo-lr -n  constraints-cpu-example
Name:       cpu-min-max-demo-lr
Namespace:  constraints-cpu-example
Type        Resource  Min   Max   Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---   ---   ---------------  -------------  -----------------------
Container   cpu       200m  800m  800m             800m           -
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$
```

创建一个不定义 资源限制 pod

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl run pod-demo-limitrange --image=nginx  --image-pull-policy=IfNotPresent -n constraints-cpu-e
xample
pod/pod-demo-limitrange created
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl get pods -n constraints-cpu-example
NAME                  READY   STATUS    RESTARTS   AGE
pod-demo-limitrange   1/1     Running   0          30s
```

可以看到使用了默认值，当前 pod 的 limits 值为 800m，requests 值为 800m，即最少为 800m pod 才能被调度，超过 800m 。pod 被 kill

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible/k8s-pod-create]
└─$kubectl get pods pod-demo-limitrange -n constraints-cpu-example   -o json | jq  .spec.containers[0].resources
{
  "limits": {
    "cpu": "800m"
  },
  "requests": {
    "cpu": "800m"
  }
}
```

这里的 demo 官网文档有比较全面的描述， 感兴趣小伙伴可以看看，[https://kubernetes.io/zh-cn/docs/concepts/policy/limit-range/](https://kubernetes.io/zh-cn/docs/concepts/policy/limit-range/) ,这里不多介绍

最后看下 使用 LimitRange 限制 PVC 的使用

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$cat limit-pvc.yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: storagelimits
spec:
  limits:
  - type: PersistentVolumeClaim
    max:
      storage: 2Gi
    min:
      storage: 1Gi
```

定义资源，查看限制信息

```bash
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl apply -f limit-pvc.yaml  -n limitrange-example
limitrange/storagelimits created
┌──[root@vms81.liruilongs.github.io]-[/]
└─$kubectl  describe  limitranges  -n limitrange-example
Name:                  storagelimits
Namespace:             limitrange-example
Type                   Resource  Min  Max  Default Request  Default Limit  Max Limit/Request Ratio
----                   --------  ---  ---  ---------------  -------------  -----------------------
PersistentVolumeClaim  storage   1Gi  2Gi  -                -              -
┌──[root@vms81.liruilongs.github.io]-[/]
└─$
```

关于 Limitrange 资源对象和小伙伴们分享到这样，之前会分享 资源配额管理 `Resource Quotas` 的一些笔记, 生活加油哦 `^_^`

## 博文参考
---
`《Kubernetes 权威指南 第四版 》`

[https://kubernetes.io/zh-cn/docs/concepts/policy/limit-range/](https://kubernetes.io/zh-cn/docs/concepts/policy/limit-range/)


