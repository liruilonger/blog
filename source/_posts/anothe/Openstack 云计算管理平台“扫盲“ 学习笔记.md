﻿---
title: Openstack 云计算管理平台“扫盲“ 学习笔记
date: 2021-06-15 
categories: Openstack
toc: true
tags:
  - Openstack
---
过自己想要的生活不是自私，要求别人按自己的意愿生活才是。 ----王尔德

<!-- more -->


## 写在前面
***
+ 嗯，学`K8S`,看到这个。所以了解下。
+ 这篇博客用于了解`Openstack`是什么，以及构成的`基础组件`。 
+ 笔记只是一些基础的东西，没有设计到Openstack基础环境配置及搭建，以后有这方面的需求，有时间在学。唉，要学的东西好多呀！**<font color="#67C23A">生活加油  ^ _ ^</font>**    2021.06.15

**<font color="#009688">过自己想要的生活不是自私，要求别人按自己的意愿生活才是。 ----王尔德</font>**
***

## 一、云介绍
### 什么是云计算？
对于到底什么是`云计算`，至少可以找到100种解释
根据 `美国国家标准与技术研究院(NIST)`定义：
`云计算`是一种`按使用量付费`的模式，这种模式提供`可用、便捷、按需的网络访问`，进入`可配置`的`计算资源共享池`(包括网络，服务器，存储，应用软件，服务)`云服务`，这些资源能够被快速提供，只需投入`少量的管理工具`，或与服务供应商进行`很少的交互`。通常涉及通过`互联网`来提供，`动态易扩展的虚拟机`的资源。
### 云计算的三大服务器模式
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210614225944712.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
### IaaS 云
+ `Iaas(Infrastructure as a Service)(an si)`,即`基础设施即服务`
+ 提供给消费者的服务是对所有`计算基础设施`的使用
+ 包括处理CPU、内存、存储、网络和其他基本的`计算资源`
+ 用户能够`部署`和`运行`任意软件，包括`操作系统`和`应用程序`
+ IaaS 通常分为三种用法：`公有云`、`私有云`和`混合云`
### PaaS 云
+ `PaaS (Platform-as-a-Service)(pa si)`，意思是`平台即服务`
+ 以`服务器平台`或者`开发环境`按需提供的服务
+ PaaS 不仅仅是单纯的基础平台，还包括对`该平台的技术支持`，`应用系统开发`、`优化`等服务
+ 简单的说，PaaS也可以说是`中间件即服务`
>例如：淘宝，京东， 只是提供了买卖双方交互的平台，卖方可以在平台上购买商铺，出售商品；买方可以到平台上选择不同的商铺购买商品(平台本身并不参与买卖过程)，电商ERP等

### SaaS 云
+ `SaaS(Software-as-a-Service)(sa si)软件即服务`
+ 是一种通过 Internet 提供软件的模式
+ `软件厂商`将应用软件部署在服务器上，客户可以根据自己实际需求，通过互联网自助购买所需的应用软件服务
+ SaaS 云服务也可以说是一种`软件云服务`
>#例如：苹果的APP，所有的软件都在苹果的app服务器上，用户可以根据自己的需求进行购买,或者在阿里云上租一个Mysql服务

### 知名云服务器商
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210614230732444.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

###  远程控制云服务器
#### 远程控制协议
+ `RDP` (端口号`3389`)，适用于 `Windows `服务器
>`步骤一`：使用组合快捷键: “Win+R”组合键, 可打开“运行”对话框。
`步骤二`：输入 "mstsc"，打开远程连接对话框。
+ `SSH`(端口号`22`)，适用于 `Linux `服务器、`防火墙`等设备
```bash
#lrzsz软件的功能，可以实现windows和linux系统直接进行文件传输
#linux中的文件上传到windows系统中，格式：[root@localhost ~]# sz
#windows中的文件上传到linux系统中指定目录下，先cd进入到指定目录下，然后拖拽即可
[root@localhost ~]$ yum -y install lrzsz
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615102720460.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

+ 使用 xshell 在`多个终端`同时执行`相同的命令`
>`第一步`：选择 “查看”，选择 “撰写栏”![在这里插入图片描述](https://img-blog.csdnimg.cn/20210614232059970.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
>`第二步`：选择图标，选择 “全部会话”
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210614232135997.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70) 
### 常用远程控制软件
`PC端`：Xshell、SecureCRT、Putty等
`手机端`：iTerminal、Termius、阿里云APP等

## 二、Openstack概述
`OpenStack `是一个由` NASA`(美国国家航空航天局)和 `Rackspace `合作研发并发起的项目
+ `OpenStack` 是一套 `IaaS` (基础云) 解决方案
+ `OpenStack` 是一个`开源`的`云计算管理平台`
+ 以 `Apache` 许可证为授权
### Openstack 组件
#### `Openstack` 结构图
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210614232647865.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
|组件名称|描述|
|--|--|
|Horizon组件 |提供web页面环境，可以让用户注册账号，创建云主机，管理云主机；|
|Keystone组件 |为所有组件提供公共的授权和认证；|
|Nova组件 |被部署在多个节点上，提供了虚拟机的管理；|
|Glance组件 |提供了镜像的管理功能，镜像则是构成虚拟机最基础的块结构；|
|Swift组件 |对象存储组件，只有在搭建存储云的时候才会使用；|
|Neutron组件 |网络组件，为openstack提供庞大的网络管理功能【会和centos7的networkmanager冲突，需要停止或卸载掉】|
|Cinder组件 |为openstack提供卷管理，永久存储的服务|

#### Openstack 组件 — Horizon
+ `Horizon 组件`:
	- [x] Horizon 为 `OpenStack` 服务的 `Web` 控制面板，它可以管理实例、镜像、创建密钥对，对实例添加卷、操作 Swift 容器等。
+ `Horizon` 具有如下一些`特点`：
	- [x] 实例管理：创建、终止实例，查看终端日志，VNC连接，添加卷等；
	- [x] 访问与安全管理：创建安全群组，管理密钥对，设置浮动 IP 等；
	- [x] 镜像管理：编辑或删除镜像
	- [x] 管理用户、配额及项目用

#### Openstack 组件 — Keystone
+ `Keystone` 组件
	- [x] 为其他服务器提供`认证`和`授权`的集中`身份管理服务`
	- [x] 也提供了集中的`目录服务`
	- [x] 支持多种身份`认证模式`，如密码认证、令牌认证、以及 AWS(亚马逊web服务)登录
	- [x] 为用户和其他服务提供了` SSO `认证服务
```js
#openstack部署完成以后，用户可以可以在openstack中创建很多台虚拟机，keystone组件可以提供对所
有虚拟机的权限认证的管理服务
```
####  Openstack 组件 — Nova(镜像管理)
+ `Nova` 组件
	- [x] 在节点上用于`管理虚拟机的服务`
	- [x] `Nova `是一个`分布式的服`务，能够与` Keyston`e 交互实现`认证`，与 `Glance `交互实现`镜像管理`
	- [x] `Nova `被设计成在`标准硬件上`能够进行`水平扩展`,安装在多台机器。
	- [x] 启动实例时，如果有则需要`下载镜像`
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615105447626.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
```js
#nova组件被部署在所有的节点上，用户在访问openstack要创建虚拟机时，openstack通过路由将请求发
送给openstack集群中的其中一个nova节点，nova会在对应的节点上创建虚拟机，并将结果返回给用户(用
户在虚拟机上进行的其他操作，也是由nova节点来完成的)
```
#### Openstack 组件 — Glance
+ `Glance` 组件(类似于面向对象设计模式的`原型模式`和`享元模式`的结合),镜像管理
	- [x] 扮演`虚拟机镜像`注册的角色
	- [x] 允许用户直接存储拷贝`服务器镜像`
	- [x] 这些镜像可以用于`新建虚拟机`的`模板`
```js
`Glance组件` #类似于将镜像光盘上传到网络中，其他虚拟机要安装操作系统时，直接使用该镜像光盘即可，不需要重新下载【利用qcow2 —— 写时复制的技术】
`COW`【copy on write】 #写时复制，例如：qcow2
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615115248444.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
#### Openstack 组件 — Swift
+ `Swift` 组件
	- [x] 这是对象存储的组件
	- [x] 对于大部分用户来说，swift 不是必须的
	- [x] 你只有存储数量到一定级别，而且是非结构化数据才有这样的需求

#### Openstack 组件 — Neutron(网络虚拟)
+ `Neutron` 组件,网络抽象。
	- [x] 一种软件，定义`网络服务`
	- [x] 用于创建网络、子网、路由器、管理浮动IP地址
	- [x] 可以实现虚拟交换机、虚拟路由器
	- [x] 可用于在项目中创建 VPN

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615120032122.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
```js
`用户在openstack中创建多台云主机时，多台云主机可能在不同的nova节点上`
`例如：A,B,C三家公司，分别购买了多台云主机，这些云主机都在不同的nova节点上`
`对于不同公司而言，自己购买的云主机需要可以互相连通，但是又不能和其他公司的云主机连通，这些功能
则由Neutron 组件可以实现`
```
####  Openstack 组件 — Cinder(存储虚拟)
+ `Cinder` 组件
	- [x] 为虚拟机管理存储卷的服务
	- [x] 为运行在 Nova 中的实例提供永久的块存储
	- [x] 可以通过快照进行数据备份
	- [x] 经常应用在实例存储环境中，如数据库文件

### Openstack 集群
#### 集群结构
![在这里插入图片描述](https://img-blog.csdnimg.cn/20210615120417909.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
#### 配置 yum 与 时间服务器
+ 搭建 yum 服务器
+ 搭建 chronyd 服务器

#### 嗯，需要学的东西挺多的，下周估计要工作了，时间不够，还要刻几方印，还有其他的东西要看，没法实战，所以暂时不看了，有这方面的需求，有时间在学。唉，要学的东西好多呀！ ^ _ ^    2021.06.15
