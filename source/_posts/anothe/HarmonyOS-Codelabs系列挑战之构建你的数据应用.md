---
title: HarmonyOS-Codelabs系列挑战之构建你的数据应用
tags:
  - HarmonyOS
categories:
  - HarmonyOS
toc: true
recommend: 1
keywords: HarmonyOS
uniqueId: '2021-09-25 13:35:09/HarmonyOS-Codelabs系列挑战之构建你的数据应用.html'
mathJax: false
date: 2021-09-25 21:35:09
thumbnail:
---
**<font color="009688"> 人生中的三大悲剧：熟到没法做情侣，饿得不知道吃什么，困得就是睡不着。**</font>
<!-- more -->
### <font color=orange>写在前面</font>
***
+ 嗯，有这样一个活动，所以搞了一个小Demo，顺便学习一个js的鸿蒙的开发方式,感兴趣的小伙伴积极参与，活动地址：[HarmonyOS线上Codelabs 系列挑战赛正式开启，多重好礼等你来拿！
](https://mp.weixin.qq.com/s/ClHXvYH4haj4-sJBUZ8hwA)
+ 博文主要是一些前端组件使用的一个Demo，用到`chart,switch,list,swiper,tabs` 等组件.
+ 整理感觉`JS`的开发方式比`安卓`要干净许多，好多都不用那么麻烦了，类似于`前端UI框架`一样，很方便，但是组件用起来和`UI框架`的比`相对不是特别灵活`，可能是因为`适用性`的问题，因为`多个终端兼容`。

**<font color="009688"> 人生中的三大悲剧：熟到没法做情侣，饿得不知道吃什么，困得就是睡不着。**</font>
 ***

### <font color=green>先看看效果：</font>
||||
|--|--|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/19246e149daa47a8a400ae30cc976269.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_18,color_FFFFFF,t_70,g_se,x_16)|![在这里插入图片描述](https://img-blog.csdnimg.cn/a41569028da646d5b5af6f49b9379bb5.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_17,color_FFFFFF,t_70,g_se,x_16)|![在这里插入图片描述](https://img-blog.csdnimg.cn/81bb9bb534784c5989c2e32ffa3c2907.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_17,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/3df27f7089c444f08c0160c6f086f433.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_17,color_FFFFFF,t_70,g_se,x_16)|![在这里插入图片描述](https://img-blog.csdnimg.cn/ee8dc7b93d4f4d549d79971500954562.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_18,color_FFFFFF,t_70,g_se,x_16)|![在这里插入图片描述](https://img-blog.csdnimg.cn/b6750a91493f4d32aa50719e6ee7aeaa.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_17,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/3256fdd6370d4bc9821917164e588a0e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_18,color_FFFFFF,t_70,g_se,x_16)| **<font color=brown>数据展示这块有点垃圾，占时也没想Dao别的处理方式，</font>** |**<font color=brown>只是用list循环了一个线性图的数据，时间原因，有时间在搞一下</font>**|
### 官方Demo
整体上是在官方的Demo基础上做的，这里把官方的Demo放在这里，感兴趣小伙伴可以看看：[【JS基础组件】switch、chart的使用](https://developer.huawei.com/consumer/cn/codelabsPortal/carddetails/HarmonyOS-ChartSwitch)


### 下面开始愉快尝试

|步骤|
|--|
|**<font color=chocolate>新建项目</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/cea13b9a2ef64bf789de1c4325b05849.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/d246a83daff84339bdd81fcfa9521bce.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=royalblue>连接模拟器</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/7ce992f25c614869a1b8f4e448168816.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

### <font color=chocolate>代码编写</font>

|**<font color=red>主要是pages文件中</font>**|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/70f8fd6ece0f473c90d2e3f0b6ccd207.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|


#### <font color=purple>index.js</font>
```js
export default {
    data: {
        interval: null, // 定时器对象
        showText: true, // 是否显示文本
        title: "数据展示应用Demo",
        textOn: '动态',
        textOff: '静态',
        allowScale: true, // 文本尺寸跟随系统设置字体缩放
        dataLength: 30, // 数据长度
        barGroup: 3, // 柱状图组数
        DataSegment: {
            startColor: '#8477DF',
            endColor: '#97CF0A2C',
            value: 50,
            name: "中部",
        },
        DataSegments:[
            {
                startColor: '#8477DF',
                endColor: '#97CF0A2C',
                value: 20,
                name: "中部",
            },
            {
                value: 20,
                name: "中部",
            },
            {
                value: 20,
                name: "西部",
            },
            {
                startColor: '#84DF',
                endColor: '#97CA2C',
                value: 20,
                name: "中部",
            }
        ],
        lineData: null, // 线形图数据
        lineOps: { // 线形图样式
            xAxis: {
                min: 0,
                max: 5,
                display: true
            },
            yAxis: {
                min: 0,
                max: 100,
                display: true
            },
            series: {
                lineStyle: {
                    width: '1px',
                    smooth: true
                },
                headPoint: {
                    shape: 'circle',
                    size: 10,
                    strokeWidth: 2,
                    fillColor: '#ffffff',
                    strokeColor: '#8477DF',
                    display: true
                },
                loop: {
                    margin: 2
                }
            }
        },
        percent: 100, // 量规图进度

        barData: [ // 柱状图数据
            {
                fillColor: '#97CF0A2C',
                data: [20, 20, 99, 56, 23]
            },
            {
                fillColor: '#6D0A7ACF',
                data: [99, 88, 2, 67, 12]
            },
            {
                fillColor: '#6A0ACFA1',
                data: [56, 2, 77, 99, 78]
            }
        ],
        barOps: { // 柱状图样式

            xAxis: {
                min: 0,
                max: 20,
                display: true,
                axisTick: 5
            },
            yAxis: {
                min: 0,
                max: 100,
                display: true
            }
        }
    },
// 初始化
    onInit() {
        this.changeLine();
    },
    change(e) {
        if (e.checked) {
            this.interval = setInterval(() => {
                this.changeLine(); // 更新线形图数据
                this.changeGauge(); // 更新量规图数据
                this.changeBar(); // 更新柱状图数据
            }, 1000)
        } else {
            clearInterval(this.interval);
        }
    },
// 更新线形图数据
    changeLine() {
        var dataArray = [];
        for (var i = 0; i < this.dataLength; i++) {
            var nowValue = Math.floor(Math.random() * 99 + 1);
            var obj = {
                "value": nowValue, // Y坐标
                "description": nowValue + "", // 当前点的注释内容
                "textLocation": "top", // 注释内容位置
                "textColor": "#CDCACA", // 注释内容颜色
                "pointStyle": { // 节点形状
                    "shape": "circle", // 形状
                    "size": 5, // 形状大小
                    "fillColor": "#CF0A2C", // 填充颜色
                    "strokeColor": "#CF0A2C" // 边框颜色
                }
            };
            dataArray.push(obj);
        }

        this.lineData = [
            {
                strokeColor: '#0081ff',
                fillColor: '#FF07CDC4', // 线的颜色
                data: dataArray,
                gradient: true,
            }
        ]
    },
// 更新量规图数据
    changeGauge() {
        this.percent = parseInt(this.percent) >= 99 ? 0 : parseInt(this.percent) + 1;

    },
// 更新柱状图数据
    changeBar() {
        for (var i = 0;i < this.barGroup; i++) {
            var dataArray = this.barData[i].data;
            for (var j = 0;j < this.dataLength; j++) {
                dataArray[j] = Math.floor(Math.random() * 99 + 1);
            }
        }
        this.barData = this.barData.splice(0, this.barGroup + 1);
    },
    changes(e) {
        console.log("Tab index: " + e.index);
    },
}


```
#### <font color=amber>index.css</font>
```css
.container{
    display:flex;
    flex-direction:column;
}
.line-class{
    display:flex;
    flex-direction:column;
}
.title{
    font-size: 30px;
}



.switch-block {
    margin-top: 10px;
    width: 98%;
    height: 50px;
    display:flex;
    justify-content: space-between;
}
.switch {
    text-padding: 10px; /* texton/textoff中最长文本两侧距离滑块边界的距离 */
    font-size: 12px;
    texton-color: #5F5F5F; /* 选中字体颜色 */
    textoff-color: #707070; /* 未选中字体颜色 */
}


 .gauge-block, .bar-block {
    margin-top: 10px;
    position: relative;
    width: 98%;
    border-radius: 10px;
    background-color: #25FAB27B;
    height: 50%;
    justify-content: center;
}
.chart-block{
    margin-top: 20px;
    position: relative;
    width: 98%;
    height: 90%;
    border-radius: 10px;
    background-color: #25FAB27B;
    justify-content: center;
}
.chart-block-class{
    margin-top: 20px;
    position: relative;
    width: 98%;
    border-radius: 10px;

    height: 60%;
    justify-content: center;
}
.text-speed{
    position: absolute;
    top: 10px;
    left: 20px;
    width: 10px;
    font-size: 10px;
}
.chart-data {
    margin: 10px 5px 10px;
    width: 100%;
    height: 90%;
}
.text-time {
    position: absolute;
    font-size: 10px;
    bottom: 2px;
    right: 10px;
}
.gauge-block, .bar-block {
    margin-top: 10px;
}

.data-gauge{
    width: 200%;
    height: 60%;
    margin: 5px 5px 5px; /* 刻度条的宽度 */
    start-angle: 0; /* 起始角度 */
    total-angle: 270; /* 最大角度 */
    stroke-width: 10px;
    colors:  #fab27b,#b2d235, #8DCF0A2C ,#D2A68DFF, #BA17A98E,#bd6758,#f58220,#5c7a29,#1a2933; /* 颜色 */
    weights: 1, 1, 1,1,1,1,1,1,1; /* 颜色占比权重 */
}
.data-bar {
    width: 100%;
    height: 90%;
    margin: 10px 5px 10px;
}
.swiper {
    indicator-color: #cf2411;
    indicator-size: 25px;
    indicator-bottom: 20px;
    indicator-right: 30px;
}

```
#### <font color=royalblue>index.hml</font>
```html
<div class="container">
    <div class="container">
        <div class="switch-block">
            <text class="title">
                {{ title }}
            </text>
            <switch class="switch"
                    showtext="{{ showText }}"
                    texton="{{ textOn }}"
                    textoff="{{ textOff }}"
                    allow-scale="{{ allowScale }}"
                    onchange="change">
            </switch>
        </div>
        <tabs class="tabs" index="0" vertical="false" onchange="changes">
            <tab-bar class="tab-bar" mode="fixed">
                <text class="tab-text">数据展示</text>
                <text class="tab-text"> 数据详情</text>
            </tab-bar>
            <tab-content class="tabcontent" scrollable="true">
                <div>
                    <tabs class="tabs" index="0" vertical="false" onchange="changes">
                        <tab-bar class="tab-bar" mode="fixed">
                            <text class="tab-text">线形图</text>
                            <text class="tab-text">柱状图</text>
                            <text class="tab-text">量规图</text>
                            <text class="tab-text">圆形图表</text>
                        </tab-bar>
                        <tab-content class="tabcontent" scrollable="true">
                            <div class="line-class">
                                <div class="bar-block">
                                    <text class="text-speed">利润</text>
                                    <chart class="chart-data" type="line" ref="linechart" options="{{ lineOps }}"
                                           datasets="{{ lineData }}">
                                    </chart>
                                    <text class="text-time">线形图</text>
                                </div>
                                <div class="bar-block">
                                    <text class="text-speed">销量</text>
                                    <chart class="data-bar" type="bar" id="bar-chart" options="{{ barOps }}"
                                           datasets="{{ barData }}">
                                    </chart>
                                    <text class="text-time">年份</text>
                                </div>

                            </div>
                            <div class="line-class">
                                <div class="bar-block">
                                    <text class="text-speed">销量</text>
                                    <chart class="data-bar" type="bar" id="bar-chart" options="{{ barOps }}"
                                           datasets="{{ barData }}">
                                    </chart>
                                    <text class="text-time">年份</text>
                                </div>
                                <div class="bar-block">
                                    <text class="text-speed">利润</text>
                                    <chart class="chart-data" type="line" ref="linechart" options="{{ lineOps }}"
                                           datasets="{{ lineData }}">
                                    </chart>
                                    <text class="text-time">线形图</text>
                                </div>
                            </div>
                            <div class="chart-block">
                                <chart type="gauge" percent="{{ percent }}"></chart>
                                <text class="text-speed">量规图</text>
                            </div>

                            <div>
                                <swiper class="swiper" id="swiper" index="0" indicator="true" loop="true"
                                        interval='10000' digital="false" autoplay="true">
                                    <div class="swiperContent">
                                        <div class="chart-block-class">
                                            <chart type="rainbow" animationduration="1000000"
                                                   effects="true"
                                                   segments="{{ DataSegments }}"></chart>
                                            <text class="text-speed">占比类圆形图表</text>
                                        </div>
                                    </div>
                                    <div class="swiperContent">
                                        <div class="chart-block-class">
                                            <chart type="loading" segments="{{ DataSegment }}">
                                            </chart>
                                            <text class="text-speed">加载类圆形图表</text>
                                        </div>
                                    </div>
                                    <div class="swiperContent">
                                        <div class="chart-block-class" >
                                            <chart  type="progress" effects="true"
                                                   segments="{{ DataSegment }}">
                                            </chart>
                                            <text class="text-speed">进度类圆形图表</text>
                                        </div>
                                    </div>
                                </swiper>
                            </div>
                        </tab-content>
                    </tabs>
                </div>
                <div>
                    <div class="container">
                        <list class="todo-wrapper">
                            <list-item for="{{ barData }}" class="todo-item">
                                <text class="todo-title">{{ $item.data }}</text>
                            </list-item>

                        </list>
                        <list class="todo-wrapper">
                            <list-item for="{{ lineData.data }}" class="todo-item">
                                <text class="todo-title">{{ $item.value }}</text>
                            </list-item>
                        </list>
                    </div>

                <!--  <text class="title">
                        {{ barData }}
                    </text>
                    <text class="title">
                        {{ lineData }}
                    </text>

                    <text class="title">
                        {{ DataSegment }}
                    </text>-->
                </div>
            </tab-content>
        </tabs>
    </div>

</div>
```