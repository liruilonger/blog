﻿---
title: 使用Markdown写博客的一些总结
date: 2021/6/3 20:46:25
categories: 写作
toc: true
tags:
  - Markdown

---
傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波

<!-- more -->


##### `流水线-`流水线吞吐量计算

·`流水线的吞吐率`(Though Put rate, `TP`)是指在`单位时间内流水线所完成的任务数量`或`输出的结果数量`。计算流水线吞吐率的`最基本的公式如下`:
$$
TP=\cfrac{指令条数}{流水线执行时间} 
$$
`流水线最大吞吐量(实践公式)等于流水线周期的导数`：
$$
TP_{max} = \lim_{n\longrightarrow \propto} \cfrac{n}{(k+n-1)\Delta t}=\cfrac{1}{\Delta t}
$$

```js
·`流水线的吞吐率`(Though Put rate, `TP`)是指在`单位时间内流水线所完成的任务数量`或`输出的结果数量`。计算流水线吞吐率的`最基本的公式如下`:
$$
TP=\cfrac{指令条数}{流水线执行时间} 
$$
`流水线最大吞吐量(实践公式)等于流水线周期的导数`：
$$
TP_{max} = \lim_{n\longrightarrow \propto} \cfrac{n}{(k+n-1)\Delta t}=\cfrac{1}{\Delta t}
$$
```

>+ 图片+ 表格 的格式化图片的处理方式，可以放大图片，博文里涉及到的图片我喜欢放到表格里，这个看着整齐一点，一些平台放到表格里的图片是没办法单机放大的

##### 🎈🎈🎈🎈 手机端🎈🎈🎈🎈 

|主页|音乐|标签|关于|分类|
|--|--|--|--|--|
|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242334.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242366.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242384.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242403.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242436.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|

```js
##### 🎈🎈🎈🎈 手机端🎈🎈🎈🎈 

|主页|音乐|标签|关于|分类|
|--|--|--|--|--|
|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242334.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242366.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242384.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242403.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![请添加图片描述](https://img-blog.csdnimg.cn/20210628190242436.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|
```

# <font color=plum>模板</font>
## <font color="#C0C4CC">模板一(笔记类模板)</font>

### 写在前面
***
+ 用`Markdown`写博客一年多了，最开始是用富文本写，后来发现`Markdown`更适合我，而且`CSDN`提供了导入导出的功能，图片可以云存储，所以导出了博文在本地也可以直接看，尤其是笔记类型很方便。所以这里总结一下自己常用模板和小伙伴们分享下<font color="#C0C4CC">[这里写一些为啥要整理这部分笔记，整理这部分笔记的前因后果]</font>
+ 笔记主要是关于自己使用`Morkdown`的一些`常用模板`的`版式`总结。<font color="#C0C4CC">[这里写整理笔记的具体方式，是读那本书笔记，看那个视频笔记，或者对于那个问题的笔记]</font>
+ 笔记由两部分内容构成: `笔记类模板`和`技术点问题类模板`<font color="#C0C4CC">[这里写笔记内容的构成]</font>


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font><font color="#C0C4CC">[这里写一句鼓励自己的话,可以加一个颜色(可选)]</font>
 ***

<font color="#C0C4CC">博客内容</font>

# <font color="#C0C4CC">一级标题</font>
## <font color="#C0C4CC">二级标题</font>
### <font color="#C0C4CC">三级标题</font>
#### <font color="#C0C4CC">四级标题</font>
##### <font color="#C0C4CC">五级标题</font>
###### <font color="#C0C4CC">六级标题</font>

`内容文字颜色设置`
<font color="#000000">具体的正文</font>
<font color="#303133">具体的正文</font>
<font color="#606266">具体的正文</font>
<font color="#909399">具体的正文</font>
<font color="#C0C4CC">具体的正文</font>

`重点标记性文字设置`
**<font color="#409EFF">具体的标记性正文</font>**
**<font color="#67C23A">具体的标记性正文</font>**
**<font color="#E6A23C">具体的标记性正文</font>**
**<font color="#F56C6C">具体的标记性正文</font>**
**<font color="#009688">具体的标记性正文</font>**
**<font color="##00C5CD">具体的标记性正文</font >**

```js
## <font color="#C0C4CC">模板一(笔记类模板)</font>

### 写在前面
***
+ 用`Markdown`写博客一年多了，最开始是用富文本写，后来发现`Markdown`更适合我，而且`CSDN`提供了导入导出的功能，图片可以云存储，所以导出了博文在本地也可以直接看，尤其是笔记类型很方便。所以这里总结一下自己常用模板和小伙伴们分享下<font color="#C0C4CC">[这里写一些为啥要整理这部分笔记，整理这部分笔记的前因后果]</font>
+ 笔记主要是关于自己使用`Morkdown`的一些`常用模板`的`版式`总结。<font color="#C0C4CC">[这里写整理笔记的具体方式，是读那本书笔记，看那个视频笔记，或者对于那个问题的笔记]</font>
+ 笔记由两部分内容构成: `笔记类模板`和`技术点问题类模板`<font color="#C0C4CC">[这里写笔记内容的构成]</font>


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font><font color="#C0C4CC">[这里写一句鼓励自己的话,可以加一个颜色(可选)]</font>
 ***

<font color="#C0C4CC">博客内容</font>

# <font color="#C0C4CC">一级标题</font>
## <font color="#C0C4CC">二级标题</font>
### <font color="#C0C4CC">三级标题</font>
#### <font color="#C0C4CC">四级标题</font>
##### <font color="#C0C4CC">五级标题</font>
###### <font color="#C0C4CC">六级标题</font>

`内容文字颜色设置`
<font color="#000000">具体的正文</font>
<font color="#303133">具体的正文</font>
<font color="#606266">具体的正文</font>
<font color="#909399">具体的正文</font>
<font color="#C0C4CC">具体的正文</font>

`重点标记性文字设置`
**<font color="#409EFF">具体的标记性正文</font>**
**<font color="#67C23A">具体的标记性正文</font>**
**<font color="#E6A23C">具体的标记性正文</font>**
**<font color="#F56C6C">具体的标记性正文</font>**
**<font color="#009688">具体的标记性正文</font>**
**<font color="##00C5CD">具体的标记性正文</font >**
```
***
## <font color="#C0C4CC">模板二(技术点问题类模板)</font>
### <font color="#F56C6C">我的需求：
+ 项目中遇到的`什么Bug`需要解决，遇到的`哪一类技术难点`需要攻克<font color="#C0C4CC">[这里写一些为啥要整理这部分笔记，整理博客的前因后果]

### <font color="#F56C6C">我需要解决的问题：
+ 实现业务和流程的解耦等<font color="#C0C4CC">[这里写一我的需求bug需要解决什么问题]
### <font color="#F56C6C">我是这样做的：
+ `百度`寻找解决办法，向公司大佬`请教`。
+ 在构建上基于某设计模式，利用了什么特性，结合了什么思想等。
+ <font color="#C0C4CC">[这里大概描述一下我解决问题的步骤，方式]

**<font color="009688">你存在的意义，诠释了我仓促青春里的爱情。</font>**<font color="#C0C4CC">[这里写一句鼓励自己的话,可以加一个颜色(可选)]</font>

***
<font color="#C0C4CC">博客内容</font>

# <font color="#C0C4CC">一级标题</font>
## <font color="#C0C4CC">二级标题</font>
### <font color="#C0C4CC">三级标题</font>
#### <font color="#C0C4CC">四级标题</font>
##### <font color="#C0C4CC">五级标题</font>
###### <font color="#C0C4CC">六级标题</font>

`内容文字颜色设置`
<font color="#000000">具体的正文</font>
<font color="#303133">具体的正文</font>
<font color="#606266">具体的正文</font>
<font color="#909399">具体的正文</font>
<font color="#C0C4CC">具体的正文</font>

`重点标记性文字设置`
**<font color="#409EFF">具体的标记性正文</font>**
**<font color="#67C23A">具体的标记性正文</font>**
**<font color="#E6A23C">具体的标记性正文</font>**
**<font color="#F56C6C">具体的标记性正文</font>**
**<font color="#009688">具体的标记性正文</font>**
**<font color="##00C5CD">具体的标记性正文</font >**

```js
### <font color="#F56C6C">我的需求：
+ 项目中遇到的`什么Bug`需要解决，遇到的`哪一类技术难点`需要攻克<font color="#C0C4CC">[这里写一些为啥要整理这部分笔记，整理博客的前因后果]

### <font color="#F56C6C">我需要解决的问题：
+ 实现业务和流程的解耦等<font color="#C0C4CC">[这里写一我的需求bug需要解决什么问题]
### <font color="#F56C6C">我是这样做的：
+ `百度`寻找解决办法，向公司大佬`请教`。
+ 在构建上基于某设计模式，利用了什么特性，结合了什么思想等。
+ <font color="#C0C4CC">[这里大概描述一下我解决问题的步骤，方式]

**<font color="009688">你存在的意义，诠释了我仓促青春里的爱情。**<font color="#C0C4CC">[这里写一句鼓励自己的话,可以加一个颜色(可选)]</font>

***
<font color="#C0C4CC">博客内容</font>

# <font color="#C0C4CC">一级标题
## <font color="#C0C4CC">二级标题
### <font color="#C0C4CC">三级标题
#### <font color="#C0C4CC">四级标题
##### <font color="#C0C4CC">五级标题
###### <font color="#C0C4CC">六级标题

`内容文字颜色设置`
<font color="#000000">具体的正文</font>
<font color="#303133">具体的正文</font>
<font color="#606266">具体的正文</font>
<font color="#909399">具体的正文</font>
<font color="#C0C4CC">具体的正文</font>

`重点标记性文字设置`
**<font color="#409EFF">具体的标记性正文</font>**
**<font color="#67C23A">具体的标记性正文</font>**
**<font color="#E6A23C">具体的标记性正文</font>**
**<font color="#F56C6C">具体的标记性正文</font>**
**<font color="#009688">具体的标记性正文</font>**
**<font color="##00C5CD">具体的标记性正文</font >**
```

***

#### [模板一具体的Demo](https://blog.csdn.net/sanhewuyang/article/details/116557620)
#### [模板二具体的Demo](https://blog.csdn.net/sanhewuyang/article/details/109671135)
#### [模板二具体的Demo](https://blog.csdn.net/sanhewuyang/article/details/113191009)

# <font color=seagreen>关于一些建议</font>

#### <font color=chocolate>快捷键</font>

**<font color=yellowgreen>建议对于常用语法加一个快捷键的方式，比如以下之类</font>**
```js
ctrl + `  ===> `选中标记`
table + enter === > 表格模板
ctrl + *  ===>  *选中标记*
```
#### <font color=seagreen>自定义模版功能</font>
**<font color=plum>自己有一个自己的模板库，用的时候可以选择，写随笔很方便</font>**
#### Markdown样式自定义
**<font color=camel>自己可以定义自己得css样式。或者提供一些样式选择，类似现在的那个代码样式的组件，感觉很不错。</font>**



## 自定义版权
***
© 2018-2021 liruilong@aliyun.com，All rights reserved.
 本文为博主原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接和本声明。

***
