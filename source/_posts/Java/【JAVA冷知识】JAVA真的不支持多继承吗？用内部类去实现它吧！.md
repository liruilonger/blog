---
title: 【JAVA冷知识】JAVA居然支持多继承吗？让我们用内部类去吧
tags:
  - JAVA
categories:
  - JAVA
toc: true
recommend: 1
keywords: java
uniqueId: '2022-02-10 18:29:24/【JAVA冷知识】JAVA真的不支持多继承吗？用内部类去实现它！.html'
mathJax: false
date: 2022-02-11 02:29:24
thumbnail:
---
**<font color="009688">与亲近之人不要说气话，不要说反话，不要不说话。——烽火戏诸侯 《剑来》**</font>
<!-- more -->
## 写在前面
***
+ `JAVA冷知识`,今天和小伙伴分享的是
+ 通过内部类的方式实现`JAVA的多继承`
+ 一个`Demo`和`JDK源码`中的具体场景
+ 部分内容参考
  + `《编写高质量代码(改善Java程序的151个建议)》`
  + `《Effective Java》中文版第3版`
+ 博文理解有误的地方小伙伴留言私信一起讨论


**<font color="009688">与亲近之人不要说气话，不要说反话，不要不说话。——烽火戏诸侯 《剑来》**</font>
 ***

**<font color=camel>众多周知，对于面向对象语言来讲，JAVA是不支持多继承的，只支持单继承，但是提供了接口来补偿。</font>**

**<font color=camel>在实际的项目中，接口更多的用于行为的委托，把类本身一些是共性但又是特定的行为委托给一个接口的具体实现，当然接口也可以用于属性的委托，对象结构型的设计模式大都采用接口的方式来实现对对象内部组成的注册和操作</font>**


**<font color=red>如果实现java的多继承，其实很简单，关键是对于内部类的特征的掌握，内部类可以继承一个与外部类无关的类，保证了内部类天然独立性，根据这个特性从而实现一个类可以继承多个类的效果</font>**

>下面我们看一个Demo，声明父母两个接口，实现父母两个类，看如何通过内部类来继承父母类，而不是通过，接口委托的方式，

### <font color=camel>一个Demo</font>

父亲接口
```java
package com.liruilong;

/**
 * @Project_name: workspack
 * @Package: com.liruilong
 * @Description: 父亲接口
 * @Author: 1224965096@qq.com
 * @WeChat_Official_Accounts: 山河已无恙
 * @blog: https://liruilong.blog.csdn.net/
 * @Date: 2022/2/12  2:48
 */
public interface Father {
    /**
     * @return: int
     * @Description 强壮的行为
     * @author LiRuilong
     * @date  2022/2/12  2:49
     **/
     int strong();
}
```
父亲实现类
```java
package com.liruilong;

/**
 * @Project_name: workspack
 * @Package: com.liruilong
 * @Description: 父亲类
 * @Author: 1224965096@qq.com
 * @WeChat_Official_Accounts: 山河已无恙
 * @blog: https://liruilong.blog.csdn.net/
 * @Date: 2022/2/12  2:51
 */
public class FatherImpl implements Father {

    static  public   String height = "身体超高";

   /**
    * @return: int
    * @Description  强壮值
    * @author LiRuilong
    * @date  2022/2/12  2:51
    **/
    @Override
    public int strong() {
        return 8;
    }
}
```
母亲接口
```java
package com.liruilong;

/**
 * @Project_name: workspack
 * @Package: com.liruilong
 * @Description: 母亲接口
 * @Author: 1224965096@qq.com
 * @WeChat_Official_Accounts: 山河已无恙
 * @blog: https://liruilong.blog.csdn.net/
 * @Date: 2022/2/12  2:50
 */
public interface Mother {
   /**
    * @return: int
    * @Description 温柔的行为
    * @author LiRuilong
    * @date  2022/2/12  2:50
    **/
   int Kind();
}
```
母亲实现类
```java
package com.liruilong;

/**
 * @Project_name: workspack
 * @Package: com.liruilong
 * @Description: 母亲类
 * @Author: 1224965096@qq.com
 * @WeChat_Official_Accounts: 山河已无恙
 * @blog: https://liruilong.blog.csdn.net/
 * @Date: 2022/2/12  2:51
 */
public class MotherImpl implements Mother{
    static   public  String pretty = "脸蛋特别漂亮";
   /**
    * @return: int
    * @Description 温柔值
    * @author LiRuilong
    * @date  2022/2/12  2:51
    **/
    @Override
    public int Kind() {
        return 8;
    }
}

```
OK，准备工作做好了， 看我们如何实现。
```java
package com.liruilong;

import java.util.logging.Logger;

/**
 * @Project_name: workspack
 * @Package: com.liruilong
 * @Description: 孩子类
 * @Author: 1224965096@qq.com
 * @WeChat_Official_Accounts: 山河已无恙
 * @blog: https://liruilong.blog.csdn.net/
 * @Date: 2022/2/12  13:16
 */
public class Son extends FatherImpl implements Mother {
    static Logger logger = Logger.getAnonymousLogger();

    MotherSpecial motherSpecial = new MotherSpecial();


    @Override
    public int strong() {
        return super.strong() + 1;
    }

    @Override
    public int Kind() {
        return motherSpecial.Kind();
    }


    @Override
    public String toString() {
        return "Son{" +
                "height=" + height +"," +
                "pretty=" + MotherSpecial.pretty +
                '}';
    }


    public class MotherSpecial extends MotherImpl {
        @Override
        public int Kind() {
            return super.Kind() - 1;
        }
    }

    public static void main(String[] args) {
        Son son = new Son();
        logger.info(son.toString());
        logger.info(son.strong()+"");
        logger.info(son.Kind()+"");
    }

}
```
我们用内部类继承一个外部类无关的类，实现了`Son`类的多继承
```java
Bad level value for property: .level
Bad level value for property: java.util.logging.ConsoleHandler.level
Can''t set level for java.util.logging.ConsoleHandler
二月 12, 2022 2:02:06 下午 com.liruilong.Son main
信息: Son{height=身体超高,pretty=脸蛋特别漂亮}
二月 12, 2022 2:02:06 下午 com.liruilong.Son main
信息: 9
二月 12, 2022 2:02:06 下午 com.liruilong.Son main
信息: 7

Process finished with exit code 0

```
这里只是讨论这样的写法，我个人认为，这种方法有些`鸡肋`。这种方式实现的`多继承`，完全可以通`组合`的方式来实现，我们简单分析一下优缺点

## <font color=camel>优缺点分析</font>

### 优点： 
通过内部类的方式，把继承关系控制在类的内部，理论上`比通过组合的方式更加安全`，代码可读性要好一点。

更符合设计原则中的`迪米特法则`，又称`最少知道原则(Demeter Principle)`，一个实体应当尽量少地与其他实体之间发生相互作用，使得系统功能模块相对独立。


### 缺点：

首先通过继承的方式实现，`打破了类的封装性`，子类依赖于其超类中特定功能的实现细节。 超类的实现有可能会随着发行版本的不同而有所变化，如果真的发生了变化，即使子类的代码完全没有改变，但是子类可能会遭到破坏因而，子类必须要跟着其超类的更新而演变，除非超类是专门为了扩展而设计的，并且具有`很好的文挡说明`。

其次，通过这样的方式实现的，不符合常态思想，尤其内部类同名的情况，容易被忽略某些特性(见JDK源码)。 而且不满足`合成复用原则(Composite Reuse Principle)`，尽量使用合成/聚合的方式，而不是使用继承。


## <font color=red>JDK源码中的运用</font>

关于通过内部类来实现`java多继承`的`JDK`场景，我们简单分析一下

### <font color=red>asList</font>
```java
List<Integer> integers = Arrays.asList(1, 2, 3);
```
这个代码小伙伴们一定不陌生，这里通过`Arrays`工具类来生成一个`List`，但是这里的`List`并不是真正的`ArrayList`，而是在`Arrays工具类`内部定义的一个`继承了AbstractList的静态内部类ArrayList`,这里java通过内部类的方式巧妙的实现了。
```java
  .......
    @SafeVarargs
    @SuppressWarnings("varargs")
    public static <T> List<T> asList(T... a) {
        return new ArrayList<>(a);
    }

    /**
     * @serial include
     */
    private static class ArrayList<E> extends AbstractList<E>
        implements RandomAccess, java.io.Serializable
    {
        private static final long serialVersionUID = -2764017481108945198L;
        private final E[] a;

        ArrayList(E[] array) {
            a = Objects.requireNonNull(array);
        }
    .................    
```
>但是这里同样需要注意的是`通过内部类实现多继承要考虑其类的特殊性`：

**<font color=red>这样生成的`List`调用`add`方法会抛`不支持的操作的异常`，基于`Arrays`的`ArrayList`是一个静态私有内部类，除了Arrays能访问以外，其他类都不能访问，`正常的ArrayList中add方法是ArrayList父类提供，Arrays的内部类ArrayList没有覆写add方法。`</font>**

下面源码为`ArrayList静态内部类`实现的个方法。

```java
    /**
     * @serial include
     */
    private static class ArrayList<E> extends AbstractList<E>
        implements RandomAccess, java.io.Serializable
    {
        private static final long serialVersionUID = -2764017481108945198L;
        private final E[] a;

        ArrayList(E[] array) {
            a = Objects.requireNonNull(array);
        }

        @Override
        public int size() {
            return a.length;
        }

        @Override
        public Object[] toArray() {
            return a.clone();
        }

        @Override
        @SuppressWarnings("unchecked")
        public <T> T[] toArray(T[] a) {
            int size = size();
            if (a.length < size)
                return Arrays.copyOf(this.a, size,
                                     (Class<? extends T[]>) a.getClass());
            System.arraycopy(this.a, 0, a, 0, size);
            if (a.length > size)
                a[size] = null;
            return a;
        }

        @Override
        public E get(int index) {
            return a[index];
        }

        @Override
        public E set(int index, E element) {
            E oldValue = a[index];
            a[index] = element;
            return oldValue;
        }

        @Override
        public int indexOf(Object o) {
            E[] a = this.a;
            if (o == null) {
                for (int i = 0; i < a.length; i++)
                    if (a[i] == null)
                        return i;
            } else {
                for (int i = 0; i < a.length; i++)
                    if (o.equals(a[i]))
                        return i;
            }
            return -1;
        }

        @Override
        public boolean contains(Object o) {
            return indexOf(o) != -1;
        }

        @Override
        public Spliterator<E> spliterator() {
            return Spliterators.spliterator(a, Spliterator.ORDERED);
        }

        @Override
        public void forEach(Consumer<? super E> action) {
            Objects.requireNonNull(action);
            for (E e : a) {
                action.accept(e);
            }
        }

        @Override
        public void replaceAll(UnaryOperator<E> operator) {
            Objects.requireNonNull(operator);
            E[] a = this.a;
            for (int i = 0; i < a.length; i++) {
                a[i] = operator.apply(a[i]);
            }
        }

        @Override
        public void sort(Comparator<? super E> c) {
            Arrays.sort(a, c);
        }
    }
```

即没有实现`add`和`remove`方法，所以`asList`返回的为一个`长度不可变的列表`，数组为多长转换为列表为多长，即`不在保持列表动态变长的特性`。

### <font color=blue>subList</font>
嗯，不多讲，直接上代码
```java
        ArrayList arrayList = new ArrayList();
        LinkedList linkedList = new LinkedList();
        Vector vector = new Vector();
        linkedList.subList(2,3);
        arrayList.subList(2,3);
        vector.subList(2,3);
```
`List`提供一个`subList`方法,与`String`的`subString`有点类似,这里的`List`通过`subList`生成子`list`方式也是通过`内部类继承方式的多继承`实现的。

**<font color=purple>当然这里，具体需要分析，`ArrayList`和`其他List`的实现的方式略有不同</font>**

`ArrayList`是自己定义的`内部类SubList`继承`AbstractList`实现的

```java
public class ArrayList<E> extends AbstractList<E>
        implements List<E>, RandomAccess, Cloneable, java.io.Serializable
{
  .......
    public List<E> subList(int fromIndex, int toIndex) {
        subListRangeCheck(fromIndex, toIndex, size);
        return new SubList(this, 0, fromIndex, toIndex);
    }
.....
    private class SubList extends AbstractList<E> implements RandomAccess {
        private final AbstractList<E> parent;
        private final int parentOffset;
        private final int offset;
        int size;
    .........    
```

`LinkedList`的`subList`方法是由`AbstractList`实现的，它会根据`是不是随机存储`提供不同的`实现方法`，`subList`返回的类也是`AbstractList的子类SubList`。
```java
public abstract class AbstractList<E> extends AbstractCollection<E> implements List<E> {
  ........
   public List<E> subList(int fromIndex, int toIndex) {
        return (this instanceof RandomAccess ?
                new RandomAccessSubList<>(this, fromIndex, toIndex) :
                new SubList<>(this, fromIndex, toIndex));
    }
   class SubList<E> extends AbstractList<E> {
     ...
    }
   class RandomAccessSubList<E> extends SubList<E> implements RandomAccess{
    .......
    }
    ........ 
}
```

**<font color=green>这里需要注意的是，不管是ArrayList还是LinkedList等其他List，通过SubList内部类生成的List，其所有的方法(get,add,set,remove等)都是在`原始列表上操作`的，它`自身并没有生成一个数组或是链表`，也就是子列表只是原列表的一个`视图(View)`,所有的修改都`反映在原列表`上。</font>**










