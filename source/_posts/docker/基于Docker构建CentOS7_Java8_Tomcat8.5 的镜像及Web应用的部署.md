---
title: 基于Docker构建CentOS7_Java8_Tomcat8.5 的镜像及Web应用的部署
tags:
  - Docker
  - 容器
categories:
  - Docker
toc: true
recommend: 1
keywords: docker
uniqueId: '2021-07-27 13:01:57/基于Docker构建CentOS7_Java8_Tomcat8.5 的镜像及Web应用的部署.html'
mathJax: false
date: 2021-07-27 21:01:57
thumbnail:
---
我所渴求的无非是将心中脱颖欲出的本性付诸生活。为什么竟如此艰难呢？  ----黑塞

<!-- more -->
## 写在前面
***
+ 公司项目去`Oracle`用`teleDB`，都在容器上部署，所以系统的学习下，之前着急忙慌的。
+ 本博客笔记属于`实战类`，适用于小白
+ 博文有错误的地方，或者关于博文的问题，可以留言讨论。
+ 生活加油！


**<font color="009688"> 我所渴求的无非是将心中脱颖欲出的本性付诸生活。为什么竟如此艰难呢？  ----黑塞**</font>
 ***

## 安装 Docker

### Win 10 安装 Docker

+ 具体步骤可以参考[*菜鸟教程*](https://www.runoob.com/docker/windows-docker-install.html)

+ 因为`Docker`是运行在`Linux`上的所以需要安装虚拟机,win10 默认有` Hyper-V`，不需要安装，还需要安装一个`Linux`内核，可以参考[适用于 Linux 的 Windows 子系统安装指南 (Windows 10)](https://docs.microsoft.com/zh-cn/windows/wsl/install-win10#manual-installation-steps)
##### 关于 WSL 简单了解下
>WSL允许您<font color=purple>直接在Windows上运行Linux环境(包括命令行工具和应用程序)，而不需要传统虚拟机或双引导设置的开销</font>。WSL特别有助于web开发人员以及使用Bash和linux优先工具(例如。Ruby, Python)在Windows上使用它们的工具链，并确保开发环境和生产环境之间的一致性。当您在Windows上安装Linux版本时，您将获得一个完整的Linux环境。WsL1与Windows是隔离的- Ul是终端，你可以安装工具。在不修改或中断Windows安装的情况下，将语言和编译器导入Linux环境。我们推荐使用WsL2。

完成上述步骤之后，可以启动`docker`：

![](https://img-blog.csdnimg.cn/5b05a3dd0faf4d418f4c33b44dab2e11.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)


```bash
##这是一个基本docker教程
git clone https://github.com/docker/getting-started.git

cd  getting-started
docker build -t docker101tutorial .
```
之后启动点击`restart ` 完成Dokcer启动

![\[外链图片转存失败,源站可能有防盗链机制,建议将图片保存下来直接上传(img-3Ak86kxZ-1627389782504)(imgclip_1.png "imgclip_1.png")\]](https://img-blog.csdnimg.cn/3abb5f281fc64ba2b2e81fd3204e5d1d.png)


```bash
PS C:\Users\lenovo> docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS
NAMES
PS C:\Users\lenovo> docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
```
![](https://img-blog.csdnimg.cn/194aa5f42e3546fa86dd8ac8477e2d5f.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

***

### Linux 安装 Docker


##### 安装条件：

+ 需要64位操作系统,至少 RHEL6.5 以上的版本，强烈推荐 RHEL7
+ docker安装时，内核要求在3.0以上，RHEL7的内核默认在3.0以上，不满足可以单独升级系统内核。`uname -r  `或者`cat /proc/version`
+ 关闭防火墙 (不是必须):firewalld【RHEL7使用】，Iptables【RHEL6使用】,Docker安装时，会自动的接管防火墙，并向防火墙里添加配置，如果防火墙存在，会产生冲突。
##### 安装步骤：

+ 卸载防火墙 `yum remove -y firewalld-*`
+ 安装软件包 `yum install docker`
+ 开启路由转发 `/etc/sysctl.conf net.ipv4.ip_forward=1` 使用`sysctl -p`让配置立刻生效(否则需要重新虚拟机)docker是通过虚拟交互机来进行通讯的，需要开启路由转发的功能。

![](https://img-blog.csdnimg.cn/9d862620730544b99d421f349a1a6234.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)


`软件的 BUG` : `iptables -nL FORWARD` 版本 大于 1.12 时会设置 `FORWARD` 的默认规则，被设置为 `DROP`,对于有些docker的版本中，`FORWARD`链的规则被设置成了`DROP`，会造成容器和宿主机之间无法通讯。
![在这里插入图片描述](https://img-blog.csdnimg.cn/62570588d6b045289982c76bd16d2935.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

**<font color=blue>解决办法：`修改 /lib/systemd/system/docker.server`</font>**

![在这里插入图片描述](https://img-blog.csdnimg.cn/80ed4476e8634761b8a6cb96611eff2b.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

**<font color=chocolate>重载配置文件，重启服务 </font>**
```bash
systemctl daemon-reload 
systemctl restart docker
```

![](https://img-blog.csdnimg.cn/785106d687024717bb3f3ac4b749384d.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

## 基于Docker构建CentOS_7&JDK_1.8&Tomcat8.5 的镜像。

#### 使用 <font color=plum>commit</font>构建基础环境镜像：拉取一个centOS_7&jdk_1.8的镜像，基于该镜像启动容器后装一个tomcat8.5，另存为一个新镜像。

```bash

PS E:\docker> docker pull docker.io/mamohr/centos-java
Using default tag: latest
latest: Pulling from mamohr/centos-java
469cfcc7a4b3: Pull complete
6c9a0d503960: Pull complete
Digest: sha256:11988ca920fe0a3f3dd382b62690d993e843b668b1fb7f31142494f1e7b5d136
Status: Downloaded newer image for mamohr/centos-java:latest
docker.io/mamohr/centos-java:latest
PS E:\docker> docker images
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
centos               latest              300e315adb2f        7 months ago        209MB
mamohr/centos-java   latest              e041132b8b32        3 years ago         577MB
```
+ **根据镜像启动容器**

```bash
docker run -it --privileged mamohr/centos-java:latest /usr/sbin/init
# 进入容器
docker exec -it f6209e004f2f /bin/bash
```
+ **下载一个tomcat**

```bash
[root@f6209e004f2f /]# cd /tmp/;wget https://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.69/bin/apache-tomcat-8.5.69.tar.gz
--2021-07-27 02:52:02--  https://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.69/bin/apache-tomcat-8.5.69.tar.gz
Resolving mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)... 101.6.8.193, 101.6.8.193, 2402:f000:1:408:8100::1, ...
Connecting to mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)|101.6.8.193|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 10561246 (10M) [application/octet-stream]
Saving to: 'apache-tomcat-8.5.69.tar.gz'

100%[==================================================================================>] 10,561,246  7.44MB/s   in 1.4s

2021-07-27 02:52:07 (7.44 MB/s) - 'apache-tomcat-8.5.69.tar.gz' saved [10561246/10561246]

[root@f6209e004f2f tmp]# tar -zxvf apache-tomcat-8.5.69.tar.gz
```

```bash
[root@f6209e004f2f tmp]# ls
apache-tomcat-8.5.69  apache-tomcat-8.5.69.tar.gz  hsperfdata_root  ks-script-hE5IPf  yum.log
[root@f6209e004f2f tmp]# mkdir /usr/tomcat8.5 ; mv apache-tomcat-8.5.69 /usr/tomcat8.5
[root@f6209e004f2f tmp]# cd /usr/tomcat8.5/
[root@f6209e004f2f tomcat8.5]# ls
apache-tomcat-8.5.69
[root@f6209e004f2f tomcat8.5]# chown -hR tomcat:tomcat apache-tomcat-8.5.69
chown: invalid user: 'tomcat:tomcat'
#create tomcat user and group
[root@f6209e004f2f tomcat8.5]# groupadd tomcat;useradd -g tomcat -s /sbin/nologin tomcat
[root@f6209e004f2f tomcat8.5]# chown -hR tomcat:tomcat apache-tomcat-8.5.69
```
+ **添加tomcat自启动systemd服务单元文件**

```java
vim /lib/systemd/system/tomcat.service
#=====
[Unit]
Description=Apache Tomcat 8.5
After=syslog.target network.target

[Service]
Type=forking
User=tomcat
Group=tomcat

Environment=JAVA_HOME=/usr/java/latest/jre
## tomcat as link -s
Environment=CATALINA_PID=/usr/tomcat8.5/apache-tomcat-8.5.69/temp/tomcat.pid
Environment=CATALINA_HOME=/usr/tomcat8.5/apache-tomcat-8.5.69
Environment=CATALINA_BASE=/usr/tomcat8.5/apache-tomcat-8.5.69
Environment='CATALINA_OPTS=-Xms512M -Xmx4096M -server -XX:+UseParallelGC'
Environment='CATALINA_OPTS=-Dfile.encoding=UTF-8 -server -Xms2048m -Xmx2048m -Xmn1024m -XX:SurvivorRatio=10 -XX:MaxTenuringThreshold=15 -XX:NewRatio=2 -XX:+DisableExplicitGC'
Environment='JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom'

ExecStart=/usr/tomcat8.5/apache-tomcat-8.5.69/bin/startup.sh
ExecStop=/bin/kill -15 $MAINPID
Restart=on-failure

[Install]
WantedBy=multi-user.target
                                                                                                                                                                               
```

+ **加载systemd服务单元配置,并启动测试。**

```java
[root@cdd43dfcdf88 system]# systemctl daemon-reload
[root@cdd43dfcdf88 system]# systemctl start tomcat
[root@cdd43dfcdf88 system]#
[root@cdd43dfcdf88 system]# systemctl enable tomcat
Created symlink from /etc/systemd/system/multi-user.target.wants/tomcat.service to /usr/lib/systemd/system/tomcat.service.
[root@cdd43dfcdf88 system]#
[root@cdd43dfcdf88 system]# systemctl status tomcat
● tomcat.service - Apache Tomcat 8.5
   Loaded: loaded (/usr/lib/systemd/system/tomcat.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-07-27 08:24:19 UTC; 18s ago
  Process: 1179 ExecStart=/usr/tomcat8.5/apache-tomcat-8.5.69/bin/startup.sh (code=exited, status=0/SUCCESS)
 Main PID: 1186 (java)
   CGroup: /docker/cdd43dfcdf8828b963c7b761ba2301fbd14643d99a44232badefa0a641fde60b/system.slice/tomcat.service
           └─1186 /usr/java/latest/jre/bin/java -Djava.util.logging.config.file=/usr/tomcat8.5/apache-tomcat-8.5.69/conf/logging.properties -Djava.util....
           ‣ 1186 /usr/java/latest/jre/bin/java -Djava.util.logging.config.file=/usr/tomcat8.5/apache-tomcat-8.5.69/conf/logging.properties -Djava.util....

Jul 27 08:24:18 cdd43dfcdf88 systemd[1]: Starting Apache Tomcat 8.5...
Jul 27 08:24:19 cdd43dfcdf88 systemd[1]: Started Apache Tomcat 8.5.

[root@cdd43dfcdf88 system]# curl http://127.0.0.1:8080



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <title>Apache Tomcat/8.5.69</title>
        <link href="favicon.ico" rel="icon" type="image/x-icon" />
        <link href="tomcat.css" rel="stylesheet" type="text/css" />
    </head>
.。。。。
```

+ **<font color=red>tomcat 沒有起來问题排查,一般为加载脚本的路径错误，或者说是设计到读写的权限问题。</font>**

```bash

[root@cdd43dfcdf88 bin]# systemctl start tomcat
Job for tomcat.service failed because the control process exited with error code. See "systemctl status tomcat.service" and "journalctl -xe" for details.
## 日志消息写到文本里，在文本里排查问题： cat log | grep tomcat
journalctl -xe >> log
```


+ **<font color=purple>生成新的镜像</font>**

```bash
[root@f6209e004f2f /]# yum clean all
Loaded plugins: fastestmirror, ovl
Cleaning repos: base extras updates
Cleaning up everything
Maybe you want: rm -rf /var/cache/yum, to also free up space taken by orphaned data from disabled or removed repos
Cleaning up list of fastest mirrors
[root@f6209e004f2f /]# exit
exit
PS E:\docker> docker  commit cdd43dfcdf88   centos7_java8_tomcat8.5:latest
sha256:b8c89e810729ae3f9870bac9273f883f9f0bb5ca88ffec7b84f88b64451d805b
PS E:\docker> docker images
REPOSITORY                             TAG                 IMAGE ID            CREATED             SIZE
centos7_java8_tomcat8.5                latest              b8c89e810729        7 seconds ago       700MB
centos7_java8_tomcat8.5_base           latest              e9e848ab97f6        2 hours ago         679MB
centos                                 latest              300e315adb2f        7 months ago        209MB
mamohr/centos-java                     latest              e041132b8b32        3 years ago         577MB
hub.c.163.com/housan993/centos7_jdk8   latest              0f793ba5281b        3 years ago
```

+ <font color=red>**使用docker run 来验新的容器**</font>

```java
docker> docker run -it --privileged mamohr/centos-java:latest /usr/sbin/init

```
![](https://img-blog.csdnimg.cn/a333633be01f4e79ba3aa9dbf41e12e2.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

+ **备份做好的镜像**

```bash
PS E:\docker> docker images
REPOSITORY                             TAG                 IMAGE ID            CREATED             SIZE
centos7_java8_tomcat8.5                latest              b8c89e810729        39 minutes ago      700MB
centos7_java8_tomcat8.5_base           latest              e9e848ab97f6        3 hours ago         679MB
centos                                 latest              300e315adb2f        7 months ago        209MB
mamohr/centos-java                     latest              e041132b8b32        3 years ago         577MB
hub.c.163.com/housan993/centos7_jdk8   latest              0f793ba5281b        3 years ago         356MB
PS E:\docker> docker save centos7_java8_tomcat8.5:latest -o  centos7_java8_tomcat8.5.tar
```
## **通过 <font color=purple>Dockerfile</font> 创建服务镜像**

这里的话，我们用之前的一個镜像為基础镜像。发布一个web应用。如果有使用docker客户端工具，那么每次可以远程直接替换，war实现部署， UAWeb.war 是我们自己的应用war包。

+ **<font color=plum>编写 `Dockerfile `文件</font>**：

```js
FROM centos7_java8_tomcat8.5:latest

MAINTAINER LIRUILONG

COPY UAWeb.war   /usr/tomcat8.5/apache-tomcat-8.5.69/webapps/UAWeb.war

EXPOSE 8080 

```
+ **<font color=amber>创建新的镜像</font>**
```bash
docker build -t uag_image .
```
![](https://img-blog.csdnimg.cn/1a39b2cf8fa54a2eaffd265859043ce3.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)


+ <font color=orange>因为容器的特殊性，所以数据没办法持久。所以一般会通过映射宿主机的方式。</font>

```java
# 给一下权限
chmod 777 /usr/tomcat8.5/apache-tomcat-8.5.69/logs
docker run -it -v  /logs:/usr/tomcat8.5/apache-tomcat-8.5.69/logs --privileged uag_image:latest  /usr/sbin/init

```

+ **<font color=purple>发布docker服务，同样需要和宿主机做端口映射。</font>**
```java
docker run -it -v /logs:/usr/tomcat8.5/apache-tomcat-8.5.69/logs  -p 9090:8080  --privileged uag_image:latest  /usr/sbin/init
```
***

关于Docker的一些基本命令，小伙伴不熟悉的可以移步到我的笔记[Docker、Podman 容器“扫盲“ 学习笔记
](https://blog.csdn.net/sanhewuyang/article/details/114435587)


