---
title: '如何搭建自己的中文Git版本库:Gitlab容器化部署'
tags:
  - Gitlab
  - Git
categories:
  - Git
toc: true
recommend: 1
keywords: Git
uniqueId: '2021-11-08 11:22:57/"如何搭建自己的中文Git版本库:Gitlab容器化部署".html'
mathJax: false
date: 2021-11-08 19:22:57
thumbnail:
---

**<font color="009688"> 我深怕自己本非美玉，故而不敢加以刻苦琢磨，却又半信自己是块美玉，故有不肯庸庸碌碌，与瓦砾为伍。于是我渐渐地脱离凡尘，疏远世人，结果便是一任愤懑与悔恨日益助长内心那怯弱的自尊心。其实任何人都是驯兽师，而那野兽，无非就是各人的性情而已。           -----中岛敦《山月记》**</font>
<!-- more -->
## 写在前面
***
有时候可能做项目组长，负责一个项目开发，但是工作是内网，也没有公司的版本库权限，那这个时候，我们怎么用处理版本控制，可以用集中式的版本库工具`SVN`，或者分布式的`Git`，这里和小伙伴分享如何搭建自己的`Git中文版本库`。

`前提条件`： 一台可以连接外网的`Linux`或者` windows`机器，当然可以是虚机或者`ESC`之类。或者只要可以装`Docker`的机器就可以。


**<font color="009688"> 我深怕自己本非美玉，故而不敢加以刻苦琢磨，却又半信自己是块美玉，故有不肯庸庸碌碌，与瓦砾为伍。于是我渐渐地脱离凡尘，疏远世人，结果便是一任愤懑与悔恨日益助长内心那怯弱的自尊心。其实任何人都是驯兽师，而那野兽，无非就是各人的性情而已。           -----中岛敦《山月记》**</font>
 ***


>服务器： liruilongs.github.io：192.168.26.55

## <font color=tomato>一、docker 环境安装</font>
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ yum -y install docker-ce
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl enable docker --now
```
**<font color=brown>配置docker加速器</font>**
```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://2tefyfv7.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```


## <font color=tomato>二、安装GitLab</font>

### <font color=green>1.安装GitLab 并配置</font>
**<font color=green>拉取镜像</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker pull beginor/gitlab-ce
```
|--|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/933c165cc3464588956f6af222cf0250.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

### <font color=plum>2.创建共享卷目录</font>
**<font color=amber>创建共享卷目录，用于持久化必要的数据和更改相关配置</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ mkdir -p /data/gitlab/etc/ /data/gitlab/log /data/gitlab/data
┌──[root@liruilongs.github.io]-[~]
└─$ chmod 777 /data/gitlab/etc/ /data/gitlab/log /data/gitlab/data
```
### <font color=brown>3.创建 Gitlab 容器</font>
**<font color=royalblue>这里的访问端口一定要要设置成80，要不push项目会提示没有报错，如果宿主机端口被占用，需要把这个端口腾出来</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker run -itd --name=gitlab --restart=always --privileged=true   -p 8443:443  -p 80:80 -p 222:22 -v  /data/gitlab/etc:/etc/gitlab -v  /data/gitlab/log:/var/log/gitlab -v  /data/gitlab/data:/var/opt/gitlab  beginor/gitlab-ce
acc95b2896e8475915275d5eb77c7e63f63c31536432b68508f2f216d4fec634
┌──[root@liruilongs.github.io]-[~]
└─$ docker ps
CONTAINER ID   IMAGE               COMMAND             CREATED          STATUS                             PORTS                                                                                                             NAMES
acc95b2896e8   beginor/gitlab-ce   "/assets/wrapper"   53 seconds ago   Up 51 seconds (health: starting)   0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:222->22/tcp, :::222->22/tcp, 0.0.0.0:8443->443/tcp, :::8443->443/tcp   gitlab
┌──[root@liruilongs.github.io]-[~]
└─$
┌──[root@liruilongs.github.io]-[~]
└─$# 
```

### <font color=plum>4.关闭容器修改相关配置文件</font>
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker stop gitlab
gitlab
```
**<font color=royalblue>external_url    'http://192.168.26.55'</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep external_url
##! For more details on configuring external_url see:
# external_url 'GENERATED_EXTERNAL_URL'
# registry_external_url 'https://registry.gitlab.example.com'
# pages_external_url "http://pages.example.com/"
# gitlab_pages['artifacts_server_url'] = nil # Defaults to external_url + '/api/v4'
# mattermost_external_url 'http://mattermost.example.com'
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/external_url 'GENERATED_EXTERNAL_URL'/a external_url\t'http://192.168.26.55' "  /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep external_url
##! For more details on configuring external_url see:
# external_url 'GENERATED_EXTERNAL_URL'
external_url    'http://192.168.26.55'
# registry_external_url 'https://registry.gitlab.example.com'
# pages_external_url "http://pages.example.com/"
# gitlab_pages['artifacts_server_url'] = nil # Defaults to external_url + '/api/v4'
# mattermost_external_url 'http://mattermost.example.com'
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=orange>gitlab_rails['gitlab_ssh_host'] = '192.168.26.55'</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_ssh_host
# gitlab_rails['gitlab_ssh_host'] = 'ssh.host_example.com'
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/gitlab_ssh_host/a gitlab_rails['gitlab_ssh_host'] = '192.168.26.55' "  /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~] 
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_ssh_host
# gitlab_rails['gitlab_ssh_host'] = 'ssh.host_example.com'
gitlab_rails['gitlab_ssh_host'] = '192.168.26.55'
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=blue>gitlab_rails[gitlab_shell_ssh_port] = 222</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_shell_ssh
# gitlab_rails['gitlab_shell_ssh_port'] = 22
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/gitlab_shell_ssh_port/a gitlab_rails['gitlab_shell_ssh_port'] = 222" /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_shell_ssh
# gitlab_rails['gitlab_shell_ssh_port'] = 22
gitlab_rails[gitlab_shell_ssh_port] = 222
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=purple>/data/gitlab/data/gitlab-rails/etc/gitlab.yml</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vim /data/gitlab/data/gitlab-rails/etc/gitlab.yml
┌──[root@liruilongs.github.io]-[~]
└─$
##############################
 gitlab:
    ## Web server settings (note: host is the FQDN, do not include http://)
    host: 192.168.26.55
    port: 80
    https: false
```

|--|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/8b98142d3a1c4f54b2096ce7af378324.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
**<font color=yellowgreen>修改完配置文件之后。直接启动容器</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker start gitlab
```
#### <font color=green>5.访问测试</font>
|访问测试|
|--|
|**<font color=red>在宿主机所在的物理机访问，`http://192.168.26.55/` ，会自动跳转到修改密码(root用户),如果密码设置的没有满足一定的复杂性，则会报500，需要从新设置</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/2f11a407499c4f73bb01c3d513d345bb.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/c05f68349fb14087b8c023b2c43c1c71.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=seagreen>登录进入仪表盘</font>**|


## <font color=brown>三、新建项目，push代码测试</font>

|新建一个项目，push代码测试|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/50da96a334234626969052cb4c5c35ec.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/7203aa8028e64fc2be35d0c536d0a3bd.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/2959b0672f8a4a31a59a0da249832281.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=yellowgreen>然后我们简单测试一下，push一个项目上去</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/9963328c77f54e618169e9a55acea031.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=orange>项目成功上传Gitlab</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/9bad889ac0124c41b6704cbc7ee3bd19.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/d5edeb74446c491eae6f687538d6b73a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|



```bash
PS F:\blogger> git init
Initialized empty Git repository in F:/blogger/.git/
PS F:\blogger> git config --global user.name "Administrator"
PS F:\blogger> git config --global user.email "admin@example.com"
PS F:\blogger> git remote add origin http://192.168.26.55/root/blog.git
PS F:\blogger> git add .
PS F:\blogger> git commit -m "Initial commit"
PS F:\blogger> git push -u origin master
Enumerating objects: 322, done.
Counting objects: 100% (322/322), done.
Delta compression using up to 8 threads
Compressing objects: 100% (302/302), done.
Writing objects: 100% (322/322), 11.31 MiB | 9.22 MiB/s, done.
Total 322 (delta 24), reused 0 (delta 0)
remote: Resolving deltas: 100% (24/24), done.
To http://192.168.26.55/root/blog.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
PS F:\blogger>
```

>时间原因，关于 【搭建自己的中文Git版本库】先分享到这里。生活加油  ^ _ ^