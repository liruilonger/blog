---
title: containerd客户端工具nerdctl的使用
tags:
  - containerd
  - nerdctl
categories:
  - 容器
toc: true
recommend: 1
keywords: containerd
uniqueId: '2021-10-11 11:33:07/containerd客户端工具nerdctl的使用.html'
mathJax: false
date: 2021-10-11 19:33:07
thumbnail:
---
> 摘要
首页显示摘要内容(替换成自己的)
<!-- more -->
## 写在前面
***




**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font><font color="#C0C4CC">[这里写一句鼓励自己的话,可以加一个颜色(可选)]</font>
 ***

1.19 版以前的Kubernetes 需要通过一个名为 Dockershim 的模块连接到 Docker，然后由Docker 连接到 Containerd 来创建容器。从技术上来看，实际的容器运行时是 Containerd，而不是 Docker。Docker 的作用只不过是在 Containerd 上创建容器而已。作为人类用户，只需运行一个 Docker run 就可以创建一个容器，这一点非常方便；然而在方便的同时，Docker 也带来了许多无用的操作和技术债务，对于 Kubernetes 而言，这就是负担。Kubernetes 完全可以绕过Docker，自己在 Containerd 上创建容器，从而获得同样的效果。而Kubernetes 1.20 中就采用了这种做法。

kubernetes 发布 v1.20 时宣布将弃用 docker，并且在 2021 年下半年发布的 v1.23 版本中，彻底移除 dockershim 代码，意味着那时 kubernetes 支持的容器运行时不再包括 docker，那么我们使用 containerd 作为 runtime。

```bash
[root@vms102 ~]# yum search containerd
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
=============================================== N/S matched: containerd ================================================
containerd.x86_64 : An industry-standard container runtime
containerd.io.x86_64 : An industry-standard container runtime

  Name and summary matches only, use "search all" for everything.
[root@vms102 ~]# yum -y  install containerd.io cri-tools
```
docker 命令大家非常熟悉且好用，但 containerd 的客户端工具 ctr 及 crictl 却是极其难用，给
大家带来了诸多不变。本文主要讲 containerd 全新的一个客户端工具 nerdctl 的使用。


