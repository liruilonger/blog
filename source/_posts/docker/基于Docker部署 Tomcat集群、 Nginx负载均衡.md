---
title: 基于Docker部署 Tomcat集群、 Nginx负载均衡
tags:
  - Nginx
  - Docker
categories:
  - Docker
toc: true
recommend: 1
keywords: Docker
uniqueId: '2021-08-30 11:24:44/基于Docker部署 Tomcat集群、 Nginx负载均衡.html'
mathJax: false
date: 2021-08-30 19:24:44
thumbnail:
---
**<font color="009688">当作一百世一样。这里的道理很明白:我思故我在,既然我存在,就不能装作不存在。无论如何,我要为自己负起责任。——王小波《三十而立》 </font>**
<!-- more -->
## 写在前面
***
+ 看完`Dokcer`相关的书籍，正好有个项目要这样搞，所以自己练习一下。
+ 博文主要介绍了` Tomcat 集群+ Ngixn 负载的Docker环境部署`。
+ 两种实现方式，一种是通过`宿主机桥接`方式，一种是通过 `内部网络Docker network` 的方式 
+ Demo相关镜像以上传[**<font color=blue>DockerHub</font>**](https://hub.docker.com/):
```bash
docker pull liruilong/nginx_log
docker pull liruilong/tomcat8
```

**<font color="009688">当作一百世一样。这里的道理很明白:我思故我在,既然我存在,就不能装作不存在。无论如何,我要为自己负起责任。——王小波《三十而立》 </font>**
***

## 结构图：

![在这里插入图片描述](https://img-blog.csdnimg.cn/f4216b8e981348b6a356433868954046.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

这里仅作为一种学习，一般这种负载的话，`Nginx`是放到`主机侧`的， `JavaWeb(Tomcat) `应用放到容器里。

+ 效果

![在这里插入图片描述](https://img-blog.csdnimg.cn/59dc4e45949f47849e922ac2fa40820d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

新建文件夹。

```bash
 D=uag;mkdir $D;cd $D;mkdir uag_nginx uag_tomcat8;
 ls
 uag_nginx  uag_tomcat8
```
## <font color=brown>一，Ngixn 镜像制作</font>
```bash
cd uag_nginx/
# 用于存放配置文件
mkdir nginx
vim Dockerfile
```
#### <font color=amber>Dockerfile 文件内容</font>
```bash
FROM nginx
LABEL maintainer="uag"
ENV REFRESHED_AT 2021-08-27

EXPOSE 8099
```
####  <font color=plum>构建nginx配置文件内容</font>
这个的配置文件，在容器运行的时候通过 `-v `参数与 容器内部共享。方便后期参数更改
```bash
cd ./nginx
vim nginx.conf
```
#### <font color=orange>nginx.conf 配置文件内容</font>
```bash
user  nginx;
worker_processes  auto;

error_log  /var/log/nginx/error.log notice;
pid        /var/run/nginx.pid;
daemon off;

events {
    worker_connections  1024;
}


http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$upstream_addr - $remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;

    include /etc/nginx/conf.d/*.conf;

    server {
        listen          8099;
        server_name     localhost;

        root            /var/www/html/;
        index           index.html index.htm;

        access_log      /var/log/nginx/default_access.log main;
        error_log       /var/log/nginx/default_error.log;

        
        location / {
                proxy_pass http://backend;
        }

        location ~ .* {
                        proxy_pass http://backend;
                        proxy_set_header Host $http_host;
                        proxy_set_header X-Real-IP $remote_addr;
                        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                }

        

}
    # 这里配置负载 
    upstream backend {
    server 172.23.231.190:8069;
    server 172.23.231.190:8079;
    server 172.23.231.190:8089;
}
}

```
#### <font color=green>配置负载:`172.23.231.190`为宿主机IP,8069,8079,8089为对应的Java Web 暴露的应用端口。</font>
```bash
 # 这里配置负载 
    upstream backend {
    server 172.23.231.190:8069;
    server 172.23.231.190:8079;
    server 172.23.231.190:8089;
}

```
#### <font color=seagreen>构建Nginx镜像</font>
```
docker build -t uag/uag_nginx .
```

## <font color=royalblue>二，java Web(Tomcat)应用镜像构建</font>
```bash
cd uag_tomcat8/

vim Dockerfile
```
#### <font color=orange>Dockerfile 文件内容</font>
```bash
FROM dordoka/tomcat
MAINTAINER LIRUILONG

COPY UAWeb.war   /opt/tomcat/webapps/UAWeb.war

EXPOSE 8080

ENTRYPOINT [ "/opt/tomcat/bin/catalina.sh", "run" ]

```
#### <font color=seagreen>上传对应的War包</font>
```bash
 ls
Dockerfile  UAWeb.war
```
#### <font color=red>构建镜像</font>
````bash
 docker build -t uag/uag_tomcat .
````

## <font color=blue>三，运行容器</font>

### <font color=amber>Nginx镜像</font>
```bash
docker run -d -p 8099:8099 --name uag_nginx  -v $PWD/nginx/nginx.conf:/etc/nginx/nginx.conf   uag/uag_nginx nginx
```
###  <font color=yellowgreen>java Web(Tomcat)镜像</font>
```bash
 docker run -it -d -p 8089:8080 --name uag_app_1  uag/uag_tomcat
 docker run -it -d -p 8079:8080 --name uag_app_2  uag/uag_tomcat
 docker run -it -d -p 8069:8080 --name uag_app_3  uag/uag_tomcat
```

> **<font color=camel>查看运行的容器</font>**

![在这里插入图片描述](https://img-blog.csdnimg.cn/65bea4a441f941708e17e880e606090b.png)
+ **<font color=chocolate>浏览器访问</font>**

![在这里插入图片描述](https://img-blog.csdnimg.cn/7abfe460ea744c7c8a49f5da5dbc1496.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

> <font color=orange>查看负载方式:新进程的方式</font>

![在这里插入图片描述](https://img-blog.csdnimg.cn/4a085115caed46d3b5c869b3cfa30d02.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

> <font color=camel>查看负载方式:--volumes-from 方式</font>

Dockerfile文件
```bash
FROM nginx
LABEL maintainer="uag"
ENV REFRESHED_AT 2021-08-27


VOLUME  /var/log/nginx/
EXPOSE 80

```
```bash
┌──(liruilong㉿Liruilong)-[/mnt/e/docker/uag/uag_nginx]
└─$ docker run  -it --rm --volumes-from nginx_log  centos cat /var/log/nginx/default_access.log
172.23.231.190:8069 - 172.17.0.1 - - [30/Aug/2021:12:55:02 +0000] "GET /UAWeb/services/listServices HTTP/1.1" 200 12660 "http://127.0.0.1:8099/UAWeb/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"

172.23.231.190:8079 - 172.17.0.1 - - [30/Aug/2021:12:55:02 +0000] "GET /UAWeb/axis2-web/css/axis-style.css HTTP/1.1" 200 1587 "http://127.0.0.1:8099/UAWeb/services/listServices" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.23.231.190:8069 - 172.17.0.1 - - [30/Aug/2021:12:55:02 +0000] "GET /UAWeb/axis2-web/images/asf-logo.gif HTTP/1.1" 200 5866 "http://127.0.0.1:8099/UAWeb/services/listServices" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.23.231.190:8079 - 172.17.0.1 - - [30/Aug/2021:12:55:02 +0000] "GET /UAWeb/axis2-web/images/axis_l.jpg HTTP/1.1" 200 12340 "http://127.0.0.1:8099/UAWeb/services/listServices" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.23.231.190:8089 - 172.17.0.1 - - [30/Aug/2021:12:55:03 +0000] "GET /UAWeb/services/listServices HTTP/1.1" 200 12660 "http://127.0.0.1:8099/UAWeb/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.23.231.190:8069 - 172.17.0.1 - - [30/Aug/2021:12:55:03 +0000] "GET /UAWeb/axis2-web/images/asf-logo.gif HTTP/1.1" 200 5866 "http://127.0.0.1:8099/UAWeb/services/listServices" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92
```

## <font color=blue>四，构建好镜像上传仓库：</font>

![在这里插入图片描述](https://img-blog.csdnimg.cn/d8bed770269343e99d6b770916110aa3.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

嗯，需要注册一个`Docker Hub账号`，然后登录，需要镜像前面加 `账户名/` 

```bash
┌──(liruilong㉿Liruilong)-[/mnt/e/docker/uag/uag_nginx]
└─$ docker push liruilong/nginx_log
The push refers to repository [docker.io/liruilong/nginx_log]
An image does not exist locally with the tag: liruilong/nginx_log

┌──(liruilong㉿Liruilong)-[/mnt/e/docker/uag/uag_nginx]
└─$ docker tag 9c9af0362eb9  liruilong/nginx_log

┌──(liruilong㉿Liruilong)-[/mnt/e/docker/uag/uag_nginx]
└─$ docker push liruilong/nginx_log
The push refers to repository [docker.io/liruilong/nginx_log]
fb04ab8effa8: Pushed
8f736d52032f: Pushed
009f1d338b57: Pushed
678bbd796838: Pushed
d1279c519351: Pushed
f68ef921efae: Pushed
latest: digest: sha256:2af7e8aeab84e8a816caf6b0342e1a45f95c7089ff52578040ea3a4c28a943c7 size: 1570

┌──(liruilong㉿Liruilong)-[/mnt/e/docker/uag/uag_nginx]
└─$  docker pull liruilong/nginx_log  # 拉去镜像

```
![在这里插入图片描述](https://img-blog.csdnimg.cn/11cb1d8570b84bad9f7ff6d35970dca8.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)


## 五，<font color=blue>扩展</font>

上面的部署方式，在宿主机一样可以通过端口访问到应用。考虑安全问题，我希望不能通过宿主机访问到应用，要求只能通过 Nginx 反向代理到上游应用服务器，即tomcat的容器端口不映射给宿主机。那这个这么处理。

我们先来分析一下之前的，之前我们是通过桥接模式进行网络互通，即Ngixn每次负载的时候都要通过宿主机访问Tomcat应用服务器，现在我们要`直接通过Nginx到Tomcat`。这里我们要用到docker 1.9 的新特性 `network` ，对于1.9 之前，我们可以使用` link`来实现容器之间的网络互通。

#### <font color=red>`network `方式隔离业务容器</font>


**<font color=tomato>需要我们改一下Nginx的配置文件</font>**
```bash

    upstream backend {
    server uag_app_3:8080;
    server uag_app_2:8080;
    server uag_app_1:8080;
    }
```

**<font color=orange>解释一下这里的写法：使用 network 之后，由于这四个容器是在同一个网络内部启动的，即位于相同的网段,彼此可以`ping`通,同时`Docker`将会感知到所有在这个网络下运行的容器,并且通过`/etc/hosts`文件将这些容器的地址保存到`本地DNS`中(这个映射可能看不到，但是实际可以ping通的)。我们就直接可以在容器中使用过。可以直接通过 `容器名` 或者 `容器名.网络名` (`uag_app_3`,`uag_app_3.uag_net`) 通过 DNS 解析获取到 IP。</font>**

**<font color=yellowgreen>这样，我们在`配置Nginx负载`的时候，就直接可以通过 `容器名：端口` 的方式进行负载，这样的我们即`隔离业务容器`，同时，不需要`维护 IP和端口，这个DNS注册是自动的`,如果任何一个`容器重启了,那么它们的IP地址信息则会自动在/etc/hosts文件中更新`。也就是说,对底层容器的修改并不会对我们的应用程序正常工作产生影响。</font>**

看下结果：
![在这里插入图片描述](https://img-blog.csdnimg.cn/bdedb4f06505403cae8624adfc010b4d.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

**<font color=amber>这里我们写了一个小脚本，用来演示：</font>**
```bash
#/bin/bash

# 删除启动容器
docker  rm -f uag_nginx  uag_app_2 uag_app_1 uag_app_3


#定义局域网络名字
N=NetWorkUag

if docker network ls | grep $N
then
  echo "网络已存在"
else
  docker network create $N
fi
docker run -it -d  --net $N --name uag_app_1 -h uag_app_1 uag/uag_tomcat
docker run -it -d  --net $N --name uag_app_2 -h uag_app_2 uag/uag_tomcat
docker run -it -d  --net $N --name uag_app_3 -h uag_app_2 uag/uag_tomcat
docker run -d -p 5678:8099 --name uag_nginx --net $N  -v $PWD/nginx/nginx.conf:/etc/nginx/nginx.conf    nginx_log nginx
docker ps
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/9a75e267afec492390dd21b518c9f54a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

>**<font color=blue>负载情况</font>**
```bash

┌──(liruilong㉿Liruilong)-[/mnt/e/docker/uag/uag_tomcat8]
└─$ docker exec -it  uag_nginx /bin/bash
root@0584832ed231:/# cat /var/log/nginx/default_access.log
172.20.0.4:8080 - 172.20.0.1 - - [02/Sep/2021:13:25:19 +0000] "GET /UAWeb/ HTTP/1.1" 200 3496 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.20.0.3:8080 - 172.20.0.1 - - [02/Sep/2021:13:25:36 +0000] "GET /UAWeb/ HTTP/1.1" 499 0 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.20.0.4:8080 - 172.20.0.1 - - [02/Sep/2021:13:25:36 +0000] "GET /UAWeb/ HTTP/1.1" 200 3496 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.20.0.3:8080 - 172.20.0.1 - - [02/Sep/2021:13:25:37 +0000] "GET /UAWeb/ HTTP/1.1" 200 3496 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.20.0.2:8080 - 172.20.0.1 - - [02/Sep/2021:13:25:38 +0000] "GET /UAWeb/ HTTP/1.1" 200 3496 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
172.20.0.4:8080 - 172.20.0.1 - - [02/Sep/2021:13:25:40 +0000] "GET /UAWeb/services/listServices HTTP/1.1" 200 12653 "http://127.0.0.1:5678/UAWeb/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36" "-"
root@0584832ed231:/#
```