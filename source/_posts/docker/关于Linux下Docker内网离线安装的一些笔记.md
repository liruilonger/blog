---
title: 关于Linux下Docker内网离线安装的一些笔记
tags:
  - Docker
categories:
  - Docker
toc: true
recommend: 1
keywords: Docker
uniqueId: '2022-12-25 04:44:03/关于Linux下Docker内网离线安装的一些笔记.html'
mathJax: false
date: 2022-12-25 12:44:03
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 博文为内网离线安装 docker 教程
+ 安装前提是你需要有一台最小化安装的  Linux 机器,可以是虚机
+ 如果不是最小化，可能需要手动下载一些依赖
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***

### 离线安装 Docker

需要找一台有网的 Linux 机器，把涉及的 rpm 依赖包导出来。
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$yum -y install docker-ce --downloadonly --downloaddir=/root/soft
┌──[root@vms152.liruilongs.github.io]-[~]
└─$cd soft/
┌──[root@vms152.liruilongs.github.io]-[~/soft]
└─$ls *.rpm
audit-libs-python-2.8.5-4.el7.x86_64.rpm              fuse3-libs-3.6.1-4.el7.x86_64.rpm
checkpolicy-2.5-8.el7.x86_64.rpm                      fuse-overlayfs-0.7.2-6.el7_8.x86_64.rpm
containerd.io-1.6.14-3.1.el7.x86_64.rpm               libcgroup-0.41-21.el7.x86_64.rpm
container-selinux-2.119.2-1.911c772.el7_8.noarch.rpm  libsemanage-python-2.5-14.el7.x86_64.rpm
docker-ce-20.10.22-3.el7.x86_64.rpm                   policycoreutils-python-2.5-34.el7.x86_64.rpm
docker-ce-cli-20.10.22-3.el7.x86_64.rpm               python-IPy-0.75-6.el7.noarch.rpm
docker-ce-rootless-extras-20.10.22-3.el7.x86_64.rpm   setools-libs-3.3.8-4.el7.x86_64.rpm
docker-scan-plugin-0.23.0-3.el7.x86_64.rpm            slirp4netns-0.4.3-4.el7_8.x86_64.rpm
expect-5.45-14.el7_1.x86_64.rpm                       tcl-8.5.13-8.el7.x86_64.rpm
```
然后归档上传到你内网的机器

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tar -cvf docker.tar ./soft/
```
在内网机器解压，通过下面的命令安装，如果提示缺少依赖，可以到这个网站下载下来在上传 [https://rpmfind.net/linux/rpm2html/search.php](https://rpmfind.net/linux/rpm2html/search.php)

```bash
rpm -ivh --replacefiles  soft/*
```
如果提示依赖冲突，可以考虑使用 `rpm -ivh --force  soft/*`

安装完成做简单测试
```bash
docker -v
```

### 修改 Docker Docker 运行时根目录

注意: 由于 docker 的限制，如果在部署后再调整需要清理 docker 镜像和容器等后再重新配置并重启，建议在首次部署即考虑该项。Docker 对磁盘的占用较大，请预先设置 docker 的 root dir 为大磁盘路径 ，并重启查看是否生效

`Docker v17.05.0` 以下版本配置方法修改或新增文件 `/etc/docker.conf` 或者 `/etc/sysconfig/docker`，请根据当 docker版本自行判断 `--graph=/new/path/to/docker-data`

`Docker v17.05.0 ` 以上版本配置方法,修改 `/etc/docker/daemon.json` ,如果不存在则创建一个
```bash
mkdir /etc/docker;touch /etc/docker/daemon.json
```
json 中填写下面的内容
```json
{
"data-root": "/new/path/to/docker-data"
}
```

上面的 `/new/path/to/docker-data` 即是实际重新设置的 docker 工作目录，请按实际情况设置具体路径。修改后需重启 docker 生效
```bash
$sudo systemctl restart docker
```
如果需要开机自启：
```bash
$sudo systemctl enable docker
```
通过 `docker info` 可以查看配置信息
```bash
$docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.9.1-docker)
  scan: Docker Scan (Docker Inc., v0.23.0)

Server:
 Containers: 1
  Running: 1
  Paused: 0
  Stopped: 0
 Images: 1
 Server Version: 20.10.22
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: runc io.containerd.runc.v2 io.containerd.runtime.v1.linux
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 9ba4b250366a5ddde94bb7c9d1def331423aa323
 runc version: v1.1.4-0-g5fd4c4d
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
 Kernel Version: 3.10.0-957.el7.x86_64
 Operating System: CentOS Linux 7 (Core)
 OSType: linux
 Architecture: x86_64
 CPUs: 16
 Total Memory: 31.26GiB
 Name: ddzx_file
 ID: FHNQ:4QSZ:XHNS:6NTR:SJVX:O2OG:YG5Q:5MSI:ISKZ:4SJ2:VQGA:6R2O
 Docker Root Dir: /app/ali_asr/docker-data
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```

## 博文参考

***

