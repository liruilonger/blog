---
title: 使用容器搭建wrodpress博客
tags:
  - wrodpress
  - 个人博客搭建
categories:
  - 个人博客搭建
toc: true 
recommend: 1
keywords: wrodpress
uniqueId: '2021-10-05 03:36:07/使用容器搭建wrodpress博客.html'
mathJax: false
date: 2021-10-05 11:36:07
thumbnail:
---
**<font color="009688"> 生活的意义就是学着真实的活下去，生命的意义就是寻找生活的意义 -----山河已无恙**</font>
<!-- more -->
## 写在前面
***
+ 对于不喜欢使用`markdown`的小伙伴，个人感觉这个很不错，使用`富文本`的方式写博客，所以整理了下。
+ 非常方便，只需要一个`linux或者win系统`，然后安装一个`docker`就可以，如果希望外网可以访问，可以搞一个内网穿透。




**<font color="009688"> 生活的意义就是学着真实的活下去，生命的意义就是寻找生活的意义 -----山河已无恙**</font>

***

# <font color=seagreen>搭建命令</font>

## <font color=chocolate>容器环境安装</font>
```bash
yum -y install docker 
```
## <font color=brown>数据库容器运行</font>

```bash
┌──[root@liruilongs.github.io]-[~/docker]
└─$ docker run -dit --name=db --restart=always -v $PWD/db:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=liruilong -e WORDPRESS_DATABASE=wordpress hub.c.163.com/library/mysql
8605e77f8d50223f52619e6e349085566bc53a7e74470ac0a44340620f32abe8
┌──[root@liruilongs.github.io]-[~/docker]
└─$ docker ps
CONTAINER ID   IMAGE                         COMMAND                  CREATED         STATUS         PORTS      NAMES
8605e77f8d50   hub.c.163.com/library/mysql   "docker-entrypoint.s…"   6 seconds ago   Up 4 seconds   3306/tcp   db
```

## <font color=purple>博客平台
</font>
```bash
┌──[root@liruilongs.github.io]-[~/docker]
└─$ docker run -itd --name=blog --restart=always -v $PWD/blog:/var/www/html -p 80 -e WORDPRESS_DB_HOST=172.17.0.2 -e WORDPRESS_DB_USER=root -e WORDPRESS_DB_PASSWORD=liruilong -e WORDPRESS_DB_NAME=wordpress hub.c.163.com/library/wordpress
a90951cdac418db85e9dfd0e0890ec1590765c5770faf9893927a96ea93da9f5
┌──[root@liruilongs.github.io]-[~/docker]
└─$ docker ps
CONTAINER ID   IMAGE                             COMMAND                  CREATED         STATUS         PORTS                                     NAMES
a90951cdac41   hub.c.163.com/library/wordpress   "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds   0.0.0.0:49271->80/tcp, :::49271->80/tcp   blog
8605e77f8d50   hub.c.163.com/library/mysql       "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   3306/tcp                                  db
┌──[root@liruilongs.github.io]-[~/docker]
└─$
```

# <font color=royalblue>需要注意的问题</font>

+ 共享目录的权限问题，宿主机的目录权限要设置一下。
+ 容器运行参数问题，一般要写对了


# <font color=brown>展示</font>

|展示|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/56cf58eef9f2444f9156f01ba734669e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/02422a7ccaef43678b6e8f7bb0fe0953.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

# 另
**<font color=camel>也可以使用 容器连接的方式，这里使用的是默认值</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker run -dit --name=db --restart=always -v $PWD/db:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=liruil
ong -e WORDPRESS_DATABASE=wordpress hub.c.163.com/library/mysql
c4a88590cb21977fc68022501fde1912d0bb248dcccc970ad839d17420b8b08d
┌──[root@liruilongs.github.io]-[~]
└─$ docker run -dit --name blog --link=db:mysql -p 80:80 hub.c.163.com/library/wordpress
8a91caa1f9fef1575cc38788b0e8739b7260729193cf18b094509dcd661f544b
```
