---
title: 关于Linux挂载 fstab 和 systemd.mount 使用场景的一些笔记
tags:
  - Systemd
categories:
  - Systemd
toc: true
recommend: 1
keywords: Systemd
uniqueId: '2022-10-26 09:46:02/关于Linux挂载fstab 和systemd.mount的一些笔记整理.html'
mathJax: false
date: 2022-10-26 17:46:02
thumbnail:
---

**<font color="009688"> 不知归路，宁愿一世无悔追逐。——王小波**</font>

<!-- more -->
## 写在前面
***
+ 整理这方面的笔记
+ 在 `stackoverflow` 的`Unix & Linux` 社区看到相关的问题。
+ 有大佬做了解答，感觉问题不错，答案也不错，整理分享给小伙伴
+ 原问题地址：[tmp on tmpfs: fstab vs tmp.mount with systemd？ https://unix.stackexchange.com/questions/722496/tmp-on-tmpfs-fstab-vs-tmp-mount-with-systemd](https://unix.stackexchange.com/questions/722496/tmp-on-tmpfs-fstab-vs-tmp-mount-with-systemd)
+ 这里简单总结：
  + 通过配置挂载点 `/etc/fstab` 是我们管理挂载的首选方法。
  + 建议使用 `mount unit` 作为`工具`，即用于自动配置。类似我们起服务一样,做为一个`service unit` 和 二进制文件直接执行的方式
  + 两个挂载方式是冲突的，想要自动设置挂载的工具不应该尝试编辑`/etc/fstab`

**<font color="009688"> 不知归路，宁愿一世无悔追逐。——王小波**</font>

***

### 一些介绍

对于不熟悉的小伙伴这里简单介绍下：

`systemd.mount`  用于封装一个文件系统挂载点(也向后兼容传统的 /etc/fstab 文件)。 系统中所有的 `".mount"` 为后缀的单元文件， 封装了一个由 systemd 管理的文件系统挂载点。类似Service unit 一样，可以配置自动挂载
```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$systemctl list-unit-files -t mount
UNIT FILE                     STATE
........
tmp.mount                     disabled
var-lib-nfs-rpc_pipefs.mount  static
```

就拿 `tmp.mount`来讲， 通过 `systemctl cat tmp.mount` 可以查看 mount 的单元文件`/usr/lib/systemd/system/tmp.mount`，

可以看到挂载信息，当前的文件系统类型为 `Type=tmpfs`, 挂载位置为 `Where=/tmp` 系统临时文件夹，挂载的存储设备为 `What=tmpfs` 基于内存的存储
```bash 
┌──[root@vms82.liruilongs.github.io]-[~]
└─$systemctl cat tmp.mount
[Unit]
Description=Temporary Directory
Documentation=man:hier(7)
Documentation=http://www.freedesktop.org/wiki/Software/systemd/APIFileSystems
ConditionPathIsSymbolicLink=!/tmp
DefaultDependencies=no
Conflicts=umount.target
Before=local-fs.target umount.target
After=swap.target

[Mount]
What=tmpfs
Where=/tmp
Type=tmpfs
Options=mode=1777,strictatime

# Make 'systemctl enable tmp.mount' work:
[Install]
WantedBy=local-fs.target
```

`systemctl status tmp.mount` 命令可以查看状态

```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$systemctl status tmp.mount
● tmp.mount - Temporary Directory
   Loaded: loaded (/usr/lib/systemd/system/tmp.mount; disabled; vendor preset: disabled)
   Active: inactive (dead)
    Where: /tmp
     What: tmpfs
     Docs: man:hier(7)
           http://www.freedesktop.org/wiki/Software/systemd/APIFileSystems
┌──[root@vms82.liruilongs.github.io]-[~]
└─$
```
但是传统的挂载方式我们一般是使用 `mount` 或者直接写到`/etc/fstab`文件下。类似下面这样

```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$cat /etc/fstab
UUID=9875fa5e-2eea-4fcc-a83e-5528c7d0f6a5 /                       xfs     defaults        0 0
```

然后来看下这个问题

### 原问题

To have `/tmp` on tmpfs, I know I can use an entry in `/etc/fstab`, but I do not understand the role of `/etc/default/tmpfs` [mentioned][1] sometimes, and in what case I need to create or modify it.

Recently, I often see suggested to use systemd `tmp.mount` confuguration. For example, [on Debian][2]:


为了在tmpfs上有`/tmp`，我知道我可以使用`/etc/fstab`中的条目，但我不明白`/etc/default/tmpfs`的作用[提到][1]，在什么情况下我需要创建或修改它。


```bash 
$ sudo cp /usr/share/systemd/tmp.mount /etc/systemd/system/
$ sudo systemctl enable tmp.mount
```


Which of the two methods is more appropriate for everyday use? In what situations one is better than the other? When do I need to deal with `/etc/default/tmpfs`?


[1]: https://manpages.debian.org/bullseye/initscripts/tmpfs.5.en.html
[2]: https://wiki.debian.org/SSDOptimization/#Reduction_of_SSD_write_frequency_via_RAMDISK

这两种方法哪一种更适合日常使用？在什么情况下一种比另一种更好？

***

### 答案

On some systems, `/tmp` is a `tmpfs` by default, and this is the configuration [provided by systemd’s “API File Systems”][1]. Fedora-based systems follow this pattern to various extents; Fedora itself ships `/usr/lib/systemd/system/tmp.mount` and enables it, but RHEL 8 ships it without enabling it. On such systems, masking and unmasking the unit is the appropriate way of disabling or enabling a `tmpfs` `/tmp`, [as documented in the API File Systems documentation][2].

在某些系统上，`/tmp `  默认是 `tmpfs`，这是 `systemd`的 `API文件系统`提供的配置。基于 `Fedora` 的系统在不同程度上遵循这种模式。; `Fedora` 本身发布了 `/usr/lib/systemd/system/tmp.mount` 并启用它，但是 `RHEL 8` 没有启用它。在这样的系统上，屏蔽和解密单元是禁用或启用 `tmpfs /tmp` 的适当方法，如API文件系统文档中所述。

这里的 `API文件系统` 即指：[https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems/](https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems/)

`APIFileSystems` : Linux 内核为用户空间与它通信提供了多种不同的方式。许多设施都有系统调用，其他的隐藏在 Netlink 接口之后，甚至还有一些通过虚拟文件系统(例如/proc或/sys. 这些文件系统是编程接口，它们实际上并没有真正的持久存储支持。他们只是使用内核的文件系统接口作为各种不相关机制的接口。类似地，`用户空间将一些文件系统用于其自身的 API 目的`，用于存储共享内存段、共享临时文件或套接字。

`Fedora`: 是由Fedora项目社区开发、红帽公司赞助，目标是创建一套新颖、多功能并且自由(开放源代码)的操作系统。Fedora是商业化的Red Hat Enterprise Linux发行版的上游源码。

***

Other systems such as Debian [don’t ship `tmp.mount` in a directly-usable location][3]; this is why you need to copy it to `/etc/systemd/system` if you want to use it. This has the unfortunate side-effect of creating a full override of `tmp.mount` in `/etc`, which means that if the `systemd` package ships a different version of `tmp.mount` in `/lib/systemd/system` in the future, it will be ignored. On such systems I would recommend using `/etc/fstab` instead.

其他系统，如`Debian`，并没有将 `tmp.mount` 放在可直接使用的位置，因此如果你想使用它，需要将其复制到 `/etc/systemd/system` 。这样做的副作用是在 `/etc`中创建了一个完全覆盖的`tmp.mount`，这意味着如果将来 `systemd `软件包在 `/lib/systemd/system` 中提供不同版本的 `tmp.mount`，它将被忽略。在这样的系统中，我建议使用 `/etc/fstab`代替

这里被忽略是因为单元文件的优先级问题，优先级从高到底
+ 本地配置的系统单元: /etc/systemd/system
+ 运行时配置的系统单元: /run/systemd/system
+ 软件包安装的系统单元: /usr/lib/systemd/system

***

In both setups, `/etc/fstab` is still the recommended way of [customising `/tmp` mounts][4], *e.g.* to change their size; [`man systemd.mount`][5] says


在这两种设置中，`/etc/fstab` 仍然是自定义`/tmp`挂载的推荐方式，例如改变其大小；`man systemd.mount`

> In general, configuring mount points through `/etc/fstab` is the preferred approach to manage mounts for humans.

> 通常，通过配置挂载点 `/etc/fstab` 是为人类管理挂载的首选方法。

and [the API File Systems documentation concurs][2]. 
[API文件系统文档也推荐这种][2]。


Using mount units is [recommended for tooling][5], *i.e.* for automated configuration: 
建议使用  mount units  作为工具，即用于自动配置。

> For tooling, writing mount units should be preferred over editing `/etc/fstab`.
> 对于工具来说，编写挂载单元应该比编辑`/etc/fstab`更合适。


(This means that tools which want to automatically set up a mount shouldn’t try to edit `/etc/fstab`, which is error-prone, but should instead install a mount unit, which can be done atomically and can also be overridden by a system administrator using systemd features.)

(这意味着想要自动设置挂载的工具不应该尝试编辑`/etc/fstab`，这很容易出错，而是应该安装一个 `mount unit`，这可以原子化地完成，也可以由系统管理员使用systemd功能覆盖。)

`/etc/default/tmpfs` is used by Debian’s `sysvinit`, so it’s irrelevant with systemd.
`/etc/default/tmpfs` 被 `Debian` 的 `sysvinit` 使用，所以它与 `systemd` 没有关系。

  [1]: https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems/
  [2]: https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems/#whyareyoutellingmeallthisijustwanttogetridofthetmpfsbackedtmp
  [3]: https://bugs.debian.org/783509
  [4]: https://unix.stackexchange.com/q/352042/86440
  [5]: https://www.freedesktop.org/software/systemd/man/systemd.mount.html#fstab



## 博文引用资源
***

[tmp on tmpfs: fstab vs tmp.mount with systemd: https://unix.stackexchange.com/questions/722496/tmp-on-tmpfs-fstab-vs-tmp-mount-with-systemd](https://unix.stackexchange.com/questions/722496/tmp-on-tmpfs-fstab-vs-tmp-mount-with-systemd)

[API File Systems :https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems/](https://www.freedesktop.org/wiki/Software/systemd/APIFileSystems/)


