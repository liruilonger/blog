---
title: 关于Linux 网络抓包的一些笔记整理
tags:
  - 抓包
categories:
  - 抓包
toc: true
recommend: 1
keywords: tcpdump
uniqueId: '2022-12-19 1:32:09/关于Linux 网络抓包的一些笔记整理.html'
mathJax: false
date: 2022-12-20 00:32:09
thumbnail:
---

**<font color="009688"> 这世界的存在完全只是就它对一个其他事物的，一个进行 "表象者" 的关系来说的，这个进行 "表象者" 就是人自己    -----《作为意志和表象的世界》(第一篇 世界作为表象初论)**</font>

<!-- more -->
## 写在前面
***
+ 遇到一个 `ping 单通` 的情况，需要抓包分析下，所以整理这部分笔记
+ 博文内容涉及： 
  + `HTTP/TCP` 抓包分析 Demo
  + `ICMP` 抓包分析 Demo
  + `Nginx` 抓包分析用户名密码 Demo
+ 理解不足小伙伴帮忙指正

**<font color="009688"> 这世界的存在完全只是就它对一个其他事物的，一个进行 "表象者" 的关系来说的，这个进行 "表象者" 就是人自己    -----《作为意志和表象的世界》(第一篇 世界作为表象初论)**</font>

***

## tcpdump 简单介绍

在 Linux 中常常使用 `tcpdump` 网络抓包工具来进行抓包，排查一些网络问题,需要安装的工具包

```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$rpm -ql tcpdump || yum -y install tcpdump
```

tcpdump 抓包命令基本用法 `tcpdump [选项] [过滤条件]`

常见监控选项
+ `-i` 指定监控的`网络接口`,指定抓取某一个网卡接口的流量；不使用`-i` 指定网卡，`tcpdump` 会默认抓取第一块网卡的流量；`-i any` 监控抓取的是所有网卡的流量；
+ `-A` 转换为ACSII码，以方便阅读
+ `-w` 将数据包信息保存到指定文件
+ `-r` 从指定文件读取数据包信息
+ `-c` 定义抓包个数 (默认会一直抓取下去)
+ `-nn` 不解析域名

tcpdump的过滤条件，按Ctrl + C 键停止抓包
+ 类型：`host、net、port 80、portrange 600-800`
+ 方向：`src(从指定IP接收)、dst(发送到指定ip)`
+ `协议`：tcp、udp、ip、wlan、arp、......
+ `多个条件组合`：and、or、not


## 实战

### HTTP/TCP 请求抓包分析

使用 python2 的  `SimpleHTTPServer` 模块启动一个 简单的 HTTP 服务
```bash
[root@vms152 ~]# jobs
[1]+  运行中               coproc COPROC python -m SimpleHTTPServer 8080 &
```

发出HTTP请求

```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$curl 192.168.29.152:8080
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
。。。。。
```
抓包命令 `tcpdump -i ens32  -A -nn -w /root/web.cap  host 192.168.29.152 and port 8080`

抓取 `ens32`网卡，主机为 `192.168.29.152` 端口为 `8080` 的所有出站入站的包。抓包写入 `/root/web.cap`
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tcpdump -i ens32  -A -nn -w /root/web.cap  host 192.168.29.152 and port 8080
tcpdump: listening on ens32, link-type EN10MB (Ethernet), capture size 262144 bytes
^C11 packets captured
11 packets received by filter
0 packets dropped by kernel
```
抓取到 `host 192.168.29.152 and port 8080` 的所有包，这里数据包包括出站和入站的所有包，没有指定协议即，即包括所有

`tcpdump` 的数据包文件是是乱码，Linux 下需要通过 `-r` 命令来读取 `tcpdump -A -r /root/web.cap `
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tcpdump -A -r /root/web.cap
reading from file /root/web.cap, link-type EN10MB (Ethernet)
03:24:14.285274 IP vms152.liruilongs.github.io.41204 > 192.168.29.152.webcache: Flags [S], seq 317480493, win 29200, options [mss 1460,sackOK,TS val 270896233 ecr 0,nop,wscale 7], length 0
E..<.D@.@.................^-......r............
.%.i........
03:24:14.286103 IP 192.168.29.152.webcache > vms152.liruilongs.github.io.41204: Flags [S.], seq 601235906, ack 317480494, win 64240, options [mss 1460], length 0
E..,9.....HJ............#.!...^.`...lm........
03:24:14.286137 IP vms152.liruilongs.github.io.41204 > 192.168.29.152.webcache: Flags [.], ack 1, win 29200, length 0
E..(.E@.@..     ..............^.#.!.P.r.....
03:24:14.286282 IP vms152.liruilongs.github.io.41204 > 192.168.29.152.webcache: Flags [P.], seq 1:84, ack 1, win 29200, length 83: HTTP: GET / HTTP/1.1
E..{.F@.@.................^.#.!.P.r.....GET / HTTP/1.1
User-Agent: curl/7.29.0
Host: 192.168.29.152:8080
Accept: */*


03:24:14.286480 IP 192.168.29.152.webcache > vms152.liruilongs.github.io.41204: Flags [.], ack 84, win 64240, length 0
E..(9.....HM............#.!...^.P.............
03:24:14.287219 IP 192.168.29.152.webcache > vms152.liruilongs.github.io.41204: Flags [P.], seq 1:18, ack 84, win 64240, length 17: HTTP: HTTP/1.0 200 OK
E..99.....H;............#.!...^.P.......HTTP/1.0 200 OK

03:24:14.287228 IP vms152.liruilongs.github.io.41204 > 192.168.29.152.webcache: Flags [.], ack 18, win 29200, length 0
E..(.G@.@.................^.#.!.P.r.....
03:24:14.287349 IP 192.168.29.152.webcache > vms152.liruilongs.github.io.41204: Flags [FP.], seq 18:913, ack 84, win 64240, length 895: HTTP
E...9.....D.............#.!...^.P...@...Server: SimpleHTTP/0.6 Python/2.7.5
Date: Mon, 19 Dec 2022 19:24:14 GMT
Content-type: text/html; charset=UTF-8
Content-Length: 758

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
。。。。。。。
</body>
</html>

03:24:14.287355 IP vms152.liruilongs.github.io.41204 > 192.168.29.152.webcache: Flags [.], ack 914, win 30430, length 0
E..(.H@.@.................^.#.%TP.v.....
03:24:14.287560 IP vms152.liruilongs.github.io.41204 > 192.168.29.152.webcache: Flags [F.], seq 84, ack 914, win 30430, length 0
E..(.I@.@.................^.#.%TP.v.....
03:24:14.287692 IP 192.168.29.152.webcache > vms152.liruilongs.github.io.41204: Flags [.], ack 85, win 64239, length 0
E..(9.....HJ............#.%T..^.P....F........
```
通过上面的抓包数据，可以看到一次 完整的 TCP 的握手挥手。上面的数据直接看不太方便，所以需要一些工具来分析包内数据，可以通过 `wireshark` 分析抓取的数据包，导出文件用 `wireshark` 打开

![在这里插入图片描述](https://img-blog.csdnimg.cn/76e5a9b1e7fb4063b525ff99a7f919b6.png)

可以看到，前三个包为一次 TCP 握手建立连接，第四个包为 建立 TCP 连接后的客户端发出 HTTP 请求(这个时候也有一次 ACK )，之后的三次为 数据传输的过程，然后是 HTTP 响应(FIN/ACK),四次挥手的，这里看到三次，是因为在 HTTP 协议返回的时候，携带了第一次挥手的报文，在抓包的时候我们没有指定系协议，所以看到三次。

![在这里插入图片描述](https://img-blog.csdnimg.cn/c9f9c2b02cdc4b349f50d0423c0f76da.png)

指定协议抓包可以更清楚的看到三次握手，四次挥手
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tcpdump -i ens32  -A -nn host 192.168.29.152 and port 8080 and tcp
```

同时可以通过抓包获取传输的数据

![在这里插入图片描述](https://img-blog.csdnimg.cn/80738a5f2e204382bfd1c1d73ab2eec8.png)

### ICMP(ping) 请求抓包分析

有的时候，网络不通，我可以通过 抓包的方式来确认是出站的问题，还是入站的问题


通过 `192.168.29.152 ` 机器 去 `ping` 机器 ` 192.168.26.152` ，网络不通，一直超时，说明包进去了，但是没有回来
```bash
[root@vms152 ~]# ip a | grep gl
    inet 192.168.29.152/24 brd 192.168.29.255 scope global ens32
[root@vms152 ~]# ping -i 0.01 -c 3 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.

--- 192.168.26.152 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 70ms

[root@vms152 ~]#
```

但是通过 ` 192.168.26.152` 去 `ping` 机器 `192.168.29.152` 确是通的，想确认是那里问题
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ip a | grep gl
    inet 192.168.26.152/24 brd 192.168.26.255 scope global ens32
┌──[root@vms152.liruilongs.github.io]-[~]
└─$ping -i 0.01 -c 3 192.168.29.152
PING 192.168.29.152 (192.168.29.152) 56(84) bytes of data.
64 bytes from 192.168.29.152: icmp_seq=1 ttl=128 time=0.907 ms
64 bytes from 192.168.29.152: icmp_seq=2 ttl=128 time=1.03 ms
64 bytes from 192.168.29.152: icmp_seq=3 ttl=128 time=0.991 ms

--- 192.168.29.152 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 20ms
rtt min/avg/max/mdev = 0.907/0.977/1.033/0.052 ms
┌──[root@vms152.liruilongs.github.io]-[~]
└─$
```

`29.152` 和 `26.152` 不同网段通信，需要路由实现，当前我们通过 Linux 的软路由功能实现,下面为软路由的机器，为了方便，这里称为代理机器或者网关机器。
```bash
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$ip a | grep gl
    inet 192.168.29.154/24 brd 192.168.29.255 scope global ens32
    inet 192.168.26.154/24 brd 192.168.26.255 scope global ens33
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$
```

在 ` 26.152 ` 对 `29.152 ` 进行抓包
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tcpdump -i ens32  -A -nn  host 192.168.29.152 and icmp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
```
同时在 `29.152` 发起对 `26.152` 的 `ping` 请求
```bash
[root@vms152 ~]# ping -i 0.01 -c 3 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.

--- 192.168.26.152 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 37ms

[root@vms152 ~]#
```
可以看到，可以收到`29.152`的 ping 的请求 `ICMP echo request`，并且做出了响应 `ICMP echo reply`
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$tcpdump -i ens32  -A -nn  host 192.168.29.152 and icmp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on ens32, link-type EN10MB (Ethernet), capture size 262144 bytes
04:35:24.595447 IP 192.168.29.152 > 192.168.26.152: ICMP echo request, id 13245, seq 1, length 64
E..TL.@.?.5...........~.3......c.....$...................... !"#$%&'()*+,-./01234567
04:35:24.595485 IP 192.168.26.152 > 192.168.29.152: ICMP echo reply, id 13245, seq 1, length 64
E..T2...@...............3......c.....$...................... !"#$%&'()*+,-./01234567
04:35:24.607313 IP 192.168.29.152 > 192.168.26.152: ICMP echo request, id 13245, seq 2, length 64
E..TL.@.?.5.............3......c.....R...................... !"#$%&'()*+,-./01234567
04:35:24.607344 IP 192.168.26.152 > 192.168.29.152: ICMP echo reply, id 13245, seq 2, length 64
E..T2...@...............3......c.....R...................... !"#$%&'()*+,-./01234567
04:35:24.635735 IP 192.168.29.152 > 192.168.26.152: ICMP echo request, id 13245, seq 3, length 64
E..TL.@.?.5o............3......c....~....................... !"#$%&'()*+,-./01234567
04:35:24.635783 IP 192.168.26.152 > 192.168.29.152: ICMP echo reply, id 13245, seq 3, length 64
E..T2&..@...............3......c....~....................... !"#$%&'()*+,-./01234567
```
说明问题不在这里，是其他机器的问题, 可以出现在软路由的机器上面,这次在 软路由的机器上面 抓包分析

```bash
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$tcpdump -i ens32  -A -nn  host 192.168.29.152 and icmp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on ens32, link-type EN10MB (Ethernet), capture size 262144 bytes
04:42:23.905630 IP 192.168.29.152 > 192.168.26.152: ICMP echo request, id 13246, seq 1, length 64
E..TY.@.@.(............P3......c....#....................... !"#$%&'()*+,-./01234567
04:42:23.925388 IP 192.168.29.152 > 192.168.26.152: ICMP echo request, id 13246, seq 2, length 64
E..TY#@.@.(.............3......c.....9...................... !"#$%&'()*+,-./01234567
04:42:23.950773 IP 192.168.29.152 > 192.168.26.152: ICMP echo request, id 13246, seq 3, length 64
E..TY&@.@.(...........;.3......c....s....................... !"#$%&'()*+,-./01234567
^C
3 packets captured
3 packets received by filter
0 packets dropped by kernel
```
可以看到，在`ens32` 网卡，只有 ping 请求的包 出现 `ICMP echo request` ,没有响应的包出现。即问题出现在这里，`29.152` ping 发送的包,确实到了 `26.152`，并且确实从 `26.152`返回了包，但是返回之后找不到回家的路，不知道对应的IP在那里。

不同网段互通需要路由器，这里 `26.152` 的网关配置为 `26.2` ,但是 `26.2` 不是作为路由器的机器，所以没有跨网段的能力，也没有对应的路由控制表，所以 `26.152` 的网关需要配置为 `26.154` 。

```bash
[root@vms152 ~]# ping -i 0.01 -c 4 192.168.26.152
PING 192.168.26.152 (192.168.26.152) 56(84) bytes of data.
64 bytes from 192.168.26.152: icmp_seq=1 ttl=63 time=1.13 ms
64 bytes from 192.168.26.152: icmp_seq=2 ttl=63 time=0.875 ms
64 bytes from 192.168.26.152: icmp_seq=3 ttl=63 time=0.854 ms
64 bytes from 192.168.26.152: icmp_seq=4 ttl=63 time=4.00 ms

--- 192.168.26.152 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 31ms
rtt min/avg/max/mdev = 0.854/1.715/4.001/1.324 ms
[root@vms152 ~]#
```
同样抓包可以确认
```bash
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$tcpdump -i ens32  -A -nn host 192.168.29.152 and icmp
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on ens32, link-type EN10MB (Ethernet), capture size 262144 bytes
22:37:01.732511 IP 192.168.29.152 > 192.168.26.152: ICMP echo request, id 13818, seq 31, length 64
E..T..@.@.............h.5......c....e/...................... !"#$%&'()*+,-./01234567
22:37:01.733040 IP 192.168.26.152 > 192.168.29.152: ICMP echo reply, id 13818, seq 31, length 64
E..T;...?..x..........p.5......c....e/...................... !"#$%&'()*+,-./01234567
22:37:02.917124 IP 192.168.29.152 > 192.168.26.152: ICMP echo request, id 13818, seq 32, length 64
E..T.V@.@...............5.. ...c............................ !"#$%&'()*+,-./01234567
22:37:02.917591 IP 192.168.26.152 > 192.168.29.152: ICMP echo reply, id 13818, seq 32, length 64
E..T?...?..-............5.. ...c............................ !"#$%&'()*+,-./01234567
^C
4 packets captured
4 packets received by filter
0 packets dropped by kernel
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$
```
ping 单通除了防火墙之外、网关是一个需要注意的地方。

### Nginx 请求抓包分析用户名密码

安装 nginx ,配置nginx的用户认证
```bash
┌──[root@vms152.liruilongs.github.io]-[~]
└─$rpm -ql nginx || yum -y install  nginx
```
备份修改配置文件
```bash
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$cp /etc/nginx/nginx.conf  /etc/nginx/nginx.conf.bak
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$vim /etc/nginx/nginx.conf
```
认证需要 在配置文件 `server` 模块下面添加对应的配置
```bash
server {
        ...........
        auth_basic "auth-liruilong";
        auth_basic_user_file /etc/nginx/pass;
```
安装压测工具，http-tools可以创建访问网站的用户名和密码
```bash
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$yum -y install  httpd-tools
......
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$htpasswd -c /etc/nginx/pass liruilong
New password:
Re-type new password:
Adding password for user liruilong
```
启动服务
```bash
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$systemctl start  nginx
┌──[root@vms.154.liruilongs.github.io]-[~]
└─$systemctl is-active nginx
active
```
访问抓包

![在这里插入图片描述](https://img-blog.csdnimg.cn/d51099b341524ef7974e7498c2e1efda.png)

```bash
┌──[root@vms.154.liruilongs.github.io]-[/var/log/nginx]
└─$tcpdump -i ens33 -A -w /root/web.cap host 192.168.26.1 and port 80
tcpdump: listening on ens33, link-type EN10MB (Ethernet), capture size 262144 bytes

^C67 packets captured
68 packets received by filter
0 packets dropped by kernel
```
通过抓包解析登录用户名密码
```bash
┌──[root@vms.154.liruilongs.github.io]-[/var/log/nginx]
└─$tcpdump -A -r /root/web.cap | grep Basic
reading from file /root/web.cap, link-type EN10MB (Ethernet)
Authorization: Basic bGlydWlsb25nOmxpcnVpbG9uZw==
Authorization: Basic bGlydWlsb25nOmxpcnVpbG9uZw==
Authorization: Basic bGlydWlsb25nOmxpcnVpbG9uZw==
Authorization: Basic bGlydWlsb25nOmxpcnVpbG9uZw==
┌──[root@vms.154.liruilongs.github.io]-[/var/log/nginx]
└─$echo "bGlydWlsb25nOmxpcnVpbG9uZw==" | base64 -d \n
liruilong:liruilong
┌──[root@vms.154.liruilongs.github.io]-[/var/log/nginx]
└─$
```

关于 `tcpdump` 抓包 就和小伙伴分享到这里，生活加油 `^_^`


