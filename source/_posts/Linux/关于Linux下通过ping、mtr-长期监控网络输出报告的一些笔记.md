---
title: 关于Linux下通过ping、mtr 长期监控网络输出日志报告的一些笔记
tags:
  - 网络测试
categories:
  - 网络测试
toc: true
recommend: 1
keywords: Mtr
uniqueId: '2022-12-24 08:31:45/关于Linux下通过ping、mtr 长期监控网络输出报告的一些笔记.html'
mathJax: false
date: 2022-12-24 16:31:45
thumbnail:
---

**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 分享一些 通过 `ping、mtr` 长期监控网络输出报告的笔记
+ 博文内容涉及
+ 通过`systemd-run` 启动一个临时的 `ping Service`
+ 通过`systemd-run` 启动一个临时的 `mtr time & mtr Srevice` 
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 我所渴求的，無非是將心中脫穎語出的本性付諸生活，為何竟如此艱難呢 ------赫尔曼·黑塞《德米安》**</font>

***
关于 `ping` 和 `mtr` 的作用使用这里不多介绍，下面提供的实现方式前提是系统通过  `Systemd` 来引导进程。

###  ping  


对于 ping 来讲可以 通过 `systemd-run` 来生成临时 `Service` ，然后通过  `journalctl` 获取命令

```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$systemd-run --unit ping-print --slice ping  /usr/bin/ping  192.168.29.154
Running as unit ping-print.service.
┌──[root@vms82.liruilongs.github.io]-[~]
└─$systemctl is-active ping-print.service
active
```
直接查看 ping 日志
```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$journalctl -u ping-print | tail -n 5
12月 24 16:36:54 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=87 ttl=128 time=0.782 ms
12月 24 16:36:55 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=88 ttl=128 time=0.596 ms
12月 24 16:36:56 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=89 ttl=128 time=0.713 ms
12月 24 16:36:57 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=90 ttl=128 time=0.801 ms
12月 24 16:36:58 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=91 ttl=128 time=0.678 ms
┌──[root@vms82.liruilongs.github.io]-[~]
└─$date
2022年 12月 24日 星期六 16:37:11 CST
┌──[root@vms82.liruilongs.github.io]-[~]
└─$
```
导出 ping 日志
```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$journalctl -u ping-print.service  > ping.192.168.29.154.log
┌──[root@vms82.liruilongs.github.io]-[~]
└─$ls -hl ping.192.168.29.154.log;tail -n 3 ping.192.168.29.154.log
-rw-r--r-- 1 root root 46K 12月 24 16:41 ping.192.168.29.154.log
12月 24 16:41:45 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=378 ttl=128 time=5.52 ms
12月 24 16:41:46 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=379 ttl=128 time=0.841 ms
12月 24 16:41:47 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=380 ttl=128 time=0.787 ms
┌──[root@vms82.liruilongs.github.io]-[~]
└─$
```
退出当前登录 bash，可以发现进程任然存在，不会随着bash进程消失而消失
```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$exit
登出
Connection to 192.168.26.82 closed.
PS C:\Users\山河已无恙> ssh root@192.168.26.82
root@192.168.26.82''s password:
Last login: Sat Dec 24 13:30:20 2022 from 192.168.26.1
┌──[root@vms82.liruilongs.github.io]-[~]
└─$systemctl is-active ping-print.service
active
┌──[root@vms82.liruilongs.github.io]-[~]
└─$journalctl -u ping-print | tail -n 1
12月 24 16:43:55 vms82.liruilongs.github.io ping[97218]: 64 bytes from 192.168.29.154: icmp_seq=507 ttl=128 time=0.633 ms
┌──[root@vms82.liruilongs.github.io]-[~]
└─$date
2022年 12月 24日 星期六 16:43:57 CST
┌──[root@vms82.liruilongs.github.io]-[~]
└─$
```

测试完成删除  ping 进程 
```bash
┌──[root@vms82.liruilongs.github.io]-[~]
└─$pgrep ping
97218
┌──[root@vms82.liruilongs.github.io]-[~]
└─$systemctl stop ping-print.service
┌──[root@vms82.liruilongs.github.io]-[~]
└─$pgrep ping
┌──[root@vms82.liruilongs.github.io]-[~]
└─$
```
相比于下面的方式，上面的方式更加方便管理
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$nohup /usr/bin/ping  192.168.29.154 >> 192.168.29.154.log 2>&1  &
[2] 53139
```

### Mtr

对于 mtr 来讲，我们通过配置定时输出报告的方式，启动 临时 time 每分钟执行一次 `mtr` 命令输出一次报告，同样可以通过 `journalctl` 来管理应用。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$systemd-run --on-calendar=*:*:00  --unit mtr-print-log --slice mtr /usr/sbin/mtr -r -b  192.168.29.154
Running timer as unit mtr-print-log.timer.
Will run service as unit mtr-print-log.service.
```

查看 time 状态
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$systemctl status mtr-print-log.service
● mtr-print-log.service - /usr/sbin/mtr -r -b 192.168.29.154
   Loaded: loaded (/run/systemd/system/mtr-print-log.service; static; vendor preset: disabled)
  Drop-In: /run/systemd/system/mtr-print-log.service.d
           └─50-Description.conf, 50-ExecStart.conf, 50-Slice.conf
   Active: active (running) since 六 2022-12-24 22:07:00 CST; 6s ago
 Main PID: 15427 (mtr)
   CGroup: /mtr.slice/mtr-print-log.service
           └─15427 /usr/sbin/mtr -r -b 192.168.29.154

12月 24 22:07:00 vms81.liruilongs.github.io systemd[1]: Started /usr/sbin/mtr -r -b 192.168.29.154.
```
以及下次执行时间
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$systemctl list-timers mtr-print-log.timer
NEXT                         LEFT     LAST                         PASSED  UNIT                ACTIVATES
六 2022-12-24 22:08:00 CST  23s left 六 2022-12-24 22:07:00 CST  36s ago mtr-print-log.timer mtr-print-log.service

1 timers listed.
Pass --all to see loaded but inactive timers, too.
┌──[root@vms81.liruilongs.github.io]-[~]
└─$journalctl -u mtr-print-log.service
```
查看 mtr 的日志，你可以看到他的执行日志，输出的报告，同时你可以选择合适的方式导出
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$journalctl -u  mtr-print-log.service  | tail -n 10
12月 24 22:13:00 vms81.liruilongs.github.io systemd[1]: Started /usr/sbin/mtr -r -b 192.168.29.154.
12月 24 22:13:14 vms81.liruilongs.github.io mtr[21252]: Start: Sat Dec 24 22:13:00 2022
12月 24 22:13:14 vms81.liruilongs.github.io mtr[21252]: HOST: vms81.liruilongs.github.io  Loss%   Snt   Last   Avg  Best  Wrst StDev
12月 24 22:13:14 vms81.liruilongs.github.io mtr[21252]: 1.|-- gateway (192.168.26.2)     0.0%    10    0.2   0.3   0.2   0.5   0.0
12月 24 22:13:14 vms81.liruilongs.github.io mtr[21252]: 2.|-- 192.168.29.154             0.0%    10    0.8   0.8   0.7   1.1   0.0
12月 24 22:14:00 vms81.liruilongs.github.io systemd[1]: Started /usr/sbin/mtr -r -b 192.168.29.154.
12月 24 22:14:14 vms81.liruilongs.github.io mtr[22215]: Start: Sat Dec 24 22:14:00 2022
12月 24 22:14:14 vms81.liruilongs.github.io mtr[22215]: HOST: vms81.liruilongs.github.io  Loss%   Snt   Last   Avg  Best  Wrst StDev
12月 24 22:14:14 vms81.liruilongs.github.io mtr[22215]: 1.|-- gateway (192.168.26.2)     0.0%    10    0.3   0.3   0.2   0.4   0.0
12月 24 22:14:14 vms81.liruilongs.github.io mtr[22215]: 2.|-- 192.168.29.154             0.0%    10    0.9   0.8   0.7   1.0   0.0
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
如果只希望看到输出和执行时间，那么可以这样。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$journalctl -u  mtr-print-log.service -o cat | tail -n 10
Started /usr/sbin/mtr -r -b 192.168.29.154.
Start: Sat Dec 24 22:13:00 2022
HOST: vms81.liruilongs.github.io  Loss%   Snt   Last   Avg  Best  Wrst StDev
1.|-- gateway (192.168.26.2)     0.0%    10    0.2   0.3   0.2   0.5   0.0
2.|-- 192.168.29.154             0.0%    10    0.8   0.8   0.7   1.1   0.0
Started /usr/sbin/mtr -r -b 192.168.29.154.
Start: Sat Dec 24 22:14:00 2022
HOST: vms81.liruilongs.github.io  Loss%   Snt   Last   Avg  Best  Wrst StDev
1.|-- gateway (192.168.26.2)     0.0%    10    0.3   0.3   0.2   0.4   0.0
2.|-- 192.168.29.154             0.0%    10    0.9   0.8   0.7   1.0   0.0
```


测试完成删除 `mtr` 进程
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$systemctl stop mtr-print-log.timer
┌──[root@vms81.liruilongs.github.io]-[~]
└─$systemctl is-active mtr-print-log.service
unknown
```

## 博文参考

***


https://help.aliyun.com/document_detail/98706.html

https://stackoverflow.com/questions/41741588/running-mtr-network-diagnostic-tool-in-the-background-like-nohup-processes