---
title: 关于Linux中web服务方面的一些笔记
tags:
  - Linux
  - httpd
categories:
  - httpd
toc: true
recommend: 1
keywords: Linux
uniqueId: '2021-10-08 11:12:05/关于Linux中web服务方面的一些笔记.html'
mathJax: false
date: 2021-10-08 19:12:05
thumbnail:
---
> 摘要
首页显示摘要内容(替换成自己的)
<!-- more -->
## 写在前面
***
+ 


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>
 ***

# 一、Web 基础

## <font color=purple>CentOS7中的Web服务</font>
+ 软件包：httpd
+ 系统服务：httpd

**<font color=chocolate>提供的默认配置</font>**

|提供的默认配置|
|--|
|Listen: 监听地址：端口(80)；|
|ServerName: 本站点注册的DNS名称(空缺)；|
|DocumentRoot: 网页根目录(/var/www/html);|
|DirectoryIndex: 起始页/首页文件名(index.html)|

## <font color=royalblue>独立Web站点的快速部署</font>

检查防火墙，selinux。
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$getenforce
Disabled
┌──[root@vms81.liruilongs.github.io]-[~]
└─$firewall-cmd  --get-default-zone
trusted
```
|独立Web站点的快速部署|
|--|
|装包(httpd);|
|配置(部署首页 index.html);|
|起服务(httpd);|
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$rpm -q httpd
package httpd is not installed
┌──[root@vms81.liruilongs.github.io]-[~]
└─$yum -y install httpd
Loaded plugins: fastestmirror
Loading mirror speeds from cach
...............
┌──[root@vms81.liruilongs.github.io]-[~]
└─$systemctl enable httpd --now
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.
┌──[root@vms81.liruilongs.github.io]-[~]
└─$ss -ntulpa | grep http
tcp    LISTEN     0      128      :::80                   :::*                   users:(("httpd",pid=84865,fd=4),("httpd",pid=84864,fd=4),("httpd",pid=84863,fd=4),("httpd",pid=84862,fd=4),("httpd",pid=84861,fd=4),("httpd",pid=84858,fd=4))
┌──[root@vms81.liruilongs.github.io]-[~]
└─$echo "生活加油！^_^" > /var/www/html/index.html
┌──[root@vms81.liruilongs.github.io]-[~]
└─$curl http://192.168.26.81
生活加油！^_^

```
 **<font color=orange>修改Apache的默认路径</font>**
```
┌──[root@vms81.liruilongs.github.io]-[~]
└─$rpm -qc httpd #查看httpd服务中的配置文件
/etc/httpd/conf.d/autoindex.conf
/etc/httpd/conf.d/userdir.conf
/etc/httpd/conf.d/welcome.conf
/etc/httpd/conf.modules.d/00-base.conf
/etc/httpd/conf.modules.d/00-dav.conf
/etc/httpd/conf.modules.d/00-lua.conf
/etc/httpd/conf.modules.d/00-mpm.conf
/etc/httpd/conf.modules.d/00-proxy.conf
/etc/httpd/conf.modules.d/00-systemd.conf
/etc/httpd/conf.modules.d/01-cgi.conf
/etc/httpd/conf/httpd.conf #主配置文件
/etc/httpd/conf/magic
/etc/logrotate.d/httpd
/etc/sysconfig/htcacheclean
/etc/sysconfig/httpd
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```
**<font color=red>配置文件修改</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$grep -v -w "#" /etc/httpd/conf/httpd.conf | grep -v ^$ | grep -v ^#
ServerRoot "/etc/httpd"
Listen 80                        #修改Apache的默认端口号为8080
Include conf.modules.d/*.conf
User apache
Group apache
ServerAdmin root@localhost
<Directory />
    AllowOverride none
    Require all denied
</Directory>
DocumentRoot "/var/www/html"  #修改Apache的默认路径
<Directory "/var/www">
    AllowOverride None
    Require all granted
</Directory>
<Directory "/var/www/html">
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>
<IfModule dir_module>
    DirectoryIndex index.html
</IfModule>
<Files ".ht*">
    Require all denied
</Files>
ErrorLog "logs/error_log"
LogLevel warn
<IfModule log_config_module>
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
    LogFormat "%h %l %u %t \"%r\" %>s %b" common
    <IfModule logio_module>
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
    </IfModule>
    #CustomLog "logs/access_log" common
    CustomLog "logs/access_log" combined
</IfModule>
<IfModule alias_module>
    ScriptAlias /cgi-bin/ "/var/www/cgi-bin/"
</IfModule>
<Directory "/var/www/cgi-bin">
    AllowOverride None
    Options None
    Require all granted
</Directory>
<IfModule mime_module>
    TypesConfig /etc/mime.types
    #AddType application/x-gzip .tgz
    #AddEncoding x-compress .Z
    #AddEncoding x-gzip .gz .tgz
    AddType application/x-compress .Z
    AddType application/x-gzip .gz .tgz
    #AddHandler cgi-script .cgi
    #AddHandler type-map var
    AddType text/html .shtml
    AddOutputFilter INCLUDES .shtml
</IfModule>
AddDefaultCharset UTF-8
<IfModule mime_magic_module>
    MIMEMagicFile conf/magic
</IfModule>
EnableSendfile on
IncludeOptional conf.d/*.conf
┌──[root@vms81.liruilongs.github.io]-[~]
└─$
```

# <font color=yellowgreen>二、常用配置解析</font>

## <font color=royalblue>客户机地址限制</font>
|使用 配置区段|
|--|
|每个文件夹自动继承其父目录的ACL访问权限；|
|除非针对子目录有明确设置；|

```bash
<Directory 目录的绝对路径>
......
Require all denied|granted
Require ip IP或网段地址 ......
</Directory>
### 每个文件夹自动继承其父目录的ACL访问权限 ###
<Directory /var/www/myweb>
Require all denied #拒绝所有人访问
</Directory>
<Directory /var/www/nsd>
Require all granted #拒绝所有人访问
</Directory>
<Directory /var/www/myweb/abc>
Require all granted #abc要拥有和父目录不同的ACL，需要单独去定义
</Directory>
```
**<font color=blue>禁止任何客户机访问</font>**
```xml
<Directory />
Require all denied
</Directory>

```
**<font color=orange>允许任何客户机访问</font>**
```xml
<Directory "/var/www/html">
Require all granted
</Directory>
```
**<font color=royalblue>仅允许部分客户机访问</font>**
```xml
<Directory "/var/www/html/private">
Require ip 127.0.0.1 ::1 172.25.0.11
</Directory>
```
**<font color=royalblue>自定义其他目录为httpd的根目录</font>**
```bash
[root@liruilong ~]# mkdir /webapp #创建目录，让其作为根目录
[root@liruilong ~]# vim /etc/httpd/conf/httpd.conf
118 #DocumentRoot "/var/www/html"
119 #DocumentRoot "/var/www/myweb" #注释掉
120 DocumentRoot "/webapp" #修改根目录的配置，【:Document 末行模式查找】
131 <Directory "/webapp"> #添加新的默认路径权限
132 Require all granted #添加允许权限
133 </Directory>
[root@liruilong ~]# systemctl restart httpd #重启服务
```
# <font color=green>三、虚拟Web主机</font>

**<font color=chocolate>虚拟Web主机:由同一台服务器提供多个不同的Web站点；
</font>**

## <font color=amber>配置一个虚拟站点</font>
**<font color=amber>配置文件路径</font>**
```bash
/etc/httpd/conf/httpd.conf #主配置文件
/etc/httpd/conf.d/*.conf #调用配置文件 IncludeOptional conf.d/*.conf
```
**<font color=seagreen>为每个虚拟机站点添加配置：类似于反向代理</font>**

```xml
<VirtualHost IP地址:端口> #IP地址为本机的IP地址，监控的是本机的哪个端口
ServerName 此站点的 DNS 名称
DocumentRoot 此站点的网页根目录
</VirtualHost>
```
**<font color=royalblue>定义qq 和 baidu 的虚拟主机</font>**
```xml
<VirtualHost *:80>
ServerName www.qq.com #域名
DocumentRoot /var/www/qq #根目录
</VirtualHost>
<VirtualHost *:80>
ServerName www.baidu.com #域名
DocumentRoot /var/www/baidu #根目录
</VirtualHost>
```
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$cat /etc/httpd/conf.d/nsd01.conf
<VirtualHost *:80>
ServerName www.qq.com #域名
DocumentRoot /var/www/qq #根目录
</VirtualHost>
<VirtualHost *:80>
ServerName www.baidu.com #域名
DocumentRoot /var/www/baidu #根目录
</VirtualHost>
┌──[root@vms81.liruilongs.github.io]-[~]
└─$mkdir /var/www/qq baidu
```
```bash
[root@liruilong ~]# vim /etc/hosts #定义仅本机的域名解析
......
192.168.4.5 www.qq.com
192.168.4.5 www.baidu.com
##配置测试页面
[root@liruilong ~]# echo "www.qq.com" > /var/www/qq/index.html #定义qq的默认页面
[root@liruilong ~]# echo "www.baidu.com" > /var/www/baidu/index.html #定义baidu的默认
页面
##访问测试
[root@liruilong ~]# curl www.qq.com #访问qq网页
www.qq.com
[root@liruilong ~]# curl www.baidu.com #访问baidu页面
www.baidu.com
```

## <font color=camel>对默认 Web 站点的影响</font>


**<font color=plum>一旦启用虚拟 Web 主机以后,外部的 DocumentRoot、ServerName 会被忽略；第1个虚拟站点被视为默认站点，若客户机请求的 URL 不属于任何已知站点，则由第一个站点相应；</font>**


# <font color=green>一、DNS服务概述</font>

负责域名解析的DNS服务：DNS(Domain Name System)服务是和HTTP协议一样位于应用层的协议，提供域名到IP地址之间的解析服务(通过域名查找IP地址，或逆向)。计算机既可以被赋予IP地址，也可以被赋予主机名和域名。

##  <font color=yellowgreen>DNS解析的作用</font> 

|方向|描述|
|--|--|
|正向解析|根据主机名称 (域名) 查找其对应的 IP 地址；这是最基本、最常用的功能；|
|反向解析|根据IP 地址查找其对应的主机名称 (域名)；反垃圾邮件/安全防护等领域使用；|

## <font color=tomato>DNS的分布式结构</font>

|<font color=royalblue>大型、分布式的互联网 DNS 解析库</font>|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/0d9acebad767449796dd5a2cb97b5bc0.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

```bash
#所有的域名都以.作为结尾，访问网站的时候会自动添加一个.；
#根域名服务器全球目前只有13台

###一级域名###
.com #代表国际的商业组织；
.net #代表国际的网络运营商；
.edu #代表教育机构；
.cn #代表中国；
.org #代表非盈利组织；
.us #代表美国
.kr #代表韩国
.hk #代表香港
.tw #代表台湾

###二级域名###
.net.cn #代表中国的网络运营商；
.com.cn #代表中国的商业组织；
.edu.cn #代表中国的教育机构；

###三级域名###
.sina.com.cn #具体公司域名
.liruilong.com.cn #具体公司域名
###主机站点###
可以自己根据服务的功能随意去定义
```
## <font color=brown>BIND 域名服务</font>

|<font color=plum>BIND(Berkeley Internet Name Daemon)</font>|
|--|
|伯克利 Internet 域名服务|
|官方站点：http://www.isc.org/|

```bash
┌──[root@vms81.liruilongs.github.io]-[/var/www]
└─$yum -y install bind-chroot bind
┌──[root@vms81.liruilongs.github.io]-[/var/www]
└─$rpm -qa bind*
bind-libs-lite-9.11.4-26.P2.el7_9.7.x86_64
bind-9.11.4-26.P2.el7_9.7.x86_64
bind-license-9.11.4-26.P2.el7_9.7.noarch
bind-libs-9.11.4-26.P2.el7_9.7.x86_64
bind-export-libs-9.11.4-26.P2.el7_9.7.x86_64
bind-chroot-9.11.4-26.P2.el7_9.7.x86_64
┌──[root@vms81.liruilongs.github.io]-[/var/www]
└─$
```

|<font color=royalblue>BIND 服务器端程序</font>|
|--|
|主要执行程序：`/usr/sbin/named`|
|服务名：`named`|
|默认端口：`TCP/UDP 53`|
|运行时的虚拟根环境：`/var/named/chroot/`|
|主配置文件：/etc/named.conf|
|地址库文件：/var/named/......|
|主配置文件:负责定义DNS服务器对哪个域名来进行解析|
|地址库文件:存放域名对应对应的IP地址 例如：www.baidu.com 192.168.3.4 192.168.4.3等等|

# <font color=red>二、构建DNS服务器</font>

## <font color=seagreen>named.conf 配置解析</font>

|配置部分|描述|
|--|--|
|全局配置部分|设置监听地址/端口、区域数据存放位置等|
|区域配置部分|定义 DNS 区域、类型、地址文件路径等；关键词 IN 表示 Internet，可省略；|

## <font color=royalblue>修改DNS服务的配置文件</font>
```bash
##卸载防火墙
[root@liruilong ~]# yum -y remove firewalld #卸载防火墙
##关闭SELinux
[root@liruilong ~]# vim /etc/selinux/config #重启后生效，reboot
SELINUX=disabled
[root@liruilong ~]# getenforce #查看SELinux策略
Disabled

```
systemctl reload  httpd和systemctl daemon-reload的区别
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl status  httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/httpd.service.d
           └─50-BlockIOAccounting.conf, 50-CPUShares.conf, 50-MemoryLimit.conf
   Active: active (running) since 三 2022-04-13 11:42:48 CST; 17s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 38628 (httpd)
   Status: "Total requests: 0; Current requests/sec: 0; Current traffic:   0 B/sec"
    Tasks: 6
   Memory: 2.8M (limit: 500.0M)
   CGroup: /system.slice/httpd.service
           ├─38628 /usr/sbin/httpd -DFOREGROUND
           ├─38629 /usr/sbin/httpd -DFOREGROUND
           ├─38630 /usr/sbin/httpd -DFOREGROUND
           ├─38631 /usr/sbin/httpd -DFOREGROUND
           ├─38632 /usr/sbin/httpd -DFOREGROUND
           └─38633 /usr/sbin/httpd -DFOREGROUND

4月 13 11:42:48 liruilongs.github.io systemd[1]: Starting The Apache HTTP Server...
4月 13 11:42:48 liruilongs.github.io systemd[1]: Started The Apache HTTP Server.
┌──[root@liruilongs.github.io]-[~]
└─$
```
修改
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vim /usr/lib/systemd/system/httpd.service
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl reload  httpd
Warning: httpd.service changed on disk. Run 'systemctl daemon-reload' to reload units.
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl daemon-reload
┌──[root@liruilongs.github.io]-[~]
└─$
```