---
title: 关于 Linux 中安全方面的一些笔记
tags:
  - Liunx
  - 安全
categories:
  - 安全
toc: true
recommend: 1
keywords: 安全，运维,Liunx
uniqueId: '2021-07-30 01:39:39/关于 Linux 中安全方面的一些笔记.html'
mathJax: false
date: 2021-07-30 09:39:39
thumbnail:
---

+ 笔记是学习整理的，适合`基线扫描`参考，常见`运维安全Demo`。
+ 整理了一份，希望对小伙伴有帮助.生活加油,天天开心！
+ 博文主要围绕以几个方面：
  + `Linux基本防护`：`账户安全`、`文件系统安全`、`关闭不需要的服务`
  + `用户切换`与`提权su`和`sudo`,
  + `sshd访问控制`
  + `加密与解密/对称加密`、`非对称加密`、`md5息摘要`
  + `AIDE入侵检测`
  + `端口扫描`

<!-- more -->
## 写在前面
***
+ 笔记是学习整理的，适合`基线扫描`参考，常见`运维安全Demo`。
+ 整理了一份，希望对小伙伴有帮助.生活加油,天天开心！
+ 博文主要围绕以几个方面：
  + `Linux基本防护`：`账户安全`、`文件系统安全`、`关闭不需要的服务`
  + `用户切换`与`提权su`和`sudo`,
  + `sshd访问控制`
  + `加密与解密/对称加密`、`非对称加密`、`md5息摘要`
  + `AIDE入侵检测`
  + `端口扫描`


# <font color=tomato>一、Linux基本防护</font>

|一、Linux基本防护|
|:--|
|账户安全:设置账户有效期,锁定与解锁账户密码,修
改登陆信息|
|文件系统安全:修改
文件 ATTR属性,设置mount挂载|
|关闭不需要的服务|
***
| <font color=brown>设置账号有效期</font>|
|:--|
|**<font color=red>使用chage工具</font>**|
|`-d 0`, 强制修改密码|
|`-E yyyy-mm-dd`，指定失效日期(-1 取消)|


```bash
##创建账户dachui
[root@liruilong ~]$ useradd dachui
####查看账户dachui的过期时间
[root@liruilong ~]$ chage -l dachui
最近一次密码修改时间 ：8月 18, 2020
密码过期时间 ：从不
密码失效时间 ：从不
帐户过期时间 ：从不 ##永不过期
两次改变密码之间相距的最小天数 ：0
两次改变密码之间相距的最大天数 ：99999
在密码过期之前警告的天数 ：7
###-E 指定账户dachui的过期时间
[root@liruilong ~]$ chage -E 2029-10-01 dachui
[root@liruilong ~]$ chage -l dachui
最近一次密码修改时间 ：8月 18, 2020
密码过期时间 ：从不
密码失效时间 ：从不
帐户过期时间 ：10月 01, 2029 ##过期时间指定
两次改变密码之间相距的最小天数 ：0
两次改变密码之间相距的最大天数 ：99999
在密码过期之前警告的天数 ：7
###-E 后跟数字-1，代表取消账户的过期时间设置
[root@liruilong ~]$ chage -E -1 dachui
[root@liruilong ~]$ chage -l dachui
最近一次密码修改时间 ：8月 18, 2020
密码过期时间 ：从不
密码失效时间 ：从不
帐户过期时间 ：从不 ##账户永不过期
两次改变密码之间相距的最小天数 ：0
两次改变密码之间相距的最大天数 ：99999
在密码过期之前警告的天数 ：7
```
**<font color=camel>设置强制要求用户修改密码 `-d 0` 案例</font>**

```bash
########设置强制要求用户修改密码 -d 0 案例
##第一步：给用户dachui设置密码
[root@liruilong ~]$ echo 123456 | passwd --stdin dachui
更改用户 dachui 的密码 。
passwd：所有的身份验证令牌已经成功更新。
##第二步：使用dachui用户远程本机
[root@liruilong ~]$ ssh dachui@127.0.0.1
......
dachui@127.0.0.1 s password: #输入密码123456
[dachui@liruilong ~]$ #可以登录，ctrl + D 退出
##第三步：管理员设置dachui用户必须修改密码，否则无法登录
[root@liruilong ~]$ chage -d 0 dachui
##第四步：重新使用dachui用户远程本机，需要重新设置密码
#因为是普通用户，权限低，所以密码必须是8位以上，字母和数字(tarena123)
[root@liruilong ~]$ ssh dachui@127.0.0.1
dachui@127.0.0.1s password: #输入密码123456
You are required to change your password immediately (root enforced)
Last failed login: Tue Aug 18 08:38:45 CST 2020 from localhost on ssh:notty
There was 1 failed login attempt since the last successful login.
Last login: Tue Aug 18 08:33:19 2020 from localhost
WARNING: Your password has expired.
You must change your password now and login again!
Changing password for user dachui.
Changing password for dachui.
(current) UNIX password: #输入以前的密码123456
New password: #设置新的密码为123，太过简单无法通过
BAD PASSWORD: The password is too similar to the old one
New password: #设置新的密码为654321，密码最少是8位
BAD PASSWORD: The password is shorter than 8 characters
New password: #设置新的密码：tarena123
Retype new password: #重新输入新密码：tarena123
passwd: all authentication tokens updated successfully.
Connection to 127.0.0.1 closed.
#第五步：重新使用账户dachui远程本机
[root@liruilong ~]$ ssh dachui@127.0.0.1
dachui@127.0.0.1s password: #输入密码：tarena123
Last login: Tue Aug 18 08:38:49 2020 from localhost
[dachui@liruilong ~]$ #ctrl + D 登出

```

### <font color=seagreen>账号的锁定/解锁</font>
 **<font color=blue>使用passwd命令:  `-l 锁定、-u 解锁、-S 看状态`</font>**

![在这里插入图片描述](https://img-blog.csdnimg.cn/bd75fd65a014498bb42f8cf947b498c6.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

```bash
####-S(大写) 查看用户dachui密码的状态
[root@liruilong ~]$ passwd -S dachui
dachui PS 2020-08-18 0 99999 7 -1 (密码已设置，使用 SHA512 算法。)
###-l 锁定dachui账户
[root@liruilong ~]$ passwd -l dachui
锁定用户 dachui 的密码 。
passwd: 操作成功
####-S(大写) 查看用户dachui密码的状态,密码被锁定，无法登录使用
[root@liruilong ~]$ passwd -S dachui
dachui LK 2020-08-18 0 99999 7 -1 (密码已被锁定。)
###使用账户dachui远程本机,无法登录
[root@liruilong ~]$ ssh dachui@127.0.0.1
dachui@127.0.0.1's password: #密码为: tarena123
Permission denied, please try again.
dachui@127.0.0.1's password:
###-u 解锁dachui账户
[root@liruilong ~]$ passwd -u dachui
解锁用户 dachui 的密码。
passwd: 操作成功
###重新使用账户dachui远程本机,可以登录
[root@liruilong ~]$ ssh dachui@127.0.0.1
dachui@127.0.0.1s password: #密码为: tarena123
Last failed login: Tue Aug 18 08:55:30 CST 2020 from localhost on ssh:notty
There was 1 failed login attempt since the last successful login.
Last login: Tue Aug 18 08:46:50 2020 from localhost
[dachui@liruilong ~]$ #ctrl + D 退出登录
Connection to 127.0.0.1 closed.
#####-S(大写) 重新查看用户dachui密码的状态，已解锁
[root@liruilong ~]$ passwd -S dachui
dachui PS 2020-08-18 0 99999 7 -1 (密码已设置，使用 SHA512 算法。)
```
### <font color=camel>强制定期修改密码</font>

+ **<font color=amber>配置文件 /etc/login.defs</font>**
+ **<font color=orange>主要控制属性</font>**

|主要控制属性|
|:--|
|`PASS_MAX_DAYS` —》 用户密码的最长有效期|
|`PASS_MIN_DAYS` —》 用户密码的最短有效期|
|`PASS_WARN_AGE` —》 用户密码过期的前几天会发送警告信息|

```bash
#######此管理用户密码的操作，只会对后面创建的用户生效，以前的则不影响
[root@liruilong ~]$ vim /etc/login.defs
......
25 PASS_MAX_DAYS 60 #一个密码最长可以使用60天
26 PASS_MIN_DAYS 0 #一个密码最少要用多少天，否则无法修改,0不设置
27 PASS_MIN_LEN 5 #密码的最少长度为5
28 PASS_WARN_AGE 7 #密码过期的前7天，对用户发送警告信息
......
#####对当前用户的密码进行管理，/etc/shadow 为用户密码的配置文件
###用户密码的最长有效期为99999，可以直接修改配置文件对以前的用户进行密码管理
[root@liruilong ~]$ cat /etc/shadow | head -3
root:$6$l2XJYza/aL1Ug4Ta$4kBxc4ED.pyWbBR5Yg3XVX/3lT3S0Efuh4eNxC83AdIlYDeEzcafsbV
8YkVo88T0W/vVDTxpYtiFceiUM9qQk0::0:99999:7:::
bin:*:17632:0:99999:7:::
daemon:*:17632:0:99999:7:::
```
### <font color=purple>伪装登录提示</font>
|伪装登录提示|
|:--|
|**<font color=camel>配置文件 /etc/issue、/etc/issue.net</font>**|
|`分别使用于本地、远程登录`|
|`默认会提示内核、系统等版本信息`|


这个 `centos7` 可能有点问题，可以写到 .bashrc 文件里：.bashrc文件通常也是通过某个bash启动文件来运行的。因为.bashrc文件会运行两次：一次是 当你登入bash shell时，另一次是当你启动一个bash shell时。如果你需要一个脚本在两个时刻都得 以运行，可以把这个脚本放进该文件中。

```bash
#####修改本地用户登录的提示信息
[root@liruilong ~]$ vim /etc/issue
Welcome to Tedu
#####修改网络用户登录的提示信息,远程登录时显示，ssh默认关闭此功能
##可以不修改
[root@liruilong ~]$ vim /etc/issue.net
Welcome to Ted
```

![在这里插入图片描述](https://img-blog.csdnimg.cn/0b5754c98a354cc48ce103a90c02629a.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)


## <font color=seagreen>文件系统安全</font>
### <font color=blue>锁定/解锁保护文件</font>
+ <font color=royalblue>文件属性控制</font>：`chattr、lsattr`
+ <font color=yellowgreen>、- 控制方式</font>
  + `属性i`：<font color=camel>不可变 (immutable)</font>
  + `属性a`: <font color=green>仅可追加(append only)</font>

![在这里插入图片描述](https://img-blog.csdnimg.cn/4205473f4fa74c6c8dafd91ba37ad9b1.png)

```bash
######创建一个测试文件
[root@liruilong ~]$ vim test.sh
hello the world
ni hao.
#########文件如果有i的属性，则对于此文件，增删改查均无法操作
##lsattr 查看文件的属性
[root@liruilong ~]$ lsattr test.sh
---------------- test.sh
##chattr 给文件test.sh增加i的属性
[root@liruilong ~]$ chattr +i test.sh
##查看文件test.sh属性
[root@liruilong ~]$ lsattr test.sh
----i----------- test.sh
##测试，写入操作和删除操作都无法进行
[root@liruilong ~]$ echo 123 > test.sh
-bash: test.sh: 权限不够
[root@liruilong ~]$ rm -rf test.sh
rm: 无法删除"test.sh": 不允许的操作
##取消文件test.sh的i的属性，通过lsattr查看
[root@liruilong ~]$ chattr -i test.sh
[root@liruilong ~]$ lsattr test.sh
---------------- test.sh
#########文件如果有a的属性，则对于此文件，只能执行追加的操作
####对于日志文件执行a属性，防止内容被篡改
##给文件test.sh增加一个a属性，lsattr查看
[root@liruilong ~]$ chattr +a test.sh
[root@liruilong ~]$ lsattr test.sh
-----a---------- test.sh
###测试，追加内容的操作可以进行，写入，删除失败
[root@liruilong ~]$ echo 123 >> test.sh
[root@liruilong ~]$ echo 123 > test.sh
-bash: test.sh: 不允许的操作
[root@liruilong ~]$ rm -rf test.sh
rm: 无法删除"test.sh": 不允许的操作
##取消文件test.sh的a的属性，通过lsattr查看
[root@liruilong ~]$ chattr -a test.sh
[root@liruilong ~]$ lsattr test.sh
---------------- test.sh
```
### <font color=red>文件系统挂载熟悉</font>

**<font color=yellowgreen>mount 挂载属性</font>**
+ **<font color=seagreen>noexec: 不可执行程序</font>**
+ **<font color=plum>noatime: 不更新文件的访问时间</font>**

>**noexec**:
<font color=royalblue>挂载设备时，添加此选项，则此设备中的所有程序均不可被执行(例如：病毒或木马)</font>
>**noatime**:
<font color=plum>计算机中的文件都有访问时间(atime)，修改时间(mtime)；
挂载设备时，添加此选项，则所有文件的访问时间都不再被更新；
如果计算机中的a文件被用户访问，则a文件的atime就会被修改，对于web服务器而言，会有成千上万的
用户访问网页，则这个网页的atime就会被频繁修改，会消耗大量的CPU资源，需要在挂载的时候使用</font>

```bash
#########boot分区挂载示例，其他挂载方法类似
[root@liruilong ~]$ vim /etc/fstab
UUID=c120742d-5bdf-48c3-b830-3bfb3e796009 /boot xfs
defaults,noexec,noatime 0 0
###-o remount 重新挂载,让配置生效
[root@liruilong ~]$ mount -o remount /boot/
###查看挂载的属性，/boot分区下，有了noexec,noatime
[root@liruilong ~]$ mount
......
/dev/sda1 on /boot type xfs (rw,noexec,noatime,attr2,inode64,noquota)
......
#####定义一个不断输出a的脚本，赋予x执行权限，却无法运行
[root@liruilong ~]$ vim /boot/test.sh
#!/bin/bash
while :
do
echo a
done
##必须是在/boot/目录下，执行脚本
[root@liruilong ~]$ cd /boot
[root@liruilong boot]$ chmod +x test.sh
[root@liruilong boot]$ ./test.sh
-bash: ./test.sh: 权限不够
```

### <font color=red>程序和服务控制</font>

+ <font color=tomato>禁用非必要的系统服务</font>: 使用systemctl、chkconfig (centos6使用) 工具
+ <font color=blue>可选服务列表</font>(选择进行关闭)

|**<font color=brown>可选服务列表</font>**|
|--|
|`cups`.service 打印服务|
|`postfix`.service 邮件服务|
|`NetworkManager`.service 网络管理服务(network可以替代)|
|`firewalld` 防火墙 (iptables可以替代)|
|`atd`.service 一次性计划任务(crond可以替代)|
|`bluetooth`.service 蓝牙服务|
|`autofs`.service 自动挂载|
|`pcscd`.service 智能卡设备资源管理器|


```bash
#####停止服务，并设置为开机不会自动启动
[root@liruilong ~]$ systemctl stop firewalld
[root@liruilong ~]$ systemctl disable firewalld
```


# <font color=chocolate>二、用户切换与提权</font>
##  <font color=red>su切换用户身份</font>
### <font color=seagreen>切换与提权的应用场景</font>
+ `切换用户身份，When?:`SSH远程管理/运维测试
+ `提升执行权限，when?:`管理权限细分
###  <font color=brown>su切换的基本用法:Substitube User，换人</font>
+ 快速切换为指定的其他用户
+ 普通用户执行时，需验证目标用户的口令
+ root执行时，无需验证口令

命令格式
+ 用法1：su [-] [目标用户]
+ 用法2：su [-] -c "命令" [目标用户]
### <font color=chocolate>su操作示例</font>
**<font color=purple>从普通用户切换为root，并登录新Shell环境,执行 su -，或者su - root,不指名目标用户时，默认视为root</font>**
```bash
#####创建新的用户jerry，并设置密码为123456
[root@liruilong ~]$ useradd jerry
[root@liruilong ~]$ echo 123456 | passwd --stdin jerry
更改用户 jerry 的密码 。
passwd：所有的身份验证令牌已经成功更新。
###从root用户切换到jerry用户
[root@liruilong ~]$ su - jerry
###查看当前当前登录的用户身份为jerry，普通用户的权限很低
[jerry@liruilong ~]$ whoami
jerry
[jerry@liruilong ~]$ cat /etc/shadow
cat: /etc/shadow: Permission denied
[jerry@liruilong ~]$ ls /root/
ls: cannot open directory /root/: Permission denied
[jerry@liruilong ~]$ exit #退出jerry用户登录状态
logout
```
```bash
#####创建新的用户tom，并设置密码为123456
[root@liruilong ~]$ useradd tom
[root@liruilong ~]$ echo 123456 | passwd --stdin tom
更改用户 tom 的密码 。
passwd：所有的身份验证令牌已经成功更新。
###从root用户切换到jerry用户，管理员切换成普通用户身份不需要密码
[root@liruilong ~]$ su - jerry
上一次登录：二 8月 18 10:21:41 CST 2020pts/0 上
###从jerry用户切换到tom用户,普通用户身份的切换需要输入密码
[jerry@liruilong ~]$ su - tom
Password: #输入tom用户的密码
[tom@liruilong ~]$ exit #退出tom用户的登录
logout
[jerry@liruilong ~]$ exit #退出jerry用户的登录
logout
```

```bash
###从root用户切换到jerry用户，管理员切换成普通用户身份不需要密码
[root@liruilong ~]$ su - jerry
上一次登录：二 8月 18 10:23:02 CST 2020pts/0 上
###su - 后面不跟用户，则默认是切换到root用户下
[jerry@liruilong ~]$ su -
Password: #输入管理员root的密码
Last login: Tue Aug 18 10:23:53 CST 2020 on pts/0
[root@liruilong ~]$ exit #退出，回到jerry用户下
[jerry@liruilong ~]$ exit #退出，回到最出的root解释器下

```
 **<font color=chocolate>root以指定的普通用户身份执行任务:以用户tom的身份创建目录,以用户tom的身份执行管理员操作会出错</font>**
```bash
###-c 以普通用户jerry的身份执行一条命令
[root@liruilong ~]$ su - jerry -c "touch /tmp/test.txt"
##查看文件的属性
[root@liruilong ~]$ ll /tmp/test.txt
-rw-rw-r-- 1 jerry jerry 0 8月 18 10:25 /tmp/test.txt
####错误，普通用户没有权利去重启sshd服务
[root@liruilong ~]$ su - tom -c "systemctl restart sshd"
Error creating textual authentication agent:
......
```
**<font color=blue>分析su切换的使用情况：安全日志 /var/log/secure，记录su验证、Shell开启与关闭</font>**

```bash
[root@liruilong ~]$ tail -4 /var/log/secure
Aug 18 10:25:03 localhost su: pam_unix(su-l:session): session opened for user
jerry by root(uid=0)
Aug 18 10:25:03 localhost su: pam_unix(su-l:session): session closed for user
jerry
Aug 18 10:40:50 localhost su: pam_unix(su-l:session): session opened for user
tom by root(uid=0)
Aug 18 10:40:51 localhost su: pam_unix(su-l:session): session closed for user
tom
```
## sudo 提升执行权限
### sudo提权的基本用法
+ Super or another Do，超级执行:管理员预先为用户设置执行许可,被授权用户有权执行授权的命令，验证自己的口令
+ 执行提权命令:  用法： sudo 提权命令
+ 查看提权命令: 用法：sudo -l

![在这里插入图片描述](https://img-blog.csdnimg.cn/d5351b21f8994565bf8b24073d10b5bf.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

### <font color=royalblue>配置sudo提权</font>
**<font color=chocolate>修改方法</font>**
+ `推荐：visudo`
+ `其他：vim /etc/sudoers ---> wq!(强制保存并退出)`授权记录格式`

![在这里插入图片描述](https://img-blog.csdnimg.cn/d5351b21f8994565bf8b24073d10b5bf.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

>`用户或组` 
> #指定给特定用户授权或者组(多个用户属于一个组)
> #指定给组授权时，组的前面必须加 %，例如：%wheel

>`主机列表` 
>#允许哪些客户机可以通过这个用户登录本机去执行命令，例如：ALL(指所有客户机)
>`提权身份` 
>#给第一列的用户赋予权限，例如:(root)

>`[NOPASSWD]`:命令列表 #前半部分是不需要密码执行命令；后半部分是要执行的命令，且；命令要写绝
对路径

>`%wheel ALL=(root) ALL`   #给wheel组中的所有用户，赋予root的权限，让其可以在任何主机上以root的身份去执行任何命令

![在这里插入图片描述](https://img-blog.csdnimg.cn/10939310b7ea42989e82ba036e389b51.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

**<font color=plum>允许mike以root权限执行 /sbin/ 下的所有命令,但是，禁止修改eth0网卡的参数</font>**

```bash
/sbin/* #模糊匹配，/sbin/下的所有命令都可以用sudo提权使用
!/sbin/ifconfg etho #! 代表取反,该命令不能被sudo提权使用
```
```bash
####sudo 提权，让普通用户jerry可以执行systemctl restart sshd 命令
[root@liruilong ~]$ id jerry
uid=1002(jerry) gid=1002(jerry) 组=1002(jerry)
####通过which命令，获取systemctl的绝对路径
[jerry@liruilong ~]$ which systemctl
/bin/systemctl
###最后一行追加，允许jerry用户，可以以任何人的身份，从任何客户端，执行此命令
##立刻生效
[root@liruilong ~]$ visudo
......
jerry ALL=(ALL) /usr/bin/systemctl
######测试，验证
##切换到jerry用户下
[root@liruilong ~]$ su - jerry
上一次登录：二 8月 18 10:50:16 CST 2020pts/0 上
####使用sudo提权命令，重启sshd服务
[jerry@liruilong ~]$ sudo systemctl restart sshd
[sudo] password for jerry: ##jerry用户密码：123456
Sorry, try again.
###sudo -l 查看jerry用户可以执行的sudo提权命令
[jerry@liruilong ~]$ sudo -l
......
User jerry may run the following commands on liruilong:
(ALL) /usr/bin/systemctl

```

 **<font color=green>wheel组的用户无需验证可执行所有命令</font>**


```bash
####通过which命令，获取parted分区命令的绝对路径
[root@liruilong ~]$ which parted
/usr/sbin/parted
###最后一行追加,允许tom用户,可以以任何人的身份，从任何客户端，无密码使用parted命令
##立刻生效
[root@liruilong ~]$ visudo
......
jerry ALL=(ALL) /usr/bin/systemctl
tom ALL=(ALL) NOPASSWD:/usr/sbin/parted
######测试，验证
##切换到tom用户下
[root@liruilong ~]$ su - tom
上一次登录：二 8月 18 10:40:50 CST 2020pts/0 上
##sudo提权后，tom用户下，查看磁盘/dev/sda的分区情况
[tom@liruilong ~]$ sudo parted /dev/sda print
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sda: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:
Number Start End Size Type File system Flags
1 1049kB 1075MB 1074MB primary xfs boot
2 1075MB 21.5GB 20.4GB primary lvm
[tom@liruilong ~]$ exit #退出tom用户登录
```

**<font color=purple>分析sudo提权的使用情况: 修改全局配置，启动日志`Defaults logfile="/var/log/sudo"`</font>**
```bash
##########修改全局配置，启动的日志,该日志记录sudo提权的使用情况
##最后一行添加：Defaults logfile="/var/log/sudo"
[root@liruilong ~]$ visudo
......
jerry ALL=(ALL) /usr/bin/systemctl
tom ALL=(ALL) NOPASSWD:/usr/sbin/parted
Defaults logfile="/var/log/sudo"
########验证日志信息，需先执行一次提权命令
[root@liruilong ~]$ su - tom
上一次登录：二 8月 18 10:56:52 CST 2020pts/0 上
##sudo提权后，tom用户下，查看磁盘/dev/sda的分区情况
[tom@liruilong ~]$ sudo parted /dev/sda print
Model: VMware, VMware Virtual S (scsi)
Disk /dev/sda: 21.5GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:
Number Start End Size Type File system Flags
1 1049kB 1075MB 1074MB primary xfs boot
2 1075MB 21.5GB 20.4GB primary lvm
[tom@liruilong ~]$ exit #退出tom用户的登录状态
######查看日志，看看sudo提权的使用信息
[root@liruilong ~]$ cat /var/log/sudo
Aug 18 13:02:12 : jerry : TTY=pts/0 ; PWD=/home/jerry ; USER=root ;
COMMAND=/bin/systemctl restart sshd
Aug 18 13:18:34 : tom : TTY=pts/0 ; PWD=/home/tom ; USER=root ;
COMMAND=/sbin/parted /dev/sda print
```
# <font color=brown>三、sshd访问控制</font>
## <font color=chocolate>SSH基本防护</font>
### SSH防护概述
+ 存在的安全隐患
  + 密码嗅探、键盘记录
  + 暴力枚举账号、猜解密码
+ 常见的防护措施
  + 用户限制、黑白名单
  + 更改验证方式(密码 --> 密钥对)
  + 防火墙....

### <font color=royalblue>sshd基本安全配置</font>
<font color=plum>配置文件</font> `/etc/ssh/sshd_config`
+ Port 3389 //改用非标准端口
+ ListenAddress 192.168.168.174
+ PermitrootLogin //禁止root登录

```bash
#####修改ssh配置时，先备份sshd主配置文件
[root@liruilong ~]$ cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
####修改虚拟机ssh远程连接得端口为3389
[root@liruilong ~]$ vim /etc/ssh/sshd_config
17 Port 3389
##重启sshd服务
[root@liruilong ~]$ systemctl restart sshd
#xshell测试使用ssh远程时必须加端口号3389
[c:\~]$ ssh root@192.168.2.100 3389
####用户只能通过192.168.2.100这个IP地址远程liruilong这台虚拟机
[root@liruilong ~]$ vim /etc/ssh/sshd_config
20 ListenAddress 192.168.2.100
##重启sshd服务，虚拟机新添加网卡配置其他IP地址测试
[root@liruilong ~]$ systemctl restart sshd
#xshell测试使用ssh远程时必须加端口号3389
[c:\~]$ ssh root@192.168.2.100 3389
```
```bash
#####禁止root远程登录虚拟机liruilong
[root@liruilong ~]$ vim /etc/ssh/sshd_config
38 PermitRootLogin no
##重启sshd服务,xshell使用root远程登录liruilong失败
[root@liruilong ~]$ systemctl restart sshd
#####xshell以普通用户jerry的身份可以登录liruilong虚拟机
##使用su -切换成root身份，或者使用sudo提权命令
[c:\~]$ ssh jerry@192.168.2.100 3389 #密码123456
[tom@liruilong ~]$
[tom@liruilong ~]$ su - root #su - 切换root用户
密码： #输入root密码
[root@liruilong ~]$
```
+ <font color=tomato>配置文件</font> `/etc/ssh/sshd_config`
    + `UseDNS no` //不解析客户机地址
    + `LoginGraceTime 1m` //登录限时
    + `MaxAuthTries 3 `//每连接最多认证次数


`UseDNS no  #不对客户机进行域名解析，访问网站的普通用户是没有域名的，不需要解析`
`LoginGraceTime 1m  #用户在ssh远程服务器时，如果1分钟内用户没有输入用户名和密码，则断开连接`
```bash
#########举例演示
##登录限时修改为10s,真实环境需要长一点
[root@liruilong ~]$ vim /etc/ssh/sshd_config
37 LoginGraceTime 10s
115 UseDNS no
##重启sshd服务
[root@liruilong ~]$ systemctl restart sshd
###使用xshell远程连接时，不输入密码，等待10s中，会出现连接关闭的提示
[c:\~]$ ssh jerry@192.168.2.100 3389
Connection closing...Socket close.
```
`MaxAuthTries 3  : #用户远程服务器时，密码错误，可以尝试多少次`
```bash
#这里的3不是指的3次，是一个概数，需要不断尝试错误连接，来得出这里得数字
#########举例演示
##修改连接认证次数为3
[root@liruilong ~]$ vim /etc/ssh/sshd_config
40 MaxAuthTries 3
##重启sshd服务
[root@liruilong ~]$ systemctl restart sshd
##使用xshell远程连接，输入错误密码，输入两次就会出现连接端口的提示
[c:\~]$ ssh jerry@192.168.2.100 3389
Connection closing...Socket close.

```
**<font color=orange>ssh经常一段时间就断掉解决办法去掉注释，改成</font>**
```bash
vim /etc/ssh/sshd_config

ClientAliveInterval 30
ClientAliveCountMax 86400
```
这两行的意思分别是
1. 、客户端每隔多少秒向服务发送一个心跳数据
2. 、客户端多少秒没有相应，服务器自动断掉连接

重启sshd服务
```bash
#service sshd restart
```

+ <font color=chocolate>sshd黑/白名单配</font> `配置文件 /etc/ssh/sshd_config`
    + DenyUsers USER1 USER2 ...
    + AllowUsers USER1@HOST USER2 ...
    + DenyGroups GROUP1 GROUP2 ...
    + AllowGroups GROUP1 GROUP2 ...


```bash
#####设置黑白名单时，如果要允许或拒绝多个用户，用户之间以空格作为分隔符
DenyUsers #设置黑名单，拒绝某些用户登录本机
AllowUsers #设置白名单，允许某些用户登录本机
DenyGroups #设置黑名单，拒绝一个或多个用户组登录本机
AllowGroups #设置白名单，允许一个或多个用户组登录本机
AllowUsers USER1@HOST #设置白名单，允许用户USER1从HOST这台主机登录本机
```
`应用示例:仅允许一部分用户(从指定地点)登入,其他任何用户均禁止登入`
```bash
####使用xshell让用户jerry使用3389端口登录
[c:\~]$ ssh jerry@192.168.2.100 3389
[jerry@liruilong ~]$ su - root #切换为root身份
[jerry@liruilong ~]$
#######设置白名单,仅允许tom可以ssh远程登录服务器，其他任何用户都无法登录
##如果设置的是白名单，则不需要设置黑名单了，默认是拒绝其他用户远程登录
[root@liruilong ~]$ vim /etc/ssh/sshd_config
42 AllowUsers tom
##重启sshd服务
[root@liruilong ~]$ systemctl restart sshd
######xshell远程测试
##jerry无法再登录了
[c:\~]$ ssh jerry@192.168.2.100 3389 #登录密码123456
##tom用户可以远程登录
[c:\~]$ ssh tom@192.168.2.100 3389 #登录密码 123456
[tom@liruilong ~]$

```bash
#######设置黑名单,仅拒绝tom通过ssh远程登录服务器，其他用户都允许
[root@liruilong ~]$ vim /etc/ssh/sshd_config
42 DenyUsers tom
##重启sshd服务
[root@liruilong ~]$ systemctl restart sshd
######xshell远程测试
##tom无法再登录了
[c:\~]$ ssh tom@192.168.2.100 3389 #登录密码123456
##jerry用户可以远程登录
[c:\~]$ ssh jerry@192.168.2.100 3389 #登录密码 123456
[tom@liruilong ~]$
```
```bash
#######设置黑名单,拒绝用户tom从IP地址为192.168.2.1的主机，来远程服务器svr7
[root@svr7 ~]$ vim /etc/ssh/sshd_config
42 DenyUsers tom@192.168.2.1
##重启sshd服务
[root@svr7 ~]$ systemctl restart sshd

```

### <font color=tomato>SSH密钥对验证</font>
**<font color=green>sshd验证方式控制</font>**
+ <font color=red>口令验证</font>::检查登录用户的口令是否一致:密码验证：#当windows被攻击，系统被植入木马，容易被攻击者从键盘记录等方式中获取密码
+ <font color=chocolate>密钥验证</font>:检查客户端私钥与服务器上的公钥是否匹配:密钥验证：#密钥验证的安全度很高；#不会有输入密码的过程，键盘工具方式无效；
+ <font color=chocolate>PasswordAuthentication</font> yes #ssh支持密码验证
```bash
47 AuthorizedKeysFile .ssh/authorized_keys ##指定公钥的存放位置，在用户的家目录
下的.ssh目录中的authorized_keys文件中
```
### <font color=chocolate>密钥对验证的实现思路</font>

![在这里插入图片描述](https://img-blog.csdnimg.cn/40c995b084e54087aea074a9d4695f7c.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

+ 第一步：客户机创建密钥对，私钥文件：id_rsa 公钥文件: id_rsa.pub;
+ 第二步: 客户机上传公钥到服务器上用户的家目录下 ./ssh/authorized_keys文件；
ssh-copy-id 命令，会自动将公钥放到指定用户家目录的对应文件中

**<font color=plum>`创建SSH密钥对`:使用工具 ssh-keygen</font>**
+ 可以手动指定加密算法(-t rsa 或 -t dsa)
+ 若不指定，默认采用RSA加密

```bash
######非交互的方式创建密钥对
[root@svr7 ~]$ ssh-keygen -N '' -f /root/.ssh/id_rsa
Generating public/private rsa key pair.
Your identification has been saved in /root/.ssh/id_rsa.
Your public key has been saved in /root/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:+pb7aZI5xtlulc1Z3MQjjsNGLu5mbCHxU5JfPtWOe+w root@svr7
The keys randomart image is:
+---[RSA 2048]----+
| . |
| . . .o|
| = o .o+|
| . + B o .=|
| S * ++.= |
| o = .oo= .|
| ..+*o. .o |
| .@*+. . o|
| o*O+ oE|
+----[SHA256]-----+
###密钥对会生成在/root/.ssh/目录下
[root@svr7 ~]$ ls /root/.ssh/
id_rsa id_rsa.pub known_hosts
```
`部署SSH公钥`
```bash
#方法一：将公钥通过ssh-copy-id放到192.168.2.200的服务器上
[root@svr7 ~]$ ssh-copy-id root@192.168.2.200
#查看公钥文件的保存路径
[root@client ~]$ ls /root/.ssh/
authorized_keys
```
# <font color=seagreen>四、加密与解密</font>
## <font color=purple>加解密概述</font>
### <font color=tomato>加密目的及方式</font>
+ <font color=green>确保数据的机密性对称加密：</font>
  + 加密/解密用同一个密钥
  + 非对称加密：加密/解密用不同的密钥(公钥、私钥)
+ <font color=yellowgreen>保护信息的完整性(常用于做数据完整性校验)</font>
  + 信息摘要：基于输入的信息生成长度较短、位数固定的散列值
### <font color=royalblue>常见的加密算法</font>
+ <font color=green>对称加密</font>
  + DES，Data Encryption Standard
  + AES，Advanced Encryption Standard
+ <font color=chocolate>非对称加密</font>
  + RSA，Rivest Shamirh Adleman
  + DSA，Digital Signature Algorithm
+ <font color=amber>Hash散列技术,用于信息摘要</font>
  + MD5, Message Digest Algorithm 5 : `根据输入的文本(长度不限) ,生成固定长度(比如128位)的摘要文本`
  + SHA, Secure Hash Algorithm1 : `只要输入的文本不同,则生成的摘要文本也不一样 `

### <font color=amber>MD5完整性检验</font>
+ 使用md5sum校验工具
  + 生成MD5校验值
  + 与软件官方提供的校验值比对

![在这里插入图片描述](https://img-blog.csdnimg.cn/9f3ed8ce1e6b43269e0d32beef47a902.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

## <font color=seagreen>GPG加解密厂具</font>
### <font color=blue>GnuPG简介</font>
**<font color=purple>GnuPG</font>**，GNU Privacy Guard : http://www.gnupg.org/  最流行的数据加密、数字签名工具软件
```bash
########查看gpg的版本，以及支持的算法
[root@node1 ~]$ gpg --version
gpg (GnuPG) 2.0.22
libgcrypt 1.5.3
Copyright (C) 2013 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Home: ~/.gnupg
支持的算法：
公钥：RSA, ?, ?, ELG, DSA
对称加密：IDEA, 3DES, CAST5, BLOWFISH, AES, AES192, AES256,
TWOFISH, CAMELLIA128, CAMELLIA192, CAMELLIA256
散列：MD5, SHA1, RIPEMD160, SHA256, SHA384, SHA512, SHA224
压缩：不压缩, ZIP, ZLIB, BZIP2
```
### <font color=royalblue>GPG对称加解密</font>
基本用法
+ <font color=chocolate>加密操作</font>：`--symmetric 或 -c`
+ <font color=camel>解密操作</font>：`--decrypt 或 -d`

```bash
####定义一个测试文件test.txt
[root@node1 ~]$ echo "hello the world" > test.txt
####使用gpg -c 对文件进行加密，输入加密密码 123456【tab键切换，回车确定】
[root@node1 ~]$ gpg -c test.txt
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/1742681cfccd4ae49079a7ce9b6147d5.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

```bash
####加密完成后，会生成一个以.gpg结尾的加密文件
[root@node1 ~]$ ls -l test.txt.gpg
-rw-r--r-- 1 root root 63 8月 20 09:58 test.txt.gpg
###删除原始文件
[root@node1 ~]$ rm -rf test.txt
```

```bash
######使用gpg -d解密数据,将数据解密后放到/opt/test02.txt文件中
###解密密钥存放在/root/.gnupg/secring.gpg文件中
##加密和解密在同一台服务器上，有解密密钥的存在，不需要输入密码直接解密
[root@node1 ~]$ gpg -d test.txt.gpg > /opt/test02.txt
gpg: CAST5 加密过的数据
gpg: 以 1 个密码加密
hello the world
gpg: 警告：报文未受到完整的保护
###查看解密后的文件内容
[root@node1 ~]$ cat /opt/test02.txt
hello the world
```

```bash
#######在node1上加密文件，在node2上解密文件
[root@node1 ~]$ scp test.txt.gpg root@192.168.2.200:/root/
####客户端node2上查看加密数据
[root@node2 ~]$ ls -l test.txt.gpg
-rw-r--r-- 1 root root 63 8月 20 10:16 test.txt.gpg
######解密数据，存放到test.txt文件中
##根据提示需要输入密码 123456
[root@node2 ~]$ gpg -d test.txt.gpg > /root/test.txt
##查看解密后的数据
[root@node2 ~]$ ls -l test.txt
-rw-r--r-- 1 root root 16 8月 20 10:17 test.txt
[root@node2 ~]$ cat test.txt
hello the world
```

### <font color=tomato>GPG非对称加解密</font>
+ 基本过程
![在这里插入图片描述](https://img-blog.csdnimg.cn/243b4075ea054666844f5467b76b6b4c.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

+ 前期准备工作
  + UserB 创建密钥对：`--gen-key`
  + UserB 导出公钥：`--export、--armor 或 -a`
  + UserA 导入公钥：`--import`

```bash
[root@node1 ~]$ gpg --gen-key
gpg (GnuPG) 2.0.22; Copyright (C) 2013 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
请选择您要使用的密钥种类：
(1) RSA and RSA (default) #默认使用密钥类型
(2) DSA and Elgamal
(3) DSA (仅用于签名)
(4) RSA (仅用于签名)
您的选择？ #直接回车选择默认密钥类型
RSA 密钥长度应在 1024 位与 4096 位之间。
您想要用多大的密钥尺寸？(2048) #密钥长度，默认2048
您所要求的密钥尺寸是 2048 位
请设定这把密钥的有效期限。
0 = 密钥永不过期 #默认选项
<n> = 密钥在 n 天后过期 #3 代表3天后过期
<n>w = 密钥在 n 周后过期 #3w 代表3周后过期
<n>m = 密钥在 n 月后过期 #3m 代表3月后过期
<n>y = 密钥在 n 年后过期 #3y 代表3年后后期
密钥的有效期限是？(0) #回车，默认选择永不过期
密钥永远不会过期
以上正确吗？(y/n)y #选择y,确定以上选择
You need a user ID to identify your key; the software constructs the user ID
from the Real Name, Comment and Email Address in this form:
"Heinrich Heine (Der Dichter) <heinrichh@duesseldorf.de>"
真实姓名：userb #填写密钥名称
电子邮件地址：userb@tedu.cn #填写密钥邮箱
注释：test key #注释信息，任意填写
您选定了这个用户标识：
“userb (test key) <userb@tedu.cn>”
更改姓名(N)、注释(C)、电子邮件地址(E)或确定(O)/退出(Q)？ #大写O确定,回车
##这里输入的密码，是解密数据时需要输入的，选择“回车”，则代表解密不需要额外的密码验证，直接解密出
数据

```

```bash
#####生成密钥对时，需要大量的随机数,是从/dev/random获取的，如果无法很快生成密钥，ctrl+C 结束
命令，修改随机数生成器
###修改随机数的生成器为 /dev/urandom
[root@node1 ~]$ mv /dev/random /dev/random.bak
[root@node1 ~]$ ln -s /dev/urandom /dev/random
###根据上面的操作重新生成密钥对，如果终端异常，输入 reset 重置终端
[root@node1 ~]$ gpg --gen-key
####查看生成的密钥
[root@node1 ~]$ gpg --list-keys
/root/.gnupg/pubring.gpg
------------------------
pub 2048R/8B8D56D4 2020-08-20
uid userb (test key) <userb@tedu.cn>
sub 2048R/90116CE3 2020-08-20

```

```bash
####根据密钥名称userb导出公钥
[root@node1 ~]$ gpg -a --export userb > /tmp/userb.pub
###也可以根据密钥邮箱userb@tedu.cn导出公钥
[root@node1 ~]$ gpg -a --export userb@tedu.cn > /tmp/userb02.pub
#####查看导出的公钥userb.pub内容
[root@node1 ~]$ cat /tmp/userb.pub
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (GNU/Linux)
mQENBF895QwBCACwfDPG4vzoazhIGnLIghIN49ByACQG1YB2cHvoqITQ06KDve0p
EHDayr2mcGGwZbv96tp3HDIoS70QpbMdiqri5zD3Jnms2C37pfRmPDjUJ6eQQQJJ
......
#####查看导出的公钥userb02.pub内容
[root@node1 ~]$ cat /tmp/userb02.pub
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: GnuPG v2.0.22 (GNU/Linux)
mQENBF895QwBCACwfDPG4vzoazhIGnLIghIN49ByACQG1YB2cHvoqITQ06KDve0p
EHDayr2mcGGwZbv96tp3HDIoS70QpbMdiqri5zD3Jnms2C37pfRmPDjUJ6eQQQJJ
......


```

```bash
########客户机node2导入来自node1的公钥
####第一步：node1拷贝公钥到node2虚拟机中的/tmp目录下
[root@node1 ~]$ scp /tmp/userb.pub 192.168.2.200:/tmp/
####第二步：客户机client导入公钥/tmp/userb.pub
#首先，查看列表，是否有公钥
[root@node2 ~]$ gpg --list-keys
gpg: /root/.gnupg/trustdb.gpg：建立了信任度数据库
#然后，根据文件导入公钥
[root@node2 ~]$ gpg --import /tmp/userb.pub
gpg: 密钥 8B8D56D4：公钥“userb (test key) <userb@tedu.cn>”已导入
gpg: 合计被处理的数量：1
gpg: 已导入：1 (RSA: 1)
#最后，再次查看列表，新的公钥已经导入
[root@node2 ~]$ gpg --list-keys
/root/.gnupg/pubring.gpg
------------------------
pub 2048R/8B8D56D4 2020-08-20
uid userb (test key) <userb@tedu.cn>
sub 2048R/90116CE3 2020-08-20
```

+ <font color=plum>基本用法</font>
    + <font color=chocolate>加密操作：</font>`--encrypt 或 -e`
    + <font color=seagreen>指定目标用户：</font>`--recipient 或 -r`
    + <font color=blue>解密操作：</font>`--decrypt 或 -d`

```bash
######在客户机node2上生成一个测试文件clear.txt
[root@node2 ~]$ echo test > /root/clear.txt
#####-e 加密，-r 使用公钥userb对文件clear.txt进行加密
[root@node2 ~]$ gpg -e -r userb clear.txt
gpg: 90116CE3：没有证据表明这把密钥真的属于它所声称的持有者
pub 2048R/90116CE3 2020-08-20 userb (test key) <userb@tedu.cn>
主钥指纹： 0644 7DE7 C2A8 EE21 F41F 939B C53B 6BCA 8B8D 56D4
子钥指纹： 80B5 C5AB 7AA9 287C F411 AD7C 0105 14A9 9011 6CE3
这把密钥并不一定属于用户标识声称的那个人。如果您真的知道自
己在做什么，您可以在下一个问题回答 yes。
无论如何还是使用这把密钥吗？(y/N)y #y 确定使用公钥userb加密
#####加密数据生成，以后发送文件也是发送.gpg结尾的加密文件
#####加密文件发送过去以后，再通过私钥进行解密
[root@node2 ~]$ ls -l /root/clear.txt.gpg
-rw-r--r-- 1 root root 346 8月 20 11:10 /root/clear.txt.gpg
#######客户端client发送加密文件给svr7
[root@client ~]$ scp /root/clear.txt.gpg root@192.168.2.100:/root/
######-d 解密数据；-r 使用userb私钥解密，要解密文件为clear.txt.gpg
##解密后的数据存放到 clear.txt文件中
[root@node1 ~]$ gpg -d -r userb clear.txt.gpg > clear.txt
gpg: 由 2048 位的 RSA 密钥加密，钥匙号为 90116CE3、生成于 2020-08-20
“userb (test key) <userb@tedu.cn>”
##查看clear.txt中解密后的数据
[root@node1 ~]$ cat clear.txt
test
```


### <font color=plum>GPG软件签名与验证</font>

|**<font color=orange>软件签名与验证过程</font>**|
|--|
|软件官方以私钥对软件包执行数字签名|
|用户下载软件包、软件官方的公钥|
|以官方公钥验证软件包签名，确保数据来源正确|

>服务器使用私钥对发送出的所以数据文件进行签名；客户端可以使用公钥来对数据文件进行验证，查看其是否是有服务器发送的；如果是，则代表来源正确；如果不是，则数据文件不安全，删除

| **<font color=plum>GPG软件签名与验证</font>**|命令|
|--|--|
|为软件包建立签名文件|分离式签名：`--detach-sign 或 -b`|
|验证软件包签名|验证签名：`--verify`|
**<font color=plum>node1</font>**
```bash
####### -b 服务器node1对文件clear.txt进行签名
##签名后会生成一个签名文件
[root@node1 ~]$ gpg -b clear.txt
[root@node1 ~]$ ls -l clear.txt.sig
-rw-r--r-- 1 root root 287 8月 20 11:29 clear.txt.sig
######将数据文件clear.txt和签名文件clear.txt.sig发送给客户端client
[root@node1 ~]$ scp clear.txt clear.txt.sig root@192.168.2.200:/root/
```
**<font color=brown>node2</font>**
```bash
#########客户端node2通过 --verify 验证数据文件clear.txt的来源
##验证签名时，签名文件放到前面，数据文件跟在后面
[root@node2 ~]$ gpg --verify clear.txt.sig clear.txt
gpg: 于 2020年08月20日 星期四 11时29分15秒 CST 创建的签名，使用 RSA，钥匙号 8B8D56D4
gpg: 完好的签名，来自于“userb (test key) <userb@liruilong.cn>”
gpg: 警告：这把密钥未经受信任的签名认证！
gpg: 没有证据表明这个签名属于它所声称的持有者。
主钥指纹： 0644 7DE7 C2A8 EE21 F41F 939B C53B 6BCA 8B8D 56D4
```

# <font color=tomato>五、AIDE入侵检测</font>

## <font color=green>初始化系统</font>

###  <font color=seagreen>安装软件包</font>
AIDE(Advanced intrusion detection environment),该软件为一套入侵检测系统,配置yum源即可安装aide软件
```bash
[root@node1 ~]$ yum -y install aide
```
|**<font color=brown>aide原理：</font>**|
|--|
|在没有被人篡改之前对我们的计算机做一个全面的检查；|
|记录每个文件的状态(包括数据的时间，大小，权限，哈希值等)；|
|当计算机被入侵以后，aide对计算机做同样的校验，同样的检查；|
|最后，通过对比入侵之前检查的结果和入侵之后的检查结果，找到哪些数据发生了哪些变化|

### <font color=red>修改配置文件</font>

**<font color=plum>AIDE默认配置文件为`/etc/aide.conf`</font>**嗯，这个系统不一样会有出入

![在这里插入图片描述](https://img-blog.csdnimg.cn/e45260b6b64147edb4b697dc090216a2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)

```bash

[root@node1 ~]$ vim /etc/aide.conf
1 # Example configuration file for AIDE.
2
3 @@define DBDIR /var/lib/aide #aide对数据校验后，结果文件的存放位置
4 @@define LOGDIR /var/log/aide #aide日志文件的目录
......
###当系统被入侵后，需要将入侵前的数据aide.db.new.gz，更名为aide.db.gz
##系统被入侵后，aide重新校验时会将新生成的校验结果aide.db.new.gz和以前校验结果aide.db.gz进行对比
7 database=file:@@{DBDIR}/aide.db.gz
......
#####定义校验完数据后，将校验结果存放到哪个目录下的哪个文件中
##默认存放到/var/lib/aide目录下；保存的文件名问aide.db.new.gz
12 database_out=file:@@{DBDIR}/aide.db.new.gz
......
28 #p: permissions #校验文件的权限是否被修改
29 #i: inode: #校验文件的inode节点是否被修改
30 #n: number of links #校验文件的链接数量是否增多或减少
31 #u: user #校验文件的所有者是否被修改
32 #g: group #校验文件的所属组否是被修改
33 #s: size #校验文件的大小是否变化
......
42 #md5: md5 checksum #校验文件md5生成的哈希值
43 #sha1: sha1 checksum #校验文件sha1生成的哈希值
44 #sha256: sha256 checksum #校验文件sha256生成的哈希值
......
######定义变量，变量中包含以上的所有校验
54 FIPSR = p+i+n+u+g+s+m+c+acl+selinux+xattrs+sha256
......
71 NORMAL = sha256 #定义变量，校验哈希值，会调用
```

### <font color=yellowgreen>修改配置文件</font>

**<font color=plum>校验的目录设置</font>**
```bash
[root@node1 ~]$ vim /etc/aide.conf
......
99 /boot/ NORMAL #对/boot目录，进行sha256校验,NORMAL为变量
100 /bin/ NORMAL #同上
101 /sbin/ NORMAL
102 /lib/ NORMAL
103 /lib64/ NORMAL
104 /opt/ NORMAL
......
112 !/usr/src/ #! 取反，设置不需要校验的目录
113 !/usr/tmp/

```

**<font color=royalblue>第一步：批量注释掉所有的校验目录</font>**
||
|--|
|步骤一： vim 打开文件/etc/aide.conf|
|步骤二： 末行模式下，光标移动到99行/boot/的首字母上【末行模式 :99】|
|步骤三： Ctrl + v 进入到可视化模式，按键盘的下键，拉到最后一行|
|步骤四： Shift + i 进入到插入模式,插入注释符号#;|
|步骤五： 按键盘左上角的Esc键，批量注释完成|

**<font color=yellowgreen>第二步：插入一行校验目录</font>**
```bash
[root@node1 ~]$ vim /etc/aide.conf
......
93 DATAONLY = p+n+u+g+s+acl+selinux+xattrs+sha256
......
######对/tmp/目录进行校验，校验选项由变量DATAONLY决定
98 /tmp/ DATAONLY
99 #/boot/ CONTENT_EX
100 #/bin/ CONTENT_EX
......

```
### <font color=blue>初始化检查</font>

**<font color=seagreen>在没有被攻击入侵前,根据配置文件，对数据进行校验操作</font>**
```bash
#####对/tmp/目录进行校验操作
[root@node1 ~]$ aide --init
AIDE, version 0.15.1
### AIDE database at /var/lib/aide/aide.db.new.gz initialized.
```
```bash
######查看生成的校验结果数据
#aide每次校验,生成的校验结果文件名都相同
[root@node1 ~]$ ls /var/lib/aide/aide.db.new.gz
/var/lib/aide/aide.db.new.gz

```
 ### <font color=tomato>备份数据库</font>

**<font color=green>在被入侵前，将校验的数据库文件备份到安全的地方 如，优盘、光盘、移动硬盘、网络存储</font>**

```bash
######将校验的结果数据文件备份到/mnt目录下
[root@node1 ~]$ mv /var/lib/aide/aide.db.new.gz /mnt/
```
## <font color=yellowgreen>入侵检查</font>

### <font color=green>执行入侵检查</font>
**<font color=yellowgreen>将之前备份的校验数据库文件还原</font>**
```bash
#####将之前备份的校验数据库文件拷贝会 /var/lib/aide
##配置文件中已经定义了新生成的数据库文件会和aide.db.gz进行比对，所以需要改名
[root@node1 ~]$ cp /mnt/aide.db.new.gz /var/lib/aide/aide.db.gz
```
**<font color=green>根据数据库执行入侵检测</font>**
```bash
#####没做任何改变前，进行比对，结果正常
[root@node1 ~]$ aide --check
AIDE, version 0.15.1
### All files match AIDE database. Looks okay!
######对/tmp目录下的文件进行多次修改，重新使用aide进行校验比对
[root@node1 ~]$ echo aide >> /tmp/test.txt
[root@node1 ~]$ rm -rf /tmp/userb.pub
[root@node1 ~]$ echo "xxx" > /tmp/x.txt
[root@node1 ~]$ echo "ccc" > /tmp/c.txt
#####没做任何改变前，进行比对,会列出详细的文件修改信息
[root@node1 ~]$ aide --check
AIDE 0.15.1 found differences between database and filesystem!!
Start timestamp: 2020-08-20 13:06:18
Summary:
Total number of files: 17
Added files: 2
Removed files: 1
Changed files: 1
---------------------------------------------------
Added files:
---------------------------------------------------
added: /tmp/c.txt
added: /tmp/x.txt
---------------------------------------------------
Removed files:
---------------------------------------------------
removed: /tmp/userb.pub
---------------------------------------------------
Changed files:
---------------------------------------------------
changed: /tmp/test.txt
---------------------------------------------------
Detailed information about changes:
---------------------------------------------------
File: /tmp/test.txt
Size : 0 , 5
SHA256 : 47DEQpj8HBSa+/TImW+5JCeuQeRkm5NM , OP7pCdRCH5j0ENIbJ9BV1SZScyCe+FEY
```
# <font color=red>六，端口扫描</font>

## <font color=chocolate>为什么需要扫描？</font>

>以获取一些公开/非公开信息为目的,检测潜在的风险,查找可攻击目标,收集设备/主机/系统/软件信息,发现可利用的安全漏洞

### <font color=purple>扫描方式及工具</font>

|**<font color=purple>典型的扫描方式</font>**|
|--|
|`Scan`，主动探测(主动扫描百度或天猫的服务器)|
|`Sniff`，被动监听/嗅探 (A机器给B机器发送数据包时，B可以进行监听数据包)|
|`Capture`，数据包捕获(抓包)|

>数据包捕获 通过地址伪装，欺骗，把数据诱导到我们的主机上，对数据抓包；在交换机和路由器上也可以抓包；

### <font color=plum>常见的安全分析工具</font>
`扫描器`：NMAP ——> 主动去扫描其他的主机

`协议分析`：tcpdump、WireShark ——> 网络上抓包，截取数据

## <font color=brown>NMAP扫描</font>

NMAP一款强大的网络探测利器工具，支持多种探测技术
+ ping 扫描
+ 多端口扫描
+ TCP/IP指纹检验
.....

基本用法 :` nmap [扫描类型] [选项] <扫描目标 ...>`

常用的扫描类型
+ -sS，TCP SYN扫描(半开)
+ -sT，TCP 连接扫描(全开)
+ -sU，UDP扫描
+ -sP，ICMP扫描
+ -A，目标系统全面分析

![在这里插入图片描述](https://img-blog.csdnimg.cn/8a69411ce37c4b98bb4df1873885c552.png)

>**<font color=amber>TCP SYN扫描(全开):</font>**
#左边图：要检查目标主机的80端口是否打开，扫描时，A主机向目标主机的80端口发送请求建立连接的
请求syn,目标主机回应syn和ack,A主机也回应一个ack，连接建立，目标主机的80端口是打开的；

>**<font color=purple>TCP SYN扫描(半开):</font>**
#右边图：要检查目标主机的80端口是否打开，扫描时，A主机向目标主机的80端口发送请求建立连接的
请求syn,目标主机回应syn和ack, 目标主机回应了，代表目标主机的80端口是打开的，A主机不再回应
ack，节省一个ack回应的资源；
#因为并不需要建立连接，只要目标主机回应即可，当扫描更多主机时会节省更多的系统资源；

### <font color=brown>NMAP应用示例</font>

**<font color=red>检查哪些主机开启FTP、SSH服务端</font>**
嗯，这个代码不对应该，这里扫描的的是53端口
```bash
#####扫描查看UDP协议的端口状态
##-n 取消客户端的反向域名解析; -p 指定扫描的端口号
[root@node1 ~]$ nmap -n -sU 192.168.2.200 -p 53
Starting Nmap 6.40 ( http://nmap.org ) at 2020-08-20 15:05 CST
Nmap scan report for 192.168.2.200
Host is up (0.0012s latency). #up状态，网络延迟为0.0012s
PORT STATE SERVICE
53/udp closed domain ##53端口，处于关闭状态
MAC Address: 00:0C:29:3D:40:D8 (VMware)
Nmap done: 1 IP address (1 host up) scanned in 0.04 seconds
```
**<font color=orange>扫描查看UDP协议53-55的端口状态</font>**
```bash
#####扫描查看UDP协议53-55的端口状态
##-n 取消客户端的反向域名解析; -p 指定扫描的端口号
[root@node1 ~]$ nmap -n -sU 192.168.2.200 -p 53-55
Starting Nmap 6.40 ( http://nmap.org ) at 2020-08-20 15:07 CST
Nmap scan report for 192.168.2.200
Host is up (0.00053s latency). #up状态，网络延迟为0.00053s
PORT STATE SERVICE
53/udp closed domain #53端口，关闭
54/udp closed xns-ch #53端口，关闭
55/udp closed isi-gl #53端口，关闭
MAC Address: 00:0C:29:3D:40:D8 (VMware)
Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds
```
**<font color=green>扫描查看TCP协议53-55和25的端口状态</font>**
```bash
#####扫描查看TCP协议53-55和25的端口状态
###对于不连续的端口号用逗号【,】分隔
##-n 取消客户端的反向域名解析; -p 指定要扫描的端口号
[root@node1 ~]$ nmap -n 192.168.2.200 -p 53-55,25
Starting Nmap 6.40 ( http://nmap.org ) at 2020-08-20 15:10 CST
Nmap scan report for 192.168.2.200
Host is up (0.00050s latency). #up状态，网络延迟为0.00050s
PORT STATE SERVICE
25/udp closed smtp #25端口，关闭
53/udp closed domain #53端口，关闭
54/udp closed xns-ch #54端口，关闭
55/udp closed isi-gl #55端口，关闭
MAC Address: 00:0C:29:3D:40:D8 (VMware)
Nmap done: 1 IP address (1 host up) scanned in 0.05 seconds
```
**<font color=plum>扫描http端口</font>**
```bash
#######client上安装httpd服务，开启80端口
[root@node2 ~]$ yum -y install httpd
[root@node2 ~]$ systemctl start httpd
######在node1扫描查看node2上的80端口的状态
[root@node1 ~]$ nmap -n 192.168.2.200 -p 80
Starting Nmap 6.40 ( http://nmap.org ) at 2020-08-20 15:22 CST
Nmap scan report for 192.168.2.200
Host is up (0.00034s latency).
PORT STATE SERVICE
80/tcp open http #打开，服务http
MAC Address: 00:0C:29:3D:40:D8 (VMware)
Nmap done: 1 IP address (1 host up) scanned in 0.06 seconds
#########扫描查看网络中所有2.0网段上的80端口的状态
[root@node1 ~]$ nmap -n 192.168.2.0/24 -p 80
Starting Nmap 6.40 ( http://nmap.org ) at 2020-08-20 15:24 CST
Nmap scan report for 192.168.2.200 #2.200主机
Host is up (0.00030s latency).
PORT STATE SERVICE
80/tcp open http #打开，服务http
MAC Address: 00:0C:29:3D:40:D8 (VMware)
Nmap scan report for 192.168.2.254 #2.254主机
Host is up (0.00030s latency).
PORT STATE SERVICE
80/tcp closed http #打开，服务http
MAC Address: 00:50:56:C0:00:02 (VMware)
Nmap scan report for 192.168.2.100 #2.100主机
Host is up (0.000032s latency).
PORT STATE SERVICE
80/tcp closed http #打开，服务http
Nmap done: 256 IP addresses (3 hosts up) scanned in 4.74 seconds
```

**<font color=orange>检查目标主机的存活状态(是否可ping通)</font>**
```bash
#####使用ping扫描整个2.0网段的主机，查看是否开机
[root@node1 ~]$ nmap -n -sP 192.168.2.0/24
Starting Nmap 6.40 ( http://nmap.org ) at 2020-08-20 15:28 CST
Nmap scan report for 192.168.2.200
Host is up (0.00011s latency). #2.200开机
MAC Address: 00:0C:29:3D:40:D8 (VMware)
Nmap scan report for 192.168.2.254
Host is up (0.00027s latency). #2.254开机
MAC Address: 00:50:56:C0:00:02 (VMware)
Nmap scan report for 192.168.2.100 #2.100开机
Host is up.
Nmap done: 256 IP addresses (3 hosts up) scanned in 5.69 seconds
```

**<font color=orange>复合扫描：检查操作系统指纹、软件版本等信息</font>**

```bash
#######全面扫描整个目标主机
[root@node1 ~]$ nmap -n -A 192.168.2.200
Starting Nmap 6.40 ( http://nmap.org ) at 2020-08-20 15:31 CST
Nmap scan report for 192.168.2.200
Host is up (0.00079s latency).
Not shown: 998 closed ports
###目标主机开启了22端口，使用的是ssh服务，使用软件为OpenSSH 7.4
22/tcp open ssh OpenSSH 7.4 (protocol 2.0)
| ssh-hostkey: 2048 37:68:ae:d7:23:48:16:74:ed:c9:d8:de:44:8a:b5:50 (RSA)
|_256 c7:14:c1:8f:1a:1b:64:e8:c1:06:56:74:ec:9a:50:29 (ECDSA)
###目标主机开启了80端口，使用的是http服务，使用软件为 Apache httpd 2.4.6
##目标主机的操作系统为CentOS
80/tcp open http Apache httpd 2.4.6 ((CentOS))
| http-methods: Potentially risky methods: TRACE
|_See http://nmap.org/nsedoc/scripts/http-methods.html
|_http-title: Apache HTTP Server Test Page powered by CentOS
MAC Address: 00:0C:29:3D:40:D8 (VMware)
No exact OS matches for host (If you know what OS is running on it, see
http://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=6.40%E=4%D=8/20%OT=22%CT=1%CU=43694%PV=Y%DS=1%DC=D%G=Y%M=000C29%T
###########目标主机使用的是红帽的linux系统
OS:M=5F3E26E1%P=x86_64-redhat-linux-gnu)SEQ(SP=102%GCD=1%ISR=10E%TI=Z%CI=I%
OS:TS=A)SEQ(SP=102%GCD=1%ISR=10D%TI=Z%TS=A)SEQ(SP=102%GCD=1%ISR=10D%TI=Z%II
OS:=I%TS=A)OPS(O1=M5B4ST11NW7%O2=M5B4ST11NW7%O3=M5B4NNT11NW7%O4=M5B4ST11NW7
OS:%O5=M5B4ST11NW7%O6=M5B4ST11)WIN(W1=7120%W2=7120%W3=7120%W4=7120%W5=7120%
OS:W6=7120)ECN(R=Y%DF=Y%T=40%W=7210%O=M5B4NNSNW7%CC=Y%Q=)T1(R=Y%DF=Y%T=40%S
OS:=O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%R
OS:D=0%Q=)T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W=
OS:0%S=A%A=Z%F=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U
OS:1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DF
OS:I=N%T=40%CD=S)
####连接到目标主机所经过的路由器跳转的数量
##同一网段下，直连，为1
Network Distance: 1 hop
TRACEROUTE
HOP RTT ADDRESS
1 0.79 ms 192.168.2.200 ##网络延迟为0.79ms
OS and Service detection performed. Please report any incorrect results at
http://nmap.org/submit/ .
######整个检测一共花费了19.51秒
Nmap done: 1 IP address (1 host up) scanned in 19.51 seconds
```
**<font color=yellowgreen>嗯，这是我扫描自己的win10的结果</font>**
```bash
┌──(liruilong㉿Liruilong)-[/mnt/c/Users/lenovo]
└─$ nmap -n -A 172.30.0.1
Starting Nmap 7.91 ( https://nmap.org ) at 2021-09-27 20:41 CST
Stats: 0:00:50 elapsed; 0 hosts completed (1 up), 1 undergoing Service Scan
Service scan Timing: About 80.00% done; ETC: 20:42 (0:00:12 remaining)
Nmap scan report for 172.30.0.1
Host is up (0.87s latency).
Not shown: 990 closed ports
PORT     STATE SERVICE         VERSION
135/tcp  open  msrpc           Microsoft Windows RPC
139/tcp  open  netbios-ssn     Microsoft Windows netbios-ssn
443/tcp  open  ssl/https       VMware Workstation SOAP API 16.1.2
| fingerprint-strings:
|   FourOhFourRequest:
|     HTTP/1.1 404 Not Found
|     Date: Mon, 27 Sep 2021 12:41:49 GMT
|     Connection: close
|     Content-Security-Policy: block-all-mixed-content
|     Content-Type: text/plain; charset=utf-8
|     Strict-Transport-Security: max-age=31536000
|     X-Content-Type-Options: nosniff
|     X-Frame-Options: DENY
|     X-XSS-Protection: 1
|     Content-Length: 0
|   GetRequest:
|     HTTP/1.1 403 Forbidden
|     Date: Mon, 27 Sep 2021 12:41:49 GMT
|     Connection: close
|     Content-Security-Policy: block-all-mixed-content
|     Content-Type: text/plain; charset=utf-8
|     Strict-Transport-Security: max-age=31536000
|     X-Content-Type-Options: nosniff
|     X-Frame-Options: DENY
|     X-XSS-Protection: 1
|     Content-Length: 0
|   HTTPOptions:
|     HTTP/1.1 501 Not Implemented
|     Date: Mon, 27 Sep 2021 12:41:49 GMT
|     Connection: close
|     Content-Security-Policy: block-all-mixed-content
|     Content-Type: text/plain; charset=utf-8
|     Strict-Transport-Security: max-age=31536000
|     X-Content-Type-Options: nosniff
|     X-Frame-Options: DENY
|     X-XSS-Protection: 1
|     Content-Length: 0
|   RTSPRequest:
|     HTTP/1.1 400 Bad Request
|     Date: Mon, 27 Sep 2021 12:41:59 GMT
|     Connection: close
|     Content-Type: text/html
|     Content-Length: 50
|     <HTML><BODY><H1>400 Bad Request</H1></BODY></HTML>
|   SIPOptions:
|     HTTP/1.1 400 Bad Request
|     Date: Mon, 27 Sep 2021 12:43:06 GMT
|     Connection: close
|     Content-Type: text/html
|     Content-Length: 50
|_    <HTML><BODY><H1>400 Bad Request</H1></BODY></HTML>
|_http-title: Site doesn't have a title (text/plain; charset=utf-8).
| ssl-cert: Subject: commonName=VMware/countryName=US
| Not valid before: 2021-02-17T10:21:06
|_Not valid after:  2022-02-17T10:21:06
|_ssl-date: TLS randomness does not represent time
| vmware-version:
|   Server version: VMware Workstation 16.1.2
|   Build: 17966106
|   Locale version: INTL
|   OS type: win32-x86
|_  Product Line ID: ws
445/tcp  open  microsoft-ds?
903/tcp  open  ssl/vmware-auth VMware Authentication Daemon 1.10 (Uses VNC, SOAP)
1027/tcp open  msrpc           Microsoft Windows RPC
1057/tcp open  msrpc           Microsoft Windows RPC
3306/tcp open  mysql           MySQL (unauthorized)
3389/tcp open  ms-wbt-server   Microsoft Terminal Services
| rdp-ntlm-info:
|   Target_Name: LIRUILONG
|   NetBIOS_Domain_Name: LIRUILONG
|   NetBIOS_Computer_Name: LIRUILONG
|   DNS_Domain_Name: Liruilong
|   DNS_Computer_Name: Liruilong
|   Product_Version: 10.0.19041
|_  System_Time: 2021-09-27T12:43:57+00:00
| ssl-cert: Subject: commonName=Liruilong
| Not valid before: 2021-09-07T13:31:59
|_Not valid after:  2022-03-09T13:31:59
|_ssl-date: 2021-09-27T12:44:02+00:00; 0s from scanner time.
4000/tcp open  remoteanything?
| fingerprint-strings:
|   GetRequest:
|     HTTP/1.1 200 OK
|     X-Powered-By: Hexo
|     Content-Type: text/html
|     Date: Mon, 27 Sep 2021 12:41:42 GMT
|     Connection: close
|     <!doctype html>
|     <html lang="zh"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"><meta><title>
|     </title><link rel="manifest" href="/manifest.json"><meta name="application-name" content="
|     "><meta name="msapplication-TileImage" content="https://cdn.jsdelivr.net/gh/removeif/removeif-demo@latest/img/favicon.png"><meta name="apple-mobile-web-app-capable" content="yes"><meta name="apple-mobile-web-app-title" content="
|     "><meta name="apple-mobile-web-app-status-bar-style" content="default"><meta property="og:type" content="blog"><meta property="og:title" content="
|     "><meta property="og:url" content="https://removeif.github.io/"><meta property="og:site_name" cont
|   HTTPOptions:
|     HTTP/1.1 404 Not Found
|     X-Powered-By: Hexo
|     Content-Security-Policy: default-src 'none'
|     X-Content-Type-Options: nosniff
|     Content-Type: text/html; charset=utf-8
|     Content-Length: 143
|     Date: Mon, 27 Sep 2021 12:41:47 GMT
|     Connection: close
|     <!DOCTYPE html>
|     <html lang="en">
|     <head>
|     <meta charset="utf-8">
|     <title>Error</title>
|     </head>
|     <body>
|     <pre>Cannot OPTIONS /</pre>
|     </body>
|     </html>
|   NoMachine, RPCCheck, RTSPRequest:
|     HTTP/1.1 400 Bad Request
|_    Connection: close
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port4000-TCP:V=7.91%I=7%D=9/27%Time=6151BC06%P=x86_64-pc-linux-gnu%r(Ge
SF:tRequest,3890,"HTTP/1\.1\x20200\x20OK\r\nX-Powered-By:\x20Hexo\r\nConte
SF:nt-Type:\x20text/html\r\nDate:\x20Mon,\x2027\x20Sep\x202021\x2012:41:42
SF:\x20GMT\r\nConnection:\x20close\r\n\r\n<!doctype\x20html>\n<html\x20lan
SF:g=\"zh\"><head><meta\x20charset=\"utf-8\"><meta\x20name=\"viewport\"\x2
SF:0content=\"width=device-width,\x20initial-scale=1,\x20maximum-scale=1\"
SF:><meta><title>\xe5\xb1\xb1\xe6\xb2\xb3\xe5\xb7\xb2\xe6\x97\xa0\xe6\x81\
SF:x99</title><link\x20rel=\"manifest\"\x20href=\"/manifest\.json\"><meta\
SF:x20name=\"application-name\"\x20content=\"\xe5\xb1\xb1\xe6\xb2\xb3\xe5\
SF:xb7\xb2\xe6\x97\xa0\xe6\x81\x99\"><meta\x20name=\"msapplication-TileIma
SF:ge\"\x20content=\"https://cdn\.jsdelivr\.net/gh/removeif/removeif-demo@
SF:latest/img/favicon\.png\"><meta\x20name=\"apple-mobile-web-app-capable\
SF:"\x20content=\"yes\"><meta\x20name=\"apple-mobile-web-app-title\"\x20co
SF:ntent=\"\xe5\xb1\xb1\xe6\xb2\xb3\xe5\xb7\xb2\xe6\x97\xa0\xe6\x81\x99\">
SF:<meta\x20name=\"apple-mobile-web-app-status-bar-style\"\x20content=\"de
SF:fault\"><meta\x20property=\"og:type\"\x20content=\"blog\"><meta\x20prop
SF:erty=\"og:title\"\x20content=\"\xe5\xb1\xb1\xe6\xb2\xb3\xe5\xb7\xb2\xe6
SF:\x97\xa0\xe6\x81\x99\"><meta\x20property=\"og:url\"\x20content=\"https:
SF://removeif\.github\.io/\"><meta\x20property=\"og:site_name\"\x20cont")%
SF:r(NoMachine,2F,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nConnection:\x20cl
SF:ose\r\n\r\n")%r(HTTPOptions,180,"HTTP/1\.1\x20404\x20Not\x20Found\r\nX-
SF:Powered-By:\x20Hexo\r\nContent-Security-Policy:\x20default-src\x20'none
SF:'\r\nX-Content-Type-Options:\x20nosniff\r\nContent-Type:\x20text/html;\
SF:x20charset=utf-8\r\nContent-Length:\x20143\r\nDate:\x20Mon,\x2027\x20Se
SF:p\x202021\x2012:41:47\x20GMT\r\nConnection:\x20close\r\n\r\n<!DOCTYPE\x
SF:20html>\n<html\x20lang=\"en\">\n<head>\n<meta\x20charset=\"utf-8\">\n<t
SF:itle>Error</title>\n</head>\n<body>\n<pre>Cannot\x20OPTIONS\x20/</pre>\
SF:n</body>\n</html>\n")%r(RTSPRequest,2F,"HTTP/1\.1\x20400\x20Bad\x20Requ
SF:est\r\nConnection:\x20close\r\n\r\n")%r(RPCCheck,2F,"HTTP/1\.1\x20400\x
SF:20Bad\x20Request\r\nConnection:\x20close\r\n\r\n");
Service Info: OS: Windows; CPE: cpe:/o:microsoft:windows, cpe:/o:vmware:Workstation:16.1.2

Host script results:
|_nbstat: NetBIOS name: LIRUILONG, NetBIOS user: <unknown>, NetBIOS MAC: 00:15:5d:1d:9f:bd (Microsoft)
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2021-09-27T12:43:57
|_  start_date: N/A

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 148.10 seconds

┌──(liruilong㉿Liruilong)-[/mnt/c/Users/lenovo]
└─$
```

