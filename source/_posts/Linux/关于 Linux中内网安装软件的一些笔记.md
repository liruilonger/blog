---
title: 关于 Linux中内网安装软件的一些笔记
tags:
  - Linux
  - rpm
categories:
  - Linux
toc: true
recommend: 1
keywords: Linux
uniqueId: '2021-10-30 03:33:37/关于 Linux中内网安装软件的一些笔记.html'
mathJax: false
date: 2021-10-30 11:33:37
thumbnail:
---
**<font color="009688"> 可事实是，唯恐暴露才华不足的卑怯的畏惧，和厌恶钻研刻苦的惰怠，就是我的全部了。  ——中岛敦《山月记》**</font>
<!-- more -->
## 写在前面
***
+ 对于可以连接外网的服务器装软件，只要配置yum源就可以随便使用了
+ 但是对于内网来说，不能连接外网，一般情况下，管控平台只有上传的权限。尤其一些涉密的岗位，比如电力,电信,军工之类的。
+ 今天和小伙伴聊聊内网服务器如何装软件的问题。

**<font color="009688"> 可事实是，唯恐暴露才华不足的卑怯的畏惧，和厌恶钻研刻苦的惰怠，就是我的全部了。  ——中岛敦《山月记》**</font>
 ***



## <font color=green>方法一、直接下载安装包</font>

**<font color=camel>找一台有网机器安装 Nginx需要的软件包(尽量同版本的操作系统，最小化安装)</font>**
```bash
┌──[root@liruilongs.github.io]-[~]  
└─$ yum -y install nginx --downloadonly --downloaddir=/root/soft  #把Nginx需要的软件包下载到 /root/soft 下
```
**<font color=purple>检查一下</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cd /root/soft/
┌──[root@liruilongs.github.io]-[~/soft]
└─$ ls
nginx-1.20.1-9.el7.x86_64.rpm  nginx-filesystem-1.20.1-9.el7.noarch.rpm
┌──[root@liruilongs.github.io]-[~/soft]
└─$ 
```
**<font color=yellowgreen>把整个文件夹打包拷贝到内网环境，然后执行   `rpm -ivh /xx/* ` 命令安装所有依赖包</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ rpm -ivh /root/soft/*
warning: /root/soft/nginx-1.20.1-9.el7.x86_64.rpm: Header V4 RSA/SHA256 Signature, key ID 352c64e5: NOKEY
Preparing...                          ################################# [100%]
Updating / installing...
   1:nginx-filesystem-1:1.20.1-9.el7  ################################# [ 50%]
   2:nginx-1:1.20.1-9.el7             ################################# [100%]
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl start nginx
┌──[root@liruilongs.github.io]-[~]
└─$ curl http://127.0.0.1:80
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Welcome to CentOS</title>
  <style rel="stylesheet" type="text/css">

```
**<font color=brown>如果内网环境有 `createrepo`命令的话，把下载的东西拷贝过去，可以自定义yum源，通过yum的方式安装</font>**
```bash

┌──[root@liruilongs.github.io]-[~]
└─$ createrepo  -v /root/soft/
Spawning worker 0 with 1 pkgs
Spawning worker 1 with 1 pkgs
Worker 0: reading nginx-1.20.1-9.el7.x86_64.rpm
Worker 1: reading nginx-filesystem-1.20.1-9.el7.noarch.rpm
Workers Finished
Saving Primary metadata
Saving file lists metadata
Saving other metadata
Generating sqlite DBs
Starting other db creation: Tue Nov  2 02:41:26 2021

▽
[nginx]
Ending other db creation: Tue Nov  2 02:41:26 2021
Starting filelists db creation: Tue Nov  2 02:41:26 2021
Ending filelists db creation: Tue Nov  2 02:41:26 2021
Starting primary db creation: Tue Nov  2 02:41:26 2021
Ending primary db creation: Tue Nov  2 02:41:26 2021
Sqlite DBs complete
┌──[root@liruilongs.github.io]-[~]
└─$ cd /root/soft/
┌──[root@liruilongs.github.io]-[~/soft]
└─$ ls
nginx-1.20.1-9.el7.x86_64.rpm  nginx-filesystem-1.20.1-9.el7.noarch.rpm  repodata
┌──[root@liruilongs.github.io]-[~/soft]
└─$ cd repodata/;ls
2a85d8bedd0e987fe0c492840e8d9e7194f1da556db1282b6b731cc0c6978ded-primary.sqlite.bz2
8fe41a398aa040ec1b69ea2d54fae6c91dda6964a51a737b9becfa05bb7504f4-other.sqlite.bz2
a8c56a126109fae47bdd4dfa6e33e8575bd9e660fc6826f6623fb8d08f1ce293-filelists.xml.gz
b5c57aa7aecbcfe4826a9e4c4dcb3c05193b0ed64e6858e4e755bc785003a009-primary.xml.gz
dbc9a1a851b14aeb972e05dac2b17a896848993ebbd494a01b566c75ba5d0ef8-other.xml.gz
ed1fa31928cf100748169e2b5ff1cb354b1c45a0b860f805830eb679872d38fd-filelists.sqlite.bz2
repomd.xml
┌──[root@liruilongs.github.io]-[~/soft/repodata]
└─$ cd ..;createrepo --update  ./
Saving Primary metadata
Saving file lists metadata
Saving other metadata
Generating sqlite DBs
Sqlite DBs complete
┌──[root@liruilongs.github.io]-[~/soft]
└─$ vim nginx.repo
┌──[root@liruilongs.github.io]-[~/soft]
└─$ cat nginx
cat: nginx: No such file or directory
┌──[root@liruilongs.github.io]-[~/soft]
└─$ cat nginx.repo
[nginx]
name=nginx
baseurl=file://root/soft
enabled=1
gpgcheck=0
priority=1
┌──[root@liruilongs.github.io]-[~/soft]
└─$
```

## <font color=yellowgreen>方法二、通过IOS挂载配置</font>

**<font color=orange>`iso`挂载的方式，`iso`挂载方式这里可以使用不同的协议，但是内网，所以我们只能用`file`协议，前提将有包的`IOS`镜像拷贝到要装包的机器。当然，如果这个机器所在内网的其他机器有相关的包，可以使用`http`，`ftp`协议。</font>**


挂载本地镜像文件。正常IOS文件一般会放到dev/ 文件夹下，需要把相关的文件挂载到指定的目录下，
```bash
[root@liruilongs.github.io ~]# mkdir /centos7 #创建挂载点
[root@liruilongs.github.io yum.repos.d]# mount /dev/CentOS-7-x86_64-DVD-1810.iso /centos7/ #挂载镜像光盘
[root@liruilongs.github.io yum.repos.d]# ls /centos7/ #验证挂载结果
```
**<font color=green>将yum的原本配置备份</font>**
```bash
[root@liruilongs.github.io ~]# cd /etc/yum.repos.d/ #进入yum的repo文件目录下
[root@liruilongs.github.io yum.repos.d]# mkdir oldrepo #创建目录
[root@liruilongs.github.io yum.repos.d]# ls
[root@liruilongs.github.io yum.repos.d]# mv CentOS-* oldrepo/ #将所有的.repo的文件移动到oldrepo目录下
[root@liruilongs.github.io yum.repos.d]# ls
```
**<font color=brown>第三步：配置本地yum源</font>**
```bash
[root@liruilongs.github.io ~]# vim /etc/yum.repos.d/centos7.repo #创建一个新的repo文件
[centos] #自定义名字，具有唯一性，随便定义
name=Centos7 #对软件源的描述信息
baseurl=file:///centos7 #指定yum服务端的访问路径
gpgcheck=0 # 0为不检测，1为检测，要更改为0，检测只是检测红帽官方打包的rpm包
enabled=1 #是否立即生效，1为是
[root@liruilongs.github.io ~]# yum clean all #清空客户端下的yum清单列表
[root@liruilongs.github.io ~]# yum repolist #重新加载服务端的清单列表
```
**<font color=royalblue>这样我们通过iso装包就配置好啦。之后就可以通过yum使用了</font>**



