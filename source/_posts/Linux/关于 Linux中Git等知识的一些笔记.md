---
title: 关于 Linux中Git等知识的一些笔记
tags:
  - Git
  - Linux
categories:
  - Git
toc: true
recommend: 1
keywords: Git
uniqueId: '2021-08-10 12:00:14/关于 Linux中Git等知识的一些笔记.html'
mathJax: false
date: 2021-08-10 20:00:14
thumbnail:
---
**<font color="009688"> 似水流年，转眼到了不惑之年。我和大家一样，对周围的事逐渐司空见惯。过去的事过去了，未过去的事也不能叫我惊讶。--------王小波**</font>


<!-- more -->

## 写在前面
***
+ 嗯，`Git`一般都用平台上的，自己用`Githup`，`Gitee`，公司用的`Gitlab`,服务端的没搞过，基本用的都是客户端。
+ 之前实习分了一个项目做组长，所以自己在阿里云上面搭了`SVN`的服务端，折腾了大半夜，但是后来也没怎么用。感兴趣小伙伴可以去围观:[阿里云Linux下搭建SVN服务端](https://www.cnblogs.com/liruilong/p/12459835.html)
+ 想把`Linux`的东西总结一下，然后告一段落，学学微服务，python，考一个华为RS认证。
+ 笔记基本是听课整理的笔记，不足之处小伙伴留言。另准备自己在服务器上搭一个Gitlab.


**<font color="009688"> 似水流年，转眼到了不惑之年。我和大家一样，对周围的事逐渐司空见惯。过去的事过去了，未过去的事也不能叫我惊讶。--------王小波**</font>

 ***

# <font color=amber>一、Git概述</font>
### <font color=chocolate>版本控制</font>
版本控制系统就是一个控制文件版本的系统
+ `版本仓库`是版本控制的核心
+ 任意客户端可以连接使用版本仓库
### <font color=tomato>主流的版本控制系统</font>
+ `集中式`版本控制系统
   + 所有客户端`共享一个仓库`
   + 所有操作都需要联网
   + 代表软件：`Subversion`
+ `分布式`版本控制系统
  + 每个客户端都有一个`仓库`的`完整克隆`
  + 支持断网操作
  + 代表软件：`Git`
  
![](https://img-blog.csdnimg.cn/6db81a375db64633965818ab073c00e7.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
### <font color=chocolate>集中式版本控制SVC</font>
> 服务器记录所有版本，客户端仅有最新版本，无法实现断网操作！
![](https://img-blog.csdnimg.cn/8615b19db9564edabd73a310eeab1288.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
> 集中式版本控制 
+ 客户端只会`保留最新`得`版本号`；
+ 如果发生`断网`，`客户端`继续版本更新，就会导致`丢失断网期间的数据版本`

### <font color=blue>分布式版本控制</font>
>服务器记录所有版本，客户端也有所有版本，具有本地仓库功能，`支持断网操作`！
![](https://img-blog.csdnimg.cn/e83debf7f7e5477880c3886a6666d7fd.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
分布式版本控制 

+ 客户端拥有本地仓库，会保留所以的历史版本；
+ 如果发生断网，客户端继续版本更新，数据修改的版本会被提交到本地仓库；
+ 当网络回复时，客户端会将所有本地仓库的版本数据提交到远程服务器上

### <font color=camel>Git基本概念</font>
+ `Git 仓库`: 保存所有数据的地方
+ `工作区`: 实时编辑文件的地方 (只看到一个文件)
+ `暂存区`: 就是一个文件，索引文件，保存下次将提交的文件列表信息

简易的命令行入门教程:
```bash
##Git 全局设置:
git config --global user.name "意必固我"
git config --global user.email "1224965096@qq.com"
##创建 git 仓库:
mkdir Demos
cd Demos
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin git@gitee.com:liruilonger/Demos.git
git push -u origin master
##已有仓库?
cd existing_git_repo
git remote add origin git@gitee.com:liruilonger/Demos.git
git push -u origin master
```

### <font color=tomato>Git工作流</font>
![在这里插入图片描述](https://img-blog.csdnimg.cn/f2fbc9e242974c8982703234b8c61f1b.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)
## <font color=seagreen>部署Git服务</font>
### <font color=amber>部署Git仓库</font>
+ 安装软件(192.168.2.100作为远程Git服务器)

```bash
yum -y install git
```

```bash
###git服务器的部署，需要先创建一个数据目录
[root@web1 ~]$ mkdir /var/git
###初始化一个空的项目project，可以存放版本数据，让客户端上传和下载数据
#--bare 长选项，代指空仓库
[root@web1 ~]$ git init /var/git/project --bare
初始化空的 Git 版本库于 /var/git/project/
###查看初始化后，项目project下的文件
[root@web1 ~]$ ls /var/git/project/
branches description hooks objects
config HEAD info refs
git #存储的数据都是以密文的方式存储的，无法直接打开看到，需要通过git命令查看
```
### <font color=yellowgreen>客户端测试</font>
+ 在客户端克隆服务器资源进行测试

```bash
###客户端安装git软件包
[root@liruilongs.github.io ~]$ yum -y install git
###客户端克隆服务端的所有project数据，每次克隆，都会完整克隆一遍
[root@liruilongs.github.io ~]$ git clone root@192.168.2.100:/var/git/project
正克隆到 'project'...
The authenticity of host '192.168.2.100 (192.168.2.100)' can't be established.
ECDSA key fingerprint is SHA256:wpTzE5md4arrZVRHhaKFacrEHYWldSWAPwghApqXXfo.
ECDSA key fingerprint is MD5:7d:d3:26:9d:05:b4:f5:e0:0e:e4:2d:34:2d:c5:a4:3e.
Are you sure you want to continue connecting (yes/no)? yes ##ssh连接
Warning: Permanently added '192.168.2.100' (ECDSA) to the list of known hosts.
root@192.168.2.100's password: ##输入密码验证
warning: 您似乎克隆了一个空版本库。 #下载project成功
[root@liruilongs.github.io ~]$ ls /root/project/ #空的版本库
[root@liruilongs.github.io ~]$ ls -a /root/project/ #只有隐藏文件
. .. .git
```
# <font color=red>二、GIt版本控制</font>
### <font color=seagreen>提交数据</font>
在客户端对仓库进行编辑，创建新资料:注意：liruilongs.github.io为客户端
```bash
########## git客户端所有的操作都必须在相应的git仓库下进行 ###########
##进入project工作区
[root@liruilongs.github.io ~]$ cd /root/project/
##编辑，操作文件
[root@liruilongs.github.io project]$ mkdir demo
[root@liruilongs.github.io project]$ cp /etc/hosts demo/
[root@liruilongs.github.io project]$ cp /etc/passwd init.txt
##查看工作区 project 的状态信息
[root@liruilongs.github.io project]$ git status
# 位于分支 master
#
# 初始提交
#
# 未跟踪的文件:
# (使用 "git add <file>..." 以包含要提交的内容)
#
# demo/
# init.txt
提交为空，但是存在尚未跟踪的文件(使用 "git add" 建立跟踪)
```
### <font color=red>提交数据到本地版本仓库</font>
>提交暂存区
```bash
#.代表当前目录，提交当前目录下的所有数据到暂存区中
[root@liruilongs.github.io project]$ git add .
###每个工作区下，都有一个 .git 的隐藏目录，这个目录就是本地仓库
##提交数据时，就是把数据在 .git 目录下进行备份
[root@liruilongs.github.io project]$ ls -a
. .. demo .git init.txt
```
>将暂存区修改提交到本地仓库
```bash
###指定提交数据的人的邮箱和名字，只需要执行一次
[root@liruilongs.github.io project]$ git config --global user.email "zhang@163.com"
[root@liruilongs.github.io project]$ git config --global user.name "zhang san"
##将暂存区的修改提交到本地仓库(.git)
[root@liruilongs.github.io project]$ git commit -m "first file version"
[master(根提交) 6023adb] first file version
2 files changed, 23 insertions(+)
create mode 100644 demo/init.txt
create mode 100644 init.txt
```
### <font color=blue>将数据推送至远程Git服务器</font>

```bash
###配置提交方式，只需要执行一次
[root@liruilongs.github.io project]$ git config --global push.default simple
###提交本地仓库的数据到远程的git服务器上；
#可以用 du -sh /var/git/project/ 查看web1提交前后目录大小的变化
[root@liruilongs.github.io project]$ git push
root@192.168.2.100 s password: #输入密码
Counting objects: 5, done.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (5/5), 787 bytes | 0 bytes/s, done.
Total 5 (delta 0), reused 0 (delta 0)
To root@192.168.2.100:/var/git/project
* [new branch] master -> master
####git pull 从服务端下载数据，每次下载的都是已经更新修改的数据，没有修改的不下载
[root@liruilongs.github.io project]$ git pull
root@192.168.2.100's password: #输入密码
Already up-to-date.
```
### <font color=orange> 生成更多版本</font>
```bash
###创建文件new.txt，先提交到暂存区，再从暂存区提交数据到本地仓库
[root@liruilongs.github.io project]$ echo "new file" > new.txt
[root@liruilongs.github.io project]$ git add .
[root@liruilongs.github.io project]$ git commit -m "add new.txt"
[master 02d4ef9] add new.txt
1 file changed, 1 insertion(+)
create mode 100644 new.txt
###给文件new.txt追加一行first，先提交到暂存区，再从暂存区提交数据到本地仓库
[root@liruilongs.github.io project]$ echo "first" >> new.txt
[root@liruilongs.github.io project]$ git add .
[root@liruilongs.github.io project]$ git commit -m "new.txt:first line"
[master 7dc8c9e] new.txt:first line
1 file changed, 1 insertion(+)
###给文件new.txt追加一行second，先提交到暂存区，再从暂存区提交数据到本地仓库
[root@liruilongs.github.io project]$ echo "second" >> new.txt
[root@liruilongs.github.io project]$ git add .
[root@liruilongs.github.io project]$ git commit -m "new.txt:second"
[master c191194] new.txt:second
1 file changed, 1 insertion(+)
###给文件new.txt追加一行third，先提交到暂存区，再再从暂存区提交数据到本地仓库
[root@liruilongs.github.io project]$ echo "third" >> new.txt
[root@liruilongs.github.io project]$ git add .
[root@liruilongs.github.io project]$ git commit -m "new.txt:thired"
[master ebfe460] new.txt:thired
1 file changed, 1 insertion(+)
###将本地仓库(.git)中的数据提交到远程服务器上
[root@liruilongs.github.io project]$ git push
root@192.168.2.100's password:
Counting objects: 13, done.
Compressing objects: 100% (8/8), done.
Writing objects: 100% (12/12), 961 bytes | 0 bytes/s, done.
Total 12 (delta 3), reused 0 (delta 0)
To root@192.168.2.100:/var/git/project
6023adb..ebfe460 master -> master
```
>多次修改数据，生成更多版本信息
```bash
###创建文件num.txt，先提交到暂存区，再提交到本地仓库
[root@liruilongs.github.io project]$ echo "123" > num.txt
[root@liruilongs.github.io project]$ git add .
[root@liruilongs.github.io project]$ git commit -m "num.txt:123"
[master b35b1ce] num.txt:123
1 file changed, 1 insertion(+)
create mode 100644 num.txt
###将文件num.txt内容修改为456，先提交到暂存区，再提交到本地仓库
[root@liruilongs.github.io project]$ echo "456" > num.txt
[root@liruilongs.github.io project]$ git add .
[root@liruilongs.github.io project]$ git commit -m "num.txt:456"
[master 9595def] num.txt:456
1 file changed, 1 insertion(+), 1 deletion(-)
###将文件num.txt内容修改为789，先提交到暂存区，再提交到本地仓库
[root@liruilongs.github.io project]$ echo "789" > num.txt
[root@liruilongs.github.io project]$ git add .
[root@liruilongs.github.io project]$ git commit -m "num:txt:789"
[master 30d20ed] num:txt:789
1 file changed, 1 insertion(+), 1 deletion(-)
###将本地仓库(.git)中的数据提交到远程服务器上
[root@liruilongs.github.io project]$ git push
root@192.168.2.100's password:
Counting objects: 10, done.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (9/9), 663 bytes | 0 bytes/s, done.
Total 9 (delta 3), reused 0 (delta 0)
To root@192.168.2.100:/var/git/project
ebfe460..30d20ed master -> master
```
### <font color=orange>查看日志</font>
```bash
###查看所有的版本日志信息
[root@liruilongs.github.io project]$ git log
......
commit 6023adb8f6c785c41b1857e5027d34904b25ed05 #版本唯一编码
Author: zhang san <zhang@163.com> #上传人的姓名和邮箱
Date: Thu Jul 30 10:36:19 2020 +0800 #版本更新的日期
first file version #版本修改时的注释信息
......
###只查看版本的唯一编码和注释信息
[root@liruilongs.github.io project]$ git log --pretty=oneline
......
7dc8c9e0e86c0eb3a5f976380e90f100cb6da999 new.txt:first line
02d4ef9d70844268d8c6048b1133dca2c1a5bc27 add new.txt
6023adb8f6c785c41b1857e5027d34904b25ed05 first file version
......
###只查看版本的唯一编码【前几位字符】和注释信息
[root@liruilongs.github.io project]$ git log --oneline
......
7dc8c9e new.txt:first line
02d4ef9 add new.txt
6023adb first file version
......
###查看版本的唯一编码【前几位字符】、指针位置和注释信息
[root@liruilongs.github.io project]$ git reflog
......
7dc8c9e HEAD@{5}: commit: new.txt:first line
02d4ef9 HEAD@{6}: commit: add new.txt
6023adb HEAD@{7}: commit (initial): first file version
.....
```

## <font color=royalblue>数据恢复</font>
### <font color=seagreen>HEAD指针下</font>
>`HEAD指针`是一个可以在`任何分支`和`任何版本`移动的`指针`,通过移动过`指针`我们可以将`数据还原至任何版本`,当前`HEAD指针`为`HEAD@{0}`

我们可以使用 git relog 查看当前指针

![在这里插入图片描述](https://img-blog.csdnimg.cn/7b16e72c772343629d0ac7f17c35e386.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)


```bash
[root@liruilongs.github.io project]$ git reflog
30d20ed HEAD@{0}: commit: num:txt:789
9595def HEAD@{1}: commit: num.txt:456
b35b1ce HEAD@{2}: commit: num.txt:123
ebfe460 HEAD@{3}: commit: new.txt:thired
c191194 HEAD@{4}: commit: new.txt:second
7dc8c9e HEAD@{5}: commit: new.txt:first line
02d4ef9 HEAD@{6}: commit: add new.txt
6023adb HEAD@{7}: commit (initial): first file version
HEAD #指针默认指定在最新修改的版本位置，如果想查看以前的版本号，只需将HEAD指针移动到相应版本的位置即可
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/098e733c05304cf4827aed4642eeff7a.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

### <font color=orange>查看Git日志信息</font>
```bash
###只查看版本的唯一编码【前几位字符】和注释信息
[root@liruilongs.github.io project]$ git log --oneline
30d20ed num:txt:789
9595def num.txt:456
b35b1ce num.txt:123
ebfe460 new.txt:thired
c191194 new.txt:second
7dc8c9e new.txt:first line
02d4ef9 add new.txt
6023adb first file version
```
### <font color=brown>移动HEAD指計</font>
```bash
###通过版本编号，移动指针到版本num.txt:123
[root@liruilongs.github.io project]$ git reset --hard b35b1ce
HEAD 现在位于 b35b1ce num.txt:123
### HEAD@{0} 始终指在当前版本的位置
[root@liruilongs.github.io project]$ git reflog | head -2
b35b1ce HEAD@{0}: reset: moving to b35b1ce
30d20ed HEAD@{1}: commit: num:txt:789
### 查看文件内容
[root@liruilongs.github.io project]$ cat num.txt
123
###通过版本编号，移动指针到版本num.txt:123
[root@liruilongs.github.io project]$ git reset --hard 02d4ef9
HEAD 现在位于 02d4ef9 add new.txt
### HEAD@{0} 始终指在当前版本的位置
[root@liruilongs.github.io project]$ git reflog | head -2
02d4ef9 HEAD@{0}: reset: moving to 02d4ef9
b35b1ce HEAD@{1}: reset: moving to b35b1ce
### 由于num.txt是在new.txt 之后产生的，移动指针后num.txt会消失
[root@liruilongs.github.io project]$ ls
demo init.txt new.txt
### 查看文件内容
[root@liruilongs.github.io project]$ cat new.txt
new file
### 通过git 查看到以前的版本内容，拷贝到其他位置后，要回到最新的版本下，否则会保存
##因为版本更新是按照顺序的，指针停留在以前的版本，则无法更新最新的版本
[root@liruilongs.github.io project]$ git reset --hard 30d20ed
HEAD 现在位于 30d20ed num:txt:789

```
## <font color=royalblue>分支</font>
>分支可以让开发分多条主线同时进行,每个主线互不影响
+ 按功能模块分支、按版本分支
+ 分支也可以合并

>常见分支规范如下:/常见的分支规范如下:
+ `默认master分支MASTER是主分支`,是代码的核心
+ `DEVELOP最新开发成果的分支`
+ `RELEASE分支`,为发布新产品设置的分支
+ `HOTFIX分支`,为了修复软件BUG缺陷的分支
+ `FEATURE分支`,为开发新功能设置的分支

```bash
MASTER分支 #产品的主要代码，代码的核心
DEVELOP分支 #开发产品的一个大的功能模块
RELEASE分支 #发布产品给用户使用的分支
HOTFIX分支 #修复产品BUG缺陷的分支
FEATURE分支 #开发产品的一个小的功能
```

### <font color=orange>概述暂存区修改提交到本地仓库</font>
### <font color=camel>创建分支</font>
>查看当前分支
```bash
###查看当前所在的分支
[root@liruilongs.github.io project]$ git status
# 位于分支 master
无文件要提交，干净的工作区
###查看所有分支，以 * 打头，即为当前所处的分支
[root@liruilongs.github.io project]$ git branch -v
* master 30d20ed num:txt:789
```
>创建分支
```bash
###创建分支hotfix【修复bug】和feature【开发小功能】
[root@liruilongs.github.io project]$ git branch hotfix
[root@liruilongs.github.io project]$ git branch feature
###查看所有分支，以 * 打头，即为当前所处的分支
[root@liruilongs.github.io project]$ git branch -v
feature 30d20ed num:txt:789
hotfix 30d20ed num:txt:789
* master 30d20ed num:txt:789
```
### <font color=red>切换与合并分支</font>
>切换分支
```bash
###切换到hotfix【修复bug】分支
[root@liruilongs.github.io project]$ git checkout hotfix
切换到分支 'hotfix'
###查看所有分支，以 * 打头，即为当前所处的分支
[root@liruilongs.github.io project]$ git branch -v
feature 30d20ed num:txt:789
* hotfix 30d20ed num:txt:789
master 30d20ed num:txt:789
```

> 在新分支上进行数据操作(增、删、改、查)

```bash
###在分支hotfix下，给new.txt追加一行数据
[root@liruilongs.github.io project]$ echo "fix a bug" >> new.txt
[root@liruilongs.github.io project]$ cat new.txt
new file
first
second
third
fix a bug
###提交修改数据到暂存区
[root@liruilongs.github.io project]$ git add .
###从暂存区提交修改数据到本地仓库
[root@liruilongs.github.io project]$ git commit -m "fix a bug"
[hotfix e686c17] fix a bug
1 file changed, 1 insertion(+)
```

>合并分支:将 hotfix 修改的数据合并到 master 分支

```bash
###切换分支到master下
[root@liruilongs.github.io project]$ git checkout master
切换到分支 'master'
###发现hostfix分支中的修改，在master主分支下是没有生效的
[root@liruilongs.github.io project]$ cat new.txt
new file
first
second
third
###查看所有分支，以 * 打头，即为当前所处的分支
[root@liruilongs.github.io project]$ git branch -v
feature 30d20ed num:txt:789
hotfix e686c17 fix a bug
* master 30d20ed num:txt:789
###合并分支必须处在master分支下，将其他分支合并过来
[root@liruilongs.github.io project]$ git merge hotfix
更新 30d20ed..e686c17
Fast-forward
new.txt | 1 +
1 file changed, 1 insertion(+)
###再次查看new.txt，合并成功
[root@liruilongs.github.io project]$ cat new.txt
new file
first
second
third
fix a bug
```

### <font color=yellowgreen>解决分支的版本冲实问题</font>
>在不同分支中修改相同文件的相同行数据，模拟数据冲突
```bash
###切换到hotfix分支
[root@liruilongs.github.io project]$ git checkout hotfix
切换到分支 'hotfix'
###hotfix分支下创建新文件a.txt
[root@liruilongs.github.io project]$ echo AAA > a.txt
###提交修改数据到暂存区
[root@liruilongs.github.io project]$ git add .
###从暂存区提交数据到本地仓库
[root@liruilongs.github.io project]$ git commit -m "add a.txt by hotfix"
[hotfix d96f8ac] add a.txt by hotfix
1 file changed, 1 insertion(+)
create mode 100644 a.txt
###切换到master分支
[root@liruilongs.github.io project]$ git checkout master
切换到分支 'master'
您的分支领先 'origin/master' 共 1 个提交。
(使用 "git push" 来发布您的本地提交)
###master分支下，创建新文件a.txt
[root@liruilongs.github.io project]$ echo "BBB" > a.txt
###提交修改数据到暂存区
[root@liruilongs.github.io project]$ git add .
###从暂存区提交数据到本地仓库
[root@liruilongs.github.io project]$ git commit -m "add a.txt by master"
[master afd5c52] add a.txt by master
1 file changed, 1 insertion(+)
create mode 100644 a.txt
###在master分支下，合并hotfix分支；修改相同行数据，发生冲突，无法合并
[root@liruilongs.github.io project]$ git merge hotfix
自动合并 a.txt
冲突(添加/添加)：合并冲突于 a.txt
自动合并失败，修正冲突然后提交修正的结果。
```
>查看有冲突的文件，修改文件为最终版本的数据，解决冲突
```bash
###查看冲突的a.txt文件
[root@liruilongs.github.io project]$ cat a.txt
<<<<<<< HEAD
BBB
=======
AAA
>>>>>>> hotfix
###冲突需要自己手动解决，修改master分支中的数据，则为最终数据
[root@liruilongs.github.io project]$ vim a.txt #留下的为最终数据
BBB
###将修改提交到暂存区
[root@liruilongs.github.io project]$ git add .
###将暂存区中的数据提交到本地，备注冲突解决
[root@liruilongs.github.io project]$ git commit -m "resolved"
[master b5864d8] resolved
```
### **<font color=orange>增量升级时，导出所有更新文件</font>**

![在这里插入图片描述](https://img-blog.csdnimg.cn/d3f8fba672a3476e924f51bc2fedd4b2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)
```bash
D:\iwhalecloud\uncp\uncp-all>git log --pretty=oneline -2
b3406f5801f04eef605da8fa54259efedf105fc5 (HEAD -> neimeng) <E5><86><85><E8><92><99><E4><BB><A3><E7><A0><81><E6><94><B9><E9><80><A0>
ada4c1075af4f98b3a38491cd6aa73ef48098a68 (origin/neimeng, origin/master, origin/guangxi, origin/HEAD, master) Merge branch 'master' of http://gitlab.iwhalecloud.com/uncp-all/uncp-all

D:\iwhalecloud\uncp\uncp-all>

PS D:\iwhalecloud\uncp\uncp-all> git archive -o ./export.zip b3406f5801f04eef605da8fa54259efedf105fc5   $(git diff --name-only ada4c1075af4f98b3a38491cd6aa73ef48098a68  b3406f5801f04eef605da8fa54259efedf105fc5 )
PS D:\iwhalecloud\uncp\uncp-all> git reflog
```
**<font color=seagreen>git命令</font>**
```bash
git archive -o ./export.zip NewCommitId $(git diff --name-only OldCommitId NewCommitId)
```

# <font color=red>三、Gitlab搭建</font>


>服务器： liruilongs.github.io：192.168.26.55

## <font color=tomato>一、docker 环境安装</font>
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ yum -y install docker-ce
┌──[root@liruilongs.github.io]-[~]
└─$ systemctl enable docker --now
```
**<font color=brown>配置docker加速器</font>**
```bash
sudo mkdir -p /etc/docker
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://2tefyfv7.mirror.aliyuncs.com"]
}
EOF
sudo systemctl daemon-reload
sudo systemctl restart docker
```

## <font color=tomato>二、安装GitLab</font>

### <font color=green>1.安装GitLab 并配置</font>
**<font color=green>拉取镜像</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker pull beginor/gitlab-ce
```
|--|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/933c165cc3464588956f6af222cf0250.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|

### <font color=plum>2.创建共享卷目录</font>
**<font color=amber>创建共享卷目录，用于持久化必要的数据和更改相关配置</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ mkdir -p /data/gitlab/etc/ /data/gitlab/log /data/gitlab/data
┌──[root@liruilongs.github.io]-[~]
└─$ chmod 777 /data/gitlab/etc/ /data/gitlab/log /data/gitlab/data
```
### <font color=brown>3.创建 Gitlab 容器</font>
**<font color=royalblue>这里的访问端口一定要要设置成80，要不push项目会提示没有报错，如果宿主机端口被占用，需要把这个端口腾出来</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker run -itd --name=gitlab --restart=always --privileged=true   -p 8443:443  -p 80:80 -p 222:22 -v  /data/gitlab/etc:/etc/gitlab -v  /data/gitlab/log:/var/log/gitlab -v  /data/gitlab/data:/var/opt/gitlab  beginor/gitlab-ce
acc95b2896e8475915275d5eb77c7e63f63c31536432b68508f2f216d4fec634
┌──[root@liruilongs.github.io]-[~]
└─$ docker ps
CONTAINER ID   IMAGE               COMMAND             CREATED          STATUS                             PORTS                                                                                                             NAMES
acc95b2896e8   beginor/gitlab-ce   "/assets/wrapper"   53 seconds ago   Up 51 seconds (health: starting)   0.0.0.0:80->80/tcp, :::80->80/tcp, 0.0.0.0:222->22/tcp, :::222->22/tcp, 0.0.0.0:8443->443/tcp, :::8443->443/tcp   gitlab
┌──[root@liruilongs.github.io]-[~]
└─$
┌──[root@liruilongs.github.io]-[~]
└─$# 
```

### <font color=plum>4.关闭容器修改相关配置文件</font>
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker stop gitlab
gitlab
```
**<font color=royalblue>external_url    'http://192.168.26.55'</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep external_url
##! For more details on configuring external_url see:
# external_url 'GENERATED_EXTERNAL_URL'
# registry_external_url 'https://registry.gitlab.example.com'
# pages_external_url "http://pages.example.com/"
# gitlab_pages['artifacts_server_url'] = nil # Defaults to external_url + '/api/v4'
# mattermost_external_url 'http://mattermost.example.com'
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/external_url 'GENERATED_EXTERNAL_URL'/a external_url\t'http://192.168.26.55' "  /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep external_url
##! For more details on configuring external_url see:
# external_url 'GENERATED_EXTERNAL_URL'
external_url    'http://192.168.26.55'
# registry_external_url 'https://registry.gitlab.example.com'
# pages_external_url "http://pages.example.com/"
# gitlab_pages['artifacts_server_url'] = nil # Defaults to external_url + '/api/v4'
# mattermost_external_url 'http://mattermost.example.com'
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=orange>gitlab_rails['gitlab_ssh_host'] = '192.168.26.55'</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_ssh_host
# gitlab_rails['gitlab_ssh_host'] = 'ssh.host_example.com'
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/gitlab_ssh_host/a gitlab_rails['gitlab_ssh_host'] = '192.168.26.55' "  /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~] 
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_ssh_host
# gitlab_rails['gitlab_ssh_host'] = 'ssh.host_example.com'
gitlab_rails['gitlab_ssh_host'] = '192.168.26.55'
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=blue>gitlab_rails[gitlab_shell_ssh_port] = 222</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_shell_ssh
# gitlab_rails['gitlab_shell_ssh_port'] = 22
┌──[root@liruilongs.github.io]-[~]
└─$ sed -i "/gitlab_shell_ssh_port/a gitlab_rails['gitlab_shell_ssh_port'] = 222" /data/gitlab/etc/gitlab.rb
┌──[root@liruilongs.github.io]-[~]
└─$ cat /data/gitlab/etc/gitlab.rb | grep gitlab_shell_ssh
# gitlab_rails['gitlab_shell_ssh_port'] = 22
gitlab_rails[gitlab_shell_ssh_port] = 222
┌──[root@liruilongs.github.io]-[~]
└─$
```
**<font color=purple>/data/gitlab/data/gitlab-rails/etc/gitlab.yml</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ vim /data/gitlab/data/gitlab-rails/etc/gitlab.yml
┌──[root@liruilongs.github.io]-[~]
└─$
##############################
 gitlab:
    ## Web server settings (note: host is the FQDN, do not include http://)
    host: 192.168.26.55
    port: 80
    https: false
```

|--|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/8b98142d3a1c4f54b2096ce7af378324.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
**<font color=yellowgreen>修改完配置文件之后。直接启动容器</font>**
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ docker start gitlab
```
#### <font color=green>5.访问测试</font>
|访问测试|
|--|
|**<font color=red>在宿主机所在的物理机访问，`http://192.168.26.55/` ，会自动跳转到修改密码(root用户),如果密码设置的没有满足一定的复杂性，则会报500，需要从新设置</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/2f11a407499c4f73bb01c3d513d345bb.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/c05f68349fb14087b8c023b2c43c1c71.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=seagreen>登录进入仪表盘</font>**|


## <font color=brown>三、新建项目，push代码测试</font>

|新建一个项目，push代码测试|
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/50da96a334234626969052cb4c5c35ec.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/7203aa8028e64fc2be35d0c536d0a3bd.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/2959b0672f8a4a31a59a0da249832281.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=yellowgreen>然后我们简单测试一下，push一个项目上去</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/9963328c77f54e618169e9a55acea031.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|**<font color=orange>项目成功上传Gitlab</font>**|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/9bad889ac0124c41b6704cbc7ee3bd19.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/d5edeb74446c491eae6f687538d6b73a.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5bGx5rKz5bey5peg5oGZ,size_20,color_FFFFFF,t_70,g_se,x_16)|



```bash
PS F:\blogger> git init
Initialized empty Git repository in F:/blogger/.git/
PS F:\blogger> git config --global user.name "Administrator"
PS F:\blogger> git config --global user.email "admin@example.com"
PS F:\blogger> git remote add origin http://192.168.26.55/root/blog.git
PS F:\blogger> git add .
PS F:\blogger> git commit -m "Initial commit"
PS F:\blogger> git push -u origin master
Enumerating objects: 322, done.
Counting objects: 100% (322/322), done.
Delta compression using up to 8 threads
Compressing objects: 100% (302/302), done.
Writing objects: 100% (322/322), 11.31 MiB | 9.22 MiB/s, done.
Total 322 (delta 24), reused 0 (delta 0)
remote: Resolving deltas: 100% (24/24), done.
To http://192.168.26.55/root/blog.git
 * [new branch]      master -> master
Branch 'master' set up to track remote branch 'master' from 'origin'.
PS F:\blogger>
```

>时间原因，关于 【搭建自己的中文Git版本库】先分享到这里。生活加油  ^ _ ^

