---
title: 关于 Linux中NFS的一些笔记
tags:
  - NFS
categories:
  - NFS
toc: true
recommend: 1
keywords: 存储
uniqueId: '2021-11-27 13:09:57/关于 Linux中NFS的一些笔记.html'
mathJax: false
date: 2021-11-27 21:09:57
thumbnail:
---
**<font color="009688"> 亲近的人是不应该分开太久的。没见面的时候朝思暮想，可一旦见到，是否双方都会无可奈何地感觉到这条鸿沟呢？虽然可怕，但这也许更接近事实。——中岛敦《山月记》**</font>
<!-- more -->
## 写在前面
***
+ 学习K8s的`Volumes`相关，遇到`NFS挂载`,所以放到一起总结一下
+ 这里主要是实战，理论很少。
+ 为了方便，部分地方使用了`ansible`，只用了`shell`模块，不影响阅读

**<font color="009688"> 亲近的人是不应该分开太久的。没见面的时候朝思暮想，可一旦见到，是否双方都会无可奈何地感觉到这条鸿沟呢？虽然可怕，但这也许更接近事实。——中岛敦《山月记》**</font>
 ***

# NFS 
**<font color=seagreen>NFS(Network File System, 网络文件系统),用来为客户机提供共享使用的文件夹；</font>**

**<font color=amber>将NFS服务器分享的目录，挂载到本地机器当中，本地NFS的客户端应用可以读写位于远端NFS服务器上的文件，在客户端端看起来，就像访问本地文件一样。</font>**



NFS本身的服务并没有提供数据传递的协议，而是通过使用 **<font color=blue>RPC(远程过程调用 Remote Procedure Call)来实现</font>**。**<font color=amber>当NFS启动后，会随机的使用一些端口，NFS就会向RPC去注册这些端口。RPC就会记录下这些端口，RPC会开启111端口。通过client端和sever端端口的连接来进行数据的传输。在启动nfs之前，首先要确保rpc服务启动。但是本质上还使用的TCP协议</font>**




**<font color=plum>使用NFS网络文件系统提供的共享目录存储数据时,我们需要在系统中部署一个NFSServer</font>**

## <font color=red>服务端</font>
**<font color=red>下载服务需要的包，设置开机自启</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~] #软件包：nfs-utils
└─$yum -y install nfs-utils.x86_64
┌──[root@vms81.liruilongs.github.io]-[~] #系统服务：nfs-server
└─$systemctl enable nfs-server.service  --now
```
**<font color=chocolate>创建网络共享文件夹，写入测试信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~]
└─$mkdir -p /liruilong
┌──[root@vms81.liruilongs.github.io]-[/liruilong]
└─$cd /liruilong/;echo `date` > liruilong.txt
┌──[root@vms81.liruilongs.github.io]-[/liruilong]
└─$cd /liruilong/;cat  liruilong.txt
2021年 11月 27日 星期六 21:57:10 CST
```
**<font color=seagreen>`exports `配置文件解析 (服务端)</font>**

>**<font color=amber>语法： 文件夹路径     客户机地址(权限)    客户机地址(权限)....</font>**
PS1:**<font color=red> /public 192.168.4.0/24</font>**

```bash
┌──[root@vms81.liruilongs.github.io]-[/liruilong]
└─$cat /etc/exports
┌──[root@vms81.liruilongs.github.io]-[/liruilong]  #任意主机可以访问 (ro)只读的方式
└─$echo "/liruilong *(rw,sync,no_root_squash)" > /etc/exports
```
**<font color=purple>刷新配置exportfs -arv</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[/liruilong]
└─$exportfs -arv
exporting *:/liruilong
┌──[root@vms81.liruilongs.github.io]-[/liruilong]
└─$showmount -e  #查看当前机器服务列表
Export list for vms81.liruilongs.github.io:
/liruilong *
┌──[root@vms81.liruilongs.github.io]-[/liruilong]
└─$

```
|--|
|--|
|https://img-blog.csdnimg.cn/9f73015ce2ac4092a97911970fb05601.png|

## <font color=camel>客户端</font>

>**<font color=red>这里为了方便，使用了`ansible`</font>**

**<font color=chocolate>然后我们需要在所有的使用节点安装nfs-utils，然后挂载</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m shell -a "yum -y install nfs-utils"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m shell -a "systemctl enable nfs-server.service  --now"
```
**<font color=yellowgreen>nfs共享文件测试：查看指定机器的共享文件列表：showmount -e vms81.liruilongs.github.io</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible] #查看指定机器的共享文件列表
└─$ansible node -m shell -a "showmount -e vms81.liruilongs.github.io"
192.168.26.83 | CHANGED | rc=0 >>
Export list for vms81.liruilongs.github.io:
/liruilong *
192.168.26.82 | CHANGED | rc=0 >>
Export list for vms81.liruilongs.github.io:
/liruilong *
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
## <font color=amber>挂载方式</font>
### <font color=red>手动挂载</font>
**<font color=red>挂载测试，这里我们通过手动的方式挂载</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node  -m shell -a "mount  vms81.liruilongs.github.io:/liruilong /mnt"

192.168.26.82 | CHANGED | rc=0 >>

192.168.26.83 | CHANGED | rc=0 >>

┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m shell -a "cd /mnt/;ls"
192.168.26.83 | CHANGED | rc=0 >>
liruilong.txt
192.168.26.82 | CHANGED | rc=0 >>
liruilong.txt
```
**<font color=amber>查看挂载信息</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node -m shell -a "df -h | grep  liruilong"
192.168.26.82 | CHANGED | rc=0 >>
vms81.liruilongs.github.io:/liruilong  150G  8.3G  142G    6% /mnt
192.168.26.83 | CHANGED | rc=0 >>
vms81.liruilongs.github.io:/liruilong  150G  8.3G  142G    6% /mnt
```
**<font color=plum>取消挂载</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible node  -m shell -a "umount /mnt"
```

**<font color=blue>当然，挂载方式还可以使用开机自动挂载，触发挂载的方式</font>**



### <font color=brown>开机自动挂载</font>
**<font color=tomato>挂载机器环境准备</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell  -a "yum -y install nfs-utils"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell  -a "systemctl enable nfs-utils --now"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell  -a "showmount -e 192.168.26.81"
192.168.26.100 | CHANGED | rc=0 >>
Export list for 192.168.26.81:
/vdisk     *
/tmp       *
/liruilong *
```
**<font color=yellowgreen>配置 etc/fstab</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell  -a "echo '192.168.26.81:/liruilong   /mnt/nfsmount   nfs defaults,_
netdev 0 0' >> /etc/fstab"
192.168.26.100 | CHANGED | rc=0 >>

┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell  -a "cat  /etc/fstab | grep liruilong"
192.168.26.100 | CHANGED | rc=0 >>
192.168.26.81:/liruilong   /mnt/nfsmount   nfs defaults,_netdev 0 0
```
**<font color=chocolate>刷新配置:测试</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell  -a " mkdir /mnt/nfsmount"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell  -a " mount -a"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell  -a " ls -l /mnt/nfsmount"
192.168.26.100 | CHANGED | rc=0 >>
总用量 4
-rw-r--r-- 1 root root 43 11月 27 21:57 liruilong.txt
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ll /liruilong/
总用量 4
-rw-r--r-- 1 root root 43 11月 27 21:57 liruilong.txt
```
### <font color=blue>触发挂载</font>

**<font color=green>由 autofs 服务提供的 “按需访问” 机制，只要访问挂载点，就会触发响应，自动挂载指定设备；闲置超过时限(默认5分钟)后，会自动卸载</font>**

**<font color=tomato>安装需要的软件包</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell -a "yum -y install autofs"
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell -a "systemctl enable  autofs --now"
192.168.26.100 | CHANGED | rc=0 >>
Created symlink from /etc/systemd/system/multi-user.target.wants/autofs.service to /usr/lib/systemd/system/autofs.service.
```
**<font color=purple>查看配置文件位置</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell -a "rpm -qc autofs"
[WARNING]: Consider using the yum, dnf or zypper module rather than running 'rpm'.  If you need to use
command because yum, dnf or zypper is insufficient you can add 'warn: false' to this command task or
set 'command_warnings=False' in ansible.cfg to get rid of this message.
192.168.26.100 | CHANGED | rc=0 >>
/etc/auto.master
/etc/auto.misc
/etc/auto.net
/etc/auto.smb
/etc/autofs.conf
/etc/autofs_ldap_auth.conf
/etc/sysconfig/autofs
/usr/lib/systemd/system/autofs.service
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
**<font color=camel>配置文件编辑</font>**
**<font color=royalblue>配置自动挂载需要修改两个配置文件，一个`/etc/auto.master`</font>**

**<font color=chocolate>`/etc/auto.master`这个配置文件为主配置文件，配置当前机器挂载的目录的配置文件</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell -a "echo '/liruilong  /etc/auto.misc' >> /etc/auto.master"
192.168.26.100 | CHANGED | rc=0 >>

```
**<font color=seagreen>这个配置文件为挂载目录挂载的远程目录配置文件的挂载</font>**
```bash
┌──[root@vms81.lirui<font color=camel></font>longs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell -a "echo 'sy    -fstype=nfs   192.168.26.81:/liruilong '  >>/etc/auto.misc"
192.168.26.100 | CHANGED | rc=0 >>
```
**<font color=camel>重启服务</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell -a "systemctl restart autofs"
192.168.26.100 | CHANGED | rc=0 >>

```
**<font color=red>触发挂载，查看</font>**
```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ls /liruilong/
liruilong.txt  mGAX.23  work
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell -a "ls /liruilong/sy"
192.168.26.100 | CHANGED | rc=0 >>
liruilong.txt
mGAX.23
work
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible 192.168.26.100 -m shell -a "df -ah | grep liruilong"
192.168.26.100 | CHANGED | rc=0 >>
192.168.26.81:/liruilong  150G  8.5G  142G    6% /mnt/nfsmount
/etc/auto.misc               0     0     0     - /liruilong
192.168.26.81:/liruilong  150G  8.5G  142G    6% /liruilong/sy
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```

# <font color=red>Ceph</font>

**<font color=seagreen>Ceph是一个分布式文件系统,具有高扩展、高可用、高性能的特点,Ceph可以提供`对象存储`(Java对象，JSON串)、`块存储`(ISCSI，没有经过格式化的裸的设备，即为块存储)、`文件系统存储`(NFS，客户端可以直接挂载使用)</font>**


**<font color=yellowgreen>分布式存储：分布式是指一种独特的系统架构，它由一组网络进行通信、为了完成共同的任务而协调工作的计算机节点组成。分布式系统是为了用廉价的、普通的机器完成单个计算机无法完成的计算、存储任务。其目的就是利用更多的机器，处理更多的数据。即数据不是存储在单个机器上，而是分散得存储在多个普通机器中</font>**

|常用分布式文件系统|
|--|
|Lustre|
|Hadoop|
|FastDFS|
|Ceph|
|GlusterFS|

>Ceph可以提供`PB级别`的存储空间(PB —> TB —> BG) 1024G*1024G=1048576G  最新的ceph支持`EB级别`的存储(EB —> PB —> TB)

|Ceph组件|描述|
|--|--|
|OSDS|存储设备 (共享磁盘)|
|Monitors|集群监控组件 (时时监测ceph集群中服务器的状态—正常或损坏)|
|RadosGateway(RGW)|对象存储网关|
|MDSs|存放文件系统的元数据(对象存储和块存储不需要该组件),实现文件系统共享|
|Client|ceph客户端|

Ceph组件-mon维护着集群MAP
+ ceph集群中安装ceph-osd，将磁盘共享出去，但其他用户识别到的是三块不同的硬盘
+ 安装ceph-mon，监控集群中所有集群的状态；绘制地图，即:文件，文件中包含服务器的相关信息；文件中包含所有节点共享磁盘的信息，同时也包含monitors的数量和相关信息
+ 用户访问，用户先访问集群中任何一个monitor,monitor中记录了整个集群的状态，进而访问整
个ceph集群


### 客户端访问ceph集群步骤

|--|
|--|
|![](https://img-blog.csdnimg.cn/9f73015ce2ac4092a97911970fb05601.png)|

### <font color=red>Ceph规则</font>

**<font color=royalblue>三副本原则</font>** (ceph必须至少由三台服务器组成)：#Ceph中，有三副本原则，会将存储的数据复制三份，并拷贝到其他的节点上；mon监控节点也必须至少存在三个

**<font color=seagreen>过半原则</font>** ceph中，正常服务器的数量必须超过ceph集群中服务器的一半才会正常工作，例如：有三台mon监控节点，一条损坏，集群正常工作，两台损坏，整个集群无法运行

### <font color=chocolate>Ceph产品特色</font>


```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible etcd  -m ping
192.168.26.100 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
192.168.26.102 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
192.168.26.101 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
```

```bash
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$cat ceph/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.26.102 vms102.liruilongs.github.io vms102
192.168.26.101 vms101.liruilongs.github.io vms101
192.168.26.100 vms100.liruilongs.github.io vms100
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible etcd -m copy -a "src=./ceph/hosts dest=/etc/hosts force=yes "

```

```
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$ansible etcd -m shell -a "cat /etc/hosts"
192.168.26.100 | CHANGED | rc=0 >>
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.26.102 vms102.liruilongs.github.io vms102
192.168.26.101 vms101.liruilongs.github.io vms101
192.168.26.100 vms100.liruilongs.github.io vms100
192.168.26.101 | CHANGED | rc=0 >>
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.26.102 vms102.liruilongs.github.io vms102
192.168.26.101 vms101.liruilongs.github.io vms101
192.168.26.100 vms100.liruilongs.github.io vms100
192.168.26.102 | CHANGED | rc=0 >>
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.26.102 vms102.liruilongs.github.io vms102
192.168.26.101 vms101.liruilongs.github.io vms101
192.168.26.100 vms100.liruilongs.github.io vms100
┌──[root@vms81.liruilongs.github.io]-[~/ansible]
└─$
```
