---
title: 《Linux命令行与shell脚本编程大全(第3版)》读书笔记
tags: 
  - Linux 
  - shell
categories: 
  - Linux
toc: true
recommend: 1
keywords: Linux-shell
uniqueId: '2021-07-10 04:16:22/《Linux命令行与shell脚本编程大全(第3版)》读书笔记.html'
mathJax: false
date: 2021-07-10 12:16:22
thumbnail:

---
之前在CSDN上的一些博客，陆续备份到自己的博客平台
<!-- more -->
## 写在前面
***


**傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**

***

<meta charset="utf-8">

<title data-cke-title="所见即所得编辑器, editor">所见即所得编辑器, editor</title>

<style data-cke-temp="1">html{cursor:text;*cursor:auto} img,input,textarea{cursor:default}</style>

<link type="text/css" rel="stylesheet" href="https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/contents-mp.css?t=L677">

<link type="text/css" rel="stylesheet" href="https://mp-blog.csdn.net/mp_blog/css/ck_htmledit.css">

<link type="text/css" rel="stylesheet" href="https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/imagebase/styles/imagebase.css">

<link type="text/css" rel="stylesheet" href="https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/tableselection/styles/tableselection.css">

<link type="text/css" rel="stylesheet" href="https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/chart/chart.css?t=L677">

<link type="text/css" rel="stylesheet" href="https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/codesnippet/lib/highlight/styles/atom-one-dark.css">

<style data-cke-temp="1">.cke_editable{cursor:text}.cke_editable img,.cke_editable input,.cke_editable textarea{cursor:default} .img-upload-box{margin:0 auto;width:420px;height:80px;-webkit-box-sizing:border-box;box-sizing:border-box;padding:8px;border:1px solid #e0e0e0;background-color:#fff;display:-webkit-box;display:-ms-flexbox;display:flex}.img-upload-box img.preview{display:block;width:64px;height:64px}.img-upload-box span.info-box{margin-left:16px}.img-upload-box span.info-box img.info-box{display:block;width:316px;height:auto}.img-upload-box span.info-box span.operate-box{display:-webkit-box;display:-ms-flexbox;display:flex;margin-top:16px;line-height:20px}.img-upload-box span.info-box span.operate-box span.upload-msg{font-size:14px;color: #fc5531;width:100%;text-align:center}.img-upload-box span.info-box span.operate-box a.btn{margin-left:auto;font-size:14px;color:#999;white-space:nowrap;cursor:pointer}.img-upload-box span.info-box span.operate-box a.btn.btn-redo{display:none;margin-right:16px} .htmledit_views .csdn-data-video{width:200px}.htmledit_views .csdn-data-video img{margin:0;display:block;width:200px;height:112px}.htmledit_views .csdn-data-video p{margin-top:10px;margin-bottom:0;font-size:14px} .cke_show_borders table.cke_show_border,.cke_show_borders table.cke_show_border > tr > td, .cke_show_borders table.cke_show_border > tr > th,.cke_show_borders table.cke_show_border > tbody > tr > td, .cke_show_borders table.cke_show_border > tbody > tr > th,.cke_show_borders table.cke_show_border > thead > tr > td, .cke_show_borders table.cke_show_border > thead > tr > th,.cke_show_borders table.cke_show_border > tfoot > tr > td, .cke_show_borders table.cke_show_border > tfoot > tr > th{border : #d3d3d3 1px dotted} .cke_widget_wrapper{position:relative;outline:none}.cke_widget_inline{display:inline-block}.cke_widget_wrapper:hover>.cke_widget_element{outline:2px solid #ffd25c;cursor:default}.cke_widget_wrapper:hover .cke_widget_editable{outline:2px solid #ffd25c}.cke_widget_wrapper.cke_widget_focused>.cke_widget_element,.cke_widget_wrapper .cke_widget_editable.cke_widget_editable_focused{outline:2px solid #47a4f5}.cke_widget_editable{cursor:text}.cke_widget_drag_handler_container{position:absolute;width:15px;height:0;display:block;opacity:0.75;transition:height 0s 0.2s;line-height:0}.cke_widget_wrapper:hover>.cke_widget_drag_handler_container{height:15px;transition:none}.cke_widget_drag_handler_container:hover{opacity:1}.cke_editable[contenteditable="false"] .cke_widget_drag_handler_container{display:none;}img.cke_widget_drag_handler{cursor:move;width:15px;height:15px;display:inline-block}.cke_widget_mask{position:absolute;top:0;left:0;width:100%;height:100%;display:block}.cke_widget_partial_mask{position:absolute;display:block}.cke_editable.cke_widget_dragging, .cke_editable.cke_widget_dragging *{cursor:move !important} .cke_upload_uploading img{opacity: 0.3} .cke_image_nocaption{line-height:0}.cke_editable.cke_image_sw, .cke_editable.cke_image_sw *{cursor:sw-resize !important}.cke_editable.cke_image_se, .cke_editable.cke_image_se *{cursor:se-resize !important}.cke_image_resizer{display:none;position:absolute;width:10px;height:10px;bottom:-5px;right:-5px;background:#000;outline:1px solid #fff;line-height:0;cursor:se-resize;}.cke_image_resizer_wrapper{position:relative;display:inline-block;line-height:0;}.cke_image_resizer.cke_image_resizer_left{right:auto;left:-5px;cursor:sw-resize;}.cke_widget_wrapper:hover .cke_image_resizer,.cke_image_resizer.cke_image_resizing{display:block}.cke_editable[contenteditable="false"] .cke_image_resizer{display:none;}.cke_widget_wrapper>a{display:inline-block}</style>

<u><em><strong><span style="color:#7c79e5">百度#资#在文末：更新中~~~</span></strong></em></u>

<br>

# <span style="color:#f33b45">第一部分Linux命令行</span>



## <span style="color:#3399ea">第1章初识Linux shell</span>



### 深入探究 Linux 内核

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="281" role="region" aria-label=" 图像 小部件"><img alt="" height="354" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031162932973.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031162932973.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="464" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031162932973.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22464%22%2C%22height%22%3A%22354%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



内核主要负责以下四种功能： <span style="color:#f33b45"><strong> 系统内存管理  软件程序管理  硬件设备管理  文件系统管理</strong></span>

<span style="color:#3399ea"><strong>1. 系统内存管理 </strong></span>

：操作系统内核的主要功能之一就是内存管理。<span style="color:#f33b45"><strong>内核不仅管理服务器上的可用物理内存，还可 以创建和管理虚拟内存</strong></span>

(即实际并不存在的内存)。 内核通过<span style="color:#f33b45"><strong>硬盘上的存储空间来实现虚拟内存</strong></span>

，这块区域称为<span style="color:#f33b45"><strong>交换空间(swap space)</strong></span>

。内核不

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="280" role="region" aria-label=" 图像 小部件"><img alt="" height="484" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031163228498.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031163228498.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="579" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031163228498.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22579%22%2C%22height%22%3A%22484%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> 内存存储单元按组划分成很多块，这些块称作页面(page)。内核将每个内存页面放在物理内存或交换空间。然后，内核会维护一个内存页面表，指明哪些页面位于物理内存内，哪些页面被换到了磁盘上。 内核会记录哪些内存页面正在使用中，并自动把一段时间未访问的内存页面复制到交换空间 区域(称为换出，swapping out)——即使还有可用内存。当程序要访问一个已被换出的内存页 面时，内核必须从物理内存换出另外一个内存页面给它让出空间，然后从交换空间换入请求的内 存页面。显然，这个过程要花费时间，拖慢运行中的进程。只要Linux系统在运行，为运行中的 程序换出内存页面的过程就不会停歇。

<span style="color:#3399ea"><strong>&nbsp;2. 软件程序管理 </strong></span>

**Linux操作系统将运行中的程序称为进程**。进程可以在前台运行，将输出显示在屏幕上，也 可以在后台运行，隐藏到幕后。内核控制着Linux系统如何管理运行在系统上的所有进程。

<span style="color:#f33b45"><strong>内核创建了第一个进程(称为init进程)来启动系统上所有其他进程。</strong></span>

当内核启动时，它会 将init进程加载到虚拟内存中。内核在启动任何其他进程时，都会在虚拟内存中给新进程分配一 块专有区域来存储该进程用到的数据和代码。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="279" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="0"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="278" role="region" aria-label=" 图像 小部件"><img alt="" height="99" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031164133124.png" src="https://img-blog.csdnimg.cn/20201031164133124.png" width="829" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031164133124.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22829%22%2C%22height%22%3A%2299%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>Linux操作系统有5个启动运行级。</strong></span>

- 运行级为1时，只启动基本的系统进程以及一个控制台终端进程。我们称之为单用户模式。 单用户模式通常用来在系统有问题时进行紧急的文件系统维护。
- 标准的启动运行级是3。在这个运行级上，大多数应用软件，比如网络支持程序，都会启动。
- 另一个Linux中常见的运行级是5。在这个运行级上系统会启动图形化的X Window系统

<!-- -->

<span style="color:#3399ea"><strong>3. 硬件设备管理 </strong></span>

内核的另一职责是管理硬件设备。任何Linux系统需要与之通信的设备，都需要在内核代码 中加入其驱动程序代码。驱动程序代码相当于应用程序和硬件设备的中间人，允许内核与设备之 间交换数据。在Linux内核中有两种方法用于插入设备驱动代码：

- <span style="color:#e579b6"><strong>编译进内核的设备驱动代码</strong></span>

- <span style="color:#e579b6"><strong>可插入内核的设备驱动模块</strong></span>


<!-- -->

**Linux系统将硬件设备当成特殊的文件**，称为设备文件。设备文件有3种分类：

**<span style="color:#e579b6"> 字符型设备文件  块设备文件  网络设备文件</span>

**

> 字符型设备文件是指处理数据时每次只能处理一个字符的设备。大多数类型的调制解调器和 终端都是作为字符型设备文件创建的。
> 
> 块设备文件是指处理数据时每次能处理大块数据的设备， 比如硬盘。
> 
> 网络设备文件是指采用数据包发送和接收数据的设备，包括各种网卡和一个特殊的回环设 备。这个回环设备允许Linux系统使用常见的网络编程协议同自身通信。

<span style="color:#3399ea"><strong>&nbsp;4. 文件系统管理 </strong></span>

不同于其他一些操作系统，Linux内核支持通过不同类型的文件系统从硬盘中读写数据。除 了自有的诸多文件系统外，Linux还支持从其他操作系统(比如Microsoft Windows)采用的文件 系统中读写数据。内核必须在编译时就加入对所有可能用到的文件系统的支持。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="277" role="region" aria-label=" 图像 小部件"><img alt="" height="530" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031164859508.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031164859508.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="756" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031164859508.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22756%22%2C%22height%22%3A%22530%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

Linux内核采用虚拟文件系统(Virtual File System，VFS)作为和每个文件系统交互的接口。 这为Linux内核同任何类型文件系统通信提供了一个标准接口。当每个文件系统都被挂载和使用 时，VFS将信息都缓存在内存中。

### GNU 工具

除了由内核控制硬件设备外，操作系统还需要工具来执行一些标准功能，比如控制文件和 程序。GNU组织(GNU是GNU’s Not Unix的缩写)开发了一套完整的Unix工具

<span style="color:#3399ea"><strong>1. 核心GNU工具 </strong></span>

GNU项目的主旨在于为Unix系统管理员设计出一套类似于Unix的环境。这个目标促使该项目 移植了很多常见的Unix系统命令行工具。供Linux系统使用的这组核心工具被称为coreutils(core utilities)软件包。

GNU coreutils软件包由三部分构成：<span style="color:#e579b6"><strong>  用以处理文件的工具  用以操作文本的工具  用以管理进程的工具</strong></span>

<span style="color:#3399ea"><strong>2. shell&nbsp;</strong></span>

GNU/Linux shell是一种特殊的交互式工具。它为用户提供了启动程序、管理文件系统中的文 件以及运行在Linux系统上的进程的途径。shell的核心是命令行提示符。命令行提示符是shell负责 交互的部分。它允许你输入文本命令，然后解释命令，并在内核中执行。

> shell包含了一组内部命令，用这些命令可以完成诸如复制文件、移动文件、重命名文件、显 示和终止系统中正运行的程序等操作。
> 
> 将多个shell命令放入文件中作为程序执行。这些文件被称作shell脚本。你在命令行 上执行的任何命令都可放进一个shell脚本中作为一组命令执行。
> 
> 在Linux系统上，通常有好几种Linux shell可用。不同的shell有不同的特性，有些更利于创建 脚本，有些则更利于管理进程。
> 
> 所有Linux发行版默认的shell都是bash shell。bash shell由GNU项 目开发，被当作标准Unix shell——Bourne shell(以创建者的名字命名)的替代品。bash shell的名 称就是针对Bourne shell的拼写所玩的一个文字游戏，称为Bourne again shell。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="276" role="region" aria-label=" 图像 小部件"><img alt="" height="180" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031170429635.png" src="https://img-blog.csdnimg.cn/20201031170429635.png" width="745" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031170429635.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22745%22%2C%22height%22%3A%22180%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



<span style="color:#3399ea"><strong>Linux 桌面环境&nbsp;</strong></span>

<span style="color:#e579b6"><strong>1. X Window系统&nbsp;2. KDE桌面&nbsp;3. GNOME桌面4. Unity桌面</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="275" role="region" aria-label=" 图像 小部件"><img alt="" height="203" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031170818255.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031170818255.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="740" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031170818255.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22740%22%2C%22height%22%3A%22203%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="274" role="region" aria-label=" 图像 小部件"><img alt="" height="203" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031170836690.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031170836690.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="744" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031170836690.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22744%22%2C%22height%22%3A%22203%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

## <span style="color:#3399ea">第2章走进shell</span>



输入命令setterm -inversescreen on 也可以 使用选项off关闭该特性。

**输入setterm –background white，然后按回车键， 接着输入setterm –foreground black**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="273" role="region" aria-label=" 图像 小部件"><img alt="" height="263" data-cke-saved-src="https://img-blog.csdnimg.cn/2020103117103884.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2020103117103884.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="867" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2020103117103884.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22867%22%2C%22height%22%3A%22263%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



## <span style="color:#3399ea">第3章基本的bash shell命令</span>



shell 提示符 默认bash shell提示符是美元符号($)，

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="272" role="region" aria-label=" 图像 小部件"><img alt="" height="584" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031171846125.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031171846125.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="650" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031171846125.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22650%22%2C%22height%22%3A%22584%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="271" role="region" aria-label=" 图像 小部件"><img alt="" height="599" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031171916766.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031171916766.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="720" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031171916766.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22720%22%2C%22height%22%3A%22599%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

表示标准输出和错误都不要，丢进黑洞，让他消失的无影无踪。/dev/null

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="270" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="1"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

**ls命令最基本的形式会显示当前目录下的文件和目录：**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="269" role="region" aria-label=" 图像 小部件"><img alt="" height="196" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031172315332.png" src="https://img-blog.csdnimg.cn/20201031172315332.png" width="830" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031172315332.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22830%22%2C%22height%22%3A%22196%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

如果没安装彩色终端仿真器，<span style="color:#f33b45"><strong>可用带-F参数的ls命令轻松区分文件和目录。</strong></span>

使用-F参数可 以得到如下输出：

\-F参数在目录名后加了正斜线(/)，以方便用户在输出中分辨它们。类似地，它会在可执行 文件(比如上面的my\_script文件)的后面加个星号，以便用户找出可在系统上运行的文件。

**把隐藏文件和普通文件及目录一起显示出来，就得用到-a参数。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="268" role="region" aria-label=" 图像 小部件"><img alt="" height="102" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031172420936.png" src="https://img-blog.csdnimg.cn/20201031172420936.png" width="781" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031172420936.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22781%22%2C%22height%22%3A%22102%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>-R参数是ls命令可用的另一个参数，叫作递归选项</strong></span>

的下述信息：

<span style="color:#e579b6"><strong> 文件类型，比如目录(d)、文件(-)、字符型文件(c)或块设备(b)；</strong></span>

<span style="color:#e579b6"><strong> 文件的权限(参见第6章)；</strong></span>

<span style="color:#e579b6"><strong> 文件的硬链接总数；</strong></span>

<span style="color:#e579b6"><strong> 文件属主的用户名；</strong></span>

<span style="color:#e579b6"><strong> 文件属组的组名； </strong></span>

<span style="color:#e579b6"><strong> 文件的大小(以字节为单位)； </strong></span>

<span style="color:#e579b6"><strong> 文件的上次修改时间；  文件名或目录名。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="267" role="region" aria-label=" 图像 小部件"><img alt="" height="288" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031172721146.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031172721146.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="830" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031172721146.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22830%22%2C%22height%22%3A%22288%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 过滤输出列表

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="266" role="region" aria-label=" 图像 小部件"><img alt="" height="211" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031173111987.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031173111987.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="717" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031173111987.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22717%22%2C%22height%22%3A%22211%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#e579b6"><strong> 问号(?)代表一个字符；  星号(*)代表零个或多个字符。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="265" role="region" aria-label=" 图像 小部件"><img alt="" height="249" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031173255903.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031173255903.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="832" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031173255903.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22832%22%2C%22height%22%3A%22249%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="264" role="region" aria-label=" 图像 小部件"><img alt="" height="314" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031173404786.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031173404786.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="757" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031173404786.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22757%22%2C%22height%22%3A%22314%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="263" role="region" aria-label=" 图像 小部件"><img alt="" height="213" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031173722485.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031173722485.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="828" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031173722485.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22828%22%2C%22height%22%3A%22213%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>touch命令创建了你指定的新文件，并将你的用户名作为文件的属主。注意，文件的大小是 零，因为touch命令只创建了一个空文件</strong></span>

。

touch命令还可用来改变文件的修改时间。这个操作并不需要改变文件的内容。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="262" role="region" aria-label=" 图像 小部件"><img alt="" height="215" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031174025689.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031174025689.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="757" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031174025689.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22757%22%2C%22height%22%3A%22215%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="261" role="region" aria-label=" 图像 小部件"><img alt="" height="175" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031174315385.png" src="https://img-blog.csdnimg.cn/20201031174315385.png" width="653" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031174315385.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22653%22%2C%22height%22%3A%22175%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### **复制文件**

<span style="color:#e579b6"><strong>最好是加上-i选项，强制shell询问是否需要覆盖已有文件。</strong></span>

<span style="color:#e579b6"><strong>cp命令的-R参数威力强大。可以用它在一条命令中递归地复制整个目录的内容。</strong></span>

<span style="color:#f33b45"><strong>-d只列出目录本身的信息，不列出其中的内容</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="260" role="region" aria-label=" 图像 小部件"><img alt="" height="196" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031174815461.png" src="https://img-blog.csdnimg.cn/20201031174815461.png" width="825" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031174815461.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22825%22%2C%22height%22%3A%22196%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**也可以在cp命令中使用通配符。**

### 链接文件

<span style="color:#e579b6"><strong> 符号链接  硬链接</strong></span>

**<span style="color:#f33b45">符号链接</span>

就是一个实实在在的文件，它指向存放在虚拟目录结构中某个地方的另一个文件。 这两个通过符号链接在一起的文件，彼此的内容并不相同。**<span style="color:#f33b45"><strong>用ln命令以及-s选项</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="259" role="region" aria-label=" 图像 小部件"><img alt="" height="233" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031175155504.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031175155504.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="770" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031175155504.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22770%22%2C%22height%22%3A%22233%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#3399ea"><strong>证明链接文件是独立文件的方法是查看inode编号&nbsp;</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="258" role="region" aria-label=" 图像 小部件"><img alt="" height="202" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031175628943.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031175628943.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="785" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031175628943.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22785%22%2C%22height%22%3A%22202%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#3399ea"><strong>硬链接会创建独立的虚拟文件，其中包含了原始文件的信息及位置。但是它们从根本上而言 是同一个文件。引用硬链接文件等同于引用了源文件。要</strong></span>

<span style="color:#f33b45"><strong>创建硬链接，原始文件也必须事先存在， 只不过这次使用ln命令时不再需要加入额外的参数了</strong></span>

<span style="color:#3399ea"><strong>。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="257" role="region" aria-label=" 图像 小部件"><img alt="" height="255" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031175914316.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031175914316.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="685" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031175914316.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22685%22%2C%22height%22%3A%22255%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="256" role="region" aria-label=" 图像 小部件"><img alt="" height="107" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031180055773.png" src="https://img-blog.csdnimg.cn/20201031180055773.png" width="697" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031180055773.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22697%22%2C%22height%22%3A%22107%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>带有硬链接的文件共享inode编号。这是因为它们终归是同一个文件</strong></span>

### 重命名文件

在Linux中，重命名文件称为移动(moving)。mv命令可以将文件和目录移动到另一个位置 或重新命名。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="255" role="region" aria-label=" 图像 小部件"><img alt="" height="233" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031180428332.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031180428332.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="646" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031180428332.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22646%22%2C%22height%22%3A%22233%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 删除文件

在Linux中，删除(deleting)叫作移除(removing)①。bash shell中删除文件的命令是rm。rm 命令的基本格式非常简单。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="254" role="region" aria-label=" 图像 小部件"><img alt="" height="202" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031180728235.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201031180728235.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="775" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031180728235.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22775%22%2C%22height%22%3A%22202%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 创建目录

在Linux中创建目录很简单，用mkdir命令即可：

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="253" role="region" aria-label=" 图像 小部件"><img alt="" height="94" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031180916583.png" src="https://img-blog.csdnimg.cn/20201031180916583.png" width="820" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031180916583.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22820%22%2C%22height%22%3A%2294%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 删除目录

删除目录的基本命令是rmdir。

默认情况下，rmdir命令只删除空目录。因为我们在New\_Dir目录下创建了一个文件my\_file， 所以rmdir命令拒绝删除目录。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="252" role="region" aria-label=" 图像 小部件"><img alt="" height="152" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031181255291.png" src="https://img-blog.csdnimg.cn/20201031181255291.png" width="597" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031181255291.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22597%22%2C%22height%22%3A%22152%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

一口气删除目录及其所有内容的终极大法就是使用带有-r参数和-f参数的rm命令。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="251" role="region" aria-label=" 图像 小部件"><img alt="" height="78" data-cke-saved-src="https://img-blog.csdnimg.cn/2020103118141539.png" src="https://img-blog.csdnimg.cn/2020103118141539.png" width="605" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2020103118141539.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22605%22%2C%22height%22%3A%2278%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 查看文件内容

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="250" role="region" aria-label=" 图像 小部件"><img alt="" height="152" data-cke-saved-src="https://img-blog.csdnimg.cn/20201031181810113.png" src="https://img-blog.csdnimg.cn/20201031181810113.png" width="826" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201031181810113.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22826%22%2C%22height%22%3A%22152%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 查看整个文件

**<span style="color:#f33b45">1. cat命令&nbsp;-n参数会给所有的行加上行号。&nbsp;只想给有文本的行加上行号，可以用-b参数。&nbsp;-T参数会用^I字符组合去替换文中的所有制表符。</span>

**

**<span style="color:#f33b45">2. more命令&nbsp;more命令只支持文本文件中的基本移动</span>

**

**<span style="color:#f33b45">3. less命令，能够翻页</span>

**

### 查看部分文件

<span style="color:#e579b6">1. tail命令</span>

 tail命令会显示文件最后几行的内容(文件的“尾部”)。默认情况下，它会显示文件的末 尾10行。 通过加入-n 2使 tail命令只显示文件的最后两行：

> **<span style="color:#f33b45">-f参数是tail命令的一个突出特性。它允许你在其他进程使用该文件时查看文件的内容。 tail命令会保持活动状态，并不断显示添加到文件中的内容。这是实时监测系统日志的绝妙 方式。</span>
> 
> **

<span style="color:#e579b6">2. head命令 head命令</span>

，顾名思义，会显示文件开头那些行的内容。

## <span style="color:#3399ea">&gt;第4章更多的bash shell</span>



### 监测程序

Linux系统中使用的<span style="color:#e579b6"><strong>GNU ps</strong></span>

命令支持3种不同类型的命令行参数：

<span style="color:#e579b6"><strong> Unix风格的参数，前面加单破折线；  BSD风格的参数，前面不加破折线；  GNU风格的长参数，前面加双破折线。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="249" role="region" aria-label=" 图像 小部件"><img alt="" height="702" data-cke-saved-src="https://img-blog.csdnimg.cn/20201101172359833.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201101172359833.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="619" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201101172359833.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22619%22%2C%22height%22%3A%22702%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="248" role="region" aria-label=" 图像 小部件"><img alt="" height="255" data-cke-saved-src="https://img-blog.csdnimg.cn/20201101172616484.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201101172616484.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="834" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201101172616484.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22834%22%2C%22height%22%3A%22255%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#e579b6"><strong>&nbsp; UID：启动这些进程的用户。 </strong></span>

<span style="color:#e579b6"><strong> PID：进程的进程ID。 </strong></span>

<span style="color:#e579b6"><strong> PPID：父进程的进程号(如果该进程是由另一个进程启动的)。 </strong></span>

<span style="color:#e579b6"><strong> C：进程生命周期中的CPU利用率。  STIME：进程启动时的系统时间。 </strong></span>

<span style="color:#e579b6"><strong> TTY：进程启动时的终端设备。 </strong></span>

<span style="color:#e579b6"><strong> TIME：运行进程需要的累计CPU时间。</strong></span>

<span style="color:#e579b6"><strong> CMD：启动的程序名称。</strong></span>

> 上例中输出了合理数量的信息，这也正是大多数系统管理员希望看到的。如果想要获得更多 的信息<span style="color:#f33b45"><strong>，可采用-l参数，</strong></span>
> 
> 它会产生一个长格式输出。

<span style="color:#f33b45"><strong>2. BSD风格的参数&nbsp;</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="247" role="region" aria-label=" 图像 小部件"><img alt="" height="762" data-cke-saved-src="https://img-blog.csdnimg.cn/20201101173124698.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201101173124698.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="618" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201101173124698.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22618%22%2C%22height%22%3A%22762%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="246" role="region" aria-label=" 图像 小部件"><img alt="" height="128" data-cke-saved-src="https://img-blog.csdnimg.cn/20201101173455307.png" src="https://img-blog.csdnimg.cn/20201101173455307.png" width="821" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201101173455307.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22821%22%2C%22height%22%3A%22128%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**<span style="color:#e579b6"> &lt;：该进程运行在高优先级上。</span>

**

**<span style="color:#e579b6"> N：该进程运行在低优先级上。</span>

**

**<span style="color:#e579b6"> L：该进程有页面锁定在内存中。</span>

**

**<span style="color:#e579b6"> s：该进程是控制进程。</span>

**

**<span style="color:#e579b6"> l：该进程是多线程的。</span>

**

**<span style="color:#e579b6"> +：该进程运行在前台。</span>

**

<span style="color:#f33b45"><strong>3. GNU长参数&nbsp;</strong></span>

GNU开发人员在这个新改进过的ps命令中加入了另外一些参数。其中一些GNU长参数 复制了现有的Unix或BSD类型的参数，而另一些则提供了新功能。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="245" role="region" aria-label=" 图像 小部件"><img alt="" height="607" data-cke-saved-src="https://img-blog.csdnimg.cn/20201101173654337.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201101173654337.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="656" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201101173654337.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22656%22%2C%22height%22%3A%22607%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>实时监测进程 top命令跟ps命令相似，能够显示进程信息，但它是实时显 示的</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="244" role="region" aria-label=" 图像 小部件"><img alt="" height="332" data-cke-saved-src="https://img-blog.csdnimg.cn/20201101173824774.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201101173824774.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="815" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201101173824774.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22815%22%2C%22height%22%3A%22332%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#e579b6"><strong> PID：进程的ID。</strong></span>

<span style="color:#e579b6"><strong> USER：进程属主的名字。</strong></span>

<span style="color:#e579b6"><strong> PR：进程的优先级。</strong></span>

<span style="color:#e579b6"><strong> NI：进程的谦让度值。</strong></span>

<span style="color:#e579b6"><strong> VIRT：进程占用的虚拟内存总量。</strong></span>

<span style="color:#e579b6"><strong> RES：进程占用的物理内存总量。</strong></span>

<span style="color:#e579b6"><strong> SHR：进程和其他进程共享的内存总量。 </strong></span>

<span style="color:#e579b6"><strong> S：进程的状态(D代表可中断的休眠状态，R代表在运行状态，S代表休眠状态，T代表 跟踪状态或停止状态，Z代表僵化状态)。</strong></span>

<span style="color:#e579b6"><strong> %CPU：进程使用的CPU时间比例。</strong></span>

**<span style="color:#e579b6"> %MEM：进程使用的内存占可用内存的比例。</span>

**

**<span style="color:#e579b6"> TIME+：自进程启动到目前为止的CPU时间总量。 </span>

**

**<span style="color:#e579b6"> COMMAND：进程所对应的命令行名称，也就是启动的程序名</span>

**

> top命令在启动时会按照%CPU值对进程排序。可以在top运行时使用多种交互 命令重新排序。每个交互式命令都是单字符，在top命令运行时键入可改变top的行为。键入f允 许你选择对输出进行排序的字段，键入d允许你修改轮询间隔。键入q可以退出top。用户在top 命令的输出上有很大的控制权。

**结束进程**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="243" role="region" aria-label=" 图像 小部件"><img alt="" height="256" data-cke-saved-src="https://img-blog.csdnimg.cn/20201101174210416.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201101174210416.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="702" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201101174210416.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22702%22%2C%22height%22%3A%22256%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#e579b6"><strong>1. </strong></span>

**kill**<span style="color:#e579b6"><strong>命令 kill命令可通过进程ID(PID)给进程发信号。默认情况下，kill命令会向命令行中列出的 全部PID发送一个TERM信号</strong></span>

<span style="color:#e579b6"><strong>2. </strong></span>

**killall**<span style="color:#e579b6"><strong>命令 killall命令非常强大，它支持通过进程名而不是PID来结束进程。killall命令也支持通 配符</strong></span>

，

### 监测磁盘空间

Linux文件系统将所有的磁盘都并入一个虚拟目录下。在使用新的存储媒 体之前，需要把它放到虚拟目录下。这项工作称为挂载(mounting)。Linux发行版都能自动挂载特定类型的可移动存储媒体。

**挂载存储媒体** 1. <span style="color:#f33b45"><strong>mount</strong></span>

命令 Linux上用来挂载媒体的命令叫作mount。默认情况下，mount命令会输出当前系统上挂载的 设备列表。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="242" role="region" aria-label=" 图像 小部件"><img alt="" height="296" data-cke-saved-src="https://img-blog.csdnimg.cn/20201103195821403.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201103195821403.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="828" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201103195821403.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22828%22%2C%22height%22%3A%22296%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

mount命令提供如下四部分信息：  媒体的设备文件名  媒体挂载到虚拟目录的挂载点  文件系统类型  已挂载媒体的访问状态

 vfat：Windows长文件系统。

 ntfs：Windows NT、XP、Vista以及Windows 7中广泛使用的高级文件系统。

 iso9660：标准CD-ROM文件系统。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="241" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="2"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

type参数指定了磁盘被格式化的文件系统类型。Linux可以识别非常多的文件系统类型。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="240" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="3"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span style="color:#f33b45"><strong>umount命令</strong></span>

 从Linux系统上移除一个可移动设备时，不能直接从系统上移除，而应该先卸载。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="239" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="4"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span style="color:#f33b45"><strong>使用 df 命令</strong></span>

 有时你需要知道在某个设备上还有多少磁盘空间。df命令可以让你很方便地查看所有已挂载 磁盘的使用情况。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="238" role="region" aria-label=" 图像 小部件"><img alt="" height="156" data-cke-saved-src="https://img-blog.csdnimg.cn/20201109200240936.png" src="https://img-blog.csdnimg.cn/20201109200240936.png" width="841" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201109200240936.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22841%22%2C%22height%22%3A%22156%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="237" role="region" aria-label=" 图像 小部件"><img alt="" height="145" data-cke-saved-src="https://img-blog.csdnimg.cn/20201109200524757.png" src="https://img-blog.csdnimg.cn/20201109200524757.png" width="560" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201109200524757.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22560%22%2C%22height%22%3A%22145%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



<span style="color:#f33b45"><strong>&nbsp;du 命令</strong></span>

 du命令可以显示某个特定目录(默认情况下是当前目录)的 磁盘使用情况。这一方法可用来快速判断系统上某个目录下是不是有超大文件。

 -c：显示所有已列出文件总的大小。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="236" role="region" aria-label=" 图像 小部件"><img alt="" height="166" data-cke-saved-src="https://img-blog.csdnimg.cn/20201109201213396.png" src="https://img-blog.csdnimg.cn/20201109201213396.png" width="478" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201109201213396.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22478%22%2C%22height%22%3A%22166%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

 -h：按用户易读的格式输出大小，即用K替代千字节，用M替代兆字节，用G替代吉字 节。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="235" role="region" aria-label=" 图像 小部件"><img alt="" height="173" data-cke-saved-src="https://img-blog.csdnimg.cn/20201109201243578.png" src="https://img-blog.csdnimg.cn/20201109201243578.png" width="368" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201109201243578.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22368%22%2C%22height%22%3A%22173%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

 -s：显示每个输出参数的总计。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="234" role="region" aria-label=" 图像 小部件"><img alt="" height="59" data-cke-saved-src="https://img-blog.csdnimg.cn/20201109201307566.png" src="https://img-blog.csdnimg.cn/20201109201307566.png" width="372" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201109201307566.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22372%22%2C%22height%22%3A%2259%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#3399ea">处理数据文件</span>



排序，处理大量数据时的一个常用命令是sort命令

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="233" role="region" aria-label=" 图像 小部件"><img alt="" height="273" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113005909541.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113005909541.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="578" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113005909541.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22578%22%2C%22height%22%3A%22273%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

数字排序使用 -n 参数，告诉sort命令把数字识别成数字而不是字符

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="232" role="region" aria-label=" 图像 小部件"><img alt="" height="329" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113010305924.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113010305924.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="708" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113010305924.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22708%22%2C%22height%22%3A%22329%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

使用 -M 参数按照月份排序。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="231" role="region" aria-label=" 图像 小部件"><img alt="" height="677" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113010509949.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113010509949.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="840" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113010509949.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22840%22%2C%22height%22%3A%22677%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="230" role="region" aria-label=" 图像 小部件"><img alt="" height="220" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113010735668.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113010735668.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="716" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113010735668.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22716%22%2C%22height%22%3A%22220%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



\-k和-t参数在对按字段分隔的数据进行排序时非常有用，例如/etc/passwd文件。可以用-t 参数来指定字段分隔符，然后用-k参数来指定排序的字段。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="229" role="region" aria-label=" 图像 小部件"><img alt="" height="217" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113010934119.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113010934119.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="706" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113010934119.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22706%22%2C%22height%22%3A%22217%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#3399ea">搜索数据&nbsp;</span>



grep [options] pattern [file]

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="228" role="region" aria-label=" 图像 小部件"><img alt="" height="260" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113011604626.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113011604626.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="832" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113011604626.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22832%22%2C%22height%22%3A%22260%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#3399ea">压缩数据</span>



<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="227" role="region" aria-label=" 图像 小部件"><img alt="" height="146" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113011650834.png" src="https://img-blog.csdnimg.cn/20201113011650834.png" width="726" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113011650834.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22726%22%2C%22height%22%3A%22146%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

gzip软件包是GNU项目的产物，意在编写一个能够替代原先Unix中compress工具的免费版 本。

<span style="color:#86ca5e"><strong> gzip：用来压缩文件。  gzcat：用来查看压缩过的文本文件的内容。  gunzip：用来解压文件。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="226" role="region" aria-label=" 图像 小部件"><img alt="" height="296" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113012125303.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113012125303.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="825" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113012125303.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22825%22%2C%22height%22%3A%22296%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



归档数据,tar命令最开始是用来将文件写到磁带设备上归档的

tar function [options] object1 object2 ...

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="225" role="region" aria-label=" 图像 小部件"><img alt="" height="309" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113012344864.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113012344864.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="883" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113012344864.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22883%22%2C%22height%22%3A%22309%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="224" role="region" aria-label=" 图像 小部件"><img alt="" height="297" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113012535112.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113012535112.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="934" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113012535112.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22934%22%2C%22height%22%3A%22297%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="223" role="region" aria-label=" 图像 小部件"><img alt="" height="304" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113013031686.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113013031686.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="1012" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113013031686.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%221012%22%2C%22height%22%3A%22304%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="222" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="5"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="221" role="region" aria-label=" 图像 小部件"><img alt="" height="468" data-cke-saved-src="https://img-blog.csdnimg.cn/2020111301371767.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2020111301371767.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="936" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2020111301371767.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22936%22%2C%22height%22%3A%22468%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="220" role="region" aria-label=" 图像 小部件"><img alt="" height="191" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113014455292.png" src="https://img-blog.csdnimg.cn/20201113014455292.png" width="932" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113014455292.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22932%22%2C%22height%22%3A%22191%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



tar -zxvf filename.tgz 解压

## <span style="color:#3399ea">&gt;第5章理解shell</span>



> 默认的交互shell会在用户登录某个虚拟控制台终端或在GUI中运行终端仿真器时启动。不过 还有另外一个默认shell是/bin/sh，它作为默认的系统shell，用于那些需要在启动时使用的系统shell 脚本。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="219" role="region" aria-label=" 图像 小部件"><img alt="" height="493" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113205539591.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113205539591.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="944" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113205539591.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22944%22%2C%22height%22%3A%22493%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### shell 的父子关系

用于登录某个虚拟控制器终端或在GUI中运行终端仿真器时所启动的默认的<span style="color:#f33b45"><strong>交互shell，是一 个父shell。</strong></span>

在CLI提示符后输入<span style="color:#f33b45"><strong>/bin/bash命令或其他等效的bash命令时，会创建一个新的shell程序。 这个shell程序被称为子shell(child shell)。子shell也拥有CLI提示符，同样会等待命令输入。</strong></span>

当输入bash、生成子shell的时候，你是看不到任何相关的信息的，

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="218" role="region" aria-label=" 图像 小部件"><img alt="" height="353" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113210052703.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113210052703.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="914" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113210052703.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22914%22%2C%22height%22%3A%22353%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="217" role="region" aria-label=" 图像 小部件"><img alt="" height="378" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113210206360.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113210206360.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="942" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113210206360.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22942%22%2C%22height%22%3A%22378%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> 输入命令bash之后，一个子shell就出现了。第二个ps -f是在子shell中执行的。可以从显示 结果中看到有两个bash shell程序在运行。第一个bash shell程序，也就是父shell进程

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="216" role="region" aria-label=" 图像 小部件"><img alt="" height="332" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113210247350.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113210247350.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="708" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113210247350.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22708%22%2C%22height%22%3A%22332%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

在生成子shell进程时，只有部分父进程的环境被复制到子shell环境中。这会对包括变量在内 的一些东西造成影响。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="215" role="region" aria-label=" 图像 小部件"><img alt="" height="466" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113210605294.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113210605294.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="904" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113210605294.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22904%22%2C%22height%22%3A%22466%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="214" role="region" aria-label=" 图像 小部件"><img alt="" height="586" data-cke-saved-src="https://img-blog.csdnimg.cn/202011132106550.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/202011132106550.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="713" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F202011132106550.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22713%22%2C%22height%22%3A%22586%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="213" role="region" aria-label=" 图像 小部件"><img alt="" height="226" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113210727294.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113210727294.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="795" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113210727294.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22795%22%2C%22height%22%3A%22226%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>exit命令不仅能退出子shell，还能用来登出当前的虚拟控制台终端或终端仿真器软件</strong></span>

。只需 要在父shell中输入exit，就能够从容退出CLI了。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="212" role="region" aria-label=" 图像 小部件"><img alt="" height="348" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113211033278.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113211033278.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="812" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113211033278.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22812%22%2C%22height%22%3A%22348%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> **进程列表:所有的命令依次执行，**不存在任何问题。不过这并不是进程列表。<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="211" role="region" aria-label=" 图像 小部件"><img alt="" height="374" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113211430533.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113211430533.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="938" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113211430533.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22938%22%2C%22height%22%3A%22374%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>命令列表要想成为进程列表</strong></span>

，这些命令必须包含在括号里,括号的加入使 命令列表变成了进程列表，生成了一个子shell来执行对应的命令。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="210" role="region" aria-label=" 图像 小部件"><img alt="" height="348" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113211943812.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113211943812.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="905" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113211943812.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22905%22%2C%22height%22%3A%22348%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> **进程列表是一种命令分组(command grouping)**。另一种命令分组是将命令放入花括号中， 并在命令列表尾部加上分号(;)。语法为{ command; }。<span style="color:#3399ea"><strong>使用花括号进行命令分组并不 会像进程列表那样创建出子shell。</strong></span>

<span style="color:#f33b45"><strong>要想知道是否生成了子shell。得借助一个使用了环境变量的命令。如果该命令返回0，就表明没有子shell。如果返回 1或者其他更大的数字，就表明存在子shell。</strong></span>

> <span style="color:#3399ea"><strong>echo $BASH_SUBSHELL。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="209" role="region" aria-label=" 图像 小部件"><img alt="" height="263" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113213205524.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113213205524.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="940" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113213205524.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22940%22%2C%22height%22%3A%22263%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>别出心裁的子 shell 用法,&nbsp;在交互式shell中，一个高效的子shell用法就是使用后台模式</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="208" role="region" aria-label=" 图像 小部件"><img alt="" height="493" data-cke-saved-src="https://img-blog.csdnimg.cn/20201113214730780.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201113214730780.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="944" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201113214730780.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22944%22%2C%22height%22%3A%22493%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

在CLI中运用子shell的创造性方法之一就是将进程列表置入后台模式。**你既可以在子shell中 进行繁重的处理工作，同时也不会让子shell的I/O受制于终端。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="207" role="region" aria-label=" 图像 小部件"><img alt="" height="167" data-cke-saved-src="https://img-blog.csdnimg.cn/20201114120638263.png" src="https://img-blog.csdnimg.cn/20201114120638263.png" width="948" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201114120638263.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22948%22%2C%22height%22%3A%22167%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**将进程列表置入后台模式并不是子shell在CLI中仅有的创造性用法。协程就是另一种方法。**

> <span style="color:#f33b45"><strong>协程 协程可以同时做两件事</strong></span>
> 
> 。它在后台生成一个子shell，并在这个子shell中执行命令。 要进行协程处理，得使用<span style="color:#7c79e5"><strong>coproc</strong></span>
> 
> 命令，还有要在子shell中执行的命令。

** 创建子shell之外，协程基本上就是将命令置入后台模式。当输入coproc命令及其参 数之后，你会发现启用了一个后台作业。屏幕上会显示出后台作业号(1)以及进程ID(2544)。<span style="color:#f33b45"> jobs</span>

命令能够显示出协程的处理状态。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="206" role="region" aria-label=" 图像 小部件"><img alt="" height="319" data-cke-saved-src="https://img-blog.csdnimg.cn/20201114121749991.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201114121749991.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="666" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201114121749991.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22666%22%2C%22height%22%3A%22319%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> 必须确保在第一个花括号({)和命令名之间有一个空格。还必须保证命令以分号(;)结 尾。另外，分号和闭花括号(})之间也得有一个空格。

<span style="color:#f33b45"><strong>&nbsp;将协程与进程列表结合起来产生嵌套的子shell</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="205" role="region" aria-label=" 图像 小部件"><img alt="" height="170" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116013829838.png" src="https://img-blog.csdnimg.cn/20201116013829838.png" width="734" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116013829838.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22734%22%2C%22height%22%3A%22170%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> 协程能够让你尽情发挥想象力，发送或接收来自子shell中进程的信息。只有在拥有多个协 程的时候才需要对协程进行命名，因为你得和它们进行通信。否则的话，让coproc命令 将其设置成默认的名字COPROC就行了。

<span style="color:#3399ea"><strong>&nbsp;协程能够让你尽情发挥想象力，发送或接收来自子shell中进程的信息。只有在拥有多个协 程的时候才需要对协程进行命名，因为你得和它们进行通信。否则的话，让coproc命令 将其设置成默认的名字COPROC就行了。</strong></span>

### 理解 shell 的内建命令

内建命令和非内建命令的操作方式大不相同。

外部命令：也被称为文件系统命令，是存在于bash shell之外的程序。它们并不是shell 程序的一部分。外部命令程序通常位于/bin、/usr/bin、/sbin或/usr/sbin中。

ps就是一个外部命令。你可以使用which和type命令找到它。作为外部命令，ps命令执行时会创建出一个子进程

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="204" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="6"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

当外部命令执行时，会创建出一个子进程。这种操作被称为衍生(forking)。外部命令ps很 方便显示出它的父进程以及自己所对应的衍生子进程。当进程必须执行衍生操作时，它需要花费时间和精力来设置新子进程的环境

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="203" role="region" aria-label=" 图像 小部件"><img alt="" height="232" data-cke-saved-src="https://img-blog.csdnimg.cn/2020111601451874.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2020111601451874.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="530" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2020111601451874.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22530%22%2C%22height%22%3A%22232%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**<span style="color:#f33b45">内建命令</span>

 ； 的区别在不需要使用子进程来执行。它们已经和shell编译成了一 体，作为shell工具的组成部分存在。不需要借助外部程序文件来运行。内建命令的执行速度要更 快，效率也更高**

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="202" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="7"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

使用history命令 。bash shell会跟踪你用过的命令。你可以唤回这些命令 并重新使用

输入!!，然后按回 车键就能够唤出刚刚用过的那条命令来使用。

命令历史记录被保存在隐藏文件.bash\_history中，它位于用户的主目录中。这里要注意的是，

1. **bash命令的历史记录是先存放在内存中，当shell退出时才被写入到历史文件中。**
2. **要实现强制写入，需 要使用history命令的-a选项，**
3. ** 要想强制重新读 取.bash\_history文件，更新终端会话的历史记录，可以使用history -n命令。**
4. 唤回历史列表中任意一条命令。只需输入惊叹号和命令在历史列表中的编号即可。

<!-- -->

**<span style="color:#f33b45">命令别名</span>

 alias命令是另一个shell的内建命令。命令别名允许你为常用的命令(及其参数)创建另一 个名称，从而将输入量减少到最低。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="201" role="region" aria-label=" 图像 小部件"><img alt="" height="240" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116195021441.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201116195021441.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="523" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116195021441.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22523%22%2C%22height%22%3A%22240%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="200" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="8"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

因为命令别名属于内部命令，一个别名仅在它所被定义的shell进程中才有效。

## <span style="color:#3399ea">&gt;第6章使用Linux环境变量</span>



bash shell用一个叫作环境变量(environment variable)的特性来存储有关shell会话和工作环 境的信息(这也是它们被称作环境变量的原因)。这项特性允许你在内存中存储数据，以便程序 或shell中运行的脚本能够轻松访问到它们。

 全局变量  局部变量 全局环境变量 <span style="color:#f33b45"><strong>全局环境变量对于shell会话和所有生成的子shell都是可见的</strong></span>

。局部变量则只对创建它们的 shell可见

要查看全局变量，可以使用env或printenv命令。要显示个别环境变量的值，可以使用printenv命令，但是不要用env命令。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="199" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="9"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

局部环境变量 。set命令会显示为某个特定进程设置的所有环境变量，包括局部变量、全局变量 以及用户定义变量。

### 设置用户定义变量

<span style="color:#f33b45"><strong>设置局部用户定义变量&nbsp;</strong></span>

如果要给变量赋一个含有空格的字符串值，必须用单引号来界定字符串的首和尾。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="198" role="region" aria-label=" 图像 小部件"><img alt="" height="104" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116020758202.png" src="https://img-blog.csdnimg.cn/20201116020758202.png" width="611" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116020758202.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22611%22%2C%22height%22%3A%22104%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

如果是你自己创建的局 部变量或是shell脚本，请使用小写字母

**<span style="color:#f33b45">设置全局环境变量</span>

**这个过程通过export命令来完成，变量名前面不需要加$。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="197" role="region" aria-label=" 图像 小部件"><img alt="" height="349" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116021212772.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201116021212772.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="482" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116021212772.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22482%22%2C%22height%22%3A%22349%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 删除环境变量

可以用unset命令 完成这个操作。在unset命令中引用环境变量时，记住不要使用$。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="196" role="region" aria-label=" 图像 小部件"><img alt="" height="70" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116021348502.png" src="https://img-blog.csdnimg.cn/20201116021348502.png" width="534" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116021348502.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22534%22%2C%22height%22%3A%2270%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 默认的 shell 环境变量

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="195" role="region" aria-label=" 图像 小部件"><img alt="" height="351" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116021514427.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201116021514427.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="880" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116021514427.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22880%22%2C%22height%22%3A%22351%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="194" role="region" aria-label=" 图像 小部件"><img alt="" height="893" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116021543569.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201116021543569.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="881" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116021543569.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22881%22%2C%22height%22%3A%22893%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



### 设置 PATH 环境变量

PATH环境变量定义了用于进行命令和程序查找的目录。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="193" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="10"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="192" role="region" aria-label=" 图像 小部件"><img alt="" height="259" data-cke-saved-src="https://img-blog.csdnimg.cn/2020111602192070.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2020111602192070.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="753" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2020111602192070.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22753%22%2C%22height%22%3A%22259%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> **<span style="color:#f33b45">&nbsp;如果希望子shell也能找到你的程序的位置，一定要记得把修改后的PATH环境变量导出</span>
> 
> **

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="191" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="11"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

### 定位系统环境变量 

登入Linux系统启动一个bash shell时，默认情况下bash会在几个文件中查找命令。<span style="color:#f33b45"><strong>这些文件叫作启动文件或环境文件</strong></span>

。**bash检查的启动文件取决于你启动bash shell的方式。**启动bash shell有3种方式：

- **登录时作为默认登录shell**
- **作为非登录shell的交互式shell **
- **作为运行脚本的非交互shell**

<!-- -->

> 登录shell会从5个不同的启动文件里 读取命令：

1. <span style="color:#f33b45"><strong>/etc/profile </strong></span>

2. <span style="color:#f33b45"><strong>$HOME/.bash_profile </strong></span>

3. <span style="color:#f33b45"><strong>$HOME/.bashrc </strong></span>

4. <span style="color:#f33b45"><strong>$HOME/.bash_login </strong></span>

5. <span style="color:#f33b45"><strong>$HOME/.profile</strong></span>


<!-- -->

<span style="color:#f33b45"><strong>/etc/profile文件是系统上默认的bash shell的主启动文件</strong></span>

。系统上的每个用户登录时都会执行 这个启动文件。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="190" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="12"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span style="color:#f33b45"><strong>：for语句。它用来迭代/etc/profile.d目录&nbsp;</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="189" role="region" aria-label=" 图像 小部件"><img alt="" height="297" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116194225815.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201116194225815.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="770" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116194225815.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22770%22%2C%22height%22%3A%22297%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#7c79e5">大部分应用都会创建两个启动文件：一个供 bash shell使用(使用.sh扩展名)，一个供c shell使用(使用.csh扩展名)</span>

。

**$HOME目录下的启动文件**：提供一个用户专属的启动文件来定义该用户所用到的环 境变量。大多数Linux发行版只用这四个启动文件中的一到两个：

**交互式 shell 进程 **的bash shell不是登录系统时启动的(比如是在命令行提示符下敲入bash时启动)，那 么你启动的shell叫作交互式shell，

**交互式shell启动**不会访问<span style="color:#f33b45"><strong>/etc/profile文件</strong></span>

，<span style="color:#f33b45"><strong>只会检查用户</strong></span>

**HOME**<span style="color:#f33b45"><strong>目录 中的</strong></span>

**.bashrc**<span style="color:#f33b45"><strong>文件</strong></span>

。.bashrc文件有两个作用：

- **一是查看/etc目录下通用的bashrc文件，**
- **二是为用户提供一个定制自 己的命令别名和私有脚本函数的地方。**<br>

    <span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="188" role="region" aria-label=" 图像 小部件"><img alt="" height="240" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116195110875.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201116195110875.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="523" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116195110875.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22523%22%2C%22height%22%3A%22240%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>


<!-- -->

> **<span style="color:#f33b45">非交互式 shell</span>
> 
> ** 系统执行shell脚本时用的就是这种shell。不同的地方在于它 没有命令行提示符 bash shell提供了BASH\_ENV环境变量。<span style="color:#7c79e5"><strong>当shell启动一个非交互式shell进 程时，</strong></span>
> 
> <span style="color:#f33b45"><strong>它会检查这个环境变量来查看要执行的启动文件</strong>。如果有指定的文件，shell会执行该文件 里的命令</span>

**环境变量持久化**

对全局环境变量来说(Linux系统中所有用户都需要使用的变量)，可能更倾向于将新的或修 改过的变量设置放在/etc/profile文件中

**最好是在/etc/profile.d目录中创建一个以.sh结尾的文件。把所有新的或修改过的全局环境变 量设置放在这个文件中。**

在大多数发行版中，存**储个人用户永久性bash shell变量的地方是$HOME/.bashrc文件**

### 数组变量

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="187" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="13"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

## <span style="color:#3399ea">&gt;第7章理解Linux文件权限</span>



### <span style="color:#f33b45">Linux 的安全性</span>



用户权限是通过创建用户时分配的用户ID(User ID，通常缩写为UID)来跟踪的。

/etc/passwd 文件

系统账户，是系统上运行的各种服务进程访问资源用的特殊账户。所有运行在后台的服务都需要用一个 系统用户账户登录到Linux系统上。Linux为系统账户预留了1000以下的UID值。

绝大多数Linux系统都将用户密码保存在另一个单独的文件中<span style="color:#f33b45"><strong>(叫作shadow文件，位置 在/etc/shadow)</strong></span>

。只有特定的程序(比如登录程序)才能访问这个文件。

- <span style="color:#f33b45"><strong> 登录用户名 </strong></span>

- <span style="color:#f33b45"><strong> 用户密码 </strong></span>

- <span style="color:#f33b45"><strong> 用户账户的UID(数字形式) </strong></span>

- <span style="color:#f33b45"><strong> 用户账户的组ID(GID)(数字形式) </strong></span>

- <span style="color:#f33b45"><strong> 用户账户的文本描述(称为备注字段) </strong></span>

- <span style="color:#f33b45"><strong> 用户HOME目录的位置 </strong></span>

- <span style="color:#f33b45"><strong> 用户的默认shell</strong></span>


<!-- -->

> <div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="186" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="14"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>
> 
> <br>

<span style="color:#f33b45"><strong>添加新用户</strong></span>

 以使用加入了-D选项的<span style="color:#f33b45"><strong>useradd</strong></span>

 命令查看所用Linux系统中的这些默认值。 可以在-D选项后跟上一个指定的值来修改系统默认的新用户设置

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="185" role="region" aria-label=" 图像 小部件"><img alt="" height="188" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116211654595.png" src="https://img-blog.csdnimg.cn/20201116211654595.png" width="823" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116211654595.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22823%22%2C%22height%22%3A%22188%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="184" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="15"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="183" role="region" aria-label=" 图像 小部件"><img alt="" height="366" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116211544677.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201116211544677.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="868" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116211544677.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22868%22%2C%22height%22%3A%22366%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="182" role="region" aria-label=" 图像 小部件"><img alt="" height="143" data-cke-saved-src="https://img-blog.csdnimg.cn/2020111621161135.png" src="https://img-blog.csdnimg.cn/2020111621161135.png" width="840" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2020111621161135.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22840%22%2C%22height%22%3A%22143%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**<span style="color:#f33b45">删除用户 </span>

userdel可以满足这个需求**。默认情况下，userdel命令会只 删除/etc/passwd文件中的用户信息，而不会删除系统中属于该账户的任何文件。 <span style="color:#f33b45"><strong>如果加上-r参数，userdel会删除用户的HOME目录以及邮件目录</strong></span>

<span style="color:#f33b45"><strong>修改用户 </strong></span>

usermod<span style="color:#f33b45"><strong></strong></span>

它能用来修改/etc/passwd文件中的大部 分字段， <span style="color:#7c79e5"><strong> -l修改用户账户的登录名。  -L锁定账户，使用户无法登录。  -p修改账户的密码。  -U解除锁定，使用户能够登录</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="181" role="region" aria-label=" 图像 小部件"><img alt="" height="220" data-cke-saved-src="https://img-blog.csdnimg.cn/20201116211833334.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201116211833334.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="744" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201116211833334.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22744%22%2C%22height%22%3A%22220%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> \-e选项能强制用户下次登录时修改密码。你可以先给用户设置一个简单的密码，之后再强制 在下次登录时改成他们能记住的更复杂的密码

<span style="color:#f33b45"><strong>&nbsp;chpasswd命令</strong></span>

能从 标准输入自动读取登录名和密码对(由冒号分割)列表，给密码加密，然后为用户账户设置。你 也可以用重定向命令来将含有userid:passwd对的文件重定向给该命令。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="180" role="region" aria-label=" 图像 小部件"><img alt="" height="135" data-cke-saved-src="https://img-blog.csdnimg.cn/2021021811123086.png" src="https://img-blog.csdnimg.cn/2021021811123086.png" width="680" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021021811123086.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22680%22%2C%22height%22%3A%22135%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="179" role="region" aria-label=" 图像 小部件"><img alt="" height="38" data-cke-saved-src="https://img-blog.csdnimg.cn/20210218111240682.png" src="https://img-blog.csdnimg.cn/20210218111240682.png" width="335" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210218111240682.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22335%22%2C%22height%22%3A%2238%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

chsh、chfn和chage工具专门用来修改特定的账户信息。

chsh命令用来快速修改默认的用 户登录shell。使用时必须用shell的全路径名作为参数，不能只用shell名。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="178" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="16"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

chfn命令提供了在/etc/passwd文件的备注字段中存储信息的标准方法。chfn命令会将用于 Unix的finger命令的信息存进备注字段，而不是简单地存入一些随机文本(比如名字或昵称之 类的)，或是将备注字段留空。finger命令可以非常方便地查看Linux系统上的用户信息

> 出于安全性考虑，很多Linux系统管理员会在系统上禁用finger命令，不少Linux发行版 甚至都没有默认安装该命令

<span style="color:#f33b45"><strong>chage命令用来帮助管理用户账户的有效期。你需要对每个值设置多个参数，??? 没懂</strong></span>

### 使用 Linux 组

每个组都有唯一的GID——跟UID类似，在系统上这是个唯一的数值。除了GID，每个组还 有唯一的组名。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="177" role="region" aria-label=" 图像 小部件"><img alt="" height="299" data-cke-saved-src="https://img-blog.csdnimg.cn/20201118194138600.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201118194138600.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="745" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201118194138600.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22745%22%2C%22height%22%3A%22299%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

组密码允许非组内成员通过它临时成为该组成员。这个功能并不很普遍，但确实存在。

<span style="color:#f33b45"><strong>创建新组：</strong></span>

 groupadd命令可在系统上创建新组。 在创建新组时，默认没有用户被分配到该组。groupadd命令没有提供将用户添加到组中的 选项，但可以用usermod命令来弥补这一点。

<span style="color:#f33b45"><strong>修改组</strong></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="176" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="17"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

###  理解文件权限 

使用文件权限符

<span style="color:#e579b6"><strong> -代表文件  d代表目录  l代表链接  c代表字符型设备  b代表块设备  n代表网络设备</strong></span>

<span style="color:#f33b45"><strong> r代表对象是可读的  w代表对象是可写的 x代表对象是可执行的 对象的属主  对象的属组  系统其他用户</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="175" role="region" aria-label=" 图像 小部件"><img alt="" height="197" data-cke-saved-src="https://img-blog.csdnimg.cn/20201118202901648.png" src="https://img-blog.csdnimg.cn/20201118202901648.png" width="556" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201118202901648.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22556%22%2C%22height%22%3A%22197%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**默认文件权限 **umask命令用来设置所创建文件和目录 的默认权限。

<span style="color:#f33b45"><strong> u代表用户  g代表组  o代表其他  a代表上述所有</strong></span>

- **X：如果对象是目录或者它已有执行权限，赋予执行权限。**
- **s：运行时重新设置UID或GID。**
- **t：保留文件或目录。**
- **u：将权限设置为跟属主一样。 **
- **g：将权限设置为跟属组一样。**
- **o：将权限设置为跟其他用户一样**

<!-- -->

### 改变安全性设置

chmod命令用来改变文件和目录的安全性设置

<span style="color:#f33b45"><strong>[ugoa…][[+-=][rwxXstugo…]</strong></span>

**<span style="color:#e579b6"> X：如果对象是目录或者它已有执行权限，赋予执行权限。  s：运行时重新设置UID或GID。  t：保留文件或目录。  u：将权限设置为跟属主一样。  g：将权限设置为跟属组一样。  o：将权限设置为跟其他用户一样。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="174" role="region" aria-label=" 图像 小部件"><img alt="" height="251" data-cke-saved-src="https://img-blog.csdnimg.cn/20201118205628784.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201118205628784.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="584" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201118205628784.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22584%22%2C%22height%22%3A%22251%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>改变所属关系</strong></span>

 chown命令用来改变文件的属主， chgrp命令用来改变文件的默认属组。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="173" role="region" aria-label=" 图像 小部件"><img alt="" height="172" data-cke-saved-src="https://img-blog.csdnimg.cn/2020111821015217.png" src="https://img-blog.csdnimg.cn/2020111821015217.png" width="618" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2020111821015217.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22618%22%2C%22height%22%3A%22172%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> chown命令采用一些不同的选项参数。<span style="color:#f33b45"><strong>-R选项配合通配符可以递归地改变子目录和文件的所 属关系。-h选项可以改变该文件的所有符号链接文件的所属关系。</strong></span>
> 
> 只有root用户能够改变文件的属主。任何属主都可以改变文件的属组，但前提是属主必须 是原属组和目标属组的成员

chgrp命令可以更改文件或目录的默认属组。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="172" role="region" aria-label=" 图像 小部件"><img alt="" height="138" data-cke-saved-src="https://img-blog.csdnimg.cn/20201118210628715.png" src="https://img-blog.csdnimg.cn/20201118210628715.png" width="647" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201118210628715.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22647%22%2C%22height%22%3A%22138%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 共享文件

Linux系统上<span style="color:#f33b45"><strong>共享文件</strong></span>

的方法是创建组

- **设置用户ID(SUID)：当文件被用户使用时，程序会以文件属主的权限运行。 **
- ** 设置组ID(SGID)：对文件来说，<span style="color:#f33b45">程序会以文件属组的权限运行；对目录来说，目录中 创建的新文件会以目录的默认属组作为默认属组。</span>

**
- ** 粘着位：进程结束后文件还驻留(粘着)在内存中。**share

<!-- -->

SGID位对文件共享非常重要。启用SGID位后，你可以强制在一个共享目录下创建的新文件都属于该目录的属组，这个组也就成为了每个用户的属组。

SGID可通过<span style="color:#f33b45"><strong>chmod</strong></span>

命令设置。它会加到标准3位八进制值之前(组成4位八进制值)，或者在符号模式下用符号s。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="171" role="region" aria-label=" 图像 小部件"><img alt="" height="196" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206132927592.png" src="https://img-blog.csdnimg.cn/20201206132927592.png" width="587" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206132927592.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22587%22%2C%22height%22%3A%22196%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

用mkdir命令来创建希望共享的目录。然后通过**chgrp命令将目录的默认属组**改为**包含所有需要共享文件的用户的组**(你必须是该组的成员)。最后，将目录的SGID位置位，以保证 目录中新建文件都用shared作为默认属组。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="170" role="region" aria-label=" 图像 小部件"><img alt="" height="236" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206141014341.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206141014341.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="688" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206141014341.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22688%22%2C%22height%22%3A%22236%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

所有组成员都需把他们的**umask值设置成文件对属组成员可**写。在前面的例子中，umask改成了002，所以文件对属组是可写的。新文件会沿用目录的属组，而不是用户的默认属组 ，即所有的sharedemo组成员都可以访问这个文件了。

### 小结：

<span style="color:#f33b45"><strong>useradd</strong></span>

命令用来创建新的用户账户，**<span style="color:#f33b45">groupadd </span>

**命令用来创建新的组账户。修改已有用户账户，我们用<span style="color:#f33b45"><strong>usermod</strong></span>

命令。类似的<span style="color:#f33b45"><strong>groupmod</strong></span>

命令用 来修改组账户信息。

**umask**命令用来设**置系统中所创建的文件和目录的默认安全设置**。系统管理员通常 会在/etc/profile文件中设置一个默认的umask值，但你可以随时通过umask命令来修改自己的 umask值。

**chmod命令用来修改文件和目录的安全设置。只有文件的属主才能改变文件或目录的权限。 不过root用户可以改变系统上任意文件或目录的安全设置。chown和chgrp命令可用来改变文件 默认的属主和属组。**

**SGID位会强制某个目录下创建的新 文件或目录都沿用该父目录的属组，而不是创建这些文件的用户的属组。这可以为系统的用户之 间共享文件提供一个简便的途径。**

## <span style="color:#3399ea">&gt;第8章管理文件系统</span>



### 探索 Linux 文件系统

基本的 Linux 文件系统：

**1\. ext文件系统**：Linux操作系统中引入的最早的文件系统叫作**扩展文件系统( extended filesystem,简记为ext **),它为Linux提供了一个基本的类Unix文件系统:使用虚拟目录来操作硬件设备,在物理设备上按定长的块来存储数据。

ext文件系统采用名为索引节点的系统来存放虚拟目录中所存储文件的信息。索引节点系统 在每个物理设备中创建一个单独的表(称为索引节点表)来存储这些文件的信息。存储在虚拟目 录中的每一个文件在索引节点表中都有一个条目。ext文件系统名称中的extended部分来自其跟踪 的每个文件的额外数据。

**Linux通过唯一的数值(称作索引节点号)来引用索引节点表中的每个索引节点，这个值是 创建文件时由文件系统分配的。<span style="color:#3399ea">文件系统通过索引节点号而不是文件全名及路径来标识文件。</span>

**

**<span style="color:#86ca5e"> 文件名  文件大小  文件的属主  文件的属组  文件的访问权限  指向存有文件数据的每个硬盘块的指针</span>

**

**2\. ext2文件系统：**

ext2文件系统还改变了文件在数据块中存储的方式。ext文件系统常 见的问题是在文件写入到物理设备时，存储数据用的块很容易分散在整个设备中(称作碎片化， fragmentation)。数据块的碎片化会降低文件系统的性能，因为需要更长的时间在存储设备中查找 特定文件的所有块。 保存文件时，ext2文件系统通过按组分配磁盘块来减轻碎片化。通过将数据块分组，文件系 统在读取文件时不需要为了数据块查找整个物理设备。

如果计算机系统在存储文件和更新索引节点表之间发生了什么，这二者的内容就不同步了，ext2文件系统由于容易在系统崩溃或断电时损坏而臭名昭著。

**3\.日志文件系统**

日志文件系统为Linux系统增加了一层安全性。它不再使用之前先将数据直接写入存储设备 再更新索引节点表的做法，而是先将文件的更改写入到临时文件(称作日志，journal)中。在数 据成功写到存储设备和索引节点表之后，再删除对应的日志条目。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="169" role="region" aria-label=" 图像 小部件"><img alt="" height="157" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206154138716.png" src="https://img-blog.csdnimg.cn/20201206154138716.png" width="724" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206154138716.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22724%22%2C%22height%22%3A%22157%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



**4\.ext3文件系统：**

2001年，ext3文件系统被引入Linux内核中，直到最近都是几乎所有Linux发行版默认的文件 系统。它采用和**ext2文件系统相同的索引节点表结构**，但给每个存储设备增加了一个日志文件， 以将准备写入存储设备的数据先记入日志。 默认情况下，<span style="color:#f33b45"><strong>ext3文件系统用有序模式的日志功能——只将索引节点信息写入日志文件</strong>，<strong>直到数据块都被成功写入存储设备才删除</strong></span>

。你可以在创建文件系统时用简单的一个命令行选项将 ext3文件系统的日志方法改成数据模式或回写模式。 虽然ext3文件系统为Linux文件系统添加了基本的日志功能，但它仍然缺少一些功能。例如 **ext3文件系统无法恢复误删的文件，它没有任何内建的数据压缩功能**(虽然有个需单独安装的补 丁支持这个功能)，**ext3文件系统也不支持加密文件。**鉴于这些原因，Linux项目的开发人员选择 再接再厉，继续改进ext3文件系统。

**5\.ext4文件系统** 扩展ext3文件系统功能的结果是ext4文件系统，ext4文件系统在2008年 受到Linux内核官方支持，现在已是大多数流行的Linux发行版采用的默认文件系统，除了支持数据压缩和加密，**ext4文件系统还支持一个称作区段(extent)的特性**。**区段在存储设备上按块分配空间**，但在**索引节点表中只保存起始块的位置**。由于无需列出所有用来存储文件中数据的数据块，它可以在索引节点表中节省一些空间。

ext4还引入了块预分配技术(block preallocation)。如果你想在存储设备上给一个你知道要变 大的文件预留空间，ext4文件系统可以为文件分配所有需要用到的块，而不仅仅是那些现在已用到的块。ext4文件系统用0填满预留的数据块，不会将它们分配给其他文件。

- **3\. Reiser文件系统**
- **4\. JFS文件系统**
- **5\. XFS文件系统**

<!-- -->

### <span style="color:#f33b45">操作文件系统</span>



fdisk工具用来帮助管理安装在系统上的任何存储设备上的分区。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="168" role="region" aria-label=" 图像 小部件"><img alt="" height="330" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206173416733.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206173416733.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="563" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206173416733.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22563%22%2C%22height%22%3A%22330%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="167" role="region" aria-label=" 图像 小部件"><img alt="" height="396" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206160808316.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206160808316.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="600" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206160808316.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22600%22%2C%22height%22%3A%22396%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

> **<span style="color:#f33b45">分区可以按主分区(primary partition)或扩展分区(extended partition)创建。主分区可以被文件系统直接格式化，而扩展分区则只能容纳其他逻辑分区。扩展分区出现的原因是</span>
> 
> <span style="color:#7c79e5">每个存储设 备上只能有4个分区</span>
> 
> <span style="color:#f33b45">。可以通过创建一个</span>
> 
> 扩展分区，然后在扩展分区内创建多个逻辑分区进行扩展。**

<span style="color:#f33b45"><strong>创建文件系统</strong></span>

在将数据存储到分区之前，<span style="color:#3399ea"><strong>你必须用某种文件系统对其进行格式化，</strong></span>

这样Linux才能使用它。 每种文件系统类型都用自己的命令行程序来格式化分区

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="166" role="region" aria-label=" 图像 小部件"><img alt="" height="262" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206162715996.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206162715996.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="752" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206162715996.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22752%22%2C%22height%22%3A%22262%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#3399ea"><strong>为分区创建了文件系统之后，下一步是将它挂载到虚拟目录下的某个挂载点</strong></span>

，这样就可以将 数据存储在新文件系统中了。你可以将新文件系统挂载到虚拟目录中需要额外空间的任何位置。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="165" role="region" aria-label=" 图像 小部件"><img alt="" height="38" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206174325594.png" src="https://img-blog.csdnimg.cn/20201206174325594.png" width="904" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206174325594.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22904%22%2C%22height%22%3A%2238%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="164" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="18"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

**mount命令将新的硬盘分区添加到挂载点。mount命令的 -t 选项指明了要挂载的文件系统类型(ext4)。现在可以在新分区中保存 新文件和目录了**

> 这种挂载文件系统的方法只能临时挂载文件系统。当重启Linux系统时，文件系统并不会 自动挂载。要强制Linux在启动时自动挂载新的文件系统，可以将其添加到/etc/fstab文件。

**文件系统的检查与修复**

> fsck命令能够检查和修复大部分类型的Linux文件系统，包括本章早些时候讨论过的ext、 ext2、ext3、ext4、ReiserFS、JFS和XFS。该命令的格式是：** fsck options filesystem**

### <span style="color:#f33b45"><strong>逻辑卷管理</strong></span>



> <span style="color:#3399ea"><strong>二、物理卷、逻辑卷、卷组、快照卷之间的联系</strong></span>
> 
> - <span style="color:#f33b45"><strong>物理卷(Physical Volume,PV)</strong></span>
> 
> **：就是指硬盘分区，也可以是整个硬盘或已创建的软RAID，是LVM的基本存储设备。**
> - <span style="color:#f33b45"><strong>卷组(Volume Group,VG)</strong></span>
> 
> **：是由一个或多个物理卷所组成的存储池，在卷组上能创建一个或多个逻辑卷。**
> - <span style="color:#f33b45"><strong>逻辑卷(Logical Volume,LV)</strong></span>
> 
> **：类似于非LVM系统中的硬盘分区，它建立在卷组之上，是一个标准的块设备，在逻辑卷之上可以建立文件系统。**
> 
> <!-- -->
> 
> <span style="color:#f33b45"><strong>可以做这样一个设想来理解以上三者的关系：如果把PV比作地球的一个板块，VG则是一个地球，因为地球是由多个板块组成的，那么在地球上划分一个区域并标记为亚洲，则亚洲就相当于一个LV。</strong></span>
> 
> <span style="color:#3399ea"><strong>相互联系：在创建卷组时一定要为</strong></span>
> 
> <span style="color:#f33b45"><strong>逻辑卷进行快照预留出空间</strong></span>
> 
> <span style="color:#3399ea"><strong>，而后快照访问逻辑卷的另一个入口，只要把物理卷加到卷组之后，这个物理卷所提供的物理空间事先就被划分好一个个块，而这个块在没格式化之前叫做</strong></span>
> 
> <span style="color:#f33b45"><strong>PE(Physical Extend)【物理盘区】</strong></span>
> 
> <span style="color:#3399ea"><strong>，是逻辑存储的一个小匣子，</strong></span>
> 
> <span style="color:#f33b45"><strong>卷组的大小是由多个PE组成</strong></span>
> 
> <span style="color:#3399ea"><strong>，而逻辑卷的大小是把卷组中的PE放到逻辑卷中，此时，PE不再叫做PE，而是叫做</strong></span>
> 
> <span style="color:#f33b45"><strong>LE(Logical Extend)【逻辑盘区】</strong></span>
> 
> <span style="color:#3399ea"><strong>，其实，逻辑卷中的LE也叫做PE，只是站在角度不同而已。</strong></span>
> 
> <span style="color:#f33b45"><strong>如果某个物理卷损坏后，存储在逻辑卷中的LE也就会损坏</strong></span>
> 
> <span style="color:#3399ea"><strong>，想让数据不损坏，</strong></span>
> 
> <span style="color:#f33b45"><strong>可以把物理卷中PE做成镜像</strong></span>

逻辑卷管理布局，**Linux <span style="color:#3399ea">逻辑卷管理器</span>

(logical volume manager，LVM)**可以让你在无需重建整个文件系统的情况下，轻松地管理磁盘空间。用标准分区在硬盘上创建了文件系统，为已有文件系统添加额外的空间多少是一种痛苦的体验。你只能在同一个物理硬盘的可用空间范围内调整分区大小。

<span style="color:#3399ea"><strong>&nbsp;多个物理卷集中在一起可以形成一个卷组(volume group，VG)</strong></span>

。<span style="color:#f33b45"><strong>逻辑卷管理系统将卷组视 为一个物理硬盘</strong></span>

，但事实上<span style="color:#3399ea"><strong>卷组可能是由分布在多个物理硬盘上的多个物理分区组成的</strong></span>

。<span style="color:#7c79e5"><strong>卷组</strong></span>

提供了一个<span style="color:#f33b45"><strong>创建逻辑分区的平台，而这些逻辑分区则包含了文件系统。</strong></span>

整个结构中的最后一层是<span style="color:#f33b45"><strong>逻辑卷(logical volume，LV)</strong></span>

。**逻辑卷为Linux提供了创建文件系统的分区环境**，作用类似于到目前为止我们一直在探讨的**Linux中的物理硬盘分区**。Linux系统将逻 辑卷视为物理分区。<span style="color:#f33b45"><strong> 可以使用任意一种标准Linux文件系统来格式化逻辑卷，然后再将它加入Linux虚拟目录中的某个挂载点。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="163" role="region" aria-label=" 图像 小部件"><img alt="" height="345" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206182736485.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206182736485.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="811" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206182736485.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22811%22%2C%22height%22%3A%22345%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#f33b45"><strong>Linux 中的 LVM</strong></span>



**<span style="color:#f33b45">1. 快照</span>

 最初的Linux LVM 允许你在逻辑卷在线的状态下将其复制到另一个设备。**

- **LVM1只允许你创建只读快照。一旦创建了快照，就不能再写入东西了。**
- **LVM2允许你创建在线逻辑卷的可读写快照。有了可读写的快照，就可以删除原先的逻辑卷， 然后将快照作为替代挂载上。这个功能对快速故障转移或涉及修改数据的程序试验(如果失败， 需要恢复修改过的数据)非常有用。**

<!-- -->

这个功能叫作快照。**在备份由于高可靠性需求而无法锁定的重要数据时，快照功能非常给力。传统的备份方法在将文件复制到备份媒体上时通常要将文件锁定。<span style="color:#f33b45">快照允许你在复制的同时，保证运行关键任务的</span>

。**

**<span style="color:#f33b45">2. 条带化</span>

，LVM2提供的另一个引人注目的功能是条带化(striping)。**马毓姝

- 条带化可跨多个物理硬盘创建逻辑卷。当Linux LVM将文件写入逻辑卷时，文件中的数据块会被分散到多个硬盘上。每个后继数据块会被写到下一个硬盘上。 
- 条带化有助于提高硬盘的性能，因为Linux可以将一个文件的多个数据块同时写入多个硬盘， 而无需等待单个硬盘移动读写磁头到多个不同位置。这个改进同样适用于读取顺序访问的文件， 因为LVM可同时从多个硬盘读取数据。(多路复用)。

<!-- -->

> **<span style="color:#f33b45">LVM条带化不同于RAID条带化。LVM条带化不提供用来创建容错环境的校验信息。事实 上，LVM条带化会增加文件因硬盘故障而丢失的概率。单个硬盘故障可能会造成多个逻 辑卷无法访问。</span>
> 
> **

**<span style="color:#f33b45">3. 镜像</span>

 LVM镜像。镜像是一个实时更新的逻辑卷的完整副本。当你创 建镜像逻辑卷时，LVM会将原始逻辑卷同步到镜像副本中。**

- LVM逻辑卷也容易受到断电和磁盘故障的影响。一旦文件系统损坏，就有可能再也无法恢复。 LVM快照功能提供了一些安慰，你可以随时创建逻辑卷的备份副本，但对有些环境来说可能 还不够。对于涉及大量数据变动的系统

<!-- -->

### **使用 Linux LVM**

定义物理卷： 创建过程的第一步就是将硬盘上的<span style="color:#3399ea"><strong>物理分区转换成Linux LVM使用的物理卷区段，&nbsp;fdisk命令</strong></span>

。

- **创建了基本的Linux分区之后**，你需要通过**t命令改变分区类型**。**<span style="color:#f33b45">分区类型8e表示这个分区将会被用作Linux LVM系统的一部分，而不是一个直接的文件系统 (就像你在前面看到的83类型的分区)。</span>

**

<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="162" role="region" aria-label=" 图像 小部件"><img alt="" height="444" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206192939742.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206192939742.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="677" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206192939742.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22677%22%2C%22height%22%3A%22444%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- <span style="color:#f33b45"><strong>1. 用分区来</strong></span>

    **创建实际的物理卷**<span style="color:#f33b45">。这可以通过<strong>pvcreate命令来完成。pvcreate定义了 用于物理卷的物理分区</strong>。它只是简单地将分区标记成Linux LVM系统中的分区而已。</span>

    想查看创建进度的话，可以使用pvdisplay命令来显示已创建的物理卷列表<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="161" role="region" aria-label=" 图像 小部件"><img alt="" height="93" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206193350817.png" src="https://img-blog.csdnimg.cn/20201206193350817.png" width="506" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206193350817.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22506%22%2C%22height%22%3A%2293%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- **2**. **创建卷组** ，物理卷中创建一个或多个卷组 <span style="color:#f33b45"><strong>命令行创建卷组，需要使用vgcreate命令。vgcreate命令需要一些命令行参数来定义 卷组名以及你用来创建卷组的物理卷名</strong></span>

    。 看新创建的卷组的细节，可用vgdisplay命令。<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="160" role="region" aria-label=" 图像 小部件"><img alt="" height="69" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206193736360.png" src="https://img-blog.csdnimg.cn/20201206193736360.png" width="545" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206193736360.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22545%22%2C%22height%22%3A%2269%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- **创建逻辑卷 ，Linux系统使用逻辑卷来模拟物理分区**<span style="color:#3399ea"><strong>，并在其中保存文件系统。Linux系统会像处理物理分 区一样处理逻辑卷，允许你定义逻辑卷中的文件系统，然后将文件系统挂载到虚拟目录上&nbsp;</strong></span>

    创建逻辑卷，使用lvcreate命令。<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="159" role="region" aria-label=" 图像 小部件"><img alt="" height="65" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206193952835.png" src="https://img-blog.csdnimg.cn/20201206193952835.png" width="577" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206193952835.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22577%22%2C%22height%22%3A%2265%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

    <br>

    卷组名(Vol1)用来标识创建新逻辑卷时要使 用的卷组。 -l选项定义了要为逻辑卷指定多少可用的卷组空间。注意，你可以按照卷组空闲空间的百分 比来指定这个值。本例中为新逻辑卷使用了所有的空闲空间。<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="158" role="region" aria-label=" 图像 小部件"><img alt="" height="431" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206194003958.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206194003958.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="762" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206194003958.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22762%22%2C%22height%22%3A%22431%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- **4\. 创建文件系统**<span style="color:#f33b45"><strong>运行完 lvcreate命令之后，逻辑卷就已经产生了</strong></span>

    ，但它还没有文件系统。<br>

    <span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="157" role="region" aria-label=" 图像 小部件"><img alt="" height="85" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206200619283.png" src="https://img-blog.csdnimg.cn/20201206200619283.png" width="646" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206200619283.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22646%22%2C%22height%22%3A%2285%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- 5\.<span style="color:#f33b45"><strong>&nbsp;挂载逻辑分区</strong></span>

    ， 在创建了新的文件系统之后，<span style="color:#f33b45"><strong>可以用标准Linux mount命令将这个卷挂载到虚拟目录中</strong></span>

    ，就跟它是物理分区一样。**唯一的不同是你需要用特殊的路径来标识逻辑卷**<span style="color:#f33b45">。</span>

- 6\. 修改LVM Linux LVM的好处在于能够动态修改文件系统，因此最好有工具能够让你实现这些操作。在 Linux有一些工具允许你修改现有的逻辑卷管理配置。<br>

    <span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="156" role="region" aria-label=" 图像 小部件"><img alt="" height="208" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206203709651.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206203709651.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="777" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206203709651.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22777%22%2C%22height%22%3A%22208%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>


<!-- -->

fdisk命令用来对存储设备进行分区， 以便安装文件系统。<span style="color:#3399ea"><strong>在分区存储设备时，必须定义在上面使用什么类型的文件系统。</strong></span>

<span style="color:#f33b45"><strong>在存储设备分区上直接创建文件系统的一个限制因素是</strong></span>

，如果硬盘空间用完了，你无法轻易 地改变文件系统的大小。但<span style="color:#f33b45"><strong>Linux支持逻辑卷管理，这是一种跨多个存储设备创建虚拟分区的方 法。这种方法允许你轻松地扩展一个已有文件系统</strong></span>

，而不用完全重建。Linux LVM包提供了跨多 个存储设备创建逻辑卷的命令行命令。

## <span style="color:#3399ea">&gt;第9童安装软件程序</span>



<span style="color:#f33b45"><strong>Linux上能见到的各种包管理系统(package management system，PMS)</strong></span>

，以及用来进行软件安装、管理和删除的命令行工具。

PMS工具及相关命令在不同的Linux发行版上有很大的不同。Linux中广泛使用的两种主要的 PMS基础工具是dpkg和rpm。

- **基于Debian的发行版(如Ubuntu和Linux Mint)使用的是dpkg命令，这些发行版的PMS工具 也是以该命令为基础的.**- 用 aptitude 管理软件包

    <!-- -->

- **基于Red Hat的发行版(如Fedora、openSUSE及Mandriva)使用的是rpm命令，该命令是其PMS 的底层基础。类似于dpkg命令，rmp命令能够列出已安装包、安装新包和删除已有软件。**-  yum：在Red Hat和Fedora中使用。

    <!-- -->


<!-- -->

**列出已安装包**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="155" role="region" aria-label=" 图像 小部件"><img alt="" height="361" data-cke-saved-src="https://img-blog.csdnimg.cn/20201206210151304.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201206210151304.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="841" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201206210151304.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22841%22%2C%22height%22%3A%22361%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

如果需要找出系统上的某个特定文件属于哪个软件包，万能的yum可以做到！只要输 入命令： <span style="color:#f33b45"><strong>yum provides file_name</strong></span>

**用 yum 安装软件 **yum install package\_name

- 可以手动下载rpm安装文件并用yum安装，这叫作本地安装。基本的命令是： **<span style="color:#f33b45">yum localinstall package_name.rpm</span>

**
- 对更新列表中的所有包进行更新，只要输入如下命令：**<span style="color:#f33b45"> yum update</span>

**
- 只删除软件包而保留配置文件和数据文件，就用如下命令：**<span style="color:#f33b45"> yum remove package_name</span>

**
- 要删除软件和它所有的文件，就用erase选项： <span style="color:#f33b45"><strong>yum erase package_name</strong></span>


<!-- -->

**处理损坏的包依赖关系 **

- 如果系统出现了这个问题，先试试下面的命令：**<span style="color:#f33b45"> yum clean all</span>

**
- **<span style="color:#f33b45">yum update --skip-broken --skip-broken&nbsp;</span>

** 选项允许你忽略依赖关系损坏的那个包，继续去更新其他软件包。这可能 救不了损坏的包，但至少可以更新系统上的其他包。
- <span style="color:#f33b45"><strong>yum deplist package_name </strong></span>

 这个命令显示了所有包的库依赖关系以及什么软件可以提供这些库依赖关系。一旦知道某个 包需要的库，你就能安装它们了

<!-- -->

**yum 软件仓库**

- 要想知道你现在正从哪些仓库中获取软件，输入如下命令： yum repolist
- yum的仓库定义文件位于 /etc/yum.repos.d。你需要添加正确的URL，并获得必要的加密密钥。

<!-- -->

## <span style="color:#3399ea">&gt;第10章使用编辑器</span>



vi编辑器是Unix系统最初的编辑器。它使用控制台图形模式来模拟文本编辑窗口，允许查看 文件中的行、在文件中移动、插入、编辑和替换文本.

### vim 编辑器

> Linux which命令用于查找文件。which指令会在环境变量$PATH设置的目录里查找符合条件的文件。
> 
> <span style="color:#f33b45"><strong>readlink –f</strong>命令就可以了。它能够立刻找出链接文件的最后一环。</span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="154" role="region" aria-label=" 图像 小部件"><img alt="" height="307" data-cke-saved-src="https://img-blog.csdnimg.cn/20201214202235765.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201214202235765.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="841" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201214202235765.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22841%22%2C%22height%22%3A%22307%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

1. **如在启动vim时未指定文件名，或者这个文件不存在，vim会开辟一段新的缓冲区域来编辑。**
2. **如果你在命令行下指定了一个已有文件的名字，vim会将文件的整个内容都读到一块缓冲区域来准备编辑**

<!-- -->

vim编辑器有两种操作模式：  普通模式  插入模式

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="153" role="region" aria-label=" 图像 小部件"><img alt="" height="280" data-cke-saved-src="https://img-blog.csdnimg.cn/20201214202514364.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201214202514364.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="601" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201214202514364.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22601%22%2C%22height%22%3A%22280%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

vim中有用来移动 光标的命令。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="152" role="region" aria-label=" 图像 小部件"><img alt="" height="348" data-cke-saved-src="https://img-blog.csdnimg.cn/20201214203218542.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20201214203218542.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="655" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20201214203218542.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22655%22%2C%22height%22%3A%22348%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- **<span style="color:#f33b45"> h：左移一个字符。 </span>

**
- **<span style="color:#f33b45"> j：下移一行(文本中的下一行)。 </span>

**
- **<span style="color:#f33b45"> k：上移一行(文本中的上一行)。</span>

**
- **<span style="color:#f33b45"> l：右移一个字符。 </span>

**

<!-- -->

在大的文本文件中一行一行地来回移动会特别麻烦，幸而vim提供了一些能够提高移动速度 的命令。

- <span style="color:#f33b45"><strong> PageDown(或Ctrl+F)：下翻一屏。</strong></span>

- <span style="color:#f33b45"><strong> PageUp(或Ctrl+B)：上翻一屏。 </strong></span>

- <span style="color:#f33b45"><strong> G：移到缓冲区的最后一行。</strong></span>

- <span style="color:#f33b45"><strong> num G：移动到缓冲区中的第num行。</strong></span>

- <span style="color:#f33b45"><strong> gg：移到缓冲区的第一行。</strong></span>


<!-- -->

vim编辑器在普通模式下有个特别的功能叫命令行模式。命令行模式提供了一个交互式命令 行，可以输入额外的命令来控制vim的行为。要进入命令行模式，在普通模式下按下冒号键。光标会移动到消息行，然后出现冒号，等待输入命令。

- <span style="color:#f33b45"><strong> q：如果未修改缓冲区数据，退出。</strong></span>

- <span style="color:#f33b45"><strong> q!：取消所有对缓冲区数据的修改并退出。 </strong></span>

- <span style="color:#f33b45"><strong> w filename：将文件保存到另一个文件中。</strong></span>

- <span style="color:#f33b45"><strong> wq：将缓冲区数据保存到文件中并退出。</strong></span>


<!-- -->

### <span style="color:#3399ea">编辑数据</span>



<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="151" role="region" aria-label=" 图像 小部件"><img alt="" height="348" data-cke-saved-src="https://img-blog.csdnimg.cn/2020121420353956.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2020121420353956.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="868" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2020121420353956.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22868%22%2C%22height%22%3A%22348%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 复制和粘贴

vim在 删除数据时，实际上会将数据保存在单独的一个寄存器中。可以用p命令取回数据。

vim中复制命令是y(代表yank)。可以在y后面使用和d命令相同 的第二字符(yw表示复制一个单词，y$表示复制到行尾)。输入p命令粘贴

### 查找和替换

- 用vim查找命令来轻松查找缓冲区中的数据。**要输入一个查找字符串，就按下斜线(/)**
-  如果要查找的文本出现在光标当前位置之后，则光标会跳到该文本出现的第一个位置。
-  如果要查找的文本未在光标当前位置之后出现，则光标会绕过文件末尾，出现在该文本 所在的第一个位置(并用一条消息指明)。
-  输出一条错误消息，说明在文件中没有找到要查找的文本。
- 继续查找同一个单词，按下斜线键，然后按回车键。或者使用n键，表示下一个(next)
- 替换命令允许你快速用另一个单词来替换文本中的某个单词。必须进入命令行模式才能使用 替换命令。替换命令的格式是： <span style="color:#7c79e5"><strong>:s/old/new/</strong></span>

    - <span style="color:#3399ea"><strong> :s/old/new/g：一行命令替换所有old。 </strong></span>

    - <span style="color:#3399ea"><strong> :n,ms/old/new/g：替换行号n和m之间所有old。</strong></span>

    - <span style="color:#3399ea"><strong> :%s/old/new/g：替换整个文件中的所有old。</strong></span>

    - <span style="color:#3399ea"><strong> :%s/old/new/gc：替换整个文件中的所有old，但在每次出现时提示。</strong></span>

    <!-- -->


<!-- -->

### 其他编译器：

- **nano 编辑器：nano文本编辑器是Unix系统的Pico编辑器的克隆版。尽管Pico也是一款简单轻便的文本编辑 器，但是它并没有采用GPL许可协议。nano文本编辑器不仅采用了GPL许可协议，而且还加入了 GNU项目。**
- **emacs 编辑器**
- **KDE 系编辑器**
- **GNOME 编辑器**

<!-- -->

# <span style="color:#f33b45">第二部分 shell脚本编程基础</span>



### <span style="color:#3399ea">shell 的执行方式</span>



- **<span style="color:#f33b45">如果.不在PATH里面，要执行当前目录下的可执行文件，使用全路径：./executable-file</span>

    **- PATH是环境变量，如果将当前目录“./”添加到环境变量中，那么也可以不用“./”，直接输入当前目录下有可执行权限的可执行文件就可以运行了

    <!-- -->

- <span style="color:#f33b45"><strong>如果要执行一个sh脚本，不管那个脚本有没有可执行权限，都可以使用：sh [file]</strong></span>

    - 这时file是作为参数传给sh的，如果file不在当前目录下，也需要使用全路径。

    <!-- -->

- <span style="color:#f33b45"><strong>1、source命令用法：</strong></span>

    **source FileName**

    <span style="color:#3399ea">　作用:<strong>在当前bash环境下读取并执行FileName中的命令</strong></span>

    **。该filename文件可以无"执行权限"**<span style="color:#3399ea"><strong>，注：该命令通常用命令“.”来替代。source(或点)命令通常用于重新执行刚修改的初始化文档</strong>。 source命令(从 C Shell 而来)是bash shell的内置命令。点命令，就是个点符号，(从Bourne Shell而来)。</span>

    <span style="color:#f33b45"><strong>2、sh和bash命令用法：</strong></span>

    **sh FileName、 bash FileName**

    作用:在当前bash环境下读取并执行FileName中的命令。**该filename文件可以无"执行权限"**，** 注：两者在执行文件时的不同，是分别用自己的shell来跑文件**。<span style="color:#3399ea"><strong> sh使用“-n”选项进行shell脚本的语法检查，使用“-x”选项实现shell脚本逐条语句的跟踪，</strong></span>

    可以巧妙地利用shell的内置变量增强“-x”选项的输出信息等。

    <span style="color:#f33b45"><strong>3、./的命令用法：</strong></span>

    **./FileName**

    作用:打开一个子shell来读取并执行FileName中命令。** 注：运行一个shell脚本时会启动另一个命令解释器.每个shell脚本有效地运行在父shell(parent shell)的一个子进程里**.这个父shell是指在一个控制终端或在一个xterm窗口中给你命令指示符的进程. shell脚本也可以启动他自已的子进程. 这些子shell(即子进程)使脚本并行地，有效率地地同时运行脚本内的多个子任务.

- <span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="150" role="region" aria-label=" 图像 小部件"><img alt="" height="262" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112204515768.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112204515768.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="773" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112204515768.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22773%22%2C%22height%22%3A%22262%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>


<!-- -->

## <span style="color:#3399ea">&gt;第11章构建基本脚本</span>



<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="149" role="region" aria-label=" 图像 小部件"><img alt="" height="98" data-cke-saved-src="https://img-blog.csdnimg.cn/20210108191947670.png" src="https://img-blog.csdnimg.cn/20210108191947670.png" width="695" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210108191947670.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22695%22%2C%22height%22%3A%2298%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 创建 shell 脚本文件

必须在文件的第一行指定要使用的shell，其格式为：<span style="color:#f33b45"><strong> #!/bin/bash</strong></span>

PATH环境变量被设置成只在一组目录中查找命令。要让shell找到test1脚本，只需采取以下两种作法之一：

- 将shell脚本文件所处的目录添加到PATH环境变量中；
- 在提示符中用绝对或相对文件路径来引用shell脚本文件。

<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="148" role="region" aria-label=" 图像 小部件"><img alt="" height="203" data-cke-saved-src="https://img-blog.csdnimg.cn/20210108192750488.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210108192750488.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="710" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210108192750488.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22710%22%2C%22height%22%3A%22203%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**echo命令可用单引号或双引号来划定文本字符串。如果在字符串中用到了它们，你需要在 文本中使用其中一种引号，而用另外一种来将字符串划定起来。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="147" role="region" aria-label=" 图像 小部件"><img alt="" height="357" data-cke-saved-src="https://img-blog.csdnimg.cn/2021010819312133.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2021010819312133.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="810" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021010819312133.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22810%22%2C%22height%22%3A%22357%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>echo 取消换行；</strong>可以用echo语句 的-n参数。只要将第一个echo语句改成这样就行：</span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="146" role="region" aria-label=" 图像 小部件"><img alt="" height="245" data-cke-saved-src="https://img-blog.csdnimg.cn/20210108193539113.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210108193539113.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="626" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210108193539113.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22626%22%2C%22height%22%3A%22245%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 使用变量：

<span style="color:#3399ea"><strong>在shell命令使用 其他数据来处理信息。这可以通过变量来实现，常用环境变量，用户变量</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="145" role="region" aria-label=" 图像 小部件"><img alt="" height="103" data-cke-saved-src="https://img-blog.csdnimg.cn/20210109181631533.png" src="https://img-blog.csdnimg.cn/20210109181631533.png" width="478" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210109181631533.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22478%22%2C%22height%22%3A%22103%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="144" role="region" aria-label=" 图像 小部件"><img alt="" height="465" data-cke-saved-src="https://img-blog.csdnimg.cn/20210109182530894.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210109182530894.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="903" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210109182530894.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22903%22%2C%22height%22%3A%22465%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**命令替换**，有两种方法可以将命令输出赋给变量： <span style="color:#f33b45"><strong> 反引号字符(`) $()格式</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="143" role="region" aria-label=" 图像 小部件"><img alt="" height="84" data-cke-saved-src="https://img-blog.csdnimg.cn/20210109190118753.png" src="https://img-blog.csdnimg.cn/20210109190118753.png" width="471" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210109190118753.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22471%22%2C%22height%22%3A%2284%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="142" role="region" aria-label=" 图像 小部件"><img alt="" height="132" data-cke-saved-src="https://img-blog.csdnimg.cn/20210109190140410.png" src="https://img-blog.csdnimg.cn/20210109190140410.png" width="735" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210109190140410.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22735%22%2C%22height%22%3A%22132%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>命令替换会创建一个子shell来运行对应的命令。</strong></span>

子shell(subshell)是由运行该脚本的shell 所创建出来的一个独立的子shell(child shell)。<span style="color:#3399ea"><strong>正因如此，由该子shell所执行命令是无法 使用脚本中所创建的变量的</strong></span>

。

**在命令行提示符下使用路径./运行命令的话，也会创建出子shell；<span style="color:#3399ea">要是运行命令的时候 不加入路径，就不会创建子shell。如果你使用的是内建的shell命令，并不会涉及子shell</span>

**。 在命令行提示符下运行脚本时一定要留心！

### 重定向输入和输出

如果输出文件已经存在了，**重定向操作(>)符会用新的文件数据覆盖已有文件。用双大于号(>>)来追加数据。**

输入重定向和输出重定向正好相反。<span style="color:#f33b45"><strong>输入重定向将文件的内容重定向到命令，而非将命令的 输出重定向到文件。</strong></span>

**一个简单的记忆方法就是：在命令行上，命令总是在左侧，而重定向符号“指向”数据流动 的方向。小于号说明数据正在从输入文件流向命令。**

内联输入重定向(inline input redirection)：须指定一个文本标记来划分输 入数据的开始和结尾。任何字符串都可作为文本标记，但在数据的开始和结尾文本标记必须一致。

wc命令可以对对数据中的文本进行计数。默认情况下，**它会输出3个值：  文本的行数  文本的词数  文本的字节数。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="141" role="region" aria-label=" 图像 小部件"><img alt="" height="185" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112201508892.png" src="https://img-blog.csdnimg.cn/20210112201508892.png" width="711" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112201508892.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22711%22%2C%22height%22%3A%22185%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 管道

管道符号在shell编程之外也很少用到。<span style="color:#f33b45"><strong>该符号由两个 竖线构成，一个在另一个上面。然而管道符号的印刷体通常看起来更像是单个竖线(|)</strong></span>

。在美式 键盘上，它通常和反斜线(\)位于同一个键。管道被放在命令之间，将一个命令的输出重定向 到另一个命令中：

**<span style="color:#3399ea">不要以为由管道串起的两个命令会依次执行。Linux系统实际上会同时运行这两个命令，在 系统内部将它们连接起来。在第一个命令产生输出的同时，输出会被立即送给第二个命令。数据 传输不会用到任何中间文件或缓冲区。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="140" role="region" aria-label=" 图像 小部件"><img alt="" height="259" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112201955142.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112201955142.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="750" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112201955142.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22750%22%2C%22height%22%3A%22259%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="139" role="region" aria-label=" 图像 小部件"><img alt="" height="248" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112202201569.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112202201569.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="736" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112202201569.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22736%22%2C%22height%22%3A%22248%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**执行数学运算：expr 命令**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="138" role="region" aria-label=" 图像 小部件"><img alt="" height="631" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112202935609.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112202935609.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="895" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112202935609.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22895%22%2C%22height%22%3A%22631%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="137" role="region" aria-label=" 图像 小部件"><img alt="" height="219" data-cke-saved-src="https://img-blog.csdnimg.cn/2021011220312059.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2021011220312059.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="687" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021011220312059.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22687%22%2C%22height%22%3A%22219%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#3399ea"><strong>使用方括号：在bash中，在将一个数学运算结果赋给某个变量时，可以用美元符和 方括号($[ operation ])将数学表达式围起来。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="136" role="region" aria-label=" 图像 小部件"><img alt="" height="115" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112205413793.png" src="https://img-blog.csdnimg.cn/20210112205413793.png" width="440" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112205413793.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22440%22%2C%22height%22%3A%22115%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#3399ea"><strong>浮点解决方案；</strong></span>

**用内建的bash计算器， 叫作bc，能够识别： 数字(整数和浮点数)  变量(简单变量和数组)  注释(以#或C语言中的/\* \*/开始的行)  表达式  编程语句(例如if-then语句)  函数**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="135" role="region" aria-label=" 图像 小部件"><img alt="" height="249" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112205744219.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112205744219.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="722" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112205744219.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22722%22%2C%22height%22%3A%22249%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

在脚本中使用bc，<span style="color:#f33b45"><strong>variable=$(echo "options; expression" | bc) </strong></span>

第一部分options允许你设置变量。如果你需要不止一个变量，可以用分号将其分开。 expression参数定义了通过bc执行的数学表达式。这里有个在脚本中这么做的例子。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="134" role="region" aria-label=" 图像 小部件"><img alt="" height="74" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112210712968.png" src="https://img-blog.csdnimg.cn/20210112210712968.png" width="781" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112210712968.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22781%22%2C%22height%22%3A%2274%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="133" role="region" aria-label=" 图像 小部件"><img alt="" height="115" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112211009694.png" src="https://img-blog.csdnimg.cn/20210112211009694.png" width="707" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112211009694.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22707%22%2C%22height%22%3A%22115%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#3399ea"><strong>如果需要进行大量运算，最好的办法是使用内联输入重定向</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="132" role="region" aria-label=" 图像 小部件"><img alt="" height="292" data-cke-saved-src="https://img-blog.csdnimg.cn/2021011221132560.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2021011221132560.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="710" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021011221132560.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22710%22%2C%22height%22%3A%22292%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

在bash 计算器中创建的变量只在bash计算器中有效，不能在shell脚本中使用。

### **退出脚本**

<span style="color:#f33b45"><strong>查看退出状态码：</strong></span>

shell中运行的每个命令都使用退出状态码(exit status)告诉shell它已经运行完毕。

退出状态码被缩减到了0～255的区间。shell通过模运算得到这个结果。一个值的模就是被除 后的余数。最终的结果是指定的数值除以256后得到的余数。在这个例子中，指定的值是300(返 回值)，余数是44，因此这个余数就成了最后的状态退出码。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="131" role="region" aria-label=" 图像 小部件"><img alt="" height="127" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112211631934.png" src="https://img-blog.csdnimg.cn/20210112211631934.png" width="701" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112211631934.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22701%22%2C%22height%22%3A%22127%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="130" role="region" aria-label=" 图像 小部件"><img alt="" height="312" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112211650935.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112211650935.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="890" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112211650935.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22890%22%2C%22height%22%3A%22312%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>exit 命令</strong></span>

 ：exit命令允许你在脚本结束时指定一 个退出状态码。也可以在exit命令的参数中使用变量。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="129" role="region" aria-label=" 图像 小部件"><img alt="" height="86" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112211908795.png" src="https://img-blog.csdnimg.cn/20210112211908795.png" width="548" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112211908795.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22548%22%2C%22height%22%3A%2286%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

## <span style="color:#3399ea"><strong>&gt;第12章使用结构化命令</strong></span>



许多程序要求对shell脚本中的命令施加一些逻辑流程控制。有一类命令会根据条件使脚本跳 过某些命令。这样的命令通常称为结构化命令(structured command)

### <span style="color:#3399ea">使用 if-then 语句</span>



<span style="color:#f33b45"><strong>bash shell的if语句会运行if后面的那个命令。如果该命令的退出状态码(参见第11章)是0 (该命令成功运行)，位于then部分的命令就会被执行(</strong></span>

**支持缩进，类似python**<span style="color:#f33b45"><strong>)。如果该命令的退出状态码是其他值，then部分的命令就不会被执行，bash shell会继续执行脚本中的下一个命令。fi语句用来表示if-then 语句到此结束。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="128" role="region" aria-label=" 图像 小部件"><img alt="" height="201" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112213027606.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112213027606.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="530" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112213027606.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22530%22%2C%22height%22%3A%22201%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="127" role="region" aria-label=" 图像 小部件"><img alt="" height="115" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112213309176.png" src="https://img-blog.csdnimg.cn/20210112213309176.png" width="548" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112213309176.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22548%22%2C%22height%22%3A%22115%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

if语句行使用grep命令在/etc/passwd文件中查找某个用户名当前是否在系统上使用。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="126" role="region" aria-label=" 图像 小部件"><img alt="" height="151" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112213826320.png" src="https://img-blog.csdnimg.cn/20210112213826320.png" width="634" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112213826320.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22634%22%2C%22height%22%3A%22151%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#3399ea"><strong>if-then-else 语句</strong></span>



**<span style="color:#f33b45">当if语句中的命令返回退出状态码0时，then部分中的命令会被执行，这跟普通的if-then 语句一样。当if语句中的命令返回非零退出状态码时，bash shell会执行else部分中的命令。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="125" role="region" aria-label=" 图像 小部件"><img alt="" height="173" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112213951878.png" src="https://img-blog.csdnimg.cn/20210112213951878.png" width="761" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112213951878.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22761%22%2C%22height%22%3A%22173%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**嵌套 if：要检查/etc/passwd文件中是否存在某个用户名以及该用户的目录是否尚在，可以使用嵌套的 if-then语句。嵌套的if-then语句位于主if-then-else语句的else代码块中。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="124" role="region" aria-label=" 图像 小部件"><img alt="" height="284" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116175323376.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116175323376.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="609" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116175323376.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22609%22%2C%22height%22%3A%22284%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

嵌套 if：要检查/etc/passwd文件中是否存在某个用户名以及该用户的目录是否尚在，可以使用嵌套的 if-then语句。嵌套的if-then语句位于主if-then-else语句的else代码块中。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="123" role="region" aria-label=" 图像 小部件"><img alt="" height="296" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116181538938.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116181538938.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="750" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116181538938.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22750%22%2C%22height%22%3A%22296%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="122" role="region" aria-label=" 图像 小部件"><img alt="" height="307" data-cke-saved-src="https://img-blog.csdnimg.cn/2021011618155720.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2021011618155720.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="713" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021011618155720.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22713%22%2C%22height%22%3A%22307%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

可以使用else部分的另一种形式：**elif。这样就不用再书写多个if-then语句了。elif使 用另一个if-then语句延续else部分。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="121" role="region" aria-label=" 图像 小部件"><img alt="" height="157" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116181633696.png" src="https://img-blog.csdnimg.cn/20210116181633696.png" width="419" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116181633696.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22419%22%2C%22height%22%3A%22157%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**<span style="color:#f33b45">elif语句行提供了另一个要测试的命令，这类似于原始的if语句行。如果elif后命令的退 出状态码是0，则bash会执行第二个then语句部分的命令。使用这种嵌套方法，代码更清晰，逻 辑更易懂。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="120" role="region" aria-label=" 图像 小部件"><img alt="" height="175" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116182323316.png" src="https://img-blog.csdnimg.cn/20210116182323316.png" width="780" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116182323316.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22780%22%2C%22height%22%3A%22175%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="119" role="region" aria-label=" 图像 小部件"><img alt="" height="285" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116182338275.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116182338275.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="697" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116182338275.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22697%22%2C%22height%22%3A%22285%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#f33b45">test 命令</span>



**test命令提供了在if-then语句中测试不同条件的途径，如果test命令中列出的条件成立， test命令就会退出并返回退出状态码0。这样if-then语句就与其他编程语言中的if-then语句 以类似的方式工作了。如果条件不成立，test命令就会退出并返回非零的退出状态码，这使得 if-then语句不会再被执行。**

> test condition

<span style="color:#f33b45"><strong></strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="118" role="region" aria-label=" 图像 小部件"><img alt="" height="262" data-cke-saved-src="https://img-blog.csdnimg.cn/2021011618265567.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2021011618265567.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="622" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021011618265567.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22622%22%2C%22height%22%3A%22262%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>如果不写test命令的condition部分，它会以非零的退出状态码退出，并执行else语句块。</strong></span>

当你加入一个条件时，test命令会测试该条件。例如，可以使用test命令确定变量中是否 有内容。这只需要一个简单的条件表达式。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="117" role="region" aria-label=" 图像 小部件"><img alt="" height="312" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116182819774.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116182819774.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="601" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116182819774.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22601%22%2C%22height%22%3A%22312%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="116" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="19"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

**<span style="color:#f33b45">&nbsp;方括号定义了测试条件。注意，第一个方括号之后和第二个方括号之前必须加上一个空格， 否则就会报错</span>

**。<span style="color:#3399ea"><strong>test命令可以判断三类条件： </strong></span>

<br>

- <span style="color:#3399ea"><strong> 数值比较 </strong></span>

<br>

bash shell只能处理整数。如果你只是要通过echo语句来显示这个结果，那没问题
- <span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="115" role="region" aria-label=" 图像 小部件"><img alt="" height="215" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116183045244.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116183045244.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="817" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116183045244.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22817%22%2C%22height%22%3A%22215%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>


<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="114" role="region" aria-label=" 图像 小部件"><img alt="" height="408" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116200141743.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116200141743.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="669" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116200141743.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22669%22%2C%22height%22%3A%22408%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- <span style="color:#3399ea"><strong> 字符串比较 </strong></span>

<br>

大于号和小于号必须转义，否则shell会把它们当作重定向符号，把字符串值当作文件 名；

<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="113" role="region" aria-label=" 图像 小部件"><img alt="" height="228" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116200259427.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116200259427.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="760" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116200259427.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22760%22%2C%22height%22%3A%22228%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="112" role="region" aria-label=" 图像 小部件"><img alt="" height="190" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116200424527.png" src="https://img-blog.csdnimg.cn/20210116200424527.png" width="633" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116200424527.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22633%22%2C%22height%22%3A%22190%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- <span style="color:#3399ea"><strong> 文件比较</strong></span>


<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="111" role="region" aria-label=" 图像 小部件"><img alt="" height="353" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116200505726.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116200505726.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="718" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116200505726.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22718%22%2C%22height%22%3A%22353%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- **<span style="color:#f33b45">检查目录 -d测试会检查指定的目录是否存在于系统中。如果你打算将文件写入目录或是准备切换到某 个目录中，先进行测试总是件好事情。</span>

**
- **<span style="color:#f33b45">检查对象是否存在 -e比较允许你的脚本代码在使用文件或目录前先检查它们是否存在</span>

**
- **<span style="color:#f33b45">检查文件 -e比较可用于文件和目录。要确定指定对象为文件，必须用-f比较。</span>

**
- **<span style="color:#f33b45">检查是否可读 在尝试从文件中读取数据之前，最好先测试一下文件是否可读。可以使用-r比较测试。</span>

**
- **<span style="color:#f33b45">检查空文件 应该用-s比较来检查文件是否为空，尤其是在不想删除非空文件的时候。要留心的是，当 -s比较成功时，说明文件中有数据。</span>

**
- **<span style="color:#f33b45">检查是否可写 -w 比较会判断你对文件是否有可写权限。脚本test16.sh只是脚本test13.sh的修改版。现在不单 检查item_name是否存在、是否为文件，还会检查该文件是否有写入权限。</span>

**
- **<span style="color:#f33b45">检查文件是否可以执行 -x比较是判断特定文件是否有执行权限的一个简单方法。虽然可能大多数命令用不到它，但 如果你要在shell脚本中运行大量脚本，它就能发挥作用。</span>

**
- **<span style="color:#f33b45">检查所属关系 -O 比较可以测试出你是否是文件的属主。</span>

**
- **<span style="color:#f33b45">检查默认属组关系 -G比较会检查文件的默认组，如果它匹配了用户的默认组，则测试成功。由于-G比较只会 检查默认组而非用户所属的所有组，</span>

**
- **<span style="color:#f33b45">检查文件日期&nbsp;-nt比较会判定一个文件是否比另一个文件新。如果文件较新，那意味着它的文件创建日 期更近。-ot比较会判定一个文件是否比另一个文件旧。如果文件较旧，意味着它的创建日期 更早。</span>

**

<!-- -->

### 复合条件测试

if-then语句允许你使用布尔逻辑来组合测试。有两种布尔运算符可用：

- ** [ condition1 ] && [ condition2 ] **
- ** [ condition1 ] \|\| [ condition2 ]**

<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="110" role="region" aria-label=" 图像 小部件"><img alt="" height="310" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116191351283.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116191351283.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="677" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116191351283.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22677%22%2C%22height%22%3A%22310%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### if-then 的高级特性

<span style="color:#3399ea"><strong> 用于数学表达式的双括号</strong></span>

<span style="color:#3399ea"><strong> 用于高级字符串处理功能的双方括号</strong></span>

<span style="color:#3399ea"><strong>使用双括号</strong></span>

，<span style="color:#f33b45"><strong>(( expression ))&nbsp;</strong></span>

双括号命令允许你在**<span style="color:#f33b45">比较过程中使用高级数学表达</span>

**式。test命令只能在比较中使用简单的 算术操作。双括号命令提供了更多的数学符号，不需要将双括号中表达式里的大于号转义。这是双括号命令提供的另一个高级特性。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="109" role="region" aria-label=" 图像 小部件"><img alt="" height="405" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116185139347.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116185139347.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="728" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116185139347.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22728%22%2C%22height%22%3A%22405%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="108" role="region" aria-label=" 图像 小部件"><img alt="" height="296" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116185343913.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116185343913.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="486" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116185343913.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22486%22%2C%22height%22%3A%22296%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

、

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="107" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="20"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="106" role="region" aria-label=" 图像 小部件"><img alt="" height="642" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217235922315.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217235922315.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="651" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217235922315.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22651%22%2C%22height%22%3A%22642%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#3399ea"><strong>使用双方括号&nbsp;</strong></span>

<span style="color:#f33b45"><strong>[[ expression ]]</strong></span>

<span style="color:#3399ea"><strong></strong></span>

双方括号里的<span style="color:#f33b45"><strong>expression使用了test命令中采用的标准字符串比较</strong></span>

。但它提供了test命 令未提供的另一个特性——**<span style="color:#f33b45">模式匹配(pattern matching)</span>

**。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="105" role="region" aria-label=" 图像 小部件"><img alt="" height="233" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116185849287.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116185849287.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="504" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116185849287.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22504%22%2C%22height%22%3A%22233%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### case 命令

case命令会将指定的变量与不同模式进行比较。如果变量和模式是匹配的，那么shell会执行 为该模式指定的命令。可以通过竖线操作符在一行中分隔出多个模式模式。星号会捕获所有与已 知模式不匹配的值。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="104" role="region" aria-label=" 图像 小部件"><img alt="" height="359" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116185015313.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116185015313.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="606" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116185015313.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22606%22%2C%22height%22%3A%22359%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

## <span style="color:#3399ea">第13章更多的结构化命令</span>



### for命令

遍历：for命令最基本的用法就是遍历for命令自身所定义的一系列值。<span style="color:#3399ea"><strong>for循环假定每个值都是用空格分割的</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="103" role="region" aria-label=" 图像 小部件"><img alt="" height="536" data-cke-saved-src="https://img-blog.csdnimg.cn/20210215234712198.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210215234712198.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="723" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210215234712198.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22723%22%2C%22height%22%3A%22536%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**读取列表中的复杂值，<span style="color:#f33b45">使用转义字符(反斜线)来将单引号转义； 使用双引号来定义用到单引号的值。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="102" role="region" aria-label=" 图像 小部件"><img alt="" height="503" data-cke-saved-src="https://img-blog.csdnimg.cn/20210216100240242.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210216100240242.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="684" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210216100240242.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22684%22%2C%22height%22%3A%22503%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



<span style="color:#f33b45"><strong>for命令用空格来划分列表中的每个值。如果在单独的数据值中有 空格，就必须用双引号将这些值圈起来</strong></span>

<span style="color:#f33b45"><strong>从变量读取列表</strong></span>

<span style="color:#f33b45"><strong><span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="101" role="region" aria-label=" 图像 小部件"><img alt="" height="394" data-cke-saved-src="https://img-blog.csdnimg.cn/20210216100941542.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210216100941542.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="698" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210216100941542.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22698%22%2C%22height%22%3A%22394%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span></strong></span>

<span style="color:#f33b45"><strong>从命令读取值：</strong></span>

生成列表中所需值的另外一个途径就是使用命令的输出。可以用命令替换来执行任何能产生输出的命令，然后在for命令中使用该命令的输出。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="100" role="region" aria-label=" 图像 小部件"><img alt="" height="479" data-cke-saved-src="https://img-blog.csdnimg.cn/20210216101346108.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210216101346108.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="596" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210216101346108.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22596%22%2C%22height%22%3A%22479%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>更改字段分隔符：特殊的环境变量IFS，叫作内部字段分隔符(internal field separator)。 IFS环境变量定义了</strong></span>

<span style="color:#3399ea"><strong>bash shell用作字段分隔符的一系列字符： 空格  制表符  换行符(解决但行数据中的空格问题)</strong>IFS=$'\n'</span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="99" role="region" aria-label=" 图像 小部件"><img alt="" height="238" data-cke-saved-src="https://img-blog.csdnimg.cn/20210216101922616.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210216101922616.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="749" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210216101922616.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22749%22%2C%22height%22%3A%22238%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

读取 /etc/pwsswd文件，可以将IFS替换为：。

**<span style="color:#f33b45">用通配符读取目录：</span>

**应该将$file变 量用双引号圈起来

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="98" role="region" aria-label=" 图像 小部件"><img alt="" height="584" data-cke-saved-src="https://img-blog.csdnimg.cn/20210216111538600.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210216111538600.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="732" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210216111538600.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22732%22%2C%22height%22%3A%22584%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### C语言风格的for命令

for (( variable assignment ; condition ; iteration process ))

 变量赋值可以有空格；  条件中的变量不以美元符开头；  迭代过程的算式未用expr命令格式。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="97" role="region" aria-label=" 图像 小部件"><img alt="" height="486" data-cke-saved-src="https://img-blog.csdnimg.cn/20210216112132568.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210216112132568.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="639" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210216112132568.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22639%22%2C%22height%22%3A%22486%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>使用多个变量</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="96" role="region" aria-label=" 图像 小部件"><img alt="" height="423" data-cke-saved-src="https://img-blog.csdnimg.cn/20210216112441783.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210216112441783.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="612" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210216112441783.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22612%22%2C%22height%22%3A%22423%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### while命令(可以使用多个测试命令)

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="95" role="region" aria-label=" 图像 小部件"><img alt="" height="289" data-cke-saved-src="https://img-blog.csdnimg.cn/20210216113849951.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210216113849951.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="706" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210216113849951.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22706%22%2C%22height%22%3A%22289%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### until命令

until命令和while命令工作的方式完全相反，即条件问false时才循环。until命令要求你指定一个通常返回非零退 出状态码的测试命令

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="94" role="region" aria-label=" 图像 小部件"><img alt="" height="352" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217220605283.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217220605283.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="778" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217220605283.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22778%22%2C%22height%22%3A%22352%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 嵌套循环

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="93" role="region" aria-label=" 图像 小部件"><img alt="" height="547" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217221006239.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217221006239.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="745" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217221006239.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22745%22%2C%22height%22%3A%22547%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 循环处理文件数据

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="92" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="21"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="91" role="region" aria-label=" 图像 小部件"><img alt="" height="644" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217224855837.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217224855837.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="692" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217224855837.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22692%22%2C%22height%22%3A%22644%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



### 控制循环

 break命令  continue命令

**跳出单个循环**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="90" role="region" aria-label=" 图像 小部件"><img alt="" height="436" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217225115569.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217225115569.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="597" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217225115569.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22597%22%2C%22height%22%3A%22436%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**跳出内部循环**

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="89" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="22"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="88" role="region" aria-label=" 图像 小部件"><img alt="" height="721" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217230243515.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217230243515.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="685" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217230243515.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22685%22%2C%22height%22%3A%22721%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**跳出外部循环**

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="87" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="23"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="86" role="region" aria-label=" 图像 小部件"><img alt="" height="356" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217230611328.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217230611328.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="724" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217230611328.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22724%22%2C%22height%22%3A%22356%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### continue 命令

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="85" role="region" aria-label=" 图像 小部件"><img alt="" height="496" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217232107373.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217232107373.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="720" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217232107373.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22720%22%2C%22height%22%3A%22496%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 处理循环的輸出

在shell脚本中，<span style="color:#3399ea"><strong>你可以对循环的输出使用管道或进行重定向。这可以通过在done命令 之后添加一个处理命令来实现</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="84" role="region" aria-label=" 图像 小部件"><img alt="" height="429" data-cke-saved-src="https://img-blog.csdnimg.cn/20210217232333989.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210217232333989.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="696" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210217232333989.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22696%22%2C%22height%22%3A%22429%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 实例

<span style="color:#f33b45"><strong>查找可执行文件：当你从命令行中运行一个程序的时候，Linux系统会搜索一系列目录来查找对应的文件。这 些目录被定义在环境变量PATH中。如果你想找出系统中有哪些可执行文件可供使用，只需要扫 描PATH环境变量中所有的目录就行了。</strong></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="83" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="24"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="82" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="25"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="81" role="region" aria-label=" 图像 小部件"><img alt="" height="406" data-cke-saved-src="https://img-blog.csdnimg.cn/2021021800435321.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2021021800435321.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="793" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021021800435321.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22793%22%2C%22height%22%3A%22406%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="80" role="region" aria-label="代码段 小部件"><precode language="html" precodenum="26"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

创建多个用户账户(直接指定默认密码不是加密的密码。所以无法登录)

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="79" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="27"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

### 小结

## <span style="color:#3399ea">第14章处理用户输入</span>



### 命令行参数

- **向shell脚本传递数据的最基本方法是使用命令行参数。<span style="color:#f33b45">命令行参数允许在运行脚本时向命令 行添加数据。</span>

**
- **bash shell会将一些称为<span style="color:#f33b45">位置参数(positional parameter)的特殊变量</span>

分配给输入到命令行中的 所有参数。**
- **位置参数变量是标准的数字**：<span style="color:#3399ea"><strong>$0是程序名，$1是第 一个参数，$2是第二个参数，依次类推，直到第九个参数$9</strong></span>

- **每个参数都是用空格分隔的，所以shell会将空格当成两个值的分隔符。要在参数值中 包含空格，必须要用引号**

<!-- -->

<span style="color:#f33b45"><strong>读取脚本名：basename命令会返回不包含路径的脚本名</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="78" role="region" aria-label=" 图像 小部件"><img alt="" height="248" data-cke-saved-src="https://img-blog.csdnimg.cn/20210129210639359.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210129210639359.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="602" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210129210639359.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22602%22%2C%22height%22%3A%22248%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="77" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="28"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="76" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="29"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="75" role="region" aria-label=" 图像 小部件"><img alt="" height="455" data-cke-saved-src="https://img-blog.csdnimg.cn/20210129211446391.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210129211446391.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="773" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210129211446391.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22773%22%2C%22height%22%3A%22455%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**测试参数：**<span style="color:#3399ea"><strong>在使用参数前一定要检查其中是否存在数据，</strong></span>

<span style="color:#f33b45"><strong>使用了-n测试来检查命令行参数$1中是否有数据</strong></span>

<span style="color:#3399ea"><strong>。</strong></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="74" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="30"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="73" role="region" aria-label=" 图像 小部件"><img alt="" height="345" data-cke-saved-src="https://img-blog.csdnimg.cn/20210129212705339.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210129212705339.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="602" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210129212705339.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22602%22%2C%22height%22%3A%22345%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 特殊参数变量

<span style="color:#f33b45"><strong>特殊变量$#含有脚本运行时携带的命令行参数的个数，</strong></span>

也可用于参数前测试参数的总数。

<span style="color:#f33b45"><strong>抓取所有的数据:$*和$@变量可以在单个变量中存储所有的命令行参数</strong></span>

- **$\*变量会将命令行上提供的所有参数当作一个单词保存。这个单词包含了命令行中出现的每 一个参数值。基本上$\*变量会将这些参数视为一个整体**
- **$@变量会将命令行上提供的所有参数当作同一字符串中的多个独立的单词。这样 你就能够遍历所有的参数值，得到每个参数。这通常通过for命令完成**

<!-- -->

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="72" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="31"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

### 移动变量

bash shell的shift命令能够用来操作命令行参 数。shift命令会根据它们的相对位置来移动命令行参数,

- **默认情况下它会将每个参数变量向左移动一个位置。所以，变量$3 的值会移到$2中，变量$2的值会移到$1中，而变量$1的值则会被删除(注意，变量$0的值，也 就是程序名，不会改变)。**

<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="71" role="region" aria-label=" 图像 小部件"><img alt="" height="222" data-cke-saved-src="https://img-blog.csdnimg.cn/20210130183356527.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210130183356527.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="767" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210130183356527.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22767%22%2C%22height%22%3A%22222%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="70" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="32"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

### 处理选项

**<span style="color:#f33b45">在提取每个单独参数时，用case语句(参见第12章)来判断某个参数是否为选项。</span>

**

case语句会检查每个参数是不是有效选项。如果是的话，就运行对应case语句中的命令。 不管选项按什么顺序出现在命令行上，这种方法都适用。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="69" role="region" aria-label=" 图像 小部件"><img alt="" height="418" data-cke-saved-src="https://img-blog.csdnimg.cn/20210130184352598.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210130184352598.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="632" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210130184352598.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22632%22%2C%22height%22%3A%22418%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



分离参数和选项,对Linux来说，这个特殊字符是双破折线(--)。shell会用双破折线来表明选项列表结束。：

使用 getopt 命令：getopt命令可以接受一系列任意形式的命令行选项和参数，并自动将它们转换成适当的格 式。它的命令格式如下：

getopt optstring parameters

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="68" role="region" aria-label=" 图像 小部件"><img alt="" height="257" data-cke-saved-src="https://img-blog.csdnimg.cn/20210130185927412.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210130185927412.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="791" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210130185927412.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22791%22%2C%22height%22%3A%22257%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

在脚本中使用getopt 可以在脚本中使用getopt来格式化脚本所携带的任何命令行选项或参数

### 将选项标准化

在创建shell脚本时，显然可以控制具体怎么做。你完全可以决定用哪些字母选项以及它们的 用法。 但有些字母选项在Linux世界里已经拥有了某种程度的标准含义。如果你能在shell脚本中支 持这些选项，脚本看起来能更友好一些。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="67" role="region" aria-label=" 图像 小部件"><img alt="" height="573" data-cke-saved-src="https://img-blog.csdnimg.cn/2021020120150995.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2021020120150995.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="806" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021020120150995.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22806%22%2C%22height%22%3A%22573%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>



### 获得用户输入

基本的读取：<span style="color:#f33b45"><strong>read命令从标准输入(键盘)或另一个文件描述符中接受输入。在收到输入后，read命令 会将数据放进一个变量。</strong></span>

<br>

## <span style="color:#3399ea">第15章呈现数据</span>



### 理解输入和输出

<span style="color:#f33b45"><strong>标准文件描述符</strong></span>

：**Linux系统将每个对象当作文件处理。这包括输入和输出进程**<span style="color:#3399ea"><strong>。Linux用文件描述符(file descriptor)来标识每个文件对象。文件描述符是一个非负整数，可以唯一标识会话中打开 的文件。</strong></span>

<span style="color:#f33b45"><strong>每个进程一次最多可以有</strong></span>

**九个文件描述符**<span style="color:#f33b45"><strong>。出于特殊目的，bash shell保留了前三个文 件描述符(0、1和2)。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="66" role="region" aria-label=" 图像 小部件"><img alt="" height="160" data-cke-saved-src="https://img-blog.csdnimg.cn/20210119190015603.png" src="https://img-blog.csdnimg.cn/20210119190015603.png" width="871" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210119190015603.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22871%22%2C%22height%22%3A%22160%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>&nbsp;STDIN ：</strong></span>

STDIN文件描述符代表shell的标准输入。<span style="color:#3399ea"><strong>对终端界面来说，标准输入是键盘。</strong></span>

shell从STDIN 文件描述符对应的键盘获得输入，在用户输入时处理每个字符。**在使用输入重定向符号(<)时，Linux会用重定向指定的文件来替换标准输入文件描述符。 **它会读取文件并提取数据，就如同它是键盘上键入的。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="65" role="region" aria-label=" 图像 小部件"><img alt="" height="440" data-cke-saved-src="https://img-blog.csdnimg.cn/20210119190652322.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210119190652322.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="777" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210119190652322.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22777%22%2C%22height%22%3A%22440%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>STDOUT：</strong></span>

 STDOUT文件描述符代表shell的标准输出。**<span style="color:#f33b45">在终端界面上，标准输出就是终端显示器</span>

**。shell 的所有输出(包括shell中运行的程序和脚本)会被定向到标准输出中，也就是显示器。**<span style="color:#3399ea">大多数bash命令会将输出导向STDOUT文件描述符。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="64" role="region" aria-label=" 图像 小部件"><img alt="" height="186" data-cke-saved-src="https://img-blog.csdnimg.cn/20210119191217274.png" src="https://img-blog.csdnimg.cn/20210119191217274.png" width="707" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210119191217274.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22707%22%2C%22height%22%3A%22186%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>STDERR</strong></span>

 shell通过特殊的STDERR文件描述符来处理错误消息。**<span style="color:#f33b45">STDERR文件描述符代表shell的标准错 误输出。shell或shell中运行的程序和脚本出错时生成的错误消息都会发送到这个位置。</span>

**默认情况下，错误消息也会输出到显示器输出中。

重定向错误，只重定向错误

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="63" role="region" aria-label=" 图像 小部件"><img alt="" height="131" data-cke-saved-src="https://img-blog.csdnimg.cn/20210119191535575.png" src="https://img-blog.csdnimg.cn/20210119191535575.png" width="549" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210119191535575.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22549%22%2C%22height%22%3A%22131%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

重定向错误和数据，**为了避免错误信息散落在输出文件中，相较于标准输出，bashshell自动赋予了错误消息更高的优先级。这样你能够集中浏览错误信息了。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="62" role="region" aria-label=" 图像 小部件"><img alt="" height="403" data-cke-saved-src="https://img-blog.csdnimg.cn/20210119192421140.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210119192421140.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="777" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210119192421140.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22777%22%2C%22height%22%3A%22403%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 在脚本中重定向输出

可以在脚本中用STDOUT和STDERR文件描述符以在多个位置生成输出，只要简单地重定向相应的文件描述符就行了。有两种方法来在脚本中重定向输出：<span style="color:#f33b45"><strong>临时重定向行输出 ， 永久重定向脚本中的所有命令。</strong></span>

<span style="color:#3399ea"><strong>临时重定向：</strong></span>

<span style="color:#f33b45"><strong>如果有意在脚本中生成错误消息，可以将单独的一行输出重定向到STDERR</strong></span>

，你所需要做的 是使用输出重定向符来将输出信息重定向到STDERR文件描述符。在重定向到文件描述符时，你 必须在文件描述符数字之前加一个&：

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="61" role="region" aria-label=" 图像 小部件"><img alt="" height="318" data-cke-saved-src="https://img-blog.csdnimg.cn/20210119193915563.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210119193915563.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="692" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210119193915563.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22692%22%2C%22height%22%3A%22318%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

默认情况下<span style="color:#f33b45"><strong>，Linux会将STDERR导向STDOUT。但是，如果你在运行脚本时重定向了 STDERR，脚本中所有导向STDERR的文本都会被重定向。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="60" role="region" aria-label=" 图像 小部件"><img alt="" height="401" data-cke-saved-src="https://img-blog.csdnimg.cn/20210119195432241.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210119195432241.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="712" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210119195432241.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22712%22%2C%22height%22%3A%22401%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**<span style="color:#3399ea">永久重定向</span>

**

如果脚本中有大量数据需要重定向，那重定向每个echo语句就会很烦琐。<span style="color:#f33b45"><strong>取而代之，你可 以用exec命令告诉shell在脚本执行期间重定向某个特定文件描述符。</strong></span>

<span style="color:#3399ea"><strong>exec命令会启动一个新shell并将STDOUT文件描述符重定向到文件。</strong></span>

**脚本中发给STDOUT的所 有输出会被重定向到文件。 可以在脚本执行过程中重定向STDOUT。<span style="color:#7c79e5">一旦重定向了STDOUT或STDERR，就很难再将它们重定向回原来的位置。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="59" role="region" aria-label=" 图像 小部件"><img alt="" height="453" data-cke-saved-src="https://img-blog.csdnimg.cn/20210119205528807.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210119205528807.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="765" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210119205528807.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22765%22%2C%22height%22%3A%22453%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

在脚本中重定向输入：**使用与脚本中重定向STDOUT和STDERR相同的方法来将STDIN从键盘重定向到其他 位置。exec命令允许你将STDIN重定向到Linux系统上的文件中。**

### 创建自己的重定向

在shell 中最多可以有9个打开的文件描述符。其他6个从3\~8的文件描述符均可用作输入或输出重定向。

**<span style="color:#f33b45">创建输出文件描述符</span>

**:可以用exec命令来给输出分配文件描述符。和标准的文件描述符一样，一旦将另一个文件 描述符分配给一个文件，这个重定向就会一直有效，直到你重新分配。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="58" role="region" aria-label=" 图像 小部件"><img alt="" height="539" data-cke-saved-src="https://img-blog.csdnimg.cn/20210121231902376.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210121231902376.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="765" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210121231902376.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22765%22%2C%22height%22%3A%22539%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>重定向文件描述符</strong></span>

: 怎么恢复已重定向的文件描述符。你可以分配另外一个文件描述符给标准文件描述 符，反之亦然

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="57" role="region" aria-label=" 图像 小部件"><img alt="" height="381" data-cke-saved-src="https://img-blog.csdnimg.cn/20210121233345872.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210121233345872.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="698" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210121233345872.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22698%22%2C%22height%22%3A%22381%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

.

<span style="color:#f33b45"><strong>创建输入文件描述符</strong></span>

：可以用和重定向输出文件描述符同样的办法重定向输入文件描述符

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="56" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="33"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span style="color:#f33b45"><strong>创建读写文件描述符：</strong></span>

可以打开单个文件描述符来作为输入和输出。可以用同 一个文件描述符对同一个文件进行读写，由于你是对同一个文件进行数据读写，shell会维护一个 内部指针，指明在文件中的当前位置。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="55" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="34"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

**关闭文件描述符 (**exec 3>&-**)：**创建了新的输入或输出文件描述符，shell会在脚本退出时自动关闭它们。有些情况下，你需要在脚本结束前手动关闭文件描述符。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="54" role="region" aria-label=" 图像 小部件"><img alt="" height="240" data-cke-saved-src="https://img-blog.csdnimg.cn/20210122203227107.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210122203227107.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="735" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210122203227107.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22735%22%2C%22height%22%3A%22240%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 列出打开的文件描述符

<span style="color:#f33b45"><strong>lsof命令会列出整个Linux系统打开的所有文件描述符</strong></span>

### 阻止命令输出

在Linux系统上null文件的标准位置是**/dev/null**。你重定向到该位置的任何数据都会被丢掉， 不会显示。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="53" role="region" aria-label=" 图像 小部件"><img alt="" height="150" data-cke-saved-src="https://img-blog.csdnimg.cn/20210122204716962.png" src="https://img-blog.csdnimg.cn/20210122204716962.png" width="773" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210122204716962.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22773%22%2C%22height%22%3A%22150%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>也可以在输入重定向中将/dev/null作为输入文件。由于/dev/null文件不含有任何内容，程序员 通常用它来快速清除现有文件中的数据，而不用先删除文件再重新创建。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="52" role="region" aria-label=" 图像 小部件"><img alt="" height="386" data-cke-saved-src="https://img-blog.csdnimg.cn/20210122205006368.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210122205006368.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="760" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210122205006368.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22760%22%2C%22height%22%3A%22386%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#3399ea">创建临时文件</span>



<span style="color:#f33b45"><strong>Linux系统有特殊的目录，专供临时文件使用。Linux使用/tmp目录来存放不需要永久保留的 文件。大多数Linux发行版配置了系统在启动时自动删除/tmp目录的所有文件。</strong></span>

系统上的任何用户账户都有权限在读写/tmp目录中的文件。这个特性为你提供了一种创建临 时文件的简单方法，而且还不用操心清理工作。

**有个特殊命令可以用来创建临时文件。<span style="color:#f33b45">mktemp命令</span>

可以在/tmp目录中创建一个唯一的临时 文件。shell会创建这个文件，但不用默认的umask值**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="51" role="region" aria-label=" 图像 小部件"><img alt="" height="213" data-cke-saved-src="https://img-blog.csdnimg.cn/20210122211545518.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210122211545518.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="735" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210122211545518.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22735%22%2C%22height%22%3A%22213%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

创建本地临时文件，mktemp命令的输出正是它所创建的文件的名字。在脚本中使用mktemp命令 时，可能要将文件名保存到变量中，这样就能在后面的脚本中引用了。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="50" role="region" aria-label=" 图像 小部件"><img alt="" height="154" data-cke-saved-src="https://img-blog.csdnimg.cn/2021012221200012.png" src="https://img-blog.csdnimg.cn/2021012221200012.png" width="545" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021012221200012.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22545%22%2C%22height%22%3A%22154%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

mktemp命令的输出正是它所创建的文件的名字。在脚本中使用mktemp命令 时，可能要将文件名保存到变量中，这样就能在后面的脚本中引用了。

### 创建临时目录.

\-d选项告诉mktemp命令来创建一个临时目录而不是临时文件

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="49" role="region" aria-label=" 图像 小部件"><img alt="" height="422" data-cke-saved-src="https://img-blog.csdnimg.cn/20210129200912820.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210129200912820.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="764" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210129200912820.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22764%22%2C%22height%22%3A%22422%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="48" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="35"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

### <span style="color:#3399ea">记录消息</span>



将输出同时发送到显示器和日志文件，这种做法有时候能够派上用场。**<span style="color:#f33b45">你不用将输出重定向 两次</span>

**，只要用特殊的tee命令就行.<span style="color:#f33b45"><strong>既能将数据保存在文件中，也能将数据显示在屏幕上。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="47" role="region" aria-label=" 图像 小部件"><img alt="" height="208" data-cke-saved-src="https://img-blog.csdnimg.cn/20210129201519900.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210129201519900.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="701" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210129201519900.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22701%22%2C%22height%22%3A%22208%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#3399ea">实例</span>



**文件重定向常见于脚本需要读入文件和输出文件时。这个样例脚本两件事都做了。它读取.csv 格式的数据文件，输出SQL INSERT语句来将数据插入数据库**。

shell脚本使用命令行参数指定待读取的.csv文件。.csv格式用于从电子表格中导出数据，所以 你可以把数据库数据放入电子表格中，把电子表格保存成.csv格式，读取文件，然后创建INSERT 语句将数据插入MySQL数据库。

书的这个地方可能有问题，加了 < 变成两个参数了。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="46" role="region" aria-label=" 图像 小部件"><img alt="" height="271" data-cke-saved-src="https://img-blog.csdnimg.cn/20210129205510376.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210129205510376.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="766" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210129205510376.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22766%22%2C%22height%22%3A%22271%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="45" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="36"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="44" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="37"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

脚本的输入/输出都可以从标准输 入(STDIN)/标准输出(STDOUT)重定向到系统中的任意文件。除了STDOUT，你可以通过重定 向STDERR输出来重定向由脚本产生的错误消息。这可以通过重定向与STDERR输出关联的文件描 述符(也就是文件描述符2)来实现。可以将STDERR输出和STDOUT输出到同一个文件中，也可 以输出到完全不同的文件中。这样就可以将脚本的正常消息同错误消息分离开。

## <span style="color:#3399ea">第16章控制脚本</span>



### 处理信号

Linux利用信号与运行在系统中的进程进行通信，<span style="color:#3399ea"><strong>不同的Linux信号以及Linux如 何用这些信号来停止、启动、终止进程。可以通过对脚本进行编程，使其在收到特定信号时执行 某些命令，从而控制shell脚本的操作。</strong></span>

Linux系统和应用程序可以生成超过30个信号，

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="43" role="region" aria-label=" 图像 小部件"><img alt="" height="317" data-cke-saved-src="https://img-blog.csdnimg.cn/20210115215249771.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210115215249771.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="867" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210115215249771.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22867%22%2C%22height%22%3A%22317%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**默认情况下，<span style="color:#3399ea">bash shell会忽略收到的任何SIGQUIT (3)和SIGTERM (5)信号</span>

(正因为这样， 交互式shell才不会被意外终止)。但是<span style="color:#3399ea">bash shell会处理收到的SIGHUP (1)和SIGINT (2)信号</span>

。**

**生成信号；bash shell允许用键盘上的组合键生成两种基本的Linux信号。**

- **<span style="color:#3399ea">中断进程 Ctrl+C组合键会生成SIGINT信号，并将其发送给当前在shell中运行的所有进程</span>

**
- **<span style="color:#3399ea">暂停进程 Ctrl+Z组合键会生成一个SIGTSTP信号， 你可以在进程运行期间暂停进程，而无需终止它。尽管有时这可能会比较危险(比如，脚本 打开了一个关键的系统文件的文件锁)，但通常它可以在不终止进程的情况下使你能够深入脚本 内部一窥究竟。</span>

**
- **<span style="color:#3399ea">停止shell中运行的任何进程。停止(stopping)进程跟终止(terminating)进程不同：停止进程会让程序继续保留在内存中，并能从上次停止的位置 继续运行。</span>

**

<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="42" role="region" aria-label=" 图像 小部件"><img alt="" height="228" data-cke-saved-src="https://img-blog.csdnimg.cn/20210115221526723.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210115221526723.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="786" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210115221526723.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22786%22%2C%22height%22%3A%22228%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

在S列中(进程状态)，ps命令将已停止作业的状态为显示为T。**这说明命令要么被跟踪，要 么被停止了。**

**如果在有已停止作业存在的情况下，你仍旧想退出shell，只要再输入一遍exit命令就行了。 shell会退出**，**终止已停止作业。或者，既然你已经知道了已停止作业的PID，就可以用kill命令 来发送一个SIGKILL信号来终止它。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="41" role="region" aria-label=" 图像 小部件"><img alt="" height="308" data-cke-saved-src="https://img-blog.csdnimg.cn/20210115221813991.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210115221813991.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="887" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210115221813991.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22887%22%2C%22height%22%3A%22308%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#3399ea">在终止作业时，最开始你不会得到任何回应。但下次如果你做了能够产生shell提示符的操作 (比如按回车键)，你就会看到一条消息，显示作业已经被终止了.</span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="40" role="region" aria-label=" 图像 小部件"><img alt="" height="304" data-cke-saved-src="https://img-blog.csdnimg.cn/20210115222058863.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210115222058863.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="869" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210115222058863.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22869%22%2C%22height%22%3A%22304%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>捕获信号</strong></span>

：也可以不忽略信号，在信号出现时捕获它们并执行其他命令。<span style="color:#f33b45"><strong>trap命令允许你来指定shell 脚本要监看并从shell中拦截的Linux信号。</strong></span>

如果脚本收到了trap命令中列出的信号，该信号不再 由shell处理，而是交由本地处理。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="39" role="region" aria-label=" 图像 小部件"><img alt="" height="170" data-cke-saved-src="https://img-blog.csdnimg.cn/2021011613230664.png" src="https://img-blog.csdnimg.cn/2021011613230664.png" width="666" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021011613230664.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22666%22%2C%22height%22%3A%22170%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="38" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="38"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="37" role="region" aria-label=" 图像 小部件"><img alt="" height="413" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116132408188.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116132408188.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="670" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116132408188.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22670%22%2C%22height%22%3A%22413%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 捕获脚本退出

除了在shell脚本中捕获信号，你也可以在shell脚本退出时进行捕获(类似于钩子进程)。这是在shell完成任务时 执行命令的一种简便方法。要捕获shell脚本的退出，只要在trap命令后加上EXIT信号就行。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="36" role="region" aria-label=" 图像 小部件"><img alt="" height="443" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116132946332.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116132946332.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="675" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116132946332.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22675%22%2C%22height%22%3A%22443%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 修改或移除捕获

脚本中的不同位置进行不同的捕获处理，只需重新使用带有新选项的trap命令。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="35" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="39"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

** 以删除已设置好的捕获。只需要在trap命令与希望恢复默认行为的信号列表之间加上 <span style="color:#f33b45">两个破折号</span>

就行了，以在trap命令后使用<span style="color:#f33b45">单破折号来恢复信号的默认行为</span>

。单破折号和双破折号都可以 正常发挥作用**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="34" role="region" aria-label=" 图像 小部件"><img alt="" height="537" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116135922654.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116135922654.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="643" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116135922654.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22643%22%2C%22height%22%3A%22537%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

移除信号捕获后，脚本按照默认行为来处理SIGINT信号，也就是终止脚本运行。但如果信 号是在捕获被移除前接收到的，那么脚本会按照原先trap命令中的设置进行处理

### 以后台模式运行脚本

以后台模式运行shell脚本非常简单。<span style="color:#f33b45"><strong>只要在命令后加个&amp;符就行了</strong></span>

。运行多个后台作业，每次启动新作业时，Linux系统都会为其分配一个新的作业号和PID。通过ps命令，可以看到 所有脚本处于运行状态

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="33" role="region" aria-label=" 图像 小部件"><img alt="" height="346" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116140741125.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116140741125.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="747" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116140741125.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22747%22%2C%22height%22%3A%22346%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 在非控制台下运行脚本

<span style="color:#f33b45"><strong>nohup命令运行了另外一个命令来阻断所有发送给该进程的SIGHUP信号</strong></span>

。这会在退出终端会 话时阻止进程退出。

shell会给命令分配一个作业号，Linux系统会为其分配一个PID号。区 别在于，**当你使用nohup命令时，如果关闭该会话，脚本会忽略终端会话发过来的SIGHUP信号**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="32" role="region" aria-label=" 图像 小部件"><img alt="" height="74" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116173048420.png" src="https://img-blog.csdnimg.cn/20210116173048420.png" width="798" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116173048420.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22798%22%2C%22height%22%3A%2274%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

由于nohup命令会解除终端与进程的关联，进程也就不再同STDOUT和STDERR联系在一起。 <span style="color:#f33b45"><strong>为了保存该命令产生的输出，nohup命令会自动将STDOUT和STDERR的消息重定向到一个名为 </strong></span>

<span style="color:#3399ea"><strong>nohup.out</strong></span>

<span style="color:#f33b45"><strong>的文件中。</strong></span>

nohup.out文件包含了通常会发送到终端显示器上的所有输出。在进程完成运行后，你可以查 看nohup.out文件中的输出结果。

### 作业控制

启动、停止、终止以及恢复作业的这些功能统称为作业控制,<span style="color:#f33b45"><strong>作业控制中的关键命令是jobs命令</strong></span>

。jobs命令允许查看shell当前正在处理的作业。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="31" role="region" aria-label=" 图像 小部件"><img alt="" height="257" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116173527955.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116173527955.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="703" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116173527955.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22703%22%2C%22height%22%3A%22257%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

要想查看作业的PID，可以在jobs命令中加入-l选项

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="30" role="region" aria-label=" 图像 小部件"><img alt="" height="244" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116173658336.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116173658336.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="731" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116173658336.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22731%22%2C%22height%22%3A%22244%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="29" role="region" aria-label=" 图像 小部件"><img alt="" height="214" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116173723227.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116173723227.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="874" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116173723227.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22874%22%2C%22height%22%3A%22214%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- **jobs命令输出中的加号和减号。<span style="color:#f33b45">带加号的作业会被当做默认作业。在使用 作业控制命令时，如果未在命令行指定任何作业号，该作业会被当成作业控制命令的操作对象。 </span>

**
- **当前的默认作业完成处理后，带减号的作业成为下一个默认作业。任何时候都只有一个带加号的作业和一个带减号的作业，不管shell中有多少个正在运行的作业。**

<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="28" role="region" aria-label=" 图像 小部件"><img alt="" height="302" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116174343498.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116174343498.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="908" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116174343498.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22908%22%2C%22height%22%3A%22302%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- <span style="color:#f33b45">要以后台模式重启一个作业，可用bg命令加上作业号</span>

，当作业被转入后台模式时，并不会列出其PID。
- 要以前台模式重启作业，可用带有作业号的fg命令。

<!-- -->

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="27" role="region" aria-label=" 图像 小部件"><img alt="" height="263" data-cke-saved-src="https://img-blog.csdnimg.cn/20210116174701587.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210116174701587.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="679" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210116174701587.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22679%22%2C%22height%22%3A%22263%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### <span style="color:#f33b45">调整谦让度(</span>

修改脚本优先级<span style="color:#f33b45">)</span>



在多任务操作系统中(Linux就是)，内核负责将CPU时间分配给系统上运行的每个进程。调 度优先级(scheduling priority)是内核分配给进程的CPU时间(相对于其他进程)。<span style="color:#3399ea"><strong>在Linux系统 中，由shell启动的所有进程的调度优先级默认都是相同的。</strong></span>

调度优先级是个整数值，<span style="color:#3399ea"><strong>从-20(最高优先级)到+19(最低优先级)。默认情况下，bash shell 以优先级0来启动所有进程</strong></span>

。

**nice 命令：<span style="color:#f33b45">nice命令允许你设置命令启动时的调度优先级。要让命令以更低的优先级运行，只要用nice 的-n命令行来指定新的优先级级别。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="26" role="region" aria-label=" 图像 小部件"><img alt="" height="172" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112220913910.png" src="https://img-blog.csdnimg.cn/20210112220913910.png" width="553" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112220913910.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22553%22%2C%22height%22%3A%22172%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>nice命令的-n选项并不是必须的，只需要在破折号后面跟上优先级就行了。</strong></span>

nice命令阻止普通系统用户来提高命令的优先级。

<span style="color:#f33b45"><strong><span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="25" role="region" aria-label=" 图像 小部件"><img alt="" height="134" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112221012923.png" src="https://img-blog.csdnimg.cn/20210112221012923.png" width="503" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112221012923.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22503%22%2C%22height%22%3A%22134%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span></strong></span>

**renice 命令：<span style="color:#f33b45">它允许你指定 运行进程的PID来改变它的优先级。</span>

**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="24" role="region" aria-label=" 图像 小部件"><img alt="" height="304" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112221550897.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112221550897.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="805" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112221550897.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22805%22%2C%22height%22%3A%22304%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

- ** 只能对属于你的进程执行renice；**
- ** 只能通过renice降低进程的优先级； **
- ** root用户可以通过renice来任意调整进程的优先级。 如果想完全控制运行进程，必须以root账户身份登录或使用sudo命令。**

<!-- -->

### <span style="color:#3399ea">定时运行作业(定时任务)</span>



**Linux系统提供了多个在预选时间运行脚本的方法：at命令和cron表。每个方法都使用不同的技术来安排脚本的运行时间和频率。接下来会依次介绍这些方法。**

<span style="color:#f33b45"><strong>用 at 命令来计划执行作业：</strong></span>

at命令允许指定Linux系统何时运行脚本。at命令会将作业提交到队列中，指定shell何时运 行该作业。at的守护进程atd会以后台模式运行，检查作业队列来运行作业。大多数Linux发行 版会在启动时运行此守护进程。

> atd守护进程会检查系统上的一个特殊目录<span style="color:#f33b45"><strong>(通常位于/var/spool/at)来获取用at命令提交的 作业</strong></span>
> 
> 。**<span style="color:#f33b45">默认情况下，atd守护进程会每60秒检查一下这个目录。有作业时，atd守护进程会检查 作业设置运行的时间。如果时间跟当前时间匹配，atd守护进程就会运行此作业。</span>
> 
> **

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="23" role="region" aria-label=" 图像 小部件"><img alt="" height="87" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112222009574.png" src="https://img-blog.csdnimg.cn/20210112222009574.png" width="564" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112222009574.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22564%22%2C%22height%22%3A%2287%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

at命令的基本格式非常简单：<span style="color:#f33b45"><strong> at [-f filename] time</strong></span>

 默认情况下，at命令会将STDIN的输入放到队列中。你可以用-f参数来指定用于读取命令(脚 本文件)的文件名。

time参数指定了Linux系统何时运行该作业。如果你指定的时间已经错过，at命令会在第二 天的那个时间运行指定的作业。

**<span style="color:#f33b45">at命令能识别多种不同的时间格式</span>

。 **

-  标准的小时和分钟格式，比如10:15。
-  AM/PM指示符，比如10:15 PM。
-  特定可命名时间，比如now、noon、midnight或者teatime(4 PM)。

<!-- -->

**除了指定运行作业的时间，也可以通过不同的日期格式指定特定的日期。 **

-  标准日期格式，比如MMDDYY、MM/DD/YY或DD.MM.YY。
-  文本日期，比如Jul 4或Dec 25，加不加年份均可。
-  你也可以指定时间增量。-  当前时间+25 min
    -  明天10:15 PM
    -  10:15+7天

    <!-- -->


<!-- -->

在你使用at命令时，**该作业会被提交到作业队列(job queue)**。作业队列会保存通过at命令 提交的待处理的作业。针对不同优先级，<span style="color:#f33b45"><strong>存在26种不同的作业队列。作业队列通常用小写字母a~z 和大写字母A~Z来指代。</strong></span>

作业队列的字母排序越高，作业运行的优先级就越低(更高的nice值)。**默认情况下，at的作业会被提交到a作业队列。如果想以更高优先级运行作业，可以用-q参数指定不同的队列字母。**

**获取作业的输出**，

> 当作业在Linux系统上运行时，显示器并不会关联到该作业，Linux系统会将 提交该作业的用户的电子邮件地址作为STDOUT和STDERR。任何发到STDOUT或STDERR的输出都 会通过邮件系统发送给该用户。使用e-mail作为at命令的输出极其不便。at命令利用sendmail应用程序来发送邮件。如 果你的系统中没有安装sendmail，那就无法获得任何输出！<span style="color:#f33b45"><strong>因此在使用at命令时，最好在脚本 中对STDOUT和STDERR进行重定向</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="22" role="region" aria-label=" 图像 小部件"><img alt="" height="210" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112223220916.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112223220916.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="549" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112223220916.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22549%22%2C%22height%22%3A%22210%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**at命令会显示分配给作业的作业号以及为作业安排的运行时间。-f选项指明使用哪个脚本 文件，now指示at命令立刻执行该脚本。如果不想在at命令中使用邮件或重定向，最好加上-M选项来屏蔽作业产生的输出信息。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="21" role="region" aria-label=" 图像 小部件"><img alt="" height="296" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112223736287.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112223736287.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="563" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112223736287.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22563%22%2C%22height%22%3A%22296%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>列出等待的作业：atq命令可以查看系统中有哪些作业在等待。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="20" role="region" aria-label=" 图像 小部件"><img alt="" height="394" data-cke-saved-src="https://img-blog.csdnimg.cn/2021011222422665.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/2021011222422665.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="583" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F2021011222422665.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22583%22%2C%22height%22%3A%22394%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>删除作业：用atrm命令来删除等待中的作业。</strong></span>

只能删除你提交的作业，不能删除其他人的。

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="19" role="region" aria-label=" 图像 小部件"><img alt="" height="301" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112224448156.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210112224448156.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="437" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112224448156.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22437%22%2C%22height%22%3A%22301%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 安排需要定期执行的脚本(脚本执行自动化)

<span style="color:#f33b45"><strong>Linux系统使用cron程序来安排要定期执行的作业。cron程序会在后台运行并检查一个特殊的 表(被称作cron时间表)，以获知已安排执行的作业。</strong></span>

cron时间表 cron时间表采用一种特别的格式来指定作业何时运行。cron时间表允许你用特定值、取值范围(比如1\~5)或者是通配符(星号)来指定条目其格式如下：

> <span style="color:#f33b45"><strong>min hour dayofmonth month dayofweek command</strong></span>

- **每天的10:15运行一个命令 ：<span style="color:#3399ea">15 10 * * * command&nbsp;</span>

 在dayofmonth、month以及dayofweek字段中使用了通配符，表明cron会在每个月每天的10:15 执行该命令**
- **每周一4:15 PM运行的命令：<span style="color:#3399ea">15 16 * * 1 command </span>

 可以用三字符的文本值(mon、tue、wed、thu、fri、sat、sun)或数值(0为周日，6为周六) 来指定dayofweek表项。**
- **在每个月的第一天中午12点执行命令 <span style="color:#3399ea">&nbsp;00 12 1 * * command&nbsp;</span>

 dayofmonth表项指定月份中的日期值(1\~31)。**
- **<span style="color:#f33b45">设置一个在每个月的最后一天执行的命令，</span>

<span style="color:#3399ea">&nbsp;00 12 * * * if [`date +%d -d tomorrow` = 01 ] ; then ; command&nbsp;</span>

<span style="color:#f33b45">它会在每天中午12点来检查是不是当月的最后一天，如果是，cron将会运行该命令。</span>

**
- **命令列表必须指定要运行的命令或脚本的全路径名。<span style="color:#3399ea">&nbsp;15 10 * * * /home/rich/test4.sh &gt; test4out&nbsp;</span>

 你可以像在普通的命令行中那样，添加 任何想要的命令行参数和重定向符号。(**cron程序会用提交作业的用户账户运行该脚本。因此，你必须有访问该命令和命令中指定的 输出文件的权限。**)**

<!-- -->

构建cron时间表 每个系统用户(包括root用户)都可以用自己的cron时间表来运行安排好的任务。<span style="color:#f33b45"><strong>Linux提供 了crontab命令来处理cron时间表。要列出已有的cron时间表，可以用-l选项。</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="18" role="region" aria-label=" 图像 小部件"><img alt="" height="62" data-cke-saved-src="https://img-blog.csdnimg.cn/20210112225522963.png" src="https://img-blog.csdnimg.cn/20210112225522963.png" width="540" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210112225522963.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22540%22%2C%22height%22%3A%2262%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**默认情况下，用户的cron时间表文件并不存在。要为cron时间表添加条目，可以用-e选项。 在添加条目时，crontab命令会启用一个文本编辑器，使用已有的cron时间表作 为文件内容(或者是一个空文件，如果时间表不存在的话)。**

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="17" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="40"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="16" role="region" aria-label=" 图像 小部件"><img alt="" data-cke-saved-src="https://img-blog.csdnimg.cn/20210102155155834.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210102155155834.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210102155155834.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22%22%2C%22height%22%3A%22%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**<span style="color:#f33b45">浏览cron目录</span>

 如果你创建的脚本对精确的执行时间要求不高，即你只是需要在指定小时，天，月，周末来执行指定的脚本，用预配置的cron脚本目录会更方便。有4个 基本目录：hourly、daily、monthly和weekly。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="15" role="region" aria-label=" 图像 小部件"><img alt="" height="270" data-cke-saved-src="https://img-blog.csdnimg.cn/20210115201210927.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210115201210927.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="942" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210115201210927.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22942%22%2C%22height%22%3A%22270%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

因此，如果脚本需要每天运行一次，只要将脚本复制到daily目录，cron就会每天执行它。

<span style="color:#f33b45"><strong>anacron程序，主要用于执行错过的作业，</strong></span>

如果某个作业在cron时间表中安排运行的时间已到，但这时候Linux系统处于关机状态，那么 这个作业就不会被运行。当系统开机时，cron程序不会再去运行那些错过的作业。要解决这个问 题，许多Linux发行版还包含了anacron程序。

这个功能常用于进行常规日志维护的脚本。如果系统在脚本应该运行的时间刚好关机， 日志文件就不会被整理，可能会变很大。通过anacron，至少可以保证系统每次启动时整理日 志文件。

**anacron程序只会处理位于cron目录的程序，比如/etc/cron.monthly。<span style="color:#f33b45">它用时间戳来决定作业 是否在正确的计划间隔内运行了。</span>

每个cron目录都有个时间戳文件，该文件位于/var/spool/ anacron。**

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="14" role="region" aria-label=" 图像 小部件"><img alt="" height="531" data-cke-saved-src="https://img-blog.csdnimg.cn/20210115201956858.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210115201956858.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="892" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210115201956858.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22892%22%2C%22height%22%3A%22531%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

**anacron时间表的基本格式和cron时间表略有不同**：

<span style="color:#f33b45"><strong>period delay identifier command </strong></span>

period条目定义了作业多久运行一次，以天为单位。anacron程序用此条目来检查作业的时间 戳文件。

- **delay条目会指定系统启动后anacron程序需要等待多少分钟再开始运行错过的脚本。**
- **command条目包含了run-parts程序和一个cron脚本目录名。run-parts程序负责运行目录中传给它的 任何脚本。 **- **anacron不会运行位于/etc/cron.hourly的脚本。这是因为anacron程序不会处理执行时间 需求小于一天的脚本。**

    <!-- -->

- **identifier条目是一种特别的非空字符串，如cron-weekly。它用于唯一标识日志消息和错误 邮件中的作业**。

<!-- -->

### 使用新 shell 启动脚本

<span style="color:#f33b45"><strong>当用户登入bash shell时需要运行的启动文件</strong></span>

- <span style="color:#f33b45"><strong> $HOME/.bash_profile </strong></span>


<!-- -->

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="13" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="41"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

- <span style="color:#f33b45"><strong> $HOME/.bash_login</strong></span>

    <br>

    <span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="12" role="region" aria-label=" 图像 小部件"><img alt="" height="79" data-cke-saved-src="https://img-blog.csdnimg.cn/20210115204345500.png" src="https://img-blog.csdnimg.cn/20210115204345500.png" width="579" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210115204345500.png%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22579%22%2C%22height%22%3A%2279%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

    <span style="color:#f33b45"><strong></strong></span>

- <span style="color:#f33b45"><strong> $HOME/.profile</strong></span>


<!-- -->

<span style="color:#3399ea"><strong>应该将需要在</strong></span>

<span style="color:#f33b45"><strong>登录时运行的脚本</strong></span>

<span style="color:#3399ea"><strong>放在上面第一个文件中。</strong></span>

**每次启动一个新shell时，bash shell都会运行.bashrc文件。**可以这样来验证：在主目录下 的.bashrc文件中加入一条简单的echo语句，然后启动一个新shell

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="11" role="region" aria-label=" 图像 小部件"><img alt="" height="437" data-cke-saved-src="https://img-blog.csdnimg.cn/20210115205736300.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210115205736300.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="742" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210115205736300.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22742%22%2C%22height%22%3A%22437%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="10" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="42"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

**.bashrc文件通常也是通过某个bash启动文件来运行的。因为.bashrc文件会运行两次：一次是 当你登入bash shell时，另一次是当你启动一个bash shell时。如果你需要一个脚本在两个时刻都得 以运行，可以把这个脚本放进该文件中。**

# <span style="color:#f33b45">第三部分 高级shell脚本编程</span>



# <span style="color:#f33b45">第17章创建函数</span>



### <span style="color:#3399ea">基本的脚本函数：</span>



<span style="color:#3399ea">创建方式：</span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="9" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="43"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

要在脚本中使用函数，<span style="color:#f33b45"><strong>只需要像其他shell命令一样，在行中指定函数名就行了。&nbsp;</strong></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="8" role="region" aria-label=" 图像 小部件"><img alt="" height="213" data-cke-saved-src="https://img-blog.csdnimg.cn/20210318192234168.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210318192234168.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="544" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210318192234168.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22544%22%2C%22height%22%3A%22213%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="7" role="region" aria-label=" 图像 小部件"><img alt="" height="267" data-cke-saved-src="https://img-blog.csdnimg.cn/20210318192225384.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210318192225384.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="545" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210318192225384.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22545%22%2C%22height%22%3A%22267%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

### 返回值

bash shell会把函数当作一个小型脚本，<span style="color:#f33b45"><strong>运行结束时会返回一个退出状态码</strong></span>

(参见第11章)。 有3种不同的方法来为函数生成退出状态码。

<span style="color:#f33b45"><strong>默认退出状态码：</strong></span>

默认情况下，函数的退出状态码是函数中最后一条命令返回的退出状态码。在函数执行结束 后，<span style="color:#3399ea"><strong>可以用标准变量$?来确定函数的退出状态码</strong></span>

。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="6" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="44"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |
|  |

<span tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_inline cke_widget_image cke_image_nocaption" data-cke-display-name="图像" data-cke-widget-id="5" role="region" aria-label=" 图像 小部件"><img alt="" height="316" data-cke-saved-src="https://img-blog.csdnimg.cn/20210318193229470.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" src="https://img-blog.csdnimg.cn/20210318193229470.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70" width="494" data-cke-widget-data="%7B%22hasCaption%22%3Afalse%2C%22src%22%3A%22https%3A%2F%2Fimg-blog.csdnimg.cn%2F20210318193229470.png%3Fx-oss-process%3Dimage%2Fwatermark%2Ctype_ZmFuZ3poZW5naGVpdGk%2Cshadow_10%2Ctext_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n%2Csize_16%2Ccolor_FFFFFF%2Ct_70%22%2C%22alt%22%3A%22%22%2C%22width%22%3A%22494%22%2C%22height%22%3A%22316%22%2C%22lock%22%3Atrue%2C%22align%22%3A%22none%22%2C%22classes%22%3Anull%7D" data-cke-widget-upcasted="1" data-cke-widget-keep-attr="0" data-widget="image" class="cke_widget_element"><span class="cke_reset cke_widget_drag_handler_container" style="background:rgba(220,220,220,0.5);background-image:url(https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png);display:none;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation" draggable="true"></span><span class="cke_image_resizer" title="点击并拖拽以改变尺寸">​</span></span>

<span style="color:#f33b45"><strong>使用 return 命令：</strong></span>

使用return命令来退出函数并返回特定的退出状态码。return命令允许指定一个 整数值来定义函数的退出状态码，从而提供了一种简单的途径来编程设定函数退出状态码。<span style="color:#f33b45"><strong>退出状态码必须是0~255</strong></span>

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="4" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="45"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

- <span style="color:#3399ea"><strong>如果在用$?变量提取函数返回值之前执行了其他命令，函数的返回值就会丢失。记住，$? 变量会返回执行的最后一条命令的退出状态码</strong></span>

- <span style="color:#3399ea"><strong>由于退出状态码必须小于256，函数的结果必须生成 一个小于256的整数值。任何大于256的值都会产生一个错误值。</strong></span>


<!-- -->

<span style="color:#f33b45"><strong>使用函数输出：&nbsp;</strong></span>

result='dbl' 会将dbl函数的输出赋给$result变量

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="3" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="46"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

### 在函数中使用变量

<span style="color:#f33b45"><strong>向函数传递参数：</strong></span>

函数可以使用标准的参数环境变量来表示命令行上传给函数的参数，<span style="color:#3399ea"><strong>函数名会在$0 变量中定义，函数命令行上的任何参数都会通过$1、$2等定义。也可以用特殊变量$#来判断传 给函数的参数数目</strong></span>

。

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="2" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="47"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

**由于函数使用特殊参数环境变量作为自己的参数值，因此它无法直接获取脚本在命令行中的 参数值**

<span style="color:#f33b45"><strong>在函数中处理变量</strong></span>

：函数使用两种类型的变量：  全局变量  局部变量

- <span style="color:#3399ea"><strong>全局变量这里不多记了，用的基本都是局部变量：变量声明的前面加上local关键字。</strong></span>

- **也可以在变量赋值语句中使用local关键字： local temp=$[ $value + 5 ]，**local关键字保证了变量只局限在该函数中。如果脚本中在该函数之外有同样名字的变量， 那么shell将会保持这两个变量的值是分离的

<!-- -->

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="1" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="48"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

### <span style="color:#f33b45"><strong>数组变量和函数：</strong></span>



向函数传数组参数：**如果试图将该数组变量作为函数参数，函数只会取数组变量的第一个值。**

<div tabindex="-1" contenteditable="false" data-cke-widget-wrapper="1" data-cke-filter="off" class="cke_widget_wrapper cke_widget_block cke_widget_codeSnippet" data-cke-display-name="代码段" data-cke-widget-id="0" role="region" aria-label="代码段 小部件"><precode language="bash" precodenum="49"></precode><span class="cke_reset cke_widget_drag_handler_container" style="background: url(&quot;https://csdnimg.cn/release/blog_editor_html/release1.8.0/ckeditor/plugins/widget/images/handle.png&quot;) rgba(220, 220, 220, 0.5); top: -15px; left: 0px;"><img class="cke_reset cke_widget_drag_handler" data-cke-widget-drag-handler="1" src="data:image/gif;base64,R0lGODlhAQABAPABAP///wAAACH5BAEKAAAALAAAAAABAAEAAAICRAEAOw==" width="15" title="点击并拖拽以移动" height="15" role="presentation"></span></div>

解决办法：**将该数组变量的值分解成单个的值，然后将这些值作为函数参数使 用。在函数内部，可以将所有的参数重新组合成一个新的变量.**

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

<br>

# <span style="color:#f33b45">第18章图形化桌面环境中的脚本程</span>



# <span style="color:#f33b45">第19章初识sed和gawk2</span>



# <span style="color:#f33b45">第20正则表达式</span>



# <span style="color:#f33b45">第21章sed进阶</span>



# <span style="color:#f33b45">第22章gawk进阶"</span>



# <span style="color:#f33b45">第23章使用其他shell</span>



# <span style="color:#f33b45">第四部分创建实用的脚本</span>



# <span style="color:#f33b45">第24章编写简单的脚本实用工具2</span>



# <span style="color:#f33b45">第25章创建与数据库、Web及电子...</span>



# <span style="color:#f33b45">第26章一些小有意思的脚本</span>



# <span style="color:#f33b45">附录A bash命令快速指南</span>



# <span style="color:#f33b45">附录B sed和gawk快速指南</span>



<br>

<br>

### <span style="color:#f33b45">https://pan.baidu.com/s/1872SOUVIVF4E-GwybOyxtw&nbsp;<br>提##：iokd&nbsp;</span>




