---
title: iptables 命令和 iptables.service 服务的区别是上面 ?
tags:
  - Linux
categories:
  - Linux
toc: true
recommend: 1
keywords: iptables
uniqueId: >-
  2022-12-05 19:14:44/What is the difference between the iptables command and
  iptables.service? .html
mathJax: false
date: 2022-12-06 03:14:44
thumbnail:
---

**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

<!-- more -->
## 写在前面
***
+ 关于`iptables` 命令 和 `iptabls.service`  的一些疑惑
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font>

***
我对 `iptables` 命令 和 `iptabls.service`  的一些疑惑,在 `unix.stackexchange.com` 提了相关问题，有小伙伴为我做了解答，下面为问题和答案

我一直以为因为有 `iptables.service` 服务才可以使用 `iptables` 命令，实际上并不是这样的，`iptalbes` 是基于内核的，和 `iptables.service` 没有关系，不用安装任何工具包就可以使用 `iptable` 命令添加的防火墙规则，只是添加的规则是临时的，基于内存的，会在系统重启前消失，所以需要 `iptables.service`,`firewalld.service` 来对添加的规则进行保存。在系统重启后重载对应的防火墙规则，在系统关机时卸载对应的规则。

***

### 问题：

#### What is the difference between the iptables command and iptables.service?

I always thought I could only use the `iptables` command if I had the `iptables.service` service installed, but then I found out I was wrong

```bash
┌──[root@vms16.liruilongs.github.io]-[~]
└─$systemctl status iptables.service
Unit iptables.service could not be found.
┌──[root@vms16.liruilongs.github.io]-[~]
└─$iptables -A INPUT -p icmp --icmp-type 13 -j DROP
┌──[root@vms16.liruilongs.github.io]-[~]
└─$iptables -A OUTPUT -p icmp --icmp-type 14 -j DROP
```

After adding the rule, it took effect immediately


Without the `iptables.service` service, the command can still be used

```bash
┌──[root@vms16.liruilongs.github.io]-[~]
└─$whereis iptables
iptables: /usr/sbin/iptables /usr/libexec/iptables /usr/share/man/man8/iptables.8.gz
┌──[root@vms16.liruilongs.github.io]-[~]
└─$which iptables
/usr/sbin/iptables
```

I'm curious why we need `iptables.service ` and what it means, for simplicity we can use `firewalld.service`.

***

```bash
┌──[root@vms16.liruilongs.github.io]-[~]
└─$yum -y install  iptables-services.x86_64 > /dev/null
┌──[root@vms16.liruilongs.github.io]-[~]
└─$systemctl status iptables.service
● iptables.service - IPv4 firewall with iptables
   Loaded: loaded (/usr/lib/systemd/system/iptables.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
```

I installed it and found only the configuration file

```bash
┌──[root@vms16.liruilongs.github.io]-[~]
└─$rpm -qlc iptables
/etc/sysconfig/ip6tables-config
/etc/sysconfig/iptables-config
```
What else does `iptable.service` do?

Can I assume so?  `iptables` is kernel related and does not require `iptables.service` to be installed to work, but I see a lot of people reloading configuration files and restarting `iptable.service` after changing `iptable` rules, is that the right idea?

***
### 答案：


The `iptables` command is used to add or delete rules and chains and can be used without the service file. What `iptables.service` does is to automatically load a saved ruleset on boot and to unload the rules at shutdown. There's a few safety checks in the scripts, like setting default chain policies to ACCEPT on shutdown, to prevent the system from having unusable rulesets. If you want to manually load rules every time you boot the system you can, the service just makes it easier.

`firewalld` presents a simpler interface to defining rules than `iptables` but that is really the major difference. And underneath the covers, `firewalld` uses `iptables` to implement the rules. Personally, I prefer using `iptables` but I have gotten used to the configuration over the years. The choice of `iptables` or `firewalld` is really up to what you're comfortable with.

***

Thank you very much, it solved my doubts. Am I to understand that the rules configured with iptables are temporary, in-memory behavior (like /proc), not persistent, and if the system is rebooted, the rules are gone unless you export the data before shutting down. iptables.service automatically saves these rules and loads them automatically after a reboot. @doneal24 – 
山河以无恙
 

---


@山河以无恙 Your understanding is correct. Any changes made by the iptables command will disappear when you reboot unless you save them and then reload after the boot. – 
doneal24
 

---


## 博文参考


https://unix.stackexchange.com/questions/727282/what-is-the-difference-between-the-iptables-command-and-iptables-service-or-wha