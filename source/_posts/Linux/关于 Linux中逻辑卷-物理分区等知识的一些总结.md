---
title: 关于 Linux中卷/分区等知识的一些总结
tags: 'Linux'
categories: 'Linux'
toc: true
recommend: 1
keywords: Linux
uniqueId: '2021-07-10 07:44:31/关于 Linux中逻辑卷/物理分区等知识的一些总结.html'
mathJax: false
date: 2021-07-10 15:44:31
thumbnail:
---

**<font color="009688"> 自殺並不是一定就是軟弱，常常倒是一種堅定的抗議，是鮮活可愛的心向生命要求意義的無可奈何的慘烈方式。  ------- 史鐵生《我與地壇》**</font>
<!-- more -->
# 写在前面
***
+ 和小伙伴们分享一些`Linux`中`存储`相关的知识
+ 这部分东西基本用不到，但是要有一个`清晰`的定义
+ 文章内容涉及：
+ `Linux`文件存储，`LVM`相关概念
+ 使用`Linxu`文件系统`格式化分区`,`分区挂载`
+ 调整`逻辑卷`，添加`交换分区`等实战Demo
+ 可以区分以下概念
+ `主分区`，`扩展分区`，`逻辑分区`，`交换分区`，`卷组`，`物理卷`，`逻辑卷`，`VDO卷`等


![](https://img-blog.csdnimg.cn/3baffb6c16dc43cc92b87baad5700594.png)




**<font color="009688"> 自殺並不是一定就是軟弱，常常倒是一種堅定的抗議，是鮮活可愛的心向生命要求意義的無可奈何的慘烈方式。  ------- 史鐵生《我與地壇》**</font>

***
## 一、标准分区文件存储

​​​​​![Linux文件存储](https://img-blog.csdnimg.cn/6f3d4a91cbd9491db1181b550fbd9a58.png)

正常的拿到一块磁盘我们会使用`lsblk`查看分区，之后会使用`fdisk`或者`parted`进行`分区` ，`分区`之后会使用`mkfs`相关`指定文件系统`进行`格式化处理`，最后通过`mount`进行`挂载处理`，挂载到对应的目录，我们就可以正常的使用目录。顺序如下：

1. 识别磁盘(lsblk) 
2. 分区
    + fdisk(MSDOS分区表)
    + gdisk(GPT分区表 >2.2TB磁盘)
    + parted(两种都可以)
3. 格式化(mkfs相关)
4. 挂载(mount) 
5. 访问挂载点

**<font color=amber>具体的流程</font>**
```bash
$lsblk    #查看分区状态
$fdisk /dev/vdc     #使用fdisk分区工具，对/dev/vdc硬盘进行分区
p 查询分区状态 			
q 不保存退出
n 新建分区
Select (default p):      #回车，默认是主分区
Partition number (1-4, default 1):     #第一个分区默认序号是1
First sector ：    #起始扇区，直接回车
Last sector       +500M      #创建500M分区
w    #保存退出
$partprobe /dev/vdb 或者 partx -a /dev/vdb  #刷新硬盘分区表：
$mkfs.xfs /dev/vdc1     #使用xfs文件系统为vdc1这个分区格式化
$mount   /dev/vdc1   /mnt    #将新分区挂载到/mnt目录
$df -h   #h查看挂载信息
$umount   /dev/vdc1   #卸载分区

```
下面为我机器的存储分布，可以看到只有一块磁盘，有两个分区，一个普通的分区，挂载到了根目录，一个交换分区
```bash
┌──[root@liruilongs.github.io]-[~]
└─$ lsblk
NAME   MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda      8:0    0  200G  0 disk
├─sda1   8:1    0  150G  0 part /
└─sda2   8:2    0   10G  0 part [SWAP]
┌──[root@liruilongs.github.io]-[~]
└─$
```
## 二、使用LVM的文件存储

我们用上面标准分区在硬盘上创建了文件系统，为已有文件系统添加额外的空间多少是一种痛苦的体验。我们只能在同一个物理硬盘的可用空间范围内调整分区大小。

使用了`Linux 逻辑卷管理器(logical volume manager，LVM)`它可以让你在无需重建整个文件系统的情况下，轻松地管理磁盘空间。


### 什么是LVM

通过`LVM`将 `多个物理卷(物理分区)`集中在一起可以形成一个`卷组(volume group，VG)`。`逻辑卷管理系统`将`卷组`视 为`一个物理硬盘`，但事实上`卷组`可能是由`分布在多个物理硬盘上的多个物理分区组成的`。`卷组`提供了一个`创建逻辑分区的平台`，而这些`逻辑分区`则`包含了文件系统`。



![](https://img-blog.csdnimg.cn/1ae776b79da644ada5ce2c07426cbc89.png)

整个结构中的最后一层是`逻辑卷(logical volume，LV)`。`逻辑卷`为Linux提供了创建`文件系统`的`分区环境`，作用类似于到目前为止我们一直在探讨的Linux中的`物理硬盘分区`。Linux系统将`逻辑卷`视为`物理分区`。 可以使用任意一种`标准Linux文件系统`来`格式化逻辑卷`，然后再将它加入`Linux虚拟目`录中的某个挂载点。

LVM逻辑卷管理机制的思想:`化零(物理卷PV)为整(卷组VG)`、`动态扩容伸缩`、`按需(逻辑卷LV)分配`。把零散的分区(PV物理设备(物理卷)),整编为大卷组(VG虚拟磁盘)，然后根据需要获取空间(虚拟分区LV)

### 物理卷、逻辑卷、卷组、快照卷之间的联系
+ **物理卷(Physical Volume,PV)**：就是指硬盘分区，也可以是整个硬盘或已创建的软RAID，是LVM的基本存储设备。
+ **卷组(Volume Group,VG)**：是由一个或多个物理卷所组成的存储池，在卷组上能创建一个或多个逻辑卷。
+ **逻辑卷(Logical Volume,LV)**：类似于非LVM系统中的硬盘分区，它建立在卷组之上，是一个标准的块设备，在逻辑卷之上可以建立文件系统。
+ **快照** 最初的Linux LVM 中允许你在逻辑卷在线的状态下将其复制到另一个设备,传统的备份方法在将文件复制到备份媒体上时通常要将文件锁定。快照允许你在复制的同时，保证运行关键任务的。

**<font color=red>可以做这样一个设想来理解以上三者的关系：如果把`PV`比作地球的一个板块，`VG`则是一个地球，因为地球是由多个板块组成的，那么在地球上划分一个区域并标记为亚洲，则亚洲就相当于一个`LV`。</font>**



>**<font color=amber>相互联系：</font>** 在创建卷组时一定要为逻辑卷进行快照预留出空间，而后快照访问逻辑卷的另一个入口，只要把物理卷加到卷组之后，这个物理卷所提供的物理空间事先就被划分好一个个块，而这个块在没格式化之前叫做PE(Physical Extend)【物理盘区】，是逻辑存储的一个小匣子，卷组的大小是由多个PE组成，而逻辑卷的大小是把卷组中的PE放到逻辑卷中，此时，PE不再叫做PE，而是叫做LE(Logical Extend)【逻辑盘区】，其实，逻辑卷中的LE也叫做PE，只是站在角度不同而已。

如果某个物理卷损坏后，存储在逻辑卷中的LE也就会损坏，想让数据不损坏，可以把物理卷中PE做成镜像.镜像是一个实时更新的逻辑卷的完整副本。当你创 建镜像逻辑卷时，LVM会将原始逻辑卷同步到镜像副本中

### LVM管理工具
+ 物理卷操作(不常用)：pvscan、pvdisplay、pvcreate
+ 卷组操作：vgscan(扫描)、vgdisplay(显示)、vgcreate、vgremove、vgextend(扩容)
+ 逻辑卷操作：lvscan、lvdisplay、lvcreate、lvremove、lvextend (PE：分配逻辑卷空间的时候，最小的单位,默认为4M)

#### 创建卷组：

创建逻辑卷之前先要创建卷组，然后从卷组中划分空间给逻辑卷，语法：
```bash
$vgcreate  [-s  PE大小]  卷组名  分区.. ..
```
demo
```bash
$vgcreate /dev/myvg /dev/vdb2     #正常创建卷组，默认扩展单元是4M
$vgdisplay  /dev/myvg   #可以在PE一栏中看到扩张单元的大小
$vgremove /dev/myvg   #删除卷组myvg
$vgcreate -s 16MiB /dev/myvg /dev/vdb2    #创建扩展单元为16MiB的卷组(目前练习题要求)，之后可以用vgdisplay查看PE的大小

```
#### 扩展卷组
```bash
  vgextend  卷组名  分区.. ..
```
#### 创建逻辑卷：
语法
```bash
$lvcreate  -L  大小  -n  名称   卷组名
$lvcreate  -l  PE个数  -n  名称   卷组名
```
demo
```bash
$lvcreate -n mylv -L 800M /dev/myvg    #正常创建
$lvcreate -n mylv -l 50 /dev/myvg    #按照扩展单元的数量创建之后可以用vgs查看
$vgs     #查看逻辑
```
#### 扩展逻辑卷：
```bash
$lvextend -L 300M /dev/test/vo    #将名字叫vo的逻辑卷扩容到300M

$blkid /dev/test/vo    #查看vo逻辑卷的文件系统
$lsblk    #再查看该逻辑卷的挂载点
$xfs_growfs /vo    #刷新大小，如果是xfs的文件系统
$resize2fs  /vo     #刷新大小，如果是ext的文件系统
```
#### 逻辑卷的设备位置
```bash
/dev/卷组名/逻辑卷名
或者
/dev/mapper/卷组名-逻辑卷名
```
#### 扫描磁盘LVM信息
```bash
$vgscan 
  Reading all physical volumes.  This may take a while...
  Found volume group "myvg" using metadata type lvm2
  Found volume group "test" using metadata type lvm2
  Found volume group "rhel" using metadata type lvm2
$pvscan
  PV /dev/vdb2   VG myvg            lvm2 [1008.00 MiB / 208.00 MiB free]
  PV /dev/vdb1   VG test            lvm2 [<2.00 GiB / <1.67 GiB free]
  PV /dev/vda2   VG rhel            lvm2 [<29.00 GiB / 0    free]
  Total: 3 [<31.98 GiB] / in use: 3 [<31.98 GiB] / in no VG: 0 [0   ]
$lvscan 
  ACTIVE            '/dev/myvg/mylv' [800.00 MiB] inherit
  ACTIVE            '/dev/test/vo' [336.00 MiB] inherit
  ACTIVE            '/dev/rhel/swap' [<2.17 GiB] inherit
  ACTIVE            '/dev/rhel/root' [<26.83 GiB] inherit
$lsblk 
NAME          MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sr0            11:0    1 1024M  0 rom  
vda           252:0    0   30G  0 disk 
├─vda1        252:1    0    1G  0 part /boot
└─vda2        252:2    0   29G  0 part 
  ├─rhel-root 253:0    0 26.8G  0 lvm  /
  └─rhel-swap 253:1    0  2.2G  0 lvm  [SWAP]
vdb           252:16   0   10G  0 disk 
├─vdb1        252:17   0    2G  0 part 
│ └─test-vo   253:2    0  336M  0 lvm  /vo
└─vdb2        252:18   0    1G  0 part 
  └─myvg-mylv 253:3    0  800M  0 lvm  
vdc           252:32   0   10G  0 disk 
└─vdc1        252:33   0  500M  0 part /mnt
$

```
## 三、VDO卷

### 什么是VDO卷
虚拟数据优化器，可以对数据进行重删过滤：

`VDO(Virtual Data Optimize)`是`RHEL8/Centos8`上新推出的一个存储相关技术(最早在7.5测试版中开始测试)，是Redhat收购的Permabit公司的技术。

`VDO`的主要作用是节省磁盘空间，比如让1T的磁盘能装下1.5T的数据，从而降低数据中心的成本。

**那vdo是如何实现**：关键原理主要是重删和压缩，重删就是硬盘里拷贝来相同的数据，以前要占多份空间，现在只需要1份空间就可以了。类似我们在百度网盘中上传一个大型软件安装包，能实现秒传，其实是之前就有，所以无需再传一遍，也无需再占百度一份空间。另一方面是数据压缩，类似于压缩软件的算法，也可以更加节省磁盘空间。

`VDO是一个内核模块`，目的是通过重删减少磁盘的空间占用，以及减少复制带宽，VDO是基于块设备层之上的，也就是在原设备基础上映射出mapper虚拟设备，然后直接使用即可，功能的实现主要基于以下技术:

**零区块的排除** ：在初始化阶段，整块为0的会被元数据记录下来，这个可以用水杯里面的水和沙子混合的例子来解释，使用滤纸(零块排除)，把沙子(非零空间)给过滤出来，然后就是下一个阶段的处理。

**重复数据删除** ：在第二阶段，对于输入的数据会判断是不是冗余数据(在写入之前就判断)，这个部分的数据通过UDS内核模块来判断(Universal Deduplication Service)，被判断为重复数据的部分不会被写入，然后对元数据进行更新，直接指向原始已经存储的数据块即可。

**压缩**：一旦消零和重删完成，LZ4压缩会对每个单独的数据块进行处理，然后压缩好的数据块会以固定大小4KB的数据块存储在介质上，由于一个物理块可以包含很多的压缩块，这个也可以加速读取的性能。

### vdo基本操作：
+ vdo  create  --name=VDO卷名称 --device=设备路径 --vdoLogicalSize=逻辑大小
+ vdo  list
+ vdo  status  -n VDO卷名称
+ vdostats  [--human-readable]  [/dev/mapper/VDO卷名称]
+ vdo  remove  -n VDO卷名称


### 创建VDO卷
```bash
$yum  -y  install  vdo     #装包
$systemctl restart vdo   #启动服务，并设置开机自启
# 找到vdc磁盘(此盘之前不能有配置)
$man  vdo   #搜索 /example ，在例子中查找相关配置命令
$vdo create --name=vdo0 --device=/dev/sdb1 --vdoLogicalSize=10T    #name是vdo卷的名称，device是使用哪个物理磁盘，最后是定义虚拟磁盘大小
 
$mkfs.xfs  -K   /dev/mapper/myvdo    #格式化 `-K可以加快速度`
#设置/etc/fstab文件，并且使用mount  -a检测
/dev/mapper/myvdo /vblock xfs _netdev 0 0      #	`_netdev的作用是等待网络相关程序启动后再执行挂载任务，通常网络启动之后vdo服务也就起来了，就可以挂载了`
```

## 四、交换分区

### 什么是交换分区

`相当于win的虚拟内存，在物理内存不足时借用硬盘空间,硬盘中预先划分一定的空间`，然后将把内存中暂时不常用的数据临时存放到硬盘中，以便腾出物理内存空间让更活跃的程序服务来使用的技术，其设计目的是为了解决真实物理内存不足的问题。

但由于交换分区毕竟是通过硬盘设备读写数据的， 速度肯定要比物理内存慢，所以只有当真实的物理内存耗尽后才会调用交换分区的资源.

**交换分区：虚拟内存，一般设置成物理内存的1~2倍，<16G**

### 格式化交换分区
```bash
$swapon -s    #查询交换分区状态，通常有可能会存在个默认的交换分区，不能删除
# 先用fdisk /dev/vdb  再创建一个512M的分区比如创建了vdb3
$mkswap  /dev/vdb3    #格式化交换分区
$vim /etc/fstab   ##修改开机挂载磁盘的文件
/dev/vdb3   swap     swap    defaults        0 0
$swapon  -a  #测试上述文件是否修改正确
$swapon -s  #再次查看会多出swap分区
```
### 启用/停止/查看交换分区使用情况：
```bash
$swapon   设备路径
$swapoff   设备路径
$swapon  -s
$free
```

## 五、实战

### 调整逻辑卷大小：

将逻辑卷 vo 及其文件系统大小调整到 300MiB。确保文件系统内容保持不变。
```bash
$lvscan #找出要扩展的逻辑卷 
$lvextend -L 300MiB /dev/test/vo #扩展逻辑卷 
$blkid /dev/test/vo #检查文件系统格式 
$xfs_growfs 逻辑卷对应的挂载点 #适用于 XFS 文件系统 或者 
$resize2fs 逻辑卷对应的挂载点 #适用于 EXT2/3/4 文件系统
```
### 添加交换分区：
```bash
$fdisk /dev/vdb #修改磁盘 vdb
.. ..
Command (m for help): n #添加新分区
Partition number (2-128, default 2): #直接回车(默认)
First sector (4194304-20971486, default 4194304): #直接回车(默认)
Last sector, *sectors or +size{K,M,G,T,P} (4194304-20971486,default 20971486): +512M
Created a new partition 2 of type 'Linux filesystem' and of size 512 MiB.
Command (m for help): w #保存分区表，并退出
The partition table has been altered.
Syncing disks.
$partprobe /dev/vdb #刷新分区表
$mkswap /dev/vdb2 #格式化自建分区 vdb2
$vim /etc/fstab
/dev/vdb2 swap swap defaults 0 0
$swapon -a #启用 fstab 中的交换设备
$swapon -s #查看交换分区信息
```
### 创建卷组逻辑卷：
根据以下要求，创建新的逻辑卷：

+ 逻辑卷的名字为 mylv，属于 myvg 卷组，大小为 50 个扩展单元
+ 卷组 myvg 中的逻辑卷的扩展块大小应当为 16MiB
+ 使用 vfat 文件系统将逻辑卷 mylv 格式化
+ 此逻辑卷应当在系统启动时自动挂载到/mnt/mydata 目录下

```bash
$fdisk /dev/vdb $修改磁盘 vdb
.. ..
Command (m for help): n #添加新分区
Partition number (3-128, default 3): #直接回车(默认)
First sector (5242880-20971486, default 5242880): #直接回车(默认)
Last sector, *sectors or +size{K,M,G,T,P} (5242880-20971486,default 20971486): +1000M
Created a new partition 3 of type 'Linux filesystem' and of size 1000 MiB.
Command (m for help): w #保存分区表，并退出
The partition table has been altered.
Syncing disks.
$partprobe /dev/vdb #刷新分区表
$vgcreate -s 16MiB myvg /dev/vdb3 #建卷组(使用分区 vdb3)
$lvcreate -l 50 -n mylv myvg #建逻辑卷
$mkfs.vfat /dev/myvg/mylv #格式化
$mkdir /mnt/mydata #创建挂载点目录
$vim /etc/fstab #设置开机挂载
/dev/myvg/mylv /mnt/mydata vfat defaults 0 0
$mount -a #启用&测试开机挂载
```
### 使用vdo创建 VDO 卷

根据如下要求，创建新的 VDO 卷：

+ 使用未分区的磁盘(/dev/vdc)
+ 此 VDO 卷的名称为 myvdo
+ 此 VDO 卷的逻辑大小为 50G
+ 此 VDO 卷使用 xfs 文件系统格式化
+ 此 VDO 卷在系统启动时自动挂载到/vblock 目录下

```bash
$yum install vdo #装包
$systemctl enable --now vdo #起服务
$vdo create --name=myvdo --device=/dev/vdc --vdoLogicalSize=50G
 #新建 VDO 卷
$mkfs.xfs -K /dev/mapper/myvdo #格式化
#或者 mkfs.ext4 -E nodiscard ...
$mkdir /vblock #创建挂载点目录
$vim /etc/fstab
/dev/mapper/myvdo /vblock xfs _netdev 0 0
$mount -a #启用&测试开机挂载
```
### 整块磁盘不分区添加卷组给逻辑卷扩容
```
[root@192 test]# lsblk
NAME                                                                                        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
fd0                                                                                           2:0    1    4K  0 disk
sr0                                                                                          11:0    1 1024M  0 rom
vda                                                                                         252:0    0   60G  0 disk
├─vda1                                                                                      252:1    0    1G  0 part /boot
└─vda2                                                                                      252:2    0   59G  0 part
  ├─centos-root                                                                             253:0    0 35.6G  0 lvm  /
  ├─centos-swap                                                                             253:1    0    6G  0 lvm  [SWAP]
  └─centos-home                                                                             253:5    0 17.4G  0 lvm  /home
vdb                                                                                         252:16   0  200G  0 disk
├─vgdata-thinpool_tmeta                                                                     253:2    0    1G  0 lvm
│ └─vgdata-thinpool                                                                         253:4    0  100G  0 lvm
│   ├─docker-253:0-1692282-344d108b922046afa6ee291c2131278fc1ef61d6158978adeaefbfaae8f5441a 253:8    0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-677abca6a7a59352105c639c35d409751c66e748929cfa41375c60fa47f66cef 253:9    0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-6ffeeb0769f037daaa0cd922fb9affbbbb9aae2173e263c77f0470ad4cf434ea 253:10   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-1fc2a72136de7b4445bc995546942b276c2d3704986ba8194dbdb2502ec353f6 253:11   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-0972ab9e861a9b6156434e235326c7a840eb16ca2192dad1b2c44d455014799c 253:12   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-e092d7e3fb740b79656e688deb90e43effc7a349f05b642b9b1999ae2ca2207f 253:13   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-37a80f16a9748f28a150db0d9a1f39f680dd709d0fbfe2a95fae6f2b3498d722 253:14   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-6b8d1b126f81574efa238f663975f5f1af6f3b341867d07853538bb2dd13251d 253:15   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-c1c43b542d41348d5bdbf9710e2354c7c158d5d3c111aea4745bd8cfc44c3d4d 253:16   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-6ff93101f85f43f3a5846127377b3b0ac706d451547cc018cf48e863ff54f9c2 253:17   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-d9e8294c1ab6857986deebcf7a5a5c062fb634ab32a8e3f6be574430e38daae6 253:18   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-3f5170d166049027571184698c2c9ff24ef9f422431737a70958ba3512d74c7a 253:19   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-2c8679b21e20630cd78e4d3e1c0ba75e47d2bd5e6039196d0d00c544696816e5 253:20   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-cb04dbc661d056738bddf26a5795933c5bf51669274dc8289fe82083f9f00914 253:21   0   10G  0 dm   /var/lib/doc
│   └─docker-253:0-1692282-5efd249cde2594bf456eb60e3711d28c71ffdc56fbe88917c3b5c55aee631271 253:22   0   10G  0 dm   /var/lib/doc
├─vgdata-thinpool_tdata                                                                     253:3    0  100G  0 lvm
│ └─vgdata-thinpool                                                                         253:4    0  100G  0 lvm
│   ├─docker-253:0-1692282-344d108b922046afa6ee291c2131278fc1ef61d6158978adeaefbfaae8f5441a 253:8    0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-677abca6a7a59352105c639c35d409751c66e748929cfa41375c60fa47f66cef 253:9    0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-6ffeeb0769f037daaa0cd922fb9affbbbb9aae2173e263c77f0470ad4cf434ea 253:10   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-1fc2a72136de7b4445bc995546942b276c2d3704986ba8194dbdb2502ec353f6 253:11   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-0972ab9e861a9b6156434e235326c7a840eb16ca2192dad1b2c44d455014799c 253:12   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-e092d7e3fb740b79656e688deb90e43effc7a349f05b642b9b1999ae2ca2207f 253:13   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-37a80f16a9748f28a150db0d9a1f39f680dd709d0fbfe2a95fae6f2b3498d722 253:14   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-6b8d1b126f81574efa238f663975f5f1af6f3b341867d07853538bb2dd13251d 253:15   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-c1c43b542d41348d5bdbf9710e2354c7c158d5d3c111aea4745bd8cfc44c3d4d 253:16   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-6ff93101f85f43f3a5846127377b3b0ac706d451547cc018cf48e863ff54f9c2 253:17   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-d9e8294c1ab6857986deebcf7a5a5c062fb634ab32a8e3f6be574430e38daae6 253:18   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-3f5170d166049027571184698c2c9ff24ef9f422431737a70958ba3512d74c7a 253:19   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-2c8679b21e20630cd78e4d3e1c0ba75e47d2bd5e6039196d0d00c544696816e5 253:20   0   10G  0 dm   /var/lib/doc
│   ├─docker-253:0-1692282-cb04dbc661d056738bddf26a5795933c5bf51669274dc8289fe82083f9f00914 253:21   0   10G  0 dm   /var/lib/doc
│   └─docker-253:0-1692282-5efd249cde2594bf456eb60e3711d28c71ffdc56fbe88917c3b5c55aee631271 253:22   0   10G  0 dm   /var/lib/doc
├─vgdata-lvzpaas                                                                            253:6    0   40G  0 lvm  /zpaas
└─vgdata-lvzpaasssd                                                                         253:7    0   30G  0 lvm  /zpaasssd
vdc                                                                                         252:32   0  200G  0 disk
[root@192 test]#
```
vgdata-lvzpaas是一个镜像仓库的挂载，但是40G，不够用了，希望扩容到80G

所用我们用vdc这块磁盘来扩容。
```bash
[root@192 test]# vgscan
  Reading volume groups from cache.
  Found volume group "vgdata" using metadata type lvm2
  Found volume group "centos" using metadata type lvm2
[root@192 test]# pvscan
  PV /dev/vdb    VG vgdata          lvm2 [<200.00 GiB / <28.00 GiB free]
  PV /dev/vda2   VG centos          lvm2 [<59.00 GiB / 0    free]
  Total: 2 [258.99 GiB] / in use: 2 [258.99 GiB] / in no VG: 0 [0   ]
[root@192 test]# lvscan
  ACTIVE            '/dev/vgdata/thinpool' [100.00 GiB] inherit
  ACTIVE            '/dev/vgdata/lvzpaas' [40.00 GiB] inherit
  ACTIVE            '/dev/vgdata/lvzpaasssd' [30.00 GiB] inherit
  ACTIVE            '/dev/centos/swap' [6.00 GiB] inherit
  ACTIVE            '/dev/centos/home' [<17.39 GiB] inherit
  ACTIVE            '/dev/centos/root' [<35.61 GiB] inherit
[root@192 test]#
```
LVM 扫描，我们可以看到，当前有2个卷组，有两个物理卷用于LVM

把vdc添加卷组vgdata
```bash
[root@192 test]# vgextend  vgdata /dev/vdc
  Physical volume "/dev/vdc" successfully created.
  Volume group "vgdata" successfully extended
```
查看详细信息，发现vgdata多了一个PV
```bash
[root@192 test]# vgs
  VG     #PV #LV #SN Attr   VSize   VFree
  centos   1   3   0 wz--n- <59.00g      0
  vgdata   2   3   0 wz--n- 399.99g 187.99g
[root@192 test]#
```
对逻辑卷`/dev/vgdata/lvzpaas`进行扩容,扩容为80G
```bash
[root@192 test]# lvextend  -L 80GiB /dev/vgdata/lvzpaas
  Size of logical volume vgdata/lvzpaas changed from 40.00 GiB (10240 extents) to 80.00 GiB (20480 extents).
  Logical volume vgdata/lvzpaas successfully resized.
```
查看LV,发现以扩容到80G
```bash
[root@192 test]# lvscan
  ACTIVE            '/dev/vgdata/thinpool' [100.00 GiB] inherit
  ACTIVE            '/dev/vgdata/lvzpaas' [80.00 GiB] inherit
  ACTIVE            '/dev/vgdata/lvzpaasssd' [30.00 GiB] inherit
  ACTIVE            '/dev/centos/swap' [6.00 GiB] inherit
  ACTIVE            '/dev/centos/home' [<17.39 GiB] inherit
  ACTIVE            '/dev/centos/root' [<35.61 GiB] inherit
```
查看当前扩容卷组的文件系统
```bash
[root@192 test]# blkid /dev/vgdata/lvzpaas
/dev/vgdata/lvzpaas: UUID="a6e13e77-23fa-4b4e-8b8a-2d666631476d" TYPE="xfs"
```
刷新大小
```bash
[root@192 test]# xfs_growfs /zpaas
meta-data=/dev/mapper/vgdata-lvzpaas isize=512    agcount=4, agsize=2621440 blks
         =                       sectsz=512   attr=2, projid32bit=1
         =                       crc=1        finobt=0 spinodes=0
data     =                       bsize=4096   blocks=10485760, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0 ftype=1
log      =internal               bsize=4096   blocks=5120, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
data blocks changed from 10485760 to 20971520
```
查看扩容结果
```bash
[root@192 test]#  df -h | grep zpaas
/dev/mapper/vgdata-lvzpaasssd   30G   33M   30G    1% /zpaasssd
/dev/mapper/vgdata-lvzpaas      80G   14G   67G   17% /zpaas
[root@192 test]# lsblk  | grep /zpaas
├─vgdata-lvzpaas                                                                            253:6    0   80G  0 lvm  /zpaas
└─vgdata-lvzpaasssd                                                                         253:7    0   30G  0 lvm  /zpaasssd
└─vgdata-lvzpaas                                                                            253:6    0   80G  0 lvm  /zpaas
[root@192 test]#
```
