---
title: 华为云：基于AppCube零代码开发微信小程序【玩转华为云】
tags:
  - AppCube
categories:
  - AppCube
toc: true
recommend: 1
keywords: AppCube
uniqueId: '2023-02-14 08:31:33/华为云：基于AppCube零代码开发微信小程序.html'
mathJax: false
date: 2023-02-14 16:31:33
thumbnail:
---

**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

<!-- more -->
## 写在前面

***
+ 分享一个华为云开发微信小程序产品 `AppCube`
+ 通过 `AppCube` 可以基于零代码或者低代码开发微信小程序
+ 免费版支持试用，可以一键发布分享。
+ 免费版支持最多9个用户使用。如果是小团队，赶快来褥羊毛吧
+ 博文内容为：免费版试用教程，包括搭建小程序，微信扫扫描填写信息
+ 理解不足小伙伴帮忙指正


**<font color="009688"> 对每个人而言，真正的职责只有一个：找到自我。然后在心中坚守其一生，全心全意，永不停息。所有其它的路都是不完整的，是人的逃避方式，是对大众理想的懦弱回归，是随波逐流，是对内心的恐惧 ——赫尔曼·黑塞《德米安》**</font>

***

通下面的链接进入可以直接申请免费试用

https://console.huaweicloud.com/appcube/?region=cn-north-4#/home

![在这里插入图片描述](https://img-blog.csdnimg.cn/c522e6a22d1245cea1a51c4d38ef4bdc.png)

### 资源申请

选择AppCube免费版，点击页面右上角“免费试用”，完成购买。

![在这里插入图片描述](https://img-blog.csdnimg.cn/7c6846d1043346fb9d89e4a3e5c317f4.png)


申请完点击进入首页

![在这里插入图片描述](https://img-blog.csdnimg.cn/22a0c9ec491b44dab657e6eda36c372c.png)

### 配置业务用户信息

首先需要配置业务用户信息

免费版提供额外9名业务用户名额，进入配置页面，点击页面左上角，选择“业务配置中心”

![在这里插入图片描述](https://img-blog.csdnimg.cn/9ab485c3f0834a6f9c6be4f238817826.png)

### 构建小程序

配置完选择构建小程序方式

![在这里插入图片描述](https://img-blog.csdnimg.cn/a134076b82b94cd097d01e0ab1dee090.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/f0fc0827948b40daba12311ba8607309.png)

这里我们选择零代码构建，选择需要构建的模板

![在这里插入图片描述](https://img-blog.csdnimg.cn/2dba3624e320432aa8016975bfa74f35.png)

点击安装模板，需要填写命名空间信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/31f85f8f0d3c4d1b810e7bd205c33508.png)

可以直接使用当前的模板，也可以在当前模板做一些编辑信息。
![在这里插入图片描述](https://img-blog.csdnimg.cn/fe106561f9504829bdfa01bce1a7b3c8.png)

下面我们简单修改，添加一个会议位置选择框，并且添加默认值

![在这里插入图片描述](https://img-blog.csdnimg.cn/ee8e3d3b17c841c8b29afd47e145975b.png)

修改完切记要保存

![在这里插入图片描述](https://img-blog.csdnimg.cn/726e40fa50604608bb6e0fd2e6292f68.png)

可以预览查看修改信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/0f7d8b5649624e90b78b04fc9f85a3a4.png)

把需要用户填写的页面分享出去
![在这里插入图片描述](https://img-blog.csdnimg.cn/4bc6bb9e11e04e8589656da12914fc22.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/a8c7bedd6f36480cb7ea03e0d0ef5c60.png)
通过微信烧卖可以直接填写相关采集数据
![在这里插入图片描述](https://img-blog.csdnimg.cn/d0ad07f2bf0c45158e9b8baf4fae291e.png)

### 测试采集

手机扫描填写相关信息

![在这里插入图片描述](https://img-blog.csdnimg.cn/41bf9cb826104916a828f4d0a2dd31ab.png)

![在这里插入图片描述](https://img-blog.csdnimg.cn/6b27adfc21754fec8133417d95dd5247.png)


填写完成，通过后天可以看下详细信息
![在这里插入图片描述](https://img-blog.csdnimg.cn/4fc878fb50cd4ac1b93d58a33ec70fa0.png)







## 博文部分内容参考

文中涉及参考链接内容版权归原作者所有，如有侵权请告知

***

https://bbs.huaweicloud.com/blogs/366795

***

© 2018-2023 liruilonger@gmail.com, All rights reserved. 保持署名-非商用-相同方式共享(CC BY-NC-SA 4.0)
