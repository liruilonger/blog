---
title: 华为HCIP R&S 考试笔记
tags:
  - 网络
  - HCIP
  - 华为
categories:
  - HCIP
toc: true
recommend: 1
keywords: HCIP
uniqueId: '2021-08-18 11:59:01/华为HCIP R&S 考试笔记.html'
mathJax: false
date: 2021-08-18 19:59:01
thumbnail:
---
> 摘要
首页显示摘要内容(替换成自己的)
<!-- more -->
## 写在前面
***


。
**<font color="009688"> 有些人认为,人应该充满境界高尚的思想,去掉格调地低下的思想。这种说法听上去美妙,却是我感到莫大的恐慌。因为高尚的思想和低下的思想的总和就是我自己--------王小波**</font>

 ***

## 1. ТСР/IР基础知识.
### 1.1 传输介质简介有些人认为,人应该充满境界高尚的思想,去掉格调地低下的思想。这种说法听上去美妙,却是我感到莫大的恐慌。因为高尚的思想和低下的思想的总和就是我自己
通信网络除了包含通信设备本身之外,还包含连接这些设备的`传输介质`,如`同轴电缆`、`双绞线`和`光纤`等。不同的传输介质具有不同的特性,这些特性直接影响到通信的诸多方面,如线路编码方式、传输速度和传输距离等。

+ 两个终端,用一条能承载数据传输的物理介质(也称为传输介质)连接起来,就组成了一个最简单的网络。

|**<font color=royalblue>同轴电缆</font>**|**<font color=green>双绞线</font>**|**<font color=orange>光纤</font>**|**<font color=orange>串口电缆</font>**|
|--|--|--|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/1d082842df574806bfb812f7d683ddb2.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![在这里插入图片描述](https://img-blog.csdnimg.cn/90fe806767a84bd7bec25942a774b6d1.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![在这里插入图片描述](https://img-blog.csdnimg.cn/0e1d32849d8043f1ba36ee13539bdcd5.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|![在这里插入图片描述](https://img-blog.csdnimg.cn/effa26e4252f43fd981e139006648dc5.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|
|`同轴电缆`是一种早期使用的传输介质,同轴电缆的标准分为两种, `10BASE2`和`10BASE5`这两种标准都支持`10Mbps`的传输速率,最长传输距离分别为`185米和500米`。一般情况下`10Base2`同轴电缆使用`BNC接头`, `10Base5`同轴电缆使用`N型接头`。10BASE5和10BASE2是早期的两种以太网标准,它们均采用同轴电缆作为传输介质。10BASE5和10BASE2所使用的同轴电缆的直径分别为9.5mm和5mm ,所以前者又称为粗缆,后者又称为细缆。现在, 10Mbps的传输速率早已不能满足目前企业网络需求,`因此同轴电缆在目前企业网络中很少应用。` |与同轴电缆相比`双绞线(Twisted Pair )`具有更低的制造和部署成本,因此在企业网络中被广泛应用。双绞线可分为`屏蔽双绞线`(Shielded Twisted Pair , STP)和`非屏蔽双绞线`(Unshielded Twisted Pair, UTP),`屏蔽双绞线在双绞线与外层绝缘封套之间有一个金属屏蔽层`,可以`屏蔽电磁干扰`。双绞线有很多种类型,不同类型的双绞线所支持的传输速率一般也不相同。例如, `3类双绞线支持10Mbps传输速率;5类双绞线支持100Mbps传输速率;超5类双绞线及更高级别的双绞线支持干兆以太网传输。`双绞线使用R-45接头连接网络设备。为保证终端能够正确收发数据, `RJ-45接头中的针脚必须按照一定的线序排列`。 |双绞线和同轴电缆传输数据时使用的是电信号,而光纤传输数据时使用的是`光信号`。光纤支持的传输速率包括`10Mbps , 100Mbps , 1Gbps , 10Gbps` ,甚至更高。根据光纤传输光信号模式的不同,光纤又可分为`单模光纤和多模光纤`。`单模光纤只能传输一种模式的光不存在模间色散,因此适用于长距离高速传输`。多模光纤允许不同模式的光在一根光纤上传输,由于模间色散较大而导致信号脉冲展宽严重,`因此多模光纤主要用于局域网中的短距离传输`。光纤连接器种类很多,常用的连接器包括`ST , FC, SC , LC`连接器。 |网络通信中常常会用到各种各样的`串口电缆`。常用的串口电缆标准为`RS-232` ,同时也是推荐的标准。但是RS-232的传输速率有限,传输距离仅为6米。其他的串口电缆标准可以支持更长的传输距离,例如RS-422和RS-485的传输距离可达1200米.RS-422和RS-485串"口电缆通常使用V.35接头,这种接头在上世纪80年代已经淘汰,但是现在仍在`帧中继、ATM等传统网络上使用`。V.24是RS-232标准的欧洲版。RS-232本身没有定义接头标准常用的接头类型为DB-9和DB-25,现在, RS-232已逐渐被FireWire, USB等新标准取代,新产品和新设备已普遍使用USB标准。|

#### 冲突域
||
|--|
| ![123131](https://img-blog.csdnimg.cn/1888693d34104c059a401a3d4e54176f.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70) |
|如图是一个`10BASE5`以太网,每个主机都是用同一根同轴电缆来与其它主机进行通信,因此,这里的`同轴电缆`又被称为`共享介质`,相应的网络被称为`共享介质网络`,或简称为`共享式网络`。共享式网络中,不同的主机同时发送数据时,就会产生 **<font color=brown>信号冲突</font>** 的问题,解决这问题的方法一般是采用`载波侦听多路访问/冲突检测技术( Carrier Sense MultipleAccess/Collision Detection )`.|

`CSMA/CD`的基本工作过程如下:
+ 终端设备不停地检测共享线路的状态。如果线路空闲,则可以发送数据;
+ 如果线路不空闲则等待一段时间后继续检测(延时时间由退避算法决定)。
+ 如果有另外一个设备同时发送数据,两个设备发送的数据会产生冲突。终端设备检测到冲突之后,会马上停止发送自己的数据,并发送特殊阻塞信息,以强化冲突信号,使线路上其他站点能够尽早检测到冲突。终端设备检测到冲突后,等待一段时间之后再进行数据发送(延时时间由退避算法决定)

+ CSMA/CD的工作原理可简单总结为:先听后发,边发边听,冲突停发,随机延迟后重发`。

#### 双工模式

||
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/9c79fcb3941c4a80b1bc7b31170d62d8.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|
| **<font color=blue>半双工</font>** :在半双工模式( half-duplex mode )下,通信双方都能发送和接收数据,但不能同时进行。当一台设备发送时,另一台只能接收,反之亦然。对讲机是半双工的典型例子。|
| **<font color=brown>全双工</font>** :在全双工模式(full-duplex mode )下,通信双方都能同时接收和发送数据。电话网络是典型的全双工例子。以太网上的通信模式包括半双工和全双工两种:半双工模式下,共享物理介质的通信双方必须采用CSMA/CD机制来避免冲突。例如,10BASE5以太网的通信模式就必须是半双工模式。全双工模式下,通信双方可以同时实现双向通信,这种模式不会产生冲突,因此不需要使用CSMA/CD机制。例如, 10BASE-T以太网的通信模式就可以是全双工模式。同一物理链路上相连的两台设备的双工模式必须保持一致。|


### 1.2 以太网帧结构
网络中传输数据时需要定义并`遵循一些标准`,`以太网`是根据`IEEE 802.3标准`来`管理和控制数据帧的`。了解IEEE 802.3标准是充分理解以太网中链路层通信的基础。

||
|--|
|20世纪60年代以来,计算机网络得到了飞速发展。各大厂商和标准组织为了在数据通信网络领域占据主导地位,纷纷推出了各自的网络架构体系和标准,如1BM公司的SNA协议Novell公司的IPX/SPX协议,以及广泛流行的OSI参考模型和TCP/P协议。同时,各大厂商根据这些协议生产出了不同的硬件和软件。标准组织和厂商的共同努力促进了网络技术的快速发展和网络设备种类的迅速增长。|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/efb8f33d63724df18a5f978949c4935a.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|
|·网络通信中, “协议”和“标准”这两个词汇常常可以混用。同时,协议或标准本身又常常具有层次的特点。一般地,关注于逻辑数据关系的协议通常被称为上层协议,而关注于物理数据流的协议通常被称为底层协议。IEEE 802就是一套用来管理物理数据流在局域网中传输的标准,包括在局域网中传输物理数据的802.3以太网标准。除以太外,还有一些用来管理物理数据流在广域网中传输的标准,如PPP ( Point-to-Point Protocol ) ,高级数据链路控制HDLC ( High-Level Data Link Control ). |


|||
|--|--|
|国际标准化组织ISO于1984年提出了OSI RM (Open System Interconnection" Reference Model ,开放系统互连参考模型), OS1参考模型很快成为了计算机网络通信的基础模型。|OsI参考模型具有以下优点:简化了相关的网络操作;提供了不同厂商之间的兼容性;促进了标准化工作;结构上进行了分层;易于学习和操作。 |
|![在这里插入图片描述](https://img-blog.csdnimg.cn/648b0213b95a4f1b9cd082a4e370f416.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|`物理层`:在设备之间传输比特流,规定了电平、速度和电缆针脚。<br/>`数据链路层`:将比特组合成字节,再将字节组合成帧,使用链路层地址(以太网使用MAC地址)来访问介质,并进行差错检测。<br/>`网络层`:提供逻辑地址,供路由器确定路径。<br/>`传输层`:提供面向连接或非面向连接的数据传递以及进行重传前的差错检测。<br/>`会话层`:负责建立、管理和终止表示层实体之间的通信会话。该层的通信由不同设备中的应用程序之间的服务请求和响应组成。<br/>`表示层`:提供各种用于应用层数据的编码和转换功能,确保一个系统的应用层发送的数据能被另一个系统的应用层识别。</br>`应用层`: OSI参考模型中最靠近用户的一层,为应用程序提供网络服务。|


||
|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/8e77a9da4e6f4de69e8ff3970efd381d.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)|
|`TCP/IP模型`同样采用了分层结构,层与层相对独立但是相互之间也具备非常密切的协作关系。, <br>`TCP/P模型将网络分为四层`。TCP/P模型不关注底层物理介质,主要关注终端之间的逻辑数据流转发。TCP/IP模型的核心是网络层和传输层,网络层解决网络之间的逻辑转发问题传输层保证源端到目的端之间的可靠传输。最上层的应用层通过各种协议向终端用户提供业务应用。|


|||
|--|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/f89236f499374952b37939469b2bd02a.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70) | 应用数据需要经过TCP/P每一层处理之后才能通过网络传输到目的端`,每一层上都使用该层的协议数据单元PDU ( Protocol Data Unit )彼此交换信息`。`不同层的PDU中包含有不同的信息,因此PDU在不同层被赋予了不同的名称。`<br>如上层数据在`传输层`添加`TCP报头后得到的PDU被称为Segment (数据段)` ;<br>数据段被传递给`网络层`,网络层添加`IP报头得到的PDU被称为Packet (数据包)` ;<br>数据包被传递到`数据链路层`,封装`数据链路层报头得到的PDU被称为Frame (数据帧)` ;<br>最后,帧被转换为`比特`,通过网络介质传输。这种协议栈逐层向下传递数据,并添加报头和报尾的过程称为`封装`。|


|数据链路层||
|--|--|
|![在这里插入图片描述](https://img-blog.csdnimg.cn/7e5d3ac08dd5434d9aec8a2fcee3a5d0.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)||
|![在这里插入图片描述](https://img-blog.csdnimg.cn/9127fdf442c644e4954aefa0052c5f56.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)||
|![在这里插入图片描述](https://img-blog.csdnimg.cn/b036f7fd40854f998b8b7dde3719fb05.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)||
|![在这里插入图片描述](https://img-blog.csdnimg.cn/be6cc16e20dc426dbad0f7b1c76a41af.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)||






### 1.3 1P编址
### 1.4 ICMP协议
### 1.5 ARP协议
### 1.6 传输层协议
### 1.7 数据转发过程
