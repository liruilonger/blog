---
title: 华为云服务DevOps高级工程师(HCIP)-DevCloud 学习笔记
tags: 
  - HCIP
  - DevOps
categories: 
  - DevOps
toc: true
recommend: 1
keywords: 华为
uniqueId: '2021-07-17 07:47:14/华为云服务DevOps高级工程师(HCIP).html'
mathJax: false
date: 2021-07-17 15:47:14
thumbnail:
---
> 摘要
首页显示摘要内容(替换成自己的)
<!-- more -->
## 写在前面
***
+ 用`Markdown`写博客一年多了，最开始是用富文本写，后来发现`Markdown`更适合我，而且`CSDN`提供了导入导出的功能，图片可以云存储，所以导出了博文在本地也可以直接看，尤其是笔记类型很方便。所以这里总结一下自己常用模板和小伙伴们分享下
+ 笔记主要是关于自己使用`Morkdown`的一些`常用模板`的`版式`总结。<font color="#C0C4CC">[这里写整理笔记的具体方式，是读那本书笔记，看那个视频笔记，或者对于那个问题的笔记]
+ 笔记由两部分内容构成: `笔记类模板`和`技术点问题类模板`


**<font color="009688"> 傍晚时分，你坐在屋檐下，看着天慢慢地黑下去，心里寂寞而凄凉，感到自己的生命被剥夺了。当时我是个年轻人，但我害怕这样生活下去，衰老下去。在我看来，这是比死亡更可怕的事。--------王小波**</font><font color="#C0C4CC">[这里写一句鼓励自己的话,可以加一个颜色(可选)]</font>
 ***


# 一、持续规划与设计
## .2 敏捷项目规划
### 1.2.1 创建华为云DevCloud项目
### 1.2.2 使用Scrum项目模板进行项目规划，并管理Epic和Feature
表是对不同类型工作项的说明，以及凤凰商城项目的映射关系：
|工作项类型|	说明|
|<font color=royalblue>Epic</font>	|通常是公司`重要战略举措`，比如 “凤凰商城” ，对于“无极限零部件公司”是一个与企业生存攸关的关键战略措施。|
|<font color=blue>Feature</font>|	通常是对`用户有价值的功能`，用户可以通过使用特性满足他们的需求。比如 “凤凰商城” 中的 “门店网络查询功能”，特性通常会通过多个迭代持续交付。|
|<font color=yellowgreen>Story</font>|	通常是对一个功能进行`用户场景细分`，并且能在一个迭代内完成，Story通常需要满足`INVEST原则`。|
|<font color=green>Task</font>|	通常是`用户故事的细分`，`准备环境`，`准备测试用例`等都可以是完成Story的细分任务。|


![在这里插入图片描述](https://img-blog.csdnimg.cn/20210717161718494.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3Nhbmhld3V5YW5n,size_16,color_FFFFFF,t_70)

步骤 2 在Epic“凤凰商城”下方单击图标“插入子节点 ” 新建Feature， 输入标题“门店网络”，回车保存。

步骤 3 按照同样的方式，为Feature“门店网络”添加Story“作为用户应该可以查看、查询所有门店网络”。

步骤 4 项目规划折叠。某用户角色只关心Feature级别的列表，单击“Feature”旁边图标 完成折叠。折叠后如下图所示：

步骤 5 导出项目规划。 您可以将项目规划导出到Excel，以条目化的方式查看以及管理。单击右上角图标 ，在下拉列表中选择导出方式。